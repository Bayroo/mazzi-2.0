//
//  PostsVC.swift
//  Mazzi
//
//  Created by Bayara on 11/22/18.
//  Copyright © 2018 Mazzi Application. All rights reserved.
//

import Foundation
import UIKit
import KVNProgress
import Apollo

class PostsVC: UIViewController, UITableViewDelegate, UITableViewDataSource , UIPickerViewDelegate , UIPickerViewDataSource{
  
@IBOutlet weak var mytableView: UITableView!
var classId = GlobalStaticVar.classId
var myId = SettingsViewController.userId
var posts = [PostsQuery.Data.Post]()
var selectedTopic = ""
var filteredPosts = [PostsByTopicQuery.Data.PostsByTopic]()
var filteredByUsersPosts = [PostsByClassUserIdQuery.Data.PostsByClassUserId]()
var files = [PostsQuery.Data.Post.File]()
var dateformatter = DateFormatter()
var selectedIndex = 0
var teacherId = GlobalStaticVar.teacherId
var className = GlobalStaticVar.className
var ownerId = ""
var attachmentCount = 0
var skip = 0
var limit = 20
var mynickname = ""
var mylastname = ""
var myImg = ""
var settings = ClassQuery.Data.Class.ClassSetting()
let refreshControl = UIRefreshControl()
var isFilterClicked = false
//---------------pickerView----------------
    var subview = UIView()
    var pickerTitle = UILabel()
    var closePicker = UIButton()
    var pickerView = UIPickerView()
    var isPickerShow = false
    var filterBtn = UIButton()
    var isFilterDone = false
    var selectedTopicId = ""
    var selectedTeacherId = ""
    var AllusersList = [ClassUsersListQuery.Data.ClassesUserListClassId]()
    var allTopics = [ClassTopicsQuery.Data.ClassTopic.Result]()
    var isfilterByNameBtnClicked = false
//------------------------------------------
    let apollo: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken]
        let url = URL(string:SettingsViewController.graphQL)!
        
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    weak var delegate: MClassListVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customize()
        self.initPickerView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        self.navigationController?.navigationBar.topItem?.title = ""
//        if self.teacherId == self.myId {
//            let rightBtn = UIBarButtonItem(image: UIImage(named:"editmenu"), style: .plain, target: self, action: #selector(self.goSettings))
//            self.navigationController?.navigationBar.topItem?.rightBarButtonItem = rightBtn
//        }
    }
    
    @objc func goSettings(){
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "setting") as? ClassroomSettingVC
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isFilterClicked = false
        self.isfilterByNameBtnClicked = false
        self.dismissPicker()
        checkInternet()
        self.mytableView.isHidden = true
        if SettingsViewController.online == true{
            self.isFilterDone = false
            _ = self.apollo.clearCache()
//            self.getSettings()
            self.selectedIndex = 0
//            self.getPosts()
//            self.getUserRole()
//            self.getUsersList()
            if self.allTopics.count == 0 {
//                self.getTopicList()
            }
        }else{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу")
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.navigationController?.navigationBar.isHidden = true
        self.isFilterDone = false
        self.isfilterByNameBtnClicked = false
        self.dismissPicker()
    }
    @IBAction func joinClassBtn(_ sender: Any) {
        
    }
    
    func initPickerView(){
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.subview.frame = CGRect(x:0, y:self.view.bounds.height,width: self.view.bounds.width, height:250)
        self.pickerTitle.frame = CGRect(x:self.view.bounds.width/2 - 75, y:4, width:150,height:20)
        self.pickerView.frame = CGRect(x:0,y:28,width:self.view.bounds.width,height:220)
        self.closePicker.frame = CGRect(x:4,y:4,width:20,height:20)
        self.filterBtn.frame = CGRect(x: self.view.bounds.width - 100, y: 0, width: 120, height: 30)
        self.filterBtn.layer.cornerRadius = 5
        self.filterBtn.layer.borderWidth = 0
        self.filterBtn.layer.borderColor = UIColor.white.cgColor
        self.filterBtn.setTitle("Шүүх", for: .normal)
        self.filterBtn.setTitleColor(UIColor.white, for: .normal)
        self.filterBtn.layer.masksToBounds = true
        self.filterBtn.titleLabel?.font = GlobalVariables.customFont12
        self.filterBtn.addTarget(self, action: #selector(self.FilterBtnClicked), for: .touchUpInside)
        self.subview.backgroundColor = GlobalVariables.todpurple
        self.pickerView.backgroundColor = UIColor.white
        self.pickerTitle.textAlignment = NSTextAlignment.center
        self.pickerTitle.text = ""
        self.pickerTitle.font = GlobalVariables.customFont12
        self.pickerTitle.textColor = UIColor.white
        self.closePicker.setImage(UIImage(named: "closewhite"), for: UIControl.State.normal)
        self.closePicker.addTarget(self, action: #selector(dismissPicker), for: .touchUpInside)
        
        self.subview.addSubview(self.pickerTitle)
        self.subview.addSubview(self.pickerView)
        self.subview.addSubview(self.closePicker)
        self.subview.addSubview(self.filterBtn)
        self.view.addSubview(self.subview)
    }
    
    func customize(){
//        self.navigationItem.leftBarButtonItem  = UIBarButtonItem(image: UIImage(named:"Rounded"), style: .plain, target: self, action: #selector(backtoRoot))
      
        self.mytableView.delegate = self
        self.mytableView.dataSource = self
        self.mytableView.register((UINib(nibName: "FilterCell", bundle: nil)), forCellReuseIdentifier: "filtercell")
        self.mytableView.register((UINib(nibName: "postsCell", bundle: nil)), forCellReuseIdentifier: "postsCell")
        self.mytableView.register((UINib(nibName: "InsertPostCell", bundle: nil)), forCellReuseIdentifier: "insertcell")
        self.mytableView.refreshControl = self.refreshControl
        self.refreshControl.addTarget(self, action: #selector(reloadPosts), for: .valueChanged)
        let userInfo = userDefaults.dictionary(forKey: "userInfo")
        self.mynickname = userInfo!["nickname"] as! String
        self.mylastname = userInfo!["lastname"] as! String
        self.myImg = userInfo!["image"] as! String
        self.getSettings()
    }
    
    func getSettings(){
        let query = ClassQuery(classId: GlobalStaticVar.classId)
        apollo.fetch(query: query) { result, error in
            if result?.data?.class != nil {
                self.settings = (result?.data?.class?.classSettings)!
                self.refreshControl.endRefreshing()
            }else{
                
            }
        }
    }
    
    @objc func reloadPosts(){
        self.dismissPicker()
        self.isfilterByNameBtnClicked = false
        self.isFilterDone = false
        checkInternet()
        if SettingsViewController.online {
            self.getPosts()
            self.getSettings()
            self.refreshControl.endRefreshing()
        }else{
             self.refreshControl.endRefreshing()
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу")
        }
    }
    
    func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
        var gradientImage:UIImage?
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            gradientLayer.render(in: context)
            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        UIGraphicsEndImageContext()
        return gradientImage
    }
    
    @objc func backtoRoot(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func getPosts(){
        self.skip = 0
        let query = PostsQuery(userId: self.myId, classId: self.classId, skip: self.skip, limit: self.limit)
        KVNProgress.show(withStatus: "", on: self.view)
        apollo.fetch(query: query) { result, error in
            KVNProgress.dismiss()
            if result?.data?.posts != nil {
                self.posts = (result?.data?.posts)! as! [PostsQuery.Data.Post]
         
                if GlobalStaticVar.notifiedComment == true {
                    self.performSegue(withIdentifier: "toComment", sender: self)
                }
                if GlobalStaticVar.isApprovePost == true {
                    let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "setting") as? ClassroomSettingVC
                    self.navigationController?.pushViewController(viewController!, animated: false)
                }
                if GlobalStaticVar.newPost == true {
                    self.performSegue(withIdentifier: "toComment", sender: self)
                }
                
                self.mytableView.reloadData()
                self.refreshControl.endRefreshing()
            }else{
                 self.refreshControl.endRefreshing()
            }
        }
    }
    
     func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.posts.count > 0 {
            let lastElement = self.posts.count - 1
            if indexPath.row == lastElement {
                self.skip = self.skip + 20
                if self.posts.count == self.skip {
                    let query = PostsQuery(userId: self.myId, classId: self.classId, skip: self.skip, limit: self.limit)
                    KVNProgress.show(withStatus: "", on: self.view)
                    apollo.fetch(query: query) { result, error in
                        KVNProgress.dismiss()
                        if result?.data?.posts != nil {
                            let p = (result?.data?.posts)! as! [PostsQuery.Data.Post]
                            self.posts.append(contentsOf: p)
                             DispatchQueue.main.async {
                                self.mytableView.reloadData()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let title : UILabel = UILabel()
        if section == 0 {
            title.textColor = GlobalVariables.purple
            title.text =  ""
        }else if section == 1 {
            title.textColor = GlobalVariables.purple
            title.text =  ""
        }else {
            title.textColor = GlobalVariables.purple
            title.text =  ""
        }
        return title.text
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = GlobalVariables.F5F5F5
        let headerBtn = UIButton(frame: CGRect(x: self.view.bounds.width/2 - 75, y: 0, width: 150, height: 30))
        headerBtn.setTitle("Шүүж харах", for: .normal)
        headerBtn.titleLabel?.font = GlobalVariables.customFont14
        headerBtn.setTitleColor(GlobalVariables.todpurple, for: .normal)
        headerBtn.addTarget(self, action: #selector(showFilterPicker), for: .touchUpInside)
        headerView.addSubview(headerBtn)
        let icon = UIImageView(frame: CGRect(x: self.view.bounds.width/2 + 55, y: 10, width: 10, height: 10))
        if self.isFilterClicked {
              icon.image = UIImage(named: "expand-arrow")
        }else{
              icon.image = UIImage(named: "expand-button")
        }
        headerView.addSubview(icon)
        return headerView
    }
    
    @objc func showFilterPicker(){
        self.dismissPicker()
        self.isfilterByNameBtnClicked = false
        self.isFilterClicked = !self.isFilterClicked
        self.mytableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 30
        }else{
            return 0
        }
    }
//
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if self.isFilterClicked{
                    return 50
            }else{
                    return 0
                }
        }else {
          return  UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else if section == 1 {
            return 1
        }else{
            if self.isFilterDone == true {
                if self.isfilterByNameBtnClicked == true {
                    return self.filteredByUsersPosts.count
                }else{
                    return self.filteredPosts.count
                }
            }else{
                return self.posts.count
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        checkInternet()
         if  SettingsViewController.online {
            if self.isPickerShow == false{
                if indexPath.section == 0{
                    
                }else if indexPath.section == 1{
                    if self.settings.restrictedPost == true && self.settings.approvePost == false{
                        if self.teacherId != self.myId {
                            KVNProgress.showError(withStatus: "Энэ ангид зөвхөн багш нийтлэл оруулах эрхтэй!")
                        }else{
                            GlobalStaticVar.isPostCreate = true
                            self.performSegue(withIdentifier: "updatePost", sender: self)
                        }
                    }else if self.settings.restrictedPost == true && self.settings.approvePost == true {
                        if self.teacherId != self.myId {
                            let alert = UIAlertController(title: "", message: "Таны нийтлэлийг багш зөвшөөрсөн тохиолдолд нийтлэнэ!", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "ОК", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction) -> Void in
                                GlobalStaticVar.isPostCreate = true
                                self.performSegue(withIdentifier: "updatePost", sender: self)
                            }))
                            alert.addAction(UIAlertAction(title: "Болих", style: UIAlertAction.Style.cancel, handler:{ (alert:UIAlertAction) -> Void in
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }else{
                            GlobalStaticVar.isPostCreate = true
                            self.performSegue(withIdentifier: "updatePost", sender: self)
                        }
                    }else{
                        GlobalStaticVar.isPostCreate = true
                        self.performSegue(withIdentifier: "updatePost", sender: self)
                    }
                    
                }else{
                    self.selectedIndex = indexPath.row
                    self.performSegue(withIdentifier: "toComment", sender: self)
                }
            }else{
                  self.dismissPicker()
            }
         }else{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу")
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
       _ = self.apollo.clearCache()
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "filtercell", for: indexPath) as! filterCell
            cell.initCell()
            cell.contentView.backgroundColor = GlobalVariables.F5F5F5
            if isFilterDone == true {
                if self.isfilterByNameBtnClicked == true{
                    cell.nameBtn.setTitle("Бүгдийг харах", for: .normal)
                    cell.nameBtn.setTitleColor(UIColor.red, for: .normal)
                    
                    cell.topicBtn.setTitle("Хичээлээр", for: .normal)
                    cell.topicBtn.setTitleColor(UIColor.black, for: .normal)
                }else{
                    cell.topicBtn.setTitle("Бүгдийг харах", for: .normal)
                    cell.topicBtn.setTitleColor(UIColor.red, for: .normal)
                    
                    cell.nameBtn.setTitle("Хэрэглэгчээр", for: .normal)
                    cell.nameBtn.setTitleColor(UIColor.black, for: .normal)
                }
            }else{
                cell.nameBtn.setTitle("Хэрэглэгчээр", for: .normal)
                cell.nameBtn.setTitleColor(UIColor.black, for: .normal)
                
                cell.topicBtn.setTitle("Хичээлээр", for: .normal)
                cell.topicBtn.setTitleColor(UIColor.black, for: .normal)
            }
            
             cell.nameBtn.addTarget(self, action: #selector(showPickerTeacher), for: .touchUpInside)
             cell.topicBtn.addTarget(self, action: #selector(showPickerTopic), for: .touchUpInside)
            return cell
            
        }else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "insertcell", for: indexPath) as! InsertPostCell
            cell.initcell()
            if self.myImg == ""{
                cell.imgView.image = UIImage(named: "circle-avatar")
            }else{
                cell.imgView.sd_setImage(with:URL(string: "\(SettingsViewController.fileurl)\(self.myImg)"),placeholderImage: UIImage(named: "circle-avatar"))
            }
            cell.accessoryType = .disclosureIndicator
            return cell
        }else{
            if isFilterDone == true {
                if self.isfilterByNameBtnClicked == false{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "postsCell", for: indexPath) as! postsCell
                    cell.initCell()
                    
                    var filesCount = 0
                    var linksCount = 0
                    
                    if self.filteredPosts[indexPath.row].file != nil{
                        filesCount = (self.filteredPosts[indexPath.row].file?.count)!
                    }
                    if  self.filteredPosts[indexPath.row].link != nil{
                        linksCount = (self.filteredPosts[indexPath.row].link?.count)!
                    }
                    cell.filterBtuserBtn.addTarget(self, action: #selector(self.toPushProfile(_:)), for: .touchUpInside)
                    cell.filterBtuserBtn.tag = indexPath.row

                    self.attachmentCount = filesCount + linksCount
                    let topic = self.filteredPosts[indexPath.row].topicId?.name ?? ""
                    if topic == "" {
                        cell.filterBtn.isHidden = true
                    }else{
                        if self.filteredPosts[indexPath.row].topicId?.id != "5ca2b669a861d443508b75e9"{
                            cell.filterBtn.isHidden = false
                            cell.filterBtn.setTitle( topic, for: .normal)
                        }else{
                            cell.filterBtn.isHidden = true
                        }
                    }
                    cell.attachLabel.isHidden = self.attachmentCount == 0 ? true : false
                    cell.attachLabel.text = "\(self.attachmentCount) \("Хавсралт")"
                    cell.label.text = self.filteredPosts[indexPath.row].content ?? ""
                    cell.commentLabel.isHidden = self.filteredPosts[indexPath.row].commentCount == 0 ? true : false
                    cell.commentLabel.text = "\(self.filteredPosts[indexPath.row].commentCount!) \("Сэтгэгдэл")"
                    let lastlogin = NSDate(timeIntervalSince1970: (self.filteredPosts[indexPath.row].postedDate!/1000))
                    self.dateformatter.dateStyle = .short
                    self.dateformatter.timeStyle = .short
                    let last = self.dateformatter.string(from: lastlogin as Date)
                    cell.createdLabel.text = last
                    let nickname = self.filteredPosts[indexPath.row].ownerId!.nickname ?? ""
                    let lastname = self.filteredPosts[indexPath.row].ownerId!.lastname ?? ""
                    cell.nameLabel.text = lastname
                    cell.lastnameLabel.text = nickname
                    let img = self.filteredPosts[indexPath.row].ownerId!.image!
                    
                    if img == ""{
                        cell.proImg.image = UIImage(named: "circle-avatar")
                    }else{
                        cell.proImg.sd_setImage(with:URL(string: "\(SettingsViewController.fileurl)\(img)"),placeholderImage: UIImage(named: "circle-avatar"))
                    }
                    
                    cell.editBtn.tag = indexPath.row
                    cell.editBtn.addTarget(self, action: #selector(editPost(_:)), for: .touchUpInside)
                    self.attachmentCount = 0
                    return cell
                    
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "postsCell", for: indexPath) as! postsCell
                    cell.initCell()
                    
                    var filesCount = 0
                    var linksCount = 0
                    
                    if self.filteredByUsersPosts[indexPath.row].file != nil{
                        filesCount = (self.filteredByUsersPosts[indexPath.row].file?.count)!
                    }
                    if  self.filteredByUsersPosts[indexPath.row].link != nil{
                        linksCount = (self.filteredByUsersPosts[indexPath.row].link?.count)!
                    }
                    
                    self.attachmentCount = filesCount + linksCount
                    let topic = self.filteredByUsersPosts[indexPath.row].topicId?.name ?? ""
                    if topic == ""{
                        cell.filterBtn.isHidden = true
                    }else{
                        if self.filteredByUsersPosts[indexPath.row].topicId?.id != "5ca2b669a861d443508b75e9"{
                            cell.filterBtn.isHidden = false
                            cell.filterBtn.setTitle( topic, for: .normal)
                        }else{
                            cell.filterBtn.isHidden = true
                        }
                    }
                    cell.attachLabel.isHidden = self.attachmentCount == 0 ? true : false
                    cell.attachLabel.text = "\(self.attachmentCount) \("Хавсралт")"
                    cell.label.text = self.filteredByUsersPosts[indexPath.row].content ?? ""
                    cell.commentLabel.isHidden = self.filteredByUsersPosts[indexPath.row].commentCount == 0 ? true : false
                    cell.commentLabel.text = "\(self.filteredByUsersPosts[indexPath.row].commentCount!) \("Сэтгэгдэл")"
                    let lastlogin = NSDate(timeIntervalSince1970: (self.filteredByUsersPosts[indexPath.row].postedDate!/1000))
                    self.dateformatter.dateStyle = .short
                    self.dateformatter.timeStyle = .short
                    let last = self.dateformatter.string(from: lastlogin as Date)
                    cell.createdLabel.text = last
                    let nickname = self.filteredByUsersPosts[indexPath.row].ownerId!.nickname ?? ""
                    let lastname = self.filteredByUsersPosts[indexPath.row].ownerId!.lastname ?? ""
                    cell.nameLabel.text = lastname
                    cell.lastnameLabel.text = nickname
                    let img = self.filteredByUsersPosts[indexPath.row].ownerId!.image!
                    
                    if img == ""{
                        cell.proImg.image = UIImage(named: "circle-avatar")
                    }else{
                        cell.proImg.sd_setImage(with:URL(string: "\(SettingsViewController.fileurl)\(img)"),placeholderImage: UIImage(named: "circle-avatar"))
                    }
                    cell.filterBtuserBtn.addTarget(self, action: #selector(self.toPushProfile(_:)), for: .touchUpInside)
                    cell.filterBtuserBtn.tag = indexPath.row
                    
                    cell.editBtn.tag = indexPath.row
                    cell.editBtn.addTarget(self, action: #selector(editPost(_:)), for: .touchUpInside)
                    self.attachmentCount = 0
                    return cell
                }
            }else{

                let cell = tableView.dequeueReusableCell(withIdentifier: "postsCell", for: indexPath) as! postsCell
                cell.initCell()
                
                var filesCount = 0
                var linksCount = 0
                cell.filterBtuserBtn.addTarget(self, action: #selector(self.toPushProfile(_:)), for: .touchUpInside)
                cell.filterBtuserBtn.tag = indexPath.row
                
                if self.posts[indexPath.row].file != nil{
                    filesCount = (self.posts[indexPath.row].file?.count)!
                }
                if  self.posts[indexPath.row].link != nil{
                    linksCount = (self.posts[indexPath.row].link?.count)!
                }
                
                self.attachmentCount = filesCount + linksCount
                let topic = self.posts[indexPath.row].topicId?.name ?? ""
                if topic == "" {
                      cell.filterBtn.isHidden = true
                }else{
                    if self.posts[indexPath.row].topicId?.id != "5ca2b669a861d443508b75e9"{
                        cell.filterBtn.isHidden = false
                        cell.filterBtn.setTitle( topic, for: .normal)
                    }else{
                        cell.filterBtn.isHidden = true
                    }
                }
              
                cell.attachLabel.isHidden = self.attachmentCount == 0 ? true : false
                cell.attachLabel.text = "\(self.attachmentCount) \("Хавсралт")"
                cell.label.text = self.posts[indexPath.row].content ?? ""
                cell.commentLabel.isHidden = self.posts[indexPath.row].commentCount == 0 ? true : false
                cell.commentLabel.text = "\(self.posts[indexPath.row].commentCount!) \("Сэтгэгдэл")"
                let lastlogin = NSDate(timeIntervalSince1970: (self.posts[indexPath.row].postedDate!/1000))
                self.dateformatter.dateStyle = .short
                self.dateformatter.timeStyle = .short
                let last = self.dateformatter.string(from: lastlogin as Date)
                cell.createdLabel.text = last
                let nickname = self.posts[indexPath.row].ownerId?.nickname ?? ""
                let lastname = self.posts[indexPath.row].ownerId?.lastname ?? ""
                cell.nameLabel.text = lastname
                cell.lastnameLabel.text = nickname
                let img = self.posts[indexPath.row].ownerId?.image ?? ""
                
                if img == ""{
                    cell.proImg.image = UIImage(named: "circle-avatar")
                }else{
                    cell.proImg.sd_setImage(with:URL(string: "\(SettingsViewController.fileurl)\(img)"),placeholderImage: UIImage(named: "circle-avatar"))
                }
                
                cell.editBtn.tag = indexPath.row
                cell.editBtn.addTarget(self, action: #selector(editPost(_:)), for: .touchUpInside)
                self.attachmentCount = 0
                return cell
            }
         
        }
       
    }
    
     @objc func toPushProfile(_ sender:Any) {
        if self.isFilterDone == true{
            if self.isfilterByNameBtnClicked == true {
                if self.myId != self.filteredByUsersPosts[(sender as AnyObject).tag].ownerId?.id ?? "" {
                    let viewController = storyboard?.instantiateViewController(withIdentifier: "ProfileContact") as! ProfileContact
                    viewController.contactid = self.filteredByUsersPosts[(sender as AnyObject).tag].ownerId?.id ?? ""
                    viewController.username = self.filteredByUsersPosts[(sender as AnyObject).tag].ownerId?.nickname ?? ""
                    viewController.membernickname = self.filteredByUsersPosts[(sender as AnyObject).tag].ownerId?.nickname ?? ""
                    viewController.memberdate = self.filteredByUsersPosts[(sender as AnyObject).tag].ownerId?.birthday ?? 0
                    viewController.member_onlineAt = self.filteredByUsersPosts[(sender as AnyObject).tag].ownerId?.onlineAt ?? 0
                    viewController.memberphone = self.filteredByUsersPosts[(sender as AnyObject).tag].ownerId?.phone ?? ""
                    viewController.memberImg = self.filteredByUsersPosts[(sender as AnyObject).tag].ownerId?.image ?? ""
                    let navController = UINavigationController(rootViewController: viewController)
                    GlobalVariables.isFromClassMembers = true
                    self.present(navController, animated:true, completion: nil)
                }
            }else{
                if self.myId != self.filteredPosts[(sender as AnyObject).tag].ownerId?.id ?? "" {
                    let viewController = storyboard?.instantiateViewController(withIdentifier: "ProfileContact") as! ProfileContact
                    viewController.contactid = self.filteredPosts[(sender as AnyObject).tag].ownerId?.id ?? ""
                    viewController.username = self.filteredPosts[(sender as AnyObject).tag].ownerId?.nickname ?? ""
                    viewController.membernickname = self.filteredPosts[(sender as AnyObject).tag].ownerId?.nickname ?? ""
                    viewController.memberdate = self.filteredPosts[(sender as AnyObject).tag].ownerId?.birthday ?? 0
                    viewController.member_onlineAt = self.filteredPosts[(sender as AnyObject).tag].ownerId?.onlineAt ?? 0
                    viewController.memberphone = self.filteredPosts[(sender as AnyObject).tag].ownerId?.phone ?? ""
                    viewController.memberImg = self.filteredPosts[(sender as AnyObject).tag].ownerId?.image ?? ""
                    let navController = UINavigationController(rootViewController: viewController)
                    GlobalVariables.isFromClassMembers = true
                    self.present(navController, animated:true, completion: nil)
                }
            }
        }else{
            if self.myId != self.posts[(sender as AnyObject).tag].ownerId?.id ?? "" {
                let viewController = storyboard?.instantiateViewController(withIdentifier: "ProfileContact") as! ProfileContact
                viewController.contactid = self.posts[(sender as AnyObject).tag].ownerId?.id ?? ""
                viewController.username = self.posts[(sender as AnyObject).tag].ownerId?.nickname ?? ""
                viewController.membernickname = self.posts[(sender as AnyObject).tag].ownerId?.nickname ?? ""
                viewController.memberdate = self.posts[(sender as AnyObject).tag].ownerId?.birthday ?? 0
                viewController.member_onlineAt = self.posts[(sender as AnyObject).tag].ownerId?.onlineAt ?? 0
                viewController.memberphone = self.posts[(sender as AnyObject).tag].ownerId?.phone ?? ""
                viewController.memberImg = self.posts[(sender as AnyObject).tag].ownerId?.image ?? ""
                let navController = UINavigationController(rootViewController: viewController)
                GlobalVariables.isFromClassMembers = true
                self.present(navController, animated:true, completion: nil)
            }
        }
    }
    
    @objc func editPost(_ sender:Any) {
        self.selectedIndex = (sender as AnyObject).tag!
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Засварлах", style: .default, handler: { action in
            GlobalStaticVar.isPostCreate = false
            self.performSegue(withIdentifier: "updatePost", sender: self)
        }))
        alert.addAction(UIAlertAction(title: "Устгах", style: .default, handler:{action in
            let subAlert = UIAlertController(title: nil, message: "Устгахдаа итгэлтэй байна уу", preferredStyle: .alert)
            subAlert.addAction(UIAlertAction(title:"Тийм", style: .default, handler: { (action) in
                let postId = self.posts[(sender as AnyObject).tag].id
                let mutation = DeletePostMutation(postId: postId!, classId: self.classId, userId: self.myId)
                self.apollo.perform(mutation: mutation) { result, error in
                    if result?.data?.deletePost?.error == false{
                        KVNProgress.showSuccess(withStatus: "Амжилттай устлаа")
                        self.getPosts()
                    }else{
                        KVNProgress.showError(withStatus: result?.data?.deletePost?.message)
                    }
//                     print("error:: ",result?.data?.deletePost?.error as Any)
                }
                }))
            
            subAlert.addAction(UIAlertAction(title: "Үгүй", style: .cancel, handler: nil))
            self.present(subAlert,animated: true)
        }))
         alert.addAction(UIAlertAction(title: "Болих", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "updatePost"{
            
            if GlobalStaticVar.isPostCreate{
                let controller = segue.destination as! UpdatePostVC
                controller.classId = self.classId
                controller.teacherId = self.teacherId
                controller.posterImg = self.myImg
                controller.posterNickname = self.mynickname
                controller.posterLastname = self.mylastname
            }else{
                let controller = segue.destination as! UpdatePostVC
                controller.content = self.posts[self.selectedIndex].content ?? ""
                //todo here is error then commented
                if (self.posts[self.selectedIndex].file?.count)! > 0{
//                    controller.files = [self.posts[self.selectedIndex].file]
                    controller.files = self.posts[self.selectedIndex].file! as! [SinglepostQuery.Data.Post.File]
                }
                controller.posterImg = self.posts[self.selectedIndex].ownerId!.image ?? ""
                controller.posterNickname = self.posts[self.selectedIndex].ownerId!.nickname ?? ""
                controller.posterLastname = self.posts[self.selectedIndex].ownerId!.lastname ?? ""
                controller.postedDate = self.posts[self.selectedIndex].postedDate ?? 0
                controller.classId = self.classId
                controller.postId = self.posts[self.selectedIndex].id ?? ""
                controller.teacherId = self.teacherId
            }
            
        }else if segue.identifier == "toComment"{
            let controller = segue.destination as! CommentsVC

            if GlobalStaticVar.notifiedComment == true {
                for i in 0..<self.posts.count {
                    if self.posts[i].postId == GlobalStaticVar.notifiedRoodPostId {
                        controller.posterImg = self.posts[i].ownerId?.image ?? ""
                        controller.posterLastName = self.posts[i].ownerId?.lastname ?? ""
                        controller.posterName = self.posts[i].ownerId?.nickname ?? ""
                        controller.postContent = self.posts[i].content ?? ""
                        controller.classId = GlobalStaticVar.notifiedClassId
                        controller.postId =  GlobalStaticVar.notifiedRoodPostId
                        controller.postDate = self.posts[i].postedDate ?? 0
                        controller.postOwnerId = self.posts[i].ownerId?.id ?? ""
                        if self.posts[self.selectedIndex].topicId?.id != "5ca2b669a861d443508b75e9"{
                            controller.topic = self.posts[self.selectedIndex].topicId?.name ?? ""
                        }
                    }
                }
            }else if GlobalStaticVar.newPost == true{
                for i in 0..<self.posts.count {
                    if self.posts[i].id == GlobalStaticVar.notifiedPostId {
                        controller.posterImg = self.posts[i].ownerId?.image ?? ""
                        controller.posterLastName = self.posts[i].ownerId?.lastname ?? ""
                        controller.posterName = self.posts[i].ownerId?.nickname ?? ""
                        controller.postContent = self.posts[i].content ?? ""
                        controller.classId = GlobalStaticVar.notifiedClassId
                        controller.postId =  GlobalStaticVar.notifiedPostId
                        controller.postDate = self.posts[i].postedDate ?? 0
                        controller.postOwnerId = self.posts[i].ownerId?.id ?? ""
                        if self.posts[self.selectedIndex].topicId?.id != "5ca2b669a861d443508b75e9"{
                            controller.topic = self.posts[self.selectedIndex].topicId?.name ?? ""
                        }
                    }
                }
            }else{
                if self.isFilterDone == true {
                    if self.isfilterByNameBtnClicked == false {
                        controller.posterImg = self.filteredPosts[self.selectedIndex].ownerId?.image ?? ""
                        controller.posterLastName = self.filteredPosts[self.selectedIndex].ownerId?.lastname ?? ""
                        controller.posterName = self.filteredPosts[self.selectedIndex].ownerId?.nickname ?? ""
                        controller.postContent = self.filteredPosts[self.selectedIndex].content ?? ""
                        controller.classId = self.classId
                        controller.postId = self.filteredPosts[self.selectedIndex].id ?? ""
                        controller.postDate = self.filteredPosts[self.selectedIndex].postedDate ?? 0
                        controller.postOwnerId = self.filteredPosts[self.selectedIndex].ownerId?.id ?? ""
                        if self.filteredPosts[self.selectedIndex].topicId?.id != "5ca2b669a861d443508b75e9"{
                            controller.topic = self.filteredPosts[self.selectedIndex].topicId?.name ?? ""
                        }
                    }else if self.isfilterByNameBtnClicked == true {
                        controller.posterImg = self.filteredByUsersPosts[self.selectedIndex].ownerId?.image ?? ""
                        controller.posterLastName = self.filteredByUsersPosts[self.selectedIndex].ownerId?.lastname ?? ""
                        controller.posterName = self.filteredByUsersPosts[self.selectedIndex].ownerId?.nickname ?? ""
                        controller.postContent = self.filteredByUsersPosts[self.selectedIndex].content ?? ""
                        controller.classId = self.classId
                        controller.postId = self.filteredByUsersPosts[self.selectedIndex].id ?? ""
                        controller.postDate = self.filteredByUsersPosts[self.selectedIndex].postedDate ?? 0
                        controller.postOwnerId = self.filteredByUsersPosts[self.selectedIndex].ownerId?.id ?? ""
                        if self.filteredByUsersPosts[self.selectedIndex].topicId?.id != "5ca2b669a861d443508b75e9"{
                            controller.topic = self.filteredByUsersPosts[self.selectedIndex].topicId?.name ?? ""
                        }
                    }
                }else{
                    controller.posterImg = self.posts[self.selectedIndex].ownerId?.image ?? ""
                    controller.posterLastName = self.posts[self.selectedIndex].ownerId?.lastname ?? ""
                    controller.posterName = self.posts[self.selectedIndex].ownerId?.nickname ?? ""
                    controller.postContent = self.posts[self.selectedIndex].content ?? ""
                    controller.classId = self.classId
                    controller.postId = self.posts[self.selectedIndex].id ?? ""
                    controller.postDate = self.posts[self.selectedIndex].postedDate ?? 0
                    controller.postOwnerId = self.posts[self.selectedIndex].ownerId?.id ?? ""
                    if self.posts[self.selectedIndex].topicId?.id != "5ca2b669a861d443508b75e9"{
                         controller.topic = self.posts[self.selectedIndex].topicId?.name ?? ""
                    }
                }
            }
        }
    }
    
    @IBAction func goExam(_ sender: Any) {
        if self.teacherId == self.myId {
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "beginexam") as? BeginExamVC
            self.navigationController?.pushViewController(viewController!, animated: true)
        }else{
          self.performSegue(withIdentifier: "tocdio", sender: self)
        }
    }
    
    func getUserRole(){
        let query = ClassUserListQuery(userId: self.myId, classId: self.classId)
         apollo.fetch(query: query) { result, error in
            if result?.data?.classUserList != nil {
                GlobalStaticVar.classRole = result?.data?.classUserList?.classRole ?? ""
            }else{
                
            }
        }
    }
    
    @objc func filterByTopic(){
        isfilterByNameBtnClicked = false
        self.filteredByUsersPosts = []
        self.skip = 0
        self.selectedTopic =  self.selectedTopicId
        let query = PostsByTopicQuery(userId: self.myId, classId: self.classId, topic: self.selectedTopic, skip: self.skip , limit: self.limit)
         apollo.fetch(query: query) { result, error in
            if result?.data?.postsByTopic != nil {
                self.isFilterDone = true
                self.filteredPosts = (result?.data?.postsByTopic)! as! [PostsByTopicQuery.Data.PostsByTopic]
                self.mytableView.reloadData()
              
            }else{
                
            }
        }
    }
    
    @objc func filterByUser(){
        
        self.filteredPosts = []
        self.skip = 0
        let  selecteduserId = self.selectedTeacherId

        let query = PostsByClassUserIdQuery(userId: self.myId, classId: self.classId, classUserId: selecteduserId, skip: self.skip, limit: self.limit)
          apollo.fetch(query: query) { result, error in
            if result?.data?.postsByClassUserId != nil {
                self.isFilterDone = true
                self.filteredByUsersPosts = (result?.data?.postsByClassUserId)! as! [PostsByClassUserIdQuery.Data.PostsByClassUserId]
                self.mytableView.reloadData()
               
            }else{
                
            }
        }
    }
    
    @objc func exitFilter(){
        self.navigationController?.navigationBar.topItem?.rightBarButtonItem = nil
        self.reloadPosts()
    }
    // ---------------------------------------- FILTER BY PICKERVIEW
    
    @objc func showPickerTeacher(){
    
        self.pickerTitle.text = "Багш сонгох"
        if self.isFilterDone == true{
            self.reloadPosts()
        }else{
            if self.isPickerShow == false {
                if self.AllusersList.count > 0 {
                    self.selectedTeacherId = self.AllusersList[0].userId?.id ?? ""
                }
                self.isPickerShow = true
                self.isfilterByNameBtnClicked = true
                self.pickerView.reloadAllComponents()
                UIView.transition(with: self.subview, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    self.subview.frame = CGRect(x:0, y: self.view.frame.height-250,width: self.view.bounds.width, height:250)
                })

            }else{
                self.isfilterByNameBtnClicked = false
                self.dismissPicker()
            }
        }
        
    }
    
    @objc func showPickerTopic(){
   
        self.isfilterByNameBtnClicked = false
        self.pickerTitle.text  = "Хичээл сонгох"
        
        if isFilterDone == true {
            self.reloadPosts()
        }else if self.isPickerShow == false {
            if self.allTopics.count > 0 {
                self.selectedTopicId = self.allTopics[0].id ?? ""
            }
            self.isPickerShow = true
            self.pickerView.reloadAllComponents()
            UIView.transition(with: self.subview, duration: 0.3, options: .transitionCrossDissolve, animations: {
                self.subview.frame = CGRect(x:0, y: self.view.frame.height-250,width: self.view.bounds.width, height:250)
            })
        }else{
            self.dismissPicker()
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if self.isfilterByNameBtnClicked == true {
            return self.AllusersList.count
        }else{
            return self.allTopics.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if self.isfilterByNameBtnClicked == true {
            return self.AllusersList[row].userId?.nickname ?? ""
        }else {
            return self.allTopics[row].name ?? ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if self.isfilterByNameBtnClicked == true {
            self.selectedTeacherId = (self.AllusersList[row].userId?.id)!
        }else{
            self.selectedTopicId = (self.allTopics[row].id)!
        }
    }
    
    @objc func dismissPicker(){
        if self.isPickerShow == true {
            UIView.transition(with: self.subview, duration: 0.3, options: .transitionCrossDissolve, animations: {
                self.isPickerShow = false
                self.subview.frame = CGRect(x:0, y:self.view.bounds.height,width: self.view.bounds.width, height:250)
            })
        }
    }
    
    func getUsersList(){
        checkInternet()
        if SettingsViewController.online {
            let query = ClassUsersListQuery(classId: self.classId)
            KVNProgress.show(withStatus: "", on: self.view)
            self.apollo.fetch(query: query) { result, error in
                KVNProgress.dismiss()
                if result?.data?.classesUserListClassId != nil {
                    self.AllusersList = (result?.data?.classesUserListClassId)! as! [ClassUsersListQuery.Data.ClassesUserListClassId]
                }else{
                
                }
            }
        }else{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу")
        }
    }
    
    func getTopicList(){
        checkInternet()
        if SettingsViewController.online {
            let query = ClassTopicsQuery(classId: self.classId)
            KVNProgress.show(withStatus: "", on: self.view)
            self.apollo.fetch(query: query) { result, error in
                KVNProgress.dismiss()
                if result?.data?.classTopics?.result != nil{
                    self.allTopics = result?.data?.classTopics?.result as! [ClassTopicsQuery.Data.ClassTopic.Result]
                }else{
                    KVNProgress.showError(withStatus: result?.data?.classTopics?.message)
                }
            }
        }else{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу")
        }
    }
    
    @objc func FilterBtnClicked(){
        self.dismissPicker()
        if isfilterByNameBtnClicked == true {
            self.filterByUser()
        }else{
            self.filterByTopic()
        }
    }
}


