//
//  ClassRequestVC.swift
//  Mazzi
//
//  Created by Bayara on 11/22/18.
//  Copyright © 2018 Mazzi Application. All rights reserved.
//

import Foundation
import UIKit
import KVNProgress
import Apollo

class ClassRequestVC:UIViewController{
    
      @IBOutlet weak var label: UILabel!
      @IBOutlet weak var textfield: UITextField!
      @IBOutlet weak var sendbtn: UIButton!
      @IBOutlet weak var errorLabel: UILabel!
    //      var apollo = SettingsViewController.apollo
    
    let apollo: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        // Add additional headers as needed
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
//        print("TOKEN APOLLOO:: ", GlobalVariables.headerToken)
        let url = URL(string:SettingsViewController.graphQL)!
        
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
      override func viewDidLoad() {
            super.viewDidLoad()
            self.customize()
        }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.textfield.becomeFirstResponder()
    }


    func customize(){
        self.title = "Анги руу орох хүсэлт илгээх"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(backtoRoot))

        let bottomLine4 = CALayer()
        bottomLine4.frame = CGRect(x:0.0, y:self.textfield.frame.height - 1, width:self.textfield.frame.width, height: 1.0)
        bottomLine4.backgroundColor = GlobalVariables.todpurple.cgColor
        self.textfield.borderStyle = UITextField.BorderStyle.none
        self.textfield.layer.addSublayer(bottomLine4)
        
        
        self.sendbtn.backgroundColor = GlobalVariables.todpurple
        self.sendbtn.setTitleColor(GlobalVariables.TextTod, for: UIControl.State.normal)
        self.textfield.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))

        self.sendbtn.layer.cornerRadius = 25
        self.sendbtn.layer.masksToBounds = true
        self.sendbtn.backgroundColor = GlobalVariables.purple
        self.sendbtn.titleLabel?.textColor = UIColor.white
        
        // BUTTON GRADIENT
//        if let btn = self.sendbtn {
//            let gradient = CAGradientLayer()
//            var bounds = self.sendbtn.bounds
//            bounds.size.height = 50
//            gradient.frame = bounds
//            gradient.colors = [ GlobalVariables.newPurple.cgColor,GlobalVariables.gradiendBlack.cgColor]
//            gradient.startPoint = CGPoint(x: 0, y: 0)
//            gradient.endPoint = CGPoint(x: 0, y: 1)
//            if let image = getImageFrom(gradientLayer: gradient) {
////                navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
//               btn.setBackgroundImage(image, for: UIControl.State.normal)
//            }
//        }

        
//    }
//
//    func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
//        var gradientImage:UIImage?
//        UIGraphicsBeginImageContext(gradientLayer.frame.size)
//        if let context = UIGraphicsGetCurrentContext() {
//            gradientLayer.render(in: context)
//            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
//        }
//        UIGraphicsEndImageContext()
//        return gradientImage

    }
    
   @IBAction func sendReq(_ sender: Any) {
        checkInternet()
        if SettingsViewController.online {
            if self.textfield.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")) == ""{
                self.textfield.placeholder = "Ангийн код оруулна уу"
            }else{
                //            print("==========================================")
                //            print("myID:: ", SettingsViewController.userId)
                //            print("CLASS CODE:: ", self.textfield.text!)
                let mutation = RequestClassMutation(userId: SettingsViewController.userId, classCode: self.textfield.text!)
                self.apollo.perform(mutation: mutation) { result, error in
//                    print("res:", result?.data as Any)
                    if  (result?.data?.requestClassRoomList?.error == false) {
                        KVNProgress.showSuccess(withStatus: "Хүсэлт амжилттай илгээгдлээ.")
                        self.backtoRoot()
                    }else {
                        KVNProgress.showError(withStatus: result?.data?.requestClassRoomList?.message)
                    }
                }
            }
        }else{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу!")
        }
    }
    @objc func textFieldDidChange(textField:UITextField){
        self.textfield.textColor = UIColor.black
    }
    
    @objc func backtoRoot(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissKeyboard(){
        self.textfield.resignFirstResponder()
    }
}
