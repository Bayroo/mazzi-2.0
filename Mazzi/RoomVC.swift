//
//  RoomVC.swift
//  Mazzi
//
//  Created by Bayara on 9/27/18.
//  Copyright © 2018 Mazzi App LLC. All rights reserved.
//

import Foundation
import SwiftyJSON
import SocketIO
import KVNProgress
import Dispatch
import Apollo

class RoomVC:UIViewController,UITableViewDelegate,UITableViewDataSource ,UITextViewDelegate,UIWebViewDelegate, UICollectionViewDelegate , UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var spiner: UIActivityIndicatorView!
    var roomName = ""
    var token = ""
    var classId = ""
    var numb = 0
    var questions = [QuestionQuery.Data.StudentTakeQuestion]()
    var options = [QuestionQuery.Data.StudentTakeQuestion.Option]()
    var answers = [AnswerInputType]()
    var selectedIndex = -1
    var teacherId = ""
    var seminarId = ""
    var launchID = ""
    var questionId = ""
    var dic = [String : Any]()
    var intAnswer = [String]()
    var multiple = [Int]()
    var letters = [String]()
    var cdiotoken = GlobalVariables.headerToken
    var strAnswer = "Хариултаа энд бичнэ үү ..."
    var isShortAnswer = false
    var isImgAnswer = false
    var isFormula = false
//    var cdiosocket  = SettingsViewController.cdiosocket
    var apollo = SettingsViewController.apollo
    var tapGesture:UITapGestureRecognizer!
    var isMultiChoice = true
    var oneTry = false
    var totaltime = Double()
    var timer = Timer()
    var isStartedTimer = false
    var seconds = 60
    var msg = ""
    var qNumbers = [Int]()
    var answeredQuestionIndex = [Int]()
    
    //=========================================
    var previewView = UIView()
    var mycollectionView : UICollectionView!
    var isShowPreview = false
    //=========================================
    
    static var cdiomanagers  = SocketManager(socketURL: URL(string: SettingsViewController.cdiosocketUrl)!, config: [ .compress,.connectParams(["userId" : GlobalVariables.user_id, "token" : GlobalVariables.headerToken ]) ,.secure(false) ,.forceNew(true),.log(false) ,.forceWebsockets(true),.path("/socket/socket.io")])
     var cdiosocket = cdiomanagers.defaultSocket
    
    @IBOutlet weak var formulaDescLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    var timeLabel = UILabel()
    @IBOutlet weak var MyImgView: UIImageView!
    @IBOutlet weak var questionLblWithImg: UILabel!
    @IBOutlet weak var leftBtn: UIButton!
    @IBOutlet weak var pointLabel: UILabel!
    @IBOutlet weak var hintLabel: UILabel!
    @IBOutlet weak var rightBtn: UIButton!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var mytableview: UITableView!
    @IBOutlet weak var mywebview: UIWebView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var stringAnswerText: UITextView!
    @IBOutlet var questionLabel: UILabel!
    @IBOutlet weak var line1: UIView!
    @IBOutlet weak var line2: UIView!
    @IBOutlet weak var myScrollview: UIScrollView!
    var previewHeight = 0
    var navHegiht = 44
    var shortAnswerTextView = UITextView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customize()
        self.initUI()
        self.customizeView()
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)
        self.classId  = GlobalStaticVar.classId
       
        RoomVC.cdiomanagers  = SocketManager(socketURL: URL(string: SettingsViewController.cdiosocketUrl)!, config: [ .compress,.connectParams(["userId" : GlobalVariables.user_id, "token":GlobalVariables.headerToken]) ,.secure(false) ,.forceNew(true),.log(false) ,.forceWebsockets(true),.path("/socket/socket.io")])
        cdiosocket = RoomVC.cdiomanagers.defaultSocket
        
        self.classId = GlobalStaticVar.classId
        if self.cdiosocket.status != .connected {
            self.establishSocketConnections()
        }
        self.receiveNotify()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.timeLabel.isHidden = true
        _ = self.apollo.clearCache()
        self.cdiosocket.disconnect()
        self.cdiosocket.removeAllHandlers()
    }
    
    func receiveNotify(){
         NotificationCenter.default.addObserver(self, selector: #selector(self.establishSocketConnections), name: Notification.Name("cdiosocketIsConnected"), object: nil)
    }
    
    func customizeView(){
        self.previewView.frame = CGRect(x: 0, y: self.view.bounds.height, width: self.view.bounds.width, height: self.view.bounds.height - 44)
        self.previewView.backgroundColor = GlobalVariables.F5F5F5
        
        let redLabel = UILabel(frame: CGRect(x: 8, y: 4, width: self.previewView.frame.width - 16, height: 20))
        redLabel.textColor = UIColor.red
        redLabel.font = GlobalVariables.customFont14
        redLabel.text = " - Хариулаагүй асуултууд (улаан)"
        
        let blackLabel = UILabel(frame: CGRect(x: 8, y: 28, width: self.previewView.frame.width - 16, height: 20))
        blackLabel.textColor = UIColor.black
        blackLabel.font = GlobalVariables.customFont14
        blackLabel.text = " - Хариулсан асуултууд (хар)"
        
        let description = UILabel(frame: CGRect(x: 8, y: 52, width: self.previewView.frame.width - 16, height: 20))
        description.textColor = UIColor.black
        description.font = GlobalVariables.customFont14
        description.text = " * Асуултын дугаар дээр дарж асуултаа харна."
        
        self.previewView.addSubview(redLabel)
        self.previewView.addSubview(blackLabel)
        self.previewView.addSubview(description)
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //.horizontal
        layout.sectionInset = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
        layout.minimumLineSpacing = 8.0
        let Colframe = CGRect(x: 0, y: 100, width: self.view.bounds.width, height: self.previewView.bounds.height)
        self.mycollectionView = UICollectionView(frame: Colframe, collectionViewLayout: layout)
        self.mycollectionView.register(UINib(nibName: "launchPreview", bundle: nil), forCellWithReuseIdentifier: "previewLaunch")
        self.mycollectionView.register(UINib(nibName: "headerView", bundle: nil), forCellWithReuseIdentifier: "header")
        self.mycollectionView.delegate = self
        self.mycollectionView.dataSource = self
        self.mycollectionView.backgroundColor = UIColor.white
        self.view.addSubview(self.previewView)
        self.previewView.addSubview(self.mycollectionView)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "header", for: indexPath) as! headerView
        header.layoutIfNeeded()
        header.frame = CGRect(x: 0, y: 0, width: 100, height: 50)
        header.sectionTitle.text = "section 1 "
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.mycollectionView.bounds.size.width - 40) / 7
        return CGSize(width: width,height: width)
    }
    
    @objc func toPreview(){
        if self.isShowPreview == false{
            if self.isShortAnswer == true {
                self.strAnswerPut(self.shortAnswerTextView)
            }
            UIView.animate(withDuration: 0.3) {
                  var ySpace : CGFloat!
                if UIDevice().userInterfaceIdiom == .phone {
                    switch UIScreen.main.nativeBounds.height {
                    case 1136:
                        print("iPhone 5 or 5S or 5C")
                        ySpace = 64.0
                    case 1334:
                        print("iPhone 6/6S/7/8")
                         ySpace = 64.0
                    case 2208:
                        print("iPhone 6+/6S+/7+/8+")
                         ySpace = 64.0
                    case 2436:
                        print("iPhone X")
                         ySpace = 84.0
                    default:
                        print("unknown")
                    }
                }
                
//                print("width::", UIScreen.main.bounds.width)
                self.previewView.frame = CGRect(x: 0.0, y: ySpace, width: self.view.bounds.width, height: self.view.bounds.height)
                self.mycollectionView.reloadData()
                self.isShowPreview = true
            }
        }else{
            UIView.animate(withDuration: 0.3) {
                self.previewView.frame = CGRect(x: 0, y: self.view.bounds.height, width: self.view.bounds.width, height: self.view.bounds.height - 44)
                self.mycollectionView.reloadData()
                self.isShowPreview = false
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.questions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "previewLaunch", for: indexPath) as! launchPreviewCell
        cell.mylabel.text = String(indexPath.row + 1)
//        print("UNANSWERED:: ", self.answeredQuestionIndex)
        if !self.answeredQuestionIndex.contains(indexPath.row){
            cell.mylabel.textColor = UIColor.red
        }else{
            cell.mylabel.textColor = UIColor.black
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.3) {
//            print("selectedNumb:: ", indexPath.row)
            self.numb = indexPath.row
            
            if self.questions[self.numb].questionType == "5bb463809bebff2ce482238b" {
                self.isShortAnswer = true
            }else{
                self.isShortAnswer = false
            }
            
            if self.questions[self.numb].optionType == "onlyImageOption" {
                self.isImgAnswer = true
            }else{
                self.isImgAnswer = false
            }
            
            if self.questions[self.numb].optionType == "onlyFormulaOption"{
                self.isFormula = true
            }else{
                self.isFormula = false
            }
            
            self.showAnswer()
            self.previewView.frame = CGRect(x: 0, y: self.view.bounds.height, width: self.view.bounds.width, height: self.view.bounds.height - 44)
            self.options = self.questions[self.numb].options! as! [QuestionQuery.Data.StudentTakeQuestion.Option]
            print("collectionView didselect")
            self.mytableview.reloadData()
        }
    }

    func customize(){
        
        self.letters = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T", "U","V","W","X","Y","Z"]
        self.nextBtn.setTitle("Үргэлжлүүлэх", for: UIControl.State.normal)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(baktoroot))
        self.navigationController?.navigationBar.tintColor = UIColor.white
//        print("NAV HEIGHT:: ", self.navigationController?.navigationBar.frame.height)
        self.timeLabel.frame = CGRect(x:self.view.bounds.size.width/2-125 , y:10 , width:250 , height:20)
        timeLabel.textAlignment = NSTextAlignment.center
        timeLabel.textColor = UIColor.white
        timeLabel.font = GlobalVariables.customFont16
        self.navigationController?.navigationBar.addSubview(self.timeLabel)
    }
    
    @objc func establishSocketConnections() {
        
        self.cdiosocket.on("connect") { ( dataArray, ack) -> Void in
            self.cdiosocket.emit("student_join_room",["key":self.roomName , "classId":self.classId])
            self.launchRoom()
            print("CDIO SOCKET CONNECTED")
        }
        getFireUrl(url: "my") { (suc) in
            if suc == true {
                print("url changed successfully")
                self.cdiosocket.setReconnecting(reason: "not connected")
            }
        }
        
        self.cdiosocket.on("disable_launch") { ( dataArray, ack) -> Void in
            print("disable_launch")
            KVNProgress.showSuccess(withStatus: "Шалгалтын цаг дууслаа!")
            self.navigationController?.popViewController(animated: true)
        }
        
        self.cdiosocket.on("disconnect") { (data, SocketAckEmitter) in
            print(" CDIO SOCKET DISCONECTED")
        }
        
        self.cdiosocket.on("statusChange"){(data,ack) in
            print("CDIO SOCKET STATUSCHANGE: ", data)
        }
        self.cdiosocket.connect()
    }
    
    func runTimer(){
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer(){
        seconds -= 1
        var ti = NSInteger(self.totaltime)
        ti -= 1000
        self.totaltime = Double(ti)
        if self.totaltime < 60000.0 && self.totaltime > 59000.0  {
            KVNProgress.showError(withStatus: "Цаг дуусахад 1 минут үлдлээ" ,on:self.view)
        }else if self.totaltime < 500 {
            self.stopTimerTest()
            self.DirectsendAnswer()
        }
        self.timeLabel.text = self.timeString(time: TimeInterval(ti))
    }
    
    func stopTimerTest() {
        self.isStartedTimer = false
        self.timer.invalidate()
    }
    
    func timeString(time: TimeInterval)->String {
        let hours = Int(time) / 3600000
        let minutes = Int(time) % 3600000 / 60000
        let seconds = Int(time) % 60000 / 1000
        return String(format:"%02i : %02i : %02i",hours,minutes, seconds)
    }
    
    func launchRoom(){
//        print("launchRoom")
        if self.token == "" || self.token == " " || self.token.count == 0 {
            self.navigationItem.rightBarButtonItem = nil
            self.spiner.isHidden = false
            self.label.isHidden = false
            self.spiner.startAnimating()
            self.cdiosocket.on("launch_room"){data, ack in
                  self.spiner.isHidden = true
                  self.label.isHidden = true
                  self.spiner.stopAnimating()
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "reload-1"), style: .plain, target: self, action: #selector(self.toPreview))
                let res = JSON(data)
                self.token = res[0]["token"].stringValue
                let query = QuestionQuery(token: self.token, classId: self.classId)
                KVNProgress.show()
                self.apollo.fetch(query: query) { result, error in
                    KVNProgress.dismiss()
                    if result?.data?.studentTakeQuestion == nil || result?.data?.studentTakeQuestion?.count == 0{
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        
                        self.questions = (result?.data?.studentTakeQuestion)! as! [QuestionQuery.Data.StudentTakeQuestion]
                        if  self.questions.count == 0{
                            let alert = UIAlertController(title: "", message: "Асуулт хоосон байна. ", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction) -> Void in
                                self.navigationController?.popViewController(animated: true)
                            }))
                            self.navigationController!.present(alert, animated: true, completion: nil)
                        }else{
                            self.initAnswers()
                            if self.isStartedTimer == false{
                                if self.totaltime != 0{
                                    self.runTimer()
                                    self.isStartedTimer = true
                                }else{
                                    self.timeLabel.isHidden = true
                                }
                            }
                            if self.answers.isEmpty {
                                let gg = AnswerInputType.init(studentId: SettingsViewController.userId, questionId:self.questions[self.numb].id, launchId: self.launchID, intAnswer: self.intAnswer,  stringAnswer:" " )
                                self.answers.append(gg)
                            }
                           
                            if self.questions[self.numb].questionType == "5bb463809bebff2ce482238b" {
                                self.isShortAnswer = true
                            }else{
                                self.isShortAnswer = false
                            }
                            
                            if self.questions[self.numb].optionType == "onlyImageOption"{
                                self.isImgAnswer = true
                            }else{
                                 self.isImgAnswer = false
                            }
                        
                            if self.questions[self.numb].optionType == "onlyFormulaOption"{
                                self.isFormula = true
                            }else{
                                self.isFormula = false
                            }
                            self.initGesture()
                            self.options = self.questions[self.numb].options! as! [QuestionQuery.Data.StudentTakeQuestion.Option]
//                            print("launchroom reload")
                            self.mytableview.reloadData()
                        }
                    }
                }
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
             return 1
        }else{
            if self.isShortAnswer == true {
                 return 1
            }else{
                 return  self.options.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
                 return UITableView.automaticDimension
        }else{
            if self.isShortAnswer == true{
                return 100
            }else{
                return UITableView.automaticDimension
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("tableview didselect")
        if indexPath.section == 0{
            
        }else if indexPath.section == 1 && self.isShortAnswer == false  {
            if !self.answeredQuestionIndex.contains(self.numb){
                self.answeredQuestionIndex.append(self.numb)
            }
            if self.questions[self.numb].answerLength! > 1 {
                if intAnswer.contains(options[indexPath.row].id!){
                    let index = intAnswer.index(where:{$0 == options[indexPath.row].id!})
                    intAnswer.remove(at: index!)
                    self.mytableview.reloadData()
                }else{
                    if self.intAnswer.count < self.questions[self.numb].answerLength! {
                        intAnswer.append(options[indexPath.row].id!)
                        self.mytableview.reloadData()
                    }
                }
            }else{
                intAnswer = [(options[indexPath.row].id!)] // [(String(indexPath.row))]
                self.mytableview.reloadData()
            }
            let gg = AnswerInputType.init(studentId: SettingsViewController.userId, questionId:self.questions[self.numb].id, launchId: self.launchID, intAnswer: intAnswer,  stringAnswer:"" )
            if self.answers.count > 0 {
                if self.answers.contains(where: {$0.questionId == gg.questionId!}){
                    let index =  self.answers.index(where: {$0.questionId == gg.questionId!})
                    self.answers.remove(at: index!)
                    self.answers.append(gg)
                    self.mytableview.reloadData()
                }else{
                    self.answers.append(gg)
                    self.mytableview.reloadData()
                }
            }else{
                self.answers.append(gg)
                self.mytableview.reloadData()
            }
        }
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.numb == self.questions.count - 1 {
            self.nextBtn.setTitle("Илгээх", for: UIControl.State.normal)
        }else{
            self.nextBtn.setTitle("Үргэлжлүүлэх", for: UIControl.State.normal)
        }
        if indexPath.section == 0 {
             let cell = tableView.dequeueReusableCell(withIdentifier: "questioncell", for: indexPath) as! questionCell
            cell.initcell()
            
            if self.oneTry {
                cell.leftBtn.isHidden = true
            }else{
                cell.leftBtn.isHidden = false
            }
            if self.numb == 0 {
                cell.leftBtn.isHidden = true
            }
            if self.numb == self.questions.count - 1 {
                cell.rightBtn.isHidden = true
            }else{
                cell.rightBtn.isHidden = false
            }
            if self.questions[self.numb].explanation != "" {
                cell.hintLabel.isHidden = false
                cell.hintLabel.text = "ТАЙЛБАР: \( self.questions[self.numb].explanation ?? "")"
            }else{
                cell.hintLabel.text = ""
                cell.hintLabel.isHidden = true
            }
            
            cell.countLabel.text = "Асуулт \(self.numb + 1)\("/")\(self.questions.count)"
            cell.pointLabel.text = "Оноо: \(self.questions[self.numb].weight ?? 0)"
            cell.leftBtn.addTarget(self, action: #selector(prevQuestion), for: .touchUpInside)
            cell.rightBtn.addTarget(self, action: #selector(nextQuestion), for: .touchUpInside)
            if !(self.questions[self.numb].image?.isEmpty)!{
                cell.myWebView.isHidden = true
            }else{
                cell.myWebView.isHidden = false
                cell.myWebView.loadHTMLString(self.questions[self.numb].content!, baseURL: nil)
            } 
            
            return cell
        }else{
            print(self.questions[self.numb].optionType ?? "")
            if self.isImgAnswer == true{
                let cell = tableView.dequeueReusableCell(withIdentifier: "imgCell", for: indexPath) as! imageAnswerCell
                cell.initcellData()
                cell.btn.setTitle(self.letters[indexPath.row], for: UIControl.State.normal)
             
                if self.intAnswer.contains(options[indexPath.row].id!){
                    cell.accessoryType = .checkmark
                    cell.btn.layer.backgroundColor = GlobalVariables.purple.cgColor
                    cell.btn.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    cell.backview.backgroundColor = GlobalVariables.purple
                    cell.tintColor = GlobalVariables.purple
                }else{
                    cell.accessoryType = .none
                    cell.btn.layer.backgroundColor = UIColor.clear.cgColor
                    cell.btn.setTitleColor(GlobalVariables.purple, for: UIControl.State.normal)
                    cell.backview.backgroundColor = GlobalVariables.F5F5F5
                }
               
              if !(self.options[indexPath.row].image?.isEmpty)! {
                    cell.imgview.isHidden = false
                    let img = self.options[indexPath.row].image!
                    cell.imgview.sd_setImage(with: URL(string: "\(SettingsViewController.cdioFileUrl)\(img)"), placeholderImage: UIImage(named: "loading2"))
                }
                return cell
            }else if self.isFormula == true {
                  let cell = tableView.dequeueReusableCell(withIdentifier: "formula", for: indexPath) as! formulaCell
                    cell.initcellData()
                    cell.btn.setTitle(self.letters[indexPath.row], for: UIControl.State.normal)
                
                if self.intAnswer.contains(options[indexPath.row].id!){
                    cell.accessoryType = .checkmark
                    cell.btn.layer.backgroundColor = GlobalVariables.purple.cgColor
                    cell.btn.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    cell.backview.backgroundColor = GlobalVariables.purple
                    cell.tintColor = GlobalVariables.purple
                }else{
                    cell.accessoryType = .none
                    cell.btn.layer.backgroundColor = UIColor.clear.cgColor
                    cell.btn.setTitleColor(GlobalVariables.purple, for: UIControl.State.normal)
                    cell.backview.backgroundColor = GlobalVariables.F5F5F5
                }
                
                    if !(self.options[indexPath.row].formula?.isEmpty)! {
                        cell.webview.loadHTMLString(self.options[indexPath.row].formula!, baseURL: nil)
                    }
                  return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "AnswersCell", for: indexPath) as! AnswersCell
                cell.initcellData()
                cell.txtView.delegate = self
                if self.isShortAnswer == true {
                    self.mytableview.contentSize.height = 650
                    self.shortAnswerTextView = cell.txtView
                    cell.txtView.resignFirstResponder()
                    cell.txtView.isHidden = false
                    cell.label.isHidden = true
                    if self.strAnswer != "" {
                        cell.txtView.text = self.strAnswer
                        self.strAnswer = ""
                    }else{
                        cell.txtView.text = "Хариултаа энд бичнэ үү ..."
                    }
                    cell.txtView.textColor = UIColor.lightGray
                    cell.accessoryType = .none
                    cell.backview.isHidden = true
                    cell.btn.isHidden = true
                }else{
                    cell.label.isHidden = false
                    cell.btn.isHidden = false
                    cell.backview.isHidden = false
                    cell.txtView.isHidden = true
                    cell.initcellData()
                    cell.label.text = options[indexPath.row].content!
                    
                    cell.btn.setTitle(self.letters[indexPath.row], for: UIControl.State.normal)
                    if self.intAnswer.contains(options[indexPath.row].id!){
                        cell.accessoryType = .checkmark
                        cell.btn.layer.backgroundColor = GlobalVariables.purple.cgColor
                        cell.btn.setTitleColor(UIColor.white, for: UIControl.State.normal)
                        cell.backview.backgroundColor = GlobalVariables.purple
                        cell.label.textColor = UIColor.white
                        cell.tintColor = GlobalVariables.purple
                    }else{
                        cell.accessoryType = .none
                        cell.btn.layer.backgroundColor = UIColor.clear.cgColor
                        cell.btn.setTitleColor(GlobalVariables.purple, for: UIControl.State.normal)
                        cell.backview.backgroundColor = GlobalVariables.F5F5F5
                        cell.label.textColor = GlobalVariables.purple
                    }
                }
                return cell
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
             self.strAnswerPut(textView)
             self.mytableview.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            return false
        }
        return true
    }
    
    @objc func baktoroot(){
        let alert = UIAlertController(title: "", message: "Та асуулгаас гарахдаа итгэлтэй байна уу? ", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Тийм", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction) -> Void in
            self.navigationController?.popViewController(animated: true)
            KVNProgress.dismiss()
        }))
        alert.addAction(UIAlertAction(title: "Үгүй", style: UIAlertAction.Style.cancel, handler: nil))
        self.navigationController!.present(alert, animated: true, completion: nil)
    }
    
    func initUI(){
        self.nextBtn.backgroundColor = GlobalVariables.purple
        self.nextBtn.setTitleColor(GlobalVariables.TextTod, for: UIControl.State.normal)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black

        }
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                self.mytableview.setContentOffset(CGPoint(x: 0, y: 200), animated: true)
            case 1334:
                print("iPhone 6/6S/7/8")
                self.mytableview.setContentOffset(CGPoint(x: 0, y: 60), animated: true)
            case 2208:
                print("iPhone 6+/6S+/7+/8+")
                self.mytableview.setContentOffset(CGPoint(x: 0, y: 60), animated: true)
            case 2436:
                print("iPhone X")
                self.mytableview.setContentOffset(CGPoint(x: 0, y: 60), animated: true)
            default:
                print("unknown")
            }
        }
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        self.strAnswerPut(textView)
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.strAnswerPut(textView)
    }
    
    func textViewShouldReturn(_ textView: UITextView) -> Bool {
        
        self.strAnswerPut(textView)
        return true
    }
    
    func strAnswerPut(_ textView: UITextView){
//        print("strAnswerPut: ",textView.text)
        textView.resignFirstResponder()
         self.mytableview.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        
        if textView.text == "" || textView.text == " " {
    
            textView.text = "Хариултаа энд бичнэ үү ..."
            textView.textColor = UIColor.lightGray
            self.strAnswer = ""
            if self.answeredQuestionIndex.count > 0{
                for i in 0..<self.answeredQuestionIndex.count{
                    if self.answeredQuestionIndex[i] == self.numb{
                        self.answeredQuestionIndex.remove(at: i)
                         self.strAnswer = ""
                        break
                    }
                }
            }
        }else{
       
            if textView.text != "Хариултаа энд бичнэ үү ..." {
                 self.strAnswer = textView.text!
                if self.strAnswer != "" ||  self.strAnswer != "Хариултаа энд бичнэ үү ..." {
                    if !self.answeredQuestionIndex.contains(self.numb){
                        self.answeredQuestionIndex.append(self.numb)
                        self.strAnswer = ""
//                        self.shortAnswerTextView.text = ""
                    }
                }
            }
        
            let gg = AnswerInputType.init(studentId: SettingsViewController.userId, questionId:self.questions[self.numb].id, launchId: self.launchID, intAnswer: intAnswer,  stringAnswer:  self.strAnswer )
        
            if self.answers.contains(where: {$0.questionId == gg.questionId!}){
                let index = self.answers.index(where: {$0.questionId == gg.questionId!})
                self.answers.remove(at: index!)
                self.answers.append(gg)
            }else{
                self.answers.append(gg)
            }
             self.strAnswer = ""
        }
    }
    
    func initGesture(){
        self.mytableview.delegate = self
        self.mytableview.dataSource = self
        self.mytableview.rowHeight = UITableView.automaticDimension
//        self.mytableview.estimatedRowHeight = 44
        self.mytableview.register((UINib(nibName: "AnswersCell", bundle: nil)), forCellReuseIdentifier: "AnswersCell")
        self.mytableview.register((UINib(nibName: "imageAnsverCell", bundle: nil)), forCellReuseIdentifier: "imgCell")
        self.mytableview.register((UINib(nibName: "questionCell", bundle: nil)), forCellReuseIdentifier: "questioncell")
        self.mytableview.register((UINib(nibName: "formulaCell", bundle: nil)), forCellReuseIdentifier: "formula")
        if !oneTry {
            let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
            swipeRight.direction = UISwipeGestureRecognizer.Direction.right
            self.view.addGestureRecognizer(swipeRight)
        }
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                self.prevQuestion()
            case UISwipeGestureRecognizer.Direction.left:
                self.nextQuestion()
            default:
                break
            }
        }
    }
    
    func initAnswers(){
//        print("INITANSWERS")
        if self.answers.count > 0{
            self.answers.removeAll()
        }
        for i in 0..<self.questions.count {
            if self.questions[i].answerLength! == 1 {
                self.intAnswer = []
            } else {
                if self.intAnswer.count <= self.questions[i].answerLength! {
                    self.intAnswer = []
                }
            }
            let gg = AnswerInputType.init(studentId: SettingsViewController.userId, questionId:self.questions[i].id, launchId: self.launchID, intAnswer: self.intAnswer,  stringAnswer:"" )
            
            if self.answers.count > 0 {
                if self.answers.contains(where: {$0.questionId == gg.questionId!}){
                    let index =  self.answers.index(where: {$0.questionId == gg.questionId!})
                    self.answers.remove(at: index!)
                    self.answers.append(gg)
                    self.mytableview.reloadData()
                }else{
                    self.answers.append(gg)
                    self.mytableview.reloadData()
                }
            }else{
                self.answers.append(gg)
                self.mytableview.reloadData()
            }
            self.intAnswer.removeAll()
        }
    }
    
    func showAnswer (){
//        print("show ANswer")
        if self.answers.count > 0 {
            if self.answers.contains(where: {$0.questionId == self.questions[numb].id!}){
                let index =  self.answers.index(where: {$0.questionId == self.questions[numb].id!})
                let g = self.answers[index!].intAnswer!
                let o = g!
                if self.questions[self.numb].answerLength! > 1 {
                    if o.isEmpty{
                        self.intAnswer = []
                    }else{
                        for i in 0..<o.count {
                            self.intAnswer.append(o[i]!)
                        }
                    }
                }else{
                    
                    if o.isEmpty {
                        self.intAnswer.removeAll()
                        self.intAnswer = []
                    }else{
                        self.intAnswer = [String(o[0]!)]
                    }
                }
                self.strAnswer = self.answers[index!].stringAnswer!!
                if self.strAnswer == "Хариултаа энд бичнэ үү ..."{
                    self.strAnswer = ""
                }
                 self.mytableview.reloadData()
            }else{
                self.intAnswer.removeAll()
                self.intAnswer = []
                self.strAnswer = ""
                self.mytableview.reloadData()
            }
        }else{
            self.intAnswer = []
            self.strAnswer = ""
        }
    }
    
   @objc func nextQuestion(){
    print("NEXT")
        if self.questions.count - 1 > self.numb {
            if self.isShortAnswer == true {
                self.strAnswerPut(self.shortAnswerTextView)
            }
            self.numb += 1
            self.options = self.questions[self.numb].options! as! [QuestionQuery.Data.StudentTakeQuestion.Option]
           
            if self.questions[self.numb].questionType == "5bb463809bebff2ce482238b" {
                self.isShortAnswer = true
            }else{
                self.isShortAnswer = false
            }
            
            if self.questions[self.numb].optionType == "onlyImageOption"{
                self.isImgAnswer = true
            }else{
                self.isImgAnswer = false
            }
            
            if self.questions[self.numb].optionType == "onlyFormulaOption"{
                self.isFormula = true
            }else{
                self.isFormula = false
            }
            
            if self.numb == self.questions.count - 1 {
                self.nextBtn.setTitle("Илгээх", for: UIControl.State.normal)
            }else{
                self.nextBtn.setTitle("Үргэлжлүүлэх", for: UIControl.State.normal)
            }
            self.showAnswer()
            self.mytableview.reloadData()
        }
        
    }
    
    @objc func prevQuestion(){
        print("PREV")
        if self.numb > 0 {
            if self.isShortAnswer == true {
                 self.strAnswerPut(self.shortAnswerTextView)
            }
           
            self.numb -= 1
            self.options = self.questions[self.numb].options! as! [QuestionQuery.Data.StudentTakeQuestion.Option]
            
            self.nextBtn.setTitle("Үргэлжлүүлэх", for: UIControl.State.normal)
            if self.questions[self.numb].questionType == "5bb463809bebff2ce482238b" {
                self.isShortAnswer = true
            }else{
                self.isShortAnswer = false
            }
            if self.questions[self.numb].optionType == "onlyImageOption"{
                self.isImgAnswer = true
            }else{
                self.isImgAnswer = false
            }
            
            if self.questions[self.numb].optionType == "onlyFormulaOption"{
                self.isFormula = true
            }else{
                self.isFormula = false
            }
            
            self.showAnswer()
            self.mytableview.reloadData()
        }
    }
    
    @IBAction func sendAnswer(_ sender: Any) {
        if self.numb < self.questions.count {
            if self.numb + 1  == self.questions.count - 1 {
                self.nextBtn.setTitle("Илгээх", for: UIControl.State.normal)
            }else{
                //self.nextBtn.setTitle("Үргэлжлүүлэх", for: UIControl.State.normal)
            }
            if self.numb + 1  == self.questions.count {
                checkInternet()
                if SettingsViewController.online {
                    
                    let mutation = SendAnswerMutation(teacherId: self.teacherId, seminarId: self.seminarId, answers: self.answers, classId: self.classId)
                   
                    for i in 0..<self.questions.count{
                      
                        if !self.answeredQuestionIndex.contains(i){
                            if !qNumbers.contains(i + 1){
                                qNumbers.append(i + 1)
                            }
                        }else{
                            if qNumbers.count > 0{
                                if qNumbers.contains(i + 1){
                                     let indeks = qNumbers.index(where:{$0 == i + 1})
                                    qNumbers.remove(at: indeks!)
                                }
                            }
                        }
                            if qNumbers.count > 0{
                                 msg = "\("Та") \(qNumbers)\("-р асуултанд хариулахгүй илгээх гэж байна !")"
                            }else{
                                 msg = "Та хариултаа илгээхдээ итгэлтэй байна уу ?"
                            }
                    }
                    
                    let alert = UIAlertController(title: "", message: msg, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Илгээх", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction) -> Void in
                        print("answerCOUNT::", self.answers.count)
                        
                        KVNProgress.show()
                        self.apollo.perform(mutation: mutation) { result, error in
                            if  (result?.data?.studentGiveQuestion?.error == false) {
                                KVNProgress.dismiss()
                                let point = String(format:"%0.2f", (result?.data!.studentGiveQuestion?.result!.average)!)
                                self.cdiosocket.emit("student_signal_teacher",["key":self.roomName,"studentId":SettingsViewController.userId])
                                self.stopTimerTest()
                                let alerts = UIAlertController(title: "", message: "Та \(point)\("%") оноо авсан байна", preferredStyle: UIAlertController.Style.alert)
                                alerts.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (alerts:UIAlertAction) -> Void in
                                    self.navigationController?.popViewController(animated: true)
                                }))
                                KVNProgress.dismiss()
                                self.present(alerts, animated: true, completion: nil)
                            }else{
                                KVNProgress.showError(withStatus: result?.data?.studentGiveQuestion?.message, on: self.view)
                            }
                        }
                    }))
                    alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.cancel, handler:{ (alert:UIAlertAction) -> Void in
                        print("close numb: ", self.numb)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }else{
                    KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу!")
                }
                
            }else{
                self.nextQuestion()
            }
        }else
        {
            print("elseNumb:: ", self.numb)
        }
    }
    
    func DirectsendAnswer(){
        print("DirectsendAnswer")
        if SettingsViewController.online {
//            print("teacherID: ",self.teacherId)
//              print("seminarId: ",self.seminarId)
              print("answers: ",self.answers)
            let mutation = SendAnswerMutation(teacherId: self.teacherId, seminarId: self.seminarId, answers: self.answers, classId: self.classId)
                KVNProgress.show()
                self.apollo.perform(mutation: mutation) { result, error in
//                    print("result:: ",result)
                    if  (result?.data?.studentGiveQuestion?.error == false) {
                        KVNProgress.dismiss()
                        let point = String(format:"%0.2f", (result?.data!.studentGiveQuestion?.result!.average)!)
                        self.cdiosocket.emit("student_signal_teacher",["key":self.roomName,"studentId":SettingsViewController.userId])
                        let alerts = UIAlertController(title: "Таны цаг дууслаа", message: "Таны хариулт илгээгдсэн. Таны дүн: \(point)\("%")", preferredStyle: UIAlertController.Style.alert)
                        alerts.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (alerts:UIAlertAction) -> Void in
                            self.stopTimerTest()
                            self.navigationController?.popViewController(animated: true)
                        }))
                        KVNProgress.dismiss()
                        self.present(alerts, animated: true, completion: nil)
                    }else{
                         KVNProgress.dismiss()
//                        print("ERRORL:: ", result?.data?.studentGiveQuestion?.message)
                        KVNProgress.showError(withStatus: result?.data?.studentGiveQuestion?.message, on: self.view)
                    }
                }

        }else{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу!")
        }
    }
    
}
