//
//  LinkLoadVC.swift
//  Mazzi
//
//  Created by Bayara on 12/22/18.
//  Copyright © 2018 woovoo. All rights reserved.
//

import Foundation
import KVNProgress
import UIKit
import CoreGraphics
import QuartzCore

class LinkLoadVC:UIViewController,UIWebViewDelegate{
    var link = ""
    var fullLink = ""
    @IBOutlet weak var webview : UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(backtoRoot))
        self.webview.delegate = self
        if link != ""{
            self.loadRequest()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func loadRequest(){
        if link.contains("http://") || link.contains("https://"){
            fullLink = link
        }else{
            fullLink = "\("http://")\(link)"
        }
        let url = NSURL (string: fullLink);
        let requestObj = NSURLRequest(url: url! as URL);
        webview.loadRequest(requestObj as URLRequest);
    }
    
    @objc func backtoRoot(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
