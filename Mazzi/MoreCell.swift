//
//  MoreCell.swift
//  Mazzi
//
//  Created by Bayara on 2/15/19.
//  Copyright © 2019 Mazz Application. All rights reserved.
//

import Foundation

class MoreCell:UITableViewCell{
   
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
}
