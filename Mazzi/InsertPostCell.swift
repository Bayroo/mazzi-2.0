//
//  InsertPostCell.swift
//  Mazzi
//
//  Created by Bayara on 12/15/18.
//  Copyright © 2018 Mazzi Application. All rights reserved.
//

import Foundation

class InsertPostCell:UITableViewCell{
    
    @IBOutlet weak var imgView:UIImageView!
    
    func initcell(){
        self.imgView.layer.borderWidth = 1
        self.imgView.layer.borderColor = GlobalVariables.F5F5F5.cgColor
        self.imgView.layer.cornerRadius = 17.5
        self.imgView.layer.masksToBounds = true
    }
}
