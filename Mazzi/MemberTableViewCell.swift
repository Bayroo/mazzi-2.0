//
//  MemberTableViewCell.swift
//  Mazzi
//
//  Created by Bayara on 12/10/18.
//  Copyright © 2018 Mazzi Application. All rights reserved.
//

import Foundation

class MemberTableViewCell:UITableViewCell{
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var lastnameLabel: UILabel!
    @IBOutlet weak var kickBtn: UIButton!
    @IBOutlet weak var roleLabel: UILabel!
    
    func initcell(){
        self.kickBtn.layer.cornerRadius = 10
        self.kickBtn.layer.borderWidth = 1
        self.kickBtn.layer.borderColor = UIColor.black.cgColor
        self.kickBtn.layer.masksToBounds = true
    }
}
