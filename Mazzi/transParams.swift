//
//  transParams.swift
//  Mazzi
//
//  Created by Bayara on 5/27/19.
//  Copyright © 2019 Mazz Application. All rights reserved.
//

import Foundation

class transParams{
    @objc dynamic var key_number = ""
    @objc dynamic var trans_number = ""
    @objc dynamic var trans_amount = ""
    @objc dynamic var lang = 0
    var dictionary: [String: Any] { return ["key_number": key_number, "trans_number": trans_number, "trans_amount": trans_amount, "lang": lang ] }
    
    init(key_number: String!,trans_number: String!, trans_amount: String!, lang: Int) {
        self.key_number = key_number
        self.trans_number = trans_number
        self.trans_amount = trans_amount
        self.lang = lang
    }
}
