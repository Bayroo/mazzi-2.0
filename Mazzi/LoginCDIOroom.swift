//
//  LoginCDIOroom.swift
//  Mazzi
//
//  Created by Bayara on 9/24/18.
//  Copyright © 2018 Mazzi App LLC. All rights reserved.
//

import Foundation
import UIKit
import Apollo
import SocketIO
import SwiftyJSON
import KVNProgress

class LoginCDIOroom:UIViewController{
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var roomLabel: UILabel!
    @IBOutlet weak var roomTextfield: UITextField!
    @IBOutlet weak var continueBtn: UIButton! 
    var seminarId = ""
    var launchId = ""
    var teacherId = ""
    var classId = ""
    var myId = ""
    var oneTry = false
    var totaltime = Double()
    var isClicked = false
    var apollo = SettingsViewController.apollo
//    var cdiosocket = SettingsViewController.cdiosocket
    override func viewDidLoad() {
        super .viewDidLoad()
        customize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)
        self.roomTextfield.becomeFirstResponder()
        _ = self.apollo.clearCache()
        self.totaltime = 0
        self.isClicked = false
        self.classId = GlobalStaticVar.classId
        self.myId = SettingsViewController.userId
        if !isClicked {
            self.continueBtn.isEnabled = true
        }
    }
    
    func customize(){
        print(SettingsViewController.cdiosocketUrl)
        self.title = "Нэвтрэх"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(baktoroot))
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        
//        self.titleLabel.textColor = GlobalVariables.todpurple
//        self.roomLabel.text = "Өрөөний нэр"
//        self.roomLabel.textColor = GlobalVariables.todpurple.withAlphaComponent(0.4)
        self.roomTextfield.layer.borderColor = GlobalVariables.purple.cgColor
        self.roomTextfield.layer.borderWidth = 1.0
        self.roomTextfield.layer.cornerRadius = 5
        self.continueBtn.layer.cornerRadius = 5
        self.continueBtn.backgroundColor = GlobalVariables.purple
        self.continueBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
        self.roomTextfield.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
       
//        if let btn = self.continueBtn {
//            let gradient = CAGradientLayer()
//            var bounds = self.continueBtn.bounds
//            bounds.size.height = 50
//            gradient.frame = bounds
//            gradient.colors = [ GlobalVariables.newPurple.cgColor,GlobalVariables.gradiendBlack.cgColor]
//            gradient.startPoint = CGPoint(x: 0, y: 0)
//            gradient.endPoint = CGPoint(x: 0, y: 1)
//            if let image = getImageFrom(gradientLayer: gradient) {
//                //                navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
//                btn.setBackgroundImage(image, for: UIControl.State.normal)
//            }
//        }
    
    }
    
//    func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
//        var gradientImage:UIImage?
//        UIGraphicsBeginImageContext(gradientLayer.frame.size)
//        if let context = UIGraphicsGetCurrentContext() {
//            gradientLayer.render(in: context)
//            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
//        }
//        UIGraphicsEndImageContext()
//        return gradientImage
//    }
    
    @IBAction func next(_ sender: Any) {
        checkInternet()
        isClicked = true
        if isClicked {
            self.continueBtn.isEnabled = false
        }
        if SettingsViewController.online == false{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу!")
            self.continueBtn.isEnabled = true
        }else{
            if roomTextfield.text == "" || roomTextfield.text == " " || roomTextfield.text == nil {
                self.roomLabel.text = "Өрөөний нэр оруулна уу!"
                self.roomLabel.textColor = UIColor.red
                self.roomTextfield.layer.borderColor = UIColor.red.cgColor
                self.isClicked = false
                self.continueBtn.isEnabled = true
            }else{
                KVNProgress.show()
//                GlobalStaticVar.classId
//                let query = CheckQuery(studentId: GlobalVariables.user_id , key:roomTextfield.text! ,classId: GlobalStaticVar.classId)
                print("GlobalStaticVar.classId: ",GlobalStaticVar.classId)
                let query = StudentCheckLaunchQuery(studentId: self.myId , key: roomTextfield.text!, classId: self.classId)
                apollo.fetch(query: query) { result, error in
                    KVNProgress.dismiss()
//                    print(result as Any)
//                    print("RESPO:: ",result?.data?.studentCheckLaunch?.response as Any)
                    if  (result?.data?.studentCheckLaunch?.response) == "OK" {
//                        print(result?.data?.studentCheckLaunch?.launch)
                        self.seminarId = (result?.data?.studentCheckLaunch?.launch?.seminarId)!
                        self.launchId = (result?.data?.studentCheckLaunch?.launch?.id)!
                        self.teacherId = (result?.data?.studentCheckLaunch?.launch?.teacherId)!
                        if result?.data?.studentCheckLaunch?.launch?.launchSettings?.oneTry != nil {
                              self.oneTry = (result?.data?.studentCheckLaunch?.launch?.launchSettings?.oneTry)!
                        }
                        if (result?.data?.studentCheckLaunch?.launch?.started)! {
                            let currentTime = Date().timeIntervalSince1970 * 1000
                            let startedTime = (result?.data?.studentCheckLaunch?.launch?.startedAt)!
                            let p = currentTime - startedTime
                            if (result?.data?.studentCheckLaunch?.launch?.launchSettings?.timer) != nil {
                                self.totaltime = ((result?.data?.studentCheckLaunch?.launch?.launchSettings?.timer)!) - p
                            }
                        }else{
                            if (result?.data?.studentCheckLaunch?.launch?.launchSettings?.timer) != nil {
                                self.totaltime = ((result?.data?.studentCheckLaunch?.launch?.launchSettings?.timer)!)
                            }
                        }
                        self.isClicked = false
                        self.continueBtn.isEnabled = true
                        self.performSegue(withIdentifier: "toRoom", sender: self)
                    }else  {
//                        print("status:: ", result?.data?.studentCheckLaunch?.status ?? "")
//                        print("jhgdahjwhgd:: ",result?.data?.studentCheckLaunch?.response ?? "")
//                        KVNProgress.showError(withStatus: result?.data?.studentCheckLaunch?.response)
                        let alert = UIAlertController(title: "Алдаа", message: result?.data?.studentCheckLaunch?.response!, preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler:  { (alert:UIAlertAction) -> Void in }))
                        
                        self.present(alert, animated: true, completion: nil)
                        
                        self.isClicked = false
                        self.continueBtn.isEnabled = true
                    }
                    if error != nil {
                        print("error: ",error as Any)
                        KVNProgress.showError(withStatus: "Сервертэй холбогдоход алдаа гарлаа!")
                        self.isClicked = false
                        self.continueBtn.isEnabled = true
                        getFireUrl(url: "my") { (suc) in
                            if suc == true {
                                print("url changed successfully")
                            }
                        }
                    }
                }
            }
        }
     
    }
    
    @objc func baktoroot(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissKeyboard(){
        roomTextfield.resignFirstResponder()
    }
    
     @objc func textFieldDidChange(textField:UITextField){
        self.roomLabel.text = ""
//        self.roomLabel.textColor = GlobalVariables.todpurple.withAlphaComponent(0.4)
        self.roomTextfield.layer.borderColor = GlobalVariables.purple.cgColor
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toRoom" {
            let controller = segue.destination as! RoomVC
            GlobalStaticVar.roomName = roomTextfield.text!
            controller.roomName = roomTextfield.text!
            controller.seminarId = self.seminarId
            controller.launchID = self.launchId
            controller.teacherId = self.teacherId
            controller.oneTry = self.oneTry
            controller.totaltime = self.totaltime
        }
    }
}
