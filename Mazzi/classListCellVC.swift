//
//  classListCellVC.swift
//  Mazzi
//
//  Created by Bayara on 7/18/19.
//  Copyright © 2019 Mazz Application. All rights reserved.
//

import Foundation

class classListCellVC: UICollectionViewCell {
    
    @IBOutlet weak var classNameLabel: UILabel!
    @IBOutlet weak var ownerNameLabel: UILabel!
    @IBOutlet weak var countMemberLabel: UILabel!
    @IBOutlet weak var badgeBtn: UIButton!
}
