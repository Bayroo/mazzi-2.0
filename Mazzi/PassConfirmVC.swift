//
//  PassConfirmVC.swift
//  Mazzi
//
//  Created by Bayara on 7/28/18.
//  Copyright © 2018 Mazzi App LLC. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import KVNProgress
import FirebaseAuth
import Firebase
import MessageUI
import LocalAuthentication

class PassConfirmVC : UIViewController,AuthUIDelegate {
    
    let userDefaults = UserDefaults.standard
    var visible = 0
    @IBOutlet weak var passField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var spiner: UIActivityIndicatorView!
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var forgetpassBtn: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var touchView: UIView!
    @IBOutlet weak var checkBox: UIImageView!
    
    var ischecked = false
    var recipients = [] as NSArray
    var userid = ""
    var image = ""
    var phone = ""
    var key = ""
    var gender = ""
    var region = ""
    var displayphone = ""
    var is_confirmed = false
    var birthday:Double = 0
    var lastname = ""
    var surname = ""
    var email = ""
    let limitLength = 20
    var register : String = ""
    var name = ""
    var verificationID = ""
    var ticketName = ""
    var token = ""
    var roleID = ""
    let localAuthenticationContext = LAContext()
    @IBOutlet weak var backbtn: UIButton!
    
    
    @IBOutlet weak var touchIdBtn: UIButton!
    override func viewDidLoad() {
        super .viewDidLoad()
        self.navigationItem.title = "Нууц үг оруулах"
        errorLabel.text = ""
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
//        self.backbtn.tintColor = UIColor.black
        
        let image = UIImage (named: "Back")?.withRenderingMode(.alwaysTemplate)
        self.backbtn.setImage(image, for: .normal)
        self.backbtn.tintColor = UIColor.black
        
        self.avatarImg.image = UIImage(named: "Type")
        self.passField.textColor = GlobalVariables.purple
        
        let bottomLine4 = CALayer()
        bottomLine4.frame = CGRect(x:0.0, y:self.passField.frame.height - 1, width:self.passField.frame.width, height: 1.0)
        bottomLine4.backgroundColor = GlobalVariables.lightGray.cgColor
        self.passField.borderStyle = UITextField.BorderStyle.none
        self.passField.layer.addSublayer(bottomLine4)
        
//      self.passField.attributedPlaceholder = NSAttributedString(string: "Нууц үгээ оруулна уу!",attributes: [NSAttributedString.Key.foregroundColor: GlobalVariables.purple.withAlphaComponent(0.2)])
        
        self.btn.setTitleColor(GlobalVariables.TextTod, for: UIControl.State.normal)
        self.btn.layer.cornerRadius = 25
        
        self.touchView.backgroundColor = GlobalVariables.purple
        self.touchView.layer.cornerRadius = 5
        
        self.forgetpassBtn.setTitleColor(GlobalVariables.purple, for: UIControl.State.normal)
        userid = (userDefaults.object(forKey: "user_id") as? String) ?? ""
        NotificationCenter.default.addObserver(self, selector: #selector(showKeyboard(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        let attrs = [
            NSAttributedString.Key.font : UIFont(name: "OpenSans", size: 17.0),
            NSAttributedString.Key.foregroundColor : UIColor.black,
            NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
        
        let attributedString = NSMutableAttributedString(string:"")

        
        let buttonTitleStr = NSMutableAttributedString(string:"Нууц үг мартсан", attributes:attrs)
        attributedString.append(buttonTitleStr)
        self.forgetpassBtn.setAttributedTitle(attributedString, for: .normal)
        
    }
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func showPassBtn(_ sender: Any) {
        if self.passField.isSecureTextEntry == false {
            self.passField.isSecureTextEntry = true
        }else{
             self.passField.isSecureTextEntry = false
        }
    }
    @IBAction func AuthTouchId(_ sender: Any) {
        if self.ischecked  == false {
            self.checkBox.image = UIImage(named: "checkbox-checked")
            self.ischecked = true
        }else{
            self.checkBox.image = UIImage(named: "checkbox")
            self.ischecked = false
            
            self.userDefaults.set(false, forKey: "isTouchId")
            self.userDefaults.synchronize()
            self.passField.text = ""
            self.passField.becomeFirstResponder()
        }
    }
    
    func autchTouchId(){
        print("hello there!.. You have clicked the touch ID")
        
        let myContext = LAContext()
        let myLocalizedReasonString = "Хурууны хээгээ таниулна уу"
        
        var authError: NSError?
        if #available(iOS 8.0, macOS 10.12.1, *) {
            if myContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
                myContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: myLocalizedReasonString) { success, evaluateError in
                    
                    DispatchQueue.main.async {
                        if success {
                            self.checkBox.image = UIImage(named: "checkbox-checked")
                            if let pass = self.userDefaults.string(forKey: "pass"){
                                self.passField.text = pass
                            }
                            self.userDefaults.set(true, forKey: "isTouchId")
                            self.userDefaults.synchronize()
                            self.loginByTouchId()
                            // User authenticated successfully, take appropriate action
                            //  self.successLabel.text = "Awesome!!... User authenticated successfully"
                        } else {
                            self.userDefaults.removeObject(forKey: "pass")
                            self.userDefaults.set(false, forKey: "isTouchId")
                            self.userDefaults.synchronize()
                            self.passField.text = ""
                            self.checkBox.image = UIImage(named: "checkbox")
                            // User did not authenticate successfully, look at error and take appropriate action
                            //                            self.successLabel.text = "Sorry!!... User did not authenticate successfully"
                        }
                    }
                }
            } else {
                // Could not evaluate policy; look at authError and present an appropriate message to user
                //                successLabel.text = "Sorry!!.. Could not evaluate policy."
                self.userDefaults.set(false, forKey: "isTouchId")
                self.userDefaults.synchronize()
                self.passField.text = ""
                self.checkBox.image = UIImage(named: "checkbox")
            }
        } else {
            // Fallback on earlier versions
            self.passField.text = ""
            self.userDefaults.set(false, forKey: "isTouchId")
            self.userDefaults.synchronize()
            self.checkBox.image = UIImage(named: "checkbox")
            KVNProgress.showError(withStatus: "")
            self.errorLabel.text = "Ooops!!.. This feature is not supported."
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let status = self.userDefaults.bool(forKey: "isTouchId")
        if status == true {
            self.checkBox.image = UIImage(named: "checkbox-checked")
            self.autchTouchId()
        }else{
            self.userDefaults.set(false, forKey: "isTouchId")
            self.userDefaults.synchronize()
            self.checkBox.image = UIImage(named: "checkbox")
            self.passField.text = ""
            self.passField.becomeFirstResponder()
        }
       
        if GlobalVariables.isFromPassRecover == true {
            self.passField.attributedPlaceholder = NSAttributedString(string: "Шинэ нууц үгээ оруулна уу",attributes: [NSAttributedString.Key.foregroundColor: GlobalVariables.purple.withAlphaComponent(0.2)])
        }else{
            self.passField.attributedPlaceholder = NSAttributedString(string: "Нууц үг",attributes: [NSAttributedString.Key.foregroundColor: GlobalVariables.purple.withAlphaComponent(0.2)])
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        GlobalVariables.isFromPassRecover = false
//         self.navigationController?.navigationBar.isHidden = true
    }
    
    @objc func showKeyboard(notification: Notification) {
        if let frame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let height = frame.cgRectValue.height
            if height > 100{
                let keyboardHeight = Int(height.rounded(.toNearestOrEven))
                self.userDefaults.set(keyboardHeight, forKey: "keyboardheight")
                self.userDefaults.synchronize()
            }
        }
    }
    
    @objc func backtoROot(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func confirm(_ sender: Any) {
        if self.ischecked == true{
             self.autchTouchId()
        }else{
             self.LoginWithPass()
        }
    }
    
    @IBAction func recoverPass(_ sender: Any) {
        let when = DispatchTime.now() + 2
        self.phone = GlobalVariables.phone
        KVNProgress.show()
        PhoneAuthProvider.provider().verifyPhoneNumber("+\(self.phone)", uiDelegate: self, completion: { (verificationID, error) in
            GlobalVariables.phone = self.phone
            if let error = error {
                 KVNProgress.dismiss()
                print("LoginViewController - sendkey: \(error)")
             
                let alert = UIAlertController(title: "", message: "Дахин оролдоно уу!", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            } else{
                self.verificationID = verificationID!
                KVNProgress.dismiss()
                let alert = UIAlertController(title: "", message: "Баталгаажуулах код илгээлээ.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction) -> Void in
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        self.performSegue(withIdentifier: "recoverPass", sender: self)
                    }
                    }))
                self.present(alert, animated: true, completion: nil)
            
                self.userDefaults.set(self.verificationID, forKey: "authPass")
                self.userDefaults.synchronize()
               
            }
        })
    }
    @objc func dismissKeyboard(){
        passField.resignFirstResponder()
    }
    
    func updateFcmToken(){
        let basurl = SettingsViewController.baseurl
        let headers = SettingsViewController.headers
        print("/////////////////////////////////////////////////////////////////////////////")

         Alamofire.request("\(basurl)update_fcmToken", method: .post, parameters: ["userId":GlobalVariables.user_id,"fcm_token":GlobalVariables.fcm_token], encoding: JSONEncoding.default,headers:headers).responseJSON{ response in
            switch response.result {
            case .success:
                let res = JSON(response.result.value!)
                if res["error"] == false {
                    print("FCM UPDATE SUCCESS")
                }
            case .failure(let e):
                print(e)
            }
        }
    }
    
    func loginByTouchId(){
        let basurl = SettingsViewController.baseurl
        let headers = SettingsViewController.headers
        
        if let pass = self.userDefaults.string(forKey: "pass"){
            self.passField.text = pass
        }
        KVNProgress.show()
        Alamofire.request("\(basurl)login_with_pass", method: .post, parameters: ["phone":GlobalVariables.phone,"password":self.passField.text!], encoding: JSONEncoding.default,headers:headers).responseJSON{ response in
            KVNProgress.dismiss()
            //                print("response:: ",response)
            if response.response?.statusCode == 200 {
                let res = JSON(response.value!)
                //                    print("res::",res)
                print(" ----LOGIN_WITH_PASS---- ")
                self.token =  res["token"].stringValue
                self.userDefaults.set(self.token, forKey: "token")
                self.userDefaults.synchronize()
                Utils.sharedInstance.token =  self.token
                GlobalVariables.headerToken =  self.token
                
                if res["error"].bool! == false {
                    self.userid = res["_id"].string ?? ""
                    SettingsViewController.userId = self.userid
                    GlobalVariables.user_id = self.userid
                    self.roleID = res["roleId"].string ?? ""
                    self.image = res["image"].string ?? ""
                    self.name = res["nickname"].string ?? ""
                    self.lastname = res["lastname"].string ?? ""
                    self.email = res["email"].string ?? ""
                    self.phone = (self.userDefaults.string(forKey: "phone")!)
                    self.region = res["region"].string ?? ""
                    self.register = res["registerId"].string ?? ""
                    self.gender = res["gender"].string ?? ""
                    self.is_confirmed = res["is_confirmed"].bool ?? false
                    self.birthday = res["birthday"].double ?? 0
                    self.ticketName = res["ticket"]["ticketName"].string ?? ""
                    let profile = ["_id":self.userid,"image":self.image,"nickname": self.name, "phone": self.phone, "key": self.key, "region": self.region, "lastname": self.lastname,  "surname": self.surname, "birthday": self.birthday,"email":self.email, "gender":self.gender, "fcm_token":GlobalVariables.fcm_token, "password": self.passField.text! , "registerId": self.register] as [String : Any]
                    KVNProgress.show()
                    User_group.updateprofile(profile: profile, userids: res["_id"].string!, completion: { (status) in
                        if status == true {
                            DispatchQueue.main.async{
                                self.userDefaults.set(self.ticketName, forKey: "ticketName")
                                self.userDefaults.set(self.userid, forKey: "user_id")
                                self.userDefaults.set(self.passField.text!, forKey:"pass")
                                
                                let userInfo = ["image": self.image,
                                                "nickname":self.name,
                                                "lastname": self.lastname,
                                                "registerId": self.register,
                                                "birthday":self.birthday,
                                                "email":self.email,
                                                "gender":self.gender,
                                                "region":self.region,
                                                "displayphone": self.phone,
                                                "is_confirmed": self.is_confirmed
                                    ] as [String : Any]
                                self.userDefaults.set(self.displayphone, forKey: "phone")
                                self.userDefaults.set(self.roleID, forKey: "role")
                                UserDefaults.standard.set(userInfo, forKey: "userInfo")
                                SettingsViewController.userId = res["_id"].string!
                                
                                self.userDefaults.synchronize()
                                //  print("us: ",userInfo)
                            }
                            // KVNProgress.dismiss()
                            let when = DispatchTime.now() + 3 // change 2 to desired number of seconds
                            DispatchQueue.main.asyncAfter(deadline: when) {
                                if self.visible == 1 {
                                    self.viewDidLoad()
                                    self.dismiss(animated: true, completion: nil)
                                }else{
                                    self.updateFcmToken()
                                    self.userDefaults.set(true, forKey:"isTouchId")
                                    self.userDefaults.set(self.passField.text, forKey: "pass")
                                    GlobalVariables.isloggedin = "true"
                                    self.userDefaults.set(GlobalVariables.isloggedin, forKey: "isLogin")
                                    self.userDefaults.synchronize()
                                    KVNProgress.show()
                                    Conversation.getconversations(userid: res["_id"].string!, limit: 40, skip: 0) { (conversations) in }
                                    let time = DispatchTime.now() + 1
                                    DispatchQueue.main.asyncAfter(deadline: time) {
                                        KVNProgress.dismiss()
                                        self.navigationController?.popToRootViewController(animated:true)
                                    }
                                }
                            }
                        }
                        
                    })
                }
                else{
                    self.userDefaults.set(false, forKey: "isTouchId")
                    self.userDefaults.synchronize()
                    self.checkBox.image = UIImage(named: "checkbox")
                    self.passField.text = ""
                    self.passField.becomeFirstResponder()
                    self.userDefaults.removeObject(forKey: "pass")
                    
                    let alert = UIAlertController(title: "Алдаа !", message: "Нууц үг буруу байна !", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func LoginWithPass(){
        let basurl = SettingsViewController.baseurl
        let headers = SettingsViewController.headers
        if self.passField.text! == "" {
            let alert = UIAlertController(title: "Алдаа !", message: "Нууц үг оруулна уу", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            KVNProgress.show()
            Alamofire.request("\(basurl)login_with_pass", method: .post, parameters: ["phone":GlobalVariables.phone,"password":self.passField.text!], encoding: JSONEncoding.default,headers:headers).responseJSON{ response in
                KVNProgress.dismiss()
                if response.response?.statusCode == 200 {
                    let res = JSON(response.value!)
                    self.token =  res["token"].stringValue
                    self.userDefaults.set(self.token, forKey: "token")
                    self.userDefaults.synchronize()
                    Utils.sharedInstance.token =  self.token
                    GlobalVariables.headerToken =  self.token
                    if res["error"].bool! == false {
                        self.userid = res["_id"].string ?? ""
                        SettingsViewController.userId = self.userid
                        GlobalVariables.user_id = self.userid
                        self.roleID = res["roleId"].string ?? ""
                        self.image = res["image"].string ?? ""
                        self.name = res["nickname"].string ?? ""
                        self.lastname = res["lastname"].string ?? ""
                        self.email = res["email"].string ?? ""
                        self.phone = (self.userDefaults.string(forKey: "phone")!)
                        self.region = res["region"].string ?? ""
                        self.register = res["registerId"].string ?? ""
                        self.gender = res["gender"].string ?? ""
                        self.is_confirmed = res["is_confirmed"].bool ?? false
                        self.birthday = res["birthday"].double ?? 0
                        self.ticketName = res["ticket"]["ticketName"].string ?? ""
                        let profile = ["_id":self.userid,"image":self.image,"nickname": self.name, "phone": self.phone, "key": self.key, "region": self.region, "lastname": self.lastname,  "surname": self.surname, "birthday": self.birthday,"email":self.email, "gender":self.gender, "fcm_token":GlobalVariables.fcm_token, "password": self.passField.text! , "registerId": self.register] as [String : Any]
                        KVNProgress.show()
                        User_group.updateprofile(profile: profile, userids: res["_id"].string!, completion: { (status) in
                            if status == true {
                                DispatchQueue.main.async{
                                    self.userDefaults.set(self.ticketName, forKey: "ticketName")
                                    self.userDefaults.set(self.userid, forKey: "user_id")
                                    self.userDefaults.set(self.passField.text!, forKey:"pass")
                                    
                                    let userInfo = ["image": self.image,
                                                    "nickname":self.name,
                                                    "lastname": self.lastname,
                                                    "registerId": self.register,
                                                    "birthday":self.birthday,
                                                    "email":self.email,
                                                    "gender":self.gender,
                                                    "region":self.region,
                                                    "displayphone": self.phone,
                                                    "is_confirmed": self.is_confirmed
                                        ] as [String : Any]
                                    self.userDefaults.set(self.displayphone, forKey: "phone")
                                    self.userDefaults.set(self.roleID, forKey: "role")
                                    UserDefaults.standard.set(userInfo, forKey: "userInfo")
                                    SettingsViewController.userId = res["_id"].string!
                                    self.userDefaults.synchronize()
                                }
                                let when = DispatchTime.now() + 3 // change 2 to desired number of seconds
                                DispatchQueue.main.asyncAfter(deadline: when) {
                                    if self.visible == 1 {
                                        self.viewDidLoad()
                                        self.dismiss(animated: true, completion: nil)
                                    }else{
                                        self.updateFcmToken()
                                        GlobalVariables.isloggedin = "true"
//                                        self.userDefaults.removeObject(forKey: "pass")
                                        self.userDefaults.set(false, forKey:"isTouchId")
                                        self.userDefaults.set(GlobalVariables.isloggedin, forKey: "isLogin")
                                        self.userDefaults.synchronize()
                                        KVNProgress.show()
                                        Conversation.getconversations(userid: res["_id"].string!, limit: 40, skip: 0) { (conversations) in }
                                        let time = DispatchTime.now() + 1
                                        DispatchQueue.main.asyncAfter(deadline: time) {
                                            KVNProgress.dismiss()
                                            self.navigationController?.popToRootViewController(animated:true)
                                        }
                                    }
                                }
                            }
                            
                        })
                    }
                    else{
                        let alert = UIAlertController(title: "Алдаа !", message: "Нууц үг буруу байна !", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
    }
}
