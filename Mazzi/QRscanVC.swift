//
//  QRscanVC.swift
//  Mazzi
//
//  Created by Bayara on 8/18/18.
//  Copyright © 2018 Mazzi App LLC. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import AVFoundation
import SwiftyJSON
import Alamofire
import KVNProgress
import Apollo
import CoreLocation


class QRscanVC: UIViewController,AVCaptureMetadataOutputObjectsDelegate{
    
    var currentLocation: CLLocation!
    var lat : Double!
    var long : Double!
    let locManager = CLLocationManager()
    var myId = SettingsViewController.userId
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var checkinOrCheckout = ""
    var message = ""
    var qrCode = ""
    var classId = ""
    var apollo: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
        let url = URL(string:SettingsViewController.graphQL)!
        
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:UIImage(named: "05"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(back))
        cameraViewInit()
    }
    
   @objc func back(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func cameraViewInit(){
        
        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        let metadataOutput = AVCaptureMetadataOutput()
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr, .ean13]
        } else {
            failed()
            return
        }
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)
        //here is qr view added subview
        //        let imgViewframe = CGRect(x: 0, y: 40, width: view.frame.width, height: view.frame.height/2)
        //        let imageview = UIImageView(frame: imgViewframe)
        //        imageview.image = UIImage(named: "qrlogo")
        //        view.addSubview(imageview)
        
        captureSession.startRunning()
        
    }
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.classId = GlobalStaticVar.classId
        self.myId = SettingsViewController.userId
        
        let when = DispatchTime.now() + 2
        DispatchQueue.main.asyncAfter(deadline: when) {
           self.getCurrentLocation()
        }
        
        
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            print(metadataObject.type)
            found(code: stringValue)
        }
    }
    
    func found(code: String) {
        if  let decodedData = Data(base64Encoded: code)  {
            if let decodedString = String(data: decodedData, encoding: .utf8){
                    if let dataFrom = decodedString.data(using: .utf8, allowLossyConversion: false) {
                        let json = JSON(dataFrom)
                        self.qrCode = json["code"].string ?? ""
                        self.sendCode()
                        print("code:",  self.qrCode)
                    }
            }else{
                KVNProgress.showError(withStatus: "Буруу QR байна")
                self.back()
            }
        }else{
            KVNProgress.showError(withStatus: "Буруу QR байна")
            self.back()
        }
    }
    
    func sendCode(){
//        print("myId: ", self.myId)
//         print("code: ", self.qrCode)
//         print("lat: ", self.lat)
//         print("long: ", self.long)
        
        let mutation = CheckIrtsMutation(userId: self.myId, code: self.qrCode, lat: self.lat, long: self.long,classId: self.classId)
         self.apollo.perform(mutation: mutation) { result, error in
            print("RESULT:: ",result as Any)
            if result?.data?.checkInOrOut?.error == false {
                let alert = UIAlertController(title: "", message: "Ирц амжилттай бүртгэгдлээ", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "ОК", style: .default  , handler: {action in
                     self.back()
                }))
                self.present(alert, animated: true, completion: nil)
            }else{
                KVNProgress.showError(withStatus: result?.data?.checkInOrOut?.message)
                self.back()
            }
        }
    }
    
//    func getQr(id:String){
//
//        let baseurl = SettingsViewController.QRurl
//        let  header = ["authorization": "BPXjEvWfXxx5qpQHHFMEJEB8fA3xAczC"]
//        KVNProgress.show()
//        Alamofire.request("\(baseurl)getTicket", method: .post, parameters: ["ticket_id":id], encoding: JSONEncoding.default, headers:header).responseJSON{ response in
//        KVNProgress.dismiss()
//            print("ticket_id:", id)
//            print("res:" ,response)
//
//            switch response.result {
//            case .success:
//                let res = JSON(response.result.value!)
//                let id = res["result"]["_id"].string!
//                let name = res["result"]["qr"].string!
//                self.insertToServer(id: id, name: name)
//
//            case .failure(let e):
//                let error = e as NSError
//                if error.code == -1009{
//                    let alert = UIAlertController(title: "Алдаа", message: "Интернэт холболтоо шалгана уу!", preferredStyle: UIAlertController.Style.alert)
//                    alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
//                    self.present(alert, animated: true, completion: nil)
//                }else if error.code == -1004{
//                    let alert = UIAlertController(title: "Алдаа", message: "Бүртгэгдсэн эсвэл буруу QR код байна!", preferredStyle: UIAlertController.Style.alert)
//                    alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
//                    self.present(alert, animated: true, completion: nil)
//                } else if error.code == -1005{
//                    let alert = UIAlertController(title: "Алдаа", message: "Сервэртэй холбогдоход алдаа гарлаа!", preferredStyle: UIAlertController.Style.alert)
//                    alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
//                    self.present(alert, animated: true, completion: nil)
//                }
//            }
//        }
//    }
    
    func insertToServer(id:String,name:String){
        
        let userInfo = userDefaults.dictionary(forKey: "userInfo")
        let baseurl = SettingsViewController.baseurl
        let header = ["authorization": "BPXjEvWfXxx5qpQHHFMEJEB8fA3xAczC"]
        let phone = "976\(userInfo!["displayphone"] as! String)"
        KVNProgress.show()
        Alamofire.request("\(baseurl)insertTicket", method: .post, parameters: ["phone":phone,"ticket":["ticket_id":id, "ticketName":name]], encoding: JSONEncoding.default,headers:header).responseJSON{ response in
        KVNProgress.dismiss()
            switch response.result{
            case .success:
                
                if((response.result.value) != nil) {
                    let res = JSON(response.result.value!)
                    let result = res["result"]
                    let ticketName = result["ticket"]["ticketName"].string ?? ""
                    UserDefaults.standard.set(ticketName, forKey: "ticketName")
                    UserDefaults.standard.synchronize()

                    let alert = UIAlertController(title: "", message: "Амжилттай бүртгэгдлээ.", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction) -> Void in
                          self.navigationController?.popViewController(animated: true)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                //    self.navigationController?.popToRootViewController(animated: true)
                  
                    
                }
            case .failure(let e):
                print(e)
                let alert = UIAlertController(title: "insert", message: "Интернет холболтоо шалгана уу !", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "failView"{
            
        }else if segue.identifier == "sucView"{
            
        }
    }
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    // LOCATION REQUEST
    
    func getCurrentLocation(){
        
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            currentLocation = locManager.location
            self.lat = currentLocation.coordinate.latitude
            self.long = currentLocation.coordinate.longitude
        }else{
            self.locationManager(manager: locManager, didChangeAuthorizationStatus: .notDetermined)
        }
    }
    
    private func locationManager(manager: CLLocationManager!,
                                 didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        var shouldIAllow = false
        var locationStatus = ""
        switch status {
        case CLAuthorizationStatus.restricted:
            locationStatus = "Restricted Access to location"
        case CLAuthorizationStatus.denied:
            locationStatus = "User denied access to location"
        case CLAuthorizationStatus.notDetermined:
            locationStatus = "notDetermined"
        default:
            locationStatus = "Allowed to location Access"
            shouldIAllow = true
        }
        print(locationStatus)
        if shouldIAllow == false {
            let alert = UIAlertController(title: "", message: "Утасныхаа байршил тогтоогчийг идэвхжүүлнэ үү", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Тохиргоо", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction) -> Void in
                let alertt = UIAlertController(title: "Заавар", message: "Settings -> Privacy -> Location Services -> Mazzi : While Using the App", preferredStyle: UIAlertController.Style.alert)
                alertt.addAction(UIAlertAction(title: "OK", style: .cancel, handler:  { (alert:UIAlertAction) -> Void in }))
                self.present(alertt, animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
