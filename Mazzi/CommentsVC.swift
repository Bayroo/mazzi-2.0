//
//  CommentsVC.swift
//  Mazzi
//
//  Created by Bayara on 11/28/18.
//  Copyright © 2018 Mazzi Application. All rights reserved.
//

import Foundation
import KVNProgress
import Apollo
import AVFoundation
import Photos
import MobileCoreServices
import SwiftyJSON
import Alamofire

class CommentsVC : UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    enum AttachmentType: String{
        case camera, photoLibrary
    }
    
    @IBOutlet weak var bottomTool: UIView!
    @IBOutlet weak var commentTxtField: UITextField!
    @IBOutlet weak var commentSendBtn: UIButton!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var attachImageBtn: UIButton!
    
//    var apollo = SettingsViewController.apollo
    var newPostId = ""
    var isposting = false
    var selectedSection = 0
    var path = ""
    var isPicked = false
    var pikedImage : UIImage!
    var topic = ""
    var selectedUrl: URL!
    var collapsed = false
    var postId = ""
    var classId = ""
    var fileInput : InputPostFile!
    var commentFiles = [PostCommentsQuery.Data.PostComment.File]()
    var userId = SettingsViewController.userId
    var comments = [PostCommentsQuery.Data.PostComment]()
    var files = [SinglepostQuery.Data.Post.File]()
    var links = [String]()
    var inputComment = InputPostComment(ownerId: SettingsViewController.userId, content: "")
    var tapGesture: UITapGestureRecognizer!
    var singlepost = SinglepostQuery.Data.Post()
    var dateformatter = DateFormatter()
    var selectedIndex = 0
    var backview = UIView()
    var selectedFileIndex = 0
    var postContent = ""
    var postDate : Double!
    var posterImg = ""
    var posterName = ""
    var posterLastName = ""
    var bottomConstraint:NSLayoutConstraint?
    var staticheight:CGFloat = 50
    var postOwnerId = ""
    var teacherId = GlobalStaticVar.teacherId
    let apollo: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        // Add additional headers as needed
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
//        print("TOKEN APOLLOO:: ", GlobalVariables.headerToken)
        let url = URL(string:SettingsViewController.graphQL)!
        
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getSubUser()
        self.customize()
    }
    @IBAction func sendComment(_ sender: Any) {
        self.sendComment()
    }
    
    @objc func chooseImg(){
        if self.isPicked == true {
            let actionSheet = UIAlertController(title: "", message: "Үйлдлээ сонгох".uppercased(), preferredStyle: .actionSheet)
            
            actionSheet.addAction(UIAlertAction(title: "Оруулсан зургаа солих", style: .default, handler: { (action) -> Void in
                 self.authorisationStatus(attachmentTypeEnum: .photoLibrary, vc: self)
            }))
            
            actionSheet.addAction(UIAlertAction(title: "Оруулсан зургаа устгах", style: .default, handler: { (action) -> Void in
               self.isPicked = false
               self.pikedImage = nil
               self.attachImageBtn.setImage(UIImage(named: "jpg"), for: .normal)
                if self.commentTxtField.text == "" || self.commentTxtField.text == " " {
                    self.commentSendBtn.setImage(UIImage(named: "001-bear-pawprint-saaral"), for: .normal)
                    self.commentSendBtn.isEnabled = false
                }
            }))
            
            actionSheet.addAction(UIAlertAction(title: "Болих", style: .cancel, handler: nil))
            self.present(actionSheet, animated: true, completion: nil)
            
        }else{
            self.authorisationStatus(attachmentTypeEnum: .photoLibrary, vc: self)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        GlobalStaticVar.notifiedComment = false
        GlobalStaticVar.newPost = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.commentTxtField.resignFirstResponder()
        self.classId = GlobalStaticVar.classId
        if GlobalStaticVar.notifiedComment == true {
            self.classId = GlobalStaticVar.notifiedClassId
            if GlobalStaticVar.newPost == true {
                 self.postId = GlobalStaticVar.notifiedPostId
            }else{
                 self.postId = GlobalStaticVar.notifiedRoodPostId
            }
        }
        self.getPost()
        if GlobalStaticVar.isPendingPost == true{
            self.bottomTool.isHidden = true
        }else{
             self.bottomTool.isHidden = false
        }
//        print("TEACHERID:: ", self.teacherId)
        if self.postOwnerId != self.userId {
            if self.userId  != self.teacherId {
                self.navigationItem.rightBarButtonItem = nil
            }else{
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:"editmenu"), style: .plain, target: self, action: #selector(editPost(_:)))
            }
        }else{
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:"editmenu"), style: .plain, target: self, action: #selector(editPost(_:)))
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
         self.navigationItem.rightBarButtonItem = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.commentTxtField.resignFirstResponder()
        return true
    }
    
    func customize(){
//        print("FILES::", self.files)
        self.bottomTool.backgroundColor = GlobalVariables.saaral
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(backtoRoot))
        self.tableview.delegate = self
        self.tableview.dataSource = self
        self.commentTxtField.delegate = self
        self.tableview.register((UINib(nibName: "postsCell", bundle: nil)), forCellReuseIdentifier: "postsCell")
        self.tableview.register((UINib(nibName: "CommentCell", bundle: nil)), forCellReuseIdentifier: "coment")
        self.tableview.register((UINib(nibName: "FilesCell", bundle: nil)), forCellReuseIdentifier: "filecell")
        self.tableview.register((UINib(nibName: "commentListCell", bundle: nil)), forCellReuseIdentifier: "commentList")
        self.tableview.rowHeight = UITableView.automaticDimension
        
        NotificationCenter.default.addObserver(self, selector: #selector(CommentsVC.showKeyboard(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CommentsVC.hideKeyboard(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        bottomConstraint = NSLayoutConstraint(item: self.bottomTool, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        view.addConstraint(bottomConstraint!)
       
        self.commentSendBtn.layer.cornerRadius = 15
        self.commentSendBtn.layer.masksToBounds = true
        self.commentSendBtn.isEnabled = false
        self.attachImageBtn.addTarget(self, action: #selector(chooseImg), for: .touchUpInside)
        self.commentTxtField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }
    
    func getPost(){
//        print(self.userId)
//        print(self.classId)
//        print(self.postId)
        
        let query = SinglepostQuery(userId: self.userId, classId: self.classId, postId: self.postId)
        KVNProgress.show(withStatus: "", on: self.view)
         apollo.fetch(query: query) { result, error in
            KVNProgress.dismiss()
            if result?.data?.post != nil {
                self.singlepost = (result!.data?.post)! as SinglepostQuery.Data.Post
                    self.posterName = self.singlepost.ownerId?.nickname ?? ""
                    self.posterImg = self.singlepost.ownerId?.image ?? ""
                    self.posterLastName = self.singlepost.ownerId?.lastname ?? ""
                    self.postContent = self.singlepost.content ?? ""
                    self.postOwnerId = self.singlepost.ownerId?.id ?? ""
                if self.singlepost.file != nil {
                    self.files = (self.singlepost.file! as! [SinglepostQuery.Data.Post.File])
                }
                if self.singlepost.link != nil{
                    self.links = self.singlepost.link! as! [String]
                }
                self.getComments()
            }else{
//                print("RESULT NIL : :",result)
            }
        }
    }
    
    @objc func showKeyboard(notification: Notification) {
        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
                self.view.addGestureRecognizer(tapGesture)
        if let frame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let height = frame.cgRectValue.height
            bottomConstraint?.constant = -height
            self.tableview.contentInset.bottom = height
            self.tableview.scrollIndicatorInsets.bottom = 20
            UIView.animate(withDuration: 0, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.view.layoutIfNeeded()
                if self.comments.count > 0{
                    self.tableview.scrollToRow(at: IndexPath.init(row: self.comments.count - 1, section:3), at: .bottom, animated: false)
                }
            }) { (completed) in
                
            }
        }
    }
    
    @objc func hideKeyboard(notification: Notification) {
        self.view.removeGestureRecognizer(self.tapGesture)
        bottomConstraint?.constant = 0
        self.tableview.contentInset.bottom = 0
        UIView.animate(withDuration: 0, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.view.layoutIfNeeded()
            if self.comments.count > 0{
                self.tableview.scrollToRow(at: IndexPath.init(row: self.comments.count - 1, section: 3), at: .bottom, animated: false)
            }
        }) { (completed) in
            
        }
    }
    
    @objc func dismissKeyboard(){
        self.commentTxtField.resignFirstResponder()
    }
    
    @objc func editPost(_ sender:Any) {
        if self.isposting == false{
            self.selectedIndex = (sender as AnyObject).tag!
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            if self.postOwnerId == self.userId{
                alert.addAction(UIAlertAction(title: "Засварлах", style: .default, handler: { action in
                    GlobalStaticVar.isPostCreate = false
                    self.performSegue(withIdentifier: "updatePost", sender: self)
                }))
                
            }
            
            alert.addAction(UIAlertAction(title: "Устгах", style: .default, handler:{action in
                let subAlert = UIAlertController(title: nil, message: "Устгахдаа итгэлтэй байна уу", preferredStyle: .alert)
                subAlert.addAction(UIAlertAction(title:"Тийм", style: .default, handler: { (action) in
                    //                let postId = self.posts[(sender as AnyObject).tag].id
                    //                print("POSTID:: ", postId! )
                    let mutation = DeletePostMutation(postId: self.postId, classId: self.classId, userId: self.userId)
                    self.apollo.perform(mutation: mutation) { result, error in
//                        print("response:: ",result?.data?.deletePost?.message as Any)
                        if result?.data?.deletePost?.error == false{
                            KVNProgress.showSuccess(withStatus: "Амжилттай устлаа")
                            self.backtoRoot()
                        }else{
                            KVNProgress.showError(withStatus: result?.data?.deletePost?.message)
                        }
                        print("error:: ",result?.data?.deletePost?.error as Any)
                    }
                }))
                subAlert.addAction(UIAlertAction(title: "Үгүй", style: .cancel, handler: nil))
                self.present(subAlert,animated: true)
            }))
            alert.addAction(UIAlertAction(title: "Болих", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
        
    }
    
    @objc func sendComment(){
     self.isposting = true
        if self.pikedImage != nil {
            uploadImage(teacherId: self.teacherId, image: self.pikedImage, completion: {(state,imageurl) in
                DispatchQueue.main.async {
                    let str = JSON(imageurl)
                    if !str.isEmpty{
                        let msg = str["error"].boolValue
                        if msg == false{
                            let url = str["url"].string ?? ""
                            let ext = str["extension"].string ?? ""
                            let name = "\("image")\(ext.lowercased())"
                            let size = str["file_size"].double ?? 0
                            let thumb = str["thumbnail"].string ?? ""
                            let imageurl = InputPostFile.init(extension: ext, url: url, name: name, size: size, thumbnail: thumb)
                            let imputPostCommmnet = InputPostComment.init(ownerId: self.userId, content: self.commentTxtField.text!,file: imageurl)
                            
                            let muteition = InsertCommentPostMutation(postId: self.postId, classId: self.classId, inputPostComment: imputPostCommmnet)
                             self.apollo.perform(mutation: muteition) { result, error in
                                if result?.data?.insertCommentPost?.error == false{
                                    self.commentTxtField.text = ""
                                    self.isPicked = false
                                    self.pikedImage = nil
                                    self.attachImageBtn.setImage(UIImage(named: "jpg"), for: .normal)
                                    self.commentTxtField.resignFirstResponder()
                                    self.commentSendBtn.setImage(UIImage(named: "001-bear-pawprint-saaral"), for: .normal)
                                    self.commentSendBtn.isEnabled = false
                                    self.isposting = false
                                    self.reloadComments()
                                }else{
                                    self.isposting = false
                                    KVNProgress.showError(withStatus: result?.data?.insertCommentPost?.message)
                                    self.commentSendBtn.setImage(UIImage(named: "001-bear-pawprint"), for: .normal)
                                    self.commentSendBtn.isEnabled = true
                                }
                            }
                        }
                    }
                }
            })
        }else{
            if self.commentTxtField.text!.isEmpty{
                KVNProgress.showError(withStatus: "Сэтгэгдлээ оруулна уу")
                    self.isposting = false
            }else{
                self.commentSendBtn.setImage(UIImage(named: "001-bear-pawprint-saaral"), for: .normal)
                self.commentSendBtn.isEnabled = false
                self.inputComment = InputPostComment.init(ownerId: self.userId, content: self.commentTxtField.text!)
                let mutation = InsertCommentPostMutation(postId: self.postId, classId: self.classId, inputPostComment: self.inputComment)
                //            KVNProgress.show()
                apollo.perform(mutation: mutation) { result, error in
                    if result!.data?.insertCommentPost?.error == false{
                            self.isposting = false
                        self.commentTxtField.text = ""
                        self.commentTxtField.resignFirstResponder()
                        self.reloadComments()
                    }else{
                            self.isposting = false
                        KVNProgress.showError(withStatus: result?.data?.insertCommentPost?.message)
                        self.commentSendBtn.setImage(UIImage(named: "001-bear-pawprint"), for: .normal)
                        self.commentSendBtn.isEnabled = true
                    }
                }
            }
        }
     
    }
    
    func uploadImage( teacherId: String, image:UIImage, completion: @escaping (Bool,Any) -> Swift.Void) {
        let baseurl = SettingsViewController.cdioFileUrl
        let parameters = ["userId":teacherId,"path":self.path]
        let URL = "\(baseurl)fileUpload"
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(image.jpegData(compressionQuality: 0.1)!, withName: "file", fileName: "image\(Int(Date().timeIntervalSince1970)).jpg", mimeType: "jpeg")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to:URL)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    //                        let progressPercent = Int(Progress.fractionCompleted*100)
                    //                        KVNProgress.show(withStatus: "Зураг хуулж байна \(progressPercent)%", on: self.view)
                    KVNProgress.show(withStatus: "Зураг хуулж байна. Түр хүлээнэ үү", on: self.view)
                })
                upload.responseJSON { response in
                    if let JSON = response.result.value {
                        let res = JSON
                        completion(true, res)
                    }
                    else{
                    }
                }
            case .failure(let encodingError):
                //self.delegate?.showFailAlert()
                print("Message - encode: \(encodingError)")
            }
        }
      
    }
    
    func getSubUser(){
        let query = GetsubUserQuery.init(userId: self.userId)
        apollo.fetch(query: query) { result, error in
            if let path =  result?.data?.subUser?.path{
                self.path = path
            }
        }
    }
    
    @objc func backtoRoot(){
        if self.isposting == false{
             self.navigationController?.popViewController(animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
   
        if indexPath.section == 1{
            self.selectedFileIndex = indexPath.row
            self.performSegue(withIdentifier: "toLink", sender: self)
        }else if indexPath.section == 2{
            self.selectedFileIndex = indexPath.row
            self.selectedSection = indexPath.section
            self.performSegue(withIdentifier: "fileview", sender: self)
        }else if indexPath.section == 3 {
            if self.comments[indexPath.row].ownerId?.id == self.userId{
                if self.comments[indexPath.row].file!.count > 0{
                    self.selectedSection = indexPath.section
                    self.selectedFileIndex = indexPath.row
                    self.editComment(index: indexPath.row)

                }else{
                    self.selectedSection = indexPath.section
                    self.selectedFileIndex = indexPath.row
                     self.editComment(index: indexPath.row)
                }
               
            }else{
                if self.postOwnerId == self.userId {
                    self.selectedSection = indexPath.section
                    self.selectedFileIndex = indexPath.row
                    self.editCommentByPostOwner(index: indexPath.row)
                }else{
                    if self.comments[indexPath.row].file!.count > 0{
                        self.selectedFileIndex = indexPath.row
                        self.selectedSection = indexPath.section
                        self.performSegue(withIdentifier: "fileview", sender: self)
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return 1
        }else if section == 1{
             return self.links.count
        }else if section == 2{
             return self.files.count
        }else{
            return self.comments.count
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (range.location == 0 && string == " ") {
            return false
        }else{
            return true
        }
    }
    
    @objc func textFieldDidChange(textField:UITextField){
        if (self.commentTxtField.text?.count)! > 0{
            // string contains non-whitespace characters
            self.commentSendBtn.setImage(UIImage(named: "001-bear-pawprint"), for: .normal)
            self.commentSendBtn.isEnabled = true
            
        }else{
            self.commentSendBtn.setImage(UIImage(named: "001-bear-pawprint-saaral"), for: .normal)
            self.commentSendBtn.isEnabled = false
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     
       if  indexPath.section == 1 {
            return  60
        }else if indexPath.section == 2{
            return 60
        }else if indexPath.section == 3 {
            if self.comments[indexPath.row].file!.count > 0{
                 return 210
            }else{
                 return  UITableView.automaticDimension
            }
       }else{
         return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.tableview.rowHeight = UITableView.automaticDimension
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "coment", for: indexPath) as! CommentCell
             cell.topicLabel.backgroundColor = UIColor.white
            cell.initcell()
            
            if self.topic  != ""{
                     cell.topicLabel.backgroundColor = GlobalVariables.black
                     cell.topicLabel.isHidden = false
                     cell.topicLabel.text = "    \(self.topic)   ".uppercased()
                    
                     cell.topicLabel.layer.cornerRadius = 5
                     cell.topicLabel.layer.masksToBounds = true
                     cell.topicLabel.textColor = .white
                }else{
                     cell.topicLabel.isHidden = true
                     cell.topicLabel.backgroundColor = UIColor.white
                }
            
            var lastlogin = NSDate()
            if self.postDate != nil {
                lastlogin = NSDate(timeIntervalSince1970: (self.postDate!/1000))
            }
            self.dateformatter.dateStyle = .short
            self.dateformatter.timeStyle = .short
            let last = self.dateformatter.string(from: lastlogin as Date)
            cell.dateLabel.text = last
           
            cell.nameLabel.text = self.posterLastName
            cell.lastnameLabel.text = self.posterName
            cell.contentLabel.text = self.postContent
            if self.posterImg == ""{
                 cell.proImg.image = UIImage(named: "circle-avatar")
            }else{
                 cell.proImg.sd_setImage(with:URL(string: "\(SettingsViewController.fileurl)\(self.posterImg)"),placeholderImage: UIImage(named: "circle-avatar"))
            }
            return cell
        }else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "filecell", for: indexPath) as! FileCell
            cell.contentView.backgroundColor = GlobalVariables.saaral
            cell.separatorInset = .init()
            cell.label.text = self.links[indexPath.row]
            cell.imgview.image = UIImage(named: "hyperlink")
            if self.comments.count != 0{
            
                self.tableview.separatorStyle = .singleLine
            }
            return cell
        }else if indexPath.section == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "filecell", for: indexPath) as! FileCell
            cell.contentView.backgroundColor = GlobalVariables.saaral
            cell.separatorInset = .init()
            if self.comments.count != 0{
//                self.tableview.separatorStyle = .singleLine
            }
            
            cell.label.text = self.files[indexPath.row].name ?? ""
           
            let thumb = "\(SettingsViewController.cdioFileUrl)\(self.files[indexPath.row].thumbnail ?? "")"
            let ext = self.files[indexPath.row].extension!
            
            switch(ext){
            case ".jpg":
                cell.imgview.sd_setImage(with:URL(string: thumb),placeholderImage: UIImage(named: "loading2"))
            case ".png":
                cell.imgview.sd_setImage(with:URL(string: thumb),placeholderImage: UIImage(named: "loading2"))
            case ".pdf":
                cell.imgview.image = UIImage(named: "pdf")
            case ".doc":
                cell.imgview.image = UIImage(named: "doc")
            case ".docx":
                cell.imgview.image = UIImage(named: "doc")
            case ".mov":
                cell.imgview.image = UIImage(named: "mov")
            case ".MOV":
                cell.imgview.image = UIImage(named: "mov")
            case ".mp4":
                cell.imgview.sd_setImage(with:URL(string: thumb),placeholderImage: UIImage(named: "loading2"))
            case ".MP4":
                cell.imgview.sd_setImage(with:URL(string: thumb),placeholderImage: UIImage(named: "loading2"))
            default:
                cell.imgview.image = UIImage(named: "clip")
            }
            return cell
        }else{
            
            self.tableview.separatorStyle = .none
            let cell = tableView.dequeueReusableCell(withIdentifier: "commentList", for: indexPath) as! CommentListCell
            cell.initcell()
            
            if self.comments[indexPath.row].file!.count > 0 {
                cell.attachedImg.isHidden = false
                let commImg = "\(SettingsViewController.cdioFileUrl)\(self.comments[indexPath.row].file![0]?.url ?? "")"
                cell.attachedImg.sd_setImage(with:URL(string: commImg),placeholderImage: UIImage(named: "loading2"))
            }else{
                cell.attachedImg.isHidden = true
            }
            
            let nickname = self.comments[indexPath.row].ownerId!.nickname ?? ""
            let lastname = self.comments[indexPath.row].ownerId!.lastname ?? ""
            let lastlogin = NSDate(timeIntervalSince1970: (self.comments[indexPath.row].postedDate!/1000))
            self.dateformatter.dateStyle = .short
            self.dateformatter.timeStyle = .short
            let last = self.dateformatter.string(from: lastlogin as Date)
            cell.dataLabel.text = last
            let img = self.comments[indexPath.row].ownerId!.image ?? ""
            if img == ""{
                cell.imgView.image = UIImage(named: "circle-avatar")
            }else{
                cell.imgView.sd_setImage(with:URL(string: "\(SettingsViewController.fileurl)\(img)"),placeholderImage: UIImage(named: "circle-avatar"))
            }
            cell.namelabel.text = lastname
            cell.lastnameLabel.text = nickname
            cell.contentLabel.text = self.comments[indexPath.row].content!
           
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let title : UILabel = UILabel()
        title.textColor = UIColor.white
   
            if section == 3 {
                title.textColor = UIColor.white
                title.text =  "Сэтгэгдэл"
            }
 
        return title.text
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.white
        
        let headerLabel = UILabel(frame: CGRect(x: 40, y: 5, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height-5))
        headerLabel.font = UIFont(name: "segoeui", size: 14)
        headerLabel.textColor = UIColor.gray
        headerLabel.text = self.tableView(self.tableview, titleForHeaderInSection: section)
        headerLabel.sizeToFit()
        
        let icon = UIImageView()
        icon.frame = CGRect(x:12,y:4,width:20,height:20)
        icon.layer.cornerRadius = 10
        icon.layer.masksToBounds = true
        icon.contentMode = .scaleAspectFit
        icon.image = UIImage(named: "comment")
        
        if section == 3 {
             headerView.addSubview(headerLabel)
             headerView.addSubview(icon)
        }
       
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 3{
            return 40
        }
        return 0
    }
    
    func getComments(){
         _ = self.apollo.clearCache()
         let query = PostCommentsQuery.init(postId: self.postId, classId: self.classId, userId: self.userId)
         KVNProgress.show()
         apollo.fetch(query: query) { result, error in
            KVNProgress.dismiss()
//            print("res: ", result?.data as Any )
            if result?.data?.postComments != nil {
                self.comments = (result?.data?.postComments)! as! [PostCommentsQuery.Data.PostComment]
//                print("res;:", self.comments)
//                if self.comments.count > 0 {
//                    for i in 0..<self.comments.count {
//                        self.commentFiles = self.comments[i].file! as! [PostCommentsQuery.Data.PostComment.File]
//                    }
//                }
                self.tableview.reloadData()
            }else{
                
            }
        }
    }
    
    func reloadComments(){
     _ = self.apollo.clearCache()
        let query = PostCommentsQuery.init(postId: self.postId, classId: self.classId, userId: self.userId)
        KVNProgress.show()
        apollo.fetch(query: query) { result, error in
              KVNProgress.dismiss()
            //            print("res: ", result?.data as Any )
            if result?.data?.postComments != nil {
                self.comments = (result?.data?.postComments)! as! [PostCommentsQuery.Data.PostComment]
                self.tableview.reloadData()
                if self.comments.count > 0 {
                    self.tableview.scrollToRow(at: IndexPath.init(row: self.comments.count - 1, section: 3), at: .bottom, animated: false)
                }
            }else{

            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            self.commentTxtField.resignFirstResponder()
            return false
        }
        return true
    }
    
     func editComment(index : Int){
        self.selectedIndex = index
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Засварлах", style: .default, handler: { action in
           self.performSegue(withIdentifier: "updatecomment", sender: self)
        }))
        
        alert.addAction(UIAlertAction(title: "Устгах", style: .default, handler:{action in
            let subAlert = UIAlertController(title: nil, message: "Устгахдаа итгэлтэй байна уу", preferredStyle: .alert)
            subAlert.addAction(UIAlertAction(title:"Тийм", style: .default, handler: { (action) in
              let commId = self.comments[index].id!
                
                let mutation = DeletePostMutation( postId: commId, classId: self.classId, userId: self.userId)
                self.apollo.perform(mutation: mutation) { result, error in

                    if result?.data?.deletePost?.error == false {
                        KVNProgress.showSuccess(withStatus: "Амжилттай устлаа")
                        self.getComments()
                    }else{
                        KVNProgress.showError(withStatus: result?.data?.deletePost?.message)
                    }
//                    print("error:: ",result?.data?.deletePost?.error as Any)
                }
            }))
            subAlert.addAction(UIAlertAction(title: "Үгүй", style: .cancel, handler: nil))
            self.present(subAlert,animated: true)
        }))
        
        if self.comments[self.selectedIndex].file!.count > 0 {
            alert.addAction(UIAlertAction(title: "Томруулж харах", style: .default, handler: { action in
                GlobalStaticVar.isPostCreate = false
                self.performSegue(withIdentifier: "fileview", sender: self)
            }))
        }
        alert.addAction(UIAlertAction(title: "Болих", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "updatecomment"{
            let controller = segue.destination as! updateCommentVC
            controller.classId = self.classId
            controller.content = self.comments[selectedIndex].content!
            controller.postId = self.comments[selectedIndex].id!
            if self.comments[selectedIndex].file != nil {
                controller.commentFiles = self.comments[selectedIndex].file! as! [PostCommentsQuery.Data.PostComment.File]
            }
        }else if segue.identifier == "fileview"{
            let PV = segue.destination as! PageViewController
            if self.selectedSection == 2 {
                PV.urls = [self.files[selectedFileIndex].url!]
                PV.sFileUrl = self.files[selectedFileIndex].url!
                PV.sMsgType = "document"
            }else{
                PV.urls = [self.comments[selectedFileIndex].file![0]?.url!] as! [String]
                PV.sFileUrl = self.comments[selectedFileIndex].file![0]?.url ?? ""
                PV.sMsgType = "document"
            }
           
        }else if segue.identifier == "updatePost"{
            let controller = segue.destination as! UpdatePostVC
            controller.content = self.postContent
            //todo here is error then commented
            if self.links.count > 0 {
                  controller.links = self.links
            }
            controller.selectedTopicId = self.topic
            controller.files = self.files
            controller.posterNickname = self.posterName
            controller.posterLastname = self.posterLastName
            controller.postedDate = self.postDate
            controller.classId = self.classId
            controller.postId = self.postId
            controller.teacherId = self.postOwnerId
            controller.posterImg = self.posterImg
        }else if segue.identifier == "toLink" {
             let controller = segue.destination as! LinkLoadVC
            controller.link = self.links[selectedFileIndex]
        }
    }

     func numberOfSections(in tableView: UITableView) -> Int {
        if self.comments.count == 0{
            return 3
        }
        return 4
    }
    
    func authorisationStatus(attachmentTypeEnum: AttachmentType, vc: UIViewController){

        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            if attachmentTypeEnum == AttachmentType.camera{
                openCamera()
            }
            if attachmentTypeEnum == AttachmentType.photoLibrary{
                photoLibrary()
            }
    
        case .denied:
            print("permission denied")
            self.addAlertForSettings(attachmentTypeEnum)
        case .notDetermined:
            print("Permission Not Determined")
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == PHAuthorizationStatus.authorized{
                    // photo library access given
                    print("access given")
                    if attachmentTypeEnum == AttachmentType.camera{
                        self.openCamera()
                    }
                    if attachmentTypeEnum == AttachmentType.photoLibrary{
                        self.photoLibrary()
                    }
                  
                }else{
                    print("restriced manually")
                    self.addAlertForSettings(attachmentTypeEnum)
                }
            })
        case .restricted:
            print("permission restricted")
            self.addAlertForSettings(attachmentTypeEnum)
        default:
            break
        }
    }
    
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .camera
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    //MARK: - PHOTO PICKER
    func photoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .photoLibrary
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    //MARK: - SETTINGS ALERT
    func addAlertForSettings(_ attachmentTypeEnum: AttachmentType){
        var alertTitle: String = ""
        if attachmentTypeEnum == AttachmentType.camera{
            alertTitle = "App does not have access to your camera. To enable access, tap settings and turn on Camera."
        }
        if attachmentTypeEnum == AttachmentType.photoLibrary{
            alertTitle = "App does not have access to your photos. To enable access, tap settings and turn on Photo Library Access."
        }
        
        let cameraUnavailableAlertController = UIAlertController (title: alertTitle , message: nil, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title:"Settings", style: .destructive) { (_) -> Void in
            let settingsUrl = NSURL(string:UIApplication.openSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            }
        }
        let cancelAction = UIAlertAction(title:"Settings", style: .default, handler: nil)
        cameraUnavailableAlertController .addAction(cancelAction)
        cameraUnavailableAlertController .addAction(settingsAction)
        self.present(cameraUnavailableAlertController , animated: true, completion: nil)
    }
    
    func editCommentByPostOwner(index: Int){
        self.selectedIndex = index
//        let gg = self.comments[index].ownerId?.id ?? ""
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Устгах", style: .default, handler:{action in
            let subAlert = UIAlertController(title: nil, message: "Устгахдаа итгэлтэй байна уу", preferredStyle: .alert)
            subAlert.addAction(UIAlertAction(title:"Тийм", style: .default, handler: { (action) in
                let commId = self.comments[index].id!
                
//                print("commIDLL ", commId)
//                print("classId ", self.classId)
//                print("userId ", gg)
                
                let mutation = DeletePostMutation( postId: commId, classId: self.classId, userId: self.userId)
                self.apollo.perform(mutation: mutation) { result, error in
//                    print("result:: " , result)
                    if result?.data?.deletePost?.error == false {
                        KVNProgress.showSuccess(withStatus: "Амжилттай устлаа")
                        self.getComments()
                    }else{
                        KVNProgress.showError(withStatus: result?.data?.deletePost?.message)
                    }
                    //  print("error:: ",result?.data?.deletePost?.error as Any)
                }
            }))
            subAlert.addAction(UIAlertAction(title: "Үгүй", style: .cancel, handler: nil))
            self.present(subAlert,animated: true)
        }))
        if self.comments[self.selectedIndex].file!.count > 0 {
            alert.addAction(UIAlertAction(title: "Томруулж харах", style: .default, handler: { action in
                GlobalStaticVar.isPostCreate = false
                self.performSegue(withIdentifier: "fileview", sender: self)
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Болих", style: .cancel, handler: nil))
        self.present(alert, animated: true)
        
    }
}

extension CommentsVC {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
      
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        if let videoURL = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.mediaURL)] as? URL {
            CameraView.visible = 0
            print("videoURL :: ", videoURL)
        }else if let pickedImage1 = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            CameraView.visible = 0
            self.pikedImage = pickedImage1
            self.isPicked = true
            self.attachImageBtn.setImage(pickedImage1, for: .normal)
            self.commentSendBtn.setImage(UIImage(named: "001-bear-pawprint"), for: .normal)
            self.commentSendBtn.isEnabled = true
        }else if let pickedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as? UIImage {
             CameraView.visible = 0
             self.pikedImage = pickedImage
             self.isPicked = true
             self.attachImageBtn.setImage(pickedImage, for: .normal)
            self.commentSendBtn.setImage(UIImage(named: "001-bear-pawprint"), for: .normal)
            self.commentSendBtn.isEnabled = true
        }
        self.tableview.reloadData()
        picker.dismiss(animated: true, completion: nil)
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
        return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
        return input.rawValue
    }

}

