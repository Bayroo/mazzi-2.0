//
//  EditTopicVC.swift
//  Mazzi
//
//  Created by Bayara on 1/23/19.
//  Copyright © 2019 woovoo. All rights reserved.
//

import Foundation
import Apollo
import KVNProgress

class EditTopicVC : UIViewController,UITextFieldDelegate,UITextViewDelegate{
    var myId = SettingsViewController.userId
    var classId = GlobalStaticVar.classId
    var editTopicId = ""
    var editedTopicName = ""
    var topics = [ObjectTopicsQuery.Data.ObjectTopic.Result]()
    @IBOutlet weak var txtfield: UITextField!
    @IBOutlet weak var txtview: UITextView!
    @IBOutlet weak var saveBtn: UIButton!
    var apollo: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken]
        let url = URL(string:SettingsViewController.graphQL)!
        
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customize()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        GlobalVariables.isUpdateTopic = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.topics = []
        self.txtfield.becomeFirstResponder()
        if GlobalVariables.isUpdateTopic == true{
            self.saveBtn.setTitle("Засах", for: .normal)
            self.txtview.isHidden = false
            self.txtview.text = self.editedTopicName
        }else{
            self.txtview.isHidden = true
            self.txtfield.text = ""
            self.saveBtn.setTitle("Хадгалах", for: .normal)
        }
    }
    
    func customize(){
        self.navigationItem.leftBarButtonItem  = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(backtoRoot))
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        self.txtfield.delegate = self
        self.txtfield.layer.borderWidth = 1
        self.txtfield.layer.borderColor = GlobalVariables.todpurple.cgColor
        self.txtfield.layer.cornerRadius = 5
        
        self.txtview.delegate = self
        self.txtview.layer.borderWidth = 1
        self.txtview.layer.borderColor = GlobalVariables.todpurple.cgColor
        self.txtview.layer.cornerRadius = 5
        
        self.saveBtn.layer.cornerRadius = 5
        self.saveBtn.backgroundColor = GlobalVariables.todpurple
        self.saveBtn.setTitleColor(UIColor.white, for: .normal)
    }
    @objc func dismissKeyboard (){
        self.txtfield.resignFirstResponder()
        self.txtview.resignFirstResponder()
    }
    
    @objc func backtoRoot(){
        GlobalVariables.isUpdateTopic = false
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveTopic(_ sender: Any) {
        if GlobalVariables.isUpdateTopic == true{
            self.updateTopic()
        }else{
             self.saveTopic()
        }
       
    }
    
    func saveTopic(){
        if self.txtfield.text != "" && self.txtfield.text != " " {

            let mutation = InsertTopicMutation(userId: self.myId, classId: self.classId, name: self.txtfield.text!)
            self.apollo.perform(mutation: mutation) { result, error in
                if result?.data?.insertTopic?.error == false{
                    KVNProgress.showSuccess(withStatus: "Амжилттай")
                    self.backtoRoot()
                }else{
                    KVNProgress.showError(withStatus: result?.data?.insertTopic?.message)
                }
            }

        }else{
            KVNProgress.showError(withStatus: "Сэдэв оруулна уу")
        }
    }
    
    func updateTopic(){
        
        let inpput = InputObjectTopic(name: self.txtview.text)
        let mutation = UpdateObjectTopicMutation(userId: self.myId, topicId: self.editTopicId, InputObjectTopic: inpput)

        self.apollo.perform(mutation: mutation) { result, error in
            if result?.data?.updateObjectTopic?.error == false{
                KVNProgress.showSuccess(withStatus: "Амжилттай")
                self.backtoRoot()
            }else{
                KVNProgress.showError(withStatus: result?.data?.updateObjectTopic?.message)
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (range.location == 0 && string == " ") {
            return false
        }else{
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.txtfield.resignFirstResponder()
        return true
    }
    
    private func textField(_ textField: UITextField, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            self.txtfield.resignFirstResponder()
            return false
        }
        return true
    }
}
