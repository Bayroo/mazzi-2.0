//
//  TermsofUseVC.swift
//  Mazzi
//
//  Created by Janibekm on 12/7/17.
//  Copyright © 2017 Mazzi App LLC. All rights reserved.
//

import UIKit

class TermsofUseVC: UIViewController{
var phone = ""
var countCode = ""
var inputnumber = ""
    
    @IBOutlet weak var textview: UITextView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.textview.layer.borderWidth = 0.0
        self.textview.layer.borderColor = UIColor.black.cgColor
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(baktoroot))
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Үйлчилгээний нөхцөл"
    }
    
    override func viewDidAppear(_ animated: Bool) {
       
    }
    
    override func viewWillDisappear(_ animated: Bool) {
         self.navigationController?.navigationBar.isHidden = true
    }
    @objc func baktoroot(){
        self.navigationController?.popViewController(animated: false)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
