//
//  CorpListVC.swift
//  Mazzi
//
//  Created by Bayara on 5/20/19.
//  Copyright © 2019 Mazz Application. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import KVNProgress
import SwiftyJSON
import Apollo

class CorpListVC: UIViewController ,UITableViewDelegate, UITableViewDataSource{
    @IBOutlet weak var corpTableView: UITableView!
    
    var userId = SettingsViewController.userId
    var corpList = [UsersCorporateMemberListClassPopulatedQuery.Data.UsersCorporateMemberListClassPopulated.Result]()
    let apollos: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        // Add additional headers as needed
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
        //        print("TOKEN APOLLOO:: ", GlobalVariables.headerToken)
        let url = URL(string:SettingsViewController.graphQL)!
        
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Байгууллагууд"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(backtoRoot))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "nemehBTN"), style: .plain, target: self, action: #selector(sendReq))
        self.corpTableView.delegate = self
        self.corpTableView.dataSource = self
        self.corpTableView.register((UINib(nibName: "corpcell", bundle: nil)), forCellReuseIdentifier: "corp")
        getFireUrl(url: "sss") { (suc) in
            print("suc")
        }
        
        self.getCorpList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.userId = SettingsViewController.userId
        
    }
    
    @objc func backtoRoot(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func getCorpList(){
          print("GRAPHQL URL :: ",SettingsViewController.graphQL)
        let query = UsersCorporateMemberListClassPopulatedQuery(userId: self.userId)
        print(" userID:", self.userId)
         self.apollos.fetch(query: query) { result, error in
//             print("BLABAL:: ", result)
            if result?.data?.usersCorporateMemberListClassPopulated?.error == false {
                
                self.corpList = result?.data?.usersCorporateMemberListClassPopulated?.result as! [UsersCorporateMemberListClassPopulatedQuery.Data.UsersCorporateMemberListClassPopulated.Result]
//                print("RESULT::: ", self.corpList)
                self.corpTableView.reloadData()
            }else{
            }
        }
    }
    
    @objc func sendReq(){
//         self.performSegue(withIdentifier: "joinreq", sender: self)
         let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "request") as? ClassRequestVC
         self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.corpList.count == 0 {
//            self.performSegue(withIdentifier: "joinreq", sender: self)
             self.sendReq()
        }else{
           
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "classlist") as? MClassListVC
            viewController?.roleId = self.corpList[indexPath.row].corporateMember?.corporateMemberPositionId ?? ""
            viewController?.corpId = self.corpList[indexPath.row].corporate?.id ?? ""
            if self.corpList[indexPath.row].classes != nil {
                 viewController?.allClassList = self.corpList[indexPath.row].classes! as! [UsersCorporateMemberListClassPopulatedQuery.Data.UsersCorporateMemberListClassPopulated.Result.Class]
            }
            self.navigationController?.pushViewController(viewController!, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.corpList.count == 0 {
             return 1
        }else{
             return self.corpList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "corp", for: indexPath) as! corpCell
        if self.corpList.count == 0 {
            cell.corpNameLabel.text = "Байгууллагад орох хүсэлт явуулах"
        }else{
            cell.corpNameLabel.text = self.corpList[indexPath.row].corporate?.name ?? ""
        }
       
        return cell
    }
}
