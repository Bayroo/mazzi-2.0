//
//  StudentIdRegVC.swift
//  Mazzi
//
//  Created by Bayara on 2/1/19.
//  Copyright © 2019 woovoo. All rights reserved.
//

import Foundation
import Apollo
import KVNProgress

class StudentIdRegVC:UIViewController,UITextFieldDelegate{
    
    @IBOutlet weak var regBtn: UIButton!
    @IBOutlet weak var txtfield: UITextField!
    var myStudentId = ""
    var myId = SettingsViewController.userId
   let apollos: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        // Add additional headers as needed
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
        //        print("TOKEN APOLLOO:: ", GlobalVariables.headerToken)
        let url = URL(string:SettingsViewController.graphQL)!
        
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "05"), style: .plain, target: self, action: #selector(back))
        self.txtfield.delegate = self
        self.txtfield.layer.cornerRadius = 5
        self.txtfield.layer.borderWidth = 1
        self.txtfield.layer.borderColor = GlobalVariables.purple.cgColor
        self.regBtn.layer.cornerRadius = 5
        self.regBtn.backgroundColor = GlobalVariables.purple
        self.regBtn.setTitleColor(UIColor.white, for: .normal)
        self.regBtn.layer.masksToBounds = true
        self.regBtn.addTarget(self, action: #selector(SetStudentId), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.txtfield.becomeFirstResponder()
        if GlobalVariables.isUpdateStudentId == true{
            self.txtfield.text = self.myStudentId
            self.regBtn.setTitle("Засах", for:.normal)
             self.title = "Оюутны код засах"
        }else{
             self.title = "Оюутны код бүртгүүлэх"
            self.txtfield.text = ""
            self.regBtn.setTitle("Бүртгүүлэх", for:.normal)
        }
    }
    
    @objc func back(){
        self.txtfield.resignFirstResponder()
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func SetStudentId(){
        let mutation = InsertStudentIdMutation(userId: self.myId, studentIdCode: self.txtfield.text!)
        self.apollos.perform(mutation: mutation) { result, error in
            if result?.data?.insertStudentIdCode?.error == false{
                if GlobalVariables.isUpdateStudentId == true{
                     KVNProgress.showSuccess(withStatus: "Амжилттай засагдлаа")
                     GlobalVariables.isUpdateStudentId = false
                }else{
                     KVNProgress.showSuccess(withStatus: "Амжилттай бүртгэгдлээ")
                }
                self.back()
            }else{
                KVNProgress.showError(withStatus: result?.data?.insertStudentIdCode?.message)
            }
        }
    }
}
