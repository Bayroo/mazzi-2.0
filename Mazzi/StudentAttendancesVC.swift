//
//  StudentAttendancesVC.swift
//  Mazzi
//
//  Created by Bayara on 1/11/19.
//  Copyright © 2019 woovoo. All rights reserved.
//

import Foundation
import Apollo
import KVNProgress
import SwiftyJSON
class StudentAttendancesVC : UIViewController,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource{
   
    

    @IBOutlet weak var tableview: UITableView!
    var classId = GlobalStaticVar.classId
    var topic = [UsersAttendancesQuery.Data.UsersAttandance.TopicId]()
    var myId = ""
    var attendanceId = ""
    var refrechControl = UIRefreshControl()
    var dateformatter = DateFormatter()
    var endTime : Double!
    var lists = [UsersAttendancesQuery.Data.UsersAttandance]()
    var att = UsersAttendancesQuery.Data.UsersAttandance.Attendance()
    var LateMinSum = 0
    var teachers = [ClassUsersListQuery.Data.ClassesUserListClassId]()
    var AllusersList = [ClassUsersListQuery.Data.ClassesUserListClassId]()
    var allTopics = [ClassTopicsQuery.Data.ClassTopic.Result]()
    var filteredAttsByTopis = [UsersAttandancesByTopicIdQuery.Data.UsersAttandancesByTopicId]()
    var selectedTeacherId = ""
//    let myPicker = UIPickerView()
    var apollo: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        // Add additional headers as needed
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
//      print("TOKEN APOLLOO:: ", GlobalVariables.headerToken)
        let url = URL(string:SettingsViewController.graphQL)!
        
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    var subview = UIView()
    var pickerTitle = UILabel()
    var closePicker = UIButton()
    var pickerView = UIPickerView()
    var isPickerShow = false
    var filterBtn = UIButton()
    var isFilterByName = false
    var isFilterByTopic = false
    var selectedTopicId = ""
    var filtered = [UsersAttandancesByTeacherIdQuery.Data.UsersAttandancesByTeacherId]()
//  var filterByTopic = [UsersAttandancesByTopicId.d]()
    var isFilterDone = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customize()
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        
    }
    
    func customize(){
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(back))
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissPicker)))
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "Миний ирц"
        self.tableview.delegate = self
        self.tableview.dataSource = self
        self.tableview.register((UINib(nibName: "StudentAttenceCell", bundle: nil)), forCellReuseIdentifier: "studentAttence")
        self.tableview.register((UINib(nibName: "headerCell", bundle: nil)), forCellReuseIdentifier: "headercell")
        self.tableview.refreshControl = self.refrechControl
        self.refrechControl.addTarget(self, action: #selector(reloadData), for: .valueChanged)
        
        self.subview.frame = CGRect(x:0, y:self.view.bounds.height,width: self.view.bounds.width, height:250)
        self.pickerTitle.frame = CGRect(x:self.view.bounds.width/2 - 75, y:4, width:150,height:20)
        self.pickerView.frame = CGRect(x:0,y:28,width:self.view.bounds.width,height:220)
        self.closePicker.frame = CGRect(x:4,y:4,width:20,height:20)
        self.filterBtn.frame = CGRect(x: self.view.bounds.width - 100, y: 0, width: 120, height: 30)
        self.filterBtn.layer.cornerRadius = 5
        self.filterBtn.layer.borderWidth = 0
        self.filterBtn.layer.borderColor = UIColor.white.cgColor
        self.filterBtn.setTitle("Шүүх", for: .normal)
        self.filterBtn.setTitleColor(UIColor.white, for: .normal)
        self.filterBtn.layer.masksToBounds = true
        self.filterBtn.titleLabel?.font = GlobalVariables.customFont12
        self.filterBtn.addTarget(self, action: #selector(self.filterbyName), for: .touchUpInside)
        self.subview.backgroundColor = GlobalVariables.todpurple
        self.pickerView.backgroundColor = UIColor.white
        self.pickerTitle.textAlignment = NSTextAlignment.center
        self.pickerTitle.text = ""
        self.pickerTitle.font = GlobalVariables.customFont12
        self.pickerTitle.textColor = UIColor.white
        self.closePicker.setImage(UIImage(named: "closewhite"), for: UIControl.State.normal)
        self.closePicker.addTarget(self, action: #selector(dismissPicker), for: .touchUpInside)
        
        self.subview.addSubview(self.pickerTitle)
        self.subview.addSubview(self.pickerView)
        self.subview.addSubview(self.closePicker)
        self.subview.addSubview(self.filterBtn)
        self.view.addSubview(self.subview)
        
    }
    
    @objc func dismissPicker(){
    
        if self.isPickerShow == true {
            UIView.transition(with: self.subview, duration: 0.3, options: .transitionCrossDissolve, animations: {
                self.isPickerShow = false
                self.subview.frame = CGRect(x:0, y:self.view.bounds.height,width: self.view.bounds.width, height:250)
            })
        }
    }
    
    @objc func showPickerTeacher(){
        self.isFilterByTopic = false
        self.pickerTitle.text = "Багш сонгох"
        if self.isFilterDone == true{
            self.reloadData()
        }else{
            if self.isPickerShow == false {
                if self.teachers.count > 0 {
                    self.selectedTeacherId = self.teachers[0].userId?.id ?? ""
                }
                self.isPickerShow = true
                self.isFilterByName = true
                self.pickerView.reloadAllComponents()
                UIView.transition(with: self.subview, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    self.subview.frame = CGRect(x:0, y: self.view.frame.height-250,width: self.view.bounds.width, height:250)
                })
                
            }else{
                self.dismissPicker()
            }
        }
    }
    
    @objc func showPickerTopic(){
         self.isFilterByName = false
         self.pickerTitle.text  = "Хичээл сонгох"
        
        if isFilterDone == true {
            self.reloadData()
        }else if self.isPickerShow == false {
            if self.allTopics.count > 0 {
                self.selectedTopicId = self.allTopics[0].id ?? ""
            }
            self.isPickerShow = true
            self.isFilterByTopic = true
            self.pickerView.reloadAllComponents()
            UIView.transition(with: self.subview, duration: 0.3, options: .transitionCrossDissolve, animations: {
                self.subview.frame = CGRect(x:0, y: self.view.frame.height-250,width: self.view.bounds.width, height:250)
            })
        }else{
            self.dismissPicker()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        print("LOLL ::", SettingsViewController.graphQL )
        checkInternet()
        super.viewWillAppear(animated)
        self.myId = SettingsViewController.userId
        self.tableview.isHidden = false
        self.getList()
        self.getTeachersList()
        self.getTopicList()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
       _ = self.apollo.clearCache()
    }
    
    @objc func reloadData(){
        self.isFilterDone = false
        self.isFilterByName = false
        self.isFilterByTopic = false
        self.filteredAttsByTopis.removeAll()
        self.filtered.removeAll()
        let query = UsersAttendancesQuery(userId: self.myId, classId: self.classId)
        self.apollo.fetch(query: query) { result, error in
            if result?.data?.usersAttandances != nil {
                let gg = (result?.data?.usersAttandances)! as! [UsersAttendancesQuery.Data.UsersAttandance]
                if gg.count > 0 {
                    for i in 0..<gg.count{
                        if gg[i].attendance != nil {
                            if !self.lists.contains(where:{$0.id == gg[i].id ?? ""}){
                                self.lists.append(gg[i])
                                self.tableview.reloadData()
                                self.refrechControl.endRefreshing()
                            }
                        }
                    }
                }
                self.checkLateMin()
                self.refrechControl.endRefreshing()
            }else{
                
            }
        }
    }
    
    @objc func back(){
        self.navigationController?.popViewController(animated: true)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        let title : UILabel = UILabel()
//        if section == 0 {
//            title.textColor = GlobalVariables.purple
//            title.text =  "Багш".uppercased()
//        }else if section == 1 {
//            title.textColor = GlobalVariables.purple
//            title.text =  "Оюутан \("(")\(self.acceptedUsers.count)\(")")".uppercased()
//        }else {
//            title.textColor = GlobalVariables.purple
//            title.text =  "Хүлээгдэж буй оюутнууд \("(")\(self.pending.count)\(")")".uppercased()
//        }
//        return title.text
//    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        if section == 0 {
            
        }else{
            
            headerView.backgroundColor =  GlobalVariables.todpurple
            let headerLabel = UILabel(frame: CGRect(x: 8, y: 5, width:
                120, height: 30))
            headerLabel.font = GlobalVariables.customFont14
            headerLabel.textColor = UIColor.white
            headerLabel.numberOfLines = 2
            headerLabel.lineBreakMode = .byWordWrapping
            headerLabel.text = "Ирцийн мэдээлэл"
            headerLabel.sizeToFit()
            headerView.addSubview(headerLabel)
            
            let headerLabel1 = UILabel(frame: CGRect(x: self.tableview.bounds.width - 128, y: 5, width:
                120, height: 30))
            headerLabel1.font =  GlobalVariables.customFont14
            headerLabel1.numberOfLines = 2
            headerLabel1.lineBreakMode = .byWordWrapping
            headerLabel1.textColor = UIColor.white
            headerLabel1.text = "Бүртгүүлсэн цаг"
            headerLabel1.sizeToFit()
            headerView.addSubview(headerLabel1)
     
        }
       return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1{
              return 30
        }else{
            return 0
        }
      
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
             return 100
        }else{
             return 70
        }
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
             return 1
        }else{
            if self.isFilterByName == true{
                return self.filtered.count
            }else if self.isFilterByTopic == true{
                return self.filteredAttsByTopis.count
            }else{
                return self.lists.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        print("isFilterByName::",self.isFilterByName)
//        print("isFilterByTopic::",self.isFilterByTopic)
//        self.checkLateMin()
        if indexPath.section == 0{
             let cell = tableView.dequeueReusableCell(withIdentifier: "headercell", for: indexPath) as! headerCell
             cell.initcell()
             cell.LateMinLabel.textColor = UIColor.red
             cell.LateMinLabel.text = "\(String(self.LateMinSum)) мин"
            if self.isFilterByName == true{
                cell.filterByNameBtn.setTitle("Бүгдийг харах", for: .normal)
                cell.filterByNameBtn.setTitleColor(UIColor.red, for: .normal)
            }else{
                cell.filterByNameBtn.setTitle("Багшийн нэрээр", for: .normal)
                 cell.filterByNameBtn.setTitleColor(UIColor.black, for: .normal)
            }
            
            if self.isFilterByTopic == true{
                cell.filterByTopicBtn.setTitle("Бүгдийг харах", for: .normal)
                cell.filterByTopicBtn.setTitleColor(UIColor.red, for: .normal)
            }else{
                cell.filterByTopicBtn.setTitle("Хичээлийн нэрээр", for: .normal)
                cell.filterByTopicBtn.setTitleColor(UIColor.black, for: .normal)
            }
            
            cell.filterByNameBtn.addTarget(self, action: #selector(showPickerTeacher), for: .touchUpInside)
            cell.filterByTopicBtn.addTarget(self, action: #selector(showPickerTopic), for: .touchUpInside)
            return cell
        }else{
            
            if self.isFilterByName == true {
                let cell = tableView.dequeueReusableCell(withIdentifier: "studentAttence", for: indexPath) as! StudentAttenceCell
                if indexPath.row % 2 == 0{
                    cell.backgroundColor = UIColor.white
                }else{
                    cell.backgroundColor = GlobalVariables.F5F5F5
                }

                cell.nickNameLabel.text = self.filtered[indexPath.row].userId?.lastname ?? ""
                cell.lastnameLabel.text = self.filtered[indexPath.row].userId?.nickname ?? ""
                
                let lastlogin = NSDate(timeIntervalSince1970: (self.filtered[indexPath.row].createdDate!/1000))
                self.dateformatter.dateStyle = .short
                self.dateformatter.timeStyle = .none
                let last = self.dateformatter.string(from: lastlogin as Date)
                cell.dateLabel.text = "\(last),"
                
                let startTime = NSDate(timeIntervalSince1970: ((self.filtered[indexPath.row].time?.timeBegan)!/1000))
                self.dateformatter.dateStyle = .none
                self.dateformatter.timeStyle = .short
                let started = self.dateformatter.string(from: startTime as Date)
                
                let endTime = NSDate(timeIntervalSince1970: ((self.filtered[indexPath.row].time?.timeEnds)!/1000))
                self.dateformatter.dateStyle = .none
                self.dateformatter.timeStyle = .short
                let ended = self.dateformatter.string(from: endTime as Date)
                
                cell.timeLabel.text = "\(started)-\(ended)"
                if self.filtered[indexPath.row].topicId?.name == "empty"{
                    cell.RadiusLabel.text = ""
                }else{
                    cell.RadiusLabel.text = self.filtered[indexPath.row].topicId?.name ?? ""  //String(self.lists[indexPath.row].radius!)
                }
//                print("gg:: ",self.filtered[indexPath.row].attendance)
                if self.filtered[indexPath.row].attendance?.checkedIn != 0 && self.filtered[indexPath.row].attendance?.checkedIn != nil{
                    let regtime = NSDate(timeIntervalSince1970: ((self.filtered[indexPath.row].attendance?.checkedIn)!/1000))
                    self.dateformatter.dateStyle = .none
                    self.dateformatter.timeStyle = .short
                    let reg = self.dateformatter.string(from: regtime as Date)
                    
                    let regtDouble = regtime.timeIntervalSince1970
                    let regtint = Int(regtDouble * 1000)
                    
                    let endtDouble = endTime.timeIntervalSince1970
                    let endtInt = Int(endtDouble * 1000)
                    if regtint > endtInt {
                        let latedMin = regtint - endtInt
                        let gg =  String(format: "%2d" ,latedMin/60000) //Double(latedMin/60000)
                        cell.RegTimeLabel.text = reg
                        cell.lateMinLabel.text = "-\(gg) мин"
                    }else{
                        cell.RegTimeLabel.text = reg
                    }
                }else{
                    if self.filtered[indexPath.row].attendance?.state == "" || self.filtered[indexPath.row].attendance?.state == " "{
                        cell.RegTimeLabel.text =  "-"
                    }else{
                        cell.RegTimeLabel.text = self.filtered[indexPath.row].attendance?.state ?? "-"
                    }
                }
                return cell
            }else if self.isFilterByTopic == true{
                let cell = tableView.dequeueReusableCell(withIdentifier: "studentAttence", for: indexPath) as! StudentAttenceCell
                if indexPath.row % 2 == 0{
                    cell.backgroundColor = UIColor.white
                }else{
                    cell.backgroundColor = GlobalVariables.F5F5F5
                }
                cell.nickNameLabel.text = self.filteredAttsByTopis[indexPath.row].userId?.lastname ?? ""
                cell.lastnameLabel.text = self.filteredAttsByTopis[indexPath.row].userId?.nickname ?? ""
              
                let lastlogin = NSDate(timeIntervalSince1970: (self.filteredAttsByTopis[indexPath.row].createdDate!/1000))
                self.dateformatter.dateStyle = .short
                self.dateformatter.timeStyle = .none
                let last = self.dateformatter.string(from: lastlogin as Date)
                cell.dateLabel.text = "\(last),"
                
                let startTime = NSDate(timeIntervalSince1970: ((self.filteredAttsByTopis[indexPath.row].time?.timeBegan)!/1000))
                self.dateformatter.dateStyle = .none
                self.dateformatter.timeStyle = .short
                let started = self.dateformatter.string(from: startTime as Date)
                
                let endTime = NSDate(timeIntervalSince1970: ((self.filteredAttsByTopis[indexPath.row].time?.timeEnds)!/1000))
                self.dateformatter.dateStyle = .none
                self.dateformatter.timeStyle = .short
                let ended = self.dateformatter.string(from: endTime as Date)
                
                cell.timeLabel.text = "\(started)-\(ended)"
                if self.filteredAttsByTopis[indexPath.row].topicId?.name == "empty"{
                    cell.RadiusLabel.text = ""
                }else{
                    cell.RadiusLabel.text = self.filteredAttsByTopis[indexPath.row].topicId?.name ?? ""  //String(self.lists[indexPath.row].radius!)
                } //String(self.lists[indexPath.row].radius!)
                
                if self.filteredAttsByTopis[indexPath.row].attendance?.checkedIn != 0 && self.filteredAttsByTopis[indexPath.row].attendance?.checkedIn != nil {
                    let regtime = NSDate(timeIntervalSince1970: ((self.filteredAttsByTopis[indexPath.row].attendance?.checkedIn)!/1000 ))
                    self.dateformatter.dateStyle = .none
                    self.dateformatter.timeStyle = .short
                    let reg = self.dateformatter.string(from: regtime as Date)
                    
                    let regtDouble = regtime.timeIntervalSince1970
                    let regtint = Int(regtDouble * 1000)
                    
                    let endtDouble = endTime.timeIntervalSince1970
                    let endtInt = Int(endtDouble * 1000)
                    if regtint > endtInt {
                        let latedMin = regtint - endtInt
                        let gg =  String(format: "%2d" ,latedMin/60000) //Double(latedMin/60000)
                        cell.RegTimeLabel.text = reg
                        cell.lateMinLabel.text = "-\(gg) мин"
                    }else{
                        cell.RegTimeLabel.text = reg
                    }
                }else{
                    if self.filteredAttsByTopis[indexPath.row].attendance?.state == "" || self.filteredAttsByTopis[indexPath.row].attendance?.state == " "{
                        cell.RegTimeLabel.text =  "-"
                    }else{
                        cell.RegTimeLabel.text = self.filteredAttsByTopis[indexPath.row].attendance?.state ?? "-"
                    }
                }
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "studentAttence", for: indexPath) as! StudentAttenceCell
                if indexPath.row % 2 == 0{
                    cell.backgroundColor = UIColor.white
                }else{
                    cell.backgroundColor = GlobalVariables.F5F5F5
                }
                cell.nickNameLabel.text = self.lists[indexPath.row].userId?.lastname ?? ""
                cell.lastnameLabel.text = self.lists[indexPath.row].userId?.nickname ?? ""
                
                let lastlogin = NSDate(timeIntervalSince1970: (self.lists[indexPath.row].createdDate!/1000))
                self.dateformatter.dateStyle = .short
                self.dateformatter.timeStyle = .none
                let last = self.dateformatter.string(from: lastlogin as Date)
                cell.dateLabel.text = "\(last),"
                
                let startTime = NSDate(timeIntervalSince1970: ((self.lists[indexPath.row].time?.timeBegan)!/1000))
                self.dateformatter.dateStyle = .none
                self.dateformatter.timeStyle = .short
                let started = self.dateformatter.string(from: startTime as Date)
                
                let endTime = NSDate(timeIntervalSince1970: ((self.lists[indexPath.row].time?.timeEnds)!/1000))
                self.dateformatter.dateStyle = .none
                self.dateformatter.timeStyle = .short
                let ended = self.dateformatter.string(from: endTime as Date)
                
                cell.timeLabel.text = "\(started)-\(ended)"
                if self.lists[indexPath.row].topicId?.name == "empty"{
                    cell.RadiusLabel.text = ""
                }else{
                    cell.RadiusLabel.text = self.lists[indexPath.row].topicId?.name ?? ""  //String(self.lists[indexPath.row].radius!)
                }
                
//                print("----------------------------------------------------")
//                print("LLISIOHKSEFBE::: ",self.lists[indexPath.row].attendance )
                if self.lists[indexPath.row].attendance?.checkedIn != 0 && self.lists[indexPath.row].attendance?.checkedIn != nil{
                    let regtime = NSDate(timeIntervalSince1970: ((self.lists[indexPath.row].attendance?.checkedIn)!/1000))
                    self.dateformatter.dateStyle = .none
                    self.dateformatter.timeStyle = .short
                    let reg = self.dateformatter.string(from: regtime as Date)
                    
                    let regtDouble = regtime.timeIntervalSince1970
                    let regtint = Int(regtDouble * 1000)
                    
                    let endtDouble = endTime.timeIntervalSince1970
                    let endtInt = Int(endtDouble * 1000)
                    if regtint > endtInt {
                        let latedMin = regtint - endtInt
                        let gg =  String(format: "%2d" ,latedMin/60000) //Double(latedMin/60000)
                        cell.RegTimeLabel.text = reg
                        cell.lateMinLabel.text = "-\(gg) мин"
                    }else{
                        cell.RegTimeLabel.text = reg
                    }
                }else{
                    if self.lists[indexPath.row].attendance?.state == "" || self.lists[indexPath.row].attendance?.state == " "{
                        cell.RegTimeLabel.text =  "-"
                    }else{
                        cell.RegTimeLabel.text = self.lists[indexPath.row].attendance?.state ?? "-"
                    }
                    
                }
                return cell
            }
        }
    }
    
    func checkLateMin(){
        self.LateMinSum = 0
        if self.isFilterByName == true {
            if self.filtered.count > 0{
                for i in 0..<self.filtered.count {
                    let regtime = NSDate(timeIntervalSince1970: ((self.filtered[i].attendance?.checkedIn)!/1000))
                    let endTime = NSDate(timeIntervalSince1970: ((self.filtered[i].time?.timeEnds)!/1000))
                    
                    self.dateformatter.dateStyle = .none
                    self.dateformatter.timeStyle = .short
                    
                    let regtDouble = regtime.timeIntervalSince1970
                    let regtint = Int(regtDouble * 1000)
                    
                    let endtDouble = endTime.timeIntervalSince1970
                    let endtInt = Int(endtDouble * 1000)
                    
                    if regtint > endtInt {
                        let latedMin = regtint - endtInt
                        let gg = latedMin/60000
                        self.LateMinSum  += gg
                    }else{
                        
                    }
                    self.tableview.reloadData()
                }
            }else{
//                self.tableview.isHidden = true
            }
        }else if self.isFilterByTopic == true {
//            print("GG:: ",self.isFilterDone)
//             print("GGoo:: ",self.isFilterByTopic)
            if self.filteredAttsByTopis.count > 0{
                for i in 0..<self.filteredAttsByTopis.count {
                    let regtime = NSDate(timeIntervalSince1970: ((self.filteredAttsByTopis[i].attendance?.checkedIn)!/1000))
                    let endTime = NSDate(timeIntervalSince1970: ((self.filteredAttsByTopis[i].time?.timeEnds)!/1000))
                    
                    self.dateformatter.dateStyle = .none
                    self.dateformatter.timeStyle = .short
                    
                    let regtDouble = regtime.timeIntervalSince1970
                    let regtint = Int(regtDouble * 1000)
                    
                    let endtDouble = endTime.timeIntervalSince1970
                    let endtInt = Int(endtDouble * 1000)
                    
                    if regtint > endtInt {
                        let latedMin = regtint - endtInt
                        let gg = latedMin/60000
                        self.LateMinSum  += gg
                    }else{
                        
                    }
                    self.tableview.reloadData()
                }
            }else{
//                self.tableview.isHidden = true
            }
        }else{
            if self.lists.count > 0{
                for i in 0..<self.lists.count {
                    let regtime = NSDate(timeIntervalSince1970: ((self.lists[i].attendance?.checkedIn)!/1000))
                    let endTime = NSDate(timeIntervalSince1970: ((self.lists[i].time?.timeEnds)!/1000))
                    
                    self.dateformatter.dateStyle = .none
                    self.dateformatter.timeStyle = .short
                    
                    let regtDouble = regtime.timeIntervalSince1970
                    let regtint = Int(regtDouble * 1000)
                    
                    let endtDouble = endTime.timeIntervalSince1970
                    let endtInt = Int(endtDouble * 1000)
                    
                    if regtint > endtInt {
                        let latedMin = regtint - endtInt
                        let gg = latedMin/60000
                        self.LateMinSum  += gg
                    }else{
                    }
                    self.tableview.reloadData()
                }
            }else{
                self.tableview.isHidden = true
            }
        }
    }
    
    func getList(){
    
        if SettingsViewController.online == true {
            let query = UsersAttendancesQuery(userId: self.myId, classId: self.classId)
            KVNProgress.show()
            self.apollo.fetch(query: query) { result, error in
//                print("GG:: ",result?.data?.usersAttandances)
                KVNProgress.dismiss()
                if  (result?.data?.usersAttandances?.count)! > 0 || result?.data?.usersAttandances != nil{
                    let gg = (result?.data?.usersAttandances)! as! [UsersAttendancesQuery.Data.UsersAttandance]
                    if gg.count > 0 {
                        for i in 0..<gg.count{
                            if gg[i].attendance != nil {
                                if !self.lists.contains(where:{$0.id == gg[i].id ?? ""}){
                                    self.lists.append(gg[i])
                                    self.tableview.reloadData()
                                }
                            }
                        }
                        self.checkLateMin()
                    }
                }else{
                    //                print("this")
                    self.tableview.isHidden = true
                }
            }
        }else{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу")
        }
     
    }
    
    func getTeachersList(){
        checkInternet()
        if SettingsViewController.online{
            let query = ClassUsersListQuery(classId: self.classId)
            KVNProgress.show(withStatus: "", on: self.view)
            self.apollo.fetch(query: query) { result, error in
                KVNProgress.dismiss()
                
                if (result?.data?.classesUserListClassId!.count)! > 0{
                    self.AllusersList = (result?.data?.classesUserListClassId)! as! [ClassUsersListQuery.Data.ClassesUserListClassId]
                    
                    for i in 0..<self.AllusersList.count{
                        if self.AllusersList[i].classRole == "Admin" ||  self.AllusersList[i].classRole == "Teacher"{
                            if self.teachers.count > 0 {
                                
                                if !self.teachers.contains(where:{$0.id == self.AllusersList[i].id}){
                                    self.teachers.append(self.AllusersList[i])
                                    self.pickerView.reloadAllComponents()
                                }
                            }else{
                                self.teachers.append(self.AllusersList[i])
                                self.pickerView.reloadAllComponents()
                            }
                        }
                    }
                    //                self.mytableview.reloadData()
                    //                self.refreshControl.endRefreshing()
                }
            }
        }else{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу")
        }
      
    }
    
    func getTopicList(){
        checkInternet()
        if SettingsViewController.online {
            let query = ClassTopicsQuery(classId: self.classId)
            KVNProgress.show(withStatus: "", on: self.view)
            self.apollo.fetch(query: query) { result, error in
                KVNProgress.dismiss()
                if result?.data?.classTopics?.result != nil{
                    self.allTopics = result?.data?.classTopics?.result as! [ClassTopicsQuery.Data.ClassTopic.Result]
                    self.pickerView.reloadAllComponents()
                }else{
                    KVNProgress.showError(withStatus: result?.data?.classTopics?.message)
                }
            }
        }else{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу")
        }
     
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
         if self.isFilterByName == true {
              return self.teachers.count
         }else{
              return self.allTopics.count
        }
      
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        print("teacherCount:: ", self.teachers.count)
//        print("pickerRow:: ", row)
        if self.isFilterByName == true {
            if self.teachers.count > row{
                return self.teachers[row].userId?.nickname ?? ""
            }else{
                return self.teachers[0].userId?.nickname ?? ""
            }
        }else {
            return self.allTopics[row].name ?? ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if self.isFilterByName == true {
             self.selectedTeacherId = (self.teachers[row].userId?.id)!
        }else{
             self.selectedTopicId = (self.allTopics[row].id)!
        }
    }
    
    @objc func filterbyName(){
        checkInternet()
        if SettingsViewController.online {
            if isFilterByName == true{
                //            print("filterName:::: ")
                if self.teachers.count == 1 {
                    self.selectedTeacherId = (self.teachers[0].userId?.id)!
                }
                let query = UsersAttandancesByTeacherIdQuery(userId: self.myId, classId: self.classId, teacherId: self.selectedTeacherId)
                self.apollo.fetch(query: query) { result, error in
                    if result?.data?.usersAttandancesByTeacherId != nil{
                        if result?.data?.usersAttandancesByTeacherId?.count != 0{
                            let gg = (result?.data?.usersAttandancesByTeacherId)! as! [UsersAttandancesByTeacherIdQuery.Data.UsersAttandancesByTeacherId]
                            
                            for i in 0..<gg.count {
                                if gg[i].attendance?.id != nil {
                                    if !self.filtered.contains(where:{$0.attendance?.id == gg[i].attendance?.id}){
                                        self.filtered.append(gg[i])
//                                        print(self.filtered)
                                        if self.filtered.count > 0{
                                            self.isFilterDone = true
                                            self.checkLateMin()
                                        }
                                        
                                        // self.tableview.reloadData()
                                    }else{
                                        self.isFilterDone = true
                                        self.tableview.reloadData()
                                    }
                                }
                            }
                            
                        }else{
                            self.isFilterByName = false
                            KVNProgress.showError(withStatus: "Тухайн багш дээр ирц бүртгэгдээгүй байна.")
                        }
                        
                    }else{
                        self.isFilterByName = false
                        KVNProgress.showError(withStatus: "Тухайн багш дээр ирц бүртгэгдээгүй байна.")
                    }
                }
            }else{
                //             print("filterBYTopic:::: ")
                let queery = UsersAttandancesByTopicIdQuery(userId: self.myId, classId: self.classId, topicId: self.selectedTopicId)
                self.apollo.fetch(query: queery) { result, error in
                    if result?.data?.usersAttandancesByTopicId != nil{
                        if result?.data?.usersAttandancesByTopicId?.count != 0 {
                            
                            let oo = (result?.data?.usersAttandancesByTopicId)! as! [UsersAttandancesByTopicIdQuery.Data.UsersAttandancesByTopicId]
                            for i in 0..<oo.count{
                                if oo[i].attendance?.id != nil {
                                    if !self.filteredAttsByTopis.contains(where:{$0.attendance?.id == oo[i].attendance?.id}){
                                        self.filteredAttsByTopis.append(oo[i])
                                        if self.filteredAttsByTopis.count > 0{
                                            self.isFilterDone = true
                                            self.checkLateMin()
                                        }
                                        // self.tableview.reloadData()
                                    }else{
                                        self.isFilterDone = true
                                        self.checkLateMin()
                                        self.tableview.reloadData()
                                    }
                                }
                            }
                        }else{
                            self.isFilterByTopic = false
                            KVNProgress.showError(withStatus: "Тухайн хичээл дээр ирц бүртгэгдээгүй байна.")
                        }
                        
                    }else{
                        self.isFilterByTopic = false
                        KVNProgress.showError(withStatus: "Тухайн хичээл дээр ирц бүртгэгдээгүй байна")
                    }
                }
            }
            self.dismissPicker()
        
        }else{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу")
        }
    }
}
