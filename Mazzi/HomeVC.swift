//
//  HomeVC.swift
//  Mazzi
//
//  Created by Bayara on 7/28/18.
//  Copyright © 2018 Mazzi App LLC. All rights reserved.
//

import Foundation
import UIKit
import Apollo
//import SideMenu
import Contacts
import SwiftyJSON
import SocketIO
import AVKit
import RealmSwift
import KVNProgress
import SDWebImage
import MapKit
import CoreLocation
import Alamofire
import SideMenu
import HexColors
import RMessage

protocol Modal {
    func show(animated:Bool)
    func dismiss(animated:Bool)
    var backgroundView:UIView {get}
    var dialogView:UIView {get set}
}

public enum SocketAlertType {
    case success
    case error
    case notice
    case warning
    case info
    case custom(UIColor, UIColor)
    
    var backgroundColor: UIColor {
        switch self {
        case .success: return UIColor(0x4CAF50)
        case .error: return UIColor(0xf44336)
        case .notice: return UIColor(0x2196F3)
        case .warning: return UIColor(0xFFC107)
        case .info: return UIColor(0x009688)
        case .custom(let backgroundColor, _): return backgroundColor
        }
    }
    
    var textColor: UIColor {
        switch self {
        case .custom(_, let textColor): return textColor
        default: return UIColor(0xFFFFFF)
        }
    }
}

extension Modal where Self:UIView{
    func show(animated:Bool){
        self.backgroundView.alpha = 0
        self.dialogView.center = CGPoint(x: self.center.x, y: self.frame.height + self.dialogView.frame.height/2)
        UIApplication.shared.delegate?.window??.rootViewController?.view.addSubview(self)
        if animated {
            UIView.animate(withDuration: 0.33, animations: {
                self.backgroundView.alpha = 0.66
            })
            UIView.animate(withDuration: 0.33, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 10, options: UIView.AnimationOptions(rawValue: 0), animations: {
                self.dialogView.center  = self.center
            }, completion: { (completed) in
                
            })
        }else{
            self.backgroundView.alpha = 0.66
            self.dialogView.center  = self.center
        }
    }
}

class HomeVC : UIViewController , UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,CLLocationManagerDelegate,UIScrollViewDelegate{
    var menuController: SideMenuViewController!
    var centerController: UIViewController!
    var currentLocation: CLLocation!
    var lat : Double!
    var long : Double!
    let locManager = CLLocationManager()
    var name = ""
    var lastname = ""
    var register = ""
    var menuList:[String] = []
    var pics:[String] = []
    var userId = ""
    var contacts : Results<Contacts_realm>!
    var user_group : Results<User_group_realm>!
    let tapGestures = UITapGestureRecognizer()
    var conversations : Results<Conversation_realm>!
    var conversation_ids = [String]()
    var myId = SettingsViewController.userId
    var chatBadges = 0
    var mclassBadges = 0
    var mchatNotify = [SumNotificationsQuery.Data.SumNotification.Result.Mchat]()
    var mclassNotify = [SumNotificationsQuery.Data.SumNotification.Result.Mclass]()
    var apolloHome: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        // Add additional headers as needed
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
        let url = URL(string:SettingsViewController.graphQL)!
        
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    @IBOutlet weak var mycollectionview: UICollectionView!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var menubtn: UIBarButtonItem!
    @IBAction func menu(_ sender: Any) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        apolloHome = { () -> ApolloClient in 
            let configuration = URLSessionConfiguration.default
            // Add additional headers as needed
            configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
            let url = URL(string:SettingsViewController.graphQL)!
            
            return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
        }()
        self.getNotify()
    }
    
    func getBalance(){
        let query = BalanceQuery(userId: self.myId)
        self.apolloHome.fetch(query: query) { result, error in
//            print("RESULT>>:: ", result?.data?.balance)
            if result?.data?.balance?.error == false {
                let balance = result?.data?.balance?.result?.balance ?? 0.0
                print("BALANCE:: ", balance)
                self.title = "Таны дансанд: \(balance)₮"
            }else{
                
            }
        }
    }
 
    override func viewDidLoad() {
        super .viewDidLoad()
//        self.checkVersion()
//        self.title = "Таны дансанд: 35000000₮"
        self.locManager.delegate = self
       
        mycollectionview.delegate = self
        mycollectionview.dataSource = self
        mycollectionview!.register(UINib(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "homeCell")
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //.horizontal
        layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        layout.minimumLineSpacing = 15.0
        layout.minimumInteritemSpacing = 4.0
        mycollectionview.setCollectionViewLayout(layout, animated: true)
        menuList = ["МАЗЗИ ЧАТ","M CLASS","ХОЛБОО БАРИХ","QR УНШИГЧ","ДАНС"]
        pics = ["nChat.png","nMclass.png","nContactUs.png","nQR.png","wallet.png"]
        
//        menuList = ["МАЗЗИ ЧАТ","M CLASS","ХОЛБОО БАРИХ","QR УНШИГЧ"]
//        pics = ["nChat.png","nMclass.png","nContactUs.png","nQR.png"]
        
        //here is clear cached images cos: cache is full our images not saved
        SDImageCache.shared().clearMemory()
        SDImageCache.shared().clearDisk()
        newSideMenu()
//        print("GRAPHQL URL :: ",SettingsViewController.graphQL)
    }
    //todojani added here
    func newSideMenu(){
        SideMenuManager.default.menuPresentMode = .viewSlideInOut
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuEnableSwipeGestures = false
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        SideMenuManager.default.menuAnimationTransformScaleFactor = CGFloat(1)
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width * 0.8
        SideMenuManager.default.menuWidth = screenWidth
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
            if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
                if CLLocationManager.isRangingAvailable() {
                    // do stuff
//                    print("LOCATION")
                }
            }
        }
    }
    
    func getCurrentLocation(){
        
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            currentLocation = locManager.location
            self.lat = currentLocation.coordinate.latitude
            self.long = currentLocation.coordinate.longitude
        }else{
            self.locationManager(manager: locManager, didChangeAuthorizationStatus: .notDetermined)
        }
    }
    
    private func locationManager(manager: CLLocationManager!,
                                 didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        var shouldIAllow = false
        var locationStatus = ""
        switch status {
        case CLAuthorizationStatus.restricted:
            locationStatus = "Restricted Access to location"
        case CLAuthorizationStatus.denied:
            locationStatus = "User denied access to location"
        case CLAuthorizationStatus.notDetermined:
            locationStatus = "notDetermined"
        default:
            locationStatus = "Allowed to location Access"
            shouldIAllow = true
        }
//        print(locationStatus)
        if shouldIAllow == false {
            let alert = UIAlertController(title: "", message: "Утасныхаа байршил тогтоогчийг идэвхжүүлнэ үү", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Тохиргоо", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction) -> Void in
                let alertt = UIAlertController(title: "Заавар", message: "Settings -> Privacy -> Location Services -> Mazzi : While Using the App", preferredStyle: UIAlertController.Style.alert)
                alertt.addAction(UIAlertAction(title: "OK", style: .cancel, handler:  { (alert:UIAlertAction) -> Void in }))
                self.present(alertt, animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
        var gradientImage:UIImage?
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            gradientLayer.render(in: context)
            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        UIGraphicsEndImageContext()
        return gradientImage
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func updateBadge(notification:Notification){
        _ = self.apolloHome.clearCache()
        self.getNotify()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        checkInternet()
        self.myId = SettingsViewController.userId
        self.getBalance()
        _ = self.apolloHome.clearCache()
        self.checkLogin()
        socket.disconnect()
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateBadge(notification:)), name: Notification.Name("received_notify"), object: nil)
        self.setNeedsStatusBarAppearanceUpdate()
        self.navigationController?.navigationBar.isHidden = true
        getFireUrl(url: "sss") { (suc) in
            print("suc")
        }

        if(userDefaults.object(forKey: "keyboardheight") != nil){
            let keyboardHeight = (userDefaults.object(forKey: "keyboardheight") as? Int)!
            SettingsViewController.keyboardHeight = keyboardHeight
            if let name = userDefaults.string(forKey: "nickname"){
                SettingsViewController.username = name
            }
        }
        
        if socket.status == .notConnected || socket.status == .disconnected{
            if SettingsViewController.online == true{
                socketInitialize()
            }else{
                KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу!")
            }
        }
        
//        cameraAllowsAccessToApplicationCheck()
        DataStore.configureMigration()
        self.getNotify()
        //todojani here is going to select chat icon
        
        if GlobalStaticVar.clickedNotification != ""{
            UIView.setAnimationsEnabled(false)
            self.performSegue(withIdentifier: "tochat", sender: self)
            UIView.setAnimationsEnabled(true)
        }
        if GlobalStaticVar.selectedClassId != ""{
            UIView.setAnimationsEnabled(false)
            if SettingsViewController.online {
                self.checkinfo()
                
            }else{
                KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу!")
            }
            
            UIView.setAnimationsEnabled(true)
        }
    }
    
    func checkVersion() {
        let baseurl = SettingsViewController.baseurl
        Alamofire.request("\(baseurl)check_version", method: .post, parameters: ["type":"IOS"], encoding: JSONEncoding.default).responseJSON
            { response in
                switch response.result {
                case .success:
                    let res = JSON(response.result.value!)
                    let version = res["message"].string ?? ""
                    if let text = Bundle.main.infoDictionary?["CFBundleShortVersionString"]  as? String {
                        if text == version {
                            
                        }else{
                            let alert = UIAlertController(title: "", message: "Шинэ хувилбар гарсан байна. ", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Татах", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction) -> Void in
                                let appStoreLink = "https://itunes.apple.com/us/app/mazzi/id1452131622?ls=1&mt=8"
                                if let url = URL(string: appStoreLink), UIApplication.shared.canOpenURL(url) {
                                    UIApplication.shared.open(url, options: [:], completionHandler: {(success: Bool) in
                                        if success {
                                            print("Launching \(url) was successful")
                                        }})
                                }
                            }))
                            
                            alert.addAction(UIAlertAction(title: "Хаах", style: .cancel, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                case .failure(let e):
                    print(e)
                }
        }
    }
    
    func checkLogin(){
        let islogin = userDefaults.object(forKey: "isLogin") as? String ?? "false"
        if islogin == "true"{
            SettingsViewController.userId = userDefaults.string(forKey: "user_id")!
            GlobalVariables.user_id = userDefaults.string(forKey: "user_id")!
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondViewController = storyboard.instantiateViewController(withIdentifier: "login") as! LoginViewController
            self.navigationController?.pushViewController(secondViewController, animated: false)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0{
            self.performSegue(withIdentifier: "tochat", sender: self)
        }else if indexPath.row == 1{
            if SettingsViewController.online {
                 self.performSegue(withIdentifier: "corp", sender: self)
//                let CorpVC = CorpListVC(nibName: "CorpListVC", bundle: nil)
//                self.navigationController?.pushViewController(CorpVC, animated: true)
//                self.checkinfo()
            }else{
                KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу!")
            }
            
        }else if indexPath.row == 2{
            self.performSegue(withIdentifier: "toContactUs", sender: self)
        }else if indexPath.row == 3{
            self.locManager.requestWhenInUseAuthorization()
     
            let when = DispatchTime.now() + 1
                DispatchQueue.main.asyncAfter(deadline: when) {
//                    self.getCurrentLocation()
                    self.performSegue(withIdentifier: "qr", sender: self)
                }
            
        }else{
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "wallet") as? walletVC
            self.navigationController?.pushViewController(viewController!, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (mycollectionview.bounds.size.width - 30) / 2
        return CGSize(width: width,height: 150)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeCell", for: indexPath) as! HomeCollectionViewCell
        
        if indexPath.row == 0 {
            if  self.chatBadges > 0 {
                cell.badgeCountLabel.isHidden = false
                let gg = String( self.chatBadges)
                cell.badgeCountLabel.setTitle(gg, for: UIControl.State.normal)
            }
            
        }else if indexPath.row == 1{
            if self.mclassBadges > 0 {
                cell.badgeCountLabel.isHidden = false
                let gg = String(self.mclassBadges)
                cell.badgeCountLabel.setTitle(gg, for: UIControl.State.normal)
            }
            
        }else{
            cell.badgeCountLabel.isHidden = true
        }
        
        cell.backgroundColor = UIColor.white
        cell.myLabel.textColor = GlobalVariables.todpurple
        cell.myLabel.text = menuList[indexPath.row]
        cell.imageviews.image = UIImage(named: pics[indexPath.row])
        return cell
    }
    
    func cameraAllowsAccessToApplicationCheck(){
        let authorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authorizationStatus {
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video,completionHandler: { (granted:Bool) -> Void in
                if granted {
                    SettingsViewController.cameraAccess = true
                }
                else {
                    SettingsViewController.cameraAccess = false
                }
            })
        case .authorized:
            SettingsViewController.cameraAccess = true
        case .denied, .restricted:
            SettingsViewController.cameraAccess = false
        }
    }
    
    @objc func goscaneer (){
        self.performSegue(withIdentifier: "qr", sender: self)
    }
    
    func convertCGImageToCIImage(inputImage: CGImage) -> CIImage! {
        let ciImage = CIImage(cgImage: inputImage, options: nil)
        return ciImage
    }
    
    func convert(cmage:CIImage) -> UIImage
    {
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent)!
        let image:UIImage = UIImage.init(cgImage: cgImage)
        return image
    }
    
    func checkinfo(){
        let userInfo = userDefaults.dictionary(forKey: "userInfo")
        if userInfo!["nickname"] != nil && userInfo!["nickname"] as! String != "" {
            if userInfo!["lastname"] != nil && userInfo!["lastname"] as! String != ""  {
                if userInfo!["registerId"] != nil && userInfo!["registerId"] as! String != ""  {
                    let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "classlist") as? MClassListVC
                    self.navigationController?.pushViewController(viewController!, animated: true)
                }else{
                    let alert = UIAlertController(title: "", message: "Регистрийн дугаараа оруулна уу", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction) -> Void in
                        self.performSegue(withIdentifier: "editpro", sender: self)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }else{
                let alert = UIAlertController(title: "", message: "Овог нэрээ оруулна уу", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction) -> Void in
                    self.performSegue(withIdentifier: "editpro", sender: self)
                    
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
        else{
            let alert = UIAlertController(title: "", message: "Нэрээ оруулна уу", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction) -> Void in
                self.performSegue(withIdentifier: "editpro", sender: self)
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "qr" {
            let controller = segue.destination as! QRscanVC
            controller.lat = self.lat
            controller.long = self.long
        }
    }
    
    
    func getNotify(){
     
        _ = self.apolloHome.clearCache()
        let query = SumNotificationsQuery(userId: self.myId)
        self.apolloHome.fetch(query: query) { result, error in
//            print("QUREY RESULT:: " , result?.data?.sumNotifications)
            if result?.data?.sumNotifications != nil {
                self.mchatNotify = (result!.data?.sumNotifications?.result?.mchat)! as! [SumNotificationsQuery.Data.SumNotification.Result.Mchat]
                self.mclassNotify = (result!.data?.sumNotifications?.result?.mclass)! as! [SumNotificationsQuery.Data.SumNotification.Result.Mclass]
                self.mclassBadges = 0
                self.chatBadges = 0
                
                for i in 0..<self.mclassNotify.count{
                    self.mclassBadges += self.mclassNotify[i].badge!
                }
                
                for i in 0..<self.mchatNotify.count{
                    self.chatBadges += self.mchatNotify[i].badge!
                }

                GlobalStaticVar.AppBadgeCount =  self.chatBadges + self.mclassBadges
                getBadgeCount()
                self.mycollectionview.reloadData()
            }
        }
    }
}
