////
//  ContactViewController.swift
//  Mazzi
//
//  Created by Janibekm on 8/29/17.
//  Copyright © 2017 Mazzi App LLC. All rights reserved.
//

import UIKit
import Contacts
import Alamofire
import SwiftyJSON
import SocketIO
import RealmSwift
import UserNotifications
import UserNotificationsUI
import MessageUI
import KVNProgress

class ContactViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,UISearchBarDelegate, MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var mySearchBar: UISearchBar!
    @IBOutlet weak var txtfield: UITextField!
    var refresher = UIRefreshControl()
    var contactid = ""
    var numbersToDB = [String]()
    var num = [String]()
    var isMazzi = true
    var contacts : Results<Contacts_realm>!
    var installedContacts : Results<Contacts_realm>!
    var accessAuthorized = false
    var is_all = true
    var customSC = UISegmentedControl()
    var sectionIndex = Int()
    var selectredIndex = Int()
    var mynumber = ""
    @IBOutlet weak var topconstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var recentJoinedScrollView: UIScrollView!
    @IBOutlet weak var contacTopView: UIView!
    @IBOutlet weak var allcontactcount: UILabel!
    @IBOutlet weak var joinedContactsText: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    var newMember = false
    var newMemberName = ""
    let userDefaults = UserDefaults.standard
    var filteredContacts  :Results<Contacts_realm>!
    var searchActive : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.get_contacts()
        self.get_installed()
        self.tableView.refreshControl = self.refresher
        self.refresher.addTarget(self, action: #selector(reloadContacts), for: .valueChanged)
        self.contacts = try! Realm().objects(Contacts_realm.self).sorted(byKeyPath: "name", ascending: true).filter("created_at != 0")
        self.getOnlineContacts()
        self.checkjoined()
        self.mySearchBar.delegate = self
        self.mySearchBar.barTintColor = GlobalVariables.F5F5F5
        self.mySearchBar.tintColor = GlobalVariables.todpurple
        mynumber = (userDefaults.object(forKey: "myNumber") as? String)!
        let attributesNormal = [
            NSAttributedString.Key.foregroundColor : GlobalVariables.lightGray,
            NSAttributedString.Key.font : GlobalVariables.customFont12
        ]
        
        let attributesSelected  = [
            NSAttributedString.Key.foregroundColor : GlobalVariables.todpurple,
            NSAttributedString.Key.font : GlobalVariables.customFont12
        ]
        
        UITabBarItem.appearance().setTitleTextAttributes(attributesNormal as [NSAttributedString.Key : Any], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes(attributesSelected as [NSAttributedString.Key : Any], for: .selected)
       
       
        let items = ["Маззи", "Бусад"]
        customSC = UISegmentedControl(items: items)
        self.customSC.selectedSegmentIndex = 0
        customSC.frame = CGRect(x: (self.view.bounds.size.width/2) - 70, y:7.5, width: 140, height: 30)
        customSC.layer.borderColor = GlobalVariables.purple.cgColor
        customSC.layer.borderWidth = 1
        customSC.backgroundColor = UIColor.white
        customSC.tintColor = GlobalVariables.purple
        customSC.layer.cornerRadius = 12
        customSC.layer.masksToBounds = true
        let font = UIFont (name: "segoeui", size: 12)
        customSC.setTitleTextAttributes([NSAttributedString.Key.font: font as Any],
                                        for: .normal)
        // Add target action method
        customSC.addTarget(self, action: #selector(changeSegment), for: .valueChanged)
        self.view.backgroundColor = GlobalVariables.todpurple
        self.navigationController?.navigationBar.addSubview(customSC)
        self.navigationController?.navigationBar.barTintColor = GlobalVariables.black
    }
    
    @objc func reloadContacts(){
        self.get_contacts()
        if self.customSC.selectedSegmentIndex == 0 {
            self.contacts = realm.objects(Contacts_realm.self).sorted(byKeyPath: "name", ascending: true).filter("created_at != 0")
            self.customSC.selectedSegmentIndex = 0
            self.tableView.reloadData()
        }else{
            self.contacts = realm.objects(Contacts_realm.self).sorted(byKeyPath: "name", ascending: true).filter("created_at == 0")
            self.customSC.selectedSegmentIndex = 1
            self.tableView.reloadData()
        }
    }
    
    @objc func back(){
        self.navigationController?.popViewController(animated: true)
    }

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
 
        self.customSC.isEnabled = false
        self.mySearchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.mySearchBar.showsCancelButton = false
        self.customSC.isEnabled = true
        self.mySearchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.mySearchBar.resignFirstResponder()
        self.customSC.isEnabled = true
        self.mySearchBar.text = ""
         searchActive = false
        self.tableView.reloadData()
    }
    
     @objc func changeSegment(sender: UISegmentedControl!)  {
        switch sender.selectedSegmentIndex {
        case 0:
            self.isMazzi = true
            self.contacts = try! Realm().objects(Contacts_realm.self).sorted(byKeyPath: "name", ascending: true).filter("created_at != 0")
            self.tableView.reloadData()
        case 1:
            self.contacts = nil
            self.isMazzi = false
            self.contacts = try! Realm().objects(Contacts_realm.self).sorted(byKeyPath: "name", ascending: true).filter("created_at == 0")
            self.tableView.reloadData()
        default:
            self.isMazzi = true
            self.contacts = try! Realm().objects(Contacts_realm.self).sorted(byKeyPath: "name", ascending: true).filter("created_at != 0")
            self.tableView.reloadData()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.txtfield.resignFirstResponder()
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text != "" {
            let searchText = searchBar.text?.lowercased()
               searchActive = true
            if (searchText?.isAlphanumeric)!{
                self.filteredContacts = self.contacts.filter("name CONTAINS[c]'\(searchText!)'")
                if self.filteredContacts.isEmpty{
                    self.filteredContacts = self.contacts.filter("name CONTAINS[c]'\(searchText!)'")
                }
                self.tableView.reloadData()
            }else{
                self.filteredContacts = self.contacts.filter("phone CONTAINS'\(searchText!)'")
                self.tableView.reloadData()
            }
        }else{
            searchActive = false
            self.tableView.reloadData()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        contactid = ""
        self.customSC.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        self.mySearchBar.text = ""
        self.getRealmDeletedPhones()
        self.searchActive = false
        getAccessToContacts()
        self.customSC.isHidden = false
        self.customSC.selectedSegmentIndex = 0
        self.isMazzi = true
        self.contacts = try! Realm().objects(Contacts_realm.self).sorted(byKeyPath: "name", ascending: true).filter("created_at != 0")
        self.tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let turshiltbtn = UIBarButtonItem(image: UIImage(named:"nemehBTN"), style: .plain, target: self, action: #selector(buttonTapped(sender:)))
        self.navigationController?.navigationBar.topItem?.rightBarButtonItem = turshiltbtn
    }
    
   @objc func buttonTapped(sender: UIButton){
        self.performSegue(withIdentifier: "search", sender: self)
    }
    
    func setup(){
        self.isMazzi = true
        tableView.delegate = self
        tableView.dataSource = self
        if let selectionIndexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: selectionIndexPath, animated: true)
        }
    }
    func notification(){
        let content = UNMutableNotificationContent()
        content.title = newMemberName
        content.body = "Маззид бүртгүүллээ"
        content.sound = UNNotificationSound.default
        if let path = Bundle.main.path(forResource: "assets/notification", ofType: "png") {
            let url = URL(fileURLWithPath: path)
            do {
                let attachment = try UNNotificationAttachment(identifier: "sampleImage", url: url, options: nil)
                content.attachments = [attachment]
            } catch {
                print("attachment not found.")
            }
        }
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 1.0, repeats: false)
        let request = UNNotificationRequest(identifier:ChatListViewController.requestIdentifier, content: content, trigger: trigger)
        UNUserNotificationCenter.current().delegate = self as UNUserNotificationCenterDelegate
        UNUserNotificationCenter.current().add(request){(error) in
            if (error != nil){
                print(error?.localizedDescription ?? "")
            }
        }
    }
    
    func getAccessToContacts(){
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        switch authorizationStatus {
        case .authorized:
            accessAuthorized = true
        case .denied, .notDetermined:
            accessAuthorized = false
            let alert = UIAlertController(title: "", message: "Тохиргоо руу орж хадгалсан дугаарын жагсаалт авах тохиргоог идэвхжүүлнэ үү", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
            
            alert.addAction(UIAlertAction(title: "Тохиргоо", style: UIAlertAction.Style.default, handler:  { (alert:UIAlertAction) -> Void in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }))
            
            self.present(alert, animated: true, completion: nil)
        default:
            let alert = UIAlertController(title: "", message: "Тохиргоо руу орж хадгалсан дугаарын жагсаалт авах тохиргоог идэвхжүүлнэ үү", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
            
            alert.addAction(UIAlertAction(title: "Тохиргоо", style: UIAlertAction.Style.default, handler:  { (alert:UIAlertAction) -> Void in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }))
            
            self.present(alert, animated: true, completion: nil)
            accessAuthorized = false
        }
    }
    func getRealmDeletedPhones(){
        if(self.contacts != nil){
            for i in 0..<contacts.count{
                let phonetext = contacts[i].phone
                    if self.numbersToDB.contains(phonetext){
                        
                    }else{
                      
                        RealmService.DeleteContactRealm(deleteContact:phonetext, completion: {  (complete) in
                            if complete == true{
                                if self.customSC.selectedSegmentIndex == 0 {
                                    self.contacts = realm.objects(Contacts_realm.self).sorted(byKeyPath: "name", ascending: true).filter("created_at != 0")
                                    self.customSC.selectedSegmentIndex = 0
                                    self.tableView.reloadData()
                                }else{
                                    self.contacts = realm.objects(Contacts_realm.self).sorted(byKeyPath: "name", ascending: true).filter("created_at == 0")
                                    self.customSC.selectedSegmentIndex = 1
                                    self.tableView.reloadData()
                                }
                                self.tableView.reloadData()
                            }
                        })
                        
                        break
                    }
                
            }
        }
    }
    
    func get_contacts(){
       
        self.refresher.endRefreshing()
        let store = CNContactStore()
        let fetchRequest = CNContactFetchRequest(keysToFetch: [CNContactFamilyNameKey as CNKeyDescriptor, CNContactGivenNameKey as CNKeyDescriptor, CNContactPhoneNumbersKey as CNKeyDescriptor, CNContactIdentifierKey as CNKeyDescriptor])
        do{
            try store.enumerateContacts(with: fetchRequest) { contact, stop in
             //   print("contact:" , contact)
                if contact.phoneNumbers.count > 0 {
                    self.accessAuthorized = true
                    let key = contact.identifier
                    var name = ""
                    if contact.givenName.isEmpty {
                          name = contact.familyName
                    }
                     name = contact.givenName
                    let lastname = contact.familyName
                    var phones = [String]()
                    for number: CNLabeledValue in contact.phoneNumbers {
                       
                        let numbervalue = number.value
                        var digits = (numbervalue.value(forKey: "digits") as? String)!
                        if digits.hasPrefix("+"){
                            digits.remove(at: digits.startIndex)
                            if digits.count == 8 {
                                phones.append("976\(digits)")
                                self.numbersToDB.append("976\(digits)")
                            }else{
                                phones.append(digits)
                                self.numbersToDB.append(digits)
                            }
                        }else{
                            if digits.count == 8 {
                                phones.append("976\(digits)")
                                self.numbersToDB.append("976\(digits)")
                            }else{
                                phones.append(digits)
                                self.numbersToDB.append(digits)
                            }
                        }
                    }
                            DispatchQueue.main.async {
                               
                                if phones.contains(self.mynumber){
                                  //  print(phones)
                                }else{
                                        let contactToDB = Contacts.init(id: key, name: name, lastname: lastname, surname: "", image: "", phones: phones, phone: "", gender: "", birthday: 0, state: 0, online_at: 0, created_at: 0, fcm_token: "")
                 
                                        RealmService.writeContactToRealm(installedContacts: contactToDB, completion: { (complete) in
                                            if complete == true{
                                                self.tableView.reloadData()
                                            }
                                        })
                                }
                            }
                }
            }
    }catch{
        self.getAccessToContacts()
        }
    }
    func get_installed(){
        let baseurl = SettingsViewController.baseurl
        let phone = (userDefaults.object(forKey: "myNumber") as? String)!
        if self.numbersToDB.contains(phone){
            let index = self.numbersToDB.index(of: phone)
            self.numbersToDB.remove(at: index!)
        }

        let token = UserDefaults.standard.object(forKey:"token") as! String
        let  header = ["x-access-token": token, "authorization": "BPXjEvWfXxx5qpQHHFMEJEB8fA3xAczC"]

        Alamofire.request("\(baseurl)user_info", method: .post, parameters: ["phone_numbers":self.numbersToDB], encoding: JSONEncoding.default,headers:header).responseJSON{ response in
            if response.response?.statusCode == 200 {
                let jsonData = JSON(response.value!)
                let error = jsonData["error"].boolValue
                DispatchQueue.main.async {
                    let loops = jsonData["result"]
                    if error == false {
                        for i in 0..<loops.count{
                            let dispid =    jsonData["result"][i]["_id"].string ?? ""
                            let dispphone = jsonData["result"][i]["phone"].string ?? ""
                            let dispgender = jsonData["result"][i]["gender"].string ?? ""
                            let disppic =    jsonData["result"][i]["image"].string ?? ""
                            let dispnickname = jsonData["result"][i]["nickname"].string ?? ""
                            let displastname = jsonData["result"][i]["lastname"].string ?? ""
                            let dispsurname = jsonData["result"][i]["surname"].string ?? ""
                            let dispbirthday = jsonData["result"][i]["birthday"].double ?? 0
                            let dispstate = jsonData["result"][i]["state"].int ?? 0
                            let online_at = jsonData["result"][i]["online_at"].double ?? 0
                            let fcm_token = jsonData["result"][i]["fcm_token"].string ?? ""

                            DispatchQueue.main.async {
                                let installedApp = Contacts.init(id: dispid, name: dispnickname, lastname: displastname, surname: dispsurname, image: disppic, phones: [""], phone: dispphone, gender: dispgender,birthday:dispbirthday, state: dispstate, online_at: online_at, created_at: online_at, fcm_token: fcm_token)
                                    RealmService.writeContactToRealm(installedContacts: installedApp, completion: { (writeComplete) in
                                        if writeComplete == true {
                                            self.tableView.reloadData()
                                        }
                                    })
                            }
                        }
                    }else{
                        print("ContactViewController - print error")
                    }
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.searchActive {
            if self.customSC.selectedSegmentIndex == 0{
                KVNProgress.show()
                let viewController = storyboard?.instantiateViewController(withIdentifier: "ProfileContact") as! ProfileContact
                viewController.contactid = self.filteredContacts[indexPath.row].id
                GlobalVariables.isFromContacts = true
                let navController = UINavigationController(rootViewController: viewController)
                self.present(navController, animated:true, completion: nil)
                KVNProgress.dismiss()
            }else{
                let textToShare = "Маззи апп-г татаарай. "
                if let myWebsite = NSURL(string: "https://mazzi.mn/") {
                    let objectsToShare = [textToShare, myWebsite] as [Any]
                    let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                    activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToFacebook]
                    activityVC.popoverPresentationController?.sourceView = self.view
                    self.present(activityVC, animated: true, completion: nil)
                }
            }
        }else{
            if self.customSC.selectedSegmentIndex == 0{
                KVNProgress.show()
                let viewController = storyboard?.instantiateViewController(withIdentifier: "ProfileContact") as! ProfileContact
                viewController.contactid = self.contacts[indexPath.row].id
                GlobalVariables.isFromContacts = true
                let navController = UINavigationController(rootViewController: viewController)
                self.present(navController, animated:true, completion: nil)
                KVNProgress.dismiss()
            }else{
                let textToShare = "Маззи апп-г татаарай. "
                if let myWebsite = NSURL(string: "https://mazzi.mn/") {
                    let objectsToShare = [textToShare, myWebsite] as [Any]
                    let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                    activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToFacebook]
                    activityVC.popoverPresentationController?.sourceView = self.view
                    self.present(activityVC, animated: true, completion: nil)
                }
            }
        }
        
    }
    
    public func checkjoined(){
        socket.on("joined"){data, ack in
            let joined = JSON(data)
            let number = joined[0]["phone"].string!
            if self.numbersToDB.contains("976\(number)"){
                self.numbersToDB = ["\(number)"]
                self.newMember = true
                self.get_installed()
                let when = DispatchTime.now() + 1
                DispatchQueue.main.asyncAfter(deadline: when) {
                    self.viewDidLoad()
                }
            }
        }
    }
    func getOnlineContacts(){
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when) {
            let time = DispatchTime.now() + 3
            DispatchQueue.main.asyncAfter(deadline: time, execute: {
                socket.on("user_status"){data, ack in
                    if self.customSC.selectedSegmentIndex == 0 {
                        self.contacts = realm.objects(Contacts_realm.self).sorted(byKeyPath: "name", ascending: true).filter("created_at != 0")
                        self.customSC.selectedSegmentIndex = 0
                        self.tableView.reloadData()
                    }else{
                        self.contacts = realm.objects(Contacts_realm.self).sorted(byKeyPath: "name", ascending: true).filter("created_at == 0")
                        self.customSC.selectedSegmentIndex = 1
                        self.tableView.reloadData()
                    }
                    
                    // self.recentlyjoinedView()
                }
            })
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if searchActive{
                 return self.filteredContacts.count
            }else{
                 return self.contacts.count
            }
    }
    
    @objc func sendInvite(_ sender:Any){
        if searchActive {
            let controller = MFMessageComposeViewController()
            controller.body = "Маззи апп-г татаарай. https://mazzi.mn/"
            var phonetext = self.filteredContacts[(sender as AnyObject).tag].phones.map({$0.phones}).joined(separator: ", ")
            if phonetext == "" {
                phonetext = self.filteredContacts[(sender as AnyObject).tag].phone
            }
            controller.recipients = [phonetext]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }else{
            let controller = MFMessageComposeViewController()
            controller.body = "Маззи апп-г татаарай. https://mazzi.mn/"
            var phonetext = self.contacts[(sender as AnyObject).tag].phones.map({$0.phones}).joined(separator: ", ")
            if phonetext == "" {
                phonetext = self.contacts[(sender as AnyObject).tag].phone
            }
            controller.recipients = [phonetext]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! LetterTableViewCell
        cell.clearCellData()
            if self.contacts[indexPath.row].phone == "" {
                cell.inviteBtn.isHidden = false
            }else{
                 cell.inviteBtn.isHidden = true
            }
        let fileurl = SettingsViewController.fileurl
        cell.inviteBtn.tag = indexPath.row
        cell.inviteBtn.addTarget(self, action: #selector(sendInvite(_:)), for: .touchUpInside)
        if searchActive {
                let pic = self.filteredContacts[indexPath.row].image
                let fileurl = SettingsViewController.fileurl
                let name = self.filteredContacts[indexPath.row].name
                let lastname = self.filteredContacts[indexPath.row].lastname
                let phonetext = self.filteredContacts[indexPath.row].phones.map({$0.phones}).joined(separator: ", ")
                cell.userLabel?.text = "\(name) \(lastname)"
                cell.inviteBtn.alpha = 1.0
                let phone = self.filteredContacts[indexPath.row].phone
                if pic != ""{
                    cell.userImageView?.sd_setImage(with: URL(string: "\(fileurl)\(pic)"), completed: nil)
                    cell.phoneLabel.text = phone
                }else{
                    cell.phoneLabel.text = phonetext
                    cell.userImageView.image = UIImage(named: "circle-avatar")
                }
            }else{
                let phone = self.contacts[indexPath.row].phone
                let pic = self.contacts[indexPath.row].image
                if pic != ""{
                    cell.userImageView?.sd_setImage(with: URL(string: "\(fileurl)\(pic)"), completed: nil)
                }else{
                    cell.userImageView.image = UIImage(named: "circle-avatar")
                }
                let name = self.contacts[indexPath.row].name
                let lastname = self.contacts[indexPath.row].lastname
                let phonetext = self.contacts[indexPath.row].phones.map({$0.phones}).joined(separator: ", ")
                cell.userLabel?.text = "\(name) \(lastname)"
                if phone == ""{
                    cell.phoneLabel.text = phonetext
                }else{
                    cell.phoneLabel.text = phone
                }
                
        }
      return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
}
extension ContactViewController:UNUserNotificationCenterDelegate{
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        if notification.request.identifier == ChatListViewController.requestIdentifier{
            completionHandler( [.alert,.sound,.badge])
        }
    }
}

