//
//  BeginExamVC.swift
//  Mazzi
//
//  Created by Bayara on 12/7/18.
//  Copyright © 2018 Mazzi Application. All rights reserved.
//

import Foundation
import KVNProgress
import Apollo

class BeginExamVC:UIViewController,UITableViewDataSource,UITableViewDelegate{
   
    @IBOutlet weak var exitBtn: UIButton!
    @IBOutlet weak var examBtn: UIButton!
    @IBOutlet weak var pointBtn: UIButton!
    @IBOutlet weak var subview: UIView!
    @IBOutlet weak var mytableview: UITableView!
    var exams = [String]()
    var classId = GlobalStaticVar.classId
    var teacherId = GlobalStaticVar.teacherId
//    var apollo = SettingsViewController.apollo
    var sets = [GetquestionsetsQuery.Data.QuestionSet]()
    var myId = SettingsViewController.userId
    var setId = ""
    var roomName = ""
    var dateformatter = DateFormatter()
    var ActiveLaunch = ActiveLaunchTeacherQuery.Data.ActiveLaunchTeacher()
    let refresher = UIRefreshControl()
    let apollo: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        // Add additional headers as needed
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
//        print("TOKEN APOLLOO:: ", GlobalVariables.headerToken)
        let url = URL(string:SettingsViewController.graphQL)!
        
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.teacherId == GlobalVariables.user_id{
            self.subview.isHidden = true
        }else{
            self.subview.isHidden = false
            self.mytableview.isHidden = true
        }
        self.customize()
        self.mytableview.register(UITableViewCell.self, forCellReuseIdentifier: "DefaultCell")
        exams = ["Шалгалт өгөх","Дүн харах"]
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = ""
        if GlobalStaticVar.teacherId == self.myId {
            let rightBtn = UIBarButtonItem(image: UIImage(named:"editmenu"), style: .plain, target: self, action: #selector(self.goSettings))
            self.navigationController?.navigationBar.topItem?.rightBarButtonItem = rightBtn
        }
    }
    
    @objc func goSettings(){
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "setting") as? ClassroomSettingVC
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        _ = self.apollo.clearCache()
        self.ActiveLaunch.id = ""
        self.ActiveLaunch.questionSetId = ""
        self.sets = []
        self.getActiveLaunch()
        if self.teacherId == self.myId || GlobalStaticVar.classRole == "Teacher" {
            print("admin")
            self.subview.isHidden = true
            self.mytableview.isHidden = false
            self.getQuestionSets()
            self.mytableview.reloadData()
        }else{
             print("student")
            self.subview.isHidden = false
            self.mytableview.isHidden = true
        }
    }
    
    func customize(){
        checkInternet()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "05"), style: .plain, target: self, action: #selector(back))
        self.mytableview.delegate = self
        self.mytableview.dataSource = self
        self.mytableview.register(UINib(nibName: "questionSetCell", bundle: nil), forCellReuseIdentifier: "qset")
        self.mytableview.refreshControl = self.refresher
        self.refresher.addTarget(self, action: #selector(reloadList), for: .valueChanged)
        self.pointBtn.backgroundColor = GlobalVariables.purple
        self.examBtn.backgroundColor = GlobalVariables.purple
       
        self.pointBtn.layer.cornerRadius = 5
        self.examBtn.layer.cornerRadius = 5
        
        self.exitBtn.layer.cornerRadius = 5
        self.exitBtn.backgroundColor = GlobalVariables.purple
        
    }
    
    @objc func reloadList(){
        self.getQuestionSets()
        self.getActiveLaunch()
    }
    
    func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
        var gradientImage:UIImage?
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            gradientLayer.render(in: context)
            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        UIGraphicsEndImageContext()
        return gradientImage
    }

    @objc func back(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func getQuestionSets(){
        if SettingsViewController.online == true{
            if GlobalStaticVar.classRole == "Teacher" {
                let query = GetquestionsetsQuery(classId: self.classId, teacherId:self.myId)
                KVNProgress.show(withStatus: "", on: self.view)
                apollo.fetch(query: query) { result, error in
                    KVNProgress.dismiss()
                    //          KVNProgress.dismiss()
//                                print("result:: ",result as Any)
                    if result?.data?.questionSets != nil {
                        self.sets = (result?.data?.questionSets)! as! [GetquestionsetsQuery.Data.QuestionSet]
                        self.refresher.endRefreshing()
                        self.mytableview.reloadData()
                    }else{
                        
                    }
                }
            }else{
                let query = GetquestionsetsQuery(classId: self.classId, teacherId:self.teacherId)
                KVNProgress.show(withStatus: "", on: self.view)
                apollo.fetch(query: query) { result, error in
                    KVNProgress.dismiss()
                    //          KVNProgress.dismiss()
                    //            print("result:: ",result as Any)
                    if result?.data?.questionSets != nil {
                        self.sets = (result?.data?.questionSets)! as! [GetquestionsetsQuery.Data.QuestionSet]
                        self.refresher.endRefreshing()
                        self.mytableview.reloadData()
                        
                    }else{
                        
                    }
                }
            }
             KVNProgress.dismiss()
        }else{
            KVNProgress.showError(withStatus: "Интэрнет холболтоо шалгана уу!", on: self.view)
        }
    }
    
    func getActiveLaunch(){
        let query = ActiveLaunchTeacherQuery(teacherId: self.teacherId, classId: self.classId)
        KVNProgress.show(withStatus: "", on: self.view)
        apollo.fetch(query: query) { result, error in
            KVNProgress.dismiss()
            if result?.data?.activeLaunchTeacher != nil {
                self.ActiveLaunch = (result?.data?.activeLaunchTeacher)!
                self.refresher.endRefreshing()
                self.mytableview.reloadData()
            }else{
                
            }
        }
         KVNProgress.dismiss()
//        print("ACTIVELAUNCH :: ", self.ActiveLaunch)
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if indexPath.row == 0{
                self.performSegue(withIdentifier: "tocdio", sender: self)
            }else{
                self.performSegue(withIdentifier: "toresult", sender: self)
            }
        }else{
//            print("ActiveLaunch:: ",self.ActiveLaunch.id)
            
//            if self.ActiveLaunch.id!.trimmingCharacters(in: CharacterSet(charactersIn: " ")) == "" {
//            }
//            if self.ActiveLaunch.id! != "" || self.ActiveLaunch.id! != " "{
            if self.ActiveLaunch.id!.trimmingCharacters(in: CharacterSet(charactersIn: " ")) != "" {
                if self.ActiveLaunch.questionSetId == self.sets[indexPath.row].id{
                    self.setId = self.sets[indexPath.row].id ?? ""
                    self.roomName = self.ActiveLaunch.key ?? ""
                    self.performSegue(withIdentifier: "toActivelaunch", sender: self)
                }else{
                    GlobalStaticVar.isLaunchEnd = false
                    self.setId = self.sets[indexPath.row].id ?? ""
                    self.roomName = self.sets[indexPath.row].name ?? ""
                    self.performSegue(withIdentifier: "tolaunchSettings", sender: self)
                }
            }else{
                GlobalStaticVar.isLaunchEnd = false
                self.setId = self.sets[indexPath.row].id ?? ""
                self.roomName = self.sets[indexPath.row].name ?? ""
                self.performSegue(withIdentifier: "tolaunchSettings", sender: self)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.exams.count
        }else{
            return self.sets.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultCell")!
            cell.textLabel?.text = self.exams[indexPath.row]
            self.mytableview.separatorStyle = .singleLine
            cell.accessoryType = .disclosureIndicator
            return cell
        }else{
            self.mytableview.separatorStyle = .none
            let cell = self.mytableview.dequeueReusableCell(withIdentifier: "qset") as! QuestionSetCell
            cell.semNameLabel.text = self.sets[indexPath.row].name
            cell.questionCountLabel.text = "\("Асуултын тоо:") \(self.sets[indexPath.row].questionCount ?? 0)"
            let lastlogin = NSDate(timeIntervalSince1970: (self.sets[indexPath.row].createdDate!/1000))
            self.dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let last = self.dateformatter.string(from: lastlogin as Date)
            cell.dateLabel.text = last
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = GlobalVariables.saaral
        let headerLabel = UILabel(frame: CGRect(x: 8, y: 8, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height-5))
        headerLabel.font = UIFont(name: "segoeui", size: 16)
        headerLabel.textColor = UIColor.gray
        if section == 0 {
            headerLabel.text = "Шалгалт".uppercased()
        }else{
            headerLabel.text = "Асуултын багцууд".uppercased()
        }
        //self.tableview(self.tableview, titleForHeaderInSection: section)
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "tolaunchSettings"{
            let launchVC = segue.destination as! LaunchSettingsVC
            launchVC.setId = self.setId
            launchVC.teacherId = self.teacherId
            launchVC.classId = self.classId
            launchVC.roomName = self.roomName
        }else if segue.identifier == "toActivelaunch" {
            let controller = segue.destination as! ActiveLaunchVC
            controller.isstart = self.ActiveLaunch.started!
            controller.teacherId = self.teacherId
            controller.launchId = self.ActiveLaunch.id!
            controller.roomName = self.roomName
            controller.setId = self.setId
            controller.startedTime = self.ActiveLaunch.startedAt!
            controller.Alltime = (self.ActiveLaunch.launchSettings?.timer ?? 0.0)!
        }
    }
    @IBAction func goExam(_ sender: Any) {
        if SettingsViewController.online == true{
            self.performSegue(withIdentifier: "tocdio", sender: self)
        }else{
            KVNProgress.showError(withStatus: "Интэрнет холболтоо шалгана уу", on: self.view)
        }
        
    }
    @IBAction func showPoint(_ sender: Any) {
         if SettingsViewController.online == true{
            self.performSegue(withIdentifier: "toresult", sender: self)
         }else{
            KVNProgress.showError(withStatus: "Интэрнет холболтоо шалгана уу", on: self.view)
        }
    }
    
//    @IBAction func exitFromClass(_ sender: Any) {
//        let alert = UIAlertController(title: "Ангиас гарах", message: "Та ангиас гарахдаа итгэлтэй байна уу ?", preferredStyle: UIAlertController.Style.alert)
//        alert.addAction(UIAlertAction(title: "Тийм", style: .default  , handler: {action in
//            self.leaveclass()
//        }))
//        alert.addAction(UIAlertAction(title: "Үгүй", style: .cancel, handler: nil))
//        self.present(alert, animated: true, completion: nil)
//    }
//
//    func leaveclass(){
//        checkInternet()
//        if SettingsViewController.online {
//            let mutation = LeaveClassMutation(userId: self.myId, classId: self.classId)
//            self.apollo.perform(mutation: mutation) { result, error in
//                if result?.data?.leaveClassRoomList?.error == false{
//                    KVNProgress.showSuccess()
//                    self.navigationController?.popToRootViewController(animated: true)
//                }else{
//                    KVNProgress.showError(withStatus: result?.data?.leaveClassRoomList?.message, on: self.view)
//                }
//            }
//        }else{
//            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу")
//        }
//
//    }
}
