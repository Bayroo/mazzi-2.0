//
//  AnswersCell.swift
//  Mazzi
//
//  Created by Bayara on 10/12/18.
//  Copyright © 2018 Mazzi Application LLC. All rights reserved.
//

import Foundation
import UIKit

class AnswersCell:UITableViewCell {
    @IBOutlet var btn : UIButton!
    @IBOutlet var backview : UIView!
    @IBOutlet var label : UILabel!
    @IBOutlet weak var txtView: UITextView!
    
    func initcellData(){
        
        self.btn.layer.borderWidth = 1.0
        self.btn.layer.borderColor = GlobalVariables.purple.cgColor
        self.btn.layer.cornerRadius = 20
        self.btn.layer.masksToBounds = true
        
        self.backview.layer.cornerRadius = 10
        self.backview.layer.masksToBounds = true
        self.label.textColor = GlobalVariables.purple
        self.txtView.backgroundColor = GlobalVariables.F5F5F5
    }
}
