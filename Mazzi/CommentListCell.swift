//
//  CommentListCell.swift
//  Mazzi
//
//  Created by Bayara on 12/15/18.
//  Copyright © 2018 Mazzi Application. All rights reserved.
//

import Foundation

class CommentListCell:UITableViewCell{
    @IBOutlet weak var imgView:UIImageView!
    @IBOutlet weak var namelabel:UILabel!
    @IBOutlet weak var contentLabel:UILabel!
    @IBOutlet weak var dataLabel: UILabel!
    @IBOutlet weak var lastnameLabel: UILabel!
    @IBOutlet weak var attachedImg: UIImageView!
    
    func initcell(){
        self.imgView.layer.borderWidth = 1
        self.imgView.layer.borderColor = GlobalVariables.F5F5F5.cgColor
        self.imgView.layer.masksToBounds = true
    }
}
