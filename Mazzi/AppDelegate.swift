////
//  AppDelegate.swift
//  Mazzi
//
//  Created by Mazzi App LLC on 8/28/17.
//  Copyright © 2017 Mazzi App LLC. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications
import SocketIO
import Foundation
import AVFoundation
import Photos
import Intents
import RealmSwift
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import PushKit
import Fabric
import Crashlytics
import FirebaseDatabase
import Apollo
import Alamofire
import SwiftyJSON

var realm = try! Realm()
var ref: DatabaseReference!
var selectedgroupUser:[User_group]?
var items : Results<Conversation_realm>!
var is_thread = false
var backtitle = [""]
var senderchat = ""
var lastmessageid = ""
var userdef = UserDefaults.standard

@UIApplicationMain
//PKPushRegistryDelegate
class AppDelegate: UIResponder, UIApplicationDelegate  {
    
    var navigation:UINavigationController?
    var window: UIWindow?
    var backgroundTask = UIBackgroundTaskIdentifier.invalid
    var userId = ""
    var back = "1"
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let userDefaults = UserDefaults.standard
        if(userDefaults.object(forKey: "user_id") != nil){
            let userid = userDefaults.object(forKey: "user_id") as! String
            let token = userDefaults.object(forKey: "token") as! String
            GlobalVariables.headerToken = token
            SettingsViewController.userId = userid
            GlobalVariables.user_id = userid
            self.userId = userid
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyboard.instantiateViewController(withIdentifier: "maintab") as! MainTabbarVC
        let navigationController = UINavigationController(rootViewController: nextViewController)

//        let viewController:UIViewController = storyboard.instantiateViewController(withIdentifier: "mainnav") as! MainNavViewController
//        let viewController:UIViewController = storyboard.instantiateViewController(withIdentifier: "maintab") as! MainTabbarVC
//        rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "maintab") as! MainTabbarVC
        self.window?.rootViewController = navigationController
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in  }
        window?.makeKeyAndVisible()
        
        Auth.auth()
        ref = Database.database().reference()
        DataStore.configureMigration()
        getFireUrl(url: "my") { (suc) in
            if suc == true {
                print("url changed successfully")
            }
        }
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        let audioSession = AVAudioSession.sharedInstance()
        do {
            //            try audioSession.setCategory(AVAudioSession.Category(rawValue: convertFromAVAudioSessionCategory(AVAudioSession.Category.playback)), mode: AVAudioSession.Mode)
            try audioSession.setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.default, options: AVAudioSession.CategoryOptions.defaultToSpeaker)
        } catch {
            print("Setting category to AVAudioSessionCategoryPlayback failed.")
        }
        
        Fabric.with([Crashlytics.self])
        
        return true
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        return true
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Mazzi")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        print("my app enters applicationDidEnterBackground --------> ")
                 NotificationCenter.default.post(name: .received_notify, object: nil, userInfo: nil)
    }
    
    
    func applicationwillEnterBackground(_ application: UIApplication){
        print("my app enters applicationwillEnterBackground --------> ")
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        print("my app enters foreground --------> ")
        NotificationCenter.default.post(name: .received_notify, object: nil, userInfo: nil)
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in  }
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.sound, .badge, .alert]
            UNUserNotificationCenter.current().requestAuthorization(
                
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.sound, .badge, .alert], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
    }
}

func application(_ application: UIApplication,
                 didReceiveRemoteNotification notification: [AnyHashable : Any],
                 fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
    
    if Auth.auth().canHandleNotification(notification) {
        completionHandler(.newData)
        return
    }
    
}



func applicationWillResignActive(_ application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}

func applicationDidBecomeActive(_ application: UIApplication) {
    
}

extension AppDelegate : UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("Foreground push notifications handler")
        print("--------------------------------------")
        NotificationCenter.default.post(name: .received_notify, object: nil, userInfo: nil)
        if let userInfo = notification.request.content.userInfo as? [String : AnyObject] {
            if let keyval = userInfo["type"] as? String {
                print(userInfo)
                if keyval == "mazzichat" {
                    let topMostViewController = UIApplication.shared.topMostViewController()
                    if topMostViewController is SingleChatVC {
                        if GlobalStaticVar.activeConversationId == (userInfo["conversation_id"] as? String)! {
                        
                        }else{
                            completionHandler([.alert, .sound])
                        }
                    }else if topMostViewController is ChatListViewController{
                        completionHandler([.sound])
                    }else{
                        completionHandler([.alert, .sound, .badge])
                    }
                }else {
                    GlobalStaticVar.selectedConversationId = "mclass"
                    completionHandler([.alert])
                }
            }
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void)  {
        if let userInfo = response.notification.request.content.userInfo as? [String : AnyObject] {
            print("Background and closed  push notifications handler")
            print("--------------------------------------------------------------")
            if let conversation_id = userInfo["conversation_id"] as? String {
                if !GlobalStaticVar.badgecount.contains(conversation_id){
                    GlobalStaticVar.badgecount.append(conversation_id)
                }
                let topMostViewController = UIApplication.shared.topMostViewController()
                if topMostViewController is SingleChatVC || topMostViewController is ChatListViewController{
                    if GlobalStaticVar.selectedConversationId == (userInfo["conversation_id"] as? String)! {
                        GlobalStaticVar.clickedNotification = ""
                    }else{
                        GlobalStaticVar.clickedNotification = conversation_id
                        RooTView.updateRootVC()
                    }
                }
                else {
                    GlobalStaticVar.clickedNotification = conversation_id
                    RooTView.updateRootVC()
                }
            }else if let classId = userInfo["classId"] as? String {
                let topMostViewController = UIApplication.shared.topMostViewController()
                if topMostViewController is MClassListVC{
                    if GlobalStaticVar.selectedConversationId == (userInfo["classId"] as? String)! {
                        GlobalStaticVar.selectedConversationId = ""
                    }else{
                        GlobalStaticVar.selectedConversationId = classId
                        RooTView.updateRootVC()
                    }
                }
                else {
                    GlobalStaticVar.selectedConversationId = classId
                    RooTView.updateRootVC()
                }
            }else{
                let json = JSON.init(parseJSON: userInfo["notification"] as! String)
                GlobalStaticVar.selectedClassId = json["classId"].string!
                RooTView.updateRootVC()
            }
        }
        completionHandler()
    }
}
class RooTView {
    static func updateRootVC(){
        var rootVC : UIViewController?
//        rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainnav") as! MainNavViewController
         rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "maintab") as! MainTabbarVC
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = rootVC
    }
}

//let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "maintab") as? MainTabbarVC
//self.navigationController?.pushViewController(viewController!, animated: true)

extension AppDelegate:MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        //        print("Firebase registration token: \(fcmToken)")
        GlobalVariables.fcm_token = fcmToken
        UserDefaults.standard.setValue(fcmToken, forKey: "fcm_token")
        UserDefaults.standard.synchronize()
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
    return input.rawValue
}

extension UIApplication {
    //    func homeVC() -> UIViewController? {
    //        return self.keyWindow?.rootViewController?.revealViewController()
    //    }
    func topMostViewController() -> UIViewController? {
        return self.keyWindow?.rootViewController?.topMostViewController()
    }
}

extension UIViewController {
    
    func topMostViewController() -> UIViewController {
        
        if let presented = self.presentedViewController {
            return presented.topMostViewController()
        }
        
        if let navigation = self as? UINavigationController {
            return navigation.visibleViewController?.topMostViewController() ?? navigation
        }
        
        if let tab = self as? UITabBarController {
            return tab.selectedViewController?.topMostViewController() ?? tab
        }
        
        return self
    }
}

extension Notification.Name {
    //socket
    static let received_notify = Notification.Name("received_notify")
    static let received_notify_without_badge = Notification.Name("received_notify_without_badge")
}

