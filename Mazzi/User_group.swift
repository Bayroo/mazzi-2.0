//
//  User_group.swift
//  Mazzi
//
//  Created by Janibekm on 9/28/17.
//  Copyright © 2017 Mazzi App LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire

class User_group:NSObject {
    
    var id:String = ""
    var image:String = ""
    var nickname:String = ""
    var lastname:String = ""
    var surname:String = ""
    var phone:String = ""
    var state:Int = 0
    var online_at:Double = 0
    var fcm_token:String = ""
    var registerId:String = ""
    //    var phones = [String]()
    //    var gender:String = ""
        var birthday:Double = 0
    //    var created_at:Double = 0
    
    class func updateprofile(profile: [String: Any], userids: String, completion: @escaping (Bool) -> Swift.Void)  {
    
        let baseurl = SettingsViewController.baseurl
     //   let headers = SettingsViewController.headers
        let token = UserDefaults.standard.object(forKey:"token") as! String
//        print("token: ", token)
//        print("baseUrl: ", baseurl)
//        print("header: " , headers)
        let  header = ["x-access-token": token, "authorization": "BPXjEvWfXxx5qpQHHFMEJEB8fA3xAczC"]
        Alamofire.request("\(baseurl)update_pro", method: .post, parameters: profile, encoding: JSONEncoding.default,headers:header).responseJSON{ response in
            
            if response.response?.statusCode == 200 {
                
                let res = JSON(response.value!)
                print("RESULT JSON:" , res)
                if res["error"].bool == false {
//                    print("true")
                    completion(true)
                    
                }else{
//                    print("falsee")
                    
                    completion(false)
                }
            }else{
//                print("false")
                completion(false)
            }
        }
    }
    
    
    init(id:String, image: String, nickname: String, lastname: String, surname: String, phone: String, state: Int, online_at: Double,fcm_token:String,registerId:String) {
        self.id = id
        self.image = image
        self.nickname = nickname
        self.lastname = lastname
        self.surname = surname
        self.phone = phone
        self.state = state
        self.online_at = online_at
        self.fcm_token = fcm_token
        self.registerId = registerId
        //user only contacts
        //        self.phones = phones
        //        self.gender = gender
        //        self.birthday = birthday
        //        self.created_at = created_at
    }
}

class Contacts:NSObject {
    var name: String = ""
    var lastname:String = ""
    var surname:String = ""
    var image: String = ""
    var phones = [String]()
    var phone:String = ""
    var id: String = ""
    var gender:String = ""
    var birthday:Double = 0
    var state: Int = 0
    var online_at: Double = 0
    var created_at: Double = 0
    var fcm_token:String = ""
    
    init(id: String, name: String, lastname:String, surname:String, image: String, phones: [String], phone: String, gender: String,birthday:Double, state: Int, online_at: Double,created_at:Double,fcm_token:String){
        self.id = id
        self.name = name
        self.lastname = lastname
        self.surname = surname
        self.phones = phones
        self.phone = phone
        self.image = image
        self.state = state
        self.gender = gender
        self.birthday = birthday
        self.online_at = online_at
        self.created_at = created_at
        self.fcm_token = fcm_token
        
    }
    
    
}
