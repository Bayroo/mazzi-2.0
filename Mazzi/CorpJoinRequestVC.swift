//
//  CorpJoinRequestVC.swift
//  Mazzi
//
//  Created by Bayara on 5/22/19.
//  Copyright © 2019 Mazz Application. All rights reserved.
//

import Foundation
import UIKit
import Apollo
import KVNProgress

class CorpJoinVC: UIViewController {
    
    @IBOutlet weak var txtfield: UITextField!
    @IBOutlet weak var sendBtn: UIButton!
    
    let apollo: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
        let url = URL(string:SettingsViewController.graphQL)!
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.txtfield.becomeFirstResponder()
    }
    
    func customize(){
        self.title = "Хүсэлт илгээх"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(backtoRoot))
        self.txtfield.layer.borderColor = GlobalVariables.purple.cgColor
        self.txtfield.layer.borderWidth = 1
        self.txtfield.layer.cornerRadius = 5
        
        self.sendBtn.backgroundColor = GlobalVariables.todpurple
        self.sendBtn.setTitleColor(GlobalVariables.TextTod, for: UIControl.State.normal)
        self.sendBtn.addTarget(self, action: #selector(sendReq), for: .touchUpInside)
        self.txtfield.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        
        self.sendBtn.layer.cornerRadius = 5
        self.sendBtn.layer.masksToBounds = true
        self.sendBtn.backgroundColor = GlobalVariables.purple
        self.sendBtn.titleLabel?.textColor = UIColor.white
    }
    
    @objc func backtoRoot(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissKeyboard(){
        self.txtfield.resignFirstResponder()
    }
    
    @objc func textFieldDidChange(textField:UITextField){
        self.txtfield.textColor = UIColor.black
    }
    
    @objc func sendReq() {
        checkInternet()
        if SettingsViewController.online {
            if self.txtfield.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")) == ""{
                self.txtfield.placeholder = "Байгууллагын код оруулна уу"
            }else{
                let mutation = JoinCorporateMutation(userId: SettingsViewController.userId, corpId: self.txtfield.text!)
                self.apollo.perform(mutation: mutation) { result, error in
                    //                    print("res:", result?.data as Any)
                    if  (result?.data?.joinCorporate?.error == false) {
                        KVNProgress.showSuccess(withStatus: "Хүсэлт амжилттай илгээгдлээ.")
                        self.backtoRoot()
                    }else {
                        KVNProgress.showError(withStatus: result?.data?.joinCorporate?.message)
                    }
                }
            }
        }else{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу!")
        }
    }
}
