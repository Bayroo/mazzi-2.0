//
//  inviteContacts.swift
//  Mazzi
//
//  Created by Bayara on 2/18/19.
//  Copyright © 2019 Mazz Application. All rights reserved.
//

import Foundation
import RealmSwift
import MessageUI

class inviteContacts:UIViewController,UITableViewDelegate, UITableViewDataSource,MFMessageComposeViewControllerDelegate,UISearchBarDelegate{
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
         self.dismiss(animated: true, completion: nil)
//        self.navigationController?.popViewController(animated: false)
    }
    
  
    @IBOutlet weak var mysearchBar: UISearchBar!
    @IBOutlet weak var myTableview : UITableView!
    var contacts : Results<Contacts_realm>!
    var  filteredContacts : Results<Contacts_realm>!
    var searchActive = false
    var refresher = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Найзаа урих".uppercased()
        self.myTableview.delegate = self
        self.myTableview.dataSource = self
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "05"), style: .plain, target: self, action: #selector(back))
        self.myTableview.register((UINib(nibName: "MoreTableViewCell", bundle: nil)), forCellReuseIdentifier: "more")
        self.contacts = realm.objects(Contacts_realm.self).sorted(byKeyPath: "name", ascending: true).filter("created_at == 0")
        self.mysearchBar.barTintColor = GlobalVariables.F5F5F5
        self.mysearchBar.tintColor = GlobalVariables.todpurple
        self.mysearchBar.delegate = self
        self.myTableview.refreshControl = self.refresher
        self.refresher.addTarget(self, action: #selector(reloadContacts), for: .valueChanged)
        self.myTableview.reloadData()
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        navigationController?.navigationBar.barTintColor = GlobalVariables.black
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    @objc func reloadContacts(){
        self.contacts = realm.objects(Contacts_realm.self).sorted(byKeyPath: "name", ascending: true).filter("created_at == 0")
        self.refresher.endRefreshing()
        self.myTableview.reloadData()
    }
    
    @objc func back(){
         self.dismiss(animated: true, completion: nil)
//        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.searchActive == true{
            return self.filteredContacts.count
        }else{
            return self.contacts.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "more", for: indexPath) as! MoreTableViewCell
        
        if self.searchActive == true {
            let name = self.filteredContacts[indexPath.row].name
            let lastname = self.filteredContacts[indexPath.row].lastname
            cell.inviteBtn.tag = indexPath.row
            cell.inviteBtn.addTarget(self, action: #selector(sendInvite(_:)), for: .touchUpInside)
            cell.nameLabel?.text = "\(name) \(lastname)"
            
            let phone = self.filteredContacts[indexPath.row].phone
            let phonetext = self.filteredContacts[indexPath.row].phones.map({$0.phones}).joined(separator: ", ")
            
            if phone == ""{
                cell.numberLabel.text = phonetext
            }else{
                cell.numberLabel.text = phone
            }
        }else{
            let name = self.contacts[indexPath.row].name
            let lastname = self.contacts[indexPath.row].lastname
            cell.inviteBtn.tag = indexPath.row
            cell.inviteBtn.addTarget(self, action: #selector(sendInvite(_:)), for: .touchUpInside)
            cell.nameLabel?.text = "\(name) \(lastname)"
            
            let phone = self.contacts[indexPath.row].phone
            let phonetext = self.contacts[indexPath.row].phones.map({$0.phones}).joined(separator: ", ")
            
            if phone == ""{
                cell.numberLabel.text = phonetext
            }else{
                cell.numberLabel.text = phone
            }
        }
      
        return cell
    }
    
    @objc func sendInvite(_ sender:Any){
        if searchActive {
            let controller = MFMessageComposeViewController()
            controller.body = "Маззи апп-г татаарай. https://mazzi.mn/"
            var phonetext = self.contacts[(sender as AnyObject).tag].phones.map({$0.phones}).joined(separator: ", ")
            if phonetext == "" {
                phonetext = self.contacts[(sender as AnyObject).tag].phone
            }
            controller.recipients = [phonetext]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }else{
            let controller = MFMessageComposeViewController()
            controller.body = "Маззи апп-г татаарай. https://mazzi.mn/"
            var phonetext = self.contacts[(sender as AnyObject).tag].phones.map({$0.phones}).joined(separator: ", ")
            if phonetext == "" {
                phonetext = self.contacts[(sender as AnyObject).tag].phone
            }
            controller.recipients = [phonetext]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    //-------------------  SEARCH  -------------------------------------
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.mysearchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.mysearchBar.showsCancelButton = false
        self.mysearchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.mysearchBar.resignFirstResponder()
        self.mysearchBar.text = ""
        searchActive = false
        self.myTableview.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text != "" {
            let searchText = searchBar.text?.lowercased()
            searchActive = true
            if (searchText?.isAlphanumeric)!{
                self.filteredContacts = self.contacts.filter("name CONTAINS[c]'\(searchText!)'")
                if self.filteredContacts.isEmpty{
                    self.filteredContacts = self.contacts.filter("name CONTAINS[c]'\(searchText!)'")
                }
                self.myTableview.reloadData()
            }else{
//
//                for i in 0..<self.contacts.count{
//                    let phonetext = self.contacts[i].phones.map({$0.phones}).joined(separator: ", ")
//
//
//                }
                
//               let filteredUsers = users.toArray.filter {$0.name.lowercased().contains(searchText.lowercased())}
//                let filteredUsers = self.contacts.filter {$0.phones.filter{$0.phones == searchText!)}}
                
                self.filteredContacts = self.contacts.filter("phone CONTAINS '\(searchText!)'")
                self.myTableview.reloadData()
            }
        }else{
            searchActive = false
            self.myTableview.reloadData()
        }
    }
}
