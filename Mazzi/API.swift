//  This file was automatically generated and should not be edited.

import Apollo

public struct AnswerInputType: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(studentId: Swift.Optional<String?> = nil, questionId: Swift.Optional<String?> = nil, launchId: Swift.Optional<String?> = nil, intAnswer: Swift.Optional<[String?]?> = nil, stringAnswer: Swift.Optional<String?> = nil) {
    graphQLMap = ["studentId": studentId, "questionId": questionId, "launchId": launchId, "intAnswer": intAnswer, "stringAnswer": stringAnswer]
  }

  public var studentId: Swift.Optional<String?> {
    get {
      return graphQLMap["studentId"] as! Swift.Optional<String?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "studentId")
    }
  }

  public var questionId: Swift.Optional<String?> {
    get {
      return graphQLMap["questionId"] as! Swift.Optional<String?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "questionId")
    }
  }

  public var launchId: Swift.Optional<String?> {
    get {
      return graphQLMap["launchId"] as! Swift.Optional<String?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "launchId")
    }
  }

  public var intAnswer: Swift.Optional<[String?]?> {
    get {
      return graphQLMap["intAnswer"] as! Swift.Optional<[String?]?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "intAnswer")
    }
  }

  public var stringAnswer: Swift.Optional<String?> {
    get {
      return graphQLMap["stringAnswer"] as! Swift.Optional<String?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "stringAnswer")
    }
  }
}

public struct InputPost: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(ownerId: String, content: String, link: Swift.Optional<[String?]?> = nil, file: Swift.Optional<[InputPostFile?]?> = nil, topicId: Swift.Optional<String?> = nil, topic: Swift.Optional<String?> = nil) {
    graphQLMap = ["ownerId": ownerId, "content": content, "link": link, "file": file, "topicId": topicId, "topic": topic]
  }

  public var ownerId: String {
    get {
      return graphQLMap["ownerId"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ownerId")
    }
  }

  public var content: String {
    get {
      return graphQLMap["content"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "content")
    }
  }

  public var link: Swift.Optional<[String?]?> {
    get {
      return graphQLMap["link"] as! Swift.Optional<[String?]?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "link")
    }
  }

  public var file: Swift.Optional<[InputPostFile?]?> {
    get {
      return graphQLMap["file"] as! Swift.Optional<[InputPostFile?]?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "file")
    }
  }

  public var topicId: Swift.Optional<String?> {
    get {
      return graphQLMap["topicId"] as! Swift.Optional<String?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "topicId")
    }
  }

  public var topic: Swift.Optional<String?> {
    get {
      return graphQLMap["topic"] as! Swift.Optional<String?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "topic")
    }
  }
}

public struct InputPostFile: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(`extension`: Swift.Optional<String?> = nil, url: String, name: Swift.Optional<String?> = nil, size: Swift.Optional<Double?> = nil, thumbnail: Swift.Optional<String?> = nil) {
    graphQLMap = ["extension": `extension`, "url": url, "name": name, "size": size, "thumbnail": thumbnail]
  }

  public var `extension`: Swift.Optional<String?> {
    get {
      return graphQLMap["extension"] as! Swift.Optional<String?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "extension")
    }
  }

  public var url: String {
    get {
      return graphQLMap["url"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "url")
    }
  }

  public var name: Swift.Optional<String?> {
    get {
      return graphQLMap["name"] as! Swift.Optional<String?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "name")
    }
  }

  public var size: Swift.Optional<Double?> {
    get {
      return graphQLMap["size"] as! Swift.Optional<Double?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "size")
    }
  }

  public var thumbnail: Swift.Optional<String?> {
    get {
      return graphQLMap["thumbnail"] as! Swift.Optional<String?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "thumbnail")
    }
  }
}

public struct UpdatePost: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(content: String, link: Swift.Optional<[String?]?> = nil, file: Swift.Optional<[InputPostFile?]?> = nil, topicId: Swift.Optional<String?> = nil, topic: Swift.Optional<String?> = nil) {
    graphQLMap = ["content": content, "link": link, "file": file, "topicId": topicId, "topic": topic]
  }

  public var content: String {
    get {
      return graphQLMap["content"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "content")
    }
  }

  public var link: Swift.Optional<[String?]?> {
    get {
      return graphQLMap["link"] as! Swift.Optional<[String?]?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "link")
    }
  }

  public var file: Swift.Optional<[InputPostFile?]?> {
    get {
      return graphQLMap["file"] as! Swift.Optional<[InputPostFile?]?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "file")
    }
  }

  public var topicId: Swift.Optional<String?> {
    get {
      return graphQLMap["topicId"] as! Swift.Optional<String?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "topicId")
    }
  }

  public var topic: Swift.Optional<String?> {
    get {
      return graphQLMap["topic"] as! Swift.Optional<String?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "topic")
    }
  }
}

public struct InputPostComment: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(ownerId: String, content: String, file: Swift.Optional<InputPostFile?> = nil) {
    graphQLMap = ["ownerId": ownerId, "content": content, "file": file]
  }

  public var ownerId: String {
    get {
      return graphQLMap["ownerId"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ownerId")
    }
  }

  public var content: String {
    get {
      return graphQLMap["content"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "content")
    }
  }

  public var file: Swift.Optional<InputPostFile?> {
    get {
      return graphQLMap["file"] as! Swift.Optional<InputPostFile?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "file")
    }
  }
}

/// input LaunchSettingsInput{
/// security: Boolean
/// shuffleQuestion: Boolean
/// shuffleAnswer: Boolean
/// showResult:Boolean
/// oneTry:Boolean
/// attendanceRequired:Boolean
/// reflection:Boolean
/// timer: Float
/// }
/// input InputlaunchData {
/// attendanceId:String
/// }
/// type launchSettings{
/// security: Boolean!
/// shuffleQuestion: Boolean!
/// shuffleAnswer: Boolean!
/// showResult:Boolean!
/// oneTry:Boolean!
/// reflection:Boolean!
/// timer: Float
/// }
/// type Launch {
/// _id: ObjectID
/// key: String
/// teacherId: ObjectID
/// questionSetId: ObjectID
/// classId:ObjectID
/// isActive: Boolean
/// launchedAt: Float
/// launchSettings: launchSettings
/// started: Boolean
/// startedAt: Float
/// }
/// type LaunchWithResult{
/// _id: ObjectID
/// key: String
/// teacherId: ObjectID
/// questionSetId: ObjectID
/// classId:ObjectID
/// isActive: Boolean
/// launchedAt: Float
/// launchSettings: launchSettings
/// started: Boolean
/// startedAt: Float
/// result:Result
/// }
/// type TempLaunch {
/// _id: ObjectID
/// key: String
/// teacherId: ObjectID
/// seminarId: ObjectID
/// isActive: Boolean
/// launchedAt: Float
/// launchSettings: launchSettings
/// started: Boolean
/// startedAt: Float
/// }
/// type studentCheckLaunch{
/// error:Boolean
/// status:String
/// response: String
/// launch: TempLaunch
/// }
/// type LaunchResponse {
/// error: Boolean
/// message:String
/// result:Launch
/// }
/// type LaunchWithAvg{
/// _id: ObjectID
/// key: String
/// teacherId: ObjectID
/// questionSetId: ObjectID
/// isActive: Boolean
/// launchedAt: Float
/// launchSettings: launchSettings
/// started: Boolean
/// average: Float
/// }
/// type Query {
/// checkActiveLaunch(key: String!): Launch
/// studentCheckLaunch(studentId: String!, key: String!, classId: String!):studentCheckLaunch
/// launchs(questionSetId: String!): [Launch]
/// launch(launchId: String!):Launch
/// activeLaunchTeacher(teacherId: String!, classId: String!):Launch
/// recentLaunchTeacher(teacherId: String!):[LaunchWithAvg]
/// }
/// type Mutation {
/// enableLaunch(teacherId: String!, questionSetId: String!, launchSettings: LaunchSettingsInput!,InputlaunchData:InputlaunchData, classId:String!):Launch
/// }
public struct LaunchSettingsInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(security: Swift.Optional<Bool?> = nil, shuffleQuestion: Swift.Optional<Bool?> = nil, shuffleAnswer: Swift.Optional<Bool?> = nil, showResult: Swift.Optional<Bool?> = nil, oneTry: Swift.Optional<Bool?> = nil, attendanceRequired: Swift.Optional<Bool?> = nil, reflection: Swift.Optional<Bool?> = nil, timer: Swift.Optional<Double?> = nil) {
    graphQLMap = ["security": security, "shuffleQuestion": shuffleQuestion, "shuffleAnswer": shuffleAnswer, "showResult": showResult, "oneTry": oneTry, "attendanceRequired": attendanceRequired, "reflection": reflection, "timer": timer]
  }

  public var security: Swift.Optional<Bool?> {
    get {
      return graphQLMap["security"] as! Swift.Optional<Bool?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "security")
    }
  }

  public var shuffleQuestion: Swift.Optional<Bool?> {
    get {
      return graphQLMap["shuffleQuestion"] as! Swift.Optional<Bool?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "shuffleQuestion")
    }
  }

  public var shuffleAnswer: Swift.Optional<Bool?> {
    get {
      return graphQLMap["shuffleAnswer"] as! Swift.Optional<Bool?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "shuffleAnswer")
    }
  }

  public var showResult: Swift.Optional<Bool?> {
    get {
      return graphQLMap["showResult"] as! Swift.Optional<Bool?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "showResult")
    }
  }

  public var oneTry: Swift.Optional<Bool?> {
    get {
      return graphQLMap["oneTry"] as! Swift.Optional<Bool?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "oneTry")
    }
  }

  public var attendanceRequired: Swift.Optional<Bool?> {
    get {
      return graphQLMap["attendanceRequired"] as! Swift.Optional<Bool?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "attendanceRequired")
    }
  }

  public var reflection: Swift.Optional<Bool?> {
    get {
      return graphQLMap["reflection"] as! Swift.Optional<Bool?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "reflection")
    }
  }

  public var timer: Swift.Optional<Double?> {
    get {
      return graphQLMap["timer"] as! Swift.Optional<Double?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "timer")
    }
  }
}

public struct InputlaunchData: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(attendanceId: Swift.Optional<String?> = nil) {
    graphQLMap = ["attendanceId": attendanceId]
  }

  public var attendanceId: Swift.Optional<String?> {
    get {
      return graphQLMap["attendanceId"] as! Swift.Optional<String?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "attendanceId")
    }
  }
}

public struct InputClassSettings: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(restrictedPost: Swift.Optional<Bool?> = nil, approvePost: Swift.Optional<Bool?> = nil) {
    graphQLMap = ["restrictedPost": restrictedPost, "approvePost": approvePost]
  }

  public var restrictedPost: Swift.Optional<Bool?> {
    get {
      return graphQLMap["restrictedPost"] as! Swift.Optional<Bool?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "restrictedPost")
    }
  }

  public var approvePost: Swift.Optional<Bool?> {
    get {
      return graphQLMap["approvePost"] as! Swift.Optional<Bool?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "approvePost")
    }
  }
}

public struct InputAttendance: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(settings: InputAttendanceSettings, location: Swift.Optional<InputAttendanceLocation?> = nil, radius: Swift.Optional<Double?> = nil, time: Swift.Optional<InputAttendanceTime?> = nil, topicId: Swift.Optional<String?> = nil, classTime: Swift.Optional<Double?> = nil, type: Swift.Optional<String?> = nil) {
    graphQLMap = ["settings": settings, "location": location, "radius": radius, "time": time, "topicId": topicId, "classTime": classTime, "type": type]
  }

  public var settings: InputAttendanceSettings {
    get {
      return graphQLMap["settings"] as! InputAttendanceSettings
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "settings")
    }
  }

  public var location: Swift.Optional<InputAttendanceLocation?> {
    get {
      return graphQLMap["location"] as! Swift.Optional<InputAttendanceLocation?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "location")
    }
  }

  public var radius: Swift.Optional<Double?> {
    get {
      return graphQLMap["radius"] as! Swift.Optional<Double?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "radius")
    }
  }

  public var time: Swift.Optional<InputAttendanceTime?> {
    get {
      return graphQLMap["time"] as! Swift.Optional<InputAttendanceTime?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "time")
    }
  }

  public var topicId: Swift.Optional<String?> {
    get {
      return graphQLMap["topicId"] as! Swift.Optional<String?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "topicId")
    }
  }

  public var classTime: Swift.Optional<Double?> {
    get {
      return graphQLMap["classTime"] as! Swift.Optional<Double?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "classTime")
    }
  }

  public var type: Swift.Optional<String?> {
    get {
      return graphQLMap["type"] as! Swift.Optional<String?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "type")
    }
  }
}

public struct InputAttendanceSettings: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(enforceGeoLocation: Bool) {
    graphQLMap = ["enforceGeoLocation": enforceGeoLocation]
  }

  public var enforceGeoLocation: Bool {
    get {
      return graphQLMap["enforceGeoLocation"] as! Bool
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "enforceGeoLocation")
    }
  }
}

public struct InputAttendanceLocation: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(latitude: Swift.Optional<Double?> = nil, longitude: Swift.Optional<Double?> = nil) {
    graphQLMap = ["latitude": latitude, "longitude": longitude]
  }

  public var latitude: Swift.Optional<Double?> {
    get {
      return graphQLMap["latitude"] as! Swift.Optional<Double?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "latitude")
    }
  }

  public var longitude: Swift.Optional<Double?> {
    get {
      return graphQLMap["longitude"] as! Swift.Optional<Double?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "longitude")
    }
  }
}

public struct InputAttendanceTime: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(timeEnds: Swift.Optional<Double?> = nil, timeBegan: Swift.Optional<Double?> = nil) {
    graphQLMap = ["timeEnds": timeEnds, "timeBegan": timeBegan]
  }

  public var timeEnds: Swift.Optional<Double?> {
    get {
      return graphQLMap["timeEnds"] as! Swift.Optional<Double?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "timeEnds")
    }
  }

  public var timeBegan: Swift.Optional<Double?> {
    get {
      return graphQLMap["timeBegan"] as! Swift.Optional<Double?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "timeBegan")
    }
  }
}

public struct InputObjectTopic: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(name: Swift.Optional<String?> = nil) {
    graphQLMap = ["name": name]
  }

  public var name: Swift.Optional<String?> {
    get {
      return graphQLMap["name"] as! Swift.Optional<String?>
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "name")
    }
  }
}

public final class StudentCheckLaunchQuery: GraphQLQuery {
  public let operationDefinition =
    "query studentCheckLaunch($studentId: String!, $key: String!, $classId: String!) {\n  studentCheckLaunch(studentId: $studentId, key: $key, classId: $classId) {\n    __typename\n    error\n    status\n    response\n    launch {\n      __typename\n      _id\n      key\n      teacherId\n      seminarId\n      isActive\n      launchedAt\n      launchSettings {\n        __typename\n        security\n        shuffleAnswer\n        shuffleQuestion\n        showResult\n        oneTry\n        reflection\n        timer\n      }\n      started\n      startedAt\n      type\n    }\n  }\n}"

  public var studentId: String
  public var key: String
  public var classId: String

  public init(studentId: String, key: String, classId: String) {
    self.studentId = studentId
    self.key = key
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["studentId": studentId, "key": key, "classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("studentCheckLaunch", arguments: ["studentId": GraphQLVariable("studentId"), "key": GraphQLVariable("key"), "classId": GraphQLVariable("classId")], type: .object(StudentCheckLaunch.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(studentCheckLaunch: StudentCheckLaunch? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "studentCheckLaunch": studentCheckLaunch.flatMap { (value: StudentCheckLaunch) -> ResultMap in value.resultMap }])
    }

    public var studentCheckLaunch: StudentCheckLaunch? {
      get {
        return (resultMap["studentCheckLaunch"] as? ResultMap).flatMap { StudentCheckLaunch(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "studentCheckLaunch")
      }
    }

    public struct StudentCheckLaunch: GraphQLSelectionSet {
      public static let possibleTypes = ["studentCheckLaunch"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("status", type: .scalar(String.self)),
        GraphQLField("response", type: .scalar(String.self)),
        GraphQLField("launch", type: .object(Launch.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, status: String? = nil, response: String? = nil, launch: Launch? = nil) {
        self.init(unsafeResultMap: ["__typename": "studentCheckLaunch", "error": error, "status": status, "response": response, "launch": launch.flatMap { (value: Launch) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var response: String? {
        get {
          return resultMap["response"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "response")
        }
      }

      public var launch: Launch? {
        get {
          return (resultMap["launch"] as? ResultMap).flatMap { Launch(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "launch")
        }
      }

      public struct Launch: GraphQLSelectionSet {
        public static let possibleTypes = ["TempLaunch"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("key", type: .scalar(String.self)),
          GraphQLField("teacherId", type: .scalar(String.self)),
          GraphQLField("seminarId", type: .scalar(String.self)),
          GraphQLField("isActive", type: .scalar(Bool.self)),
          GraphQLField("launchedAt", type: .scalar(Double.self)),
          GraphQLField("launchSettings", type: .object(LaunchSetting.selections)),
          GraphQLField("started", type: .scalar(Bool.self)),
          GraphQLField("startedAt", type: .scalar(Double.self)),
          GraphQLField("type", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, key: String? = nil, teacherId: String? = nil, seminarId: String? = nil, isActive: Bool? = nil, launchedAt: Double? = nil, launchSettings: LaunchSetting? = nil, started: Bool? = nil, startedAt: Double? = nil, type: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "TempLaunch", "_id": id, "key": key, "teacherId": teacherId, "seminarId": seminarId, "isActive": isActive, "launchedAt": launchedAt, "launchSettings": launchSettings.flatMap { (value: LaunchSetting) -> ResultMap in value.resultMap }, "started": started, "startedAt": startedAt, "type": type])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var key: String? {
          get {
            return resultMap["key"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "key")
          }
        }

        public var teacherId: String? {
          get {
            return resultMap["teacherId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "teacherId")
          }
        }

        public var seminarId: String? {
          get {
            return resultMap["seminarId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "seminarId")
          }
        }

        public var isActive: Bool? {
          get {
            return resultMap["isActive"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "isActive")
          }
        }

        public var launchedAt: Double? {
          get {
            return resultMap["launchedAt"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "launchedAt")
          }
        }

        public var launchSettings: LaunchSetting? {
          get {
            return (resultMap["launchSettings"] as? ResultMap).flatMap { LaunchSetting(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "launchSettings")
          }
        }

        public var started: Bool? {
          get {
            return resultMap["started"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "started")
          }
        }

        public var startedAt: Double? {
          get {
            return resultMap["startedAt"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "startedAt")
          }
        }

        public var type: String? {
          get {
            return resultMap["type"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "type")
          }
        }

        public struct LaunchSetting: GraphQLSelectionSet {
          public static let possibleTypes = ["launchSettings"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("security", type: .nonNull(.scalar(Bool.self))),
            GraphQLField("shuffleAnswer", type: .nonNull(.scalar(Bool.self))),
            GraphQLField("shuffleQuestion", type: .nonNull(.scalar(Bool.self))),
            GraphQLField("showResult", type: .nonNull(.scalar(Bool.self))),
            GraphQLField("oneTry", type: .nonNull(.scalar(Bool.self))),
            GraphQLField("reflection", type: .nonNull(.scalar(Bool.self))),
            GraphQLField("timer", type: .scalar(Double.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(security: Bool, shuffleAnswer: Bool, shuffleQuestion: Bool, showResult: Bool, oneTry: Bool, reflection: Bool, timer: Double? = nil) {
            self.init(unsafeResultMap: ["__typename": "launchSettings", "security": security, "shuffleAnswer": shuffleAnswer, "shuffleQuestion": shuffleQuestion, "showResult": showResult, "oneTry": oneTry, "reflection": reflection, "timer": timer])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var security: Bool {
            get {
              return resultMap["security"]! as! Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "security")
            }
          }

          public var shuffleAnswer: Bool {
            get {
              return resultMap["shuffleAnswer"]! as! Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "shuffleAnswer")
            }
          }

          public var shuffleQuestion: Bool {
            get {
              return resultMap["shuffleQuestion"]! as! Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "shuffleQuestion")
            }
          }

          public var showResult: Bool {
            get {
              return resultMap["showResult"]! as! Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "showResult")
            }
          }

          public var oneTry: Bool {
            get {
              return resultMap["oneTry"]! as! Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "oneTry")
            }
          }

          public var reflection: Bool {
            get {
              return resultMap["reflection"]! as! Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "reflection")
            }
          }

          public var timer: Double? {
            get {
              return resultMap["timer"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "timer")
            }
          }
        }
      }
    }
  }
}

public final class QuestionQuery: GraphQLQuery {
  public let operationDefinition =
    "query question($token: String!, $classId: String!) {\n  studentTakeQuestion(token: $token, classId: $classId) {\n    __typename\n    _id\n    content\n    isActive\n    questionType\n    options {\n      __typename\n      _id\n      content\n      image\n      formula\n      isCorrect\n    }\n    optionType\n    explanation\n    image\n    weight\n    answerLength\n  }\n}"

  public var token: String
  public var classId: String

  public init(token: String, classId: String) {
    self.token = token
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["token": token, "classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("studentTakeQuestion", arguments: ["token": GraphQLVariable("token"), "classId": GraphQLVariable("classId")], type: .list(.object(StudentTakeQuestion.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(studentTakeQuestion: [StudentTakeQuestion?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "studentTakeQuestion": studentTakeQuestion.flatMap { (value: [StudentTakeQuestion?]) -> [ResultMap?] in value.map { (value: StudentTakeQuestion?) -> ResultMap? in value.flatMap { (value: StudentTakeQuestion) -> ResultMap in value.resultMap } } }])
    }

    public var studentTakeQuestion: [StudentTakeQuestion?]? {
      get {
        return (resultMap["studentTakeQuestion"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [StudentTakeQuestion?] in value.map { (value: ResultMap?) -> StudentTakeQuestion? in value.flatMap { (value: ResultMap) -> StudentTakeQuestion in StudentTakeQuestion(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [StudentTakeQuestion?]) -> [ResultMap?] in value.map { (value: StudentTakeQuestion?) -> ResultMap? in value.flatMap { (value: StudentTakeQuestion) -> ResultMap in value.resultMap } } }, forKey: "studentTakeQuestion")
      }
    }

    public struct StudentTakeQuestion: GraphQLSelectionSet {
      public static let possibleTypes = ["studentTakeQuestion"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("_id", type: .scalar(String.self)),
        GraphQLField("content", type: .scalar(String.self)),
        GraphQLField("isActive", type: .scalar(Bool.self)),
        GraphQLField("questionType", type: .scalar(String.self)),
        GraphQLField("options", type: .list(.object(Option.selections))),
        GraphQLField("optionType", type: .scalar(String.self)),
        GraphQLField("explanation", type: .scalar(String.self)),
        GraphQLField("image", type: .scalar(String.self)),
        GraphQLField("weight", type: .scalar(Int.self)),
        GraphQLField("answerLength", type: .scalar(Int.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, content: String? = nil, isActive: Bool? = nil, questionType: String? = nil, options: [Option?]? = nil, optionType: String? = nil, explanation: String? = nil, image: String? = nil, weight: Int? = nil, answerLength: Int? = nil) {
        self.init(unsafeResultMap: ["__typename": "studentTakeQuestion", "_id": id, "content": content, "isActive": isActive, "questionType": questionType, "options": options.flatMap { (value: [Option?]) -> [ResultMap?] in value.map { (value: Option?) -> ResultMap? in value.flatMap { (value: Option) -> ResultMap in value.resultMap } } }, "optionType": optionType, "explanation": explanation, "image": image, "weight": weight, "answerLength": answerLength])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["_id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "_id")
        }
      }

      public var content: String? {
        get {
          return resultMap["content"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "content")
        }
      }

      public var isActive: Bool? {
        get {
          return resultMap["isActive"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isActive")
        }
      }

      public var questionType: String? {
        get {
          return resultMap["questionType"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "questionType")
        }
      }

      public var options: [Option?]? {
        get {
          return (resultMap["options"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Option?] in value.map { (value: ResultMap?) -> Option? in value.flatMap { (value: ResultMap) -> Option in Option(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Option?]) -> [ResultMap?] in value.map { (value: Option?) -> ResultMap? in value.flatMap { (value: Option) -> ResultMap in value.resultMap } } }, forKey: "options")
        }
      }

      public var optionType: String? {
        get {
          return resultMap["optionType"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "optionType")
        }
      }

      public var explanation: String? {
        get {
          return resultMap["explanation"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "explanation")
        }
      }

      public var image: String? {
        get {
          return resultMap["image"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "image")
        }
      }

      public var weight: Int? {
        get {
          return resultMap["weight"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "weight")
        }
      }

      public var answerLength: Int? {
        get {
          return resultMap["answerLength"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "answerLength")
        }
      }

      public struct Option: GraphQLSelectionSet {
        public static let possibleTypes = ["QuestionOption"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("content", type: .scalar(String.self)),
          GraphQLField("image", type: .scalar(String.self)),
          GraphQLField("formula", type: .scalar(String.self)),
          GraphQLField("isCorrect", type: .scalar(Bool.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, content: String? = nil, image: String? = nil, formula: String? = nil, isCorrect: Bool? = nil) {
          self.init(unsafeResultMap: ["__typename": "QuestionOption", "_id": id, "content": content, "image": image, "formula": formula, "isCorrect": isCorrect])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var content: String? {
          get {
            return resultMap["content"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "content")
          }
        }

        public var image: String? {
          get {
            return resultMap["image"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "image")
          }
        }

        public var formula: String? {
          get {
            return resultMap["formula"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "formula")
          }
        }

        public var isCorrect: Bool? {
          get {
            return resultMap["isCorrect"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "isCorrect")
          }
        }
      }
    }
  }
}

public final class QuestionTypeQuery: GraphQLQuery {
  public let operationDefinition =
    "query questionType {\n  questionType {\n    __typename\n    _id\n    name\n    isActive\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("questionType", type: .list(.object(QuestionType.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(questionType: [QuestionType?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "questionType": questionType.flatMap { (value: [QuestionType?]) -> [ResultMap?] in value.map { (value: QuestionType?) -> ResultMap? in value.flatMap { (value: QuestionType) -> ResultMap in value.resultMap } } }])
    }

    public var questionType: [QuestionType?]? {
      get {
        return (resultMap["questionType"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [QuestionType?] in value.map { (value: ResultMap?) -> QuestionType? in value.flatMap { (value: ResultMap) -> QuestionType in QuestionType(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [QuestionType?]) -> [ResultMap?] in value.map { (value: QuestionType?) -> ResultMap? in value.flatMap { (value: QuestionType) -> ResultMap in value.resultMap } } }, forKey: "questionType")
      }
    }

    public struct QuestionType: GraphQLSelectionSet {
      public static let possibleTypes = ["QuestionStructure"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("_id", type: .scalar(String.self)),
        GraphQLField("name", type: .scalar(String.self)),
        GraphQLField("isActive", type: .scalar(Bool.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, name: String? = nil, isActive: Bool? = nil) {
        self.init(unsafeResultMap: ["__typename": "QuestionStructure", "_id": id, "name": name, "isActive": isActive])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["_id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "_id")
        }
      }

      public var name: String? {
        get {
          return resultMap["name"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "name")
        }
      }

      public var isActive: Bool? {
        get {
          return resultMap["isActive"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isActive")
        }
      }
    }
  }
}

public final class StudentResultQuery: GraphQLQuery {
  public let operationDefinition =
    "query studentResult($studentId: String!, $classId: String!) {\n  singleStudentResults(studentId: $studentId, classId: $classId) {\n    __typename\n    _id\n    studentId\n    correctAnswers\n    average\n    launchId\n    seminarId\n    seminarName\n    testStartedAt\n  }\n}"

  public var studentId: String
  public var classId: String

  public init(studentId: String, classId: String) {
    self.studentId = studentId
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["studentId": studentId, "classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("singleStudentResults", arguments: ["studentId": GraphQLVariable("studentId"), "classId": GraphQLVariable("classId")], type: .list(.object(SingleStudentResult.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(singleStudentResults: [SingleStudentResult?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "singleStudentResults": singleStudentResults.flatMap { (value: [SingleStudentResult?]) -> [ResultMap?] in value.map { (value: SingleStudentResult?) -> ResultMap? in value.flatMap { (value: SingleStudentResult) -> ResultMap in value.resultMap } } }])
    }

    public var singleStudentResults: [SingleStudentResult?]? {
      get {
        return (resultMap["singleStudentResults"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [SingleStudentResult?] in value.map { (value: ResultMap?) -> SingleStudentResult? in value.flatMap { (value: ResultMap) -> SingleStudentResult in SingleStudentResult(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [SingleStudentResult?]) -> [ResultMap?] in value.map { (value: SingleStudentResult?) -> ResultMap? in value.flatMap { (value: SingleStudentResult) -> ResultMap in value.resultMap } } }, forKey: "singleStudentResults")
      }
    }

    public struct SingleStudentResult: GraphQLSelectionSet {
      public static let possibleTypes = ["StudentResult"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("_id", type: .scalar(String.self)),
        GraphQLField("studentId", type: .scalar(String.self)),
        GraphQLField("correctAnswers", type: .scalar(Int.self)),
        GraphQLField("average", type: .scalar(Double.self)),
        GraphQLField("launchId", type: .scalar(String.self)),
        GraphQLField("seminarId", type: .scalar(String.self)),
        GraphQLField("seminarName", type: .scalar(String.self)),
        GraphQLField("testStartedAt", type: .scalar(Double.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, studentId: String? = nil, correctAnswers: Int? = nil, average: Double? = nil, launchId: String? = nil, seminarId: String? = nil, seminarName: String? = nil, testStartedAt: Double? = nil) {
        self.init(unsafeResultMap: ["__typename": "StudentResult", "_id": id, "studentId": studentId, "correctAnswers": correctAnswers, "average": average, "launchId": launchId, "seminarId": seminarId, "seminarName": seminarName, "testStartedAt": testStartedAt])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["_id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "_id")
        }
      }

      public var studentId: String? {
        get {
          return resultMap["studentId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "studentId")
        }
      }

      public var correctAnswers: Int? {
        get {
          return resultMap["correctAnswers"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "correctAnswers")
        }
      }

      public var average: Double? {
        get {
          return resultMap["average"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average")
        }
      }

      public var launchId: String? {
        get {
          return resultMap["launchId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "launchId")
        }
      }

      public var seminarId: String? {
        get {
          return resultMap["seminarId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "seminarId")
        }
      }

      public var seminarName: String? {
        get {
          return resultMap["seminarName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "seminarName")
        }
      }

      public var testStartedAt: Double? {
        get {
          return resultMap["testStartedAt"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "testStartedAt")
        }
      }
    }
  }
}

public final class SendAnswerMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation sendAnswer($teacherId: String!, $seminarId: String!, $answers: [AnswerInputType]!, $classId: String!) {\n  studentGiveQuestion(teacherId: $teacherId, seminarId: $seminarId, answers: $answers, classId: $classId) {\n    __typename\n    error\n    message\n    result {\n      __typename\n      average\n    }\n  }\n}"

  public var teacherId: String
  public var seminarId: String
  public var answers: [AnswerInputType?]
  public var classId: String

  public init(teacherId: String, seminarId: String, answers: [AnswerInputType?], classId: String) {
    self.teacherId = teacherId
    self.seminarId = seminarId
    self.answers = answers
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["teacherId": teacherId, "seminarId": seminarId, "answers": answers, "classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("studentGiveQuestion", arguments: ["teacherId": GraphQLVariable("teacherId"), "seminarId": GraphQLVariable("seminarId"), "answers": GraphQLVariable("answers"), "classId": GraphQLVariable("classId")], type: .object(StudentGiveQuestion.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(studentGiveQuestion: StudentGiveQuestion? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "studentGiveQuestion": studentGiveQuestion.flatMap { (value: StudentGiveQuestion) -> ResultMap in value.resultMap }])
    }

    public var studentGiveQuestion: StudentGiveQuestion? {
      get {
        return (resultMap["studentGiveQuestion"] as? ResultMap).flatMap { StudentGiveQuestion(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "studentGiveQuestion")
      }
    }

    public struct StudentGiveQuestion: GraphQLSelectionSet {
      public static let possibleTypes = ["StudentGiveQuestion"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("result", type: .object(Result.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, result: Result? = nil) {
        self.init(unsafeResultMap: ["__typename": "StudentGiveQuestion", "error": error, "message": message, "result": result.flatMap { (value: Result) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var result: Result? {
        get {
          return (resultMap["result"] as? ResultMap).flatMap { Result(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "result")
        }
      }

      public struct Result: GraphQLSelectionSet {
        public static let possibleTypes = ["StudentGiveQuestionResponse"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("average", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(average: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "StudentGiveQuestionResponse", "average": average])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var average: Double? {
          get {
            return resultMap["average"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "average")
          }
        }
      }
    }
  }
}

public final class MclassListQuery: GraphQLQuery {
  public let operationDefinition =
    "query mclassList($userId: String!, $corpId: String!) {\n  usersClassList(userId: $userId, corporateId: $corpId) {\n    __typename\n    _id\n    classCode\n    classId\n    classOwnerId\n    classRole\n    userId {\n      __typename\n      _id\n      region\n      image\n      nickname\n      lastname\n      surname\n      birthday\n      email\n      gender\n      phone\n      state\n      online_at\n      created_at\n    }\n    subUser {\n      __typename\n      _id\n      userId\n      schoolId\n      studentIdCode\n      roleId\n      tierId\n      notification {\n        __typename\n        mchatNotificationCount\n        mclassNotificationCount\n        lastDeleted\n      }\n    }\n    roleId\n    className\n    state\n    joinedDate\n    notificationCount\n  }\n}"

  public var userId: String
  public var corpId: String

  public init(userId: String, corpId: String) {
    self.userId = userId
    self.corpId = corpId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "corpId": corpId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("usersClassList", arguments: ["userId": GraphQLVariable("userId"), "corporateId": GraphQLVariable("corpId")], type: .list(.object(UsersClassList.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(usersClassList: [UsersClassList?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "usersClassList": usersClassList.flatMap { (value: [UsersClassList?]) -> [ResultMap?] in value.map { (value: UsersClassList?) -> ResultMap? in value.flatMap { (value: UsersClassList) -> ResultMap in value.resultMap } } }])
    }

    public var usersClassList: [UsersClassList?]? {
      get {
        return (resultMap["usersClassList"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [UsersClassList?] in value.map { (value: ResultMap?) -> UsersClassList? in value.flatMap { (value: ResultMap) -> UsersClassList in UsersClassList(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [UsersClassList?]) -> [ResultMap?] in value.map { (value: UsersClassList?) -> ResultMap? in value.flatMap { (value: UsersClassList) -> ResultMap in value.resultMap } } }, forKey: "usersClassList")
      }
    }

    public struct UsersClassList: GraphQLSelectionSet {
      public static let possibleTypes = ["PopulatedClassUserList"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("_id", type: .scalar(String.self)),
        GraphQLField("classCode", type: .scalar(String.self)),
        GraphQLField("classId", type: .scalar(String.self)),
        GraphQLField("classOwnerId", type: .scalar(String.self)),
        GraphQLField("classRole", type: .scalar(String.self)),
        GraphQLField("userId", type: .object(UserId.selections)),
        GraphQLField("subUser", type: .object(SubUser.selections)),
        GraphQLField("roleId", type: .scalar(String.self)),
        GraphQLField("className", type: .scalar(String.self)),
        GraphQLField("state", type: .scalar(String.self)),
        GraphQLField("joinedDate", type: .scalar(Double.self)),
        GraphQLField("notificationCount", type: .scalar(Int.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, classCode: String? = nil, classId: String? = nil, classOwnerId: String? = nil, classRole: String? = nil, userId: UserId? = nil, subUser: SubUser? = nil, roleId: String? = nil, className: String? = nil, state: String? = nil, joinedDate: Double? = nil, notificationCount: Int? = nil) {
        self.init(unsafeResultMap: ["__typename": "PopulatedClassUserList", "_id": id, "classCode": classCode, "classId": classId, "classOwnerId": classOwnerId, "classRole": classRole, "userId": userId.flatMap { (value: UserId) -> ResultMap in value.resultMap }, "subUser": subUser.flatMap { (value: SubUser) -> ResultMap in value.resultMap }, "roleId": roleId, "className": className, "state": state, "joinedDate": joinedDate, "notificationCount": notificationCount])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["_id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "_id")
        }
      }

      public var classCode: String? {
        get {
          return resultMap["classCode"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "classCode")
        }
      }

      public var classId: String? {
        get {
          return resultMap["classId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "classId")
        }
      }

      public var classOwnerId: String? {
        get {
          return resultMap["classOwnerId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "classOwnerId")
        }
      }

      public var classRole: String? {
        get {
          return resultMap["classRole"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "classRole")
        }
      }

      public var userId: UserId? {
        get {
          return (resultMap["userId"] as? ResultMap).flatMap { UserId(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "userId")
        }
      }

      public var subUser: SubUser? {
        get {
          return (resultMap["subUser"] as? ResultMap).flatMap { SubUser(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "subUser")
        }
      }

      public var roleId: String? {
        get {
          return resultMap["roleId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "roleId")
        }
      }

      public var className: String? {
        get {
          return resultMap["className"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "className")
        }
      }

      public var state: String? {
        get {
          return resultMap["state"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "state")
        }
      }

      public var joinedDate: Double? {
        get {
          return resultMap["joinedDate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "joinedDate")
        }
      }

      public var notificationCount: Int? {
        get {
          return resultMap["notificationCount"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "notificationCount")
        }
      }

      public struct UserId: GraphQLSelectionSet {
        public static let possibleTypes = ["PublicUserInfo"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("region", type: .scalar(String.self)),
          GraphQLField("image", type: .scalar(String.self)),
          GraphQLField("nickname", type: .scalar(String.self)),
          GraphQLField("lastname", type: .scalar(String.self)),
          GraphQLField("surname", type: .scalar(String.self)),
          GraphQLField("birthday", type: .scalar(Double.self)),
          GraphQLField("email", type: .scalar(String.self)),
          GraphQLField("gender", type: .scalar(String.self)),
          GraphQLField("phone", type: .scalar(String.self)),
          GraphQLField("state", type: .scalar(Int.self)),
          GraphQLField("online_at", type: .scalar(Double.self)),
          GraphQLField("created_at", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, region: String? = nil, image: String? = nil, nickname: String? = nil, lastname: String? = nil, surname: String? = nil, birthday: Double? = nil, email: String? = nil, gender: String? = nil, phone: String? = nil, state: Int? = nil, onlineAt: Double? = nil, createdAt: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "PublicUserInfo", "_id": id, "region": region, "image": image, "nickname": nickname, "lastname": lastname, "surname": surname, "birthday": birthday, "email": email, "gender": gender, "phone": phone, "state": state, "online_at": onlineAt, "created_at": createdAt])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var region: String? {
          get {
            return resultMap["region"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "region")
          }
        }

        public var image: String? {
          get {
            return resultMap["image"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "image")
          }
        }

        public var nickname: String? {
          get {
            return resultMap["nickname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "nickname")
          }
        }

        public var lastname: String? {
          get {
            return resultMap["lastname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastname")
          }
        }

        public var surname: String? {
          get {
            return resultMap["surname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "surname")
          }
        }

        public var birthday: Double? {
          get {
            return resultMap["birthday"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "birthday")
          }
        }

        public var email: String? {
          get {
            return resultMap["email"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "email")
          }
        }

        public var gender: String? {
          get {
            return resultMap["gender"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "gender")
          }
        }

        public var phone: String? {
          get {
            return resultMap["phone"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "phone")
          }
        }

        public var state: Int? {
          get {
            return resultMap["state"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "state")
          }
        }

        public var onlineAt: Double? {
          get {
            return resultMap["online_at"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "online_at")
          }
        }

        public var createdAt: Double? {
          get {
            return resultMap["created_at"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "created_at")
          }
        }
      }

      public struct SubUser: GraphQLSelectionSet {
        public static let possibleTypes = ["PublicSubUser"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("schoolId", type: .scalar(String.self)),
          GraphQLField("studentIdCode", type: .scalar(String.self)),
          GraphQLField("roleId", type: .scalar(String.self)),
          GraphQLField("tierId", type: .scalar(String.self)),
          GraphQLField("notification", type: .object(Notification.selections)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, userId: String? = nil, schoolId: String? = nil, studentIdCode: String? = nil, roleId: String? = nil, tierId: String? = nil, notification: Notification? = nil) {
          self.init(unsafeResultMap: ["__typename": "PublicSubUser", "_id": id, "userId": userId, "schoolId": schoolId, "studentIdCode": studentIdCode, "roleId": roleId, "tierId": tierId, "notification": notification.flatMap { (value: Notification) -> ResultMap in value.resultMap }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var schoolId: String? {
          get {
            return resultMap["schoolId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "schoolId")
          }
        }

        public var studentIdCode: String? {
          get {
            return resultMap["studentIdCode"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "studentIdCode")
          }
        }

        public var roleId: String? {
          get {
            return resultMap["roleId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "roleId")
          }
        }

        public var tierId: String? {
          get {
            return resultMap["tierId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "tierId")
          }
        }

        public var notification: Notification? {
          get {
            return (resultMap["notification"] as? ResultMap).flatMap { Notification(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "notification")
          }
        }

        public struct Notification: GraphQLSelectionSet {
          public static let possibleTypes = ["SubUserNotification"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("mchatNotificationCount", type: .scalar(Int.self)),
            GraphQLField("mclassNotificationCount", type: .scalar(Int.self)),
            GraphQLField("lastDeleted", type: .scalar(Double.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(mchatNotificationCount: Int? = nil, mclassNotificationCount: Int? = nil, lastDeleted: Double? = nil) {
            self.init(unsafeResultMap: ["__typename": "SubUserNotification", "mchatNotificationCount": mchatNotificationCount, "mclassNotificationCount": mclassNotificationCount, "lastDeleted": lastDeleted])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var mchatNotificationCount: Int? {
            get {
              return resultMap["mchatNotificationCount"] as? Int
            }
            set {
              resultMap.updateValue(newValue, forKey: "mchatNotificationCount")
            }
          }

          public var mclassNotificationCount: Int? {
            get {
              return resultMap["mclassNotificationCount"] as? Int
            }
            set {
              resultMap.updateValue(newValue, forKey: "mclassNotificationCount")
            }
          }

          public var lastDeleted: Double? {
            get {
              return resultMap["lastDeleted"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "lastDeleted")
            }
          }
        }
      }
    }
  }
}

public final class RequestClassMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation RequestClass($userId: String!, $classCode: String!) {\n  requestClassRoomList(userId: $userId, classCode: $classCode) {\n    __typename\n    error\n    message\n  }\n}"

  public var userId: String
  public var classCode: String

  public init(userId: String, classCode: String) {
    self.userId = userId
    self.classCode = classCode
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "classCode": classCode]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("requestClassRoomList", arguments: ["userId": GraphQLVariable("userId"), "classCode": GraphQLVariable("classCode")], type: .object(RequestClassRoomList.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(requestClassRoomList: RequestClassRoomList? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "requestClassRoomList": requestClassRoomList.flatMap { (value: RequestClassRoomList) -> ResultMap in value.resultMap }])
    }

    public var requestClassRoomList: RequestClassRoomList? {
      get {
        return (resultMap["requestClassRoomList"] as? ResultMap).flatMap { RequestClassRoomList(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "requestClassRoomList")
      }
    }

    public struct RequestClassRoomList: GraphQLSelectionSet {
      public static let possibleTypes = ["BasicResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "BasicResponse", "error": error, "message": message])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }
    }
  }
}

public final class PostsQuery: GraphQLQuery {
  public let operationDefinition =
    "query posts($userId: String!, $classId: String!, $skip: Int, $limit: Int) {\n  posts(userId: $userId, classId: $classId, skip: $skip, limit: $limit) {\n    __typename\n    _id\n    type\n    ownerId {\n      __typename\n      _id\n      region\n      image\n      nickname\n      lastname\n      surname\n      birthday\n      email\n      gender\n      phone\n      state\n      online_at\n      created_at\n    }\n    postId\n    content\n    approved\n    instructions\n    points\n    dueDate\n    link\n    file {\n      __typename\n      extension\n      url\n      name\n      size\n      thumbnail\n    }\n    postedDate\n    commentCount\n    topicId {\n      __typename\n      _id\n      name\n      userId\n      classId\n    }\n  }\n}"

  public var userId: String
  public var classId: String
  public var skip: Int?
  public var limit: Int?

  public init(userId: String, classId: String, skip: Int? = nil, limit: Int? = nil) {
    self.userId = userId
    self.classId = classId
    self.skip = skip
    self.limit = limit
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "classId": classId, "skip": skip, "limit": limit]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("posts", arguments: ["userId": GraphQLVariable("userId"), "classId": GraphQLVariable("classId"), "skip": GraphQLVariable("skip"), "limit": GraphQLVariable("limit")], type: .list(.object(Post.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(posts: [Post?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "posts": posts.flatMap { (value: [Post?]) -> [ResultMap?] in value.map { (value: Post?) -> ResultMap? in value.flatMap { (value: Post) -> ResultMap in value.resultMap } } }])
    }

    public var posts: [Post?]? {
      get {
        return (resultMap["posts"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Post?] in value.map { (value: ResultMap?) -> Post? in value.flatMap { (value: ResultMap) -> Post in Post(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [Post?]) -> [ResultMap?] in value.map { (value: Post?) -> ResultMap? in value.flatMap { (value: Post) -> ResultMap in value.resultMap } } }, forKey: "posts")
      }
    }

    public struct Post: GraphQLSelectionSet {
      public static let possibleTypes = ["UserPopulatedPost"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("_id", type: .scalar(String.self)),
        GraphQLField("type", type: .scalar(String.self)),
        GraphQLField("ownerId", type: .object(OwnerId.selections)),
        GraphQLField("postId", type: .scalar(String.self)),
        GraphQLField("content", type: .scalar(String.self)),
        GraphQLField("approved", type: .scalar(Bool.self)),
        GraphQLField("instructions", type: .scalar(String.self)),
        GraphQLField("points", type: .scalar(Int.self)),
        GraphQLField("dueDate", type: .scalar(Double.self)),
        GraphQLField("link", type: .list(.scalar(String.self))),
        GraphQLField("file", type: .list(.object(File.selections))),
        GraphQLField("postedDate", type: .scalar(Double.self)),
        GraphQLField("commentCount", type: .scalar(Int.self)),
        GraphQLField("topicId", type: .object(TopicId.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, type: String? = nil, ownerId: OwnerId? = nil, postId: String? = nil, content: String? = nil, approved: Bool? = nil, instructions: String? = nil, points: Int? = nil, dueDate: Double? = nil, link: [String?]? = nil, file: [File?]? = nil, postedDate: Double? = nil, commentCount: Int? = nil, topicId: TopicId? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserPopulatedPost", "_id": id, "type": type, "ownerId": ownerId.flatMap { (value: OwnerId) -> ResultMap in value.resultMap }, "postId": postId, "content": content, "approved": approved, "instructions": instructions, "points": points, "dueDate": dueDate, "link": link, "file": file.flatMap { (value: [File?]) -> [ResultMap?] in value.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } } }, "postedDate": postedDate, "commentCount": commentCount, "topicId": topicId.flatMap { (value: TopicId) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["_id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "_id")
        }
      }

      public var type: String? {
        get {
          return resultMap["type"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "type")
        }
      }

      public var ownerId: OwnerId? {
        get {
          return (resultMap["ownerId"] as? ResultMap).flatMap { OwnerId(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "ownerId")
        }
      }

      public var postId: String? {
        get {
          return resultMap["postId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "postId")
        }
      }

      public var content: String? {
        get {
          return resultMap["content"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "content")
        }
      }

      public var approved: Bool? {
        get {
          return resultMap["approved"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "approved")
        }
      }

      public var instructions: String? {
        get {
          return resultMap["instructions"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "instructions")
        }
      }

      public var points: Int? {
        get {
          return resultMap["points"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "points")
        }
      }

      public var dueDate: Double? {
        get {
          return resultMap["dueDate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "dueDate")
        }
      }

      public var link: [String?]? {
        get {
          return resultMap["link"] as? [String?]
        }
        set {
          resultMap.updateValue(newValue, forKey: "link")
        }
      }

      public var file: [File?]? {
        get {
          return (resultMap["file"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [File?] in value.map { (value: ResultMap?) -> File? in value.flatMap { (value: ResultMap) -> File in File(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [File?]) -> [ResultMap?] in value.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } } }, forKey: "file")
        }
      }

      public var postedDate: Double? {
        get {
          return resultMap["postedDate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "postedDate")
        }
      }

      public var commentCount: Int? {
        get {
          return resultMap["commentCount"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "commentCount")
        }
      }

      public var topicId: TopicId? {
        get {
          return (resultMap["topicId"] as? ResultMap).flatMap { TopicId(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "topicId")
        }
      }

      public struct OwnerId: GraphQLSelectionSet {
        public static let possibleTypes = ["PublicUserInfo"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("region", type: .scalar(String.self)),
          GraphQLField("image", type: .scalar(String.self)),
          GraphQLField("nickname", type: .scalar(String.self)),
          GraphQLField("lastname", type: .scalar(String.self)),
          GraphQLField("surname", type: .scalar(String.self)),
          GraphQLField("birthday", type: .scalar(Double.self)),
          GraphQLField("email", type: .scalar(String.self)),
          GraphQLField("gender", type: .scalar(String.self)),
          GraphQLField("phone", type: .scalar(String.self)),
          GraphQLField("state", type: .scalar(Int.self)),
          GraphQLField("online_at", type: .scalar(Double.self)),
          GraphQLField("created_at", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, region: String? = nil, image: String? = nil, nickname: String? = nil, lastname: String? = nil, surname: String? = nil, birthday: Double? = nil, email: String? = nil, gender: String? = nil, phone: String? = nil, state: Int? = nil, onlineAt: Double? = nil, createdAt: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "PublicUserInfo", "_id": id, "region": region, "image": image, "nickname": nickname, "lastname": lastname, "surname": surname, "birthday": birthday, "email": email, "gender": gender, "phone": phone, "state": state, "online_at": onlineAt, "created_at": createdAt])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var region: String? {
          get {
            return resultMap["region"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "region")
          }
        }

        public var image: String? {
          get {
            return resultMap["image"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "image")
          }
        }

        public var nickname: String? {
          get {
            return resultMap["nickname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "nickname")
          }
        }

        public var lastname: String? {
          get {
            return resultMap["lastname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastname")
          }
        }

        public var surname: String? {
          get {
            return resultMap["surname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "surname")
          }
        }

        public var birthday: Double? {
          get {
            return resultMap["birthday"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "birthday")
          }
        }

        public var email: String? {
          get {
            return resultMap["email"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "email")
          }
        }

        public var gender: String? {
          get {
            return resultMap["gender"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "gender")
          }
        }

        public var phone: String? {
          get {
            return resultMap["phone"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "phone")
          }
        }

        public var state: Int? {
          get {
            return resultMap["state"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "state")
          }
        }

        public var onlineAt: Double? {
          get {
            return resultMap["online_at"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "online_at")
          }
        }

        public var createdAt: Double? {
          get {
            return resultMap["created_at"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "created_at")
          }
        }
      }

      public struct File: GraphQLSelectionSet {
        public static let possibleTypes = ["PostFile"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("extension", type: .scalar(String.self)),
          GraphQLField("url", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("size", type: .scalar(Double.self)),
          GraphQLField("thumbnail", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(`extension`: String? = nil, url: String? = nil, name: String? = nil, size: Double? = nil, thumbnail: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "PostFile", "extension": `extension`, "url": url, "name": name, "size": size, "thumbnail": thumbnail])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var `extension`: String? {
          get {
            return resultMap["extension"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "extension")
          }
        }

        public var url: String? {
          get {
            return resultMap["url"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "url")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var size: Double? {
          get {
            return resultMap["size"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "size")
          }
        }

        public var thumbnail: String? {
          get {
            return resultMap["thumbnail"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "thumbnail")
          }
        }
      }

      public struct TopicId: GraphQLSelectionSet {
        public static let possibleTypes = ["Topic"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("classId", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, name: String? = nil, userId: String? = nil, classId: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Topic", "_id": id, "name": name, "userId": userId, "classId": classId])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var classId: String? {
          get {
            return resultMap["classId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classId")
          }
        }
      }
    }
  }
}

public final class SinglepostQuery: GraphQLQuery {
  public let operationDefinition =
    "query singlepost($userId: String!, $classId: String!, $postId: String!) {\n  post(userId: $userId, classId: $classId, postId: $postId) {\n    __typename\n    _id\n    type\n    ownerId {\n      __typename\n      _id\n      image\n      nickname\n      lastname\n    }\n    postId\n    content\n    approved\n    instructions\n    points\n    link\n    file {\n      __typename\n      extension\n      url\n      name\n      size\n      thumbnail\n    }\n    postedDate\n    commentCount\n    topicId {\n      __typename\n      _id\n      name\n      userId\n      classId\n    }\n  }\n}"

  public var userId: String
  public var classId: String
  public var postId: String

  public init(userId: String, classId: String, postId: String) {
    self.userId = userId
    self.classId = classId
    self.postId = postId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "classId": classId, "postId": postId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("post", arguments: ["userId": GraphQLVariable("userId"), "classId": GraphQLVariable("classId"), "postId": GraphQLVariable("postId")], type: .object(Post.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(post: Post? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "post": post.flatMap { (value: Post) -> ResultMap in value.resultMap }])
    }

    public var post: Post? {
      get {
        return (resultMap["post"] as? ResultMap).flatMap { Post(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "post")
      }
    }

    public struct Post: GraphQLSelectionSet {
      public static let possibleTypes = ["UserPopulatedPost"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("_id", type: .scalar(String.self)),
        GraphQLField("type", type: .scalar(String.self)),
        GraphQLField("ownerId", type: .object(OwnerId.selections)),
        GraphQLField("postId", type: .scalar(String.self)),
        GraphQLField("content", type: .scalar(String.self)),
        GraphQLField("approved", type: .scalar(Bool.self)),
        GraphQLField("instructions", type: .scalar(String.self)),
        GraphQLField("points", type: .scalar(Int.self)),
        GraphQLField("link", type: .list(.scalar(String.self))),
        GraphQLField("file", type: .list(.object(File.selections))),
        GraphQLField("postedDate", type: .scalar(Double.self)),
        GraphQLField("commentCount", type: .scalar(Int.self)),
        GraphQLField("topicId", type: .object(TopicId.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, type: String? = nil, ownerId: OwnerId? = nil, postId: String? = nil, content: String? = nil, approved: Bool? = nil, instructions: String? = nil, points: Int? = nil, link: [String?]? = nil, file: [File?]? = nil, postedDate: Double? = nil, commentCount: Int? = nil, topicId: TopicId? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserPopulatedPost", "_id": id, "type": type, "ownerId": ownerId.flatMap { (value: OwnerId) -> ResultMap in value.resultMap }, "postId": postId, "content": content, "approved": approved, "instructions": instructions, "points": points, "link": link, "file": file.flatMap { (value: [File?]) -> [ResultMap?] in value.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } } }, "postedDate": postedDate, "commentCount": commentCount, "topicId": topicId.flatMap { (value: TopicId) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["_id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "_id")
        }
      }

      public var type: String? {
        get {
          return resultMap["type"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "type")
        }
      }

      public var ownerId: OwnerId? {
        get {
          return (resultMap["ownerId"] as? ResultMap).flatMap { OwnerId(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "ownerId")
        }
      }

      public var postId: String? {
        get {
          return resultMap["postId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "postId")
        }
      }

      public var content: String? {
        get {
          return resultMap["content"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "content")
        }
      }

      public var approved: Bool? {
        get {
          return resultMap["approved"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "approved")
        }
      }

      public var instructions: String? {
        get {
          return resultMap["instructions"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "instructions")
        }
      }

      public var points: Int? {
        get {
          return resultMap["points"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "points")
        }
      }

      public var link: [String?]? {
        get {
          return resultMap["link"] as? [String?]
        }
        set {
          resultMap.updateValue(newValue, forKey: "link")
        }
      }

      public var file: [File?]? {
        get {
          return (resultMap["file"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [File?] in value.map { (value: ResultMap?) -> File? in value.flatMap { (value: ResultMap) -> File in File(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [File?]) -> [ResultMap?] in value.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } } }, forKey: "file")
        }
      }

      public var postedDate: Double? {
        get {
          return resultMap["postedDate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "postedDate")
        }
      }

      public var commentCount: Int? {
        get {
          return resultMap["commentCount"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "commentCount")
        }
      }

      public var topicId: TopicId? {
        get {
          return (resultMap["topicId"] as? ResultMap).flatMap { TopicId(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "topicId")
        }
      }

      public struct OwnerId: GraphQLSelectionSet {
        public static let possibleTypes = ["PublicUserInfo"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("image", type: .scalar(String.self)),
          GraphQLField("nickname", type: .scalar(String.self)),
          GraphQLField("lastname", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, image: String? = nil, nickname: String? = nil, lastname: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "PublicUserInfo", "_id": id, "image": image, "nickname": nickname, "lastname": lastname])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var image: String? {
          get {
            return resultMap["image"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "image")
          }
        }

        public var nickname: String? {
          get {
            return resultMap["nickname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "nickname")
          }
        }

        public var lastname: String? {
          get {
            return resultMap["lastname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastname")
          }
        }
      }

      public struct File: GraphQLSelectionSet {
        public static let possibleTypes = ["PostFile"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("extension", type: .scalar(String.self)),
          GraphQLField("url", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("size", type: .scalar(Double.self)),
          GraphQLField("thumbnail", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(`extension`: String? = nil, url: String? = nil, name: String? = nil, size: Double? = nil, thumbnail: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "PostFile", "extension": `extension`, "url": url, "name": name, "size": size, "thumbnail": thumbnail])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var `extension`: String? {
          get {
            return resultMap["extension"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "extension")
          }
        }

        public var url: String? {
          get {
            return resultMap["url"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "url")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var size: Double? {
          get {
            return resultMap["size"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "size")
          }
        }

        public var thumbnail: String? {
          get {
            return resultMap["thumbnail"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "thumbnail")
          }
        }
      }

      public struct TopicId: GraphQLSelectionSet {
        public static let possibleTypes = ["Topic"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("classId", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, name: String? = nil, userId: String? = nil, classId: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Topic", "_id": id, "name": name, "userId": userId, "classId": classId])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var classId: String? {
          get {
            return resultMap["classId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classId")
          }
        }
      }
    }
  }
}

public final class DeletePostMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation deletePost($postId: String!, $classId: String!, $userId: String!) {\n  deletePost(postId: $postId, classId: $classId, userId: $userId) {\n    __typename\n    error\n    message\n  }\n}"

  public var postId: String
  public var classId: String
  public var userId: String

  public init(postId: String, classId: String, userId: String) {
    self.postId = postId
    self.classId = classId
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["postId": postId, "classId": classId, "userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deletePost", arguments: ["postId": GraphQLVariable("postId"), "classId": GraphQLVariable("classId"), "userId": GraphQLVariable("userId")], type: .object(DeletePost.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(deletePost: DeletePost? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "deletePost": deletePost.flatMap { (value: DeletePost) -> ResultMap in value.resultMap }])
    }

    public var deletePost: DeletePost? {
      get {
        return (resultMap["deletePost"] as? ResultMap).flatMap { DeletePost(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "deletePost")
      }
    }

    public struct DeletePost: GraphQLSelectionSet {
      public static let possibleTypes = ["BasicResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "BasicResponse", "error": error, "message": message])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }
    }
  }
}

public final class InsertPostMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation insertPost($InputPost: InputPost!, $classId: String!) {\n  insertPost(InputPost: $InputPost, classId: $classId) {\n    __typename\n    error\n    message\n    result {\n      __typename\n      _id\n      type\n      ownerId {\n        __typename\n        _id\n        region\n        image\n        nickname\n        lastname\n        surname\n        birthday\n        email\n        gender\n        phone\n        state\n        online_at\n        created_at\n      }\n      postId\n      content\n      approved\n      instructions\n      points\n      dueDate\n      link\n      file {\n        __typename\n        extension\n        url\n      }\n      postedDate\n      commentCount\n      topicId {\n        __typename\n        _id\n        name\n        userId\n        classId\n      }\n    }\n  }\n}"

  public var InputPost: InputPost
  public var classId: String

  public init(InputPost: InputPost, classId: String) {
    self.InputPost = InputPost
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["InputPost": InputPost, "classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("insertPost", arguments: ["InputPost": GraphQLVariable("InputPost"), "classId": GraphQLVariable("classId")], type: .object(InsertPost.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(insertPost: InsertPost? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "insertPost": insertPost.flatMap { (value: InsertPost) -> ResultMap in value.resultMap }])
    }

    public var insertPost: InsertPost? {
      get {
        return (resultMap["insertPost"] as? ResultMap).flatMap { InsertPost(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "insertPost")
      }
    }

    public struct InsertPost: GraphQLSelectionSet {
      public static let possibleTypes = ["PostResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("result", type: .object(Result.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, result: Result? = nil) {
        self.init(unsafeResultMap: ["__typename": "PostResponse", "error": error, "message": message, "result": result.flatMap { (value: Result) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var result: Result? {
        get {
          return (resultMap["result"] as? ResultMap).flatMap { Result(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "result")
        }
      }

      public struct Result: GraphQLSelectionSet {
        public static let possibleTypes = ["UserPopulatedPost"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("type", type: .scalar(String.self)),
          GraphQLField("ownerId", type: .object(OwnerId.selections)),
          GraphQLField("postId", type: .scalar(String.self)),
          GraphQLField("content", type: .scalar(String.self)),
          GraphQLField("approved", type: .scalar(Bool.self)),
          GraphQLField("instructions", type: .scalar(String.self)),
          GraphQLField("points", type: .scalar(Int.self)),
          GraphQLField("dueDate", type: .scalar(Double.self)),
          GraphQLField("link", type: .list(.scalar(String.self))),
          GraphQLField("file", type: .list(.object(File.selections))),
          GraphQLField("postedDate", type: .scalar(Double.self)),
          GraphQLField("commentCount", type: .scalar(Int.self)),
          GraphQLField("topicId", type: .object(TopicId.selections)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, type: String? = nil, ownerId: OwnerId? = nil, postId: String? = nil, content: String? = nil, approved: Bool? = nil, instructions: String? = nil, points: Int? = nil, dueDate: Double? = nil, link: [String?]? = nil, file: [File?]? = nil, postedDate: Double? = nil, commentCount: Int? = nil, topicId: TopicId? = nil) {
          self.init(unsafeResultMap: ["__typename": "UserPopulatedPost", "_id": id, "type": type, "ownerId": ownerId.flatMap { (value: OwnerId) -> ResultMap in value.resultMap }, "postId": postId, "content": content, "approved": approved, "instructions": instructions, "points": points, "dueDate": dueDate, "link": link, "file": file.flatMap { (value: [File?]) -> [ResultMap?] in value.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } } }, "postedDate": postedDate, "commentCount": commentCount, "topicId": topicId.flatMap { (value: TopicId) -> ResultMap in value.resultMap }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var type: String? {
          get {
            return resultMap["type"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "type")
          }
        }

        public var ownerId: OwnerId? {
          get {
            return (resultMap["ownerId"] as? ResultMap).flatMap { OwnerId(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "ownerId")
          }
        }

        public var postId: String? {
          get {
            return resultMap["postId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "postId")
          }
        }

        public var content: String? {
          get {
            return resultMap["content"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "content")
          }
        }

        public var approved: Bool? {
          get {
            return resultMap["approved"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "approved")
          }
        }

        public var instructions: String? {
          get {
            return resultMap["instructions"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "instructions")
          }
        }

        public var points: Int? {
          get {
            return resultMap["points"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "points")
          }
        }

        public var dueDate: Double? {
          get {
            return resultMap["dueDate"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "dueDate")
          }
        }

        public var link: [String?]? {
          get {
            return resultMap["link"] as? [String?]
          }
          set {
            resultMap.updateValue(newValue, forKey: "link")
          }
        }

        public var file: [File?]? {
          get {
            return (resultMap["file"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [File?] in value.map { (value: ResultMap?) -> File? in value.flatMap { (value: ResultMap) -> File in File(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [File?]) -> [ResultMap?] in value.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } } }, forKey: "file")
          }
        }

        public var postedDate: Double? {
          get {
            return resultMap["postedDate"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "postedDate")
          }
        }

        public var commentCount: Int? {
          get {
            return resultMap["commentCount"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "commentCount")
          }
        }

        public var topicId: TopicId? {
          get {
            return (resultMap["topicId"] as? ResultMap).flatMap { TopicId(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "topicId")
          }
        }

        public struct OwnerId: GraphQLSelectionSet {
          public static let possibleTypes = ["PublicUserInfo"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("_id", type: .scalar(String.self)),
            GraphQLField("region", type: .scalar(String.self)),
            GraphQLField("image", type: .scalar(String.self)),
            GraphQLField("nickname", type: .scalar(String.self)),
            GraphQLField("lastname", type: .scalar(String.self)),
            GraphQLField("surname", type: .scalar(String.self)),
            GraphQLField("birthday", type: .scalar(Double.self)),
            GraphQLField("email", type: .scalar(String.self)),
            GraphQLField("gender", type: .scalar(String.self)),
            GraphQLField("phone", type: .scalar(String.self)),
            GraphQLField("state", type: .scalar(Int.self)),
            GraphQLField("online_at", type: .scalar(Double.self)),
            GraphQLField("created_at", type: .scalar(Double.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, region: String? = nil, image: String? = nil, nickname: String? = nil, lastname: String? = nil, surname: String? = nil, birthday: Double? = nil, email: String? = nil, gender: String? = nil, phone: String? = nil, state: Int? = nil, onlineAt: Double? = nil, createdAt: Double? = nil) {
            self.init(unsafeResultMap: ["__typename": "PublicUserInfo", "_id": id, "region": region, "image": image, "nickname": nickname, "lastname": lastname, "surname": surname, "birthday": birthday, "email": email, "gender": gender, "phone": phone, "state": state, "online_at": onlineAt, "created_at": createdAt])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var region: String? {
            get {
              return resultMap["region"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "region")
            }
          }

          public var image: String? {
            get {
              return resultMap["image"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "image")
            }
          }

          public var nickname: String? {
            get {
              return resultMap["nickname"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "nickname")
            }
          }

          public var lastname: String? {
            get {
              return resultMap["lastname"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "lastname")
            }
          }

          public var surname: String? {
            get {
              return resultMap["surname"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "surname")
            }
          }

          public var birthday: Double? {
            get {
              return resultMap["birthday"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "birthday")
            }
          }

          public var email: String? {
            get {
              return resultMap["email"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "email")
            }
          }

          public var gender: String? {
            get {
              return resultMap["gender"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "gender")
            }
          }

          public var phone: String? {
            get {
              return resultMap["phone"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "phone")
            }
          }

          public var state: Int? {
            get {
              return resultMap["state"] as? Int
            }
            set {
              resultMap.updateValue(newValue, forKey: "state")
            }
          }

          public var onlineAt: Double? {
            get {
              return resultMap["online_at"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "online_at")
            }
          }

          public var createdAt: Double? {
            get {
              return resultMap["created_at"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "created_at")
            }
          }
        }

        public struct File: GraphQLSelectionSet {
          public static let possibleTypes = ["PostFile"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("extension", type: .scalar(String.self)),
            GraphQLField("url", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(`extension`: String? = nil, url: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "PostFile", "extension": `extension`, "url": url])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var `extension`: String? {
            get {
              return resultMap["extension"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "extension")
            }
          }

          public var url: String? {
            get {
              return resultMap["url"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "url")
            }
          }
        }

        public struct TopicId: GraphQLSelectionSet {
          public static let possibleTypes = ["Topic"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("_id", type: .scalar(String.self)),
            GraphQLField("name", type: .scalar(String.self)),
            GraphQLField("userId", type: .scalar(String.self)),
            GraphQLField("classId", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil, userId: String? = nil, classId: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Topic", "_id": id, "name": name, "userId": userId, "classId": classId])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var userId: String? {
            get {
              return resultMap["userId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "userId")
            }
          }

          public var classId: String? {
            get {
              return resultMap["classId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "classId")
            }
          }
        }
      }
    }
  }
}

public final class UpdatePostMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation updatePost($postId: String!, $classId: String!, $userId: String!, $updatePost: UpdatePost!, $topic: String!) {\n  updatePost(postId: $postId, classId: $classId, userId: $userId, UpdatePost: $updatePost, topic: $topic) {\n    __typename\n    error\n    message\n    result {\n      __typename\n      _id\n      type\n      ownerId {\n        __typename\n        _id\n        region\n        image\n        nickname\n        lastname\n        surname\n        birthday\n        email\n        gender\n        phone\n        state\n        online_at\n        created_at\n      }\n      postId\n      content\n      approved\n      instructions\n      points\n      dueDate\n      link\n      file {\n        __typename\n        url\n        extension\n      }\n      postedDate\n      commentCount\n      topicId {\n        __typename\n        _id\n        name\n        userId\n        classId\n      }\n    }\n  }\n}"

  public var postId: String
  public var classId: String
  public var userId: String
  public var updatePost: UpdatePost
  public var topic: String

  public init(postId: String, classId: String, userId: String, updatePost: UpdatePost, topic: String) {
    self.postId = postId
    self.classId = classId
    self.userId = userId
    self.updatePost = updatePost
    self.topic = topic
  }

  public var variables: GraphQLMap? {
    return ["postId": postId, "classId": classId, "userId": userId, "updatePost": updatePost, "topic": topic]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updatePost", arguments: ["postId": GraphQLVariable("postId"), "classId": GraphQLVariable("classId"), "userId": GraphQLVariable("userId"), "UpdatePost": GraphQLVariable("updatePost"), "topic": GraphQLVariable("topic")], type: .object(UpdatePost.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updatePost: UpdatePost? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "updatePost": updatePost.flatMap { (value: UpdatePost) -> ResultMap in value.resultMap }])
    }

    public var updatePost: UpdatePost? {
      get {
        return (resultMap["updatePost"] as? ResultMap).flatMap { UpdatePost(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "updatePost")
      }
    }

    public struct UpdatePost: GraphQLSelectionSet {
      public static let possibleTypes = ["PostResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("result", type: .object(Result.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, result: Result? = nil) {
        self.init(unsafeResultMap: ["__typename": "PostResponse", "error": error, "message": message, "result": result.flatMap { (value: Result) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var result: Result? {
        get {
          return (resultMap["result"] as? ResultMap).flatMap { Result(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "result")
        }
      }

      public struct Result: GraphQLSelectionSet {
        public static let possibleTypes = ["UserPopulatedPost"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("type", type: .scalar(String.self)),
          GraphQLField("ownerId", type: .object(OwnerId.selections)),
          GraphQLField("postId", type: .scalar(String.self)),
          GraphQLField("content", type: .scalar(String.self)),
          GraphQLField("approved", type: .scalar(Bool.self)),
          GraphQLField("instructions", type: .scalar(String.self)),
          GraphQLField("points", type: .scalar(Int.self)),
          GraphQLField("dueDate", type: .scalar(Double.self)),
          GraphQLField("link", type: .list(.scalar(String.self))),
          GraphQLField("file", type: .list(.object(File.selections))),
          GraphQLField("postedDate", type: .scalar(Double.self)),
          GraphQLField("commentCount", type: .scalar(Int.self)),
          GraphQLField("topicId", type: .object(TopicId.selections)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, type: String? = nil, ownerId: OwnerId? = nil, postId: String? = nil, content: String? = nil, approved: Bool? = nil, instructions: String? = nil, points: Int? = nil, dueDate: Double? = nil, link: [String?]? = nil, file: [File?]? = nil, postedDate: Double? = nil, commentCount: Int? = nil, topicId: TopicId? = nil) {
          self.init(unsafeResultMap: ["__typename": "UserPopulatedPost", "_id": id, "type": type, "ownerId": ownerId.flatMap { (value: OwnerId) -> ResultMap in value.resultMap }, "postId": postId, "content": content, "approved": approved, "instructions": instructions, "points": points, "dueDate": dueDate, "link": link, "file": file.flatMap { (value: [File?]) -> [ResultMap?] in value.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } } }, "postedDate": postedDate, "commentCount": commentCount, "topicId": topicId.flatMap { (value: TopicId) -> ResultMap in value.resultMap }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var type: String? {
          get {
            return resultMap["type"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "type")
          }
        }

        public var ownerId: OwnerId? {
          get {
            return (resultMap["ownerId"] as? ResultMap).flatMap { OwnerId(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "ownerId")
          }
        }

        public var postId: String? {
          get {
            return resultMap["postId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "postId")
          }
        }

        public var content: String? {
          get {
            return resultMap["content"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "content")
          }
        }

        public var approved: Bool? {
          get {
            return resultMap["approved"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "approved")
          }
        }

        public var instructions: String? {
          get {
            return resultMap["instructions"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "instructions")
          }
        }

        public var points: Int? {
          get {
            return resultMap["points"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "points")
          }
        }

        public var dueDate: Double? {
          get {
            return resultMap["dueDate"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "dueDate")
          }
        }

        public var link: [String?]? {
          get {
            return resultMap["link"] as? [String?]
          }
          set {
            resultMap.updateValue(newValue, forKey: "link")
          }
        }

        public var file: [File?]? {
          get {
            return (resultMap["file"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [File?] in value.map { (value: ResultMap?) -> File? in value.flatMap { (value: ResultMap) -> File in File(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [File?]) -> [ResultMap?] in value.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } } }, forKey: "file")
          }
        }

        public var postedDate: Double? {
          get {
            return resultMap["postedDate"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "postedDate")
          }
        }

        public var commentCount: Int? {
          get {
            return resultMap["commentCount"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "commentCount")
          }
        }

        public var topicId: TopicId? {
          get {
            return (resultMap["topicId"] as? ResultMap).flatMap { TopicId(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "topicId")
          }
        }

        public struct OwnerId: GraphQLSelectionSet {
          public static let possibleTypes = ["PublicUserInfo"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("_id", type: .scalar(String.self)),
            GraphQLField("region", type: .scalar(String.self)),
            GraphQLField("image", type: .scalar(String.self)),
            GraphQLField("nickname", type: .scalar(String.self)),
            GraphQLField("lastname", type: .scalar(String.self)),
            GraphQLField("surname", type: .scalar(String.self)),
            GraphQLField("birthday", type: .scalar(Double.self)),
            GraphQLField("email", type: .scalar(String.self)),
            GraphQLField("gender", type: .scalar(String.self)),
            GraphQLField("phone", type: .scalar(String.self)),
            GraphQLField("state", type: .scalar(Int.self)),
            GraphQLField("online_at", type: .scalar(Double.self)),
            GraphQLField("created_at", type: .scalar(Double.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, region: String? = nil, image: String? = nil, nickname: String? = nil, lastname: String? = nil, surname: String? = nil, birthday: Double? = nil, email: String? = nil, gender: String? = nil, phone: String? = nil, state: Int? = nil, onlineAt: Double? = nil, createdAt: Double? = nil) {
            self.init(unsafeResultMap: ["__typename": "PublicUserInfo", "_id": id, "region": region, "image": image, "nickname": nickname, "lastname": lastname, "surname": surname, "birthday": birthday, "email": email, "gender": gender, "phone": phone, "state": state, "online_at": onlineAt, "created_at": createdAt])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var region: String? {
            get {
              return resultMap["region"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "region")
            }
          }

          public var image: String? {
            get {
              return resultMap["image"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "image")
            }
          }

          public var nickname: String? {
            get {
              return resultMap["nickname"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "nickname")
            }
          }

          public var lastname: String? {
            get {
              return resultMap["lastname"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "lastname")
            }
          }

          public var surname: String? {
            get {
              return resultMap["surname"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "surname")
            }
          }

          public var birthday: Double? {
            get {
              return resultMap["birthday"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "birthday")
            }
          }

          public var email: String? {
            get {
              return resultMap["email"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "email")
            }
          }

          public var gender: String? {
            get {
              return resultMap["gender"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "gender")
            }
          }

          public var phone: String? {
            get {
              return resultMap["phone"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "phone")
            }
          }

          public var state: Int? {
            get {
              return resultMap["state"] as? Int
            }
            set {
              resultMap.updateValue(newValue, forKey: "state")
            }
          }

          public var onlineAt: Double? {
            get {
              return resultMap["online_at"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "online_at")
            }
          }

          public var createdAt: Double? {
            get {
              return resultMap["created_at"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "created_at")
            }
          }
        }

        public struct File: GraphQLSelectionSet {
          public static let possibleTypes = ["PostFile"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("url", type: .scalar(String.self)),
            GraphQLField("extension", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(url: String? = nil, `extension`: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "PostFile", "url": url, "extension": `extension`])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var url: String? {
            get {
              return resultMap["url"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "url")
            }
          }

          public var `extension`: String? {
            get {
              return resultMap["extension"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "extension")
            }
          }
        }

        public struct TopicId: GraphQLSelectionSet {
          public static let possibleTypes = ["Topic"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("_id", type: .scalar(String.self)),
            GraphQLField("name", type: .scalar(String.self)),
            GraphQLField("userId", type: .scalar(String.self)),
            GraphQLField("classId", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil, userId: String? = nil, classId: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Topic", "_id": id, "name": name, "userId": userId, "classId": classId])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var userId: String? {
            get {
              return resultMap["userId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "userId")
            }
          }

          public var classId: String? {
            get {
              return resultMap["classId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "classId")
            }
          }
        }
      }
    }
  }
}

public final class PostCommentsQuery: GraphQLQuery {
  public let operationDefinition =
    "query postComments($postId: String!, $classId: String!, $userId: String!) {\n  postComments(postId: $postId, classId: $classId, userId: $userId) {\n    __typename\n    _id\n    type\n    ownerId {\n      __typename\n      _id\n      region\n      image\n      nickname\n      lastname\n      surname\n      birthday\n      email\n      gender\n      phone\n      state\n      online_at\n      created_at\n    }\n    postId\n    content\n    approved\n    instructions\n    points\n    dueDate\n    link\n    file {\n      __typename\n      extension\n      url\n      name\n      size\n      thumbnail\n    }\n    postedDate\n    commentCount\n    topicId {\n      __typename\n      _id\n      name\n      userId\n      classId\n    }\n    topic\n  }\n}"

  public var postId: String
  public var classId: String
  public var userId: String

  public init(postId: String, classId: String, userId: String) {
    self.postId = postId
    self.classId = classId
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["postId": postId, "classId": classId, "userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("postComments", arguments: ["postId": GraphQLVariable("postId"), "classId": GraphQLVariable("classId"), "userId": GraphQLVariable("userId")], type: .list(.object(PostComment.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(postComments: [PostComment?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "postComments": postComments.flatMap { (value: [PostComment?]) -> [ResultMap?] in value.map { (value: PostComment?) -> ResultMap? in value.flatMap { (value: PostComment) -> ResultMap in value.resultMap } } }])
    }

    public var postComments: [PostComment?]? {
      get {
        return (resultMap["postComments"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [PostComment?] in value.map { (value: ResultMap?) -> PostComment? in value.flatMap { (value: ResultMap) -> PostComment in PostComment(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [PostComment?]) -> [ResultMap?] in value.map { (value: PostComment?) -> ResultMap? in value.flatMap { (value: PostComment) -> ResultMap in value.resultMap } } }, forKey: "postComments")
      }
    }

    public struct PostComment: GraphQLSelectionSet {
      public static let possibleTypes = ["UserPopulatedPost"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("_id", type: .scalar(String.self)),
        GraphQLField("type", type: .scalar(String.self)),
        GraphQLField("ownerId", type: .object(OwnerId.selections)),
        GraphQLField("postId", type: .scalar(String.self)),
        GraphQLField("content", type: .scalar(String.self)),
        GraphQLField("approved", type: .scalar(Bool.self)),
        GraphQLField("instructions", type: .scalar(String.self)),
        GraphQLField("points", type: .scalar(Int.self)),
        GraphQLField("dueDate", type: .scalar(Double.self)),
        GraphQLField("link", type: .list(.scalar(String.self))),
        GraphQLField("file", type: .list(.object(File.selections))),
        GraphQLField("postedDate", type: .scalar(Double.self)),
        GraphQLField("commentCount", type: .scalar(Int.self)),
        GraphQLField("topicId", type: .object(TopicId.selections)),
        GraphQLField("topic", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, type: String? = nil, ownerId: OwnerId? = nil, postId: String? = nil, content: String? = nil, approved: Bool? = nil, instructions: String? = nil, points: Int? = nil, dueDate: Double? = nil, link: [String?]? = nil, file: [File?]? = nil, postedDate: Double? = nil, commentCount: Int? = nil, topicId: TopicId? = nil, topic: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserPopulatedPost", "_id": id, "type": type, "ownerId": ownerId.flatMap { (value: OwnerId) -> ResultMap in value.resultMap }, "postId": postId, "content": content, "approved": approved, "instructions": instructions, "points": points, "dueDate": dueDate, "link": link, "file": file.flatMap { (value: [File?]) -> [ResultMap?] in value.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } } }, "postedDate": postedDate, "commentCount": commentCount, "topicId": topicId.flatMap { (value: TopicId) -> ResultMap in value.resultMap }, "topic": topic])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["_id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "_id")
        }
      }

      public var type: String? {
        get {
          return resultMap["type"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "type")
        }
      }

      public var ownerId: OwnerId? {
        get {
          return (resultMap["ownerId"] as? ResultMap).flatMap { OwnerId(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "ownerId")
        }
      }

      public var postId: String? {
        get {
          return resultMap["postId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "postId")
        }
      }

      public var content: String? {
        get {
          return resultMap["content"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "content")
        }
      }

      public var approved: Bool? {
        get {
          return resultMap["approved"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "approved")
        }
      }

      public var instructions: String? {
        get {
          return resultMap["instructions"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "instructions")
        }
      }

      public var points: Int? {
        get {
          return resultMap["points"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "points")
        }
      }

      public var dueDate: Double? {
        get {
          return resultMap["dueDate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "dueDate")
        }
      }

      public var link: [String?]? {
        get {
          return resultMap["link"] as? [String?]
        }
        set {
          resultMap.updateValue(newValue, forKey: "link")
        }
      }

      public var file: [File?]? {
        get {
          return (resultMap["file"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [File?] in value.map { (value: ResultMap?) -> File? in value.flatMap { (value: ResultMap) -> File in File(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [File?]) -> [ResultMap?] in value.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } } }, forKey: "file")
        }
      }

      public var postedDate: Double? {
        get {
          return resultMap["postedDate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "postedDate")
        }
      }

      public var commentCount: Int? {
        get {
          return resultMap["commentCount"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "commentCount")
        }
      }

      public var topicId: TopicId? {
        get {
          return (resultMap["topicId"] as? ResultMap).flatMap { TopicId(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "topicId")
        }
      }

      public var topic: String? {
        get {
          return resultMap["topic"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "topic")
        }
      }

      public struct OwnerId: GraphQLSelectionSet {
        public static let possibleTypes = ["PublicUserInfo"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("region", type: .scalar(String.self)),
          GraphQLField("image", type: .scalar(String.self)),
          GraphQLField("nickname", type: .scalar(String.self)),
          GraphQLField("lastname", type: .scalar(String.self)),
          GraphQLField("surname", type: .scalar(String.self)),
          GraphQLField("birthday", type: .scalar(Double.self)),
          GraphQLField("email", type: .scalar(String.self)),
          GraphQLField("gender", type: .scalar(String.self)),
          GraphQLField("phone", type: .scalar(String.self)),
          GraphQLField("state", type: .scalar(Int.self)),
          GraphQLField("online_at", type: .scalar(Double.self)),
          GraphQLField("created_at", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, region: String? = nil, image: String? = nil, nickname: String? = nil, lastname: String? = nil, surname: String? = nil, birthday: Double? = nil, email: String? = nil, gender: String? = nil, phone: String? = nil, state: Int? = nil, onlineAt: Double? = nil, createdAt: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "PublicUserInfo", "_id": id, "region": region, "image": image, "nickname": nickname, "lastname": lastname, "surname": surname, "birthday": birthday, "email": email, "gender": gender, "phone": phone, "state": state, "online_at": onlineAt, "created_at": createdAt])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var region: String? {
          get {
            return resultMap["region"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "region")
          }
        }

        public var image: String? {
          get {
            return resultMap["image"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "image")
          }
        }

        public var nickname: String? {
          get {
            return resultMap["nickname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "nickname")
          }
        }

        public var lastname: String? {
          get {
            return resultMap["lastname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastname")
          }
        }

        public var surname: String? {
          get {
            return resultMap["surname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "surname")
          }
        }

        public var birthday: Double? {
          get {
            return resultMap["birthday"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "birthday")
          }
        }

        public var email: String? {
          get {
            return resultMap["email"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "email")
          }
        }

        public var gender: String? {
          get {
            return resultMap["gender"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "gender")
          }
        }

        public var phone: String? {
          get {
            return resultMap["phone"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "phone")
          }
        }

        public var state: Int? {
          get {
            return resultMap["state"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "state")
          }
        }

        public var onlineAt: Double? {
          get {
            return resultMap["online_at"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "online_at")
          }
        }

        public var createdAt: Double? {
          get {
            return resultMap["created_at"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "created_at")
          }
        }
      }

      public struct File: GraphQLSelectionSet {
        public static let possibleTypes = ["PostFile"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("extension", type: .scalar(String.self)),
          GraphQLField("url", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("size", type: .scalar(Double.self)),
          GraphQLField("thumbnail", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(`extension`: String? = nil, url: String? = nil, name: String? = nil, size: Double? = nil, thumbnail: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "PostFile", "extension": `extension`, "url": url, "name": name, "size": size, "thumbnail": thumbnail])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var `extension`: String? {
          get {
            return resultMap["extension"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "extension")
          }
        }

        public var url: String? {
          get {
            return resultMap["url"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "url")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var size: Double? {
          get {
            return resultMap["size"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "size")
          }
        }

        public var thumbnail: String? {
          get {
            return resultMap["thumbnail"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "thumbnail")
          }
        }
      }

      public struct TopicId: GraphQLSelectionSet {
        public static let possibleTypes = ["Topic"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("classId", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, name: String? = nil, userId: String? = nil, classId: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Topic", "_id": id, "name": name, "userId": userId, "classId": classId])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var classId: String? {
          get {
            return resultMap["classId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classId")
          }
        }
      }
    }
  }
}

public final class InsertCommentPostMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation insertCommentPost($postId: String!, $classId: String!, $inputPostComment: InputPostComment!) {\n  insertCommentPost(postId: $postId, classId: $classId, InputPostComment: $inputPostComment) {\n    __typename\n    error\n    message\n    result {\n      __typename\n      _id\n      type\n      ownerId {\n        __typename\n        _id\n        region\n        image\n        nickname\n        lastname\n        surname\n        birthday\n        email\n        gender\n        phone\n        state\n        online_at\n        created_at\n      }\n      postId\n      content\n      approved\n      instructions\n      points\n      dueDate\n      link\n      file {\n        __typename\n        extension\n        url\n      }\n      postedDate\n      commentCount\n      topicId {\n        __typename\n        _id\n        name\n        userId\n        classId\n      }\n    }\n  }\n}"

  public var postId: String
  public var classId: String
  public var inputPostComment: InputPostComment

  public init(postId: String, classId: String, inputPostComment: InputPostComment) {
    self.postId = postId
    self.classId = classId
    self.inputPostComment = inputPostComment
  }

  public var variables: GraphQLMap? {
    return ["postId": postId, "classId": classId, "inputPostComment": inputPostComment]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("insertCommentPost", arguments: ["postId": GraphQLVariable("postId"), "classId": GraphQLVariable("classId"), "InputPostComment": GraphQLVariable("inputPostComment")], type: .object(InsertCommentPost.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(insertCommentPost: InsertCommentPost? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "insertCommentPost": insertCommentPost.flatMap { (value: InsertCommentPost) -> ResultMap in value.resultMap }])
    }

    public var insertCommentPost: InsertCommentPost? {
      get {
        return (resultMap["insertCommentPost"] as? ResultMap).flatMap { InsertCommentPost(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "insertCommentPost")
      }
    }

    public struct InsertCommentPost: GraphQLSelectionSet {
      public static let possibleTypes = ["PostResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("result", type: .object(Result.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, result: Result? = nil) {
        self.init(unsafeResultMap: ["__typename": "PostResponse", "error": error, "message": message, "result": result.flatMap { (value: Result) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var result: Result? {
        get {
          return (resultMap["result"] as? ResultMap).flatMap { Result(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "result")
        }
      }

      public struct Result: GraphQLSelectionSet {
        public static let possibleTypes = ["UserPopulatedPost"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("type", type: .scalar(String.self)),
          GraphQLField("ownerId", type: .object(OwnerId.selections)),
          GraphQLField("postId", type: .scalar(String.self)),
          GraphQLField("content", type: .scalar(String.self)),
          GraphQLField("approved", type: .scalar(Bool.self)),
          GraphQLField("instructions", type: .scalar(String.self)),
          GraphQLField("points", type: .scalar(Int.self)),
          GraphQLField("dueDate", type: .scalar(Double.self)),
          GraphQLField("link", type: .list(.scalar(String.self))),
          GraphQLField("file", type: .list(.object(File.selections))),
          GraphQLField("postedDate", type: .scalar(Double.self)),
          GraphQLField("commentCount", type: .scalar(Int.self)),
          GraphQLField("topicId", type: .object(TopicId.selections)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, type: String? = nil, ownerId: OwnerId? = nil, postId: String? = nil, content: String? = nil, approved: Bool? = nil, instructions: String? = nil, points: Int? = nil, dueDate: Double? = nil, link: [String?]? = nil, file: [File?]? = nil, postedDate: Double? = nil, commentCount: Int? = nil, topicId: TopicId? = nil) {
          self.init(unsafeResultMap: ["__typename": "UserPopulatedPost", "_id": id, "type": type, "ownerId": ownerId.flatMap { (value: OwnerId) -> ResultMap in value.resultMap }, "postId": postId, "content": content, "approved": approved, "instructions": instructions, "points": points, "dueDate": dueDate, "link": link, "file": file.flatMap { (value: [File?]) -> [ResultMap?] in value.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } } }, "postedDate": postedDate, "commentCount": commentCount, "topicId": topicId.flatMap { (value: TopicId) -> ResultMap in value.resultMap }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var type: String? {
          get {
            return resultMap["type"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "type")
          }
        }

        public var ownerId: OwnerId? {
          get {
            return (resultMap["ownerId"] as? ResultMap).flatMap { OwnerId(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "ownerId")
          }
        }

        public var postId: String? {
          get {
            return resultMap["postId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "postId")
          }
        }

        public var content: String? {
          get {
            return resultMap["content"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "content")
          }
        }

        public var approved: Bool? {
          get {
            return resultMap["approved"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "approved")
          }
        }

        public var instructions: String? {
          get {
            return resultMap["instructions"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "instructions")
          }
        }

        public var points: Int? {
          get {
            return resultMap["points"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "points")
          }
        }

        public var dueDate: Double? {
          get {
            return resultMap["dueDate"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "dueDate")
          }
        }

        public var link: [String?]? {
          get {
            return resultMap["link"] as? [String?]
          }
          set {
            resultMap.updateValue(newValue, forKey: "link")
          }
        }

        public var file: [File?]? {
          get {
            return (resultMap["file"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [File?] in value.map { (value: ResultMap?) -> File? in value.flatMap { (value: ResultMap) -> File in File(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [File?]) -> [ResultMap?] in value.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } } }, forKey: "file")
          }
        }

        public var postedDate: Double? {
          get {
            return resultMap["postedDate"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "postedDate")
          }
        }

        public var commentCount: Int? {
          get {
            return resultMap["commentCount"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "commentCount")
          }
        }

        public var topicId: TopicId? {
          get {
            return (resultMap["topicId"] as? ResultMap).flatMap { TopicId(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "topicId")
          }
        }

        public struct OwnerId: GraphQLSelectionSet {
          public static let possibleTypes = ["PublicUserInfo"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("_id", type: .scalar(String.self)),
            GraphQLField("region", type: .scalar(String.self)),
            GraphQLField("image", type: .scalar(String.self)),
            GraphQLField("nickname", type: .scalar(String.self)),
            GraphQLField("lastname", type: .scalar(String.self)),
            GraphQLField("surname", type: .scalar(String.self)),
            GraphQLField("birthday", type: .scalar(Double.self)),
            GraphQLField("email", type: .scalar(String.self)),
            GraphQLField("gender", type: .scalar(String.self)),
            GraphQLField("phone", type: .scalar(String.self)),
            GraphQLField("state", type: .scalar(Int.self)),
            GraphQLField("online_at", type: .scalar(Double.self)),
            GraphQLField("created_at", type: .scalar(Double.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, region: String? = nil, image: String? = nil, nickname: String? = nil, lastname: String? = nil, surname: String? = nil, birthday: Double? = nil, email: String? = nil, gender: String? = nil, phone: String? = nil, state: Int? = nil, onlineAt: Double? = nil, createdAt: Double? = nil) {
            self.init(unsafeResultMap: ["__typename": "PublicUserInfo", "_id": id, "region": region, "image": image, "nickname": nickname, "lastname": lastname, "surname": surname, "birthday": birthday, "email": email, "gender": gender, "phone": phone, "state": state, "online_at": onlineAt, "created_at": createdAt])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var region: String? {
            get {
              return resultMap["region"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "region")
            }
          }

          public var image: String? {
            get {
              return resultMap["image"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "image")
            }
          }

          public var nickname: String? {
            get {
              return resultMap["nickname"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "nickname")
            }
          }

          public var lastname: String? {
            get {
              return resultMap["lastname"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "lastname")
            }
          }

          public var surname: String? {
            get {
              return resultMap["surname"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "surname")
            }
          }

          public var birthday: Double? {
            get {
              return resultMap["birthday"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "birthday")
            }
          }

          public var email: String? {
            get {
              return resultMap["email"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "email")
            }
          }

          public var gender: String? {
            get {
              return resultMap["gender"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "gender")
            }
          }

          public var phone: String? {
            get {
              return resultMap["phone"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "phone")
            }
          }

          public var state: Int? {
            get {
              return resultMap["state"] as? Int
            }
            set {
              resultMap.updateValue(newValue, forKey: "state")
            }
          }

          public var onlineAt: Double? {
            get {
              return resultMap["online_at"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "online_at")
            }
          }

          public var createdAt: Double? {
            get {
              return resultMap["created_at"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "created_at")
            }
          }
        }

        public struct File: GraphQLSelectionSet {
          public static let possibleTypes = ["PostFile"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("extension", type: .scalar(String.self)),
            GraphQLField("url", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(`extension`: String? = nil, url: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "PostFile", "extension": `extension`, "url": url])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var `extension`: String? {
            get {
              return resultMap["extension"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "extension")
            }
          }

          public var url: String? {
            get {
              return resultMap["url"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "url")
            }
          }
        }

        public struct TopicId: GraphQLSelectionSet {
          public static let possibleTypes = ["Topic"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("_id", type: .scalar(String.self)),
            GraphQLField("name", type: .scalar(String.self)),
            GraphQLField("userId", type: .scalar(String.self)),
            GraphQLField("classId", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil, userId: String? = nil, classId: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Topic", "_id": id, "name": name, "userId": userId, "classId": classId])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var userId: String? {
            get {
              return resultMap["userId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "userId")
            }
          }

          public var classId: String? {
            get {
              return resultMap["classId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "classId")
            }
          }
        }
      }
    }
  }
}

public final class UpdateCommentPostMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation updateCommentPost($classId: String!, $postId: String!, $userId: String!, $newContent: String!) {\n  updateCommentPost(classId: $classId, postId: $postId, userId: $userId, newContent: $newContent) {\n    __typename\n    error\n    message\n  }\n}"

  public var classId: String
  public var postId: String
  public var userId: String
  public var newContent: String

  public init(classId: String, postId: String, userId: String, newContent: String) {
    self.classId = classId
    self.postId = postId
    self.userId = userId
    self.newContent = newContent
  }

  public var variables: GraphQLMap? {
    return ["classId": classId, "postId": postId, "userId": userId, "newContent": newContent]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateCommentPost", arguments: ["classId": GraphQLVariable("classId"), "postId": GraphQLVariable("postId"), "userId": GraphQLVariable("userId"), "newContent": GraphQLVariable("newContent")], type: .object(UpdateCommentPost.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updateCommentPost: UpdateCommentPost? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "updateCommentPost": updateCommentPost.flatMap { (value: UpdateCommentPost) -> ResultMap in value.resultMap }])
    }

    public var updateCommentPost: UpdateCommentPost? {
      get {
        return (resultMap["updateCommentPost"] as? ResultMap).flatMap { UpdateCommentPost(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "updateCommentPost")
      }
    }

    public struct UpdateCommentPost: GraphQLSelectionSet {
      public static let possibleTypes = ["PostResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "PostResponse", "error": error, "message": message])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }
    }
  }
}

public final class GetsubUserQuery: GraphQLQuery {
  public let operationDefinition =
    "query getsubUser($userId: String!) {\n  subUser(userId: $userId) {\n    __typename\n    _id\n    userId\n    schoolId\n    storageLimit\n    tierId\n    path\n    studentIdCode\n    subscriptionEndDate\n    roleId\n    notification {\n      __typename\n      mchatNotificationCount\n      mclassNotificationCount\n      lastDeleted\n    }\n  }\n}"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("subUser", arguments: ["userId": GraphQLVariable("userId")], type: .object(SubUser.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(subUser: SubUser? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "subUser": subUser.flatMap { (value: SubUser) -> ResultMap in value.resultMap }])
    }

    public var subUser: SubUser? {
      get {
        return (resultMap["subUser"] as? ResultMap).flatMap { SubUser(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "subUser")
      }
    }

    public struct SubUser: GraphQLSelectionSet {
      public static let possibleTypes = ["SubUser"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("_id", type: .scalar(String.self)),
        GraphQLField("userId", type: .scalar(String.self)),
        GraphQLField("schoolId", type: .scalar(String.self)),
        GraphQLField("storageLimit", type: .scalar(Double.self)),
        GraphQLField("tierId", type: .scalar(String.self)),
        GraphQLField("path", type: .scalar(String.self)),
        GraphQLField("studentIdCode", type: .scalar(String.self)),
        GraphQLField("subscriptionEndDate", type: .scalar(Double.self)),
        GraphQLField("roleId", type: .scalar(String.self)),
        GraphQLField("notification", type: .object(Notification.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, userId: String? = nil, schoolId: String? = nil, storageLimit: Double? = nil, tierId: String? = nil, path: String? = nil, studentIdCode: String? = nil, subscriptionEndDate: Double? = nil, roleId: String? = nil, notification: Notification? = nil) {
        self.init(unsafeResultMap: ["__typename": "SubUser", "_id": id, "userId": userId, "schoolId": schoolId, "storageLimit": storageLimit, "tierId": tierId, "path": path, "studentIdCode": studentIdCode, "subscriptionEndDate": subscriptionEndDate, "roleId": roleId, "notification": notification.flatMap { (value: Notification) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["_id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "_id")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var schoolId: String? {
        get {
          return resultMap["schoolId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "schoolId")
        }
      }

      public var storageLimit: Double? {
        get {
          return resultMap["storageLimit"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "storageLimit")
        }
      }

      public var tierId: String? {
        get {
          return resultMap["tierId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "tierId")
        }
      }

      public var path: String? {
        get {
          return resultMap["path"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "path")
        }
      }

      public var studentIdCode: String? {
        get {
          return resultMap["studentIdCode"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "studentIdCode")
        }
      }

      public var subscriptionEndDate: Double? {
        get {
          return resultMap["subscriptionEndDate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "subscriptionEndDate")
        }
      }

      public var roleId: String? {
        get {
          return resultMap["roleId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "roleId")
        }
      }

      public var notification: Notification? {
        get {
          return (resultMap["notification"] as? ResultMap).flatMap { Notification(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "notification")
        }
      }

      public struct Notification: GraphQLSelectionSet {
        public static let possibleTypes = ["SubUserNotification"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("mchatNotificationCount", type: .scalar(Int.self)),
          GraphQLField("mclassNotificationCount", type: .scalar(Int.self)),
          GraphQLField("lastDeleted", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(mchatNotificationCount: Int? = nil, mclassNotificationCount: Int? = nil, lastDeleted: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "SubUserNotification", "mchatNotificationCount": mchatNotificationCount, "mclassNotificationCount": mclassNotificationCount, "lastDeleted": lastDeleted])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var mchatNotificationCount: Int? {
          get {
            return resultMap["mchatNotificationCount"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "mchatNotificationCount")
          }
        }

        public var mclassNotificationCount: Int? {
          get {
            return resultMap["mclassNotificationCount"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "mclassNotificationCount")
          }
        }

        public var lastDeleted: Double? {
          get {
            return resultMap["lastDeleted"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastDeleted")
          }
        }
      }
    }
  }
}

public final class ClassUsersListQuery: GraphQLQuery {
  public let operationDefinition =
    "query classUsersList($classId: String!) {\n  classesUserListClassId(classId: $classId) {\n    __typename\n    _id\n    classCode\n    classId\n    classOwnerId\n    classRole\n    userId {\n      __typename\n      _id\n      region\n      image\n      nickname\n      lastname\n      surname\n      birthday\n      email\n      gender\n      phone\n      state\n      online_at\n      created_at\n    }\n    roleId\n    className\n    state\n    joinedDate\n    topic\n  }\n}"

  public var classId: String

  public init(classId: String) {
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("classesUserListClassId", arguments: ["classId": GraphQLVariable("classId")], type: .list(.object(ClassesUserListClassId.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(classesUserListClassId: [ClassesUserListClassId?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "classesUserListClassId": classesUserListClassId.flatMap { (value: [ClassesUserListClassId?]) -> [ResultMap?] in value.map { (value: ClassesUserListClassId?) -> ResultMap? in value.flatMap { (value: ClassesUserListClassId) -> ResultMap in value.resultMap } } }])
    }

    public var classesUserListClassId: [ClassesUserListClassId?]? {
      get {
        return (resultMap["classesUserListClassId"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [ClassesUserListClassId?] in value.map { (value: ResultMap?) -> ClassesUserListClassId? in value.flatMap { (value: ResultMap) -> ClassesUserListClassId in ClassesUserListClassId(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [ClassesUserListClassId?]) -> [ResultMap?] in value.map { (value: ClassesUserListClassId?) -> ResultMap? in value.flatMap { (value: ClassesUserListClassId) -> ResultMap in value.resultMap } } }, forKey: "classesUserListClassId")
      }
    }

    public struct ClassesUserListClassId: GraphQLSelectionSet {
      public static let possibleTypes = ["PopulatedClassUserList"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("_id", type: .scalar(String.self)),
        GraphQLField("classCode", type: .scalar(String.self)),
        GraphQLField("classId", type: .scalar(String.self)),
        GraphQLField("classOwnerId", type: .scalar(String.self)),
        GraphQLField("classRole", type: .scalar(String.self)),
        GraphQLField("userId", type: .object(UserId.selections)),
        GraphQLField("roleId", type: .scalar(String.self)),
        GraphQLField("className", type: .scalar(String.self)),
        GraphQLField("state", type: .scalar(String.self)),
        GraphQLField("joinedDate", type: .scalar(Double.self)),
        GraphQLField("topic", type: .list(.scalar(String.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, classCode: String? = nil, classId: String? = nil, classOwnerId: String? = nil, classRole: String? = nil, userId: UserId? = nil, roleId: String? = nil, className: String? = nil, state: String? = nil, joinedDate: Double? = nil, topic: [String?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "PopulatedClassUserList", "_id": id, "classCode": classCode, "classId": classId, "classOwnerId": classOwnerId, "classRole": classRole, "userId": userId.flatMap { (value: UserId) -> ResultMap in value.resultMap }, "roleId": roleId, "className": className, "state": state, "joinedDate": joinedDate, "topic": topic])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["_id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "_id")
        }
      }

      public var classCode: String? {
        get {
          return resultMap["classCode"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "classCode")
        }
      }

      public var classId: String? {
        get {
          return resultMap["classId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "classId")
        }
      }

      public var classOwnerId: String? {
        get {
          return resultMap["classOwnerId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "classOwnerId")
        }
      }

      public var classRole: String? {
        get {
          return resultMap["classRole"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "classRole")
        }
      }

      public var userId: UserId? {
        get {
          return (resultMap["userId"] as? ResultMap).flatMap { UserId(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "userId")
        }
      }

      public var roleId: String? {
        get {
          return resultMap["roleId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "roleId")
        }
      }

      public var className: String? {
        get {
          return resultMap["className"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "className")
        }
      }

      public var state: String? {
        get {
          return resultMap["state"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "state")
        }
      }

      public var joinedDate: Double? {
        get {
          return resultMap["joinedDate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "joinedDate")
        }
      }

      public var topic: [String?]? {
        get {
          return resultMap["topic"] as? [String?]
        }
        set {
          resultMap.updateValue(newValue, forKey: "topic")
        }
      }

      public struct UserId: GraphQLSelectionSet {
        public static let possibleTypes = ["PublicUserInfo"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("region", type: .scalar(String.self)),
          GraphQLField("image", type: .scalar(String.self)),
          GraphQLField("nickname", type: .scalar(String.self)),
          GraphQLField("lastname", type: .scalar(String.self)),
          GraphQLField("surname", type: .scalar(String.self)),
          GraphQLField("birthday", type: .scalar(Double.self)),
          GraphQLField("email", type: .scalar(String.self)),
          GraphQLField("gender", type: .scalar(String.self)),
          GraphQLField("phone", type: .scalar(String.self)),
          GraphQLField("state", type: .scalar(Int.self)),
          GraphQLField("online_at", type: .scalar(Double.self)),
          GraphQLField("created_at", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, region: String? = nil, image: String? = nil, nickname: String? = nil, lastname: String? = nil, surname: String? = nil, birthday: Double? = nil, email: String? = nil, gender: String? = nil, phone: String? = nil, state: Int? = nil, onlineAt: Double? = nil, createdAt: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "PublicUserInfo", "_id": id, "region": region, "image": image, "nickname": nickname, "lastname": lastname, "surname": surname, "birthday": birthday, "email": email, "gender": gender, "phone": phone, "state": state, "online_at": onlineAt, "created_at": createdAt])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var region: String? {
          get {
            return resultMap["region"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "region")
          }
        }

        public var image: String? {
          get {
            return resultMap["image"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "image")
          }
        }

        public var nickname: String? {
          get {
            return resultMap["nickname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "nickname")
          }
        }

        public var lastname: String? {
          get {
            return resultMap["lastname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastname")
          }
        }

        public var surname: String? {
          get {
            return resultMap["surname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "surname")
          }
        }

        public var birthday: Double? {
          get {
            return resultMap["birthday"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "birthday")
          }
        }

        public var email: String? {
          get {
            return resultMap["email"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "email")
          }
        }

        public var gender: String? {
          get {
            return resultMap["gender"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "gender")
          }
        }

        public var phone: String? {
          get {
            return resultMap["phone"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "phone")
          }
        }

        public var state: Int? {
          get {
            return resultMap["state"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "state")
          }
        }

        public var onlineAt: Double? {
          get {
            return resultMap["online_at"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "online_at")
          }
        }

        public var createdAt: Double? {
          get {
            return resultMap["created_at"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "created_at")
          }
        }
      }
    }
  }
}

public final class GetquestionsetsQuery: GraphQLQuery {
  public let operationDefinition =
    "query getquestionsets($classId: String!, $teacherId: String!) {\n  questionSets(classId: $classId, teacherId: $teacherId) {\n    __typename\n    _id\n    name\n    teacherId\n    classId\n    questionCount\n    createdDate\n    updatedDate\n  }\n}"

  public var classId: String
  public var teacherId: String

  public init(classId: String, teacherId: String) {
    self.classId = classId
    self.teacherId = teacherId
  }

  public var variables: GraphQLMap? {
    return ["classId": classId, "teacherId": teacherId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("questionSets", arguments: ["classId": GraphQLVariable("classId"), "teacherId": GraphQLVariable("teacherId")], type: .list(.object(QuestionSet.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(questionSets: [QuestionSet?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "questionSets": questionSets.flatMap { (value: [QuestionSet?]) -> [ResultMap?] in value.map { (value: QuestionSet?) -> ResultMap? in value.flatMap { (value: QuestionSet) -> ResultMap in value.resultMap } } }])
    }

    public var questionSets: [QuestionSet?]? {
      get {
        return (resultMap["questionSets"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [QuestionSet?] in value.map { (value: ResultMap?) -> QuestionSet? in value.flatMap { (value: ResultMap) -> QuestionSet in QuestionSet(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [QuestionSet?]) -> [ResultMap?] in value.map { (value: QuestionSet?) -> ResultMap? in value.flatMap { (value: QuestionSet) -> ResultMap in value.resultMap } } }, forKey: "questionSets")
      }
    }

    public struct QuestionSet: GraphQLSelectionSet {
      public static let possibleTypes = ["QuestionSet"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("_id", type: .scalar(String.self)),
        GraphQLField("name", type: .scalar(String.self)),
        GraphQLField("teacherId", type: .scalar(String.self)),
        GraphQLField("classId", type: .scalar(String.self)),
        GraphQLField("questionCount", type: .scalar(Int.self)),
        GraphQLField("createdDate", type: .scalar(Double.self)),
        GraphQLField("updatedDate", type: .scalar(Double.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, name: String? = nil, teacherId: String? = nil, classId: String? = nil, questionCount: Int? = nil, createdDate: Double? = nil, updatedDate: Double? = nil) {
        self.init(unsafeResultMap: ["__typename": "QuestionSet", "_id": id, "name": name, "teacherId": teacherId, "classId": classId, "questionCount": questionCount, "createdDate": createdDate, "updatedDate": updatedDate])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["_id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "_id")
        }
      }

      public var name: String? {
        get {
          return resultMap["name"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "name")
        }
      }

      public var teacherId: String? {
        get {
          return resultMap["teacherId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "teacherId")
        }
      }

      public var classId: String? {
        get {
          return resultMap["classId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "classId")
        }
      }

      public var questionCount: Int? {
        get {
          return resultMap["questionCount"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "questionCount")
        }
      }

      public var createdDate: Double? {
        get {
          return resultMap["createdDate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "createdDate")
        }
      }

      public var updatedDate: Double? {
        get {
          return resultMap["updatedDate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "updatedDate")
        }
      }
    }
  }
}

public final class EnableLaunchMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation enableLaunch($teacherId: String!, $questionSetId: String!, $launchSettings: LaunchSettingsInput!, $classId: String!, $InputlaunchData: InputlaunchData!, $type: String!, $title: String!) {\n  enableLaunch(teacherId: $teacherId, questionSetId: $questionSetId, launchSettings: $launchSettings, classId: $classId, InputlaunchData: $InputlaunchData, type: $type, title: $title) {\n    __typename\n    _id\n    key\n    teacherId\n    questionSetId\n    isActive\n    launchedAt\n    launchSettings {\n      __typename\n      security\n      shuffleQuestion\n      shuffleAnswer\n      showResult\n      oneTry\n      reflection\n      timer\n    }\n    started\n    startedAt\n  }\n}"

  public var teacherId: String
  public var questionSetId: String
  public var launchSettings: LaunchSettingsInput
  public var classId: String
  public var InputlaunchData: InputlaunchData
  public var type: String
  public var title: String

  public init(teacherId: String, questionSetId: String, launchSettings: LaunchSettingsInput, classId: String, InputlaunchData: InputlaunchData, type: String, title: String) {
    self.teacherId = teacherId
    self.questionSetId = questionSetId
    self.launchSettings = launchSettings
    self.classId = classId
    self.InputlaunchData = InputlaunchData
    self.type = type
    self.title = title
  }

  public var variables: GraphQLMap? {
    return ["teacherId": teacherId, "questionSetId": questionSetId, "launchSettings": launchSettings, "classId": classId, "InputlaunchData": InputlaunchData, "type": type, "title": title]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("enableLaunch", arguments: ["teacherId": GraphQLVariable("teacherId"), "questionSetId": GraphQLVariable("questionSetId"), "launchSettings": GraphQLVariable("launchSettings"), "classId": GraphQLVariable("classId"), "InputlaunchData": GraphQLVariable("InputlaunchData"), "type": GraphQLVariable("type"), "title": GraphQLVariable("title")], type: .object(EnableLaunch.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(enableLaunch: EnableLaunch? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "enableLaunch": enableLaunch.flatMap { (value: EnableLaunch) -> ResultMap in value.resultMap }])
    }

    public var enableLaunch: EnableLaunch? {
      get {
        return (resultMap["enableLaunch"] as? ResultMap).flatMap { EnableLaunch(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "enableLaunch")
      }
    }

    public struct EnableLaunch: GraphQLSelectionSet {
      public static let possibleTypes = ["Launch"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("_id", type: .scalar(String.self)),
        GraphQLField("key", type: .scalar(String.self)),
        GraphQLField("teacherId", type: .scalar(String.self)),
        GraphQLField("questionSetId", type: .scalar(String.self)),
        GraphQLField("isActive", type: .scalar(Bool.self)),
        GraphQLField("launchedAt", type: .scalar(Double.self)),
        GraphQLField("launchSettings", type: .object(LaunchSetting.selections)),
        GraphQLField("started", type: .scalar(Bool.self)),
        GraphQLField("startedAt", type: .scalar(Double.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, key: String? = nil, teacherId: String? = nil, questionSetId: String? = nil, isActive: Bool? = nil, launchedAt: Double? = nil, launchSettings: LaunchSetting? = nil, started: Bool? = nil, startedAt: Double? = nil) {
        self.init(unsafeResultMap: ["__typename": "Launch", "_id": id, "key": key, "teacherId": teacherId, "questionSetId": questionSetId, "isActive": isActive, "launchedAt": launchedAt, "launchSettings": launchSettings.flatMap { (value: LaunchSetting) -> ResultMap in value.resultMap }, "started": started, "startedAt": startedAt])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["_id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "_id")
        }
      }

      public var key: String? {
        get {
          return resultMap["key"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "key")
        }
      }

      public var teacherId: String? {
        get {
          return resultMap["teacherId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "teacherId")
        }
      }

      public var questionSetId: String? {
        get {
          return resultMap["questionSetId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "questionSetId")
        }
      }

      public var isActive: Bool? {
        get {
          return resultMap["isActive"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isActive")
        }
      }

      public var launchedAt: Double? {
        get {
          return resultMap["launchedAt"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "launchedAt")
        }
      }

      public var launchSettings: LaunchSetting? {
        get {
          return (resultMap["launchSettings"] as? ResultMap).flatMap { LaunchSetting(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "launchSettings")
        }
      }

      public var started: Bool? {
        get {
          return resultMap["started"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "started")
        }
      }

      public var startedAt: Double? {
        get {
          return resultMap["startedAt"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "startedAt")
        }
      }

      public struct LaunchSetting: GraphQLSelectionSet {
        public static let possibleTypes = ["launchSettings"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("security", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("shuffleQuestion", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("shuffleAnswer", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("showResult", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("oneTry", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("reflection", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("timer", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(security: Bool, shuffleQuestion: Bool, shuffleAnswer: Bool, showResult: Bool, oneTry: Bool, reflection: Bool, timer: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "launchSettings", "security": security, "shuffleQuestion": shuffleQuestion, "shuffleAnswer": shuffleAnswer, "showResult": showResult, "oneTry": oneTry, "reflection": reflection, "timer": timer])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var security: Bool {
          get {
            return resultMap["security"]! as! Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "security")
          }
        }

        public var shuffleQuestion: Bool {
          get {
            return resultMap["shuffleQuestion"]! as! Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "shuffleQuestion")
          }
        }

        public var shuffleAnswer: Bool {
          get {
            return resultMap["shuffleAnswer"]! as! Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "shuffleAnswer")
          }
        }

        public var showResult: Bool {
          get {
            return resultMap["showResult"]! as! Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "showResult")
          }
        }

        public var oneTry: Bool {
          get {
            return resultMap["oneTry"]! as! Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "oneTry")
          }
        }

        public var reflection: Bool {
          get {
            return resultMap["reflection"]! as! Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "reflection")
          }
        }

        public var timer: Double? {
          get {
            return resultMap["timer"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "timer")
          }
        }
      }
    }
  }
}

public final class DisableLaunchMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation disableLaunch($launchId: String!, $teacherId: String!, $classId: String!) {\n  disableLaunch(launchId: $launchId, teacherId: $teacherId, classId: $classId) {\n    __typename\n    _id\n    launchId\n    totalEntry\n    average\n    key\n    studentResult {\n      __typename\n      studentId\n      correctAnswers\n      average\n      nickname\n      lastname\n    }\n    count {\n      __typename\n      questionId\n      mistakeCount\n      correctCount\n    }\n    launchedAt\n  }\n}"

  public var launchId: String
  public var teacherId: String
  public var classId: String

  public init(launchId: String, teacherId: String, classId: String) {
    self.launchId = launchId
    self.teacherId = teacherId
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["launchId": launchId, "teacherId": teacherId, "classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("disableLaunch", arguments: ["launchId": GraphQLVariable("launchId"), "teacherId": GraphQLVariable("teacherId"), "classId": GraphQLVariable("classId")], type: .object(DisableLaunch.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(disableLaunch: DisableLaunch? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "disableLaunch": disableLaunch.flatMap { (value: DisableLaunch) -> ResultMap in value.resultMap }])
    }

    public var disableLaunch: DisableLaunch? {
      get {
        return (resultMap["disableLaunch"] as? ResultMap).flatMap { DisableLaunch(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "disableLaunch")
      }
    }

    public struct DisableLaunch: GraphQLSelectionSet {
      public static let possibleTypes = ["Result"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("_id", type: .scalar(String.self)),
        GraphQLField("launchId", type: .scalar(String.self)),
        GraphQLField("totalEntry", type: .scalar(Int.self)),
        GraphQLField("average", type: .scalar(Double.self)),
        GraphQLField("key", type: .scalar(String.self)),
        GraphQLField("studentResult", type: .list(.object(StudentResult.selections))),
        GraphQLField("count", type: .list(.object(Count.selections))),
        GraphQLField("launchedAt", type: .scalar(Double.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, launchId: String? = nil, totalEntry: Int? = nil, average: Double? = nil, key: String? = nil, studentResult: [StudentResult?]? = nil, count: [Count?]? = nil, launchedAt: Double? = nil) {
        self.init(unsafeResultMap: ["__typename": "Result", "_id": id, "launchId": launchId, "totalEntry": totalEntry, "average": average, "key": key, "studentResult": studentResult.flatMap { (value: [StudentResult?]) -> [ResultMap?] in value.map { (value: StudentResult?) -> ResultMap? in value.flatMap { (value: StudentResult) -> ResultMap in value.resultMap } } }, "count": count.flatMap { (value: [Count?]) -> [ResultMap?] in value.map { (value: Count?) -> ResultMap? in value.flatMap { (value: Count) -> ResultMap in value.resultMap } } }, "launchedAt": launchedAt])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["_id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "_id")
        }
      }

      public var launchId: String? {
        get {
          return resultMap["launchId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "launchId")
        }
      }

      public var totalEntry: Int? {
        get {
          return resultMap["totalEntry"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "totalEntry")
        }
      }

      public var average: Double? {
        get {
          return resultMap["average"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average")
        }
      }

      public var key: String? {
        get {
          return resultMap["key"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "key")
        }
      }

      public var studentResult: [StudentResult?]? {
        get {
          return (resultMap["studentResult"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [StudentResult?] in value.map { (value: ResultMap?) -> StudentResult? in value.flatMap { (value: ResultMap) -> StudentResult in StudentResult(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [StudentResult?]) -> [ResultMap?] in value.map { (value: StudentResult?) -> ResultMap? in value.flatMap { (value: StudentResult) -> ResultMap in value.resultMap } } }, forKey: "studentResult")
        }
      }

      public var count: [Count?]? {
        get {
          return (resultMap["count"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Count?] in value.map { (value: ResultMap?) -> Count? in value.flatMap { (value: ResultMap) -> Count in Count(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Count?]) -> [ResultMap?] in value.map { (value: Count?) -> ResultMap? in value.flatMap { (value: Count) -> ResultMap in value.resultMap } } }, forKey: "count")
        }
      }

      public var launchedAt: Double? {
        get {
          return resultMap["launchedAt"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "launchedAt")
        }
      }

      public struct StudentResult: GraphQLSelectionSet {
        public static let possibleTypes = ["singleStudentIndepthResult"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("studentId", type: .scalar(String.self)),
          GraphQLField("correctAnswers", type: .scalar(Int.self)),
          GraphQLField("average", type: .scalar(Double.self)),
          GraphQLField("nickname", type: .scalar(String.self)),
          GraphQLField("lastname", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(studentId: String? = nil, correctAnswers: Int? = nil, average: Double? = nil, nickname: String? = nil, lastname: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "singleStudentIndepthResult", "studentId": studentId, "correctAnswers": correctAnswers, "average": average, "nickname": nickname, "lastname": lastname])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var studentId: String? {
          get {
            return resultMap["studentId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "studentId")
          }
        }

        public var correctAnswers: Int? {
          get {
            return resultMap["correctAnswers"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "correctAnswers")
          }
        }

        public var average: Double? {
          get {
            return resultMap["average"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "average")
          }
        }

        public var nickname: String? {
          get {
            return resultMap["nickname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "nickname")
          }
        }

        public var lastname: String? {
          get {
            return resultMap["lastname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastname")
          }
        }
      }

      public struct Count: GraphQLSelectionSet {
        public static let possibleTypes = ["countType"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("questionId", type: .scalar(String.self)),
          GraphQLField("mistakeCount", type: .scalar(Int.self)),
          GraphQLField("correctCount", type: .scalar(Int.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(questionId: String? = nil, mistakeCount: Int? = nil, correctCount: Int? = nil) {
          self.init(unsafeResultMap: ["__typename": "countType", "questionId": questionId, "mistakeCount": mistakeCount, "correctCount": correctCount])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var questionId: String? {
          get {
            return resultMap["questionId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "questionId")
          }
        }

        public var mistakeCount: Int? {
          get {
            return resultMap["mistakeCount"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "mistakeCount")
          }
        }

        public var correctCount: Int? {
          get {
            return resultMap["correctCount"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "correctCount")
          }
        }
      }
    }
  }
}

public final class ActiveLaunchTeacherQuery: GraphQLQuery {
  public let operationDefinition =
    "query activeLaunchTeacher($teacherId: String!, $classId: String!) {\n  activeLaunchTeacher(teacherId: $teacherId, classId: $classId) {\n    __typename\n    _id\n    key\n    teacherId\n    questionSetId\n    classId\n    isActive\n    launchedAt\n    launchSettings {\n      __typename\n      security\n      shuffleQuestion\n      shuffleAnswer\n      showResult\n      oneTry\n      reflection\n      timer\n    }\n    started\n    startedAt\n    type\n  }\n}"

  public var teacherId: String
  public var classId: String

  public init(teacherId: String, classId: String) {
    self.teacherId = teacherId
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["teacherId": teacherId, "classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("activeLaunchTeacher", arguments: ["teacherId": GraphQLVariable("teacherId"), "classId": GraphQLVariable("classId")], type: .object(ActiveLaunchTeacher.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(activeLaunchTeacher: ActiveLaunchTeacher? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "activeLaunchTeacher": activeLaunchTeacher.flatMap { (value: ActiveLaunchTeacher) -> ResultMap in value.resultMap }])
    }

    public var activeLaunchTeacher: ActiveLaunchTeacher? {
      get {
        return (resultMap["activeLaunchTeacher"] as? ResultMap).flatMap { ActiveLaunchTeacher(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "activeLaunchTeacher")
      }
    }

    public struct ActiveLaunchTeacher: GraphQLSelectionSet {
      public static let possibleTypes = ["Launch"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("_id", type: .scalar(String.self)),
        GraphQLField("key", type: .scalar(String.self)),
        GraphQLField("teacherId", type: .scalar(String.self)),
        GraphQLField("questionSetId", type: .scalar(String.self)),
        GraphQLField("classId", type: .scalar(String.self)),
        GraphQLField("isActive", type: .scalar(Bool.self)),
        GraphQLField("launchedAt", type: .scalar(Double.self)),
        GraphQLField("launchSettings", type: .object(LaunchSetting.selections)),
        GraphQLField("started", type: .scalar(Bool.self)),
        GraphQLField("startedAt", type: .scalar(Double.self)),
        GraphQLField("type", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, key: String? = nil, teacherId: String? = nil, questionSetId: String? = nil, classId: String? = nil, isActive: Bool? = nil, launchedAt: Double? = nil, launchSettings: LaunchSetting? = nil, started: Bool? = nil, startedAt: Double? = nil, type: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "Launch", "_id": id, "key": key, "teacherId": teacherId, "questionSetId": questionSetId, "classId": classId, "isActive": isActive, "launchedAt": launchedAt, "launchSettings": launchSettings.flatMap { (value: LaunchSetting) -> ResultMap in value.resultMap }, "started": started, "startedAt": startedAt, "type": type])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["_id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "_id")
        }
      }

      public var key: String? {
        get {
          return resultMap["key"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "key")
        }
      }

      public var teacherId: String? {
        get {
          return resultMap["teacherId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "teacherId")
        }
      }

      public var questionSetId: String? {
        get {
          return resultMap["questionSetId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "questionSetId")
        }
      }

      public var classId: String? {
        get {
          return resultMap["classId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "classId")
        }
      }

      public var isActive: Bool? {
        get {
          return resultMap["isActive"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isActive")
        }
      }

      public var launchedAt: Double? {
        get {
          return resultMap["launchedAt"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "launchedAt")
        }
      }

      public var launchSettings: LaunchSetting? {
        get {
          return (resultMap["launchSettings"] as? ResultMap).flatMap { LaunchSetting(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "launchSettings")
        }
      }

      public var started: Bool? {
        get {
          return resultMap["started"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "started")
        }
      }

      public var startedAt: Double? {
        get {
          return resultMap["startedAt"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "startedAt")
        }
      }

      public var type: String? {
        get {
          return resultMap["type"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "type")
        }
      }

      public struct LaunchSetting: GraphQLSelectionSet {
        public static let possibleTypes = ["launchSettings"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("security", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("shuffleQuestion", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("shuffleAnswer", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("showResult", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("oneTry", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("reflection", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("timer", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(security: Bool, shuffleQuestion: Bool, shuffleAnswer: Bool, showResult: Bool, oneTry: Bool, reflection: Bool, timer: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "launchSettings", "security": security, "shuffleQuestion": shuffleQuestion, "shuffleAnswer": shuffleAnswer, "showResult": showResult, "oneTry": oneTry, "reflection": reflection, "timer": timer])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var security: Bool {
          get {
            return resultMap["security"]! as! Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "security")
          }
        }

        public var shuffleQuestion: Bool {
          get {
            return resultMap["shuffleQuestion"]! as! Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "shuffleQuestion")
          }
        }

        public var shuffleAnswer: Bool {
          get {
            return resultMap["shuffleAnswer"]! as! Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "shuffleAnswer")
          }
        }

        public var showResult: Bool {
          get {
            return resultMap["showResult"]! as! Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "showResult")
          }
        }

        public var oneTry: Bool {
          get {
            return resultMap["oneTry"]! as! Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "oneTry")
          }
        }

        public var reflection: Bool {
          get {
            return resultMap["reflection"]! as! Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "reflection")
          }
        }

        public var timer: Double? {
          get {
            return resultMap["timer"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "timer")
          }
        }
      }
    }
  }
}

public final class SingleStudentAnswerQuery: GraphQLQuery {
  public let operationDefinition =
    "query singleStudentAnswer($teacherId: String!, $launchId: String!, $studentId: String!, $questionSetId: String!) {\n  singleStudentAnswers(teacherId: $teacherId, launchId: $launchId, studentId: $studentId, questionSetId: $questionSetId) {\n    __typename\n    average\n  }\n}"

  public var teacherId: String
  public var launchId: String
  public var studentId: String
  public var questionSetId: String

  public init(teacherId: String, launchId: String, studentId: String, questionSetId: String) {
    self.teacherId = teacherId
    self.launchId = launchId
    self.studentId = studentId
    self.questionSetId = questionSetId
  }

  public var variables: GraphQLMap? {
    return ["teacherId": teacherId, "launchId": launchId, "studentId": studentId, "questionSetId": questionSetId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("singleStudentAnswers", arguments: ["teacherId": GraphQLVariable("teacherId"), "launchId": GraphQLVariable("launchId"), "studentId": GraphQLVariable("studentId"), "questionSetId": GraphQLVariable("questionSetId")], type: .object(SingleStudentAnswer.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(singleStudentAnswers: SingleStudentAnswer? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "singleStudentAnswers": singleStudentAnswers.flatMap { (value: SingleStudentAnswer) -> ResultMap in value.resultMap }])
    }

    public var singleStudentAnswers: SingleStudentAnswer? {
      get {
        return (resultMap["singleStudentAnswers"] as? ResultMap).flatMap { SingleStudentAnswer(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "singleStudentAnswers")
      }
    }

    public struct SingleStudentAnswer: GraphQLSelectionSet {
      public static let possibleTypes = ["SingleStudentAnswers"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("average", type: .scalar(Double.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(average: Double? = nil) {
        self.init(unsafeResultMap: ["__typename": "SingleStudentAnswers", "average": average])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var average: Double? {
        get {
          return resultMap["average"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average")
        }
      }
    }
  }
}

public final class UpdateClassMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation updateClass($classId: String!, $teacherId: String!, $name: String!, $code: String!, $inputclassSetting: InputClassSettings!) {\n  updateClass(classId: $classId, teacherId: $teacherId, name: $name, code: $code, InputClassSettings: $inputclassSetting) {\n    __typename\n    error\n    message\n    status\n    result {\n      __typename\n      _id\n      classCode\n      classId\n      classOwnerId\n      userId\n      roleId\n      classRole\n      className\n      state\n      joinedDate\n      topic\n    }\n  }\n}"

  public var classId: String
  public var teacherId: String
  public var name: String
  public var code: String
  public var inputclassSetting: InputClassSettings

  public init(classId: String, teacherId: String, name: String, code: String, inputclassSetting: InputClassSettings) {
    self.classId = classId
    self.teacherId = teacherId
    self.name = name
    self.code = code
    self.inputclassSetting = inputclassSetting
  }

  public var variables: GraphQLMap? {
    return ["classId": classId, "teacherId": teacherId, "name": name, "code": code, "inputclassSetting": inputclassSetting]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateClass", arguments: ["classId": GraphQLVariable("classId"), "teacherId": GraphQLVariable("teacherId"), "name": GraphQLVariable("name"), "code": GraphQLVariable("code"), "InputClassSettings": GraphQLVariable("inputclassSetting")], type: .object(UpdateClass.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updateClass: UpdateClass? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "updateClass": updateClass.flatMap { (value: UpdateClass) -> ResultMap in value.resultMap }])
    }

    public var updateClass: UpdateClass? {
      get {
        return (resultMap["updateClass"] as? ResultMap).flatMap { UpdateClass(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "updateClass")
      }
    }

    public struct UpdateClass: GraphQLSelectionSet {
      public static let possibleTypes = ["ClassUserListNormalResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
        GraphQLField("result", type: .object(Result.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil, result: Result? = nil) {
        self.init(unsafeResultMap: ["__typename": "ClassUserListNormalResponse", "error": error, "message": message, "status": status, "result": result.flatMap { (value: Result) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var result: Result? {
        get {
          return (resultMap["result"] as? ResultMap).flatMap { Result(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "result")
        }
      }

      public struct Result: GraphQLSelectionSet {
        public static let possibleTypes = ["ClassUserList"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("classCode", type: .scalar(String.self)),
          GraphQLField("classId", type: .scalar(String.self)),
          GraphQLField("classOwnerId", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("roleId", type: .scalar(String.self)),
          GraphQLField("classRole", type: .scalar(String.self)),
          GraphQLField("className", type: .scalar(String.self)),
          GraphQLField("state", type: .scalar(String.self)),
          GraphQLField("joinedDate", type: .scalar(Double.self)),
          GraphQLField("topic", type: .list(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, classCode: String? = nil, classId: String? = nil, classOwnerId: String? = nil, userId: String? = nil, roleId: String? = nil, classRole: String? = nil, className: String? = nil, state: String? = nil, joinedDate: Double? = nil, topic: [String?]? = nil) {
          self.init(unsafeResultMap: ["__typename": "ClassUserList", "_id": id, "classCode": classCode, "classId": classId, "classOwnerId": classOwnerId, "userId": userId, "roleId": roleId, "classRole": classRole, "className": className, "state": state, "joinedDate": joinedDate, "topic": topic])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var classCode: String? {
          get {
            return resultMap["classCode"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classCode")
          }
        }

        public var classId: String? {
          get {
            return resultMap["classId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classId")
          }
        }

        public var classOwnerId: String? {
          get {
            return resultMap["classOwnerId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classOwnerId")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var roleId: String? {
          get {
            return resultMap["roleId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "roleId")
          }
        }

        public var classRole: String? {
          get {
            return resultMap["classRole"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classRole")
          }
        }

        public var className: String? {
          get {
            return resultMap["className"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "className")
          }
        }

        public var state: String? {
          get {
            return resultMap["state"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "state")
          }
        }

        public var joinedDate: Double? {
          get {
            return resultMap["joinedDate"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "joinedDate")
          }
        }

        public var topic: [String?]? {
          get {
            return resultMap["topic"] as? [String?]
          }
          set {
            resultMap.updateValue(newValue, forKey: "topic")
          }
        }
      }
    }
  }
}

public final class ClassQuery: GraphQLQuery {
  public let operationDefinition =
    "query class($classId: String!) {\n  class(classId: $classId) {\n    __typename\n    name\n    code\n    teacherId\n    classSettings {\n      __typename\n      restrictedPost\n      approvePost\n    }\n  }\n}"

  public var classId: String

  public init(classId: String) {
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("class", arguments: ["classId": GraphQLVariable("classId")], type: .object(Class.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(`class`: Class? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "class": `class`.flatMap { (value: Class) -> ResultMap in value.resultMap }])
    }

    public var `class`: Class? {
      get {
        return (resultMap["class"] as? ResultMap).flatMap { Class(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "class")
      }
    }

    public struct Class: GraphQLSelectionSet {
      public static let possibleTypes = ["Class"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("name", type: .scalar(String.self)),
        GraphQLField("code", type: .scalar(String.self)),
        GraphQLField("teacherId", type: .scalar(String.self)),
        GraphQLField("classSettings", type: .object(ClassSetting.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(name: String? = nil, code: String? = nil, teacherId: String? = nil, classSettings: ClassSetting? = nil) {
        self.init(unsafeResultMap: ["__typename": "Class", "name": name, "code": code, "teacherId": teacherId, "classSettings": classSettings.flatMap { (value: ClassSetting) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var name: String? {
        get {
          return resultMap["name"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "name")
        }
      }

      public var code: String? {
        get {
          return resultMap["code"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "code")
        }
      }

      public var teacherId: String? {
        get {
          return resultMap["teacherId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "teacherId")
        }
      }

      public var classSettings: ClassSetting? {
        get {
          return (resultMap["classSettings"] as? ResultMap).flatMap { ClassSetting(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "classSettings")
        }
      }

      public struct ClassSetting: GraphQLSelectionSet {
        public static let possibleTypes = ["ClassSettings"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("restrictedPost", type: .scalar(Bool.self)),
          GraphQLField("approvePost", type: .scalar(Bool.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(restrictedPost: Bool? = nil, approvePost: Bool? = nil) {
          self.init(unsafeResultMap: ["__typename": "ClassSettings", "restrictedPost": restrictedPost, "approvePost": approvePost])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var restrictedPost: Bool? {
          get {
            return resultMap["restrictedPost"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "restrictedPost")
          }
        }

        public var approvePost: Bool? {
          get {
            return resultMap["approvePost"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "approvePost")
          }
        }
      }
    }
  }
}

public final class LeaveClassMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation leaveClass($userId: String!, $classId: String!) {\n  leaveClassRoomList(userId: $userId, classId: $classId) {\n    __typename\n    error\n    message\n  }\n}"

  public var userId: String
  public var classId: String

  public init(userId: String, classId: String) {
    self.userId = userId
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("leaveClassRoomList", arguments: ["userId": GraphQLVariable("userId"), "classId": GraphQLVariable("classId")], type: .object(LeaveClassRoomList.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(leaveClassRoomList: LeaveClassRoomList? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "leaveClassRoomList": leaveClassRoomList.flatMap { (value: LeaveClassRoomList) -> ResultMap in value.resultMap }])
    }

    public var leaveClassRoomList: LeaveClassRoomList? {
      get {
        return (resultMap["leaveClassRoomList"] as? ResultMap).flatMap { LeaveClassRoomList(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "leaveClassRoomList")
      }
    }

    public struct LeaveClassRoomList: GraphQLSelectionSet {
      public static let possibleTypes = ["BasicResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "BasicResponse", "error": error, "message": message])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }
    }
  }
}

public final class KickClassRoomMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation kickClassRoom($userId: String!, $classId: String!, $targetUserId: String!) {\n  kickClassRoomList(userId: $userId, classId: $classId, targetUserId: $targetUserId) {\n    __typename\n    error\n    message\n  }\n}"

  public var userId: String
  public var classId: String
  public var targetUserId: String

  public init(userId: String, classId: String, targetUserId: String) {
    self.userId = userId
    self.classId = classId
    self.targetUserId = targetUserId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "classId": classId, "targetUserId": targetUserId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("kickClassRoomList", arguments: ["userId": GraphQLVariable("userId"), "classId": GraphQLVariable("classId"), "targetUserId": GraphQLVariable("targetUserId")], type: .object(KickClassRoomList.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(kickClassRoomList: KickClassRoomList? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "kickClassRoomList": kickClassRoomList.flatMap { (value: KickClassRoomList) -> ResultMap in value.resultMap }])
    }

    public var kickClassRoomList: KickClassRoomList? {
      get {
        return (resultMap["kickClassRoomList"] as? ResultMap).flatMap { KickClassRoomList(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "kickClassRoomList")
      }
    }

    public struct KickClassRoomList: GraphQLSelectionSet {
      public static let possibleTypes = ["BasicResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "BasicResponse", "error": error, "message": message])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }
    }
  }
}

public final class PostApproveQuery: GraphQLQuery {
  public let operationDefinition =
    "query postApprove($userId: String!, $classId: String!, $skip: Int, $limit: Int) {\n  postsApprove(userId: $userId, classId: $classId, skip: $skip, limit: $limit) {\n    __typename\n    _id\n    type\n    ownerId {\n      __typename\n      _id\n      region\n      image\n      nickname\n      lastname\n      surname\n      birthday\n      email\n      gender\n      phone\n      state\n      online_at\n      created_at\n    }\n    postId\n    content\n    approved\n    instructions\n    points\n    dueDate\n    link\n    file {\n      __typename\n      extension\n      url\n      name\n      size\n      thumbnail\n    }\n    postedDate\n    commentCount\n    topicId {\n      __typename\n      _id\n      name\n      userId\n      classId\n    }\n  }\n}"

  public var userId: String
  public var classId: String
  public var skip: Int?
  public var limit: Int?

  public init(userId: String, classId: String, skip: Int? = nil, limit: Int? = nil) {
    self.userId = userId
    self.classId = classId
    self.skip = skip
    self.limit = limit
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "classId": classId, "skip": skip, "limit": limit]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("postsApprove", arguments: ["userId": GraphQLVariable("userId"), "classId": GraphQLVariable("classId"), "skip": GraphQLVariable("skip"), "limit": GraphQLVariable("limit")], type: .list(.object(PostsApprove.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(postsApprove: [PostsApprove?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "postsApprove": postsApprove.flatMap { (value: [PostsApprove?]) -> [ResultMap?] in value.map { (value: PostsApprove?) -> ResultMap? in value.flatMap { (value: PostsApprove) -> ResultMap in value.resultMap } } }])
    }

    public var postsApprove: [PostsApprove?]? {
      get {
        return (resultMap["postsApprove"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [PostsApprove?] in value.map { (value: ResultMap?) -> PostsApprove? in value.flatMap { (value: ResultMap) -> PostsApprove in PostsApprove(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [PostsApprove?]) -> [ResultMap?] in value.map { (value: PostsApprove?) -> ResultMap? in value.flatMap { (value: PostsApprove) -> ResultMap in value.resultMap } } }, forKey: "postsApprove")
      }
    }

    public struct PostsApprove: GraphQLSelectionSet {
      public static let possibleTypes = ["UserPopulatedPost"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("_id", type: .scalar(String.self)),
        GraphQLField("type", type: .scalar(String.self)),
        GraphQLField("ownerId", type: .object(OwnerId.selections)),
        GraphQLField("postId", type: .scalar(String.self)),
        GraphQLField("content", type: .scalar(String.self)),
        GraphQLField("approved", type: .scalar(Bool.self)),
        GraphQLField("instructions", type: .scalar(String.self)),
        GraphQLField("points", type: .scalar(Int.self)),
        GraphQLField("dueDate", type: .scalar(Double.self)),
        GraphQLField("link", type: .list(.scalar(String.self))),
        GraphQLField("file", type: .list(.object(File.selections))),
        GraphQLField("postedDate", type: .scalar(Double.self)),
        GraphQLField("commentCount", type: .scalar(Int.self)),
        GraphQLField("topicId", type: .object(TopicId.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, type: String? = nil, ownerId: OwnerId? = nil, postId: String? = nil, content: String? = nil, approved: Bool? = nil, instructions: String? = nil, points: Int? = nil, dueDate: Double? = nil, link: [String?]? = nil, file: [File?]? = nil, postedDate: Double? = nil, commentCount: Int? = nil, topicId: TopicId? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserPopulatedPost", "_id": id, "type": type, "ownerId": ownerId.flatMap { (value: OwnerId) -> ResultMap in value.resultMap }, "postId": postId, "content": content, "approved": approved, "instructions": instructions, "points": points, "dueDate": dueDate, "link": link, "file": file.flatMap { (value: [File?]) -> [ResultMap?] in value.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } } }, "postedDate": postedDate, "commentCount": commentCount, "topicId": topicId.flatMap { (value: TopicId) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["_id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "_id")
        }
      }

      public var type: String? {
        get {
          return resultMap["type"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "type")
        }
      }

      public var ownerId: OwnerId? {
        get {
          return (resultMap["ownerId"] as? ResultMap).flatMap { OwnerId(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "ownerId")
        }
      }

      public var postId: String? {
        get {
          return resultMap["postId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "postId")
        }
      }

      public var content: String? {
        get {
          return resultMap["content"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "content")
        }
      }

      public var approved: Bool? {
        get {
          return resultMap["approved"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "approved")
        }
      }

      public var instructions: String? {
        get {
          return resultMap["instructions"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "instructions")
        }
      }

      public var points: Int? {
        get {
          return resultMap["points"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "points")
        }
      }

      public var dueDate: Double? {
        get {
          return resultMap["dueDate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "dueDate")
        }
      }

      public var link: [String?]? {
        get {
          return resultMap["link"] as? [String?]
        }
        set {
          resultMap.updateValue(newValue, forKey: "link")
        }
      }

      public var file: [File?]? {
        get {
          return (resultMap["file"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [File?] in value.map { (value: ResultMap?) -> File? in value.flatMap { (value: ResultMap) -> File in File(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [File?]) -> [ResultMap?] in value.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } } }, forKey: "file")
        }
      }

      public var postedDate: Double? {
        get {
          return resultMap["postedDate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "postedDate")
        }
      }

      public var commentCount: Int? {
        get {
          return resultMap["commentCount"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "commentCount")
        }
      }

      public var topicId: TopicId? {
        get {
          return (resultMap["topicId"] as? ResultMap).flatMap { TopicId(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "topicId")
        }
      }

      public struct OwnerId: GraphQLSelectionSet {
        public static let possibleTypes = ["PublicUserInfo"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("region", type: .scalar(String.self)),
          GraphQLField("image", type: .scalar(String.self)),
          GraphQLField("nickname", type: .scalar(String.self)),
          GraphQLField("lastname", type: .scalar(String.self)),
          GraphQLField("surname", type: .scalar(String.self)),
          GraphQLField("birthday", type: .scalar(Double.self)),
          GraphQLField("email", type: .scalar(String.self)),
          GraphQLField("gender", type: .scalar(String.self)),
          GraphQLField("phone", type: .scalar(String.self)),
          GraphQLField("state", type: .scalar(Int.self)),
          GraphQLField("online_at", type: .scalar(Double.self)),
          GraphQLField("created_at", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, region: String? = nil, image: String? = nil, nickname: String? = nil, lastname: String? = nil, surname: String? = nil, birthday: Double? = nil, email: String? = nil, gender: String? = nil, phone: String? = nil, state: Int? = nil, onlineAt: Double? = nil, createdAt: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "PublicUserInfo", "_id": id, "region": region, "image": image, "nickname": nickname, "lastname": lastname, "surname": surname, "birthday": birthday, "email": email, "gender": gender, "phone": phone, "state": state, "online_at": onlineAt, "created_at": createdAt])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var region: String? {
          get {
            return resultMap["region"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "region")
          }
        }

        public var image: String? {
          get {
            return resultMap["image"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "image")
          }
        }

        public var nickname: String? {
          get {
            return resultMap["nickname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "nickname")
          }
        }

        public var lastname: String? {
          get {
            return resultMap["lastname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastname")
          }
        }

        public var surname: String? {
          get {
            return resultMap["surname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "surname")
          }
        }

        public var birthday: Double? {
          get {
            return resultMap["birthday"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "birthday")
          }
        }

        public var email: String? {
          get {
            return resultMap["email"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "email")
          }
        }

        public var gender: String? {
          get {
            return resultMap["gender"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "gender")
          }
        }

        public var phone: String? {
          get {
            return resultMap["phone"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "phone")
          }
        }

        public var state: Int? {
          get {
            return resultMap["state"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "state")
          }
        }

        public var onlineAt: Double? {
          get {
            return resultMap["online_at"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "online_at")
          }
        }

        public var createdAt: Double? {
          get {
            return resultMap["created_at"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "created_at")
          }
        }
      }

      public struct File: GraphQLSelectionSet {
        public static let possibleTypes = ["PostFile"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("extension", type: .scalar(String.self)),
          GraphQLField("url", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("size", type: .scalar(Double.self)),
          GraphQLField("thumbnail", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(`extension`: String? = nil, url: String? = nil, name: String? = nil, size: Double? = nil, thumbnail: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "PostFile", "extension": `extension`, "url": url, "name": name, "size": size, "thumbnail": thumbnail])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var `extension`: String? {
          get {
            return resultMap["extension"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "extension")
          }
        }

        public var url: String? {
          get {
            return resultMap["url"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "url")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var size: Double? {
          get {
            return resultMap["size"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "size")
          }
        }

        public var thumbnail: String? {
          get {
            return resultMap["thumbnail"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "thumbnail")
          }
        }
      }

      public struct TopicId: GraphQLSelectionSet {
        public static let possibleTypes = ["Topic"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("classId", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, name: String? = nil, userId: String? = nil, classId: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Topic", "_id": id, "name": name, "userId": userId, "classId": classId])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var classId: String? {
          get {
            return resultMap["classId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classId")
          }
        }
      }
    }
  }
}

public final class AcceptClassRoomListMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation acceptClassRoomList($classUserListId: String!, $userId: String!) {\n  acceptClassRoomList(classUserListId: $classUserListId, userId: $userId) {\n    __typename\n    error\n    message\n    result {\n      __typename\n      _id\n      classCode\n      classId\n      classOwnerId\n      userId {\n        __typename\n        _id\n        image\n        nickname\n        lastname\n        email\n        phone\n        state\n      }\n      roleId\n      className\n      state\n      joinedDate\n      topic\n    }\n  }\n}"

  public var classUserListId: String
  public var userId: String

  public init(classUserListId: String, userId: String) {
    self.classUserListId = classUserListId
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["classUserListId": classUserListId, "userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("acceptClassRoomList", arguments: ["classUserListId": GraphQLVariable("classUserListId"), "userId": GraphQLVariable("userId")], type: .object(AcceptClassRoomList.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(acceptClassRoomList: AcceptClassRoomList? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "acceptClassRoomList": acceptClassRoomList.flatMap { (value: AcceptClassRoomList) -> ResultMap in value.resultMap }])
    }

    public var acceptClassRoomList: AcceptClassRoomList? {
      get {
        return (resultMap["acceptClassRoomList"] as? ResultMap).flatMap { AcceptClassRoomList(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "acceptClassRoomList")
      }
    }

    public struct AcceptClassRoomList: GraphQLSelectionSet {
      public static let possibleTypes = ["ClassUserListResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("result", type: .object(Result.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, result: Result? = nil) {
        self.init(unsafeResultMap: ["__typename": "ClassUserListResponse", "error": error, "message": message, "result": result.flatMap { (value: Result) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var result: Result? {
        get {
          return (resultMap["result"] as? ResultMap).flatMap { Result(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "result")
        }
      }

      public struct Result: GraphQLSelectionSet {
        public static let possibleTypes = ["PopulatedClassUserList"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("classCode", type: .scalar(String.self)),
          GraphQLField("classId", type: .scalar(String.self)),
          GraphQLField("classOwnerId", type: .scalar(String.self)),
          GraphQLField("userId", type: .object(UserId.selections)),
          GraphQLField("roleId", type: .scalar(String.self)),
          GraphQLField("className", type: .scalar(String.self)),
          GraphQLField("state", type: .scalar(String.self)),
          GraphQLField("joinedDate", type: .scalar(Double.self)),
          GraphQLField("topic", type: .list(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, classCode: String? = nil, classId: String? = nil, classOwnerId: String? = nil, userId: UserId? = nil, roleId: String? = nil, className: String? = nil, state: String? = nil, joinedDate: Double? = nil, topic: [String?]? = nil) {
          self.init(unsafeResultMap: ["__typename": "PopulatedClassUserList", "_id": id, "classCode": classCode, "classId": classId, "classOwnerId": classOwnerId, "userId": userId.flatMap { (value: UserId) -> ResultMap in value.resultMap }, "roleId": roleId, "className": className, "state": state, "joinedDate": joinedDate, "topic": topic])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var classCode: String? {
          get {
            return resultMap["classCode"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classCode")
          }
        }

        public var classId: String? {
          get {
            return resultMap["classId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classId")
          }
        }

        public var classOwnerId: String? {
          get {
            return resultMap["classOwnerId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classOwnerId")
          }
        }

        public var userId: UserId? {
          get {
            return (resultMap["userId"] as? ResultMap).flatMap { UserId(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "userId")
          }
        }

        public var roleId: String? {
          get {
            return resultMap["roleId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "roleId")
          }
        }

        public var className: String? {
          get {
            return resultMap["className"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "className")
          }
        }

        public var state: String? {
          get {
            return resultMap["state"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "state")
          }
        }

        public var joinedDate: Double? {
          get {
            return resultMap["joinedDate"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "joinedDate")
          }
        }

        public var topic: [String?]? {
          get {
            return resultMap["topic"] as? [String?]
          }
          set {
            resultMap.updateValue(newValue, forKey: "topic")
          }
        }

        public struct UserId: GraphQLSelectionSet {
          public static let possibleTypes = ["PublicUserInfo"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("_id", type: .scalar(String.self)),
            GraphQLField("image", type: .scalar(String.self)),
            GraphQLField("nickname", type: .scalar(String.self)),
            GraphQLField("lastname", type: .scalar(String.self)),
            GraphQLField("email", type: .scalar(String.self)),
            GraphQLField("phone", type: .scalar(String.self)),
            GraphQLField("state", type: .scalar(Int.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, image: String? = nil, nickname: String? = nil, lastname: String? = nil, email: String? = nil, phone: String? = nil, state: Int? = nil) {
            self.init(unsafeResultMap: ["__typename": "PublicUserInfo", "_id": id, "image": image, "nickname": nickname, "lastname": lastname, "email": email, "phone": phone, "state": state])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var image: String? {
            get {
              return resultMap["image"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "image")
            }
          }

          public var nickname: String? {
            get {
              return resultMap["nickname"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "nickname")
            }
          }

          public var lastname: String? {
            get {
              return resultMap["lastname"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "lastname")
            }
          }

          public var email: String? {
            get {
              return resultMap["email"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "email")
            }
          }

          public var phone: String? {
            get {
              return resultMap["phone"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "phone")
            }
          }

          public var state: Int? {
            get {
              return resultMap["state"] as? Int
            }
            set {
              resultMap.updateValue(newValue, forKey: "state")
            }
          }
        }
      }
    }
  }
}

public final class RejectClassRoomListMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation rejectClassRoomList($classUserListId: String!, $userId: String!) {\n  rejectClassRoomList(classUserListId: $classUserListId, userId: $userId) {\n    __typename\n    error\n    message\n    result {\n      __typename\n      _id\n      classCode\n      classId\n      classOwnerId\n      userId {\n        __typename\n        _id\n        image\n        nickname\n        lastname\n        email\n        phone\n        state\n      }\n      roleId\n      className\n      state\n      joinedDate\n      topic\n    }\n  }\n}"

  public var classUserListId: String
  public var userId: String

  public init(classUserListId: String, userId: String) {
    self.classUserListId = classUserListId
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["classUserListId": classUserListId, "userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("rejectClassRoomList", arguments: ["classUserListId": GraphQLVariable("classUserListId"), "userId": GraphQLVariable("userId")], type: .object(RejectClassRoomList.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(rejectClassRoomList: RejectClassRoomList? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "rejectClassRoomList": rejectClassRoomList.flatMap { (value: RejectClassRoomList) -> ResultMap in value.resultMap }])
    }

    public var rejectClassRoomList: RejectClassRoomList? {
      get {
        return (resultMap["rejectClassRoomList"] as? ResultMap).flatMap { RejectClassRoomList(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "rejectClassRoomList")
      }
    }

    public struct RejectClassRoomList: GraphQLSelectionSet {
      public static let possibleTypes = ["ClassUserListResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("result", type: .object(Result.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, result: Result? = nil) {
        self.init(unsafeResultMap: ["__typename": "ClassUserListResponse", "error": error, "message": message, "result": result.flatMap { (value: Result) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var result: Result? {
        get {
          return (resultMap["result"] as? ResultMap).flatMap { Result(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "result")
        }
      }

      public struct Result: GraphQLSelectionSet {
        public static let possibleTypes = ["PopulatedClassUserList"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("classCode", type: .scalar(String.self)),
          GraphQLField("classId", type: .scalar(String.self)),
          GraphQLField("classOwnerId", type: .scalar(String.self)),
          GraphQLField("userId", type: .object(UserId.selections)),
          GraphQLField("roleId", type: .scalar(String.self)),
          GraphQLField("className", type: .scalar(String.self)),
          GraphQLField("state", type: .scalar(String.self)),
          GraphQLField("joinedDate", type: .scalar(Double.self)),
          GraphQLField("topic", type: .list(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, classCode: String? = nil, classId: String? = nil, classOwnerId: String? = nil, userId: UserId? = nil, roleId: String? = nil, className: String? = nil, state: String? = nil, joinedDate: Double? = nil, topic: [String?]? = nil) {
          self.init(unsafeResultMap: ["__typename": "PopulatedClassUserList", "_id": id, "classCode": classCode, "classId": classId, "classOwnerId": classOwnerId, "userId": userId.flatMap { (value: UserId) -> ResultMap in value.resultMap }, "roleId": roleId, "className": className, "state": state, "joinedDate": joinedDate, "topic": topic])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var classCode: String? {
          get {
            return resultMap["classCode"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classCode")
          }
        }

        public var classId: String? {
          get {
            return resultMap["classId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classId")
          }
        }

        public var classOwnerId: String? {
          get {
            return resultMap["classOwnerId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classOwnerId")
          }
        }

        public var userId: UserId? {
          get {
            return (resultMap["userId"] as? ResultMap).flatMap { UserId(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "userId")
          }
        }

        public var roleId: String? {
          get {
            return resultMap["roleId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "roleId")
          }
        }

        public var className: String? {
          get {
            return resultMap["className"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "className")
          }
        }

        public var state: String? {
          get {
            return resultMap["state"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "state")
          }
        }

        public var joinedDate: Double? {
          get {
            return resultMap["joinedDate"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "joinedDate")
          }
        }

        public var topic: [String?]? {
          get {
            return resultMap["topic"] as? [String?]
          }
          set {
            resultMap.updateValue(newValue, forKey: "topic")
          }
        }

        public struct UserId: GraphQLSelectionSet {
          public static let possibleTypes = ["PublicUserInfo"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("_id", type: .scalar(String.self)),
            GraphQLField("image", type: .scalar(String.self)),
            GraphQLField("nickname", type: .scalar(String.self)),
            GraphQLField("lastname", type: .scalar(String.self)),
            GraphQLField("email", type: .scalar(String.self)),
            GraphQLField("phone", type: .scalar(String.self)),
            GraphQLField("state", type: .scalar(Int.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, image: String? = nil, nickname: String? = nil, lastname: String? = nil, email: String? = nil, phone: String? = nil, state: Int? = nil) {
            self.init(unsafeResultMap: ["__typename": "PublicUserInfo", "_id": id, "image": image, "nickname": nickname, "lastname": lastname, "email": email, "phone": phone, "state": state])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var image: String? {
            get {
              return resultMap["image"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "image")
            }
          }

          public var nickname: String? {
            get {
              return resultMap["nickname"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "nickname")
            }
          }

          public var lastname: String? {
            get {
              return resultMap["lastname"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "lastname")
            }
          }

          public var email: String? {
            get {
              return resultMap["email"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "email")
            }
          }

          public var phone: String? {
            get {
              return resultMap["phone"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "phone")
            }
          }

          public var state: Int? {
            get {
              return resultMap["state"] as? Int
            }
            set {
              resultMap.updateValue(newValue, forKey: "state")
            }
          }
        }
      }
    }
  }
}

public final class ApprovePostMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation ApprovePost($userId: String!, $postId: String!, $classId: String!) {\n  approvePost(userId: $userId, postId: $postId, classId: $classId) {\n    __typename\n    error\n    message\n    result {\n      __typename\n      _id\n      type\n      ownerId {\n        __typename\n        _id\n        image\n        nickname\n        lastname\n        email\n        phone\n      }\n      postId\n      content\n      approved\n      instructions\n      points\n      link\n      file {\n        __typename\n        extension\n        url\n        name\n        size\n        thumbnail\n      }\n      postedDate\n      commentCount\n      topicId {\n        __typename\n        _id\n        name\n        userId\n        classId\n      }\n    }\n  }\n}"

  public var userId: String
  public var postId: String
  public var classId: String

  public init(userId: String, postId: String, classId: String) {
    self.userId = userId
    self.postId = postId
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "postId": postId, "classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("approvePost", arguments: ["userId": GraphQLVariable("userId"), "postId": GraphQLVariable("postId"), "classId": GraphQLVariable("classId")], type: .object(ApprovePost.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(approvePost: ApprovePost? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "approvePost": approvePost.flatMap { (value: ApprovePost) -> ResultMap in value.resultMap }])
    }

    /// insertMaterial(InputPost:InputPost!, classId: String!):PostResponse
    /// insertAssignment(InputAssignment:InputAssignment!,classId:String!):PostResponse
    public var approvePost: ApprovePost? {
      get {
        return (resultMap["approvePost"] as? ResultMap).flatMap { ApprovePost(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "approvePost")
      }
    }

    public struct ApprovePost: GraphQLSelectionSet {
      public static let possibleTypes = ["PostResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("result", type: .object(Result.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, result: Result? = nil) {
        self.init(unsafeResultMap: ["__typename": "PostResponse", "error": error, "message": message, "result": result.flatMap { (value: Result) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var result: Result? {
        get {
          return (resultMap["result"] as? ResultMap).flatMap { Result(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "result")
        }
      }

      public struct Result: GraphQLSelectionSet {
        public static let possibleTypes = ["UserPopulatedPost"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("type", type: .scalar(String.self)),
          GraphQLField("ownerId", type: .object(OwnerId.selections)),
          GraphQLField("postId", type: .scalar(String.self)),
          GraphQLField("content", type: .scalar(String.self)),
          GraphQLField("approved", type: .scalar(Bool.self)),
          GraphQLField("instructions", type: .scalar(String.self)),
          GraphQLField("points", type: .scalar(Int.self)),
          GraphQLField("link", type: .list(.scalar(String.self))),
          GraphQLField("file", type: .list(.object(File.selections))),
          GraphQLField("postedDate", type: .scalar(Double.self)),
          GraphQLField("commentCount", type: .scalar(Int.self)),
          GraphQLField("topicId", type: .object(TopicId.selections)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, type: String? = nil, ownerId: OwnerId? = nil, postId: String? = nil, content: String? = nil, approved: Bool? = nil, instructions: String? = nil, points: Int? = nil, link: [String?]? = nil, file: [File?]? = nil, postedDate: Double? = nil, commentCount: Int? = nil, topicId: TopicId? = nil) {
          self.init(unsafeResultMap: ["__typename": "UserPopulatedPost", "_id": id, "type": type, "ownerId": ownerId.flatMap { (value: OwnerId) -> ResultMap in value.resultMap }, "postId": postId, "content": content, "approved": approved, "instructions": instructions, "points": points, "link": link, "file": file.flatMap { (value: [File?]) -> [ResultMap?] in value.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } } }, "postedDate": postedDate, "commentCount": commentCount, "topicId": topicId.flatMap { (value: TopicId) -> ResultMap in value.resultMap }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var type: String? {
          get {
            return resultMap["type"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "type")
          }
        }

        public var ownerId: OwnerId? {
          get {
            return (resultMap["ownerId"] as? ResultMap).flatMap { OwnerId(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "ownerId")
          }
        }

        public var postId: String? {
          get {
            return resultMap["postId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "postId")
          }
        }

        public var content: String? {
          get {
            return resultMap["content"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "content")
          }
        }

        public var approved: Bool? {
          get {
            return resultMap["approved"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "approved")
          }
        }

        public var instructions: String? {
          get {
            return resultMap["instructions"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "instructions")
          }
        }

        public var points: Int? {
          get {
            return resultMap["points"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "points")
          }
        }

        public var link: [String?]? {
          get {
            return resultMap["link"] as? [String?]
          }
          set {
            resultMap.updateValue(newValue, forKey: "link")
          }
        }

        public var file: [File?]? {
          get {
            return (resultMap["file"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [File?] in value.map { (value: ResultMap?) -> File? in value.flatMap { (value: ResultMap) -> File in File(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [File?]) -> [ResultMap?] in value.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } } }, forKey: "file")
          }
        }

        public var postedDate: Double? {
          get {
            return resultMap["postedDate"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "postedDate")
          }
        }

        public var commentCount: Int? {
          get {
            return resultMap["commentCount"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "commentCount")
          }
        }

        public var topicId: TopicId? {
          get {
            return (resultMap["topicId"] as? ResultMap).flatMap { TopicId(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "topicId")
          }
        }

        public struct OwnerId: GraphQLSelectionSet {
          public static let possibleTypes = ["PublicUserInfo"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("_id", type: .scalar(String.self)),
            GraphQLField("image", type: .scalar(String.self)),
            GraphQLField("nickname", type: .scalar(String.self)),
            GraphQLField("lastname", type: .scalar(String.self)),
            GraphQLField("email", type: .scalar(String.self)),
            GraphQLField("phone", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, image: String? = nil, nickname: String? = nil, lastname: String? = nil, email: String? = nil, phone: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "PublicUserInfo", "_id": id, "image": image, "nickname": nickname, "lastname": lastname, "email": email, "phone": phone])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var image: String? {
            get {
              return resultMap["image"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "image")
            }
          }

          public var nickname: String? {
            get {
              return resultMap["nickname"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "nickname")
            }
          }

          public var lastname: String? {
            get {
              return resultMap["lastname"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "lastname")
            }
          }

          public var email: String? {
            get {
              return resultMap["email"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "email")
            }
          }

          public var phone: String? {
            get {
              return resultMap["phone"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "phone")
            }
          }
        }

        public struct File: GraphQLSelectionSet {
          public static let possibleTypes = ["PostFile"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("extension", type: .scalar(String.self)),
            GraphQLField("url", type: .scalar(String.self)),
            GraphQLField("name", type: .scalar(String.self)),
            GraphQLField("size", type: .scalar(Double.self)),
            GraphQLField("thumbnail", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(`extension`: String? = nil, url: String? = nil, name: String? = nil, size: Double? = nil, thumbnail: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "PostFile", "extension": `extension`, "url": url, "name": name, "size": size, "thumbnail": thumbnail])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var `extension`: String? {
            get {
              return resultMap["extension"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "extension")
            }
          }

          public var url: String? {
            get {
              return resultMap["url"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "url")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var size: Double? {
            get {
              return resultMap["size"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "size")
            }
          }

          public var thumbnail: String? {
            get {
              return resultMap["thumbnail"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "thumbnail")
            }
          }
        }

        public struct TopicId: GraphQLSelectionSet {
          public static let possibleTypes = ["Topic"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("_id", type: .scalar(String.self)),
            GraphQLField("name", type: .scalar(String.self)),
            GraphQLField("userId", type: .scalar(String.self)),
            GraphQLField("classId", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil, userId: String? = nil, classId: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Topic", "_id": id, "name": name, "userId": userId, "classId": classId])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var userId: String? {
            get {
              return resultMap["userId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "userId")
            }
          }

          public var classId: String? {
            get {
              return resultMap["classId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "classId")
            }
          }
        }
      }
    }
  }
}

public final class ChangeApproveSettingsMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation changeApproveSettings($userId: String!, $postIds: [String]!, $classId: String!, $action: Boolean!) {\n  changeApproveSettings(userId: $userId, postIds: $postIds, classId: $classId, action: $action) {\n    __typename\n    error\n    message\n  }\n}"

  public var userId: String
  public var postIds: [String?]
  public var classId: String
  public var action: Bool

  public init(userId: String, postIds: [String?], classId: String, action: Bool) {
    self.userId = userId
    self.postIds = postIds
    self.classId = classId
    self.action = action
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "postIds": postIds, "classId": classId, "action": action]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("changeApproveSettings", arguments: ["userId": GraphQLVariable("userId"), "postIds": GraphQLVariable("postIds"), "classId": GraphQLVariable("classId"), "action": GraphQLVariable("action")], type: .object(ChangeApproveSetting.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(changeApproveSettings: ChangeApproveSetting? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "changeApproveSettings": changeApproveSettings.flatMap { (value: ChangeApproveSetting) -> ResultMap in value.resultMap }])
    }

    public var changeApproveSettings: ChangeApproveSetting? {
      get {
        return (resultMap["changeApproveSettings"] as? ResultMap).flatMap { ChangeApproveSetting(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "changeApproveSettings")
      }
    }

    public struct ChangeApproveSetting: GraphQLSelectionSet {
      public static let possibleTypes = ["BasicResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "BasicResponse", "error": error, "message": message])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }
    }
  }
}

public final class CheckIrtsMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation checkIrts($userId: String!, $code: String!, $lat: Float!, $long: Float!, $classId: String!) {\n  checkInOrOut(userId: $userId, code: $code, latitude: $lat, longitude: $long, classId: $classId) {\n    __typename\n    error\n    message\n    status\n  }\n}"

  public var userId: String
  public var code: String
  public var lat: Double
  public var long: Double
  public var classId: String

  public init(userId: String, code: String, lat: Double, long: Double, classId: String) {
    self.userId = userId
    self.code = code
    self.lat = lat
    self.long = long
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "code": code, "lat": lat, "long": long, "classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("checkInOrOut", arguments: ["userId": GraphQLVariable("userId"), "code": GraphQLVariable("code"), "latitude": GraphQLVariable("lat"), "longitude": GraphQLVariable("long"), "classId": GraphQLVariable("classId")], type: .object(CheckInOrOut.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(checkInOrOut: CheckInOrOut? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "checkInOrOut": checkInOrOut.flatMap { (value: CheckInOrOut) -> ResultMap in value.resultMap }])
    }

    public var checkInOrOut: CheckInOrOut? {
      get {
        return (resultMap["checkInOrOut"] as? ResultMap).flatMap { CheckInOrOut(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "checkInOrOut")
      }
    }

    public struct CheckInOrOut: GraphQLSelectionSet {
      public static let possibleTypes = ["BasicStatusResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "BasicStatusResponse", "error": error, "message": message, "status": status])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }
    }
  }
}

public final class InsertAttendanceMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation insertAttendance($userId: String!, $classId: String!, $input: InputAttendance!) {\n  insertAttendance(userId: $userId, classId: $classId, InputAttendance: $input) {\n    __typename\n    error\n    message\n    status\n    result {\n      __typename\n      _id\n      userId\n      settings {\n        __typename\n        enforceGeoLocation\n      }\n      location {\n        __typename\n        latitude\n        longitude\n      }\n      code\n      radius\n      time {\n        __typename\n        timeEnds\n        timeBegan\n      }\n      topicId {\n        __typename\n        _id\n        name\n        userId\n        classId\n      }\n      createdDate\n      serverTime\n    }\n  }\n}"

  public var userId: String
  public var classId: String
  public var input: InputAttendance

  public init(userId: String, classId: String, input: InputAttendance) {
    self.userId = userId
    self.classId = classId
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "classId": classId, "input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("insertAttendance", arguments: ["userId": GraphQLVariable("userId"), "classId": GraphQLVariable("classId"), "InputAttendance": GraphQLVariable("input")], type: .object(InsertAttendance.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(insertAttendance: InsertAttendance? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "insertAttendance": insertAttendance.flatMap { (value: InsertAttendance) -> ResultMap in value.resultMap }])
    }

    /// Attendance
    public var insertAttendance: InsertAttendance? {
      get {
        return (resultMap["insertAttendance"] as? ResultMap).flatMap { InsertAttendance(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "insertAttendance")
      }
    }

    public struct InsertAttendance: GraphQLSelectionSet {
      public static let possibleTypes = ["AttendanceResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
        GraphQLField("result", type: .object(Result.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil, result: Result? = nil) {
        self.init(unsafeResultMap: ["__typename": "AttendanceResponse", "error": error, "message": message, "status": status, "result": result.flatMap { (value: Result) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var result: Result? {
        get {
          return (resultMap["result"] as? ResultMap).flatMap { Result(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "result")
        }
      }

      public struct Result: GraphQLSelectionSet {
        public static let possibleTypes = ["Attendance"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("settings", type: .object(Setting.selections)),
          GraphQLField("location", type: .object(Location.selections)),
          GraphQLField("code", type: .scalar(String.self)),
          GraphQLField("radius", type: .scalar(Int.self)),
          GraphQLField("time", type: .object(Time.selections)),
          GraphQLField("topicId", type: .object(TopicId.selections)),
          GraphQLField("createdDate", type: .scalar(Double.self)),
          GraphQLField("serverTime", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, userId: String? = nil, settings: Setting? = nil, location: Location? = nil, code: String? = nil, radius: Int? = nil, time: Time? = nil, topicId: TopicId? = nil, createdDate: Double? = nil, serverTime: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "Attendance", "_id": id, "userId": userId, "settings": settings.flatMap { (value: Setting) -> ResultMap in value.resultMap }, "location": location.flatMap { (value: Location) -> ResultMap in value.resultMap }, "code": code, "radius": radius, "time": time.flatMap { (value: Time) -> ResultMap in value.resultMap }, "topicId": topicId.flatMap { (value: TopicId) -> ResultMap in value.resultMap }, "createdDate": createdDate, "serverTime": serverTime])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var settings: Setting? {
          get {
            return (resultMap["settings"] as? ResultMap).flatMap { Setting(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "settings")
          }
        }

        public var location: Location? {
          get {
            return (resultMap["location"] as? ResultMap).flatMap { Location(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "location")
          }
        }

        public var code: String? {
          get {
            return resultMap["code"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "code")
          }
        }

        public var radius: Int? {
          get {
            return resultMap["radius"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "radius")
          }
        }

        public var time: Time? {
          get {
            return (resultMap["time"] as? ResultMap).flatMap { Time(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "time")
          }
        }

        public var topicId: TopicId? {
          get {
            return (resultMap["topicId"] as? ResultMap).flatMap { TopicId(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "topicId")
          }
        }

        public var createdDate: Double? {
          get {
            return resultMap["createdDate"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "createdDate")
          }
        }

        public var serverTime: Double? {
          get {
            return resultMap["serverTime"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "serverTime")
          }
        }

        public struct Setting: GraphQLSelectionSet {
          public static let possibleTypes = ["AttendanceSettings"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("enforceGeoLocation", type: .scalar(Bool.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(enforceGeoLocation: Bool? = nil) {
            self.init(unsafeResultMap: ["__typename": "AttendanceSettings", "enforceGeoLocation": enforceGeoLocation])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var enforceGeoLocation: Bool? {
            get {
              return resultMap["enforceGeoLocation"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "enforceGeoLocation")
            }
          }
        }

        public struct Location: GraphQLSelectionSet {
          public static let possibleTypes = ["AttendanceLocation"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("latitude", type: .scalar(Double.self)),
            GraphQLField("longitude", type: .scalar(Double.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(latitude: Double? = nil, longitude: Double? = nil) {
            self.init(unsafeResultMap: ["__typename": "AttendanceLocation", "latitude": latitude, "longitude": longitude])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var latitude: Double? {
            get {
              return resultMap["latitude"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "latitude")
            }
          }

          public var longitude: Double? {
            get {
              return resultMap["longitude"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "longitude")
            }
          }
        }

        public struct Time: GraphQLSelectionSet {
          public static let possibleTypes = ["AttendanceTime"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("timeEnds", type: .scalar(Double.self)),
            GraphQLField("timeBegan", type: .scalar(Double.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(timeEnds: Double? = nil, timeBegan: Double? = nil) {
            self.init(unsafeResultMap: ["__typename": "AttendanceTime", "timeEnds": timeEnds, "timeBegan": timeBegan])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var timeEnds: Double? {
            get {
              return resultMap["timeEnds"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "timeEnds")
            }
          }

          public var timeBegan: Double? {
            get {
              return resultMap["timeBegan"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "timeBegan")
            }
          }
        }

        public struct TopicId: GraphQLSelectionSet {
          public static let possibleTypes = ["Topic"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("_id", type: .scalar(String.self)),
            GraphQLField("name", type: .scalar(String.self)),
            GraphQLField("userId", type: .scalar(String.self)),
            GraphQLField("classId", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil, userId: String? = nil, classId: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Topic", "_id": id, "name": name, "userId": userId, "classId": classId])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var userId: String? {
            get {
              return resultMap["userId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "userId")
            }
          }

          public var classId: String? {
            get {
              return resultMap["classId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "classId")
            }
          }
        }
      }
    }
  }
}

public final class UsersAttendancesQuery: GraphQLQuery {
  public let operationDefinition =
    "query usersAttendances($userId: String!, $classId: String!) {\n  usersAttandances(userId: $userId, classId: $classId) {\n    __typename\n    _id\n    userId {\n      __typename\n      _id\n      nickname\n      lastname\n      created_at\n      phone\n    }\n    settings {\n      __typename\n      enforceGeoLocation\n    }\n    location {\n      __typename\n      latitude\n      longitude\n    }\n    topicId {\n      __typename\n      _id\n      name\n      userId\n      classId\n    }\n    code\n    radius\n    time {\n      __typename\n      timeEnds\n      timeBegan\n    }\n    createdDate\n    serverTime\n    classTime\n    attendance {\n      __typename\n      _id\n      userId\n      attendanceId\n      checkedIn\n      checkedOut\n      state\n      type\n    }\n  }\n}"

  public var userId: String
  public var classId: String

  public init(userId: String, classId: String) {
    self.userId = userId
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("usersAttandances", arguments: ["userId": GraphQLVariable("userId"), "classId": GraphQLVariable("classId")], type: .list(.object(UsersAttandance.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(usersAttandances: [UsersAttandance?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "usersAttandances": usersAttandances.flatMap { (value: [UsersAttandance?]) -> [ResultMap?] in value.map { (value: UsersAttandance?) -> ResultMap? in value.flatMap { (value: UsersAttandance) -> ResultMap in value.resultMap } } }])
    }

    public var usersAttandances: [UsersAttandance?]? {
      get {
        return (resultMap["usersAttandances"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [UsersAttandance?] in value.map { (value: ResultMap?) -> UsersAttandance? in value.flatMap { (value: ResultMap) -> UsersAttandance in UsersAttandance(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [UsersAttandance?]) -> [ResultMap?] in value.map { (value: UsersAttandance?) -> ResultMap? in value.flatMap { (value: UsersAttandance) -> ResultMap in value.resultMap } } }, forKey: "usersAttandances")
      }
    }

    public struct UsersAttandance: GraphQLSelectionSet {
      public static let possibleTypes = ["PopulatedAttendance"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("_id", type: .scalar(String.self)),
        GraphQLField("userId", type: .object(UserId.selections)),
        GraphQLField("settings", type: .object(Setting.selections)),
        GraphQLField("location", type: .object(Location.selections)),
        GraphQLField("topicId", type: .object(TopicId.selections)),
        GraphQLField("code", type: .scalar(String.self)),
        GraphQLField("radius", type: .scalar(Double.self)),
        GraphQLField("time", type: .object(Time.selections)),
        GraphQLField("createdDate", type: .scalar(Double.self)),
        GraphQLField("serverTime", type: .scalar(Double.self)),
        GraphQLField("classTime", type: .scalar(Double.self)),
        GraphQLField("attendance", type: .object(Attendance.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, userId: UserId? = nil, settings: Setting? = nil, location: Location? = nil, topicId: TopicId? = nil, code: String? = nil, radius: Double? = nil, time: Time? = nil, createdDate: Double? = nil, serverTime: Double? = nil, classTime: Double? = nil, attendance: Attendance? = nil) {
        self.init(unsafeResultMap: ["__typename": "PopulatedAttendance", "_id": id, "userId": userId.flatMap { (value: UserId) -> ResultMap in value.resultMap }, "settings": settings.flatMap { (value: Setting) -> ResultMap in value.resultMap }, "location": location.flatMap { (value: Location) -> ResultMap in value.resultMap }, "topicId": topicId.flatMap { (value: TopicId) -> ResultMap in value.resultMap }, "code": code, "radius": radius, "time": time.flatMap { (value: Time) -> ResultMap in value.resultMap }, "createdDate": createdDate, "serverTime": serverTime, "classTime": classTime, "attendance": attendance.flatMap { (value: Attendance) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["_id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "_id")
        }
      }

      public var userId: UserId? {
        get {
          return (resultMap["userId"] as? ResultMap).flatMap { UserId(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "userId")
        }
      }

      public var settings: Setting? {
        get {
          return (resultMap["settings"] as? ResultMap).flatMap { Setting(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "settings")
        }
      }

      public var location: Location? {
        get {
          return (resultMap["location"] as? ResultMap).flatMap { Location(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "location")
        }
      }

      public var topicId: TopicId? {
        get {
          return (resultMap["topicId"] as? ResultMap).flatMap { TopicId(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "topicId")
        }
      }

      public var code: String? {
        get {
          return resultMap["code"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "code")
        }
      }

      public var radius: Double? {
        get {
          return resultMap["radius"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "radius")
        }
      }

      public var time: Time? {
        get {
          return (resultMap["time"] as? ResultMap).flatMap { Time(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "time")
        }
      }

      public var createdDate: Double? {
        get {
          return resultMap["createdDate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "createdDate")
        }
      }

      public var serverTime: Double? {
        get {
          return resultMap["serverTime"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "serverTime")
        }
      }

      public var classTime: Double? {
        get {
          return resultMap["classTime"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "classTime")
        }
      }

      public var attendance: Attendance? {
        get {
          return (resultMap["attendance"] as? ResultMap).flatMap { Attendance(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "attendance")
        }
      }

      public struct UserId: GraphQLSelectionSet {
        public static let possibleTypes = ["PublicUserInfo"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("nickname", type: .scalar(String.self)),
          GraphQLField("lastname", type: .scalar(String.self)),
          GraphQLField("created_at", type: .scalar(Double.self)),
          GraphQLField("phone", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, nickname: String? = nil, lastname: String? = nil, createdAt: Double? = nil, phone: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "PublicUserInfo", "_id": id, "nickname": nickname, "lastname": lastname, "created_at": createdAt, "phone": phone])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var nickname: String? {
          get {
            return resultMap["nickname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "nickname")
          }
        }

        public var lastname: String? {
          get {
            return resultMap["lastname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastname")
          }
        }

        public var createdAt: Double? {
          get {
            return resultMap["created_at"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "created_at")
          }
        }

        public var phone: String? {
          get {
            return resultMap["phone"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "phone")
          }
        }
      }

      public struct Setting: GraphQLSelectionSet {
        public static let possibleTypes = ["AttendanceSettings"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("enforceGeoLocation", type: .scalar(Bool.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(enforceGeoLocation: Bool? = nil) {
          self.init(unsafeResultMap: ["__typename": "AttendanceSettings", "enforceGeoLocation": enforceGeoLocation])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var enforceGeoLocation: Bool? {
          get {
            return resultMap["enforceGeoLocation"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "enforceGeoLocation")
          }
        }
      }

      public struct Location: GraphQLSelectionSet {
        public static let possibleTypes = ["AttendanceLocation"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("latitude", type: .scalar(Double.self)),
          GraphQLField("longitude", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(latitude: Double? = nil, longitude: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "AttendanceLocation", "latitude": latitude, "longitude": longitude])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var latitude: Double? {
          get {
            return resultMap["latitude"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "latitude")
          }
        }

        public var longitude: Double? {
          get {
            return resultMap["longitude"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "longitude")
          }
        }
      }

      public struct TopicId: GraphQLSelectionSet {
        public static let possibleTypes = ["Topic"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("classId", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, name: String? = nil, userId: String? = nil, classId: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Topic", "_id": id, "name": name, "userId": userId, "classId": classId])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var classId: String? {
          get {
            return resultMap["classId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classId")
          }
        }
      }

      public struct Time: GraphQLSelectionSet {
        public static let possibleTypes = ["AttendanceTime"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("timeEnds", type: .scalar(Double.self)),
          GraphQLField("timeBegan", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(timeEnds: Double? = nil, timeBegan: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "AttendanceTime", "timeEnds": timeEnds, "timeBegan": timeBegan])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var timeEnds: Double? {
          get {
            return resultMap["timeEnds"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "timeEnds")
          }
        }

        public var timeBegan: Double? {
          get {
            return resultMap["timeBegan"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "timeBegan")
          }
        }
      }

      public struct Attendance: GraphQLSelectionSet {
        public static let possibleTypes = ["AttendanceEntry"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("attendanceId", type: .scalar(String.self)),
          GraphQLField("checkedIn", type: .scalar(Double.self)),
          GraphQLField("checkedOut", type: .scalar(Double.self)),
          GraphQLField("state", type: .scalar(String.self)),
          GraphQLField("type", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, userId: String? = nil, attendanceId: String? = nil, checkedIn: Double? = nil, checkedOut: Double? = nil, state: String? = nil, type: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "AttendanceEntry", "_id": id, "userId": userId, "attendanceId": attendanceId, "checkedIn": checkedIn, "checkedOut": checkedOut, "state": state, "type": type])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var attendanceId: String? {
          get {
            return resultMap["attendanceId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "attendanceId")
          }
        }

        public var checkedIn: Double? {
          get {
            return resultMap["checkedIn"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "checkedIn")
          }
        }

        public var checkedOut: Double? {
          get {
            return resultMap["checkedOut"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "checkedOut")
          }
        }

        public var state: String? {
          get {
            return resultMap["state"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "state")
          }
        }

        public var type: String? {
          get {
            return resultMap["type"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "type")
          }
        }
      }
    }
  }
}

public final class AttendancesEntryRecordQuery: GraphQLQuery {
  public let operationDefinition =
    "query attendancesEntryRecord($classId: String!, $attendanceId: String!) {\n  attendancesEntryRecord(classId: $classId, attendanceId: $attendanceId) {\n    __typename\n    userId {\n      __typename\n      _id\n      nickname\n      lastname\n      email\n      image\n    }\n    attendanceEntry {\n      __typename\n      _id\n      userId\n      attendanceId\n      checkedIn\n      checkedOut\n      state\n      type\n    }\n  }\n}"

  public var classId: String
  public var attendanceId: String

  public init(classId: String, attendanceId: String) {
    self.classId = classId
    self.attendanceId = attendanceId
  }

  public var variables: GraphQLMap? {
    return ["classId": classId, "attendanceId": attendanceId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("attendancesEntryRecord", arguments: ["classId": GraphQLVariable("classId"), "attendanceId": GraphQLVariable("attendanceId")], type: .list(.object(AttendancesEntryRecord.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(attendancesEntryRecord: [AttendancesEntryRecord?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "attendancesEntryRecord": attendancesEntryRecord.flatMap { (value: [AttendancesEntryRecord?]) -> [ResultMap?] in value.map { (value: AttendancesEntryRecord?) -> ResultMap? in value.flatMap { (value: AttendancesEntryRecord) -> ResultMap in value.resultMap } } }])
    }

    /// studentsAttendanceEntryRecord(userId:String!):[AttendanceEntry]
    public var attendancesEntryRecord: [AttendancesEntryRecord?]? {
      get {
        return (resultMap["attendancesEntryRecord"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [AttendancesEntryRecord?] in value.map { (value: ResultMap?) -> AttendancesEntryRecord? in value.flatMap { (value: ResultMap) -> AttendancesEntryRecord in AttendancesEntryRecord(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [AttendancesEntryRecord?]) -> [ResultMap?] in value.map { (value: AttendancesEntryRecord?) -> ResultMap? in value.flatMap { (value: AttendancesEntryRecord) -> ResultMap in value.resultMap } } }, forKey: "attendancesEntryRecord")
      }
    }

    public struct AttendancesEntryRecord: GraphQLSelectionSet {
      public static let possibleTypes = ["CLassUserListWithAttandenceEntry"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .object(UserId.selections)),
        GraphQLField("attendanceEntry", type: .object(AttendanceEntry.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: UserId? = nil, attendanceEntry: AttendanceEntry? = nil) {
        self.init(unsafeResultMap: ["__typename": "CLassUserListWithAttandenceEntry", "userId": userId.flatMap { (value: UserId) -> ResultMap in value.resultMap }, "attendanceEntry": attendanceEntry.flatMap { (value: AttendanceEntry) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: UserId? {
        get {
          return (resultMap["userId"] as? ResultMap).flatMap { UserId(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "userId")
        }
      }

      public var attendanceEntry: AttendanceEntry? {
        get {
          return (resultMap["attendanceEntry"] as? ResultMap).flatMap { AttendanceEntry(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "attendanceEntry")
        }
      }

      public struct UserId: GraphQLSelectionSet {
        public static let possibleTypes = ["PublicUserInfo"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("nickname", type: .scalar(String.self)),
          GraphQLField("lastname", type: .scalar(String.self)),
          GraphQLField("email", type: .scalar(String.self)),
          GraphQLField("image", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, nickname: String? = nil, lastname: String? = nil, email: String? = nil, image: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "PublicUserInfo", "_id": id, "nickname": nickname, "lastname": lastname, "email": email, "image": image])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var nickname: String? {
          get {
            return resultMap["nickname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "nickname")
          }
        }

        public var lastname: String? {
          get {
            return resultMap["lastname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastname")
          }
        }

        public var email: String? {
          get {
            return resultMap["email"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "email")
          }
        }

        public var image: String? {
          get {
            return resultMap["image"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "image")
          }
        }
      }

      public struct AttendanceEntry: GraphQLSelectionSet {
        public static let possibleTypes = ["AttendanceEntry"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("attendanceId", type: .scalar(String.self)),
          GraphQLField("checkedIn", type: .scalar(Double.self)),
          GraphQLField("checkedOut", type: .scalar(Double.self)),
          GraphQLField("state", type: .scalar(String.self)),
          GraphQLField("type", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, userId: String? = nil, attendanceId: String? = nil, checkedIn: Double? = nil, checkedOut: Double? = nil, state: String? = nil, type: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "AttendanceEntry", "_id": id, "userId": userId, "attendanceId": attendanceId, "checkedIn": checkedIn, "checkedOut": checkedOut, "state": state, "type": type])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var attendanceId: String? {
          get {
            return resultMap["attendanceId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "attendanceId")
          }
        }

        public var checkedIn: Double? {
          get {
            return resultMap["checkedIn"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "checkedIn")
          }
        }

        public var checkedOut: Double? {
          get {
            return resultMap["checkedOut"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "checkedOut")
          }
        }

        public var state: String? {
          get {
            return resultMap["state"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "state")
          }
        }

        public var type: String? {
          get {
            return resultMap["type"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "type")
          }
        }
      }
    }
  }
}

public final class DeleteAttendanceMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation deleteAttendance($userId: String!, $attId: String!, $classId: String!) {\n  deleteAttendance(userId: $userId, attendanceId: $attId, classId: $classId) {\n    __typename\n    error\n    message\n    status\n  }\n}"

  public var userId: String
  public var attId: String
  public var classId: String

  public init(userId: String, attId: String, classId: String) {
    self.userId = userId
    self.attId = attId
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "attId": attId, "classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteAttendance", arguments: ["userId": GraphQLVariable("userId"), "attendanceId": GraphQLVariable("attId"), "classId": GraphQLVariable("classId")], type: .object(DeleteAttendance.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(deleteAttendance: DeleteAttendance? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "deleteAttendance": deleteAttendance.flatMap { (value: DeleteAttendance) -> ResultMap in value.resultMap }])
    }

    public var deleteAttendance: DeleteAttendance? {
      get {
        return (resultMap["deleteAttendance"] as? ResultMap).flatMap { DeleteAttendance(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "deleteAttendance")
      }
    }

    public struct DeleteAttendance: GraphQLSelectionSet {
      public static let possibleTypes = ["AttendanceResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "AttendanceResponse", "error": error, "message": message, "status": status])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }
    }
  }
}

public final class AttendanceQuery: GraphQLQuery {
  public let operationDefinition =
    "query attendance($attId: String!, $classId: String!) {\n  attendance(attendanceId: $attId, classId: $classId) {\n    __typename\n    _id\n    userId\n    settings {\n      __typename\n      enforceGeoLocation\n    }\n    location {\n      __typename\n      latitude\n      longitude\n    }\n    topicId {\n      __typename\n      _id\n      name\n      userId\n      classId\n    }\n    code\n    radius\n    time {\n      __typename\n      timeEnds\n      timeBegan\n    }\n    createdDate\n    serverTime\n  }\n}"

  public var attId: String
  public var classId: String

  public init(attId: String, classId: String) {
    self.attId = attId
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["attId": attId, "classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("attendance", arguments: ["attendanceId": GraphQLVariable("attId"), "classId": GraphQLVariable("classId")], type: .object(Attendance.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(attendance: Attendance? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "attendance": attendance.flatMap { (value: Attendance) -> ResultMap in value.resultMap }])
    }

    public var attendance: Attendance? {
      get {
        return (resultMap["attendance"] as? ResultMap).flatMap { Attendance(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "attendance")
      }
    }

    public struct Attendance: GraphQLSelectionSet {
      public static let possibleTypes = ["Attendance"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("_id", type: .scalar(String.self)),
        GraphQLField("userId", type: .scalar(String.self)),
        GraphQLField("settings", type: .object(Setting.selections)),
        GraphQLField("location", type: .object(Location.selections)),
        GraphQLField("topicId", type: .object(TopicId.selections)),
        GraphQLField("code", type: .scalar(String.self)),
        GraphQLField("radius", type: .scalar(Int.self)),
        GraphQLField("time", type: .object(Time.selections)),
        GraphQLField("createdDate", type: .scalar(Double.self)),
        GraphQLField("serverTime", type: .scalar(Double.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, userId: String? = nil, settings: Setting? = nil, location: Location? = nil, topicId: TopicId? = nil, code: String? = nil, radius: Int? = nil, time: Time? = nil, createdDate: Double? = nil, serverTime: Double? = nil) {
        self.init(unsafeResultMap: ["__typename": "Attendance", "_id": id, "userId": userId, "settings": settings.flatMap { (value: Setting) -> ResultMap in value.resultMap }, "location": location.flatMap { (value: Location) -> ResultMap in value.resultMap }, "topicId": topicId.flatMap { (value: TopicId) -> ResultMap in value.resultMap }, "code": code, "radius": radius, "time": time.flatMap { (value: Time) -> ResultMap in value.resultMap }, "createdDate": createdDate, "serverTime": serverTime])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["_id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "_id")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var settings: Setting? {
        get {
          return (resultMap["settings"] as? ResultMap).flatMap { Setting(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "settings")
        }
      }

      public var location: Location? {
        get {
          return (resultMap["location"] as? ResultMap).flatMap { Location(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "location")
        }
      }

      public var topicId: TopicId? {
        get {
          return (resultMap["topicId"] as? ResultMap).flatMap { TopicId(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "topicId")
        }
      }

      public var code: String? {
        get {
          return resultMap["code"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "code")
        }
      }

      public var radius: Int? {
        get {
          return resultMap["radius"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "radius")
        }
      }

      public var time: Time? {
        get {
          return (resultMap["time"] as? ResultMap).flatMap { Time(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "time")
        }
      }

      public var createdDate: Double? {
        get {
          return resultMap["createdDate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "createdDate")
        }
      }

      public var serverTime: Double? {
        get {
          return resultMap["serverTime"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "serverTime")
        }
      }

      public struct Setting: GraphQLSelectionSet {
        public static let possibleTypes = ["AttendanceSettings"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("enforceGeoLocation", type: .scalar(Bool.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(enforceGeoLocation: Bool? = nil) {
          self.init(unsafeResultMap: ["__typename": "AttendanceSettings", "enforceGeoLocation": enforceGeoLocation])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var enforceGeoLocation: Bool? {
          get {
            return resultMap["enforceGeoLocation"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "enforceGeoLocation")
          }
        }
      }

      public struct Location: GraphQLSelectionSet {
        public static let possibleTypes = ["AttendanceLocation"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("latitude", type: .scalar(Double.self)),
          GraphQLField("longitude", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(latitude: Double? = nil, longitude: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "AttendanceLocation", "latitude": latitude, "longitude": longitude])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var latitude: Double? {
          get {
            return resultMap["latitude"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "latitude")
          }
        }

        public var longitude: Double? {
          get {
            return resultMap["longitude"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "longitude")
          }
        }
      }

      public struct TopicId: GraphQLSelectionSet {
        public static let possibleTypes = ["Topic"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("classId", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, name: String? = nil, userId: String? = nil, classId: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Topic", "_id": id, "name": name, "userId": userId, "classId": classId])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var classId: String? {
          get {
            return resultMap["classId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classId")
          }
        }
      }

      public struct Time: GraphQLSelectionSet {
        public static let possibleTypes = ["AttendanceTime"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("timeEnds", type: .scalar(Double.self)),
          GraphQLField("timeBegan", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(timeEnds: Double? = nil, timeBegan: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "AttendanceTime", "timeEnds": timeEnds, "timeBegan": timeBegan])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var timeEnds: Double? {
          get {
            return resultMap["timeEnds"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "timeEnds")
          }
        }

        public var timeBegan: Double? {
          get {
            return resultMap["timeBegan"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "timeBegan")
          }
        }
      }
    }
  }
}

public final class UpdateAttendanceMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation updateAttendance($userId: String!, $attId: String!, $input: InputAttendance!, $classId: String!) {\n  updateAttendance(userId: $userId, attendanceId: $attId, InputAttendance: $input, classId: $classId) {\n    __typename\n    error\n    message\n    status\n    result {\n      __typename\n      _id\n      userId\n      topicId {\n        __typename\n        _id\n        name\n        userId\n        classId\n      }\n      settings {\n        __typename\n        enforceGeoLocation\n      }\n      location {\n        __typename\n        latitude\n        longitude\n      }\n      code\n      radius\n      time {\n        __typename\n        timeEnds\n        timeBegan\n      }\n      createdDate\n      serverTime\n    }\n  }\n}"

  public var userId: String
  public var attId: String
  public var input: InputAttendance
  public var classId: String

  public init(userId: String, attId: String, input: InputAttendance, classId: String) {
    self.userId = userId
    self.attId = attId
    self.input = input
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "attId": attId, "input": input, "classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateAttendance", arguments: ["userId": GraphQLVariable("userId"), "attendanceId": GraphQLVariable("attId"), "InputAttendance": GraphQLVariable("input"), "classId": GraphQLVariable("classId")], type: .object(UpdateAttendance.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updateAttendance: UpdateAttendance? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "updateAttendance": updateAttendance.flatMap { (value: UpdateAttendance) -> ResultMap in value.resultMap }])
    }

    public var updateAttendance: UpdateAttendance? {
      get {
        return (resultMap["updateAttendance"] as? ResultMap).flatMap { UpdateAttendance(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "updateAttendance")
      }
    }

    public struct UpdateAttendance: GraphQLSelectionSet {
      public static let possibleTypes = ["AttendanceResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
        GraphQLField("result", type: .object(Result.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil, result: Result? = nil) {
        self.init(unsafeResultMap: ["__typename": "AttendanceResponse", "error": error, "message": message, "status": status, "result": result.flatMap { (value: Result) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var result: Result? {
        get {
          return (resultMap["result"] as? ResultMap).flatMap { Result(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "result")
        }
      }

      public struct Result: GraphQLSelectionSet {
        public static let possibleTypes = ["Attendance"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("topicId", type: .object(TopicId.selections)),
          GraphQLField("settings", type: .object(Setting.selections)),
          GraphQLField("location", type: .object(Location.selections)),
          GraphQLField("code", type: .scalar(String.self)),
          GraphQLField("radius", type: .scalar(Int.self)),
          GraphQLField("time", type: .object(Time.selections)),
          GraphQLField("createdDate", type: .scalar(Double.self)),
          GraphQLField("serverTime", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, userId: String? = nil, topicId: TopicId? = nil, settings: Setting? = nil, location: Location? = nil, code: String? = nil, radius: Int? = nil, time: Time? = nil, createdDate: Double? = nil, serverTime: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "Attendance", "_id": id, "userId": userId, "topicId": topicId.flatMap { (value: TopicId) -> ResultMap in value.resultMap }, "settings": settings.flatMap { (value: Setting) -> ResultMap in value.resultMap }, "location": location.flatMap { (value: Location) -> ResultMap in value.resultMap }, "code": code, "radius": radius, "time": time.flatMap { (value: Time) -> ResultMap in value.resultMap }, "createdDate": createdDate, "serverTime": serverTime])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var topicId: TopicId? {
          get {
            return (resultMap["topicId"] as? ResultMap).flatMap { TopicId(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "topicId")
          }
        }

        public var settings: Setting? {
          get {
            return (resultMap["settings"] as? ResultMap).flatMap { Setting(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "settings")
          }
        }

        public var location: Location? {
          get {
            return (resultMap["location"] as? ResultMap).flatMap { Location(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "location")
          }
        }

        public var code: String? {
          get {
            return resultMap["code"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "code")
          }
        }

        public var radius: Int? {
          get {
            return resultMap["radius"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "radius")
          }
        }

        public var time: Time? {
          get {
            return (resultMap["time"] as? ResultMap).flatMap { Time(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "time")
          }
        }

        public var createdDate: Double? {
          get {
            return resultMap["createdDate"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "createdDate")
          }
        }

        public var serverTime: Double? {
          get {
            return resultMap["serverTime"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "serverTime")
          }
        }

        public struct TopicId: GraphQLSelectionSet {
          public static let possibleTypes = ["Topic"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("_id", type: .scalar(String.self)),
            GraphQLField("name", type: .scalar(String.self)),
            GraphQLField("userId", type: .scalar(String.self)),
            GraphQLField("classId", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil, userId: String? = nil, classId: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Topic", "_id": id, "name": name, "userId": userId, "classId": classId])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var userId: String? {
            get {
              return resultMap["userId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "userId")
            }
          }

          public var classId: String? {
            get {
              return resultMap["classId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "classId")
            }
          }
        }

        public struct Setting: GraphQLSelectionSet {
          public static let possibleTypes = ["AttendanceSettings"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("enforceGeoLocation", type: .scalar(Bool.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(enforceGeoLocation: Bool? = nil) {
            self.init(unsafeResultMap: ["__typename": "AttendanceSettings", "enforceGeoLocation": enforceGeoLocation])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var enforceGeoLocation: Bool? {
            get {
              return resultMap["enforceGeoLocation"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "enforceGeoLocation")
            }
          }
        }

        public struct Location: GraphQLSelectionSet {
          public static let possibleTypes = ["AttendanceLocation"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("latitude", type: .scalar(Double.self)),
            GraphQLField("longitude", type: .scalar(Double.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(latitude: Double? = nil, longitude: Double? = nil) {
            self.init(unsafeResultMap: ["__typename": "AttendanceLocation", "latitude": latitude, "longitude": longitude])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var latitude: Double? {
            get {
              return resultMap["latitude"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "latitude")
            }
          }

          public var longitude: Double? {
            get {
              return resultMap["longitude"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "longitude")
            }
          }
        }

        public struct Time: GraphQLSelectionSet {
          public static let possibleTypes = ["AttendanceTime"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("timeEnds", type: .scalar(Double.self)),
            GraphQLField("timeBegan", type: .scalar(Double.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(timeEnds: Double? = nil, timeBegan: Double? = nil) {
            self.init(unsafeResultMap: ["__typename": "AttendanceTime", "timeEnds": timeEnds, "timeBegan": timeBegan])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var timeEnds: Double? {
            get {
              return resultMap["timeEnds"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "timeEnds")
            }
          }

          public var timeBegan: Double? {
            get {
              return resultMap["timeBegan"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "timeBegan")
            }
          }
        }
      }
    }
  }
}

public final class PromoteToTeacherMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation promoteToTeacher($userId: String!, $subjectUserId: String!, $classId: String!) {\n  promoteToTeacher(userId: $userId, subjectUserId: $subjectUserId, classId: $classId) {\n    __typename\n    error\n    message\n    status\n    result {\n      __typename\n      _id\n      classCode\n      classId\n      classOwnerId\n      userId {\n        __typename\n        _id\n        nickname\n        lastname\n        phone\n        email\n      }\n      roleId\n      classRole\n      className\n      state\n      joinedDate\n    }\n  }\n}"

  public var userId: String
  public var subjectUserId: String
  public var classId: String

  public init(userId: String, subjectUserId: String, classId: String) {
    self.userId = userId
    self.subjectUserId = subjectUserId
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "subjectUserId": subjectUserId, "classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("promoteToTeacher", arguments: ["userId": GraphQLVariable("userId"), "subjectUserId": GraphQLVariable("subjectUserId"), "classId": GraphQLVariable("classId")], type: .object(PromoteToTeacher.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(promoteToTeacher: PromoteToTeacher? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "promoteToTeacher": promoteToTeacher.flatMap { (value: PromoteToTeacher) -> ResultMap in value.resultMap }])
    }

    public var promoteToTeacher: PromoteToTeacher? {
      get {
        return (resultMap["promoteToTeacher"] as? ResultMap).flatMap { PromoteToTeacher(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "promoteToTeacher")
      }
    }

    public struct PromoteToTeacher: GraphQLSelectionSet {
      public static let possibleTypes = ["ClassUserListStatusResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
        GraphQLField("result", type: .object(Result.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil, result: Result? = nil) {
        self.init(unsafeResultMap: ["__typename": "ClassUserListStatusResponse", "error": error, "message": message, "status": status, "result": result.flatMap { (value: Result) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var result: Result? {
        get {
          return (resultMap["result"] as? ResultMap).flatMap { Result(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "result")
        }
      }

      public struct Result: GraphQLSelectionSet {
        public static let possibleTypes = ["PopulatedClassUserList"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("classCode", type: .scalar(String.self)),
          GraphQLField("classId", type: .scalar(String.self)),
          GraphQLField("classOwnerId", type: .scalar(String.self)),
          GraphQLField("userId", type: .object(UserId.selections)),
          GraphQLField("roleId", type: .scalar(String.self)),
          GraphQLField("classRole", type: .scalar(String.self)),
          GraphQLField("className", type: .scalar(String.self)),
          GraphQLField("state", type: .scalar(String.self)),
          GraphQLField("joinedDate", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, classCode: String? = nil, classId: String? = nil, classOwnerId: String? = nil, userId: UserId? = nil, roleId: String? = nil, classRole: String? = nil, className: String? = nil, state: String? = nil, joinedDate: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "PopulatedClassUserList", "_id": id, "classCode": classCode, "classId": classId, "classOwnerId": classOwnerId, "userId": userId.flatMap { (value: UserId) -> ResultMap in value.resultMap }, "roleId": roleId, "classRole": classRole, "className": className, "state": state, "joinedDate": joinedDate])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var classCode: String? {
          get {
            return resultMap["classCode"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classCode")
          }
        }

        public var classId: String? {
          get {
            return resultMap["classId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classId")
          }
        }

        public var classOwnerId: String? {
          get {
            return resultMap["classOwnerId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classOwnerId")
          }
        }

        public var userId: UserId? {
          get {
            return (resultMap["userId"] as? ResultMap).flatMap { UserId(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "userId")
          }
        }

        public var roleId: String? {
          get {
            return resultMap["roleId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "roleId")
          }
        }

        public var classRole: String? {
          get {
            return resultMap["classRole"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classRole")
          }
        }

        public var className: String? {
          get {
            return resultMap["className"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "className")
          }
        }

        public var state: String? {
          get {
            return resultMap["state"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "state")
          }
        }

        public var joinedDate: Double? {
          get {
            return resultMap["joinedDate"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "joinedDate")
          }
        }

        public struct UserId: GraphQLSelectionSet {
          public static let possibleTypes = ["PublicUserInfo"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("_id", type: .scalar(String.self)),
            GraphQLField("nickname", type: .scalar(String.self)),
            GraphQLField("lastname", type: .scalar(String.self)),
            GraphQLField("phone", type: .scalar(String.self)),
            GraphQLField("email", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, nickname: String? = nil, lastname: String? = nil, phone: String? = nil, email: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "PublicUserInfo", "_id": id, "nickname": nickname, "lastname": lastname, "phone": phone, "email": email])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var nickname: String? {
            get {
              return resultMap["nickname"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "nickname")
            }
          }

          public var lastname: String? {
            get {
              return resultMap["lastname"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "lastname")
            }
          }

          public var phone: String? {
            get {
              return resultMap["phone"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "phone")
            }
          }

          public var email: String? {
            get {
              return resultMap["email"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "email")
            }
          }
        }
      }
    }
  }
}

public final class DemoteFromTeacherMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation demoteFromTeacher($userId: String!, $subjectUserId: String!, $classId: String!) {\n  demoteFromTeacher(userId: $userId, subjectUserId: $subjectUserId, classId: $classId) {\n    __typename\n    error\n    message\n    status\n    result {\n      __typename\n      _id\n      classCode\n      classId\n      classOwnerId\n      userId {\n        __typename\n        _id\n        nickname\n        lastname\n        phone\n        email\n      }\n      roleId\n      classRole\n      className\n      state\n      joinedDate\n    }\n  }\n}"

  public var userId: String
  public var subjectUserId: String
  public var classId: String

  public init(userId: String, subjectUserId: String, classId: String) {
    self.userId = userId
    self.subjectUserId = subjectUserId
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "subjectUserId": subjectUserId, "classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("demoteFromTeacher", arguments: ["userId": GraphQLVariable("userId"), "subjectUserId": GraphQLVariable("subjectUserId"), "classId": GraphQLVariable("classId")], type: .object(DemoteFromTeacher.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(demoteFromTeacher: DemoteFromTeacher? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "demoteFromTeacher": demoteFromTeacher.flatMap { (value: DemoteFromTeacher) -> ResultMap in value.resultMap }])
    }

    public var demoteFromTeacher: DemoteFromTeacher? {
      get {
        return (resultMap["demoteFromTeacher"] as? ResultMap).flatMap { DemoteFromTeacher(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "demoteFromTeacher")
      }
    }

    public struct DemoteFromTeacher: GraphQLSelectionSet {
      public static let possibleTypes = ["ClassUserListStatusResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
        GraphQLField("result", type: .object(Result.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil, result: Result? = nil) {
        self.init(unsafeResultMap: ["__typename": "ClassUserListStatusResponse", "error": error, "message": message, "status": status, "result": result.flatMap { (value: Result) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var result: Result? {
        get {
          return (resultMap["result"] as? ResultMap).flatMap { Result(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "result")
        }
      }

      public struct Result: GraphQLSelectionSet {
        public static let possibleTypes = ["PopulatedClassUserList"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("classCode", type: .scalar(String.self)),
          GraphQLField("classId", type: .scalar(String.self)),
          GraphQLField("classOwnerId", type: .scalar(String.self)),
          GraphQLField("userId", type: .object(UserId.selections)),
          GraphQLField("roleId", type: .scalar(String.self)),
          GraphQLField("classRole", type: .scalar(String.self)),
          GraphQLField("className", type: .scalar(String.self)),
          GraphQLField("state", type: .scalar(String.self)),
          GraphQLField("joinedDate", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, classCode: String? = nil, classId: String? = nil, classOwnerId: String? = nil, userId: UserId? = nil, roleId: String? = nil, classRole: String? = nil, className: String? = nil, state: String? = nil, joinedDate: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "PopulatedClassUserList", "_id": id, "classCode": classCode, "classId": classId, "classOwnerId": classOwnerId, "userId": userId.flatMap { (value: UserId) -> ResultMap in value.resultMap }, "roleId": roleId, "classRole": classRole, "className": className, "state": state, "joinedDate": joinedDate])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var classCode: String? {
          get {
            return resultMap["classCode"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classCode")
          }
        }

        public var classId: String? {
          get {
            return resultMap["classId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classId")
          }
        }

        public var classOwnerId: String? {
          get {
            return resultMap["classOwnerId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classOwnerId")
          }
        }

        public var userId: UserId? {
          get {
            return (resultMap["userId"] as? ResultMap).flatMap { UserId(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "userId")
          }
        }

        public var roleId: String? {
          get {
            return resultMap["roleId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "roleId")
          }
        }

        public var classRole: String? {
          get {
            return resultMap["classRole"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classRole")
          }
        }

        public var className: String? {
          get {
            return resultMap["className"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "className")
          }
        }

        public var state: String? {
          get {
            return resultMap["state"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "state")
          }
        }

        public var joinedDate: Double? {
          get {
            return resultMap["joinedDate"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "joinedDate")
          }
        }

        public struct UserId: GraphQLSelectionSet {
          public static let possibleTypes = ["PublicUserInfo"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("_id", type: .scalar(String.self)),
            GraphQLField("nickname", type: .scalar(String.self)),
            GraphQLField("lastname", type: .scalar(String.self)),
            GraphQLField("phone", type: .scalar(String.self)),
            GraphQLField("email", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, nickname: String? = nil, lastname: String? = nil, phone: String? = nil, email: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "PublicUserInfo", "_id": id, "nickname": nickname, "lastname": lastname, "phone": phone, "email": email])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var nickname: String? {
            get {
              return resultMap["nickname"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "nickname")
            }
          }

          public var lastname: String? {
            get {
              return resultMap["lastname"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "lastname")
            }
          }

          public var phone: String? {
            get {
              return resultMap["phone"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "phone")
            }
          }

          public var email: String? {
            get {
              return resultMap["email"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "email")
            }
          }
        }
      }
    }
  }
}

public final class AssigTopicMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation assigTopic($userId: String!, $subjectUserId: String!, $classId: String!, $topics: [String]!) {\n  assignTopic(userId: $userId, subjectUserId: $subjectUserId, classId: $classId, topics: $topics) {\n    __typename\n    error\n    message\n    status\n  }\n}"

  public var userId: String
  public var subjectUserId: String
  public var classId: String
  public var topics: [String?]

  public init(userId: String, subjectUserId: String, classId: String, topics: [String?]) {
    self.userId = userId
    self.subjectUserId = subjectUserId
    self.classId = classId
    self.topics = topics
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "subjectUserId": subjectUserId, "classId": classId, "topics": topics]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("assignTopic", arguments: ["userId": GraphQLVariable("userId"), "subjectUserId": GraphQLVariable("subjectUserId"), "classId": GraphQLVariable("classId"), "topics": GraphQLVariable("topics")], type: .object(AssignTopic.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(assignTopic: AssignTopic? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "assignTopic": assignTopic.flatMap { (value: AssignTopic) -> ResultMap in value.resultMap }])
    }

    public var assignTopic: AssignTopic? {
      get {
        return (resultMap["assignTopic"] as? ResultMap).flatMap { AssignTopic(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "assignTopic")
      }
    }

    public struct AssignTopic: GraphQLSelectionSet {
      public static let possibleTypes = ["BasicStatusResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "BasicStatusResponse", "error": error, "message": message, "status": status])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }
    }
  }
}

public final class ClassUserListQuery: GraphQLQuery {
  public let operationDefinition =
    "query classUserList($userId: String!, $classId: String!) {\n  classUserList(userId: $userId, classId: $classId) {\n    __typename\n    _id\n    classCode\n    classOwnerId\n    classRole\n    userId\n    state\n    notificationCount\n    topic\n  }\n}"

  public var userId: String
  public var classId: String

  public init(userId: String, classId: String) {
    self.userId = userId
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("classUserList", arguments: ["userId": GraphQLVariable("userId"), "classId": GraphQLVariable("classId")], type: .object(ClassUserList.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(classUserList: ClassUserList? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "classUserList": classUserList.flatMap { (value: ClassUserList) -> ResultMap in value.resultMap }])
    }

    public var classUserList: ClassUserList? {
      get {
        return (resultMap["classUserList"] as? ResultMap).flatMap { ClassUserList(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "classUserList")
      }
    }

    public struct ClassUserList: GraphQLSelectionSet {
      public static let possibleTypes = ["ClassUserList"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("_id", type: .scalar(String.self)),
        GraphQLField("classCode", type: .scalar(String.self)),
        GraphQLField("classOwnerId", type: .scalar(String.self)),
        GraphQLField("classRole", type: .scalar(String.self)),
        GraphQLField("userId", type: .scalar(String.self)),
        GraphQLField("state", type: .scalar(String.self)),
        GraphQLField("notificationCount", type: .scalar(Int.self)),
        GraphQLField("topic", type: .list(.scalar(String.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, classCode: String? = nil, classOwnerId: String? = nil, classRole: String? = nil, userId: String? = nil, state: String? = nil, notificationCount: Int? = nil, topic: [String?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "ClassUserList", "_id": id, "classCode": classCode, "classOwnerId": classOwnerId, "classRole": classRole, "userId": userId, "state": state, "notificationCount": notificationCount, "topic": topic])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["_id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "_id")
        }
      }

      public var classCode: String? {
        get {
          return resultMap["classCode"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "classCode")
        }
      }

      public var classOwnerId: String? {
        get {
          return resultMap["classOwnerId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "classOwnerId")
        }
      }

      public var classRole: String? {
        get {
          return resultMap["classRole"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "classRole")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var state: String? {
        get {
          return resultMap["state"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "state")
        }
      }

      public var notificationCount: Int? {
        get {
          return resultMap["notificationCount"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "notificationCount")
        }
      }

      public var topic: [String?]? {
        get {
          return resultMap["topic"] as? [String?]
        }
        set {
          resultMap.updateValue(newValue, forKey: "topic")
        }
      }
    }
  }
}

public final class TopicsQuery: GraphQLQuery {
  public let operationDefinition =
    "query topics($userId: String!, $classId: String!) {\n  topics(userId: $userId, classId: $classId)\n}"

  public var userId: String
  public var classId: String

  public init(userId: String, classId: String) {
    self.userId = userId
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("topics", arguments: ["userId": GraphQLVariable("userId"), "classId": GraphQLVariable("classId")], type: .list(.scalar(String.self))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(topics: [String?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "topics": topics])
    }

    public var topics: [String?]? {
      get {
        return resultMap["topics"] as? [String?]
      }
      set {
        resultMap.updateValue(newValue, forKey: "topics")
      }
    }
  }
}

public final class AttendancesQuery: GraphQLQuery {
  public let operationDefinition =
    "query Attendances($userId: String!, $classId: String!) {\n  attendances(userId: $userId, classId: $classId) {\n    __typename\n    _id\n    userId\n    settings {\n      __typename\n      enforceGeoLocation\n    }\n    location {\n      __typename\n      latitude\n      longitude\n    }\n    topicId {\n      __typename\n      _id\n      name\n      userId\n      classId\n    }\n    code\n    radius\n    time {\n      __typename\n      timeEnds\n      timeBegan\n    }\n    createdDate\n    serverTime\n    classTime\n  }\n}"

  public var userId: String
  public var classId: String

  public init(userId: String, classId: String) {
    self.userId = userId
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("attendances", arguments: ["userId": GraphQLVariable("userId"), "classId": GraphQLVariable("classId")], type: .list(.object(Attendance.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(attendances: [Attendance?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "attendances": attendances.flatMap { (value: [Attendance?]) -> [ResultMap?] in value.map { (value: Attendance?) -> ResultMap? in value.flatMap { (value: Attendance) -> ResultMap in value.resultMap } } }])
    }

    /// Attendance
    public var attendances: [Attendance?]? {
      get {
        return (resultMap["attendances"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Attendance?] in value.map { (value: ResultMap?) -> Attendance? in value.flatMap { (value: ResultMap) -> Attendance in Attendance(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [Attendance?]) -> [ResultMap?] in value.map { (value: Attendance?) -> ResultMap? in value.flatMap { (value: Attendance) -> ResultMap in value.resultMap } } }, forKey: "attendances")
      }
    }

    public struct Attendance: GraphQLSelectionSet {
      public static let possibleTypes = ["Attendance"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("_id", type: .scalar(String.self)),
        GraphQLField("userId", type: .scalar(String.self)),
        GraphQLField("settings", type: .object(Setting.selections)),
        GraphQLField("location", type: .object(Location.selections)),
        GraphQLField("topicId", type: .object(TopicId.selections)),
        GraphQLField("code", type: .scalar(String.self)),
        GraphQLField("radius", type: .scalar(Int.self)),
        GraphQLField("time", type: .object(Time.selections)),
        GraphQLField("createdDate", type: .scalar(Double.self)),
        GraphQLField("serverTime", type: .scalar(Double.self)),
        GraphQLField("classTime", type: .scalar(Double.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, userId: String? = nil, settings: Setting? = nil, location: Location? = nil, topicId: TopicId? = nil, code: String? = nil, radius: Int? = nil, time: Time? = nil, createdDate: Double? = nil, serverTime: Double? = nil, classTime: Double? = nil) {
        self.init(unsafeResultMap: ["__typename": "Attendance", "_id": id, "userId": userId, "settings": settings.flatMap { (value: Setting) -> ResultMap in value.resultMap }, "location": location.flatMap { (value: Location) -> ResultMap in value.resultMap }, "topicId": topicId.flatMap { (value: TopicId) -> ResultMap in value.resultMap }, "code": code, "radius": radius, "time": time.flatMap { (value: Time) -> ResultMap in value.resultMap }, "createdDate": createdDate, "serverTime": serverTime, "classTime": classTime])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["_id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "_id")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var settings: Setting? {
        get {
          return (resultMap["settings"] as? ResultMap).flatMap { Setting(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "settings")
        }
      }

      public var location: Location? {
        get {
          return (resultMap["location"] as? ResultMap).flatMap { Location(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "location")
        }
      }

      public var topicId: TopicId? {
        get {
          return (resultMap["topicId"] as? ResultMap).flatMap { TopicId(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "topicId")
        }
      }

      public var code: String? {
        get {
          return resultMap["code"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "code")
        }
      }

      public var radius: Int? {
        get {
          return resultMap["radius"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "radius")
        }
      }

      public var time: Time? {
        get {
          return (resultMap["time"] as? ResultMap).flatMap { Time(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "time")
        }
      }

      public var createdDate: Double? {
        get {
          return resultMap["createdDate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "createdDate")
        }
      }

      public var serverTime: Double? {
        get {
          return resultMap["serverTime"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "serverTime")
        }
      }

      public var classTime: Double? {
        get {
          return resultMap["classTime"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "classTime")
        }
      }

      public struct Setting: GraphQLSelectionSet {
        public static let possibleTypes = ["AttendanceSettings"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("enforceGeoLocation", type: .scalar(Bool.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(enforceGeoLocation: Bool? = nil) {
          self.init(unsafeResultMap: ["__typename": "AttendanceSettings", "enforceGeoLocation": enforceGeoLocation])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var enforceGeoLocation: Bool? {
          get {
            return resultMap["enforceGeoLocation"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "enforceGeoLocation")
          }
        }
      }

      public struct Location: GraphQLSelectionSet {
        public static let possibleTypes = ["AttendanceLocation"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("latitude", type: .scalar(Double.self)),
          GraphQLField("longitude", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(latitude: Double? = nil, longitude: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "AttendanceLocation", "latitude": latitude, "longitude": longitude])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var latitude: Double? {
          get {
            return resultMap["latitude"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "latitude")
          }
        }

        public var longitude: Double? {
          get {
            return resultMap["longitude"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "longitude")
          }
        }
      }

      public struct TopicId: GraphQLSelectionSet {
        public static let possibleTypes = ["Topic"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("classId", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, name: String? = nil, userId: String? = nil, classId: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Topic", "_id": id, "name": name, "userId": userId, "classId": classId])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var classId: String? {
          get {
            return resultMap["classId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classId")
          }
        }
      }

      public struct Time: GraphQLSelectionSet {
        public static let possibleTypes = ["AttendanceTime"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("timeEnds", type: .scalar(Double.self)),
          GraphQLField("timeBegan", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(timeEnds: Double? = nil, timeBegan: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "AttendanceTime", "timeEnds": timeEnds, "timeBegan": timeBegan])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var timeEnds: Double? {
          get {
            return resultMap["timeEnds"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "timeEnds")
          }
        }

        public var timeBegan: Double? {
          get {
            return resultMap["timeBegan"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "timeBegan")
          }
        }
      }
    }
  }
}

public final class CreateTopicMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation createTopic($userId: String!, $classId: String!, $topics: [String]!) {\n  createTopic(userId: $userId, classId: $classId, topics: $topics) {\n    __typename\n    error\n    message\n    status\n    result\n  }\n}"

  public var userId: String
  public var classId: String
  public var topics: [String?]

  public init(userId: String, classId: String, topics: [String?]) {
    self.userId = userId
    self.classId = classId
    self.topics = topics
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "classId": classId, "topics": topics]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createTopic", arguments: ["userId": GraphQLVariable("userId"), "classId": GraphQLVariable("classId"), "topics": GraphQLVariable("topics")], type: .object(CreateTopic.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createTopic: CreateTopic? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createTopic": createTopic.flatMap { (value: CreateTopic) -> ResultMap in value.resultMap }])
    }

    public var createTopic: CreateTopic? {
      get {
        return (resultMap["createTopic"] as? ResultMap).flatMap { CreateTopic(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createTopic")
      }
    }

    public struct CreateTopic: GraphQLSelectionSet {
      public static let possibleTypes = ["TopicResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
        GraphQLField("result", type: .list(.scalar(String.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil, result: [String?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "TopicResponse", "error": error, "message": message, "status": status, "result": result])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var result: [String?]? {
        get {
          return resultMap["result"] as? [String?]
        }
        set {
          resultMap.updateValue(newValue, forKey: "result")
        }
      }
    }
  }
}

public final class DeleteTopicMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation deleteTopic($userId: String!, $classId: String!, $topics: [String]!) {\n  deleteTopic(userId: $userId, classId: $classId, topics: $topics) {\n    __typename\n    error\n    message\n    status\n  }\n}"

  public var userId: String
  public var classId: String
  public var topics: [String?]

  public init(userId: String, classId: String, topics: [String?]) {
    self.userId = userId
    self.classId = classId
    self.topics = topics
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "classId": classId, "topics": topics]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteTopic", arguments: ["userId": GraphQLVariable("userId"), "classId": GraphQLVariable("classId"), "topics": GraphQLVariable("topics")], type: .object(DeleteTopic.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(deleteTopic: DeleteTopic? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "deleteTopic": deleteTopic.flatMap { (value: DeleteTopic) -> ResultMap in value.resultMap }])
    }

    public var deleteTopic: DeleteTopic? {
      get {
        return (resultMap["deleteTopic"] as? ResultMap).flatMap { DeleteTopic(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "deleteTopic")
      }
    }

    public struct DeleteTopic: GraphQLSelectionSet {
      public static let possibleTypes = ["BasicStatusResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "BasicStatusResponse", "error": error, "message": message, "status": status])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }
    }
  }
}

public final class UpdateTopicMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation updateTopic($userId: String!, $classId: String!, $topics: [String]!) {\n  updateTopic(userId: $userId, classId: $classId, topics: $topics) {\n    __typename\n    error\n    message\n    status\n    result\n  }\n}"

  public var userId: String
  public var classId: String
  public var topics: [String?]

  public init(userId: String, classId: String, topics: [String?]) {
    self.userId = userId
    self.classId = classId
    self.topics = topics
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "classId": classId, "topics": topics]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateTopic", arguments: ["userId": GraphQLVariable("userId"), "classId": GraphQLVariable("classId"), "topics": GraphQLVariable("topics")], type: .object(UpdateTopic.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updateTopic: UpdateTopic? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "updateTopic": updateTopic.flatMap { (value: UpdateTopic) -> ResultMap in value.resultMap }])
    }

    public var updateTopic: UpdateTopic? {
      get {
        return (resultMap["updateTopic"] as? ResultMap).flatMap { UpdateTopic(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "updateTopic")
      }
    }

    public struct UpdateTopic: GraphQLSelectionSet {
      public static let possibleTypes = ["TopicResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
        GraphQLField("result", type: .list(.scalar(String.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil, result: [String?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "TopicResponse", "error": error, "message": message, "status": status, "result": result])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var result: [String?]? {
        get {
          return resultMap["result"] as? [String?]
        }
        set {
          resultMap.updateValue(newValue, forKey: "result")
        }
      }
    }
  }
}

public final class PostsByTopicQuery: GraphQLQuery {
  public let operationDefinition =
    "query postsByTopic($userId: String!, $classId: String!, $topic: String!, $skip: Int, $limit: Int) {\n  postsByTopic(userId: $userId, classId: $classId, topicId: $topic, skip: $skip, limit: $limit) {\n    __typename\n    _id\n    type\n    ownerId {\n      __typename\n      _id\n      image\n      nickname\n      lastname\n      email\n      phone\n      birthday\n      online_at\n    }\n    postId\n    content\n    approved\n    instructions\n    points\n    dueDate\n    link\n    file {\n      __typename\n      extension\n      url\n      name\n      size\n      thumbnail\n    }\n    postedDate\n    commentCount\n    topicId {\n      __typename\n      _id\n      name\n      userId\n      classId\n    }\n  }\n}"

  public var userId: String
  public var classId: String
  public var topic: String
  public var skip: Int?
  public var limit: Int?

  public init(userId: String, classId: String, topic: String, skip: Int? = nil, limit: Int? = nil) {
    self.userId = userId
    self.classId = classId
    self.topic = topic
    self.skip = skip
    self.limit = limit
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "classId": classId, "topic": topic, "skip": skip, "limit": limit]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("postsByTopic", arguments: ["userId": GraphQLVariable("userId"), "classId": GraphQLVariable("classId"), "topicId": GraphQLVariable("topic"), "skip": GraphQLVariable("skip"), "limit": GraphQLVariable("limit")], type: .list(.object(PostsByTopic.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(postsByTopic: [PostsByTopic?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "postsByTopic": postsByTopic.flatMap { (value: [PostsByTopic?]) -> [ResultMap?] in value.map { (value: PostsByTopic?) -> ResultMap? in value.flatMap { (value: PostsByTopic) -> ResultMap in value.resultMap } } }])
    }

    public var postsByTopic: [PostsByTopic?]? {
      get {
        return (resultMap["postsByTopic"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [PostsByTopic?] in value.map { (value: ResultMap?) -> PostsByTopic? in value.flatMap { (value: ResultMap) -> PostsByTopic in PostsByTopic(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [PostsByTopic?]) -> [ResultMap?] in value.map { (value: PostsByTopic?) -> ResultMap? in value.flatMap { (value: PostsByTopic) -> ResultMap in value.resultMap } } }, forKey: "postsByTopic")
      }
    }

    public struct PostsByTopic: GraphQLSelectionSet {
      public static let possibleTypes = ["UserPopulatedPost"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("_id", type: .scalar(String.self)),
        GraphQLField("type", type: .scalar(String.self)),
        GraphQLField("ownerId", type: .object(OwnerId.selections)),
        GraphQLField("postId", type: .scalar(String.self)),
        GraphQLField("content", type: .scalar(String.self)),
        GraphQLField("approved", type: .scalar(Bool.self)),
        GraphQLField("instructions", type: .scalar(String.self)),
        GraphQLField("points", type: .scalar(Int.self)),
        GraphQLField("dueDate", type: .scalar(Double.self)),
        GraphQLField("link", type: .list(.scalar(String.self))),
        GraphQLField("file", type: .list(.object(File.selections))),
        GraphQLField("postedDate", type: .scalar(Double.self)),
        GraphQLField("commentCount", type: .scalar(Int.self)),
        GraphQLField("topicId", type: .object(TopicId.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, type: String? = nil, ownerId: OwnerId? = nil, postId: String? = nil, content: String? = nil, approved: Bool? = nil, instructions: String? = nil, points: Int? = nil, dueDate: Double? = nil, link: [String?]? = nil, file: [File?]? = nil, postedDate: Double? = nil, commentCount: Int? = nil, topicId: TopicId? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserPopulatedPost", "_id": id, "type": type, "ownerId": ownerId.flatMap { (value: OwnerId) -> ResultMap in value.resultMap }, "postId": postId, "content": content, "approved": approved, "instructions": instructions, "points": points, "dueDate": dueDate, "link": link, "file": file.flatMap { (value: [File?]) -> [ResultMap?] in value.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } } }, "postedDate": postedDate, "commentCount": commentCount, "topicId": topicId.flatMap { (value: TopicId) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["_id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "_id")
        }
      }

      public var type: String? {
        get {
          return resultMap["type"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "type")
        }
      }

      public var ownerId: OwnerId? {
        get {
          return (resultMap["ownerId"] as? ResultMap).flatMap { OwnerId(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "ownerId")
        }
      }

      public var postId: String? {
        get {
          return resultMap["postId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "postId")
        }
      }

      public var content: String? {
        get {
          return resultMap["content"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "content")
        }
      }

      public var approved: Bool? {
        get {
          return resultMap["approved"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "approved")
        }
      }

      public var instructions: String? {
        get {
          return resultMap["instructions"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "instructions")
        }
      }

      public var points: Int? {
        get {
          return resultMap["points"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "points")
        }
      }

      public var dueDate: Double? {
        get {
          return resultMap["dueDate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "dueDate")
        }
      }

      public var link: [String?]? {
        get {
          return resultMap["link"] as? [String?]
        }
        set {
          resultMap.updateValue(newValue, forKey: "link")
        }
      }

      public var file: [File?]? {
        get {
          return (resultMap["file"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [File?] in value.map { (value: ResultMap?) -> File? in value.flatMap { (value: ResultMap) -> File in File(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [File?]) -> [ResultMap?] in value.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } } }, forKey: "file")
        }
      }

      public var postedDate: Double? {
        get {
          return resultMap["postedDate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "postedDate")
        }
      }

      public var commentCount: Int? {
        get {
          return resultMap["commentCount"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "commentCount")
        }
      }

      public var topicId: TopicId? {
        get {
          return (resultMap["topicId"] as? ResultMap).flatMap { TopicId(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "topicId")
        }
      }

      public struct OwnerId: GraphQLSelectionSet {
        public static let possibleTypes = ["PublicUserInfo"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("image", type: .scalar(String.self)),
          GraphQLField("nickname", type: .scalar(String.self)),
          GraphQLField("lastname", type: .scalar(String.self)),
          GraphQLField("email", type: .scalar(String.self)),
          GraphQLField("phone", type: .scalar(String.self)),
          GraphQLField("birthday", type: .scalar(Double.self)),
          GraphQLField("online_at", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, image: String? = nil, nickname: String? = nil, lastname: String? = nil, email: String? = nil, phone: String? = nil, birthday: Double? = nil, onlineAt: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "PublicUserInfo", "_id": id, "image": image, "nickname": nickname, "lastname": lastname, "email": email, "phone": phone, "birthday": birthday, "online_at": onlineAt])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var image: String? {
          get {
            return resultMap["image"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "image")
          }
        }

        public var nickname: String? {
          get {
            return resultMap["nickname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "nickname")
          }
        }

        public var lastname: String? {
          get {
            return resultMap["lastname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastname")
          }
        }

        public var email: String? {
          get {
            return resultMap["email"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "email")
          }
        }

        public var phone: String? {
          get {
            return resultMap["phone"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "phone")
          }
        }

        public var birthday: Double? {
          get {
            return resultMap["birthday"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "birthday")
          }
        }

        public var onlineAt: Double? {
          get {
            return resultMap["online_at"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "online_at")
          }
        }
      }

      public struct File: GraphQLSelectionSet {
        public static let possibleTypes = ["PostFile"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("extension", type: .scalar(String.self)),
          GraphQLField("url", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("size", type: .scalar(Double.self)),
          GraphQLField("thumbnail", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(`extension`: String? = nil, url: String? = nil, name: String? = nil, size: Double? = nil, thumbnail: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "PostFile", "extension": `extension`, "url": url, "name": name, "size": size, "thumbnail": thumbnail])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var `extension`: String? {
          get {
            return resultMap["extension"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "extension")
          }
        }

        public var url: String? {
          get {
            return resultMap["url"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "url")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var size: Double? {
          get {
            return resultMap["size"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "size")
          }
        }

        public var thumbnail: String? {
          get {
            return resultMap["thumbnail"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "thumbnail")
          }
        }
      }

      public struct TopicId: GraphQLSelectionSet {
        public static let possibleTypes = ["Topic"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("classId", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, name: String? = nil, userId: String? = nil, classId: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Topic", "_id": id, "name": name, "userId": userId, "classId": classId])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var classId: String? {
          get {
            return resultMap["classId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classId")
          }
        }
      }
    }
  }
}

public final class InsertStudentIdMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation insertStudentID($userId: String!, $studentIdCode: String!) {\n  insertStudentIdCode(userId: $userId, studentIdCode: $studentIdCode) {\n    __typename\n    error\n    message\n    status\n  }\n}"

  public var userId: String
  public var studentIdCode: String

  public init(userId: String, studentIdCode: String) {
    self.userId = userId
    self.studentIdCode = studentIdCode
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "studentIdCode": studentIdCode]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("insertStudentIdCode", arguments: ["userId": GraphQLVariable("userId"), "studentIdCode": GraphQLVariable("studentIdCode")], type: .object(InsertStudentIdCode.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(insertStudentIdCode: InsertStudentIdCode? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "insertStudentIdCode": insertStudentIdCode.flatMap { (value: InsertStudentIdCode) -> ResultMap in value.resultMap }])
    }

    public var insertStudentIdCode: InsertStudentIdCode? {
      get {
        return (resultMap["insertStudentIdCode"] as? ResultMap).flatMap { InsertStudentIdCode(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "insertStudentIdCode")
      }
    }

    public struct InsertStudentIdCode: GraphQLSelectionSet {
      public static let possibleTypes = ["BasicStatusResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "BasicStatusResponse", "error": error, "message": message, "status": status])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }
    }
  }
}

public final class PostsByClassUserIdQuery: GraphQLQuery {
  public let operationDefinition =
    "query postsByClassUserId($userId: String!, $classId: String!, $classUserId: String!, $skip: Int, $limit: Int) {\n  postsByClassUserId(userId: $userId, classId: $classId, classUserId: $classUserId, skip: $skip, limit: $limit) {\n    __typename\n    _id\n    type\n    ownerId {\n      __typename\n      _id\n      image\n      nickname\n      lastname\n      email\n      phone\n      birthday\n      online_at\n    }\n    postId\n    content\n    approved\n    instructions\n    points\n    dueDate\n    link\n    file {\n      __typename\n      extension\n      url\n      name\n      size\n      thumbnail\n    }\n    postedDate\n    commentCount\n    topicId {\n      __typename\n      _id\n      name\n      userId\n      classId\n    }\n  }\n}"

  public var userId: String
  public var classId: String
  public var classUserId: String
  public var skip: Int?
  public var limit: Int?

  public init(userId: String, classId: String, classUserId: String, skip: Int? = nil, limit: Int? = nil) {
    self.userId = userId
    self.classId = classId
    self.classUserId = classUserId
    self.skip = skip
    self.limit = limit
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "classId": classId, "classUserId": classUserId, "skip": skip, "limit": limit]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("postsByClassUserId", arguments: ["userId": GraphQLVariable("userId"), "classId": GraphQLVariable("classId"), "classUserId": GraphQLVariable("classUserId"), "skip": GraphQLVariable("skip"), "limit": GraphQLVariable("limit")], type: .list(.object(PostsByClassUserId.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(postsByClassUserId: [PostsByClassUserId?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "postsByClassUserId": postsByClassUserId.flatMap { (value: [PostsByClassUserId?]) -> [ResultMap?] in value.map { (value: PostsByClassUserId?) -> ResultMap? in value.flatMap { (value: PostsByClassUserId) -> ResultMap in value.resultMap } } }])
    }

    /// postMaterials(userId:String!,classId:String!):[UserPopulatedPost]
    public var postsByClassUserId: [PostsByClassUserId?]? {
      get {
        return (resultMap["postsByClassUserId"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [PostsByClassUserId?] in value.map { (value: ResultMap?) -> PostsByClassUserId? in value.flatMap { (value: ResultMap) -> PostsByClassUserId in PostsByClassUserId(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [PostsByClassUserId?]) -> [ResultMap?] in value.map { (value: PostsByClassUserId?) -> ResultMap? in value.flatMap { (value: PostsByClassUserId) -> ResultMap in value.resultMap } } }, forKey: "postsByClassUserId")
      }
    }

    public struct PostsByClassUserId: GraphQLSelectionSet {
      public static let possibleTypes = ["UserPopulatedPost"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("_id", type: .scalar(String.self)),
        GraphQLField("type", type: .scalar(String.self)),
        GraphQLField("ownerId", type: .object(OwnerId.selections)),
        GraphQLField("postId", type: .scalar(String.self)),
        GraphQLField("content", type: .scalar(String.self)),
        GraphQLField("approved", type: .scalar(Bool.self)),
        GraphQLField("instructions", type: .scalar(String.self)),
        GraphQLField("points", type: .scalar(Int.self)),
        GraphQLField("dueDate", type: .scalar(Double.self)),
        GraphQLField("link", type: .list(.scalar(String.self))),
        GraphQLField("file", type: .list(.object(File.selections))),
        GraphQLField("postedDate", type: .scalar(Double.self)),
        GraphQLField("commentCount", type: .scalar(Int.self)),
        GraphQLField("topicId", type: .object(TopicId.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, type: String? = nil, ownerId: OwnerId? = nil, postId: String? = nil, content: String? = nil, approved: Bool? = nil, instructions: String? = nil, points: Int? = nil, dueDate: Double? = nil, link: [String?]? = nil, file: [File?]? = nil, postedDate: Double? = nil, commentCount: Int? = nil, topicId: TopicId? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserPopulatedPost", "_id": id, "type": type, "ownerId": ownerId.flatMap { (value: OwnerId) -> ResultMap in value.resultMap }, "postId": postId, "content": content, "approved": approved, "instructions": instructions, "points": points, "dueDate": dueDate, "link": link, "file": file.flatMap { (value: [File?]) -> [ResultMap?] in value.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } } }, "postedDate": postedDate, "commentCount": commentCount, "topicId": topicId.flatMap { (value: TopicId) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["_id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "_id")
        }
      }

      public var type: String? {
        get {
          return resultMap["type"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "type")
        }
      }

      public var ownerId: OwnerId? {
        get {
          return (resultMap["ownerId"] as? ResultMap).flatMap { OwnerId(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "ownerId")
        }
      }

      public var postId: String? {
        get {
          return resultMap["postId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "postId")
        }
      }

      public var content: String? {
        get {
          return resultMap["content"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "content")
        }
      }

      public var approved: Bool? {
        get {
          return resultMap["approved"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "approved")
        }
      }

      public var instructions: String? {
        get {
          return resultMap["instructions"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "instructions")
        }
      }

      public var points: Int? {
        get {
          return resultMap["points"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "points")
        }
      }

      public var dueDate: Double? {
        get {
          return resultMap["dueDate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "dueDate")
        }
      }

      public var link: [String?]? {
        get {
          return resultMap["link"] as? [String?]
        }
        set {
          resultMap.updateValue(newValue, forKey: "link")
        }
      }

      public var file: [File?]? {
        get {
          return (resultMap["file"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [File?] in value.map { (value: ResultMap?) -> File? in value.flatMap { (value: ResultMap) -> File in File(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [File?]) -> [ResultMap?] in value.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } } }, forKey: "file")
        }
      }

      public var postedDate: Double? {
        get {
          return resultMap["postedDate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "postedDate")
        }
      }

      public var commentCount: Int? {
        get {
          return resultMap["commentCount"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "commentCount")
        }
      }

      public var topicId: TopicId? {
        get {
          return (resultMap["topicId"] as? ResultMap).flatMap { TopicId(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "topicId")
        }
      }

      public struct OwnerId: GraphQLSelectionSet {
        public static let possibleTypes = ["PublicUserInfo"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("image", type: .scalar(String.self)),
          GraphQLField("nickname", type: .scalar(String.self)),
          GraphQLField("lastname", type: .scalar(String.self)),
          GraphQLField("email", type: .scalar(String.self)),
          GraphQLField("phone", type: .scalar(String.self)),
          GraphQLField("birthday", type: .scalar(Double.self)),
          GraphQLField("online_at", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, image: String? = nil, nickname: String? = nil, lastname: String? = nil, email: String? = nil, phone: String? = nil, birthday: Double? = nil, onlineAt: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "PublicUserInfo", "_id": id, "image": image, "nickname": nickname, "lastname": lastname, "email": email, "phone": phone, "birthday": birthday, "online_at": onlineAt])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var image: String? {
          get {
            return resultMap["image"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "image")
          }
        }

        public var nickname: String? {
          get {
            return resultMap["nickname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "nickname")
          }
        }

        public var lastname: String? {
          get {
            return resultMap["lastname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastname")
          }
        }

        public var email: String? {
          get {
            return resultMap["email"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "email")
          }
        }

        public var phone: String? {
          get {
            return resultMap["phone"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "phone")
          }
        }

        public var birthday: Double? {
          get {
            return resultMap["birthday"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "birthday")
          }
        }

        public var onlineAt: Double? {
          get {
            return resultMap["online_at"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "online_at")
          }
        }
      }

      public struct File: GraphQLSelectionSet {
        public static let possibleTypes = ["PostFile"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("extension", type: .scalar(String.self)),
          GraphQLField("url", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("size", type: .scalar(Double.self)),
          GraphQLField("thumbnail", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(`extension`: String? = nil, url: String? = nil, name: String? = nil, size: Double? = nil, thumbnail: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "PostFile", "extension": `extension`, "url": url, "name": name, "size": size, "thumbnail": thumbnail])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var `extension`: String? {
          get {
            return resultMap["extension"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "extension")
          }
        }

        public var url: String? {
          get {
            return resultMap["url"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "url")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var size: Double? {
          get {
            return resultMap["size"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "size")
          }
        }

        public var thumbnail: String? {
          get {
            return resultMap["thumbnail"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "thumbnail")
          }
        }
      }

      public struct TopicId: GraphQLSelectionSet {
        public static let possibleTypes = ["Topic"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("classId", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, name: String? = nil, userId: String? = nil, classId: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Topic", "_id": id, "name": name, "userId": userId, "classId": classId])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var classId: String? {
          get {
            return resultMap["classId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classId")
          }
        }
      }
    }
  }
}

public final class NotificationQuery: GraphQLQuery {
  public let operationDefinition =
    "query notification($userId: String!, $skip: Int!, $limit: Int!) {\n  notification(userId: $userId, skip: $skip, limit: $limit) {\n    __typename\n    _id\n    type\n    content\n    ownerId {\n      __typename\n      _id\n      region\n      image\n      nickname\n      lastname\n      phone\n    }\n    classId\n    postId\n    rootPostId\n    show\n    seen\n    createdDate\n  }\n}"

  public var userId: String
  public var skip: Int
  public var limit: Int

  public init(userId: String, skip: Int, limit: Int) {
    self.userId = userId
    self.skip = skip
    self.limit = limit
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "skip": skip, "limit": limit]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("notification", arguments: ["userId": GraphQLVariable("userId"), "skip": GraphQLVariable("skip"), "limit": GraphQLVariable("limit")], type: .list(.object(Notification.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(notification: [Notification?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "notification": notification.flatMap { (value: [Notification?]) -> [ResultMap?] in value.map { (value: Notification?) -> ResultMap? in value.flatMap { (value: Notification) -> ResultMap in value.resultMap } } }])
    }

    public var notification: [Notification?]? {
      get {
        return (resultMap["notification"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Notification?] in value.map { (value: ResultMap?) -> Notification? in value.flatMap { (value: ResultMap) -> Notification in Notification(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [Notification?]) -> [ResultMap?] in value.map { (value: Notification?) -> ResultMap? in value.flatMap { (value: Notification) -> ResultMap in value.resultMap } } }, forKey: "notification")
      }
    }

    public struct Notification: GraphQLSelectionSet {
      public static let possibleTypes = ["PopulatedNotification"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("_id", type: .scalar(String.self)),
        GraphQLField("type", type: .scalar(String.self)),
        GraphQLField("content", type: .scalar(String.self)),
        GraphQLField("ownerId", type: .object(OwnerId.selections)),
        GraphQLField("classId", type: .scalar(String.self)),
        GraphQLField("postId", type: .scalar(String.self)),
        GraphQLField("rootPostId", type: .scalar(String.self)),
        GraphQLField("show", type: .list(.scalar(String.self))),
        GraphQLField("seen", type: .list(.scalar(String.self))),
        GraphQLField("createdDate", type: .scalar(Double.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, type: String? = nil, content: String? = nil, ownerId: OwnerId? = nil, classId: String? = nil, postId: String? = nil, rootPostId: String? = nil, show: [String?]? = nil, seen: [String?]? = nil, createdDate: Double? = nil) {
        self.init(unsafeResultMap: ["__typename": "PopulatedNotification", "_id": id, "type": type, "content": content, "ownerId": ownerId.flatMap { (value: OwnerId) -> ResultMap in value.resultMap }, "classId": classId, "postId": postId, "rootPostId": rootPostId, "show": show, "seen": seen, "createdDate": createdDate])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["_id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "_id")
        }
      }

      public var type: String? {
        get {
          return resultMap["type"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "type")
        }
      }

      public var content: String? {
        get {
          return resultMap["content"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "content")
        }
      }

      public var ownerId: OwnerId? {
        get {
          return (resultMap["ownerId"] as? ResultMap).flatMap { OwnerId(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "ownerId")
        }
      }

      public var classId: String? {
        get {
          return resultMap["classId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "classId")
        }
      }

      public var postId: String? {
        get {
          return resultMap["postId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "postId")
        }
      }

      public var rootPostId: String? {
        get {
          return resultMap["rootPostId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "rootPostId")
        }
      }

      public var show: [String?]? {
        get {
          return resultMap["show"] as? [String?]
        }
        set {
          resultMap.updateValue(newValue, forKey: "show")
        }
      }

      public var seen: [String?]? {
        get {
          return resultMap["seen"] as? [String?]
        }
        set {
          resultMap.updateValue(newValue, forKey: "seen")
        }
      }

      public var createdDate: Double? {
        get {
          return resultMap["createdDate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "createdDate")
        }
      }

      public struct OwnerId: GraphQLSelectionSet {
        public static let possibleTypes = ["PublicUserInfo"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("region", type: .scalar(String.self)),
          GraphQLField("image", type: .scalar(String.self)),
          GraphQLField("nickname", type: .scalar(String.self)),
          GraphQLField("lastname", type: .scalar(String.self)),
          GraphQLField("phone", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, region: String? = nil, image: String? = nil, nickname: String? = nil, lastname: String? = nil, phone: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "PublicUserInfo", "_id": id, "region": region, "image": image, "nickname": nickname, "lastname": lastname, "phone": phone])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var region: String? {
          get {
            return resultMap["region"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "region")
          }
        }

        public var image: String? {
          get {
            return resultMap["image"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "image")
          }
        }

        public var nickname: String? {
          get {
            return resultMap["nickname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "nickname")
          }
        }

        public var lastname: String? {
          get {
            return resultMap["lastname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastname")
          }
        }

        public var phone: String? {
          get {
            return resultMap["phone"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "phone")
          }
        }
      }
    }
  }
}

public final class DeleteNotificationMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation deleteNotification($userId: String!, $notificationId: String!) {\n  deleteNotification(userId: $userId, notificationId: $notificationId) {\n    __typename\n    error\n    message\n  }\n}"

  public var userId: String
  public var notificationId: String

  public init(userId: String, notificationId: String) {
    self.userId = userId
    self.notificationId = notificationId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "notificationId": notificationId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteNotification", arguments: ["userId": GraphQLVariable("userId"), "notificationId": GraphQLVariable("notificationId")], type: .object(DeleteNotification.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(deleteNotification: DeleteNotification? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "deleteNotification": deleteNotification.flatMap { (value: DeleteNotification) -> ResultMap in value.resultMap }])
    }

    public var deleteNotification: DeleteNotification? {
      get {
        return (resultMap["deleteNotification"] as? ResultMap).flatMap { DeleteNotification(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "deleteNotification")
      }
    }

    public struct DeleteNotification: GraphQLSelectionSet {
      public static let possibleTypes = ["BasicResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "BasicResponse", "error": error, "message": message])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }
    }
  }
}

public final class EquilizeMclassNotificationMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation equilizeMclassNotification($userId: String!) {\n  equilizeMclassNotification(userId: $userId) {\n    __typename\n    error\n    message\n    status\n  }\n}"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("equilizeMclassNotification", arguments: ["userId": GraphQLVariable("userId")], type: .object(EquilizeMclassNotification.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(equilizeMclassNotification: EquilizeMclassNotification? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "equilizeMclassNotification": equilizeMclassNotification.flatMap { (value: EquilizeMclassNotification) -> ResultMap in value.resultMap }])
    }

    public var equilizeMclassNotification: EquilizeMclassNotification? {
      get {
        return (resultMap["equilizeMclassNotification"] as? ResultMap).flatMap { EquilizeMclassNotification(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "equilizeMclassNotification")
      }
    }

    public struct EquilizeMclassNotification: GraphQLSelectionSet {
      public static let possibleTypes = ["BasicStatusResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "BasicStatusResponse", "error": error, "message": message, "status": status])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }
    }
  }
}

public final class EquilizeMchatNotificationMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation equilizeMchatNotification($userId: String!) {\n  equilizeMchatNotification(userId: $userId) {\n    __typename\n    error\n    message\n    status\n  }\n}"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("equilizeMchatNotification", arguments: ["userId": GraphQLVariable("userId")], type: .object(EquilizeMchatNotification.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(equilizeMchatNotification: EquilizeMchatNotification? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "equilizeMchatNotification": equilizeMchatNotification.flatMap { (value: EquilizeMchatNotification) -> ResultMap in value.resultMap }])
    }

    public var equilizeMchatNotification: EquilizeMchatNotification? {
      get {
        return (resultMap["equilizeMchatNotification"] as? ResultMap).flatMap { EquilizeMchatNotification(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "equilizeMchatNotification")
      }
    }

    public struct EquilizeMchatNotification: GraphQLSelectionSet {
      public static let possibleTypes = ["BasicStatusResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "BasicStatusResponse", "error": error, "message": message, "status": status])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }
    }
  }
}

public final class InsertFeedbackMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation insertFeedback($userId: String!, $description: String!, $version: String!, $platform: String!) {\n  insertFeedback(userId: $userId, description: $description, version: $version, platform: $platform) {\n    __typename\n    error\n    message\n    status\n  }\n}"

  public var userId: String
  public var description: String
  public var version: String
  public var platform: String

  public init(userId: String, description: String, version: String, platform: String) {
    self.userId = userId
    self.description = description
    self.version = version
    self.platform = platform
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "description": description, "version": version, "platform": platform]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("insertFeedback", arguments: ["userId": GraphQLVariable("userId"), "description": GraphQLVariable("description"), "version": GraphQLVariable("version"), "platform": GraphQLVariable("platform")], type: .object(InsertFeedback.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(insertFeedback: InsertFeedback? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "insertFeedback": insertFeedback.flatMap { (value: InsertFeedback) -> ResultMap in value.resultMap }])
    }

    public var insertFeedback: InsertFeedback? {
      get {
        return (resultMap["insertFeedback"] as? ResultMap).flatMap { InsertFeedback(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "insertFeedback")
      }
    }

    public struct InsertFeedback: GraphQLSelectionSet {
      public static let possibleTypes = ["BasicStatusResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "BasicStatusResponse", "error": error, "message": message, "status": status])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }
    }
  }
}

public final class DecreaseMclassNoticationCountMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation decreaseMclassNoticationCount($userId: String!, $classId: String!, $count: Int!) {\n  decreaseMclassNoticationCount(userId: $userId, classId: $classId, count: $count) {\n    __typename\n    error\n    message\n    status\n  }\n}"

  public var userId: String
  public var classId: String
  public var count: Int

  public init(userId: String, classId: String, count: Int) {
    self.userId = userId
    self.classId = classId
    self.count = count
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "classId": classId, "count": count]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("decreaseMclassNoticationCount", arguments: ["userId": GraphQLVariable("userId"), "classId": GraphQLVariable("classId"), "count": GraphQLVariable("count")], type: .object(DecreaseMclassNoticationCount.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(decreaseMclassNoticationCount: DecreaseMclassNoticationCount? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "decreaseMclassNoticationCount": decreaseMclassNoticationCount.flatMap { (value: DecreaseMclassNoticationCount) -> ResultMap in value.resultMap }])
    }

    public var decreaseMclassNoticationCount: DecreaseMclassNoticationCount? {
      get {
        return (resultMap["decreaseMclassNoticationCount"] as? ResultMap).flatMap { DecreaseMclassNoticationCount(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "decreaseMclassNoticationCount")
      }
    }

    public struct DecreaseMclassNoticationCount: GraphQLSelectionSet {
      public static let possibleTypes = ["BasicStatusResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "BasicStatusResponse", "error": error, "message": message, "status": status])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }
    }
  }
}

public final class SumNotificationsQuery: GraphQLQuery {
  public let operationDefinition =
    "query SumNotifications($userId: String!) {\n  sumNotifications(userId: $userId) {\n    __typename\n    error\n    message\n    status\n    result {\n      __typename\n      mchat {\n        __typename\n        _id\n        type\n        badge\n        conversationId\n        userId\n      }\n      mclass {\n        __typename\n        _id\n        type\n        badge\n        classId\n        userId\n      }\n    }\n  }\n}"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("sumNotifications", arguments: ["userId": GraphQLVariable("userId")], type: .object(SumNotification.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(sumNotifications: SumNotification? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "sumNotifications": sumNotifications.flatMap { (value: SumNotification) -> ResultMap in value.resultMap }])
    }

    public var sumNotifications: SumNotification? {
      get {
        return (resultMap["sumNotifications"] as? ResultMap).flatMap { SumNotification(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "sumNotifications")
      }
    }

    public struct SumNotification: GraphQLSelectionSet {
      public static let possibleTypes = ["SumNotificationsResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
        GraphQLField("result", type: .object(Result.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil, result: Result? = nil) {
        self.init(unsafeResultMap: ["__typename": "SumNotificationsResponse", "error": error, "message": message, "status": status, "result": result.flatMap { (value: Result) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var result: Result? {
        get {
          return (resultMap["result"] as? ResultMap).flatMap { Result(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "result")
        }
      }

      public struct Result: GraphQLSelectionSet {
        public static let possibleTypes = ["SumNotifications"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("mchat", type: .list(.object(Mchat.selections))),
          GraphQLField("mclass", type: .list(.object(Mclass.selections))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(mchat: [Mchat?]? = nil, mclass: [Mclass?]? = nil) {
          self.init(unsafeResultMap: ["__typename": "SumNotifications", "mchat": mchat.flatMap { (value: [Mchat?]) -> [ResultMap?] in value.map { (value: Mchat?) -> ResultMap? in value.flatMap { (value: Mchat) -> ResultMap in value.resultMap } } }, "mclass": mclass.flatMap { (value: [Mclass?]) -> [ResultMap?] in value.map { (value: Mclass?) -> ResultMap? in value.flatMap { (value: Mclass) -> ResultMap in value.resultMap } } }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var mchat: [Mchat?]? {
          get {
            return (resultMap["mchat"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Mchat?] in value.map { (value: ResultMap?) -> Mchat? in value.flatMap { (value: ResultMap) -> Mchat in Mchat(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [Mchat?]) -> [ResultMap?] in value.map { (value: Mchat?) -> ResultMap? in value.flatMap { (value: Mchat) -> ResultMap in value.resultMap } } }, forKey: "mchat")
          }
        }

        public var mclass: [Mclass?]? {
          get {
            return (resultMap["mclass"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Mclass?] in value.map { (value: ResultMap?) -> Mclass? in value.flatMap { (value: ResultMap) -> Mclass in Mclass(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [Mclass?]) -> [ResultMap?] in value.map { (value: Mclass?) -> ResultMap? in value.flatMap { (value: Mclass) -> ResultMap in value.resultMap } } }, forKey: "mclass")
          }
        }

        public struct Mchat: GraphQLSelectionSet {
          public static let possibleTypes = ["MchatSumNotification"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("_id", type: .scalar(String.self)),
            GraphQLField("type", type: .scalar(String.self)),
            GraphQLField("badge", type: .scalar(Int.self)),
            GraphQLField("conversationId", type: .scalar(String.self)),
            GraphQLField("userId", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, type: String? = nil, badge: Int? = nil, conversationId: String? = nil, userId: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "MchatSumNotification", "_id": id, "type": type, "badge": badge, "conversationId": conversationId, "userId": userId])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var type: String? {
            get {
              return resultMap["type"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "type")
            }
          }

          public var badge: Int? {
            get {
              return resultMap["badge"] as? Int
            }
            set {
              resultMap.updateValue(newValue, forKey: "badge")
            }
          }

          public var conversationId: String? {
            get {
              return resultMap["conversationId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "conversationId")
            }
          }

          public var userId: String? {
            get {
              return resultMap["userId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "userId")
            }
          }
        }

        public struct Mclass: GraphQLSelectionSet {
          public static let possibleTypes = ["MclassSumNotification"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("_id", type: .scalar(String.self)),
            GraphQLField("type", type: .scalar(String.self)),
            GraphQLField("badge", type: .scalar(Int.self)),
            GraphQLField("classId", type: .scalar(String.self)),
            GraphQLField("userId", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, type: String? = nil, badge: Int? = nil, classId: String? = nil, userId: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "MclassSumNotification", "_id": id, "type": type, "badge": badge, "classId": classId, "userId": userId])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var type: String? {
            get {
              return resultMap["type"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "type")
            }
          }

          public var badge: Int? {
            get {
              return resultMap["badge"] as? Int
            }
            set {
              resultMap.updateValue(newValue, forKey: "badge")
            }
          }

          public var classId: String? {
            get {
              return resultMap["classId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "classId")
            }
          }

          public var userId: String? {
            get {
              return resultMap["userId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "userId")
            }
          }
        }
      }
    }
  }
}

public final class EquilizeMclassSumNotificationMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation equilizeMclassSumNotification($userId: String!, $classId: String!) {\n  equilizeMclassSumNotification(userId: $userId, classId: $classId) {\n    __typename\n    error\n    message\n    status\n  }\n}"

  public var userId: String
  public var classId: String

  public init(userId: String, classId: String) {
    self.userId = userId
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("equilizeMclassSumNotification", arguments: ["userId": GraphQLVariable("userId"), "classId": GraphQLVariable("classId")], type: .object(EquilizeMclassSumNotification.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(equilizeMclassSumNotification: EquilizeMclassSumNotification? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "equilizeMclassSumNotification": equilizeMclassSumNotification.flatMap { (value: EquilizeMclassSumNotification) -> ResultMap in value.resultMap }])
    }

    public var equilizeMclassSumNotification: EquilizeMclassSumNotification? {
      get {
        return (resultMap["equilizeMclassSumNotification"] as? ResultMap).flatMap { EquilizeMclassSumNotification(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "equilizeMclassSumNotification")
      }
    }

    public struct EquilizeMclassSumNotification: GraphQLSelectionSet {
      public static let possibleTypes = ["BasicStatusResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "BasicStatusResponse", "error": error, "message": message, "status": status])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }
    }
  }
}

public final class EquilizeMchatSumNotificationMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation equilizeMchatSumNotification($userId: String!, $conversationId: String!) {\n  equilizeMchatSumNotification(userId: $userId, conversationId: $conversationId) {\n    __typename\n    error\n    message\n    status\n  }\n}"

  public var userId: String
  public var conversationId: String

  public init(userId: String, conversationId: String) {
    self.userId = userId
    self.conversationId = conversationId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "conversationId": conversationId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("equilizeMchatSumNotification", arguments: ["userId": GraphQLVariable("userId"), "conversationId": GraphQLVariable("conversationId")], type: .object(EquilizeMchatSumNotification.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(equilizeMchatSumNotification: EquilizeMchatSumNotification? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "equilizeMchatSumNotification": equilizeMchatSumNotification.flatMap { (value: EquilizeMchatSumNotification) -> ResultMap in value.resultMap }])
    }

    public var equilizeMchatSumNotification: EquilizeMchatSumNotification? {
      get {
        return (resultMap["equilizeMchatSumNotification"] as? ResultMap).flatMap { EquilizeMchatSumNotification(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "equilizeMchatSumNotification")
      }
    }

    public struct EquilizeMchatSumNotification: GraphQLSelectionSet {
      public static let possibleTypes = ["BasicStatusResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "BasicStatusResponse", "error": error, "message": message, "status": status])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }
    }
  }
}

public final class DeleteAllNotificationMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation deleteAllNotification($userId: String!, $classId: String!) {\n  deleteAllNotification(userId: $userId, classId: $classId) {\n    __typename\n    error\n    message\n  }\n}"

  public var userId: String
  public var classId: String

  public init(userId: String, classId: String) {
    self.userId = userId
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteAllNotification", arguments: ["userId": GraphQLVariable("userId"), "classId": GraphQLVariable("classId")], type: .object(DeleteAllNotification.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(deleteAllNotification: DeleteAllNotification? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "deleteAllNotification": deleteAllNotification.flatMap { (value: DeleteAllNotification) -> ResultMap in value.resultMap }])
    }

    public var deleteAllNotification: DeleteAllNotification? {
      get {
        return (resultMap["deleteAllNotification"] as? ResultMap).flatMap { DeleteAllNotification(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "deleteAllNotification")
      }
    }

    public struct DeleteAllNotification: GraphQLSelectionSet {
      public static let possibleTypes = ["BasicResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "BasicResponse", "error": error, "message": message])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }
    }
  }
}

public final class ObjectTopicsQuery: GraphQLQuery {
  public let operationDefinition =
    "query objectTopics($userId: String!, $classId: String!) {\n  objectTopics(userId: $userId, classId: $classId) {\n    __typename\n    error\n    message\n    status\n    result {\n      __typename\n      _id\n      name\n      userId\n      classId\n    }\n  }\n}"

  public var userId: String
  public var classId: String

  public init(userId: String, classId: String) {
    self.userId = userId
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("objectTopics", arguments: ["userId": GraphQLVariable("userId"), "classId": GraphQLVariable("classId")], type: .object(ObjectTopic.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(objectTopics: ObjectTopic? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "objectTopics": objectTopics.flatMap { (value: ObjectTopic) -> ResultMap in value.resultMap }])
    }

    public var objectTopics: ObjectTopic? {
      get {
        return (resultMap["objectTopics"] as? ResultMap).flatMap { ObjectTopic(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "objectTopics")
      }
    }

    public struct ObjectTopic: GraphQLSelectionSet {
      public static let possibleTypes = ["TopicsResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
        GraphQLField("result", type: .list(.object(Result.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil, result: [Result?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "TopicsResponse", "error": error, "message": message, "status": status, "result": result.flatMap { (value: [Result?]) -> [ResultMap?] in value.map { (value: Result?) -> ResultMap? in value.flatMap { (value: Result) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var result: [Result?]? {
        get {
          return (resultMap["result"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Result?] in value.map { (value: ResultMap?) -> Result? in value.flatMap { (value: ResultMap) -> Result in Result(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Result?]) -> [ResultMap?] in value.map { (value: Result?) -> ResultMap? in value.flatMap { (value: Result) -> ResultMap in value.resultMap } } }, forKey: "result")
        }
      }

      public struct Result: GraphQLSelectionSet {
        public static let possibleTypes = ["Topic"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("classId", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, name: String? = nil, userId: String? = nil, classId: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Topic", "_id": id, "name": name, "userId": userId, "classId": classId])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var classId: String? {
          get {
            return resultMap["classId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classId")
          }
        }
      }
    }
  }
}

public final class InsertTopicMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation insertTopic($userId: String!, $classId: String!, $name: String!) {\n  insertTopic(userId: $userId, classId: $classId, name: $name) {\n    __typename\n    error\n    message\n    status\n    result {\n      __typename\n      _id\n      name\n      userId\n      classId\n    }\n  }\n}"

  public var userId: String
  public var classId: String
  public var name: String

  public init(userId: String, classId: String, name: String) {
    self.userId = userId
    self.classId = classId
    self.name = name
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "classId": classId, "name": name]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("insertTopic", arguments: ["userId": GraphQLVariable("userId"), "classId": GraphQLVariable("classId"), "name": GraphQLVariable("name")], type: .object(InsertTopic.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(insertTopic: InsertTopic? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "insertTopic": insertTopic.flatMap { (value: InsertTopic) -> ResultMap in value.resultMap }])
    }

    public var insertTopic: InsertTopic? {
      get {
        return (resultMap["insertTopic"] as? ResultMap).flatMap { InsertTopic(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "insertTopic")
      }
    }

    public struct InsertTopic: GraphQLSelectionSet {
      public static let possibleTypes = ["SingleTopicResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
        GraphQLField("result", type: .object(Result.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil, result: Result? = nil) {
        self.init(unsafeResultMap: ["__typename": "SingleTopicResponse", "error": error, "message": message, "status": status, "result": result.flatMap { (value: Result) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var result: Result? {
        get {
          return (resultMap["result"] as? ResultMap).flatMap { Result(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "result")
        }
      }

      public struct Result: GraphQLSelectionSet {
        public static let possibleTypes = ["Topic"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("classId", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, name: String? = nil, userId: String? = nil, classId: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Topic", "_id": id, "name": name, "userId": userId, "classId": classId])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var classId: String? {
          get {
            return resultMap["classId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classId")
          }
        }
      }
    }
  }
}

public final class UpdateObjectTopicMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation updateObjectTopic($userId: String!, $topicId: String!, $InputObjectTopic: InputObjectTopic!) {\n  updateObjectTopic(userId: $userId, topicId: $topicId, InputObjectTopic: $InputObjectTopic) {\n    __typename\n    error\n    message\n    status\n    result {\n      __typename\n      _id\n      name\n      userId\n      classId\n    }\n  }\n}"

  public var userId: String
  public var topicId: String
  public var InputObjectTopic: InputObjectTopic

  public init(userId: String, topicId: String, InputObjectTopic: InputObjectTopic) {
    self.userId = userId
    self.topicId = topicId
    self.InputObjectTopic = InputObjectTopic
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "topicId": topicId, "InputObjectTopic": InputObjectTopic]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateObjectTopic", arguments: ["userId": GraphQLVariable("userId"), "topicId": GraphQLVariable("topicId"), "InputObjectTopic": GraphQLVariable("InputObjectTopic")], type: .object(UpdateObjectTopic.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updateObjectTopic: UpdateObjectTopic? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "updateObjectTopic": updateObjectTopic.flatMap { (value: UpdateObjectTopic) -> ResultMap in value.resultMap }])
    }

    public var updateObjectTopic: UpdateObjectTopic? {
      get {
        return (resultMap["updateObjectTopic"] as? ResultMap).flatMap { UpdateObjectTopic(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "updateObjectTopic")
      }
    }

    public struct UpdateObjectTopic: GraphQLSelectionSet {
      public static let possibleTypes = ["SingleTopicResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
        GraphQLField("result", type: .object(Result.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil, result: Result? = nil) {
        self.init(unsafeResultMap: ["__typename": "SingleTopicResponse", "error": error, "message": message, "status": status, "result": result.flatMap { (value: Result) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var result: Result? {
        get {
          return (resultMap["result"] as? ResultMap).flatMap { Result(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "result")
        }
      }

      public struct Result: GraphQLSelectionSet {
        public static let possibleTypes = ["Topic"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("classId", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, name: String? = nil, userId: String? = nil, classId: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Topic", "_id": id, "name": name, "userId": userId, "classId": classId])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var classId: String? {
          get {
            return resultMap["classId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classId")
          }
        }
      }
    }
  }
}

public final class DeleteObjectTopicMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation deleteObjectTopic($userId: String!, $topicId: String!, $classId: String!) {\n  deleteObjectTopic(userId: $userId, topicId: $topicId, classId: $classId) {\n    __typename\n    error\n    message\n    status\n  }\n}"

  public var userId: String
  public var topicId: String
  public var classId: String

  public init(userId: String, topicId: String, classId: String) {
    self.userId = userId
    self.topicId = topicId
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "topicId": topicId, "classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteObjectTopic", arguments: ["userId": GraphQLVariable("userId"), "topicId": GraphQLVariable("topicId"), "classId": GraphQLVariable("classId")], type: .object(DeleteObjectTopic.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(deleteObjectTopic: DeleteObjectTopic? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "deleteObjectTopic": deleteObjectTopic.flatMap { (value: DeleteObjectTopic) -> ResultMap in value.resultMap }])
    }

    public var deleteObjectTopic: DeleteObjectTopic? {
      get {
        return (resultMap["deleteObjectTopic"] as? ResultMap).flatMap { DeleteObjectTopic(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "deleteObjectTopic")
      }
    }

    public struct DeleteObjectTopic: GraphQLSelectionSet {
      public static let possibleTypes = ["BasicStatusResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "BasicStatusResponse", "error": error, "message": message, "status": status])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }
    }
  }
}

public final class UsersAttandancesByTeacherIdQuery: GraphQLQuery {
  public let operationDefinition =
    "query usersAttandancesByTeacherId($userId: String!, $classId: String!, $teacherId: String!) {\n  usersAttandancesByTeacherId(userId: $userId, classId: $classId, teacherId: $teacherId) {\n    __typename\n    _id\n    userId {\n      __typename\n      _id\n      nickname\n      lastname\n      email\n    }\n    settings {\n      __typename\n      enforceGeoLocation\n    }\n    location {\n      __typename\n      latitude\n      longitude\n    }\n    topicId {\n      __typename\n      _id\n      name\n      userId\n      classId\n    }\n    code\n    radius\n    time {\n      __typename\n      timeEnds\n      timeBegan\n    }\n    createdDate\n    serverTime\n    attendance {\n      __typename\n      _id\n      userId\n      attendanceId\n      checkedIn\n      checkedOut\n      state\n      type\n    }\n  }\n}"

  public var userId: String
  public var classId: String
  public var teacherId: String

  public init(userId: String, classId: String, teacherId: String) {
    self.userId = userId
    self.classId = classId
    self.teacherId = teacherId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "classId": classId, "teacherId": teacherId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("usersAttandancesByTeacherId", arguments: ["userId": GraphQLVariable("userId"), "classId": GraphQLVariable("classId"), "teacherId": GraphQLVariable("teacherId")], type: .list(.object(UsersAttandancesByTeacherId.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(usersAttandancesByTeacherId: [UsersAttandancesByTeacherId?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "usersAttandancesByTeacherId": usersAttandancesByTeacherId.flatMap { (value: [UsersAttandancesByTeacherId?]) -> [ResultMap?] in value.map { (value: UsersAttandancesByTeacherId?) -> ResultMap? in value.flatMap { (value: UsersAttandancesByTeacherId) -> ResultMap in value.resultMap } } }])
    }

    public var usersAttandancesByTeacherId: [UsersAttandancesByTeacherId?]? {
      get {
        return (resultMap["usersAttandancesByTeacherId"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [UsersAttandancesByTeacherId?] in value.map { (value: ResultMap?) -> UsersAttandancesByTeacherId? in value.flatMap { (value: ResultMap) -> UsersAttandancesByTeacherId in UsersAttandancesByTeacherId(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [UsersAttandancesByTeacherId?]) -> [ResultMap?] in value.map { (value: UsersAttandancesByTeacherId?) -> ResultMap? in value.flatMap { (value: UsersAttandancesByTeacherId) -> ResultMap in value.resultMap } } }, forKey: "usersAttandancesByTeacherId")
      }
    }

    public struct UsersAttandancesByTeacherId: GraphQLSelectionSet {
      public static let possibleTypes = ["PopulatedAttendance"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("_id", type: .scalar(String.self)),
        GraphQLField("userId", type: .object(UserId.selections)),
        GraphQLField("settings", type: .object(Setting.selections)),
        GraphQLField("location", type: .object(Location.selections)),
        GraphQLField("topicId", type: .object(TopicId.selections)),
        GraphQLField("code", type: .scalar(String.self)),
        GraphQLField("radius", type: .scalar(Double.self)),
        GraphQLField("time", type: .object(Time.selections)),
        GraphQLField("createdDate", type: .scalar(Double.self)),
        GraphQLField("serverTime", type: .scalar(Double.self)),
        GraphQLField("attendance", type: .object(Attendance.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, userId: UserId? = nil, settings: Setting? = nil, location: Location? = nil, topicId: TopicId? = nil, code: String? = nil, radius: Double? = nil, time: Time? = nil, createdDate: Double? = nil, serverTime: Double? = nil, attendance: Attendance? = nil) {
        self.init(unsafeResultMap: ["__typename": "PopulatedAttendance", "_id": id, "userId": userId.flatMap { (value: UserId) -> ResultMap in value.resultMap }, "settings": settings.flatMap { (value: Setting) -> ResultMap in value.resultMap }, "location": location.flatMap { (value: Location) -> ResultMap in value.resultMap }, "topicId": topicId.flatMap { (value: TopicId) -> ResultMap in value.resultMap }, "code": code, "radius": radius, "time": time.flatMap { (value: Time) -> ResultMap in value.resultMap }, "createdDate": createdDate, "serverTime": serverTime, "attendance": attendance.flatMap { (value: Attendance) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["_id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "_id")
        }
      }

      public var userId: UserId? {
        get {
          return (resultMap["userId"] as? ResultMap).flatMap { UserId(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "userId")
        }
      }

      public var settings: Setting? {
        get {
          return (resultMap["settings"] as? ResultMap).flatMap { Setting(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "settings")
        }
      }

      public var location: Location? {
        get {
          return (resultMap["location"] as? ResultMap).flatMap { Location(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "location")
        }
      }

      public var topicId: TopicId? {
        get {
          return (resultMap["topicId"] as? ResultMap).flatMap { TopicId(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "topicId")
        }
      }

      public var code: String? {
        get {
          return resultMap["code"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "code")
        }
      }

      public var radius: Double? {
        get {
          return resultMap["radius"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "radius")
        }
      }

      public var time: Time? {
        get {
          return (resultMap["time"] as? ResultMap).flatMap { Time(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "time")
        }
      }

      public var createdDate: Double? {
        get {
          return resultMap["createdDate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "createdDate")
        }
      }

      public var serverTime: Double? {
        get {
          return resultMap["serverTime"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "serverTime")
        }
      }

      public var attendance: Attendance? {
        get {
          return (resultMap["attendance"] as? ResultMap).flatMap { Attendance(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "attendance")
        }
      }

      public struct UserId: GraphQLSelectionSet {
        public static let possibleTypes = ["PublicUserInfo"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("nickname", type: .scalar(String.self)),
          GraphQLField("lastname", type: .scalar(String.self)),
          GraphQLField("email", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, nickname: String? = nil, lastname: String? = nil, email: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "PublicUserInfo", "_id": id, "nickname": nickname, "lastname": lastname, "email": email])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var nickname: String? {
          get {
            return resultMap["nickname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "nickname")
          }
        }

        public var lastname: String? {
          get {
            return resultMap["lastname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastname")
          }
        }

        public var email: String? {
          get {
            return resultMap["email"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "email")
          }
        }
      }

      public struct Setting: GraphQLSelectionSet {
        public static let possibleTypes = ["AttendanceSettings"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("enforceGeoLocation", type: .scalar(Bool.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(enforceGeoLocation: Bool? = nil) {
          self.init(unsafeResultMap: ["__typename": "AttendanceSettings", "enforceGeoLocation": enforceGeoLocation])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var enforceGeoLocation: Bool? {
          get {
            return resultMap["enforceGeoLocation"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "enforceGeoLocation")
          }
        }
      }

      public struct Location: GraphQLSelectionSet {
        public static let possibleTypes = ["AttendanceLocation"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("latitude", type: .scalar(Double.self)),
          GraphQLField("longitude", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(latitude: Double? = nil, longitude: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "AttendanceLocation", "latitude": latitude, "longitude": longitude])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var latitude: Double? {
          get {
            return resultMap["latitude"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "latitude")
          }
        }

        public var longitude: Double? {
          get {
            return resultMap["longitude"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "longitude")
          }
        }
      }

      public struct TopicId: GraphQLSelectionSet {
        public static let possibleTypes = ["Topic"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("classId", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, name: String? = nil, userId: String? = nil, classId: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Topic", "_id": id, "name": name, "userId": userId, "classId": classId])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var classId: String? {
          get {
            return resultMap["classId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classId")
          }
        }
      }

      public struct Time: GraphQLSelectionSet {
        public static let possibleTypes = ["AttendanceTime"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("timeEnds", type: .scalar(Double.self)),
          GraphQLField("timeBegan", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(timeEnds: Double? = nil, timeBegan: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "AttendanceTime", "timeEnds": timeEnds, "timeBegan": timeBegan])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var timeEnds: Double? {
          get {
            return resultMap["timeEnds"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "timeEnds")
          }
        }

        public var timeBegan: Double? {
          get {
            return resultMap["timeBegan"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "timeBegan")
          }
        }
      }

      public struct Attendance: GraphQLSelectionSet {
        public static let possibleTypes = ["AttendanceEntry"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("attendanceId", type: .scalar(String.self)),
          GraphQLField("checkedIn", type: .scalar(Double.self)),
          GraphQLField("checkedOut", type: .scalar(Double.self)),
          GraphQLField("state", type: .scalar(String.self)),
          GraphQLField("type", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, userId: String? = nil, attendanceId: String? = nil, checkedIn: Double? = nil, checkedOut: Double? = nil, state: String? = nil, type: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "AttendanceEntry", "_id": id, "userId": userId, "attendanceId": attendanceId, "checkedIn": checkedIn, "checkedOut": checkedOut, "state": state, "type": type])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var attendanceId: String? {
          get {
            return resultMap["attendanceId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "attendanceId")
          }
        }

        public var checkedIn: Double? {
          get {
            return resultMap["checkedIn"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "checkedIn")
          }
        }

        public var checkedOut: Double? {
          get {
            return resultMap["checkedOut"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "checkedOut")
          }
        }

        public var state: String? {
          get {
            return resultMap["state"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "state")
          }
        }

        public var type: String? {
          get {
            return resultMap["type"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "type")
          }
        }
      }
    }
  }
}

public final class ClassTopicsQuery: GraphQLQuery {
  public let operationDefinition =
    "query classTopics($classId: String!) {\n  classTopics(classId: $classId) {\n    __typename\n    error\n    message\n    status\n    result {\n      __typename\n      _id\n      name\n      userId\n      classId\n    }\n  }\n}"

  public var classId: String

  public init(classId: String) {
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("classTopics", arguments: ["classId": GraphQLVariable("classId")], type: .object(ClassTopic.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(classTopics: ClassTopic? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "classTopics": classTopics.flatMap { (value: ClassTopic) -> ResultMap in value.resultMap }])
    }

    public var classTopics: ClassTopic? {
      get {
        return (resultMap["classTopics"] as? ResultMap).flatMap { ClassTopic(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "classTopics")
      }
    }

    public struct ClassTopic: GraphQLSelectionSet {
      public static let possibleTypes = ["TopicsResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
        GraphQLField("result", type: .list(.object(Result.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil, result: [Result?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "TopicsResponse", "error": error, "message": message, "status": status, "result": result.flatMap { (value: [Result?]) -> [ResultMap?] in value.map { (value: Result?) -> ResultMap? in value.flatMap { (value: Result) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var result: [Result?]? {
        get {
          return (resultMap["result"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Result?] in value.map { (value: ResultMap?) -> Result? in value.flatMap { (value: ResultMap) -> Result in Result(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Result?]) -> [ResultMap?] in value.map { (value: Result?) -> ResultMap? in value.flatMap { (value: Result) -> ResultMap in value.resultMap } } }, forKey: "result")
        }
      }

      public struct Result: GraphQLSelectionSet {
        public static let possibleTypes = ["Topic"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("classId", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, name: String? = nil, userId: String? = nil, classId: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Topic", "_id": id, "name": name, "userId": userId, "classId": classId])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var classId: String? {
          get {
            return resultMap["classId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classId")
          }
        }
      }
    }
  }
}

public final class UsersAttandancesByTopicIdQuery: GraphQLQuery {
  public let operationDefinition =
    "query usersAttandancesByTopicId($userId: String!, $classId: String!, $topicId: String!) {\n  usersAttandancesByTopicId(userId: $userId, classId: $classId, topicId: $topicId) {\n    __typename\n    _id\n    userId {\n      __typename\n      _id\n      nickname\n      lastname\n      email\n    }\n    settings {\n      __typename\n      enforceGeoLocation\n    }\n    location {\n      __typename\n      latitude\n      longitude\n    }\n    topicId {\n      __typename\n      _id\n      name\n      userId\n      classId\n    }\n    code\n    radius\n    time {\n      __typename\n      timeEnds\n      timeBegan\n    }\n    createdDate\n    serverTime\n    attendance {\n      __typename\n      _id\n      userId\n      attendanceId\n      checkedIn\n      checkedOut\n      state\n      type\n    }\n  }\n}"

  public var userId: String
  public var classId: String
  public var topicId: String

  public init(userId: String, classId: String, topicId: String) {
    self.userId = userId
    self.classId = classId
    self.topicId = topicId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "classId": classId, "topicId": topicId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("usersAttandancesByTopicId", arguments: ["userId": GraphQLVariable("userId"), "classId": GraphQLVariable("classId"), "topicId": GraphQLVariable("topicId")], type: .list(.object(UsersAttandancesByTopicId.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(usersAttandancesByTopicId: [UsersAttandancesByTopicId?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "usersAttandancesByTopicId": usersAttandancesByTopicId.flatMap { (value: [UsersAttandancesByTopicId?]) -> [ResultMap?] in value.map { (value: UsersAttandancesByTopicId?) -> ResultMap? in value.flatMap { (value: UsersAttandancesByTopicId) -> ResultMap in value.resultMap } } }])
    }

    public var usersAttandancesByTopicId: [UsersAttandancesByTopicId?]? {
      get {
        return (resultMap["usersAttandancesByTopicId"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [UsersAttandancesByTopicId?] in value.map { (value: ResultMap?) -> UsersAttandancesByTopicId? in value.flatMap { (value: ResultMap) -> UsersAttandancesByTopicId in UsersAttandancesByTopicId(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [UsersAttandancesByTopicId?]) -> [ResultMap?] in value.map { (value: UsersAttandancesByTopicId?) -> ResultMap? in value.flatMap { (value: UsersAttandancesByTopicId) -> ResultMap in value.resultMap } } }, forKey: "usersAttandancesByTopicId")
      }
    }

    public struct UsersAttandancesByTopicId: GraphQLSelectionSet {
      public static let possibleTypes = ["PopulatedAttendance"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("_id", type: .scalar(String.self)),
        GraphQLField("userId", type: .object(UserId.selections)),
        GraphQLField("settings", type: .object(Setting.selections)),
        GraphQLField("location", type: .object(Location.selections)),
        GraphQLField("topicId", type: .object(TopicId.selections)),
        GraphQLField("code", type: .scalar(String.self)),
        GraphQLField("radius", type: .scalar(Double.self)),
        GraphQLField("time", type: .object(Time.selections)),
        GraphQLField("createdDate", type: .scalar(Double.self)),
        GraphQLField("serverTime", type: .scalar(Double.self)),
        GraphQLField("attendance", type: .object(Attendance.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, userId: UserId? = nil, settings: Setting? = nil, location: Location? = nil, topicId: TopicId? = nil, code: String? = nil, radius: Double? = nil, time: Time? = nil, createdDate: Double? = nil, serverTime: Double? = nil, attendance: Attendance? = nil) {
        self.init(unsafeResultMap: ["__typename": "PopulatedAttendance", "_id": id, "userId": userId.flatMap { (value: UserId) -> ResultMap in value.resultMap }, "settings": settings.flatMap { (value: Setting) -> ResultMap in value.resultMap }, "location": location.flatMap { (value: Location) -> ResultMap in value.resultMap }, "topicId": topicId.flatMap { (value: TopicId) -> ResultMap in value.resultMap }, "code": code, "radius": radius, "time": time.flatMap { (value: Time) -> ResultMap in value.resultMap }, "createdDate": createdDate, "serverTime": serverTime, "attendance": attendance.flatMap { (value: Attendance) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["_id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "_id")
        }
      }

      public var userId: UserId? {
        get {
          return (resultMap["userId"] as? ResultMap).flatMap { UserId(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "userId")
        }
      }

      public var settings: Setting? {
        get {
          return (resultMap["settings"] as? ResultMap).flatMap { Setting(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "settings")
        }
      }

      public var location: Location? {
        get {
          return (resultMap["location"] as? ResultMap).flatMap { Location(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "location")
        }
      }

      public var topicId: TopicId? {
        get {
          return (resultMap["topicId"] as? ResultMap).flatMap { TopicId(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "topicId")
        }
      }

      public var code: String? {
        get {
          return resultMap["code"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "code")
        }
      }

      public var radius: Double? {
        get {
          return resultMap["radius"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "radius")
        }
      }

      public var time: Time? {
        get {
          return (resultMap["time"] as? ResultMap).flatMap { Time(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "time")
        }
      }

      public var createdDate: Double? {
        get {
          return resultMap["createdDate"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "createdDate")
        }
      }

      public var serverTime: Double? {
        get {
          return resultMap["serverTime"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "serverTime")
        }
      }

      public var attendance: Attendance? {
        get {
          return (resultMap["attendance"] as? ResultMap).flatMap { Attendance(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "attendance")
        }
      }

      public struct UserId: GraphQLSelectionSet {
        public static let possibleTypes = ["PublicUserInfo"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("nickname", type: .scalar(String.self)),
          GraphQLField("lastname", type: .scalar(String.self)),
          GraphQLField("email", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, nickname: String? = nil, lastname: String? = nil, email: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "PublicUserInfo", "_id": id, "nickname": nickname, "lastname": lastname, "email": email])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var nickname: String? {
          get {
            return resultMap["nickname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "nickname")
          }
        }

        public var lastname: String? {
          get {
            return resultMap["lastname"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastname")
          }
        }

        public var email: String? {
          get {
            return resultMap["email"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "email")
          }
        }
      }

      public struct Setting: GraphQLSelectionSet {
        public static let possibleTypes = ["AttendanceSettings"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("enforceGeoLocation", type: .scalar(Bool.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(enforceGeoLocation: Bool? = nil) {
          self.init(unsafeResultMap: ["__typename": "AttendanceSettings", "enforceGeoLocation": enforceGeoLocation])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var enforceGeoLocation: Bool? {
          get {
            return resultMap["enforceGeoLocation"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "enforceGeoLocation")
          }
        }
      }

      public struct Location: GraphQLSelectionSet {
        public static let possibleTypes = ["AttendanceLocation"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("latitude", type: .scalar(Double.self)),
          GraphQLField("longitude", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(latitude: Double? = nil, longitude: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "AttendanceLocation", "latitude": latitude, "longitude": longitude])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var latitude: Double? {
          get {
            return resultMap["latitude"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "latitude")
          }
        }

        public var longitude: Double? {
          get {
            return resultMap["longitude"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "longitude")
          }
        }
      }

      public struct TopicId: GraphQLSelectionSet {
        public static let possibleTypes = ["Topic"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("classId", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, name: String? = nil, userId: String? = nil, classId: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Topic", "_id": id, "name": name, "userId": userId, "classId": classId])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var classId: String? {
          get {
            return resultMap["classId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classId")
          }
        }
      }

      public struct Time: GraphQLSelectionSet {
        public static let possibleTypes = ["AttendanceTime"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("timeEnds", type: .scalar(Double.self)),
          GraphQLField("timeBegan", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(timeEnds: Double? = nil, timeBegan: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "AttendanceTime", "timeEnds": timeEnds, "timeBegan": timeBegan])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var timeEnds: Double? {
          get {
            return resultMap["timeEnds"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "timeEnds")
          }
        }

        public var timeBegan: Double? {
          get {
            return resultMap["timeBegan"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "timeBegan")
          }
        }
      }

      public struct Attendance: GraphQLSelectionSet {
        public static let possibleTypes = ["AttendanceEntry"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("attendanceId", type: .scalar(String.self)),
          GraphQLField("checkedIn", type: .scalar(Double.self)),
          GraphQLField("checkedOut", type: .scalar(Double.self)),
          GraphQLField("state", type: .scalar(String.self)),
          GraphQLField("type", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, userId: String? = nil, attendanceId: String? = nil, checkedIn: Double? = nil, checkedOut: Double? = nil, state: String? = nil, type: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "AttendanceEntry", "_id": id, "userId": userId, "attendanceId": attendanceId, "checkedIn": checkedIn, "checkedOut": checkedOut, "state": state, "type": type])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var attendanceId: String? {
          get {
            return resultMap["attendanceId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "attendanceId")
          }
        }

        public var checkedIn: Double? {
          get {
            return resultMap["checkedIn"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "checkedIn")
          }
        }

        public var checkedOut: Double? {
          get {
            return resultMap["checkedOut"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "checkedOut")
          }
        }

        public var state: String? {
          get {
            return resultMap["state"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "state")
          }
        }

        public var type: String? {
          get {
            return resultMap["type"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "type")
          }
        }
      }
    }
  }
}

public final class MarkNotificationMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation markNotification($userId: String!, $notifyId: String!) {\n  markNotification(userId: $userId, notificationId: $notifyId) {\n    __typename\n    error\n    message\n    result {\n      __typename\n      _id\n      type\n      content\n      ownerId {\n        __typename\n        _id\n        nickname\n        lastname\n      }\n      classId\n      postId\n      rootPostId\n      show\n      seen\n      createdDate\n    }\n    status\n  }\n}"

  public var userId: String
  public var notifyId: String

  public init(userId: String, notifyId: String) {
    self.userId = userId
    self.notifyId = notifyId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "notifyId": notifyId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("markNotification", arguments: ["userId": GraphQLVariable("userId"), "notificationId": GraphQLVariable("notifyId")], type: .object(MarkNotification.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(markNotification: MarkNotification? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "markNotification": markNotification.flatMap { (value: MarkNotification) -> ResultMap in value.resultMap }])
    }

    public var markNotification: MarkNotification? {
      get {
        return (resultMap["markNotification"] as? ResultMap).flatMap { MarkNotification(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "markNotification")
      }
    }

    public struct MarkNotification: GraphQLSelectionSet {
      public static let possibleTypes = ["NotificationResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("result", type: .object(Result.selections)),
        GraphQLField("status", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, result: Result? = nil, status: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "NotificationResponse", "error": error, "message": message, "result": result.flatMap { (value: Result) -> ResultMap in value.resultMap }, "status": status])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var result: Result? {
        get {
          return (resultMap["result"] as? ResultMap).flatMap { Result(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "result")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public struct Result: GraphQLSelectionSet {
        public static let possibleTypes = ["PopulatedNotification"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("type", type: .scalar(String.self)),
          GraphQLField("content", type: .scalar(String.self)),
          GraphQLField("ownerId", type: .object(OwnerId.selections)),
          GraphQLField("classId", type: .scalar(String.self)),
          GraphQLField("postId", type: .scalar(String.self)),
          GraphQLField("rootPostId", type: .scalar(String.self)),
          GraphQLField("show", type: .list(.scalar(String.self))),
          GraphQLField("seen", type: .list(.scalar(String.self))),
          GraphQLField("createdDate", type: .scalar(Double.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, type: String? = nil, content: String? = nil, ownerId: OwnerId? = nil, classId: String? = nil, postId: String? = nil, rootPostId: String? = nil, show: [String?]? = nil, seen: [String?]? = nil, createdDate: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "PopulatedNotification", "_id": id, "type": type, "content": content, "ownerId": ownerId.flatMap { (value: OwnerId) -> ResultMap in value.resultMap }, "classId": classId, "postId": postId, "rootPostId": rootPostId, "show": show, "seen": seen, "createdDate": createdDate])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var type: String? {
          get {
            return resultMap["type"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "type")
          }
        }

        public var content: String? {
          get {
            return resultMap["content"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "content")
          }
        }

        public var ownerId: OwnerId? {
          get {
            return (resultMap["ownerId"] as? ResultMap).flatMap { OwnerId(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "ownerId")
          }
        }

        public var classId: String? {
          get {
            return resultMap["classId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "classId")
          }
        }

        public var postId: String? {
          get {
            return resultMap["postId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "postId")
          }
        }

        public var rootPostId: String? {
          get {
            return resultMap["rootPostId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "rootPostId")
          }
        }

        public var show: [String?]? {
          get {
            return resultMap["show"] as? [String?]
          }
          set {
            resultMap.updateValue(newValue, forKey: "show")
          }
        }

        public var seen: [String?]? {
          get {
            return resultMap["seen"] as? [String?]
          }
          set {
            resultMap.updateValue(newValue, forKey: "seen")
          }
        }

        public var createdDate: Double? {
          get {
            return resultMap["createdDate"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "createdDate")
          }
        }

        public struct OwnerId: GraphQLSelectionSet {
          public static let possibleTypes = ["PublicUserInfo"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("_id", type: .scalar(String.self)),
            GraphQLField("nickname", type: .scalar(String.self)),
            GraphQLField("lastname", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, nickname: String? = nil, lastname: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "PublicUserInfo", "_id": id, "nickname": nickname, "lastname": lastname])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var nickname: String? {
            get {
              return resultMap["nickname"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "nickname")
            }
          }

          public var lastname: String? {
            get {
              return resultMap["lastname"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "lastname")
            }
          }
        }
      }
    }
  }
}

public final class UpdateAttendanceEntryStateMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation updateAttendanceEntryState($userId: String!, $attId: String!, $state: String!, $classId: String!) {\n  updateAttendanceEntryState(userId: $userId, attendanceId: $attId, state: $state, classId: $classId) {\n    __typename\n    error\n    message\n    status\n    result {\n      __typename\n      _id\n      userId\n      attendanceId\n      checkedIn\n      checkedOut\n      state\n      type\n    }\n  }\n}"

  public var userId: String
  public var attId: String
  public var state: String
  public var classId: String

  public init(userId: String, attId: String, state: String, classId: String) {
    self.userId = userId
    self.attId = attId
    self.state = state
    self.classId = classId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "attId": attId, "state": state, "classId": classId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateAttendanceEntryState", arguments: ["userId": GraphQLVariable("userId"), "attendanceId": GraphQLVariable("attId"), "state": GraphQLVariable("state"), "classId": GraphQLVariable("classId")], type: .object(UpdateAttendanceEntryState.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updateAttendanceEntryState: UpdateAttendanceEntryState? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "updateAttendanceEntryState": updateAttendanceEntryState.flatMap { (value: UpdateAttendanceEntryState) -> ResultMap in value.resultMap }])
    }

    public var updateAttendanceEntryState: UpdateAttendanceEntryState? {
      get {
        return (resultMap["updateAttendanceEntryState"] as? ResultMap).flatMap { UpdateAttendanceEntryState(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "updateAttendanceEntryState")
      }
    }

    public struct UpdateAttendanceEntryState: GraphQLSelectionSet {
      public static let possibleTypes = ["AttendanceEntryResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
        GraphQLField("result", type: .object(Result.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil, result: Result? = nil) {
        self.init(unsafeResultMap: ["__typename": "AttendanceEntryResponse", "error": error, "message": message, "status": status, "result": result.flatMap { (value: Result) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var result: Result? {
        get {
          return (resultMap["result"] as? ResultMap).flatMap { Result(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "result")
        }
      }

      public struct Result: GraphQLSelectionSet {
        public static let possibleTypes = ["AttendanceEntry"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("attendanceId", type: .scalar(String.self)),
          GraphQLField("checkedIn", type: .scalar(Double.self)),
          GraphQLField("checkedOut", type: .scalar(Double.self)),
          GraphQLField("state", type: .scalar(String.self)),
          GraphQLField("type", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, userId: String? = nil, attendanceId: String? = nil, checkedIn: Double? = nil, checkedOut: Double? = nil, state: String? = nil, type: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "AttendanceEntry", "_id": id, "userId": userId, "attendanceId": attendanceId, "checkedIn": checkedIn, "checkedOut": checkedOut, "state": state, "type": type])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var attendanceId: String? {
          get {
            return resultMap["attendanceId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "attendanceId")
          }
        }

        public var checkedIn: Double? {
          get {
            return resultMap["checkedIn"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "checkedIn")
          }
        }

        public var checkedOut: Double? {
          get {
            return resultMap["checkedOut"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "checkedOut")
          }
        }

        public var state: String? {
          get {
            return resultMap["state"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "state")
          }
        }

        public var type: String? {
          get {
            return resultMap["type"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "type")
          }
        }
      }
    }
  }
}

public final class StudentAnswersQuery: GraphQLQuery {
  public let operationDefinition =
    "query studentAnswers($teacherId: String!, $launchId: String!, $setId: String!) {\n  studentAnswers(teacherId: $teacherId, launchId: $launchId, questionSetId: $setId) {\n    __typename\n    nickname\n    lastname\n    studentId\n    average\n    answers {\n      __typename\n      _id\n      studentId\n      questionId\n      launchId\n      intAnswer\n      stringAnswer\n      correct\n    }\n  }\n}"

  public var teacherId: String
  public var launchId: String
  public var setId: String

  public init(teacherId: String, launchId: String, setId: String) {
    self.teacherId = teacherId
    self.launchId = launchId
    self.setId = setId
  }

  public var variables: GraphQLMap? {
    return ["teacherId": teacherId, "launchId": launchId, "setId": setId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("studentAnswers", arguments: ["teacherId": GraphQLVariable("teacherId"), "launchId": GraphQLVariable("launchId"), "questionSetId": GraphQLVariable("setId")], type: .list(.object(StudentAnswer.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(studentAnswers: [StudentAnswer?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "studentAnswers": studentAnswers.flatMap { (value: [StudentAnswer?]) -> [ResultMap?] in value.map { (value: StudentAnswer?) -> ResultMap? in value.flatMap { (value: StudentAnswer) -> ResultMap in value.resultMap } } }])
    }

    public var studentAnswers: [StudentAnswer?]? {
      get {
        return (resultMap["studentAnswers"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [StudentAnswer?] in value.map { (value: ResultMap?) -> StudentAnswer? in value.flatMap { (value: ResultMap) -> StudentAnswer in StudentAnswer(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [StudentAnswer?]) -> [ResultMap?] in value.map { (value: StudentAnswer?) -> ResultMap? in value.flatMap { (value: StudentAnswer) -> ResultMap in value.resultMap } } }, forKey: "studentAnswers")
      }
    }

    public struct StudentAnswer: GraphQLSelectionSet {
      public static let possibleTypes = ["studentAnswers"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("nickname", type: .scalar(String.self)),
        GraphQLField("lastname", type: .scalar(String.self)),
        GraphQLField("studentId", type: .scalar(String.self)),
        GraphQLField("average", type: .scalar(Double.self)),
        GraphQLField("answers", type: .list(.object(Answer.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(nickname: String? = nil, lastname: String? = nil, studentId: String? = nil, average: Double? = nil, answers: [Answer?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "studentAnswers", "nickname": nickname, "lastname": lastname, "studentId": studentId, "average": average, "answers": answers.flatMap { (value: [Answer?]) -> [ResultMap?] in value.map { (value: Answer?) -> ResultMap? in value.flatMap { (value: Answer) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var nickname: String? {
        get {
          return resultMap["nickname"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "nickname")
        }
      }

      public var lastname: String? {
        get {
          return resultMap["lastname"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "lastname")
        }
      }

      public var studentId: String? {
        get {
          return resultMap["studentId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "studentId")
        }
      }

      public var average: Double? {
        get {
          return resultMap["average"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "average")
        }
      }

      public var answers: [Answer?]? {
        get {
          return (resultMap["answers"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Answer?] in value.map { (value: ResultMap?) -> Answer? in value.flatMap { (value: ResultMap) -> Answer in Answer(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Answer?]) -> [ResultMap?] in value.map { (value: Answer?) -> ResultMap? in value.flatMap { (value: Answer) -> ResultMap in value.resultMap } } }, forKey: "answers")
        }
      }

      public struct Answer: GraphQLSelectionSet {
        public static let possibleTypes = ["Answer"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("studentId", type: .scalar(String.self)),
          GraphQLField("questionId", type: .scalar(String.self)),
          GraphQLField("launchId", type: .scalar(String.self)),
          GraphQLField("intAnswer", type: .list(.scalar(String.self))),
          GraphQLField("stringAnswer", type: .scalar(String.self)),
          GraphQLField("correct", type: .scalar(Bool.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, studentId: String? = nil, questionId: String? = nil, launchId: String? = nil, intAnswer: [String?]? = nil, stringAnswer: String? = nil, correct: Bool? = nil) {
          self.init(unsafeResultMap: ["__typename": "Answer", "_id": id, "studentId": studentId, "questionId": questionId, "launchId": launchId, "intAnswer": intAnswer, "stringAnswer": stringAnswer, "correct": correct])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var studentId: String? {
          get {
            return resultMap["studentId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "studentId")
          }
        }

        public var questionId: String? {
          get {
            return resultMap["questionId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "questionId")
          }
        }

        public var launchId: String? {
          get {
            return resultMap["launchId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "launchId")
          }
        }

        public var intAnswer: [String?]? {
          get {
            return resultMap["intAnswer"] as? [String?]
          }
          set {
            resultMap.updateValue(newValue, forKey: "intAnswer")
          }
        }

        public var stringAnswer: String? {
          get {
            return resultMap["stringAnswer"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "stringAnswer")
          }
        }

        public var correct: Bool? {
          get {
            return resultMap["correct"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "correct")
          }
        }
      }
    }
  }
}

public final class UsersCorporateMemberListClassPopulatedQuery: GraphQLQuery {
  public let operationDefinition =
    "query usersCorporateMemberListClassPopulated($userId: String!) {\n  usersCorporateMemberListClassPopulated(userId: $userId) {\n    __typename\n    error\n    status\n    message\n    result {\n      __typename\n      corporate {\n        __typename\n        _id\n        corporateCode\n        ownerId\n        name\n        createdDate\n      }\n      corporateMember {\n        __typename\n        _id\n        userId\n        corporateMemberPositionId\n        createdDate\n        updatedDate\n        corporateId\n      }\n      classes {\n        __typename\n        _id\n        classCode\n        classId\n        classOwnerId\n        userId {\n          __typename\n          _id\n          image\n          nickname\n          lastname\n          email\n          birthday\n          phone\n          state\n          online_at\n        }\n        subUser {\n          __typename\n          _id\n          userId\n          schoolId\n          studentIdCode\n          roleId\n          tierId\n          notification {\n            __typename\n            mchatNotificationCount\n            mclassNotificationCount\n            lastDeleted\n          }\n        }\n        roleId\n        classRole\n        className\n        state\n        joinedDate\n        topic\n        notificationCount\n      }\n    }\n  }\n}"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("usersCorporateMemberListClassPopulated", arguments: ["userId": GraphQLVariable("userId")], type: .object(UsersCorporateMemberListClassPopulated.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(usersCorporateMemberListClassPopulated: UsersCorporateMemberListClassPopulated? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "usersCorporateMemberListClassPopulated": usersCorporateMemberListClassPopulated.flatMap { (value: UsersCorporateMemberListClassPopulated) -> ResultMap in value.resultMap }])
    }

    public var usersCorporateMemberListClassPopulated: UsersCorporateMemberListClassPopulated? {
      get {
        return (resultMap["usersCorporateMemberListClassPopulated"] as? ResultMap).flatMap { UsersCorporateMemberListClassPopulated(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "usersCorporateMemberListClassPopulated")
      }
    }

    public struct UsersCorporateMemberListClassPopulated: GraphQLSelectionSet {
      public static let possibleTypes = ["UsersCorporateMemberListResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("status", type: .scalar(String.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("result", type: .list(.object(Result.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, status: String? = nil, message: String? = nil, result: [Result?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "UsersCorporateMemberListResponse", "error": error, "status": status, "message": message, "result": result.flatMap { (value: [Result?]) -> [ResultMap?] in value.map { (value: Result?) -> ResultMap? in value.flatMap { (value: Result) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var result: [Result?]? {
        get {
          return (resultMap["result"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Result?] in value.map { (value: ResultMap?) -> Result? in value.flatMap { (value: ResultMap) -> Result in Result(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Result?]) -> [ResultMap?] in value.map { (value: Result?) -> ResultMap? in value.flatMap { (value: Result) -> ResultMap in value.resultMap } } }, forKey: "result")
        }
      }

      public struct Result: GraphQLSelectionSet {
        public static let possibleTypes = ["UsersCorporateMemberListCorporate"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("corporate", type: .object(Corporate.selections)),
          GraphQLField("corporateMember", type: .object(CorporateMember.selections)),
          GraphQLField("classes", type: .list(.object(Class.selections))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(corporate: Corporate? = nil, corporateMember: CorporateMember? = nil, classes: [Class?]? = nil) {
          self.init(unsafeResultMap: ["__typename": "UsersCorporateMemberListCorporate", "corporate": corporate.flatMap { (value: Corporate) -> ResultMap in value.resultMap }, "corporateMember": corporateMember.flatMap { (value: CorporateMember) -> ResultMap in value.resultMap }, "classes": classes.flatMap { (value: [Class?]) -> [ResultMap?] in value.map { (value: Class?) -> ResultMap? in value.flatMap { (value: Class) -> ResultMap in value.resultMap } } }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var corporate: Corporate? {
          get {
            return (resultMap["corporate"] as? ResultMap).flatMap { Corporate(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "corporate")
          }
        }

        public var corporateMember: CorporateMember? {
          get {
            return (resultMap["corporateMember"] as? ResultMap).flatMap { CorporateMember(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "corporateMember")
          }
        }

        public var classes: [Class?]? {
          get {
            return (resultMap["classes"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Class?] in value.map { (value: ResultMap?) -> Class? in value.flatMap { (value: ResultMap) -> Class in Class(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [Class?]) -> [ResultMap?] in value.map { (value: Class?) -> ResultMap? in value.flatMap { (value: Class) -> ResultMap in value.resultMap } } }, forKey: "classes")
          }
        }

        public struct Corporate: GraphQLSelectionSet {
          public static let possibleTypes = ["Corporate"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("_id", type: .scalar(String.self)),
            GraphQLField("corporateCode", type: .scalar(String.self)),
            GraphQLField("ownerId", type: .scalar(String.self)),
            GraphQLField("name", type: .scalar(String.self)),
            GraphQLField("createdDate", type: .scalar(Double.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, corporateCode: String? = nil, ownerId: String? = nil, name: String? = nil, createdDate: Double? = nil) {
            self.init(unsafeResultMap: ["__typename": "Corporate", "_id": id, "corporateCode": corporateCode, "ownerId": ownerId, "name": name, "createdDate": createdDate])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var corporateCode: String? {
            get {
              return resultMap["corporateCode"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "corporateCode")
            }
          }

          public var ownerId: String? {
            get {
              return resultMap["ownerId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "ownerId")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var createdDate: Double? {
            get {
              return resultMap["createdDate"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "createdDate")
            }
          }
        }

        public struct CorporateMember: GraphQLSelectionSet {
          public static let possibleTypes = ["CorporateMemberList"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("_id", type: .scalar(String.self)),
            GraphQLField("userId", type: .scalar(String.self)),
            GraphQLField("corporateMemberPositionId", type: .scalar(String.self)),
            GraphQLField("createdDate", type: .scalar(Double.self)),
            GraphQLField("updatedDate", type: .scalar(Double.self)),
            GraphQLField("corporateId", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, userId: String? = nil, corporateMemberPositionId: String? = nil, createdDate: Double? = nil, updatedDate: Double? = nil, corporateId: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "CorporateMemberList", "_id": id, "userId": userId, "corporateMemberPositionId": corporateMemberPositionId, "createdDate": createdDate, "updatedDate": updatedDate, "corporateId": corporateId])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var userId: String? {
            get {
              return resultMap["userId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "userId")
            }
          }

          public var corporateMemberPositionId: String? {
            get {
              return resultMap["corporateMemberPositionId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "corporateMemberPositionId")
            }
          }

          public var createdDate: Double? {
            get {
              return resultMap["createdDate"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "createdDate")
            }
          }

          public var updatedDate: Double? {
            get {
              return resultMap["updatedDate"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "updatedDate")
            }
          }

          public var corporateId: String? {
            get {
              return resultMap["corporateId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "corporateId")
            }
          }
        }

        public struct Class: GraphQLSelectionSet {
          public static let possibleTypes = ["PopulatedClassUserList"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("_id", type: .scalar(String.self)),
            GraphQLField("classCode", type: .scalar(String.self)),
            GraphQLField("classId", type: .scalar(String.self)),
            GraphQLField("classOwnerId", type: .scalar(String.self)),
            GraphQLField("userId", type: .object(UserId.selections)),
            GraphQLField("subUser", type: .object(SubUser.selections)),
            GraphQLField("roleId", type: .scalar(String.self)),
            GraphQLField("classRole", type: .scalar(String.self)),
            GraphQLField("className", type: .scalar(String.self)),
            GraphQLField("state", type: .scalar(String.self)),
            GraphQLField("joinedDate", type: .scalar(Double.self)),
            GraphQLField("topic", type: .list(.scalar(String.self))),
            GraphQLField("notificationCount", type: .scalar(Int.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, classCode: String? = nil, classId: String? = nil, classOwnerId: String? = nil, userId: UserId? = nil, subUser: SubUser? = nil, roleId: String? = nil, classRole: String? = nil, className: String? = nil, state: String? = nil, joinedDate: Double? = nil, topic: [String?]? = nil, notificationCount: Int? = nil) {
            self.init(unsafeResultMap: ["__typename": "PopulatedClassUserList", "_id": id, "classCode": classCode, "classId": classId, "classOwnerId": classOwnerId, "userId": userId.flatMap { (value: UserId) -> ResultMap in value.resultMap }, "subUser": subUser.flatMap { (value: SubUser) -> ResultMap in value.resultMap }, "roleId": roleId, "classRole": classRole, "className": className, "state": state, "joinedDate": joinedDate, "topic": topic, "notificationCount": notificationCount])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var classCode: String? {
            get {
              return resultMap["classCode"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "classCode")
            }
          }

          public var classId: String? {
            get {
              return resultMap["classId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "classId")
            }
          }

          public var classOwnerId: String? {
            get {
              return resultMap["classOwnerId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "classOwnerId")
            }
          }

          public var userId: UserId? {
            get {
              return (resultMap["userId"] as? ResultMap).flatMap { UserId(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "userId")
            }
          }

          public var subUser: SubUser? {
            get {
              return (resultMap["subUser"] as? ResultMap).flatMap { SubUser(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "subUser")
            }
          }

          public var roleId: String? {
            get {
              return resultMap["roleId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "roleId")
            }
          }

          public var classRole: String? {
            get {
              return resultMap["classRole"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "classRole")
            }
          }

          public var className: String? {
            get {
              return resultMap["className"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "className")
            }
          }

          public var state: String? {
            get {
              return resultMap["state"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "state")
            }
          }

          public var joinedDate: Double? {
            get {
              return resultMap["joinedDate"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "joinedDate")
            }
          }

          public var topic: [String?]? {
            get {
              return resultMap["topic"] as? [String?]
            }
            set {
              resultMap.updateValue(newValue, forKey: "topic")
            }
          }

          public var notificationCount: Int? {
            get {
              return resultMap["notificationCount"] as? Int
            }
            set {
              resultMap.updateValue(newValue, forKey: "notificationCount")
            }
          }

          public struct UserId: GraphQLSelectionSet {
            public static let possibleTypes = ["PublicUserInfo"]

            public static let selections: [GraphQLSelection] = [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("_id", type: .scalar(String.self)),
              GraphQLField("image", type: .scalar(String.self)),
              GraphQLField("nickname", type: .scalar(String.self)),
              GraphQLField("lastname", type: .scalar(String.self)),
              GraphQLField("email", type: .scalar(String.self)),
              GraphQLField("birthday", type: .scalar(Double.self)),
              GraphQLField("phone", type: .scalar(String.self)),
              GraphQLField("state", type: .scalar(Int.self)),
              GraphQLField("online_at", type: .scalar(Double.self)),
            ]

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(id: String? = nil, image: String? = nil, nickname: String? = nil, lastname: String? = nil, email: String? = nil, birthday: Double? = nil, phone: String? = nil, state: Int? = nil, onlineAt: Double? = nil) {
              self.init(unsafeResultMap: ["__typename": "PublicUserInfo", "_id": id, "image": image, "nickname": nickname, "lastname": lastname, "email": email, "birthday": birthday, "phone": phone, "state": state, "online_at": onlineAt])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var id: String? {
              get {
                return resultMap["_id"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "_id")
              }
            }

            public var image: String? {
              get {
                return resultMap["image"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "image")
              }
            }

            public var nickname: String? {
              get {
                return resultMap["nickname"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "nickname")
              }
            }

            public var lastname: String? {
              get {
                return resultMap["lastname"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "lastname")
              }
            }

            public var email: String? {
              get {
                return resultMap["email"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "email")
              }
            }

            public var birthday: Double? {
              get {
                return resultMap["birthday"] as? Double
              }
              set {
                resultMap.updateValue(newValue, forKey: "birthday")
              }
            }

            public var phone: String? {
              get {
                return resultMap["phone"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "phone")
              }
            }

            public var state: Int? {
              get {
                return resultMap["state"] as? Int
              }
              set {
                resultMap.updateValue(newValue, forKey: "state")
              }
            }

            public var onlineAt: Double? {
              get {
                return resultMap["online_at"] as? Double
              }
              set {
                resultMap.updateValue(newValue, forKey: "online_at")
              }
            }
          }

          public struct SubUser: GraphQLSelectionSet {
            public static let possibleTypes = ["PublicSubUser"]

            public static let selections: [GraphQLSelection] = [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("_id", type: .scalar(String.self)),
              GraphQLField("userId", type: .scalar(String.self)),
              GraphQLField("schoolId", type: .scalar(String.self)),
              GraphQLField("studentIdCode", type: .scalar(String.self)),
              GraphQLField("roleId", type: .scalar(String.self)),
              GraphQLField("tierId", type: .scalar(String.self)),
              GraphQLField("notification", type: .object(Notification.selections)),
            ]

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(id: String? = nil, userId: String? = nil, schoolId: String? = nil, studentIdCode: String? = nil, roleId: String? = nil, tierId: String? = nil, notification: Notification? = nil) {
              self.init(unsafeResultMap: ["__typename": "PublicSubUser", "_id": id, "userId": userId, "schoolId": schoolId, "studentIdCode": studentIdCode, "roleId": roleId, "tierId": tierId, "notification": notification.flatMap { (value: Notification) -> ResultMap in value.resultMap }])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var id: String? {
              get {
                return resultMap["_id"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "_id")
              }
            }

            public var userId: String? {
              get {
                return resultMap["userId"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "userId")
              }
            }

            public var schoolId: String? {
              get {
                return resultMap["schoolId"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "schoolId")
              }
            }

            public var studentIdCode: String? {
              get {
                return resultMap["studentIdCode"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "studentIdCode")
              }
            }

            public var roleId: String? {
              get {
                return resultMap["roleId"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "roleId")
              }
            }

            public var tierId: String? {
              get {
                return resultMap["tierId"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "tierId")
              }
            }

            public var notification: Notification? {
              get {
                return (resultMap["notification"] as? ResultMap).flatMap { Notification(unsafeResultMap: $0) }
              }
              set {
                resultMap.updateValue(newValue?.resultMap, forKey: "notification")
              }
            }

            public struct Notification: GraphQLSelectionSet {
              public static let possibleTypes = ["SubUserNotification"]

              public static let selections: [GraphQLSelection] = [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("mchatNotificationCount", type: .scalar(Int.self)),
                GraphQLField("mclassNotificationCount", type: .scalar(Int.self)),
                GraphQLField("lastDeleted", type: .scalar(Double.self)),
              ]

              public private(set) var resultMap: ResultMap

              public init(unsafeResultMap: ResultMap) {
                self.resultMap = unsafeResultMap
              }

              public init(mchatNotificationCount: Int? = nil, mclassNotificationCount: Int? = nil, lastDeleted: Double? = nil) {
                self.init(unsafeResultMap: ["__typename": "SubUserNotification", "mchatNotificationCount": mchatNotificationCount, "mclassNotificationCount": mclassNotificationCount, "lastDeleted": lastDeleted])
              }

              public var __typename: String {
                get {
                  return resultMap["__typename"]! as! String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "__typename")
                }
              }

              public var mchatNotificationCount: Int? {
                get {
                  return resultMap["mchatNotificationCount"] as? Int
                }
                set {
                  resultMap.updateValue(newValue, forKey: "mchatNotificationCount")
                }
              }

              public var mclassNotificationCount: Int? {
                get {
                  return resultMap["mclassNotificationCount"] as? Int
                }
                set {
                  resultMap.updateValue(newValue, forKey: "mclassNotificationCount")
                }
              }

              public var lastDeleted: Double? {
                get {
                  return resultMap["lastDeleted"] as? Double
                }
                set {
                  resultMap.updateValue(newValue, forKey: "lastDeleted")
                }
              }
            }
          }
        }
      }
    }
  }
}

public final class JoinCorporateMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation joinCorporate($userId: String!, $corpId: String!) {\n  joinCorporate(userId: $userId, corporateCode: $corpId) {\n    __typename\n    error\n    message\n    status\n  }\n}"

  public var userId: String
  public var corpId: String

  public init(userId: String, corpId: String) {
    self.userId = userId
    self.corpId = corpId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "corpId": corpId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("joinCorporate", arguments: ["userId": GraphQLVariable("userId"), "corporateCode": GraphQLVariable("corpId")], type: .object(JoinCorporate.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(joinCorporate: JoinCorporate? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "joinCorporate": joinCorporate.flatMap { (value: JoinCorporate) -> ResultMap in value.resultMap }])
    }

    /// hereglegch corp luu oroh
    public var joinCorporate: JoinCorporate? {
      get {
        return (resultMap["joinCorporate"] as? ResultMap).flatMap { JoinCorporate(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "joinCorporate")
      }
    }

    public struct JoinCorporate: GraphQLSelectionSet {
      public static let possibleTypes = ["BasicStatusResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "BasicStatusResponse", "error": error, "message": message, "status": status])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }
    }
  }
}

public final class BalanceQuery: GraphQLQuery {
  public let operationDefinition =
    "query balance($userId: String!) {\n  balance(userId: $userId) {\n    __typename\n    error\n    status\n    message\n    result {\n      __typename\n      _id\n      userId\n      balance\n      type\n    }\n  }\n}"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("balance", arguments: ["userId": GraphQLVariable("userId")], type: .object(Balance.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(balance: Balance? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "balance": balance.flatMap { (value: Balance) -> ResultMap in value.resultMap }])
    }

    /// corporateiin uusgesen buh billig avah
    public var balance: Balance? {
      get {
        return (resultMap["balance"] as? ResultMap).flatMap { Balance(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "balance")
      }
    }

    public struct Balance: GraphQLSelectionSet {
      public static let possibleTypes = ["BalanceResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("status", type: .scalar(String.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("result", type: .object(Result.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, status: String? = nil, message: String? = nil, result: Result? = nil) {
        self.init(unsafeResultMap: ["__typename": "BalanceResponse", "error": error, "status": status, "message": message, "result": result.flatMap { (value: Result) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var result: Result? {
        get {
          return (resultMap["result"] as? ResultMap).flatMap { Result(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "result")
        }
      }

      public struct Result: GraphQLSelectionSet {
        public static let possibleTypes = ["Balance"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("balance", type: .scalar(Double.self)),
          GraphQLField("type", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, userId: String? = nil, balance: Double? = nil, type: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Balance", "_id": id, "userId": userId, "balance": balance, "type": type])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var balance: Double? {
          get {
            return resultMap["balance"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "balance")
          }
        }

        public var type: String? {
          get {
            return resultMap["type"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "type")
          }
        }
      }
    }
  }
}

public final class GainTransactionMutation: GraphQLMutation {
  public let operationDefinition =
    "mutation gainTransaction($userId: String!, $amount: Int) {\n  gainTransaction(userId: $userId, amount: $amount) {\n    __typename\n    error\n    message\n    status\n    result {\n      __typename\n      transaction {\n        __typename\n        _id\n        userId\n        amount\n        flow\n        createdDate\n        finalized\n        transactionNumber\n        description\n      }\n      key\n    }\n  }\n}"

  public var userId: String
  public var amount: Int?

  public init(userId: String, amount: Int? = nil) {
    self.userId = userId
    self.amount = amount
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "amount": amount]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("gainTransaction", arguments: ["userId": GraphQLVariable("userId"), "amount": GraphQLVariable("amount")], type: .object(GainTransaction.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(gainTransaction: GainTransaction? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "gainTransaction": gainTransaction.flatMap { (value: GainTransaction) -> ResultMap in value.resultMap }])
    }

    public var gainTransaction: GainTransaction? {
      get {
        return (resultMap["gainTransaction"] as? ResultMap).flatMap { GainTransaction(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "gainTransaction")
      }
    }

    public struct GainTransaction: GraphQLSelectionSet {
      public static let possibleTypes = ["GainTransactionResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
        GraphQLField("result", type: .object(Result.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil, result: Result? = nil) {
        self.init(unsafeResultMap: ["__typename": "GainTransactionResponse", "error": error, "message": message, "status": status, "result": result.flatMap { (value: Result) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var result: Result? {
        get {
          return (resultMap["result"] as? ResultMap).flatMap { Result(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "result")
        }
      }

      public struct Result: GraphQLSelectionSet {
        public static let possibleTypes = ["GainTransactionResult"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("transaction", type: .object(Transaction.selections)),
          GraphQLField("key", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(transaction: Transaction? = nil, key: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "GainTransactionResult", "transaction": transaction.flatMap { (value: Transaction) -> ResultMap in value.resultMap }, "key": key])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var transaction: Transaction? {
          get {
            return (resultMap["transaction"] as? ResultMap).flatMap { Transaction(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "transaction")
          }
        }

        public var key: String? {
          get {
            return resultMap["key"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "key")
          }
        }

        public struct Transaction: GraphQLSelectionSet {
          public static let possibleTypes = ["Transaction"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("_id", type: .scalar(String.self)),
            GraphQLField("userId", type: .scalar(String.self)),
            GraphQLField("amount", type: .scalar(Int.self)),
            GraphQLField("flow", type: .scalar(Bool.self)),
            GraphQLField("createdDate", type: .scalar(Double.self)),
            GraphQLField("finalized", type: .scalar(Bool.self)),
            GraphQLField("transactionNumber", type: .scalar(String.self)),
            GraphQLField("description", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, userId: String? = nil, amount: Int? = nil, flow: Bool? = nil, createdDate: Double? = nil, finalized: Bool? = nil, transactionNumber: String? = nil, description: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Transaction", "_id": id, "userId": userId, "amount": amount, "flow": flow, "createdDate": createdDate, "finalized": finalized, "transactionNumber": transactionNumber, "description": description])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var userId: String? {
            get {
              return resultMap["userId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "userId")
            }
          }

          public var amount: Int? {
            get {
              return resultMap["amount"] as? Int
            }
            set {
              resultMap.updateValue(newValue, forKey: "amount")
            }
          }

          public var flow: Bool? {
            get {
              return resultMap["flow"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "flow")
            }
          }

          public var createdDate: Double? {
            get {
              return resultMap["createdDate"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "createdDate")
            }
          }

          public var finalized: Bool? {
            get {
              return resultMap["finalized"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "finalized")
            }
          }

          public var transactionNumber: String? {
            get {
              return resultMap["transactionNumber"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "transactionNumber")
            }
          }

          public var description: String? {
            get {
              return resultMap["description"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "description")
            }
          }
        }
      }
    }
  }
}

public final class TransactionsQuery: GraphQLQuery {
  public let operationDefinition =
    "query transactions($userId: String!) {\n  transactions(userId: $userId) {\n    __typename\n    error\n    message\n    status\n    result {\n      __typename\n      _id\n      userId\n      amount\n      flow\n      createdDate\n      transactionNumber\n      description\n    }\n  }\n}"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("transactions", arguments: ["userId": GraphQLVariable("userId")], type: .object(Transaction.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(transactions: Transaction? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "transactions": transactions.flatMap { (value: Transaction) -> ResultMap in value.resultMap }])
    }

    public var transactions: Transaction? {
      get {
        return (resultMap["transactions"] as? ResultMap).flatMap { Transaction(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "transactions")
      }
    }

    public struct Transaction: GraphQLSelectionSet {
      public static let possibleTypes = ["TransactionsResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .scalar(Bool.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
        GraphQLField("result", type: .list(.object(Result.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Bool? = nil, message: String? = nil, status: String? = nil, result: [Result?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "TransactionsResponse", "error": error, "message": message, "status": status, "result": result.flatMap { (value: [Result?]) -> [ResultMap?] in value.map { (value: Result?) -> ResultMap? in value.flatMap { (value: Result) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Bool? {
        get {
          return resultMap["error"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var result: [Result?]? {
        get {
          return (resultMap["result"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Result?] in value.map { (value: ResultMap?) -> Result? in value.flatMap { (value: ResultMap) -> Result in Result(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Result?]) -> [ResultMap?] in value.map { (value: Result?) -> ResultMap? in value.flatMap { (value: Result) -> ResultMap in value.resultMap } } }, forKey: "result")
        }
      }

      public struct Result: GraphQLSelectionSet {
        public static let possibleTypes = ["Transaction"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("amount", type: .scalar(Int.self)),
          GraphQLField("flow", type: .scalar(Bool.self)),
          GraphQLField("createdDate", type: .scalar(Double.self)),
          GraphQLField("transactionNumber", type: .scalar(String.self)),
          GraphQLField("description", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, userId: String? = nil, amount: Int? = nil, flow: Bool? = nil, createdDate: Double? = nil, transactionNumber: String? = nil, description: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Transaction", "_id": id, "userId": userId, "amount": amount, "flow": flow, "createdDate": createdDate, "transactionNumber": transactionNumber, "description": description])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var amount: Int? {
          get {
            return resultMap["amount"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "amount")
          }
        }

        public var flow: Bool? {
          get {
            return resultMap["flow"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "flow")
          }
        }

        public var createdDate: Double? {
          get {
            return resultMap["createdDate"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "createdDate")
          }
        }

        public var transactionNumber: String? {
          get {
            return resultMap["transactionNumber"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "transactionNumber")
          }
        }

        public var description: String? {
          get {
            return resultMap["description"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "description")
          }
        }
      }
    }
  }
}