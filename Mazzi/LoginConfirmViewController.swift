//
//  LoginConfirmViewController.swift
//  Mazzi
//
//  Created by Janibekm on 10/22/17.
//  Copyright © 2017 Mazzi App LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import FirebaseAuth

import FirebaseInstanceID
import FirebaseMessaging

class LoginConfirmViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var phoneFull: UILabel!
    @IBOutlet weak var backlogin: UIButton!
    @IBOutlet weak var verificationCode: UITextField!
    @IBOutlet weak var confirm: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var spiner: UIActivityIndicatorView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    let userDefaults = UserDefaults.standard
    let limitLength = 6
    var countcode = ""
    var inputnumber = ""
    var key = ""
    var phone = "97690909090"
    var user_id = ""
    var register = ""
    //var password = ""
    var verificationID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleLabel.textColor = GlobalVariables.purple
        self.phoneFull.textColor = GlobalVariables.purple
        self.verificationCode.textColor = GlobalVariables.purple
        let bottomLine4 = CALayer()
        bottomLine4.frame = CGRect(x:0.0, y:verificationCode.frame.height - 1, width:verificationCode.frame.width, height: 1.0)
        bottomLine4.backgroundColor = GlobalVariables.todpurple.cgColor
        verificationCode.borderStyle = UITextField.BorderStyle.none
        verificationCode.layer.addSublayer(bottomLine4)
        
        self.verificationCode.attributedPlaceholder = NSAttributedString(string: "Баталгаажуулах код",attributes: [NSAttributedString.Key.foregroundColor: GlobalVariables.purple.withAlphaComponent(0.2)])
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(backtoROot))
        
        self.confirm.setTitleColor(GlobalVariables.TextTod, for: UIControl.State.normal)
        self.confirm.layer.cornerRadius = 5
       
        self.phoneFull.text = ""
        self.descLabel.text = "\(countcode) \(inputnumber) дугаар руу илгээсэн баталгаажуулах кодыг оруулна уу"
        self.verificationCode.delegate = self
        self.errorLabel.isHidden = true
        self.spiner.isHidden = true
        self.confirm.layer.cornerRadius = 25
        self.confirm.isEnabled = false
       self.confirm.backgroundColor = GlobalVariables.purple
        checkInternet()
        verificationID = userDefaults.string(forKey: "authVerificationID")!
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        NotificationCenter.default.addObserver(self, selector: #selector(showKeyboard(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    }

    //todojani here is : batalgaajuulah code oruuldag huudsaa dudahdaa viewwill appear deeree confirm huudsaa hiigeerei
    
    override func viewWillAppear(_ animated: Bool) {
        self.verificationCode.becomeFirstResponder()
    }

    @objc func backtoROot(){
        self.navigationController?.popViewController(animated: true)
    }
    @objc func dismissKeyboard(){
        verificationCode.resignFirstResponder()
    }
    @objc func showKeyboard(notification: Notification) {
        if let frame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let height = frame.cgRectValue.height
            if height > 100{
              let keyboardHeight = Int(height.rounded(.toNearestOrEven))
                self.userDefaults.set(keyboardHeight, forKey: "keyboardheight")
                self.userDefaults.synchronize()
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        verificationCode.resignFirstResponder()
        return true
    }
    
    @IBAction func confirm(_ sender: Any) {
        self.spiner.isHidden = false
        self.spiner.startAnimating()
        self.spiner.color = UIColor.white
        self.spiner.backgroundColor = UIColor.black
        self.spiner.alpha = 0.5
        let when = DispatchTime.now() + 1.5 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.sendlogin()
        }
    }
    
    @IBAction func backlogin(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        if (range.location == 0 && string == " ") {
            return false
        }else{
            if newLength >= limitLength {
                self.confirm.isEnabled = true
              //  self.confirm.backgroundColor = GlobalVariables.appColor
            }else{
                self.confirm.isEnabled = false
              //  self.confirm.backgroundColor = UIColor.gray
            }
            return newLength <= limitLength
        }
    }
    
    public func sendlogin(){
        print("PHONEEE :: ", self.phone)
        let code = "123456" //verificationCode.text!
        let basurl = SettingsViewController.baseurl
//        print("verificationID::", verificationID)
        if verificationID == "development"{
            if SettingsViewController.online == true{
                let headers = SettingsViewController.headers
                Alamofire.request("\(basurl)login_without_pass", method: .post, parameters: ["phone":self.phone,"key":code], encoding: JSONEncoding.default,headers:headers).responseJSON{ response in
                  print("RES::" , response)
                    if response.response?.statusCode == 200 {
                        let res = JSON(response.value!)
                      
                        let token =  res["token"].stringValue
                        self.userDefaults.set(token, forKey: "token")
                        self.userDefaults.synchronize()
                        Utils.sharedInstance.token = token
                        GlobalVariables.headerToken = token
                       
                        if (res["error"] == false){
                            DispatchQueue.main.async{
                                self.phone = res["phone"].string!
                                self.user_id = res["_id"].string!
                                SettingsViewController.userId = self.user_id
                                GlobalVariables.user_id = self.user_id
                                self.key = res["key"].string ?? ""
                                self.register = res["registerId"].string!
                                let is_confirmed = res["is_confirmed"].bool!
                                //self.password = res["password"].string!
                                self.userDefaults.set(self.phone, forKey: "phone")
                                self.userDefaults.set(self.key, forKey: "key")
                                self.userDefaults.set(self.user_id, forKey: "user_id")
                                self.userDefaults.set(self.register, forKey: "register")
                                let userInfo = ["image": res["image"].string!,
                                                "nickname":res["nickname"].string!,
                                                "lastname":res["lastname"].string!,
                                                "birthday":res["birthday"].double!,
                                                "email":res["email"].string!,
                                                "gender":res["gender"].string!,
                                                "region":res["region"].string!,
                                                "register":res["registerId"].string!,
                                                "displayphone":"\(self.inputnumber)",
                                                "is_confirmed":is_confirmed
                                    ] as [String : Any]
                                UserDefaults.standard.set(userInfo, forKey: "userInfo")
                                SettingsViewController.userId = self.user_id
                                GlobalVariables.user_id = self.user_id
                                self.userDefaults.synchronize()
                            }
                            let when = DispatchTime.now() + 2 // change 2 to desired number of seconds
                            DispatchQueue.main.asyncAfter(deadline: when) {
                                self.performSegue(withIdentifier: "createpro", sender: self)
                                self.spiner.isHidden = true
                                self.spiner.stopAnimating()
                            }
                        }else{
                             self.performSegue(withIdentifier: "createpro", sender: self)
                            self.errorLabel.isHidden = false
                            self.errorLabel.text = "Нууц дугаар буруу байна !"
                            self.spiner.isHidden = true
                            self.spiner.stopAnimating()
                            checkInternet()
                        }
                    }else{
                        self.errorLabel.isHidden = false
                        self.errorLabel.text = "Сервертэй холбогдож чадсангүй дахин илгээнэ үү !!"
                        self.spiner.isHidden = true
                        self.spiner.stopAnimating()
                        checkInternet()
                    }
                }
            }else{
                self.errorLabel.text = "Интернетийн холболтоо шалгана уу!"
                self.spiner.isHidden = true
                self.spiner.stopAnimating()
                checkInternet()
            }
        }else{
            
            print("withVerificationID:", verificationID )
            print("==================================")
             print("verificationCode:", code )
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID,verificationCode: code)
            Auth.auth().signInAndRetrieveData(with: credential) { (user,error ) in

            if let error = error {
                print("LoginConfirmViewController - login error: \(error.localizedDescription)")
                self.errorLabel.isHidden = false
                self.errorLabel.text = "Нууц дугаар буруу байна !"
                self.spiner.isHidden = true
                self.spiner.stopAnimating()
                checkInternet()
                return
            }else{
                if SettingsViewController.online == true{
                    let headers = SettingsViewController.headers
                     print(" ----LOGIN_WITHOUT_PASS---- ")
                    Alamofire.request("\(basurl)login_without_pass", method: .post, parameters: ["phone":self.phone,"key":code], encoding: JSONEncoding.default,headers:headers).responseJSON{ response in
                        if response.response?.statusCode == 200 {
                            let res = JSON(response.value!)
                          
                            let token =  res["token"].stringValue
                            Utils.sharedInstance.token = token
                            GlobalVariables.headerToken = token
                            self.userDefaults.set(token, forKey: "token")
                            self.userDefaults.synchronize()
                            
                            if (res["error"] == false){
                                //todo
//                                let token = Messaging.messaging().fcmToken
                                DispatchQueue.main.async{
                                    self.phone = res["phone"].string!
                                    self.user_id = res["_id"].string!
                                    self.register = res["registerId"].string!
                                    //zasvar
                                    self.key = "123456"
//                                    self.key = res["key"].string!
                                    let is_confirmed = res["is_confirmed"].bool!
                            
                                    self.userDefaults.set(self.phone, forKey: "phone")
                                    self.userDefaults.set(self.key, forKey: "key")
                                    self.userDefaults.set(self.user_id, forKey: "user_id")
                                   self.userDefaults.set(self.register, forKey: "register")
                                    let userInfo = ["image": res["image"].string!,
                                                    "nickname":res["nickname"].string!,
                                                    "lastname":res["lastname"].string!,
                                                    "birthday":res["birthday"].double!,
                                                    "email":res["email"].string!,
                                                    "gender":res["gender"].string!,
                                                    "region":res["region"].string!,
                                                    "register":res["registerId"].string!,
                                                    "displayphone":"\(self.inputnumber)",
                                                    "is_confirmed":is_confirmed
                                        
                                        ] as [String : Any]
                                    UserDefaults.standard.set(userInfo, forKey: "userInfo")
                                    self.userDefaults.synchronize()
                                }
                                SettingsViewController.userId = self.user_id
                                GlobalVariables.user_id = self.user_id
                                let when = DispatchTime.now() + 2 // change 2 to desired number of seconds
                                DispatchQueue.main.asyncAfter(deadline: when) {
                                    self.performSegue(withIdentifier: "createpro", sender: self)
                                    self.spiner.isHidden = true
                                    self.spiner.stopAnimating()
                                }
                            }else{
                                self.errorLabel.isHidden = false
                                self.errorLabel.text = "Нууц дугаар буруу байна !"
                                self.spiner.isHidden = true
                                self.spiner.stopAnimating()
                                checkInternet()
                            }
                        }else{
                            self.errorLabel.isHidden = false
                            self.errorLabel.text = "Сервертэй холбогдож чадсангүй дахин илгээнэ үү !"
                            self.spiner.isHidden = true
                            self.spiner.stopAnimating()
                            checkInternet()
                        }
                    }
                }else{
                    self.errorLabel.text = "Интернетийн холболтоо шалгана уу!"
                    self.spiner.isHidden = true
                    self.spiner.stopAnimating()
                    checkInternet()
                }
            }
        }
    }
      
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //if segue.identifier == "createpro" {
            //let tokey = segue.destination as? CreateProfileController
        //}
    }

}
