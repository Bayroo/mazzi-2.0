//
//  MoreTableViewCell.swift
//  Mazzi
//
//  Created by Bayara on 2/18/19.
//  Copyright © 2019 Mazz Application. All rights reserved.
//

import Foundation

class MoreTableViewCell:UITableViewCell{
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var inviteBtn : UIButton!
    @IBOutlet weak var numberLabel: UILabel!
    
}
