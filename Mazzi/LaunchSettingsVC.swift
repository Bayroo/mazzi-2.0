//
//  LaunchSettingsVC.swift
//  Mazzi
//
//  Created by Bayara on 12/8/18.
//  Copyright © 2018 Mazzi Application. All rights reserved.
//

import Foundation
import KVNProgress
import Apollo

class LaunchSettingsVC:UIViewController,UITableViewDataSource,UITableViewDelegate{

    
    var setId = ""
    var teacherId = ""
    var myId = SettingsViewController.userId
//    var apollo = SettingsViewController.apollo
    var classId = ""
    var roomName = ""
    var launchId = ""
    var examTime : Double!
    var tapGesture : UITapGestureRecognizer!
    
    //-----------------------
    var subview = UIView()
    var tableView = UITableView()
    var titleLabel = UILabel()
    var closeBtn = UIButton()
    var selectedAttendanceId = ""
    var selectedIndex = 0
    var lists = [AttendancesQuery.Data.Attendance]()
    var dateformatter = DateFormatter()
    //-----------------------
    let apollo: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        // Add additional headers as needed
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
//        print("TOKEN APOLLOO:: ", GlobalVariables.headerToken)
        let url = URL(string:SettingsViewController.graphQL)!
        
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    @IBOutlet weak var launchTitle: UITextField!
    @IBOutlet weak var attendanceRequire: UISwitch!
    @IBOutlet weak var timeTxtfield: UITextField!
    @IBOutlet weak var onetry: UISwitch!
    @IBOutlet weak var shuffleQuesotion: UISwitch!
    @IBOutlet weak var shuffleAnswer: UISwitch!
    @IBOutlet weak var launchNameLabel: UILabel!
    @IBOutlet var startlaunchBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialize()
        self.initSubView()
        print("setId: ",self.setId)
        print("tId: ",self.teacherId)
        
        self.launchTitle.layer.borderWidth = 1.0
        self.launchTitle.layer.cornerRadius = 5.0
        self.launchTitle.layer.borderColor = UIColor.black.cgColor
    }
    
    func initSubView(){
        self.subview.frame = CGRect(x: 0.0, y: self.view.bounds.height, width: self.view.bounds.width, height: self.view.bounds.height - 44)
        self.subview.backgroundColor = GlobalVariables.F5F5F5
        self.tableView.frame  =  CGRect(x: 0.0, y: 20.0, width: self.view.bounds.width, height: self.view.bounds.height - 88)
        self.titleLabel.frame = CGRect(x: self.view.bounds.width/2 - 100, y: 2, width: 200, height: 40)
        self.closeBtn.frame = CGRect(x: self.view.bounds.width - 37, y: 7, width: 30, height: 30)
        self.tableView.register((UINib(nibName: "attListCel", bundle: nil)), forCellReuseIdentifier: "attlist")
//        self.titleLabel.text = "Шалгалт өгөхийн тулд бүртгүүлсэн байх ёстой ирцийг сонгоно уу"
        self.titleLabel.font = GlobalVariables.customFont12
        self.titleLabel.numberOfLines = 2
        self.titleLabel.lineBreakMode = .byWordWrapping
        self.titleLabel.textAlignment = .center
        self.view.addSubview(self.subview)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.subview.addSubview(self.titleLabel)
        self.subview.addSubview(self.closeBtn)
        self.subview.addSubview(self.tableView)
    }
    
   @objc func showSubview(){
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "close"), style: .plain, target: self, action: #selector(hideSubview))
        self.getAttendanceList()
        self.title = "Ирц сонгоно уу"
        self.view.removeGestureRecognizer(self.tapGesture)
         UIView.animate(withDuration: 0.5) {
          self.subview.frame = CGRect(x: 0.0, y: 44.0, width: self.view.bounds.width, height: self.view.bounds.height - 44)
        }
    }
    
    @objc  func hideSubview(){
        self.navigationItem.rightBarButtonItem = nil
        self.title = "Тохиргоо"
        self.view.addGestureRecognizer(self.tapGesture)
         UIView.animate(withDuration: 0.5) {
            self.subview.frame = CGRect(x: 0.0, y: self.view.bounds.height, width: self.view.bounds.width, height: self.view.bounds.height - 44)
        }
    }
    
    func initialize(){
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "05"), style: .plain, target: self, action: #selector(back))
       
        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.view.addGestureRecognizer(tapGesture)
        self.title = "Тохиргоо"
        self.onetry.isOn = false
        self.shuffleAnswer.isOn = false
        self.shuffleQuesotion.isOn = false
        self.attendanceRequire.isOn = false
        self.attendanceRequire.addTarget(self, action: #selector(changeValueAttendanceRequire), for: .valueChanged)
        
        self.startlaunchBtn.layer.cornerRadius = 5
        self.startlaunchBtn.layer.masksToBounds = true
        self.startlaunchBtn.backgroundColor = GlobalVariables.purple
        self.startlaunchBtn.titleLabel?.textColor = UIColor.white
        self.launchNameLabel.text = "\("Асуултын багцын нэр: ")\(self.roomName)"
        self.timeTxtfield.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }
    
    @objc func changeValueAttendanceRequire(){
        if self.attendanceRequire.isOn == true {
            self.showSubview()
        }else{
            self.hideSubview()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.timeTxtfield.resignFirstResponder()
        if GlobalStaticVar.isLaunchEnd == true{
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    @objc func dismissKeyboard(){
        self.launchTitle.resignFirstResponder()
        self.timeTxtfield.resignFirstResponder()
    }
    
    @objc func back(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func textFieldDidChange(textField:UITextField){

        if (textField.text?.count)! > 0{
            let t = Int(textField.text!)! * 60000
            self.examTime = Double(t)
        }else{
            self.examTime = nil
        }
    }
  
    @IBAction func enableLaunchBtn(_ sender: Any) {
        if SettingsViewController.online == true{

             let launchsetting = LaunchSettingsInput.init(security: false, shuffleQuestion: self.shuffleQuesotion.isOn, shuffleAnswer: self.shuffleAnswer.isOn, showResult: false, oneTry: self.onetry.isOn, attendanceRequired: self.attendanceRequire.isOn, reflection: false, timer: self.examTime)
            
            let inputlaunchData = InputlaunchData(attendanceId: self.selectedAttendanceId)
            print("USERROLE::: ", GlobalStaticVar.classRole )
            if GlobalStaticVar.classRole == "Teacher"{
               
                let mutation = EnableLaunchMutation(teacherId: self.myId, questionSetId: self.setId, launchSettings: launchsetting,classId: self.classId,InputlaunchData: inputlaunchData, type: "",title: self.launchTitle.text!)
                KVNProgress.show(withStatus: "", on: self.view)
                apollo.perform(mutation: mutation) { result, error in
                    KVNProgress.dismiss()
                    print("res:",result?.data?.enableLaunch as Any)
                    if result?.data?.enableLaunch != nil{
                        self.roomName = (result?.data?.enableLaunch?.key!) ?? ""
                        self.launchId = (result?.data?.enableLaunch?.id!) ?? ""
                        self.examTime = result?.data?.enableLaunch?.launchSettings?.timer ?? 0.0
                        self.performSegue(withIdentifier: "toactiveLaunch", sender: self)
                    }else{
                        KVNProgress.showError(withStatus: "Идэвхтэй шалгалт байна")
                    }
                }
            }else{
                let mutation = EnableLaunchMutation(teacherId: self.teacherId, questionSetId: self.setId, launchSettings: launchsetting,classId: self.classId,InputlaunchData: inputlaunchData, type: "",title: self.launchTitle.text!)
                KVNProgress.show(withStatus: "", on: self.view)
                apollo.perform(mutation: mutation) { result, error in
                    KVNProgress.dismiss()
                    print("ressss:",result?.data?.enableLaunch as Any)
                    if result?.data?.enableLaunch != nil{
                        self.roomName = (result?.data?.enableLaunch?.key!) ?? ""
                        self.launchId = (result?.data?.enableLaunch?.id!) ?? ""
                        self.examTime = result?.data?.enableLaunch?.launchSettings?.timer ?? 0.0
                        self.performSegue(withIdentifier: "toactiveLaunch", sender: self)
                    }else{
                        KVNProgress.showError(withStatus: "Идэвхтэй шалгалт байна")
                    }
                }
            }
        }else{
            KVNProgress.showError(withStatus: "Интэрнет холболтоо шалгана уу!", on: self.view)
        }
     
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toactiveLaunch"{
            let controller = segue.destination as! ActiveLaunchVC
            controller.isstart   = false
            controller.roomName  = self.roomName
            controller.launchId  = self.launchId
          
            controller.setId     = self.setId
            controller.Alltime   = self.examTime
            if GlobalStaticVar.classRole == "Teacher" {
                  controller.teacherId = self.myId
            }else{
                  controller.teacherId = self.teacherId
            }
//          controller.
        }
    }
    
    func getAttendanceList(){
        let query = AttendancesQuery(userId: self.myId, classId: self.classId)
        self.apollo.fetch(query: query) { result, error in
            if (result?.data?.attendances?.count)! > 0{
                self.lists = (result?.data?.attendances)! as! [AttendancesQuery.Data.Attendance]
                self.tableView.reloadData()
            }else{
               
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.selectedAttendanceId != "" {
             self.selectedAttendanceId = self.lists[indexPath.row].id ?? ""
             self.selectedIndex = indexPath.row
             self.tableView.reloadData()
        }else if  self.selectedAttendanceId == self.lists[indexPath.row].id! {
             self.selectedIndex = -1
             self.selectedAttendanceId = ""
             self.tableView.reloadData()
        }else{
            self.selectedAttendanceId = self.lists[indexPath.row].id ?? ""
            self.selectedIndex = indexPath.row
            self.tableView.reloadData()
        }
        self.hideSubview()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.lists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "attlist", for: indexPath) as! AttlistCell
    
        if self.selectedAttendanceId  != "" {
            if self.selectedIndex == indexPath.row{
                  cell.accessoryType = .checkmark
            }else{
                  cell.accessoryType = .none
            }
        }else{
              cell.accessoryType = .none
        }
      
        cell.codeLabel.text = self.lists[indexPath.row].topicId?.name ?? ""
        let lastlogin = NSDate(timeIntervalSince1970: (self.lists[indexPath.row].createdDate!/1000))
        self.dateformatter.dateStyle = .short
        self.dateformatter.timeStyle = .none
        let last = self.dateformatter.string(from: lastlogin as Date)
        cell.dateLabel.text = "\(last),"
        
        let startTime = NSDate(timeIntervalSince1970: ((self.lists[indexPath.row].time?.timeBegan)!/1000))
        self.dateformatter.dateStyle = .none
        self.dateformatter.timeStyle = .short
        let started = self.dateformatter.string(from: startTime as Date)
        
        let endTime = NSDate(timeIntervalSince1970: ((self.lists[indexPath.row].time?.timeEnds)!/1000))
        self.dateformatter.dateStyle = .none
        self.dateformatter.timeStyle = .short
        let ended = self.dateformatter.string(from: endTime as Date)
        cell.timeLabel.text = "\(started)-\(ended)"
        return cell
    }
    
}
