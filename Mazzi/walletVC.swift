//
//  walletVC.swift
//  Mazzi
//
//  Created by Bayara on 5/24/19.
//  Copyright © 2019 Mazz Application. All rights reserved.
//

import Foundation
import UIKit
import Apollo
import KVNProgress
import WebKit
class walletVC: UIViewController,UITableViewDelegate, UITableViewDataSource,WKUIDelegate,WKNavigationDelegate {
    
    @IBOutlet weak var walletTableView: UITableView!
    
    var myId = SettingsViewController.userId
    var webview = WKWebView()
    var nickname = ""
    var lastname = ""
    var birthday : Double!
    var region = ""
    var email = ""
    var displayphone = ""
    var profileImg = ""
    var fileurl = SettingsViewController.fileurl
    var items = [String]()
    var balance = ""
    let apollos: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
        let url = URL(string:SettingsViewController.graphQL)!
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Миний данс"
        self.getInfo()
        self.items = ["Дансаа цэнэглэх","Хуулга харах"]
        self.walletTableView.delegate = self
        self.walletTableView.dataSource = self
        self.walletTableView.register((UINib(nibName: "profileCell", bundle: nil)), forCellReuseIdentifier: "more")
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(backtoRoot))
        self.webview.navigationDelegate = self
        self.webview.uiDelegate = self
        self.webview.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        checkInternet()
        if SettingsViewController.online {
            self.getBalance()
        }else{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу!")
        }
        self.walletTableView.reloadData()
    }
    
    @objc func backtoRoot(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func getInfo(){
        let userInfo = userDefaults.dictionary(forKey: "userInfo")
        self.nickname = userInfo!["nickname"] as! String
        self.lastname = userInfo!["lastname"] as! String
        self.birthday = ( userInfo!["birthday"] as! Double )/1000
        self.email = userInfo!["email"] as! String
        self.region = userInfo!["region"] as! String
        self.displayphone = userInfo!["displayphone"] as! String
        self.profileImg = userInfo!["image"] as! String
        
    }
    
    func getBalance(){
        let query = BalanceQuery(userId: self.myId)
        self.apollos.fetch(query: query) { result, error in
//            print("RESULT>>:: ", result?.data?.balance)
            if result?.data?.balance?.error == false {
                let balance = result?.data?.balance?.result?.balance ?? 0.0
//                print("BALANCE:: ", balance)
                self.balance = "Таны дансанд: \(balance)₮"
                self.walletTableView.reloadData()
            }else{
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 90
        }else{
            return 50
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else{
            return self.items.count
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
//            self.performSegue(withIdentifier: "updatePro", sender: self)
        }else {
            if indexPath.row == 0{
                self.performSegue(withIdentifier: "refill", sender: self)
//                let alert = UIAlertController(title: "Төлбөрийн хэлбэрээ сонгоно уу", message: nil, preferredStyle: .actionSheet)
//                alert.addAction(UIAlertAction(title: "Дансаар", style: .default, handler: { action in
//                        self.performSegue(withIdentifier: "banks", sender: self)
//                }))
//                alert.addAction(UIAlertAction(title: "Картаар", style: .default, handler:{action in
//                    self.performSegue(withIdentifier: "refill", sender: self)
//                }))
//                alert.addAction(UIAlertAction(title: "Болих", style: .cancel, handler: nil))
//                self.present(alert, animated: true)
            
            }else if indexPath.row == 1 {
                self.performSegue(withIdentifier: "transactions", sender: self)
            }else if indexPath.row == 2{
//                let viewController = storyboard?.instantiateViewController(withIdentifier: "inviteContacts") as! inviteContacts
//                let navController = UINavigationController(rootViewController: viewController)
//                self.present(navController, animated:true, completion: nil)
//                 self.performSegue(withIdentifier: "toInvite", sender: self)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "more", for: indexPath) as! MoreCell
            cell.accessoryType = .disclosureIndicator
            cell.nameLabel.text = self.nickname.uppercased()
            cell.phoneLabel.text = self.balance //self.displayphone
            if self.profileImg == "" {
                cell.imgView.image = UIImage(named: "circle-avatar")
            }else{
                cell.imgView.sd_setImage(with: URL(string:"\(self.fileurl)\(self.profileImg)"), completed: nil)
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultCell")!
            cell.accessoryType = .disclosureIndicator
            cell.textLabel?.font = GlobalVariables.customFont14
            cell.textLabel?.text = self.items[indexPath.row]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }else{
            return 20
        }
    }
}
