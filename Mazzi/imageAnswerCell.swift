//
//  imageAnswerCell.swift
//  Mazzi
//
//  Created by Bayara on 5/3/19.
//  Copyright © 2019 Mazz Application. All rights reserved.
//

import Foundation

class imageAnswerCell: UITableViewCell {
    
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var backview: UIView!
    @IBOutlet weak var imgview: UIImageView!
    @IBOutlet weak var webview: UIWebView!
    
    
    func initcellData(){
        self.btn.layer.borderWidth = 1.0
        self.btn.layer.borderColor = GlobalVariables.purple.cgColor
        self.btn.layer.cornerRadius = 20
        self.btn.layer.masksToBounds = true
        
        self.backview.layer.cornerRadius = 10
        self.backview.layer.masksToBounds = true
        self.imgview.layer.masksToBounds = true
    }
}
