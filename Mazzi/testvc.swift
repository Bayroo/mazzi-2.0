//
//  testvc.swift
//  Mazzi
//
//  Created by Bayara on 7/16/19.
//  Copyright © 2019 Mazz Application. All rights reserved.
//

import Foundation

class testvc: UIViewController{
    @IBOutlet weak var readBtn: UIButton!
    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    override func viewDidLoad() {
        super .viewDidLoad()
        self.okBtn.layer.cornerRadius = 25
        self.okBtn.layer.masksToBounds = true
    }
    
    @IBAction func readEulaBtn(_ sender: Any) {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "termsofuse") as! TermsofUseVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func OkBtn(_ sender: Any) {
        
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "confirm") as! LoginConfirmViewController
//         let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginNameVC") as! LoginNameVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func cancelBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
