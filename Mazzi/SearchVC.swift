//
//  SearchVC.swift
//  Mazzi
//
//  Created by Janibekm on 11/20/17.
//  Copyright © 2017 Mazzi App LLC. All rights reserved.
//

import UIKit
import Contacts
import Alamofire
import SwiftyJSON
import SocketIO
import RealmSwift
import KVNProgress

class SearchVC: UIViewController,UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate , UITextFieldDelegate {
 
//    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var composeBtn: UIButton!
    @IBOutlet weak var searchBat: UISearchBar!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var groupimage: UIImageView!
    @IBOutlet weak var grouptitle: UITextField!
    @IBOutlet weak var searchhead: UIView!
    @IBOutlet weak var groupimageSelect: UIButton!
    @IBOutlet weak var createGroupChat: UIButton!
    @IBOutlet weak var searchText: UITextField!
//    @IBOutlet weak var createGroupConversation: UIButton!
    
    @IBOutlet weak var backbtn: UIButton!
    var headerLabel = UILabel()
    var selectedIndex = [Int]()
    var selecteduserid  = ""
    var isCreateChat = false
    var groupChatusers = ["\(SettingsViewController.userId)"]
    var pic = ""
    var senderchatid = ""
    var navtitle = ""
    var chatuser = [User_group]()
    var installedContacts : Results<Contacts_realm>!
    var filteredContacts : Results<Contacts_realm>!
    
    
    var chatIsThread = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isCreateChat = true
        self.tableview.delegate = self
        self.tableview.dataSource = self
        self.groupChatusers = ["\(SettingsViewController.userId)"]
            installedContacts = try! Realm().objects(Contacts_realm.self).sorted(byKeyPath: "name", ascending: true).filter("created_at != 0")
            filteredContacts = installedContacts
        self.searchBat.delegate = self
        self.grouptitle.layer.masksToBounds = true
        self.grouptitle.delegate = self
        self.grouptitle.layer.borderWidth = 1.0
        self.grouptitle.layer.borderColor = UIColor.white.cgColor
        self.grouptitle.layer.cornerRadius = 5.0
        self.grouptitle.tintColor = UIColor.white
        self.grouptitle.attributedPlaceholder = NSAttributedString(string: "Групп чатын нэрээ оруулна уу",attributes: [NSAttributedString.Key.foregroundColor: GlobalVariables.whiteGray.withAlphaComponent(0.4), NSAttributedString.Key.font: GlobalVariables.customFont12!])
        self.grouptitle.isHidden = true
    
        self.composeBtn.setImage(UIImage(named: "001-bear-pawprint"), for: .normal)
        self.composeBtn.isEnabled = false
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
//        self.backbtn.addTarget(self, action: #selector(back(_:)), for: .touchUpInside)
//        self.composeBtn.addTarget(self, action: #selector(createGroupConversation), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(back(_:)))
        let ico = UIImage.init(named: "001-bear-pawprint")?.withRenderingMode(.alwaysOriginal)
        let sendBtn = UIBarButtonItem.init(image: ico!, style: .plain, target: self, action: #selector(createGroupConversation))
        self.navigationItem.rightBarButtonItem = sendBtn
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        self.title = "Чатлах найзаа сонгох"
//        self.initheaderLabel()
    }
    
    func initheaderLabel(){
        self.headerLabel.frame = CGRect(x: self.view.bounds.width/2 - 100, y: 5, width: 200 , height: 20)
        self.headerLabel.text = "Чатлах найзаа сонгох"
        self.headerLabel.font = GlobalVariables.customFont16
        self.headerLabel.backgroundColor = UIColor.red
        self.headerLabel.textColor = UIColor.white
        self.headerLabel.textAlignment = .center
        self.view.addSubview(self.headerLabel)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)
        if realm.objects(Contacts_realm.self).count != 0{
            installedContacts = realm.objects(Contacts_realm.self).sorted(byKeyPath: "name", ascending: true).filter("created_at != 0")
            filteredContacts = installedContacts
        }
        
        if GlobalVariables.isFromChat == true {
            GlobalVariables.isFromChat = false
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if self.searchBat.text?.count != 0 {
            let searchtxt = self.searchBat.text?.lowercased()
            if (searchtxt?.isAlphanumeric)!{
                self.filteredContacts = self.installedContacts.filter("name CONTAINS[c]'\(searchtxt!)'")
            }else{
                self.filteredContacts = self.installedContacts.filter("phone CONTAINS '\(searchtxt!)'")
                self.tableview.reloadData()
            }
        }else{
            self.filteredContacts = self.installedContacts
        }
        self.tableview.reloadData()
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (range.location == 0 && string == " ") {
            return false
        }else{
            if (grouptitle.text?.count)! > 1 {
                if groupChatusers.count > 2{
                    let ico = UIImage.init(named: "001-bear-pawprint-saaral")?.withRenderingMode(.alwaysOriginal)
                    let sendBtn = UIBarButtonItem.init(image: ico!, style: .plain, target: self, action: #selector(createGroupConversation))
                    self.navigationItem.rightBarButtonItem = sendBtn
                    self.navigationItem.rightBarButtonItem?.isEnabled = true
                }
            }else{
                let ico = UIImage.init(named: "001-bear-pawprint")?.withRenderingMode(.alwaysOriginal)
                let sendBtn = UIBarButtonItem.init(image: ico!, style: .plain, target: self, action: #selector(createGroupConversation))
                self.navigationItem.rightBarButtonItem = sendBtn
                self.navigationItem.rightBarButtonItem?.isEnabled = false
            }
            return true
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        if isCreateChat == true {
            if groupChatusers.contains(self.filteredContacts[indexPath.row].id){
                let indeks = selectedIndex.index(where:{$0 == indexPath.row})
                selectedIndex.remove(at: indeks!)
                let index = self.groupChatusers.index(where: {$0 == self.filteredContacts[indexPath.row].id})
                self.groupChatusers.remove(at: index!)
                let indexs = self.chatuser.index(where: {$0.id == self.filteredContacts[indexPath.row].id})
                self.chatuser.remove(at: indexs!)
                self.tableview.reloadData()
            }else{
                selectedIndex.append(indexPath.row)
                self.groupChatusers.append(self.filteredContacts[indexPath.row].id)
                let users = User_group.init(id: self.filteredContacts[indexPath.row].id, image: self.filteredContacts[indexPath.row].image,nickname:self.filteredContacts[indexPath.row].name,lastname:self.filteredContacts[indexPath.row].lastname, surname: self.filteredContacts[indexPath.row].surname, phone: self.filteredContacts[indexPath.row].phone, state: self.filteredContacts[indexPath.row].state, online_at: self.filteredContacts[indexPath.row].online_at, fcm_token: self.filteredContacts[indexPath.row].fcm_token, registerId: self.filteredContacts[indexPath.row].register)
                self.chatuser.append(users)
                self.tableview.reloadData()
            }
//            print("count: ",groupChatusers.count)
        }else{
            let viewController = storyboard?.instantiateViewController(withIdentifier: "ProfileContact") as! ProfileContact
            viewController.contactid = self.filteredContacts[indexPath.row].id
            let navController = UINavigationController(rootViewController: viewController)
            self.present(navController, animated:true, completion: nil)
        }
       // print(selectedIndex)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if realm.objects(Contacts_realm.self).count != 0{
            return self.filteredContacts.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath) as! SearchCell
        cell.clearCellData()
        if isCreateChat == true {
            cell.backgroundColor = UIColor.clear
            cell.tintColor = GlobalVariables.purple
            if groupChatusers.contains(self.filteredContacts[indexPath.row].id){
                cell.accessoryType = .checkmark
                cell.subview.layer.borderColor = GlobalVariables.purple.cgColor
                if groupChatusers.count > 2{
                 //   print("groupChat")
                    self.navigationItem.titleView = self.grouptitle
                    self.headerLabel.isHidden = true
                    self.grouptitle.isHidden = false
                  
//                     print("2-s ih")
                    if grouptitle.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")) != "" {
                        let ico = UIImage.init(named: "001-bear-pawprint-saaral")?.withRenderingMode(.alwaysOriginal)
                        let sendBtn = UIBarButtonItem.init(image: ico!, style: .plain, target: self, action: #selector(createGroupConversation))
                        self.navigationItem.rightBarButtonItem = sendBtn
                        self.navigationItem.rightBarButtonItem?.isEnabled = true
//                        print("2-s ih with title")
                    }else{
//                        let alert = UIAlertController(title: "Алдаа !", message: "Групп чатын нэрээ оруулна уу!", preferredStyle: UIAlertControllerStyle.alert)
//                        alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertActionStyle.default, handler: nil))
//                        self.present(alert, animated: true, completion: nil)
                    }
                }else{

                    self.headerLabel.isHidden = false
                    self.grouptitle.isHidden = true
                    let ico = UIImage.init(named: "001-bear-pawprint-saaral")?.withRenderingMode(.alwaysOriginal)
                    let sendBtn = UIBarButtonItem.init(image: ico!, style: .plain, target: self, action: #selector(createPrivateChat))
                    self.navigationItem.rightBarButtonItem = sendBtn
                    self.navigationItem.rightBarButtonItem?.isEnabled = true
//                    print("private")
                     }
            }else{
                if groupChatusers.count < 2  {
                    self.headerLabel.isHidden = false
                    self.grouptitle.isHidden = true
                    let ico = UIImage.init(named: "001-bear-pawprint")?.withRenderingMode(.alwaysOriginal)
                    let sendBtn = UIBarButtonItem.init(image: ico!, style: .plain, target: self, action: #selector(createGroupConversation))
                    self.navigationItem.rightBarButtonItem = sendBtn
                    self.navigationItem.rightBarButtonItem?.isEnabled = false
                }
                cell.accessoryType = .none
            }
        }else{
             cell.backgroundColor = UIColor.clear
        }
        let fileurl = SettingsViewController.fileurl
        let mypic = self.filteredContacts[indexPath.row].image
        cell.searchProname.text = self.filteredContacts[indexPath.row].name
        if mypic != ""{
            cell.searchPropic.sd_setImage(with: URL(string:"\(fileurl)\(mypic)"), completed: nil)
        }else{
            cell.searchPropic.image = UIImage(named: "Mazzi-Icon")
        }
        cell.searchProdetail.text = self.filteredContacts[indexPath.row].phone
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    func photoGet(capturedImage: UIImage) {
        let userid = userDefaults.object(forKey: "user_id") as? String
        let fileurl = SettingsViewController.fileurl
        Message.uploadimage(chatId: userid! ,image: capturedImage, completion: { (state, imageurl) in
            DispatchQueue.main.async {
                let url = JSON(imageurl)
                self.pic = url["url"].string!
                let imageurl = URL(string:"\(fileurl)\(self.pic)")!
                self.groupimage.sd_setImage(with: imageurl, completed: { (image, error, cacheType, url) in
                    self.groupimage.sd_setImage(with: imageurl, completed: nil)
                })
            }
        })
    }

@objc func createGroupConversation() {
        let userid = SettingsViewController.userId
        let baseurl = SettingsViewController.baseurl
        if !self.groupChatusers.contains(userid){
            self.groupChatusers.append(userid)
        }
        if grouptitle.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")) == "" {
            let alert = UIAlertController(title: "Алдаа !", message: "Групп чатын нэр оруулна уу !", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            KVNProgress.show()
            let token = UserDefaults.standard.object(forKey:"token") as! String
            let  header = ["x-access-token": token, "authorization": "BPXjEvWfXxx5qpQHHFMEJEB8fA3xAczC"]
            checkInternet()
            if SettingsViewController.online == true {
                Alamofire.request("\(baseurl)create_group", method: .post, parameters: ["participants":self.groupChatusers,"image":self.pic, "title": self.grouptitle.text!, "owner_id":SettingsViewController.userId], encoding: JSONEncoding.default,headers:header).responseJSON{ response in
                    if response.response?.statusCode == 200 {
                    let res = JSON(response.value!)
//                    print("res::" , res)
                    if res["error"].bool == false {
                        let owner_id_con = res["owner_id"].string!
                        let id = res["_id"].string!
                        let title = res["title"].string!
                        let created_at = res["created_at"].double!
                        let is_thread = res["is_thread"].bool!
                        let isSecret = res["secret"].bool!
                        let image = res["image"].string!
                        let show = res["show"].arrayValue.map({$0.stringValue})
                      
                        let conversation = Conversation.init(user: self.chatuser, lastmessage: Message.init(), id: id, title: title, created_at: created_at, is_thread: is_thread, show: show, secret: isSecret, image: image,owner_id_con: owner_id_con)
                        RealmService.createConversationToRealm(createConversation: conversation, completion: { (success) in
                            if success == true{
                                KVNProgress.dismiss()
                                self.senderchatid = id
                                self.navtitle = title
                                socket.emit("create_group", ["participants":self.groupChatusers, "conversation_id":id])
                                self.chatIsThread = true
                                self.performSegue(withIdentifier: "SingleChatVC", sender: self)
                            }else{
                                KVNProgress.dismiss()
                            }
                            
                        })
                    }else{
                        KVNProgress.dismiss()
                    }
                }
            }
        }else{
//            spiner.stopAnimating()
//            spiner.isHidden = true
//            self.view.layer.opacity = 1
                KVNProgress.dismiss()
            let alert = UIAlertController(title: "Алдаа !", message: "Интернетийн холболтоо шалгана уу !", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
      }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "groupimage" {
            let camera = segue.destination as? CameraView
            camera?.disable = true
//        }else if segue.identifier == "groupchatcreated" {
//            let chatview = segue.destination as? SingleChatVC
//            chatview?.is_thread = true
//            let myid = SettingsViewController.userId
//            var showids = [String]()
//            for show in self.chatuser{
//                if show.id != myid{
//                    showids.append(show.id)
//                }
//                chatview?.showusersid.append(show.id)
//            }
//            chatview?.selectedgroupUser = self.chatuser
//            chatview?.senderchatid = self.senderchatid
//            chatview?.navtitle = [self.grouptitle.text!]
//            SingleChatVC.shows = showids
        }else if segue.identifier == "SingleChatVC"{
            let singlechat = segue.destination as? SingleChatVC
            if chatIsThread == true {
                singlechat?.is_thread = self.chatIsThread
                let myid = SettingsViewController.userId
                var showids = [String]()
                for show in self.chatuser{
                    if show.id != myid{
                        showids.append(show.id)
                    }
                    singlechat?.showusersid.append(show.id)
                }
                singlechat?.selectedgroupUser = self.chatuser
                singlechat?.senderchatid = self.senderchatid
                singlechat?.navtitle = [self.grouptitle.text!]
                SingleChatVC.shows = showids
            }else{
                singlechat?.selectedgroupUser = self.chatuser
                singlechat?.is_thread = self.chatIsThread
                let user = User_group.init(id: filteredContacts[selectedIndex[0]].id, image: filteredContacts[selectedIndex[0]].image, nickname: filteredContacts[selectedIndex[0]].name, lastname: filteredContacts[selectedIndex[0]].lastname, surname: filteredContacts[selectedIndex[0]].surname, phone: filteredContacts[selectedIndex[0]].phone, state: filteredContacts[selectedIndex[0]].state, online_at: filteredContacts[selectedIndex[0]].online_at, fcm_token: filteredContacts[selectedIndex[0]].fcm_token,registerId: filteredContacts[selectedIndex[0]].register)
                singlechat?.selectedgroupUser = [user]
                singlechat?.senderchatid = self.senderchatid
                singlechat?.navtitle = [filteredContacts[selectedIndex[0]].name]
            }
        }
    }
    
    @objc func createPrivateChat(){
        if groupChatusers.count > 2 {
            self.createGroupConversation()
        }else{
            let userid = SettingsViewController.userId
            let baseurl = SettingsViewController.baseurl
            let selecteduserid = filteredContacts[selectedIndex[0]].id
            let token = UserDefaults.standard.object(forKey:"token") as! String
            let  header = ["x-access-token": token, "authorization": "BPXjEvWfXxx5qpQHHFMEJEB8fA3xAczC"]
            checkInternet()
            if SettingsViewController.online == true {
                KVNProgress.show()
                Alamofire.request("\(baseurl)create_private", method: .post, parameters: ["user_id":userid, "participant_id":selecteduserid], encoding: JSONEncoding.default,headers:header).responseJSON{ response in
                    //  print("myid:",userid)
                    //   print("selecteduserid",selecteduserid)
                    KVNProgress.dismiss()
                    if response.response?.statusCode == 200 {
                        let getCon = JSON(response.value!)
                        let error = getCon["error"].bool!
                        if(error == false){
                            let chatid = getCon["_id"].string!
                            self.senderchatid = chatid
                            Conversation.get_singleCon(con_id : chatid, completion: { (con) in
                                
                            })
                            ChatListViewController.nowMessagingConversationID = chatid
                            self.chatIsThread = false
                            self.performSegue(withIdentifier: "SingleChatVC", sender: self)
                        }
                    }
                }
            }else{
                let alert = UIAlertController(title: "Алдаа !", message: "Интернетийн холболтоо шалгана уу ?", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Close", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}
extension String {
    var isAlphanumeric: Bool {
        return !isEmpty && range(of: "[^a-zA-Z]", options: .regularExpression) == nil
    }
}
