//
//  updateCommentVC.swift
//  Mazzi
//
//  Created by Bayara on 11/28/18.
//  Copyright © 2018 Mazzi Application. All rights reserved.
//

import Foundation
import KVNProgress
import Apollo
import AVFoundation
import Photos
import MobileCoreServices
import SwiftyJSON
import Alamofire

    class updateCommentVC:UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
        
        enum AttachmentType: String{
            case camera, photoLibrary
        }

        @IBOutlet weak var zuragView: UIView!
        @IBOutlet weak var removeImg: UIButton!
        var imagePath = ""
        var classId = ""
        var pikedImage : UIImage!
        var postId = ""
        var path = ""
        var userId = SettingsViewController.userId
        var content = ""
        var fileInput : InputPostFile!
        var commentFiles = [PostCommentsQuery.Data.PostComment.File]()
        var tapGesture: UITapGestureRecognizer!
        var teacherId = GlobalStaticVar.teacherId
        @IBOutlet weak var changeImgBtn: UIButton!
        @IBOutlet weak var imgview: UIImageView!
        @IBOutlet weak var textview: UITextView!
        @IBOutlet weak var updateBtn: UIButton!
        @IBOutlet weak var headerView: UIView!
        @IBOutlet weak var backbtn: UIButton!
        
        let apollo: ApolloClient = {
            let configuration = URLSessionConfiguration.default
            // Add additional headers as needed
            configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
            let url = URL(string:SettingsViewController.graphQL)!
            
            return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
        }()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.customize()
            if self.commentFiles.count > 0 {
                self.zuragView.isHidden = false
                let img = self.commentFiles[0].url ?? ""
                self.imagePath = img
                self.imgview.sd_setImage(with:URL(string: "\(SettingsViewController.cdioFileUrl)\(img)"),placeholderImage: UIImage(named: "loading2"))
                self.pikedImage = self.imgview.image
                self.changeImgBtn.setTitle("Зураг солих", for: .normal)
            }else{
                self.zuragView.isHidden = false
                self.imgview.image = UIImage(named: "loading2")
                self.changeImgBtn.setTitle("Зураг оруулах", for: .normal)
            }
        }
 
        func customize(){
            self.title = "Сэтгэгдэл засах"
            self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
            self.view.addGestureRecognizer(tapGesture)
            self.backbtn.addTarget(self, action: #selector(backtoRoot), for: .touchUpInside)
            self.textview.layer.borderColor = GlobalVariables.purple.cgColor
            self.textview.layer.borderWidth = 1
            self.textview.layer.cornerRadius = 5
            self.textview.layer.masksToBounds = true
            self.textview.becomeFirstResponder()
            self.updateBtn.layer.cornerRadius = 5
            self.changeImgBtn.layer.cornerRadius = 5
            self.changeImgBtn.backgroundColor = GlobalVariables.purple
            self.changeImgBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
            self.changeImgBtn.layer.masksToBounds = true
            self.textview.text = content
            self.headerView.frame = CGRect(x:0, y:0 , width: self.view.bounds.width, height: 65)
            self.headerView.backgroundColor = GlobalVariables.todpurple
            self.removeImg.addTarget(self, action: #selector(deleteImg), for: .touchUpInside)
            self.updateBtn.addTarget(self, action: #selector(updateCommWithFile), for: .touchUpInside)
            self.changeImgBtn.addTarget(self, action: #selector(chooseImg), for: .touchUpInside)
          
        }
        
        @objc func deleteImg(){
            let basurl = SettingsViewController.cdioFileUrl
            let headers = SettingsViewController.headers
            if self.imagePath != ""{
                Alamofire.request("\(basurl)deleteImage", method: .post, parameters: ["deletePath":self.imagePath], encoding: JSONEncoding.default,headers:headers).responseJSON{ response in
                    switch response.result {
                    case .success:
                        let res = JSON(response.result.value!)
                        if res["error"] == false {
                            self.imgview.image = UIImage(named: "loading2")
                            self.pikedImage = nil
                            self.changeImgBtn.setTitle("Зураг оруулах", for: .normal)
                            print("deleteImage SUCCESS")
                        }
                    case .failure(let e):
                        
                        print(e)
                    }
                }
            }else{
                self.imgview.image = UIImage(named: "loading2")
                self.pikedImage = nil
                self.changeImgBtn.setTitle("Зураг оруулах", for: .normal)
            }
        }
    
        @objc func backtoRoot(){
            let alert = UIAlertController(title: "", message: "Та сэтгэгдэл засахаа болих гэж байна !", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Тийм", style: .default  , handler: {action in
                self.dismiss(animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "Үгүй", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    
        @objc func dismissKeyboard(){
            self.textview.resignFirstResponder()
        }
        
       @objc func updateCommWithFile(){
            if self.pikedImage != nil {
                if self.imagePath != "" {
                    self.deleteImage(path: self.imagePath)
                }else{
                    uploadImage(teacherId: self.teacherId, image: self.pikedImage, completion: {(state,imageurl) in
                        DispatchQueue.main.async {
                            let str = JSON(imageurl)
                            if !str.isEmpty{
                                let msg = str["error"].boolValue
                                if msg == false{
                                    let url = str["url"].string ?? ""
                                    let ext = str["extension"].string ?? ""
                                    let name = "\("image")\(ext.lowercased())"
                                    let size = str["file_size"].double ?? 0
                                    let thumb = str["thumbnail"].string ?? ""
                                    let imageurl = InputPostFile.init(extension: ext, url: url, name: name, size: size, thumbnail: thumb)
                                    
                                    let post = UpdatePost.init(content: self.textview.text, link: [""], file: [imageurl], topicId: "")
                                    let mutation = UpdatePostMutation(postId: self.postId, classId: self.classId, userId: self.userId, updatePost: post, topic:"")
                                    
                                    self.apollo.perform(mutation: mutation) { result, error in
                                        if result?.data?.updatePost?.error == false{
                                            self.dismiss(animated: true, completion: nil)
                                        }else{
                                            KVNProgress.showError(withStatus: result?.data?.updatePost?.message)
                                        }
                                    }
                                }
                            }
                        }
                    })
                }
            }else{
                let fileInput = [InputPostFile]()
                let post = UpdatePost.init(content: self.textview.text, link: [""], file: fileInput, topicId: "")
                let mutation = UpdatePostMutation(postId: self.postId, classId: self.classId, userId: self.userId, updatePost: post, topic:"")
                
                self.apollo.perform(mutation: mutation) { result, error in
                    if result?.data?.updatePost?.error == false{
                        self.dismiss(animated: true, completion: nil)
                    }else{
                        KVNProgress.showError(withStatus: result?.data?.updatePost?.message)
                    }
                }
            }
        }
        
        func uploadImage( teacherId: String, image:UIImage, completion: @escaping (Bool,Any) -> Swift.Void) {
            let baseurl = SettingsViewController.cdioFileUrl
            let parameters = ["userId":teacherId,"path":self.path]
            let URL = "\(baseurl)fileUpload"
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                multipartFormData.append(image.jpegData(compressionQuality: 0.1)!, withName: "file", fileName: "image\(Int(Date().timeIntervalSince1970)).jpg", mimeType: "jpeg")
                for (key, value) in parameters {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
            }, to:URL)
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (Progress) in
                        //                        let progressPercent = Int(Progress.fractionCompleted*100)
                        //                        KVNProgress.show(withStatus: "Зураг хуулж байна \(progressPercent)%", on: self.view)
                        KVNProgress.show(withStatus: "Зураг хуулж байна. Түр хүлээнэ үү", on: self.view)
                    })
                    upload.responseJSON { response in
                        if let JSON = response.result.value {
                            let res = JSON
                            completion(true, res)
                        }
                        else{
                        }
                    }
                case .failure(let encodingError):
                    print("Message - encode: \(encodingError)")
                }
            }
        }
        
        func deleteImage(path: String!){
            let basurl = SettingsViewController.cdioFileUrl
            let headers = SettingsViewController.headers
            
            Alamofire.request("\(basurl)deleteImage", method: .post, parameters: ["deletePath":path], encoding: JSONEncoding.default,headers:headers).responseJSON{ response in
                switch response.result {
                case .success:
                    let res = JSON(response.result.value!)
                    if res["error"] == false {
                        self.uploadImage(teacherId: self.teacherId, image: self.pikedImage, completion: {(state,imageurl) in
                            DispatchQueue.main.async {
                                let str = JSON(imageurl)
                                if !str.isEmpty{
                                    let msg = str["error"].boolValue
                                    if msg == false{
                                        let url = str["url"].string ?? ""
                                        let ext = str["extension"].string ?? ""
                                        let name = "\("image")\(ext.lowercased())"
                                        let size = str["file_size"].double ?? 0
                                        let thumb = str["thumbnail"].string ?? ""
                                        let imageurl = InputPostFile.init(extension: ext, url: url, name: name, size: size, thumbnail: thumb)
                                        
                                        let post = UpdatePost.init(content: self.textview.text, link: [""], file: [imageurl], topicId: "")
                                        let mutation = UpdatePostMutation(postId: self.postId, classId: self.classId, userId: self.userId, updatePost: post, topic:"")
                                        
                                        self.apollo.perform(mutation: mutation) { result, error in
                                            if result?.data?.updatePost?.error == false{
                                                self.dismiss(animated: true, completion: nil)
                                            }else{
                                                KVNProgress.showError(withStatus: result?.data?.updatePost?.message)
                                            }
                                        }
                                    }
                                }
                            }
                        })
                        print("deleteImage SUCCESS")
                    }
                case .failure(let e):
                    
                    print(e)
                }
            }
        }
        func getSubUser(){
            let query = GetsubUserQuery.init(userId: self.userId)
            apollo.fetch(query: query) { result, error in
                if let path =  result?.data?.subUser?.path{
                    self.path = path
                }
            }
        }
        
        @objc func chooseImg(){
            self.authorisationStatus(attachmentTypeEnum: .photoLibrary, vc: self)
        }
        
        func authorisationStatus(attachmentTypeEnum: AttachmentType, vc: UIViewController){
            
            let status = PHPhotoLibrary.authorizationStatus()
            switch status {
            case .authorized:
                if attachmentTypeEnum == AttachmentType.camera{
                    openCamera()
                }
                if attachmentTypeEnum == AttachmentType.photoLibrary{
                    photoLibrary()
                }
                
            case .denied:
                print("permission denied")
                self.addAlertForSettings(attachmentTypeEnum)
            case .notDetermined:
                print("Permission Not Determined")
                PHPhotoLibrary.requestAuthorization({ (status) in
                    if status == PHAuthorizationStatus.authorized{
                        // photo library access given
                        print("access given")
                        if attachmentTypeEnum == AttachmentType.camera{
                            self.openCamera()
                        }
                        if attachmentTypeEnum == AttachmentType.photoLibrary{
                            self.photoLibrary()
                        }
                        
                    }else{
                        print("restriced manually")
                        self.addAlertForSettings(attachmentTypeEnum)
                    }
                })
            case .restricted:
                print("permission restricted")
                self.addAlertForSettings(attachmentTypeEnum)
            default:
                break
            }
        }
        
        func openCamera(){
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                let myPickerController = UIImagePickerController()
                myPickerController.delegate = self
                myPickerController.sourceType = .camera
                self.present(myPickerController, animated: true, completion: nil)
            }
        }
        
        //MARK: - PHOTO PICKER
        func photoLibrary(){
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                let myPickerController = UIImagePickerController()
                myPickerController.delegate = self
                myPickerController.sourceType = .photoLibrary
                self.present(myPickerController, animated: true, completion: nil)
            }
        }
        
        //MARK: - SETTINGS ALERT
        func addAlertForSettings(_ attachmentTypeEnum: AttachmentType){
            var alertTitle: String = ""
            if attachmentTypeEnum == AttachmentType.camera{
                alertTitle = "App does not have access to your camera. To enable access, tap settings and turn on Camera."
            }
            if attachmentTypeEnum == AttachmentType.photoLibrary{
                alertTitle = "App does not have access to your photos. To enable access, tap settings and turn on Photo Library Access."
            }
            
            let cameraUnavailableAlertController = UIAlertController (title: alertTitle , message: nil, preferredStyle: .alert)
            
            let settingsAction = UIAlertAction(title:"Settings", style: .destructive) { (_) -> Void in
                let settingsUrl = NSURL(string:UIApplication.openSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                }
            }
            let cancelAction = UIAlertAction(title:"Settings", style: .default, handler: nil)
            cameraUnavailableAlertController .addAction(cancelAction)
            cameraUnavailableAlertController .addAction(settingsAction)
            self.present(cameraUnavailableAlertController , animated: true, completion: nil)
        }
}

extension updateCommentVC {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        if let videoURL = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.mediaURL)] as? URL {
            CameraView.visible = 0
            print("videoURL :: ", videoURL)
        }else if let pickedImage1 = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            CameraView.visible = 0
            self.pikedImage = pickedImage1
            self.imgview.image = pickedImage1
            self.changeImgBtn.setTitle("Зураг солих", for: .normal)

        }else if let pickedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as? UIImage {
            CameraView.visible = 0
            self.pikedImage = pickedImage
            self.imgview.image = pickedImage
            self.changeImgBtn.setTitle("Зураг солих", for: .normal)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
        return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
    }
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
        return input.rawValue
    }
}
