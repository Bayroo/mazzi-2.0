//
//  classroomSettingVC.swift
//  Mazzi
//
//  Created by Bayara on 12/7/18.
//  Copyright © 2018 Mazzi Application. All rights reserved.
//

import Foundation
import KVNProgress
import Apollo

class ClassroomSettingVC:UIViewController,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var onlyTeachSwitch: UISwitch!
    @IBOutlet weak var mySwitch2: UISwitch!
    @IBOutlet weak var approveLabel: UILabel!
    @IBOutlet weak var mainTableView: UITableView!
    var posts = [PostApproveQuery.Data.PostsApprove]()
//    var apollo = SettingsViewController.apollo
    var ischecked = false
    var settings = ClassQuery.Data.Class.ClassSetting()
    var myId = SettingsViewController.userId
    var classId = GlobalStaticVar.classId
    var settingsArray = [String]()
    var attachmentCount = 0
    var dateformatter = DateFormatter()
    var restrict = false
    var approve = false
    var selectedIndex = 0
    var selectedSettingsIndex = 0
    var refresher = UIRefreshControl()
    var postids = [String]()
    var isDelete = false
    let apollo: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        // Add additional headers as needed
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
//        print("TOKEN APOLLOO:: ", GlobalVariables.headerToken)
        let url = URL(string:SettingsViewController.graphQL)!
        
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        _ = self.apollo.clearCache()
        print("-------APPEAR----------")
        self.getSettings()
       GlobalStaticVar.isApprovePost = false
    }
    override func viewDidDisappear(_ animated: Bool) {
       
    }
    
    func customize(){
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "05"), style: .plain, target: self, action: #selector(back))
        self.title =  "\("Ангийн код: ")\(GlobalStaticVar.classCode)" //"Тохиргоо"
        settingsArray = ["Хүн бүр нийтлэх","Зөвхөн багш нийтлэх","Бусдын нийтлэлийг шалгаж оруулах"]
        self.mainTableView.delegate = self
        self.mainTableView.dataSource = self
        self.mainTableView.register((UINib(nibName: "SettingCell", bundle: nil)), forCellReuseIdentifier: "settingcell")
        self.mainTableView.register((UINib(nibName: "postsCell", bundle: nil)), forCellReuseIdentifier: "postsCell")
        self.mainTableView.refreshControl = self.refresher
        self.refresher.addTarget(self, action: #selector(reloadData), for: .valueChanged)
        
        let config = KVNProgressConfiguration()
        config.minimumSuccessDisplayTime = 1.0
        KVNProgress.setConfiguration(config)
        
    }
    
    @objc func reloadData(){
        print("------------RELOAD DATA------------")
        self.getSettings()
    }
    
    @objc func back(){
        self.navigationController?.popViewController(animated: true)
         GlobalStaticVar.isPendingPost = false
    }
    
   func saveSettings() {
        let inputclassSetting = InputClassSettings(restrictedPost: self.restrict , approvePost: self.approve)
        let mutation = UpdateClassMutation(classId: GlobalStaticVar.classId, teacherId: GlobalStaticVar.teacherId, name: GlobalStaticVar.className, code: GlobalStaticVar.classCode, inputclassSetting: inputclassSetting)
//        KVNProgress.show(withStatus: "", on: self.view)
        self.apollo.perform(mutation: mutation) { result, error in
//            KVNProgress.dismiss()
            if result?.data?.updateClass != nil {
                if result?.data?.updateClass?.error == false{
                    KVNProgress.showSuccess()
                    if self.approve == true{
                        self.getPosts()
                    }else{
                    }
                    self.mainTableView.reloadData()
//                    self.back()
                }else {
                    KVNProgress.showError(withStatus: result?.data?.updateClass?.message)
                }
            }
        }
    }

    func getSettings(){
        print("------------------------------")
        let query = ClassQuery(classId: GlobalStaticVar.classId)
        KVNProgress.show(withStatus: "", on: self.view)
         apollo.fetch(query: query) { result, error in
            KVNProgress.dismiss()
            if result?.data?.class != nil {
                self.settings = (result?.data?.class?.classSettings)!
                self.restrict = self.settings.restrictedPost!
                self.approve = self.settings.approvePost!
                if self.approve == true && self.restrict == true{
                    self.selectedSettingsIndex = 2
                    self.getPosts()
                }else if self.approve == false && self.restrict == true{
                    self.posts.removeAll()
                    self.selectedSettingsIndex = 1
                }else if self.restrict == false  {
                     self.posts.removeAll()
                    self.selectedSettingsIndex = 0
                }
                print("hehh:: ",self.selectedSettingsIndex)
                self.mainTableView.reloadData()
                self.refresher.endRefreshing()
            }else{
                
            }
        }
    }
    
    func getPosts(){
        _ = self.apollo.clearCache()
         self.posts = []
        let query = PostApproveQuery(userId: self.myId, classId: self.classId, skip: 0, limit: 20)
         KVNProgress.show(withStatus: "", on: self.view)
         apollo.fetch(query: query) { result, error in
            KVNProgress.dismiss()
            if result?.data?.postsApprove != nil {
                self.posts = (result?.data?.postsApprove)! as! [PostApproveQuery.Data.PostsApprove]
//                print(self.posts)
                self.mainTableView.reloadData()
                self.refresher.endRefreshing()
            }else{
                
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.posts.count == 0{
            return 1
        }else{
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return self.settingsArray.count
        }else{
            return self.posts.count
        }
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            self.postids = []
            self.selectedSettingsIndex = indexPath.row
//            print("--------did Select -------")
//            print("SelectedIndex::", self.selectedSettingsIndex)
//            print("Approve: ",self.approve)
//            print("Restrict: ",self.restrict)
            if indexPath.row == 0{
                if self.approve == true && self.restrict == true {
                  self.changeApproveSettings()
                }else{
                    self.approve = false
                    self.restrict = false
                    self.saveSettings()
                }
               
            }else if indexPath.row == 1 {
                if self.approve == true && self.restrict == true {
                    self.changeApproveSettings()
                }else{
                    self.approve = false
                    self.restrict = true
                    self.saveSettings()
                }
               
            }else{
                self.approve = true
                self.restrict = true
                 self.saveSettings()
            }
            self.mainTableView.reloadData()
        }
         else if indexPath.section == 1{
            self.selectedIndex = indexPath.row
            self.performSegue(withIdentifier: "showDetail", sender: self)
        }
    }
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "settingcell", for: indexPath) as! Settingcell
            cell.label.text = self.settingsArray[indexPath.row]
            if selectedSettingsIndex == indexPath.row{
                cell.check.image = UIImage(named: "success")
            }else{
                cell.check.image = UIImage(named: "oval")
            }
            return cell
        }else {
//            print(indexPath.row)
            let cell = tableView.dequeueReusableCell(withIdentifier: "postsCell", for: indexPath) as! postsCell
            cell.initCell()
            cell.filterBtn.isHidden = true
            cell.lastnameLabel.isHidden = true
            var filesCount = 0
            var linksCount = 0
            if self.posts[indexPath.row].file != nil{
                filesCount = (self.posts[indexPath.row].file?.count)!
            }
            if  self.posts[indexPath.row].link != nil{
                linksCount = (self.posts[indexPath.row].link?.count)!
            }
            
            self.attachmentCount = filesCount + linksCount
            
            cell.attachLabel.isHidden = self.attachmentCount == 0 ? true : false
            cell.attachLabel.text = "\(self.attachmentCount) \("Хавсралт")"
            cell.label.text = self.posts[indexPath.row].content ?? ""
            cell.commentLabel.isHidden = self.posts[indexPath.row].commentCount == 0 ? true : false
            cell.commentLabel.text = "\(self.posts[indexPath.row].commentCount!) \("Сэтгэгдэл")"
            let lastlogin = NSDate(timeIntervalSince1970: (self.posts[indexPath.row].postedDate!/1000))
            self.dateformatter.dateStyle = .short
            self.dateformatter.timeStyle = .short
            let last = self.dateformatter.string(from: lastlogin as Date)
            cell.createdLabel.text = last
            let nickname = self.posts[indexPath.row].ownerId!.nickname ?? ""
            let lastname = self.posts[indexPath.row].ownerId!.lastname ?? ""
            cell.nameLabel.text = "\(nickname) \(lastname)"
            let img = self.posts[indexPath.row].ownerId!.image!
            
            if img == ""{
                cell.proImg.image = UIImage(named: "circle-avatar")
            }else{
                cell.proImg.sd_setImage(with:URL(string: "\(SettingsViewController.fileurl)\(img)"),placeholderImage: UIImage(named: "avatar"))
            }
            cell.editBtn.isHidden = false
            cell.editClickbnt.tag = indexPath.row
//            cell.editBtn.addTarget(self, action: #selector(editpost(_:)), for: .touchUpInside)
            cell.editClickbnt.addTarget(self, action: #selector(editpost(_:)), for: .touchUpInside)
            self.attachmentCount = 0
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let title : UILabel = UILabel()
        if section == 0 {
            title.textColor = GlobalVariables.purple
            title.text =  "Тохиргоо"
        }else {
            title.textColor = GlobalVariables.purple
            title.text =  "Хүлээгдэж буй нийтлэлүүд"
        }
        return title.text
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = GlobalVariables.whiteGray
        let headerLabel = UILabel(frame: CGRect(x: 4, y: 5, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height-5))
        headerLabel.font = UIFont.boldSystemFont(ofSize: 16)
        headerLabel.textColor = UIColor.white
        headerLabel.text = self.tableView(self.mainTableView, titleForHeaderInSection: section)
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    @objc func editpost(_ sender: Any){
        print((sender as AnyObject).tag as Any)
        let postId = self.posts[(sender as AnyObject).tag].id
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Нийтлэх", style: .default, handler: { action in
           self.approvePost(postId: postId)
        }))
        alert.addAction(UIAlertAction(title: "Устгах", style: .default, handler:{action in
            self.deletePost(postId: postId)
        }))
        alert.addAction(UIAlertAction(title: "Болих", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    func approvePost(postId: String!){
        print("-------ApprovePost-------------")
        let mutation = ApprovePostMutation(userId: self.myId, postId: postId, classId: self.classId)
        self.apollo.perform(mutation: mutation) { result, error in
//            print("rejectResult:: ", result as Any)
            if result?.data?.approvePost?.error == false{
               self.getSettings()
            }else{
                KVNProgress.showError(withStatus: result?.data?.approvePost?.message)
            }
        }
    }
    
    func deletePost(postId: String!){
        print("-------deletePost----------")
        let mutation = DeletePostMutation(postId: postId, classId: self.classId, userId: self.myId)
        self.apollo.perform(mutation: mutation) { result, error in
//            print("rejectResult:: ", result as Any)
            if result?.data?.deletePost?.error == false{
                self.getSettings()
//                self.mainTableView.reloadData()
            }else{
                KVNProgress.showError(withStatus: result?.data?.deletePost?.message)
            }
        }
    }
    
    func changeApproveSettings(){
        print("------changeApproveSettings-----------")
        if self.posts.count > 0{
            print("IF")
            for i in 0..<self.posts.count{
                if self.postids.count > 0{
                  if  !self.postids.contains(self.posts[i].id!){
                         self.postids.append(self.posts[i].id!)
                    }
                }
            }
            let alert = UIAlertController(title: "Хүлээгдэж байгаа нийтлэлүүдийг нийтлэх үү ?", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Нийтлэх", style: .default, handler: { action in
               self.isDelete = true
               self.deletePendingPosts()
            }))
            alert.addAction(UIAlertAction(title: "Устгах", style: .default, handler:{action in
               self.isDelete = false
               self.deletePendingPosts()
            }))
            alert.addAction(UIAlertAction(title: "Болих", style: .cancel, handler: nil))
            self.present(alert, animated: true)
            for i in 0..<self.posts.count{
                self.postids.append(self.posts[i].id!)
            }
        }else{
//            print("ELSE")
            self.ChangeSettings()
        }
        
    }
    
    func deletePendingPosts(){
        let mutation = ChangeApproveSettingsMutation(userId: self.myId, postIds: self.postids, classId: self.classId, action: self.isDelete)
        self.apollo.perform(mutation: mutation) { result, error in
            print("changeResult:: ", result as Any)
            if result?.data?.changeApproveSettings?.error == false{
                self.ChangeSettings()
                self.reloadData()
                self.mainTableView.reloadData()
            }else{
                KVNProgress.showError(withStatus: result?.data?.changeApproveSettings?.message)
            }
        }
    }
    
    func ChangeSettings(){
//        print("sel ectedINdex:: ",self.selectedIndex)
        if self.selectedSettingsIndex == 0{
            self.approve = false
            self.restrict = false
            self.saveSettings()
        }else if self.selectedSettingsIndex == 1 {
            self.approve = false
            self.restrict = true
            self.saveSettings()
        }else if self.selectedSettingsIndex == 2 {
            self.approve = true
            self.restrict = true
            self.saveSettings()
        }
        self.mainTableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail"{
            let controller = segue.destination as! CommentsVC
            GlobalStaticVar.isPendingPost = true
            controller.classId = self.classId
            controller.postId = self.posts[self.selectedIndex].id ?? ""
            controller.postDate = self.posts[self.selectedIndex].postedDate ?? 0
            controller.postOwnerId = self.posts[self.selectedIndex].ownerId?.id ?? ""
        }
    }
    
}
