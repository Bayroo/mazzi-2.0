//
//  CameraPreview.swift
//  Mazzi
//
//  Created by Janibekm on 10/19/17.
//  Copyright © 2017 Mazzi App LLC. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class CameraPreview: UIViewController {
    
    @IBOutlet weak var previews: UIView!
    @IBOutlet weak var capturedImage: UIImageView!
    @IBOutlet weak var backtocamera: UIButton!
    var videoURL: URL!
    let avPlayer = AVPlayer()
    var avPlayerLayer: AVPlayerLayer!
    var takenPhoto:UIImage?
    
    override func viewDidLoad() {
        
        if  GlobalVariables.isChat {
            self.view.backgroundColor = UIColor.clear
        }else{
             self.view.backgroundColor = UIColor.darkGray
        }
        super.viewDidLoad()
        if let availableImage = takenPhoto {
            self.saveImage.isHidden = false
            capturedImage.image = availableImage
        }else{
           
            self.saveImage.isHidden = true
            avPlayerLayer = AVPlayerLayer(player: avPlayer)
            avPlayerLayer.frame = view.bounds
            avPlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspect
            view.layer.insertSublayer(avPlayerLayer, at: 0)
            view.layoutIfNeeded()
            let playerItem = AVPlayerItem(url: videoURL as URL)
            avPlayer.replaceCurrentItem(with: playerItem)
            avPlayer.play()
        }
    }
    @IBOutlet weak var saveImage: UIButton!
    @IBAction func saveImage(_ sender: Any) {
        guard let imageToSave = takenPhoto else {
            return
        }
        UIImageWriteToSavedPhotosAlbum(imageToSave, nil, nil, nil)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backbtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func sendFile(_ sender: Any) {
        CameraView.visible = 0
        self.dismiss(animated: false, completion: nil)
        
    }
}

