//
//  PreRefillVC.swift
//  Mazzi
//
//  Created by Bayara on 5/24/19.
//  Copyright © 2019 Mazz Application. All rights reserved.
//

import Foundation
import UIKit
import MPNumericTextField
import Apollo
import WebKit

class PreRefillVC: UIViewController,WKNavigationDelegate,WKUIDelegate{
  
    @IBOutlet weak var txtfield: MPNumericTextField!
    @IBOutlet weak var nextBtn: UIButton!
    var myId = SettingsViewController.userId
    var token = SettingsViewController.token
    var webview = WKWebView()
    var key_num = ""
    var trans_number = ""
    var amount = ""
    var lang_ind = 0
    let apollos: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
        let url = URL(string:SettingsViewController.graphQL)!
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    override func viewDidLoad() {
       super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(backtoRoot))
        self.txtfield.isHidden = true
        self.nextBtn.isHidden = true
        self.nextBtn.addTarget(self, action: #selector(gainTransaction), for: .touchUpInside)
        self.webview.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.myId = SettingsViewController.userId
        self.token = SettingsViewController.token
        print("token:: ", self.token)
           self.loaddd()
    }
    
    @objc func backtoRoot(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissKeyboard(){
        self.txtfield.resignFirstResponder()
    }
    
    @objc func gainTransaction(){
        let mutation = GainTransactionMutation(userId: self.myId, amount: self.txtfield.numericValue.intValue)
        self.apollos.perform(mutation: mutation) { result, error in
            if result?.data?.gainTransaction?.error == false{
//                print("RESULT:: ",result?.data?.gainTransaction?.result)
                self.key_num = result?.data?.gainTransaction?.result?.key ?? ""
                self.trans_number = result?.data?.gainTransaction?.result?.transaction?.transactionNumber ?? ""
                let x : Int = result?.data?.gainTransaction?.result?.transaction?.amount ?? 0
                let myString = String(x)
                self.amount =  myString //String(result?.data?.gainTransaction?.result?.transaction?.amount) //self.txtfield.numericValue.intValue
             self.test()
//                print("RESULT:: ",result?.data?.gainTransaction)
            }else{
//                print("ERROR:: ", result?.data?.gainTransaction?.message)
            }
        }
    }
    
    func loaddd(){
        let uurl = URL(string: "https://mclass.mazzi.mn/payment?userId=\(self.myId)&token=\(self.token)")
        let request = URLRequest(url: uurl!)
        self.view.addSubview(self.webview)
        self.webview.load(request)
    }
    
    func test(){
       let url = URL(string: "https://m.egolomt.mn/billingnew/cardinfo.aspx")!
        let req = NSMutableURLRequest(url: url)
        req.httpMethod = "POST"
        req.setValue("application/x-www-form-urlencoded", forHTTPHeaderField:"Content-Type")
        let parameter: [String:Any] = ["key_number":self.key_num, "trans_number":self.trans_number, "trans_amount":self.amount, "lang":self.lang_ind]
        req.setValuesForKeys(parameter)
        self.view.addSubview(self.webview)
        self.webview.load(req as URLRequest)
    }
    
//    func loadTrans(){
//        let url = URL(string: "https://m.egolomt.mn/billingnew/cardinfo.aspx")!
//        var request = URLRequest(url: url)
//        request.httpMethod = "POST"
////        request.setValue("application/json", forHTTPHeaderField:"Accept")
//        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField:"Content-Type")
//        let parameter: [String:Any] = ["key_number":self.key_num, "trans_number":self.trans_number, "trans_amount":self.amount, "lang":self.lang_ind]
//
//        print("--------------------------------------------")
//        print("PARAMS:: ", parameter)
//        print("--------------------------------------------")
//
//        do {
//            request.httpBody = try JSONSerialization.data(withJSONObject: parameter,options: []) // pass dictionary to nsdata object and set it as request body
//            print("BODY::: ", request.value(forHTTPHeaderField: "Content-Type"))
//            self.view.addSubview(self.webview)
//            self.webview.load(request)
//        } catch let _ {
//            print("ALDAAAAAA")
//        }
//
//        let task = URLSession.shared.dataTask(with: request) { data, response, error in
//            guard let data = data,
//                let response = response as? HTTPURLResponse,
//                error == nil else {                                              // check for fundamental networking error
//                    print("error", error ?? "Unknown error")
//                    return
//            }
//
//            guard (200 ... 299) ~= response.statusCode else {                    // check for http errors
//                print("statusCode should be 2xx, but is \(response.statusCode)")
//                print("response = \(response)")
//                return
//            }
//
//             let responseString = String(data: data, encoding: .utf8)
////            print("responseString = \(responseString)")
//        }
//        task.resume()
//    }
    
}
