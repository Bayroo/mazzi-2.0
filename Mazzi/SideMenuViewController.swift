//
//  SideMenuViewController.swift
//  Mazzi App LLCChatTestV
//
//  Created by Janibekm on 8/23/17.
//  Copyright © 2017 Janibekm. All rights reserved.
//

import UIKit
//zasvar
import RealmSwift
import SwiftyJSON
import KVNProgress
import Alamofire
import SideMenu
import StoreKit
import AVFoundation

class SideMenuViewController: UIViewController, myProtocol{
    var items:[NSDictionary] = []
    let fileurl = SettingsViewController.fileurl
    var image = ""
    @IBOutlet weak var menuBackGround: UIImageView!
    @IBOutlet weak var headerBack: UIView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var proImg: UIImageView!
    @IBOutlet weak var aboutBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var exitBtn: UIButton!
    @IBOutlet weak var copyRightLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var rateBtn: UIButton!
    @IBOutlet weak var surveyBtn: UIButton!
    
    var apollo = SettingsViewController.apollo
    var birthday:Double = 0
    var lastname = ""
    var surname = ""
    var email = ""
    var password = ""
    var registerId = ""
    var visible = 0
    var complete = false
    let userDefaults = UserDefaults.standard
    var gender = ""
    var displayphone = ""
    var region = ""
    var key = ""
    var userid = ""
    var is_confirmed = true
    var fcm_token = ""
    var userId = SettingsViewController.userId
    @IBOutlet weak var genderIcon: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.proImg.layer.borderColor = GlobalVariables.purple.cgColor
        self.proImg.layer.borderWidth = 0.5
        self.username.textColor = GlobalVariables.purple
        self.phone.textColor = GlobalVariables.purple
        self.aboutBtn.setTitleColor(GlobalVariables.purple, for: UIControl.State.normal)
        self.surveyBtn.setTitleColor(GlobalVariables.purple, for: UIControl.State.normal)
        self.shareBtn.setTitleColor(GlobalVariables.purple, for: UIControl.State.normal)
        self.exitBtn.setTitleColor(GlobalVariables.purple, for: UIControl.State.normal)
        self.rateBtn.setTitleColor(GlobalVariables.purple, for: UIControl.State.normal)
        self.copyRightLabel.textColor = GlobalVariables.purple
        self.companyLabel.textColor = GlobalVariables.purple
        self.versionLabel.textColor = GlobalVariables.purple
        let text = Bundle.main.infoDictionary?["CFBundleShortVersionString"]  as? String
        self.versionLabel.text = "Хувилбар \(text!)"
        self.view.backgroundColor = GlobalVariables.todpurple
//        self.getprofile()
        items = [
                ["id":1,"title":"Хувийн мэдээлэл","description": "", "img":"editbtn-1"],
                ["id":2,"title":"Маззи","description": "", "img":"help"],
                ["id":3,"title":"Share","description": "", "img":"share"],
                ["id":4,"title":"Гарах","description": "", "img":"exit"]
        ]
    }
    
    @IBAction func ProfileImgChange(_ sender: Any) {
 
        let authorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authorizationStatus {
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video,completionHandler: { (granted:Bool) -> Void in
                if granted {
                    SettingsViewController.cameraAccess = true
                     self.performSegue(withIdentifier: "camerView", sender: self)
                }
                else {
                    SettingsViewController.cameraAccess = false
                    let alert = UIAlertController(title: "", message: "Утасныхаа тохиргоо руу орж камер ашиглах боломжийг олгоно уу !", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            })
        case .authorized:
            SettingsViewController.cameraAccess = true
            self.performSegue(withIdentifier: "camerView", sender: self)
        case .denied, .restricted:
            SettingsViewController.cameraAccess = false
            let alert = UIAlertController(title: "", message: "Утасныхаа тохиргоо руу орж камер ашиглах боломжийг олгоно уу !", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getprofile(){
        
        let userInfo = userDefaults.dictionary(forKey: "userInfo")
        image = userInfo!["image"] as! String
        if image == "" {
            self.proImg.image = UIImage(named: "avatarr")
        }else{
            self.proImg.sd_setImage(with: URL(string:"\(fileurl)\(image)"), completed: nil)
        }
        let name = userInfo!["nickname"] as! String
        let phone = userInfo!["displayphone"] as! String
        self.username.text = name
        self.phone.text = phone ///userDefaults.object(forKey: "phone") as? String
        self.gender = userInfo!["gender"] as! String
        self.lastname = userInfo!["lastname"] as! String
        self.birthday = ( userInfo!["birthday"] as! Double )/1000
        self.email = userInfo!["email"] as! String
        self.region = userInfo!["region"] as! String
        if(userDefaults.object(forKey: "fcm_token") != nil){
            fcm_token = (userDefaults.object(forKey: "fcm_token") as? String)!
        }
        self.registerId = (userInfo!["registerId"] as? String)!
        is_confirmed = userInfo!["is_confirmed"] as! Bool
        
        if self.gender == "male" {
            self.genderIcon.image = UIImage(named: "male")
        }else{
            self.genderIcon.image = UIImage(named: "female")
        }
    }
    
    func logout(){
//        print("LOGGEDOUT_USERID :: ",self.userId)
        let baseurl = SettingsViewController.baseurl
        Alamofire.request("\(baseurl)logout", method: .post, parameters: ["user_id": self.userId], encoding: JSONEncoding.default).responseJSON
            { response in
                switch response.result {
                case .success:
                    let res = JSON(response.result.value!)
                    print("logout sidemenu: ",res)
                    self.dismiss(animated: true, completion: nil)
                    if res["error"].boolValue == false{
                        print("LOGOUT SUCCESS")
                    }
                case .failure(let e):
                    print(e)
                }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        print("MENU")
        getprofile()
        self.menuBackGround.backgroundColor = GlobalVariables.todpurple
    }

    @IBAction func rateMe(_ sender: Any) {
         SKStoreReviewController.requestReview()
    }
    
    @IBAction func editpro(_ sender: Any) {
         self.performSegue(withIdentifier: "editpro", sender: self)
    }
    
    @IBAction func goMazziAbout(_ sender: Any) {
        self.performSegue(withIdentifier: "toAbout", sender: self)
    }
    
    @IBAction func goShareBtn(_ sender: UIButton) {
        let textToShare = "Маззи апп-г татаарай"
        if let myWebsite = NSURL(string: "https://mazzi.mn/"){
            let objectsToShare = [textToShare,myWebsite] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
            activityVC.popoverPresentationController?.sourceView = sender
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func feedback(_ sender: Any) {
        self.performSegue(withIdentifier: "survey", sender: self)
    }
    
    @IBAction func goLogout(_ sender: Any) {
       
        let alert = UIAlertController(title: "Системээс гарах", message: "Та маззи системээс гарахдаа итгэлтэй байна уу?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction) -> Void in
            let userDefaultsData = UserDefaults.standard
            
            //userDefaultsData.removeObject(forKey: "phone")
            userDefaultsData.removeObject(forKey: "isLogin")
            userDefaultsData.removeObject(forKey: "key")
            userDefaultsData.removeObject(forKey: "user_id")
            userDefaultsData.removeObject(forKey: "token")
            userDefaultsData.removeObject(forKey: "fcm_token")
            userDefaultsData.removeObject(forKey: "userInfo")
            userDefaultsData.removeObject(forKey: "keyboardheight")
            userDefaultsData.removeObject(forKey: "authVerificationID")
            GlobalVariables.headerToken = ""
            GlobalVariables.user_id = ""
            SettingsViewController.userId = ""
            
            RealmService.deleteAllRealm(completion: { (suc) in
                _ = self.apollo.clearCache()
              
                self.navigationController?.popToRootViewController(animated: false)
                self.logout()

            })
        }))
        alert.addAction(UIAlertAction(title: "Цуцлах", style: UIAlertAction.Style.cancel, handler: nil))
         self.present(alert, animated: true, completion: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editpro" {
            let tokey = segue.destination as? CreateProfileController
            tokey?.visible = 1
        } else  if segue.identifier == "camerView" {
            GlobalVariables.isChat = false
            let camera = segue.destination as? CameraView
            camera?.disable = true
            camera?.myProtocol = self
        }
    }
    
    func photoGet(capturedImage: UIImage) {

        let userid = userDefaults.object(forKey: "user_id") as? String
        KVNProgress.show()
        Message.uploadimage(chatId: userid! ,image: capturedImage, completion: { (state, imageurl) in
            DispatchQueue.main.async {
                let url = JSON(imageurl)
                self.image = url["url"].string!
                let imageurl = URL(string:"\(self.fileurl)\(url["url"].string!)")!
                self.proImg.sd_setImage(with: imageurl, completed: { (image, error, cacheType, url) in
                    self.proImg.sd_setImage(with: imageurl, completed: nil)
                })
                KVNProgress.dismiss()
                self.updateProfile()
            }
        })
    }
    
    func updateProfile(){
        let userInfo = userDefaults.dictionary(forKey: "userInfo")
        self.displayphone = userInfo!["displayphone"] as! String
        self.userid = (userDefaults.object(forKey: "user_id") as? String)!
        self.password = userDefaults.object(forKey: "pass") as! String
        let profile = ["_id":userid,"image":image,"nickname": self.username.text!, "phone": self.displayphone, "key": key, "region": region, "lastname": self.lastname, "birthday": self.birthday,"email":email, "gender":gender,"fcm_token":fcm_token,"registerId":registerId,"password":password] as [String : Any]
        if self.image != "" {
            KVNProgress.show()
            SettingsViewController.userId = self.userid
            GlobalVariables.user_id = self.userid
            User_group.updateprofile(profile: profile, userids: userid, completion: { (status) in
                if status == true {
                    DispatchQueue.main.async{
                        let userInfo = ["image": self.image,
                                        "nickname":self.username.text!,
                                        "lastname":self.lastname,
                                        "birthday":self.birthday,
                                        "email":self.email,
                                        "gender":self.gender,
                                        "region":self.region,
                                        "displayphone": self.displayphone,
                                        "is_confirmed": self.is_confirmed,
                                        "registerId": self.registerId,
                                        ] as [String : Any]
                        UserDefaults.standard.set(userInfo, forKey: "userInfo")
                        self.userDefaults.synchronize()
                    }
                    let when = DispatchTime.now() + 3
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        if self.visible == 1 {
                            self.viewDidLoad()
                            self.dismiss(animated: true, completion: nil)
                        }else{
                          print(self.image)
                          self.getprofile()
                        }
                        KVNProgress.dismiss()
                    }
                }else{
                    print("CreateProfileController - profile update failed from server")
                    self.dismiss(animated: true, completion: nil)
                }
            })
        }else{
            print("CreateProfileController field empty")
            let alert = UIAlertController(title: "Алдаа !", message: "Интернетийн холболтоо шалгана уу!", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Close", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func videoGet(clipedVideo: URL) {
        print("video didnt support")
    }
}
