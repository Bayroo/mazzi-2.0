//
//  RealmService.swift
//  Mazzi
//
//  Created by Janibekm on 12/21/17.
//  Copyright © 2017 Mazzi App LLC. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class RealmService {
    private init() {}
    static let service = RealmService()
    //var realm = try! Realm()
    
  
    class func deleteAllRealm( completion: @escaping (Bool) -> Swift.Void) {
        GlobalStaticVar.AppBadgeCount = 0
        getBadgeCount()
        autoreleasepool {
            do{
                try realm.write {
                    realm.deleteAll()
                    completion(true)
                }
            }catch{
                print(error)
                completion(false)
            }
        }
    }
    
    class func writeContactToRealm( installedContacts: Contacts, completion: @escaping (Bool) -> Swift.Void) {
        let  mynumber = (userDefaults.object(forKey: "myNumber") as? String)!
     //  print(installedContacts.phone)
        if installedContacts.phone != mynumber{
            
             let realmContact = Contacts_realm()
            realmContact.id = installedContacts.id
            realmContact.name = installedContacts.name
            realmContact.lastname = installedContacts.lastname
            realmContact.surname = installedContacts.surname
            realmContact.image = installedContacts.image
            realmContact.phone = installedContacts.phone
            for p in 0..<installedContacts.phones.count{
                let phoneids = Phones_p()
                phoneids.id = "\(installedContacts.id)\(installedContacts.phones[p])"
                phoneids.phones = installedContacts.phones[p]
                realmContact.phones.append(phoneids)
                
            }
            realmContact.gender = installedContacts.gender
            realmContact.birthday =  installedContacts.birthday
            realmContact.state =  installedContacts.state
            realmContact.online_at = installedContacts.online_at
            realmContact.created_at = installedContacts.created_at
            realmContact.fcm_token = installedContacts.fcm_token
            
            autoreleasepool {
                do{
                    try realm.write {
                        realm.add(realmContact, update: true)
                        completion(true)
                    }
                }catch{
                    print(error)
                    completion(false)
                }
            }
        }
    }

    class func DeleteContactRealm(deleteContact: String, completion:@escaping (Bool) -> Swift.Void){
        print("DELETE CONTACT:: ", deleteContact)
        let deleteObjs = realm.objects(Contacts_realm.self).filter("phone = '\(deleteContact)'")
            autoreleasepool {
                do{
                    try realm.write {
                        realm.delete(deleteObjs)
                    
                        completion(true)
                    }
                }catch{
                    completion(false)
                }
            }
    }
    
    class func DeleteEmptyPhoneContactRealm(deleteContact: String, completion:@escaping (Bool) -> Swift.Void){
        print("deleteCOntact")
        let deleteObjs = realm.objects(Contacts_realm.self).filter("name = '\(deleteContact)'")
        autoreleasepool {
            do{
                try realm.write {
                    realm.delete(deleteObjs)
                    completion(true)
                }
            }catch{
                completion(false)
            }
        }
    }
    
    class func updateContactRealm(updateContacts: Contacts, completion: @escaping (Bool) -> Swift.Void){
        let find = realm.object(ofType: Contacts_realm.self, forPrimaryKey: updateContacts.id)
        if find != nil{
            let singleContact = realm.objects(Contacts_realm.self).filter("id = '\(updateContacts.id)'")
            let contact = Contacts_realm()
            contact.id = singleContact[0].id
            contact.name = updateContacts.name
            contact.lastname = updateContacts.lastname
            contact.surname = updateContacts.surname
            contact.birthday = updateContacts.birthday
            contact.created_at = updateContacts.created_at
            contact.gender = updateContacts.gender
            contact.online_at = updateContacts.online_at
            contact.phone = updateContacts.phone
            for ph in updateContacts.phones{
                let newPhones = Phones_p()
                newPhones.id = "\(singleContact[0].id)\(ph)"
                newPhones.phones = ph
                contact.phones.append(newPhones)
            }
            contact.image = updateContacts.image
            contact.state = updateContacts.state
            contact.fcm_token = updateContacts.fcm_token
            autoreleasepool {
                do{
                    try realm.write {
                        realm.add(contact, update: true)
                        completion(true)
                    }
                }catch{
                    print(error)
                    completion(false)
                }
            }
        }
    }
    class func createConversationToRealm(createConversation: Conversation,completion:@escaping (Bool) -> Swift.Void){
        let conversation = Conversation_realm()
        conversation.id = createConversation.id
        conversation.image = createConversation.image
        conversation.is_thread = createConversation.is_thread
        conversation.secret = createConversation.secret
        conversation.title = createConversation.title
        conversation.created_at = createConversation.created_at
        conversation.updated_at = createConversation.created_at
        conversation.owner_id_con = createConversation.owner_id_con
        for create in createConversation.show{
            let showid = Show_id()
            showid.id = "\(createConversation.id)\(create)"
            showid.conversation_id = createConversation.id
            showid.show = create
            conversation.show.append(showid)
        }
        for users in createConversation.user{
            let userid = User_group_realm()
            userid.id = users.id
            userid.lastname = users.lastname
            userid.nickname = users.nickname
            userid.surname = users.surname
            userid.online_at = users.online_at
            userid.phone = users.phone
            userid.image = users.image
            userid.state = users.state
            userid.fcm_token = users.fcm_token
            conversation.user.append(userid)
        }
        if createConversation.lastmessage.id != ""{
            let message = Message_realm()
            message.id = createConversation.lastmessage.id
            message.content = createConversation.lastmessage.content
            //zasvar
            message.filename = createConversation.lastmessage.filename
            message.file_size = createConversation.lastmessage.file_size
            
            message.conversation_id = createConversation.id
            message.created_at = createConversation.lastmessage.created_at
            message.duration = createConversation.lastmessage.duration
            //message.owner = createConversation.lastmessage.owner
            message.owner_id = createConversation.lastmessage.owner_id
            for s in createConversation.lastmessage.seen{
                let seenid = Seen_id()
                seenid.conversation_id = conversation.id
                seenid.id = "\(createConversation.lastmessage.id)\(s)"
                seenid.seen = s
                message.seen.append(seenid)
            }
            message.type = createConversation.lastmessage.type
            conversation.updated_at = createConversation.lastmessage.created_at
            conversation.lastmessage = message
        }else{
            conversation.lastmessage = nil
        }
        if createConversation.is_thread == true {
//            print("groupchat mun  bn")
            var findConversation = realm.object(ofType: Conversation_realm.self, forPrimaryKey: conversation.id)
            if findConversation != nil {
                autoreleasepool {
                    do{
                        try realm.write {
                            findConversation?.show.removeAll()
                            findConversation = conversation
                            realm.add(findConversation!, update: true)
                            completion(true)
                        }
                    }catch{
                        completion(false)
                    }
                }
            }else{
                autoreleasepool {
                    do{
                        try realm.write {
                            realm.add(conversation, update: true)
                            completion(true)
                        }
                    }catch{
                        completion(false)
                    }
                }
            }
        }else{
            autoreleasepool {
                do{
                    try realm.write {
                        realm.add(conversation, update: true)
                        completion(true)
                    }
                }catch{
                    completion(false)
                }
            }
        }
    }
    class func updateConversationToRealm(udpateConversation: Conversation,completion:@escaping (Bool) -> Swift.Void){
        let find = realm.object(ofType: Conversation_realm.self, forPrimaryKey: udpateConversation.id)
        if find != nil{
            let conversation = Conversation_realm()
            let updateConversation = realm.objects(Conversation_realm.self).filter("id = '\(udpateConversation.id)'")
            conversation.id = updateConversation[0].id
            conversation.image = udpateConversation.image
            conversation.is_thread = udpateConversation.is_thread
            conversation.secret = udpateConversation.secret
            conversation.title = udpateConversation.title
            conversation.created_at = udpateConversation.created_at
            conversation.updated_at = udpateConversation.created_at
            conversation.owner_id_con = udpateConversation.owner_id_con
            for create in udpateConversation.show{
                let showid = Show_id()
                showid.id = "\(udpateConversation.id)\(create)"
                showid.conversation_id = udpateConversation.id
                showid.show = create
                conversation.show.append(showid)
            }
            for users in udpateConversation.user{
                let userid = User_group_realm()
                userid.id = users.id
                userid.lastname = users.lastname
                userid.nickname = users.nickname
                userid.surname = users.surname
                userid.online_at = users.online_at
                userid.phone = users.phone
                userid.image = users.image
                userid.state = users.state
                userid.fcm_token = users.fcm_token
                conversation.user.append(userid)
            }
            if udpateConversation.lastmessage.id != ""{
                let message = Message_realm()
                message.id = udpateConversation.lastmessage.id
                message.content = udpateConversation.lastmessage.content
                //zasvar
                message.filename = udpateConversation.lastmessage.filename
                message.file_size = udpateConversation.lastmessage.file_size
                
                message.conversation_id = udpateConversation.id
                message.created_at = udpateConversation.lastmessage.created_at
                message.duration = udpateConversation.lastmessage.duration
                message.owner_id = udpateConversation.lastmessage.owner_id
                for s in udpateConversation.lastmessage.seen{
                    let seenid = Seen_id()
                    seenid.conversation_id = conversation.id
                    seenid.id = "\(udpateConversation.lastmessage.id)\(s)"
                    seenid.seen = s
                    message.seen.append(seenid)
                }
                message.type = udpateConversation.lastmessage.type
                conversation.updated_at = udpateConversation.lastmessage.created_at
                conversation.lastmessage = message
            }else{
                conversation.lastmessage = nil
            }
            autoreleasepool {
                do{
                    try realm.write {
                        realm.add(conversation, update: true)
                        completion(true)
                    }
                } catch {
                    print(error)
                    completion(false)
                }
                realm.refresh()
            }
        }
    }
    
    class func deleteConversationFromRealm(deleteConversation: String,completion:@escaping (Bool) -> Swift.Void){
        let find = realm.object(ofType: Conversation_realm.self, forPrimaryKey: deleteConversation)
        if find != nil{
            let deleteObjs = realm.objects(Conversation_realm.self).filter("id = '\(deleteConversation)'")
            autoreleasepool {
                do{
                    try realm.write {
                        realm.delete(deleteObjs)
                        completion(true)
                    }
                }catch{
                    completion(false)
                }
            }
        }
    }
    
    class func createMessageToRealm(createMessage: Message,conversation_id:String,completion:@escaping (Bool) -> Swift.Void){
        let message = Message_realm()
        message.id = createMessage.id
        message.content = createMessage.content
        //zasvar
        message.filename = createMessage.filename
        message.file_size = createMessage.file_size
        
        message.owner_id = createMessage.owner_id
        message.type = createMessage.type
        //message.owner = createMessage.owner
        message.id = createMessage.id
        message.owner_id = createMessage.owner_id
        message.created_at = createMessage.created_at
        message.conversation_id = conversation_id
        for seens in createMessage.seen{
            let newseen = Seen_id()
            newseen.id = "\(createMessage.id)\(seens)"
            newseen.conversation_id = conversation_id
            newseen.seen = seens
            message.seen.append(newseen)
        }
        autoreleasepool {
            do{
                try realm.write {
                    realm.add(message, update: true)
                    completion(true)
                }
            }catch{
                print(error)
                completion(false)
            }
        }
    }
    class func updateMessageToRealm(updateMessageID: String, completion:@escaping (Bool) -> Swift.Void){
        let find = realm.object(ofType: Message_realm.self, forPrimaryKey: updateMessageID)
        if find != nil{
            let updateMessage = realm.objects(Message_realm.self).filter("id = '\(updateMessageID)'")
            let message = Message_realm()
            let userid = SettingsViewController.userId
            message.id = updateMessage[0].id
            if updateMessage[0].seen.contains(where: {$0.seen != userid}){
                let seenid = Seen_id()
                seenid.id = "\(updateMessage[0].id)\(userid)"
                seenid.seen = userid
                seenid.conversation_id = updateMessage[0].conversation_id
                message.seen.append(seenid)
            }
            message.content = updateMessage[0].content
            //zasvar
            message.filename = updateMessage[0].filename
            message.file_size = updateMessage[0].file_size
            
            message.owner_id = updateMessage[0].owner_id
            message.type = updateMessage[0].type
//            message.owner = updateMessage[0].owner
            message.owner_id = updateMessage[0].owner_id
            message.created_at = updateMessage[0].created_at
            message.conversation_id = updateMessage[0].conversation_id
            autoreleasepool {
                do{
                    try realm.write {
                        realm.add(message, update: true)
                        completion(true)
                    }
                }catch{
                    completion(false)
                }
            }
        }
    }
    
    class func deleteMessages(deleteMessageParentId: String, completion:@escaping (Bool) -> Swift.Void){
        let deleteObjs = realm.objects(Message_realm.self).filter("conversation_id = '\(deleteMessageParentId)'")
        if deleteObjs.count != 0{
            autoreleasepool {
                do{
                    try realm.write {
                        realm.delete(deleteObjs)
                        completion(true)
                    }
                }catch{
                    completion(false)
                }
            }
        }
    }
    class func deleteMsg(deleteMsgId: String, completion:@escaping (Bool) -> Swift.Void){
        let deleteObjs = realm.objects(Message_realm.self).filter("id = '\(deleteMsgId)'")
        print("deleteObjs:: ", deleteObjs)
        if deleteObjs.count != 0{
            autoreleasepool {
                do{
                    try realm.write {
                        realm.delete(deleteObjs)
                        completion(true)
                    }
                }catch{
                    completion(false)
                }
            }
        }
    }
    class func deleteMessageSeenObjs(deleteMessgeSeenObjByConid: String, completion:@escaping (Bool) -> Swift.Void){
        let deleteSeenObjs = realm.objects(Seen_id.self).filter("conversation_id = '\(deleteMessgeSeenObjByConid)'")
        if deleteSeenObjs.count != 0 {
            autoreleasepool {
                do{
                    try realm.write {
                        realm.delete(deleteSeenObjs)
                        completion(true)
                    }
                }catch{
                    completion(false)
                }
            }
        }
    }
    class func updateUserGroupRealm(updateUserGroupUser: User_group, completion:@escaping (Bool) -> Swift.Void){
        let find = realm.object(ofType: User_group_realm.self, forPrimaryKey: updateUserGroupUser.id)
        if find != nil{
            //let updateUserGroup = realm.objects(User_group_realm.self).filter("id = '\(updateUserGroupUser.id)'")
            let user = User_group_realm()
            user.id = updateUserGroupUser.id
            user.lastname = updateUserGroupUser.lastname
            user.nickname = updateUserGroupUser.nickname
            user.online_at = updateUserGroupUser.online_at
            user.phone = updateUserGroupUser.phone
            user.image = updateUserGroupUser.image
            user.state = updateUserGroupUser.state
            user.surname = updateUserGroupUser.surname
            user.fcm_token = updateUserGroupUser.fcm_token
            autoreleasepool {
                do{
                    try realm.write {
                        realm.add(user, update: true)
                        completion(true)
                    }
                }catch{

                    completion(false)
                }
            }
        }
    }
//    class func createStickersRealm(createStickers: Stickers, completion:@escaping (Bool) -> Swift.Void){
//        let newSticker = Stickers_realm()
//        newSticker.id = createStickers.id
//        newSticker.name = createStickers.name
//        newSticker.category = createStickers.category
//        newSticker.price = createStickers.price
//        newSticker.created_at = createStickers.created_at
//        newSticker.download_at = createStickers.downloaded_at
//        newSticker.is_active = createStickers.is_active
//        newSticker.downloaded = createStickers.downloaded
//        newSticker.thumbnail_url = createStickers.thumbnail_url
//        newSticker.zip_url = createStickers.zip_url
//        for img in createStickers.images{
//            let newStIMG = StickerImages_realm()
//            newStIMG.id = "\(createStickers.id)\(img.name)"
//            newStIMG.name = img.name
//            newStIMG.path = img.path
//            newSticker.images.append(newStIMG)
//        }
//        autoreleasepool {
//            do{
//                try realm.write {
//                    realm.add(newSticker, update: true)
//                    completion(true)
//                }
//            }catch{
//                completion(false)
//            }
//        }
//    }
    
    class func createStickersCatRealm(createStickers: StickersCat, completion:@escaping (Bool) -> Swift.Void){
        let newSticker = StickersCatRealm()
        newSticker.id = createStickers._id
        newSticker.name = createStickers.name
        newSticker.price = createStickers.price
        newSticker.thumb = createStickers.thumb
        for img in createStickers.stickers{
            let newStIMG = StickerImagesRealm()
            newStIMG.id = "\(createStickers._id)\(img.name)"
            newStIMG.name = img.name
            newStIMG.path = img.path
            newStIMG.categoryId = img.categoryId
            newStIMG.is_active = img.is_active
            newSticker.stickers.append(newStIMG)
        }
        autoreleasepool {
            do{
                try realm.write {
                    realm.add(newSticker, update: true)
                    completion(true)
                }
            }catch{
                completion(false)
            }
        }
    }
    
    func post(_ error:Error){
        NotificationCenter.default.post(name:Notification.Name("RealmError"),object:error)
    }
    func observeRealmErrors(in vc: UIViewController, completion: @escaping (Error?) -> Void ){
        NotificationCenter.default.addObserver(forName: NSNotification.Name("RealmError"), object: nil, queue: nil) { (notification) in
            completion(notification.object as? Error)
        }
    }
    func stopObservingErrors(in vc:UIViewController){
        NotificationCenter.default.removeObserver(vc, name: NSNotification.Name("RealmError"), object: nil)
    }
}

func bundleURL(_ name: String) -> URL? {
    return Bundle.main.url(forResource: name, withExtension: "realm")
}

final class DataStore {
    static let currentSchemaVersion: UInt64 = 4
    static func configureMigration() {
    
        let defaultURL = Realm.Configuration.defaultConfiguration.fileURL!
        //let defaultParentURL = defaultURL.deletingLastPathComponent()
        if let v0URL = bundleURL("default") {
            do {
                try FileManager.default.removeItem(at: defaultURL)
                try FileManager.default.copyItem(at: v0URL, to: defaultURL)
            } catch {
                
            }
        }
        let migrationBlock: MigrationBlock = { migration, oldSchemaVersion in
            if oldSchemaVersion < 1 {
                
            }
            if oldSchemaVersion < 2 {
                //migration.enumerateObjects(ofType: Person.className()) { oldObject, newObject in}
                migration.enumerateObjects(ofType: StickersCatRealm.className(), { (oldclass, newclass) in
                    print("realm new class added during schema ver1 to 2")
                })
                migration.enumerateObjects(ofType: StickerImagesRealm.className(), { (oldclass, newclass) in
                    print("realm stickerimages_realm clss added during schema ver1 to 2")
                })
                let userDefaults = UserDefaults.standard
                userDefaults.removeObject(forKey: "phone")
                userDefaults.synchronize()
            }
            if oldSchemaVersion < 3 {
                //migration.enumerateObjects(ofType: Person.className()) { oldObject, newObject in}
                migration.enumerateObjects(ofType: User_group_realm.className(), { (oldclass, newclass) in
                    print("realm new field fcm_token added during schema ver2 to 3")
                    newclass!["fcm_token"] = ""
                })
                migration.enumerateObjects(ofType: Contacts_realm.className(), { (oldclass, newclass) in
                    print("realm contacts_realm fcm_token clss added during schema ver2 to 2")
                    newclass!["fcm_token"] = ""
                })
                
                let userDefaults = UserDefaults.standard
                userDefaults.removeObject(forKey: "phone")
                userDefaults.synchronize()
            }
            
            if oldSchemaVersion < 4 {
                migration.enumerateObjects(ofType: Conversation_realm.className(), { (oldclass, newclass) in
                    print("realm new field fcm_token added during schema ver3 to 4")
                    newclass!["owner_id_con"] = ""
                })
            }
            print("Migration complete.")
        }
        Realm.Configuration.defaultConfiguration = Realm.Configuration(schemaVersion: 4, migrationBlock: migrationBlock)
    }
    
 
}

