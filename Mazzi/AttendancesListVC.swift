//
//  AttendancesListVC.swift
//  Mazzi
//
//  Created by Bayara on 1/10/19.
//  Copyright © 2019 woovoo. All rights reserved.
//

import Foundation
import Apollo
import KVNProgress

class AttendancesListVC: UIViewController,UITableViewDelegate , UITableViewDataSource{
  
    var beginTimee : Double!
    var endTimee  : Double!
    var radius : Double!
    var date : Double!
    var type = ""
    var code = ""
    var classTime : Double!
    var classId = GlobalStaticVar.classId
    var myId = SettingsViewController.userId
    let refreshControl = UIRefreshControl()
    var lists = [AttendancesQuery.Data.Attendance]()
    var topics = [AttendancesQuery.Data.Attendance.TopicId]()
    var endTime : Double!
    var selectedAttendanceId = ""
    var dateformatter = DateFormatter()
    @IBOutlet weak var tableview: UITableView!
    
    var apollo: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        // Add additional headers as needed
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
        //        print("TOKEN APOLLOO:: ", GlobalVariables.headerToken)
        let url = URL(string:SettingsViewController.graphQL)!
        
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getList()
        self.code = ""
        self.type = ""
    }
    
    override func viewWillDisappear(_ animated: Bool) {
       _ =  self.apollo.clearCache()
    }

    func customize(){
        self.title = "Ирцийн жагсаалт"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(back))
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.tableview.delegate = self
        self.tableview.dataSource = self
        self.tableview.register((UINib(nibName: "attListCel", bundle: nil)), forCellReuseIdentifier: "attlist")
        self.tableview.refreshControl = self.refreshControl
        self.refreshControl.addTarget(self, action: #selector(reloadData), for: .valueChanged)
    }
    
    @objc func back(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.lists.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedAttendanceId = self.lists[indexPath.row].id ?? ""
        self.endTime = self.lists[indexPath.row].time?.timeEnds
        
        self.code = self.lists[indexPath.row].code ?? ""
        self.type = self.lists[indexPath.row].__typename
        
        self.performSegue(withIdentifier: "list", sender: self)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "attlist", for: indexPath) as! AttlistCell
        if indexPath.row % 2 == 0{
            cell.backgroundColor = GlobalVariables.F5F5F5
        }else{
            cell.backgroundColor = UIColor.white
        }
        
        if self.lists[indexPath.row].topicId != nil {
            cell.codeLabel.text = self.lists[indexPath.row].topicId?.name ?? "-" // self.topics[indexPath.row].name ?? ""
        }
       
        let lastlogin = NSDate(timeIntervalSince1970: (self.lists[indexPath.row].createdDate!/1000))
        self.dateformatter.dateStyle = .short
        self.dateformatter.timeStyle = .none
        let last = self.dateformatter.string(from: lastlogin as Date)
        cell.dateLabel.text = "\(last),"
        
        let startTime = NSDate(timeIntervalSince1970: ((self.lists[indexPath.row].time?.timeBegan)!/1000))
        self.dateformatter.dateStyle = .none
        self.dateformatter.timeStyle = .short
        let started = self.dateformatter.string(from: startTime as Date)
        
        let endTime = NSDate(timeIntervalSince1970: ((self.lists[indexPath.row].time?.timeEnds)!/1000))
        self.dateformatter.dateStyle = .none
        self.dateformatter.timeStyle = .short
        let ended = self.dateformatter.string(from: endTime as Date)
        cell.timeLabel.text = "\(started)-\(ended)"
        
        return cell
    }
    
    
    @objc func reloadData(){
        self.tableview.isHidden = false
        let query = AttendancesQuery(userId: self.myId, classId: self.classId)
        self.apollo.fetch(query: query) { result, error in
            if (result?.data?.attendances?.count)! > 0 {
                self.lists = (result?.data?.attendances)! as! [AttendancesQuery.Data.Attendance]
                self.tableview.reloadData()
                self.refreshControl.endRefreshing()
               
            }else{
                 self.tableview.isHidden = true
            }
        }
    }
    
    func getList(){
        self.tableview.isHidden = false
        let query = AttendancesQuery(userId: self.myId, classId: self.classId)
         self.apollo.fetch(query: query) { result, error in
            if (result?.data?.attendances?.count)! > 0{
                self.lists = (result?.data?.attendances)! as! [AttendancesQuery.Data.Attendance]
                 self.tableview.reloadData()
            }else{
                self.tableview.isHidden = true
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "list" {
            let controller = segue.destination as! JoinedUsersListVC
            controller.attendanceId = self.selectedAttendanceId
            controller.endTime = self.endTime
            controller.code = self.code
            controller.type = self.type
        }else if segue.identifier == "updateAttendance"{
            let controller = segue.destination as! TimeRegisterVC
            controller.selectedAttendanceId = self.selectedAttendanceId
            controller.beginTimee = self.beginTimee
            controller.endTimee = self.endTimee
            controller.date = self.date
            controller.radius = self.radius
            controller.code = self.code
            controller.type = self.type
            controller.classTime = self.classTime
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let editAction = UITableViewRowAction(style: .default, title: "Засах", handler: { (action, indexPath) in
            GlobalStaticVar.isUpdateAttendance = true
            self.beginTimee = self.lists[indexPath.row].time?.timeBegan ?? 0
            self.endTimee = self.lists[indexPath.row].time?.timeEnds ?? 0
            self.radius = Double(self.lists[indexPath.row].radius!)
            self.date = self.lists[indexPath.row].createdDate ?? 0
            self.selectedAttendanceId = self.lists[indexPath.row].id ?? ""
            self.code = self.lists[indexPath.row].code ?? ""
            self.type = self.lists[indexPath.row].__typename
            self.classTime = self.lists[indexPath.row].classTime ?? 0
            self.performSegue(withIdentifier: "updateAttendance", sender: self)
        })
        editAction.backgroundColor = GlobalVariables.black

        let deleteAction = UITableViewRowAction(style: .default, title: "Устгах", handler: { (action, indexPath) in
            let selected_att_id = self.lists[indexPath.row].id
            self.deleteAttendance(selected_att_id: selected_att_id!)
        })
        deleteAction.backgroundColor = UIColor.red
        return [  deleteAction, editAction]
    }

    func deleteAttendance(selected_att_id : String){
        let alert = UIAlertController(title: "Ирц устгах", message: "Та ирц устгах гэж байна !", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Тийм", style: .default  , handler: {action in
            let mutation = DeleteAttendanceMutation(userId: self.myId, attId: selected_att_id, classId: self.classId)
            self.apollo.perform(mutation: mutation) { result, error in
                if result?.data?.deleteAttendance?.error == false{
                    KVNProgress.showSuccess()
                    self.reloadData()
                }else{
                    KVNProgress.showError(withStatus: result?.data?.deleteAttendance?.message)
                }
            }
        }))
        alert.addAction(UIAlertAction(title: "Болих", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

