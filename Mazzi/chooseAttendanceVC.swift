//
//  chooseAttendanceVC.swift
//  Mazzi
//
//  Created by Bayara on 1/17/19.
//  Copyright © 2019 woovoo. All rights reserved.
//

import Foundation
import CoreLocation

class chooseAttendanceVC: UIViewController,CLLocationManagerDelegate{
   
    @IBOutlet weak var qrBtn: UIButton!
    @IBOutlet weak var bvrtguulehBtn: UIButton!
    @IBOutlet weak var creataAttBtn: UIButton!
    @IBOutlet weak var showAttLsitBtn: UIButton!
    var myId = SettingsViewController.userId
    var currentLocation: CLLocation!
    var lat : Double!
    var long : Double!
    let locManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customize()
        self.locManager.delegate = self
        self.locManager.requestWhenInUseAuthorization()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = ""
        if GlobalStaticVar.teacherId == self.myId {
            let rightBtn = UIBarButtonItem(image: UIImage(named:"editmenu"), style: .plain, target: self, action: #selector(self.goSettings))
            self.navigationController?.navigationBar.topItem?.rightBarButtonItem = rightBtn
        }
    }
    
    @objc func goSettings(){
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "setting") as? ClassroomSettingVC
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.myId == GlobalStaticVar.teacherId || GlobalStaticVar.classRole == "Teacher"{
            self.creataAttBtn.setTitle("Ирц үүсгэх", for: UIControl.State.normal)
             self.bvrtguulehBtn.isHidden = false
        }else{
            self.bvrtguulehBtn.isHidden = true
            self.creataAttBtn.setTitle("Ирц бүртгүүлэх", for: UIControl.State.normal)
        }
    }
    
    func customize(){
        self.creataAttBtn.layer.cornerRadius = 5
        self.creataAttBtn.backgroundColor = GlobalVariables.purple
        self.creataAttBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
        self.creataAttBtn.layer.masksToBounds = true
        self.creataAttBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
        self.creataAttBtn.addTarget(self, action: #selector(createAttendance), for: .touchUpInside)
        
        self.showAttLsitBtn.layer.cornerRadius = 5
        self.showAttLsitBtn.backgroundColor = GlobalVariables.purple
        self.showAttLsitBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
        self.showAttLsitBtn.layer.masksToBounds = true
        self.showAttLsitBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
        self.showAttLsitBtn.addTarget(self, action: #selector(toList), for: .touchUpInside)
        
        self.bvrtguulehBtn.layer.cornerRadius = 5
        self.bvrtguulehBtn.backgroundColor = GlobalVariables.purple
        self.bvrtguulehBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
        self.bvrtguulehBtn.layer.masksToBounds = true
        self.bvrtguulehBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
        self.bvrtguulehBtn.addTarget(self, action: #selector(toCode), for: .touchUpInside)
        
        self.qrBtn.layer.cornerRadius = 5
        self.qrBtn.backgroundColor = GlobalVariables.purple
        self.qrBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
        self.qrBtn.layer.masksToBounds = true
        self.qrBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
        self.qrBtn.addTarget(self, action: #selector(toQR), for: .touchUpInside)
    }
    
    @objc func toQR(){
         self.getCurrentLocation()
        let viewController = storyboard?.instantiateViewController(withIdentifier: "QRscanVC") as! QRscanVC
        viewController.lat = self.lat
        viewController.long = self.long
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func createAttendance(){
        if self.myId == GlobalStaticVar.teacherId || GlobalStaticVar.classRole == "Teacher"{
            performSegue(withIdentifier: "createAttendance", sender: self)
        }else{
            performSegue(withIdentifier: "tocode", sender: self)
        }
    }
    
    @objc func toList(){
        if self.myId == GlobalStaticVar.teacherId || GlobalStaticVar.classRole == "Teacher"{
            performSegue(withIdentifier: "tolist", sender: self)
        }else{
            performSegue(withIdentifier: "studentAttendances", sender: self)
        }
        
    }
    
    @objc func toCode(){
         performSegue(withIdentifier: "tocode", sender: self)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
            if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
                if CLLocationManager.isRangingAvailable() {
                    // do stuff
                    print("LOCATION")
                }
            }
        }
    }
    
    func getCurrentLocation(){
        
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            currentLocation = locManager.location
            self.lat = currentLocation.coordinate.latitude
            self.long = currentLocation.coordinate.longitude
        }else{
            self.locationManager(manager: locManager, didChangeAuthorizationStatus: .notDetermined)
        }
    }
    
    private func locationManager(manager: CLLocationManager!,
                                 didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        var shouldIAllow = false
        var locationStatus = ""
        switch status {
        case CLAuthorizationStatus.restricted:
            locationStatus = "Restricted Access to location"
        case CLAuthorizationStatus.denied:
            locationStatus = "User denied access to location"
        case CLAuthorizationStatus.notDetermined:
            locationStatus = "notDetermined"
        default:
            locationStatus = "Allowed to location Access"
            shouldIAllow = true
        }
        print(locationStatus)
        if shouldIAllow == false {
            let alert = UIAlertController(title: "", message: "Утасныхаа байршил тогтоогчийг идэвхжүүлнэ үү", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Тохиргоо", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction) -> Void in
                let alertt = UIAlertController(title: "Заавар", message: "Settings -> Privacy -> Location Services -> Mazzi : While Using the App", preferredStyle: UIAlertController.Style.alert)
                alertt.addAction(UIAlertAction(title: "OK", style: .cancel, handler:  { (alert:UIAlertAction) -> Void in }))
                self.present(alertt, animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
