//
//  RealObjects.swift
//  Mazzi
//
//  Created by Janibekm on 1/4/18.
//  Copyright © 2018 Mazzi App LLC. All rights reserved.
//

import Foundation
import RealmSwift

class Conversation_realm:Object{

    @objc dynamic var id = ""
    var user = List<User_group_realm>()
    var show = List<Show_id>()
//todojani
//    var shows = List<String>()
    
    @objc dynamic var lastmessage: Message_realm!
    @objc dynamic var title = ""
    @objc dynamic var created_at:Double = 0
   
    @objc dynamic var updated_at:Double = 0
    @objc dynamic var is_thread = false
    @objc dynamic var secret = false
    @objc dynamic var image = ""
    @objc dynamic var owner_id_con = ""
    override static func primaryKey() -> String? {
        return "id"
    }
}

class seenStrRealm:Object {
    @objc dynamic var stringValue = ""
}
class User_group_realm:Object {
    
    @objc dynamic var id = ""
    @objc dynamic var image = ""
    @objc dynamic var nickname = ""
    @objc dynamic var lastname = ""
    @objc dynamic var surname = ""
    @objc dynamic var phone = ""
    @objc dynamic var state = 0
    @objc dynamic var online_at:Double = 0
    @objc dynamic var fcm_token: String = ""
    @objc dynamic var register: String = ""
    @objc dynamic var owner_id: String = ""
    override class func primaryKey() -> String {
        return "id"
    }
}

class Show_id:Object {
    
    @objc dynamic var id = ""
    @objc dynamic var show = ""
    @objc dynamic var conversation_id = ""
    override static func primaryKey() -> String? {
        return "id"
    }
    
}

class Seen_id:Object{
    
    @objc dynamic var id = ""
    @objc dynamic var seen = ""
    @objc dynamic var conversation_id = ""
    override static func primaryKey() -> String? {
        return "id"
    }
    
}

class Message_realm:Object{
    
    @objc dynamic var id = ""
    @objc dynamic var owner_id = ""
    @objc dynamic var conversation_id = ""
    @objc dynamic var type = ""
    @objc dynamic var content = ""
    //zasvar
    @objc dynamic var filename = ""
    @objc dynamic var file_size = 0
    
    var seen =  List<Seen_id>()
    @objc dynamic var created_at:Double = 0
    @objc dynamic var duration:Double = 0
    
    override class func primaryKey() -> String {
        return "id"
    }
    
}
class Contacts_realm:Object {
    
    @objc dynamic var name: String = ""
    @objc dynamic var lastname:String = ""
    @objc dynamic var surname:String = ""
    @objc dynamic var image: String = ""
    var phones = List<Phones_p>()
    @objc dynamic var phone:String = ""
    @objc dynamic var id: String = ""
    @objc dynamic var gender:String = ""
    @objc dynamic var birthday:Double = 0
    @objc dynamic var state: Int = 0
    @objc dynamic var online_at: Double = 0
    @objc dynamic var created_at:Double = 0
    @objc dynamic var fcm_token: String = ""
     @objc dynamic var register: String = ""
    override class func primaryKey() -> String {
        return "id"
    }
}
class Phones_p:Object{
    
    @objc dynamic var id = ""
    @objc dynamic var phones = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

class StickersCatRealm:Object{
    @objc dynamic var id:String = ""
    @objc dynamic var name:String = ""
    @objc dynamic var thumb:String = ""
    @objc dynamic var price:Int = 0
    @objc dynamic var time:Double = 0
    var stickers = List<StickerImagesRealm>()
    override static func primaryKey() -> String? {
        return "id"
    }
}

class StickerImagesRealm:Object{
    @objc dynamic var id:String = ""
    @objc dynamic var name:String = ""
    @objc dynamic var path:String = ""
    @objc dynamic var categoryId:String = ""
    @objc dynamic var is_active:Bool = true
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

//class Stickers_realm:Object{
//
//    @objc dynamic var id:String = ""
//    @objc dynamic var name:String = ""
//    @objc dynamic var zip_url:String = ""
//    @objc dynamic var created_at:Double = 0
//    var images = List<StickerImages_realm>()
//    @objc dynamic var thumbnail_url:String = ""
//    @objc dynamic var is_active:Bool = true
//    @objc dynamic var category:String = ""
//    @objc dynamic var price:Double = 0
//    @objc dynamic var downloaded:Bool = false
//    @objc dynamic var download_at:Double = 0
//
//    override static func primaryKey() -> String? {
//        return "id"
//    }
//}
//class StickerImages_realm:Object{
//
//    @objc dynamic var id:String = ""
//    @objc dynamic var name:String = ""
//    @objc dynamic var path:String = ""
//
//    override static func primaryKey() -> String? {
//        return "id"
//    }
//
//}
//new function












