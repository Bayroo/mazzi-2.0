//
//  TransactionsVC.swift
//  Mazzi
//
//  Created by Bayara on 5/27/19.
//  Copyright © 2019 Mazz Application. All rights reserved.
//

import Foundation
import UIKit
import Apollo
import KVNProgress

class TransactionVC: UIViewController,UITableViewDelegate, UITableViewDataSource{
   
    @IBOutlet weak var transactionsTableView: UITableView!
    let dateformatter = DateFormatter()
    var myId = SettingsViewController.userId
    var transactions = [TransactionsQuery.Data.Transaction.Result]()
    var refresher = UIRefreshControl()
    let apollos: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
        let url = URL(string:SettingsViewController.graphQL)!
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Гүйлгээний түүх"
        self.transactionsTableView.delegate = self
        self.transactionsTableView.dataSource = self
        self.transactionsTableView.register((UINib(nibName: "transCell", bundle: nil)), forCellReuseIdentifier: "trans")
        self.transactionsTableView.refreshControl = self.refresher
        self.refresher.addTarget(self, action: #selector(reload), for: .valueChanged)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(backtoRoot))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        _ = self.apollos.clearCache()
        self.myId = SettingsViewController.userId
        checkInternet()
        if SettingsViewController.online {
            self.getTransactions()
        }else{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу!")
        }
    }
    
    @objc func backtoRoot(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func reload(){
        let query = TransactionsQuery(userId: self.myId)
        self.apollos.fetch(query: query) { result, error in
            if result?.data?.transactions?.error == false{
                self.transactions = (result?.data?.transactions?.result)! as! [TransactionsQuery.Data.Transaction.Result]
                self.transactionsTableView.reloadData()
                self.refresher.endRefreshing()
            }else{
                // print("ERROR:: ", result?.data?.transactions?.message)
            }
        }
    }
    
    func getTransactions(){
        let query = TransactionsQuery(userId: self.myId)
         self.apollos.fetch(query: query) { result, error in
            if result?.data?.transactions?.error == false{
//                print("RESULT:: ", result?.data?.transactions?.result)
                self.transactions = (result?.data?.transactions?.result)! as! [TransactionsQuery.Data.Transaction.Result]
                self.transactionsTableView.reloadData()
                
            }else{
//                print("ERROR:: ", result?.data?.transactions?.message)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.transactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "trans", for: indexPath) as! transCells
        if indexPath.row % 2 != 0{
            cell.contentView.backgroundColor = UIColor.white
        }else{
            cell.contentView.backgroundColor = GlobalVariables.F5F5F5
        }
        let lastlogin = NSDate(timeIntervalSince1970: (self.transactions[indexPath.row].createdDate!/1000))
        dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let last = dateformatter.string(from: lastlogin as Date)
        cell.dateLabel.text = last
        
        if self.transactions[indexPath.row].flow == true {
             cell.amountlabel.textColor = GlobalVariables.green
             cell.amountlabel.text = "+\(String(self.transactions[indexPath.row].amount ?? 0))₮"
        }else{
             cell.amountlabel.textColor = UIColor.red
             cell.amountlabel.text = "-\(String(self.transactions[indexPath.row].amount ?? 0))₮"
        }
        
        cell.descLabel.text = self.transactions[indexPath.row].description ?? ""
        return cell
    }
}
