//
//  EditProfileController.swift
//  Mazzi
//
//  Created by Janibekm on 10/22/17.
//  Copyright © 2017 Mazzi App LLC. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftyJSON
import KVNProgress
import AVFoundation

class CreateProfileController: UIViewController, myProtocol, UITextFieldDelegate,UIScrollViewDelegate ,UIPickerViewDelegate,UIPickerViewDataSource{
   
    @IBOutlet weak var pickerViewShowBtn: UIButton!
    @IBOutlet weak var pickChar: UITextField!
    @IBOutlet weak var pikchar2: UITextField!
    @IBOutlet weak var backtomaster: UIButton!
    @IBOutlet weak var proScrollview: UIScrollView!
    @IBOutlet weak var proImage: UIImageView!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var taniNer: UITextField!
    @IBOutlet weak var taniOvog: UITextField!
    @IBOutlet weak var taniUrginOvog: UITextField!
    @IBOutlet weak var torsonOn: UIDatePicker!
    @IBOutlet weak var taniEmail: UITextField!
    @IBOutlet weak var completeButton: UIButton!
    @IBOutlet weak var em: UIButton!
    @IBOutlet weak var er: UIButton!
    @IBOutlet weak var registerNumber: UITextField!
    @IBOutlet weak var checkmale: UIButton!
    @IBOutlet weak var labelMale: UILabel!
    
    @IBOutlet weak var labelFemale: UILabel!
    @IBOutlet weak var checkFemale: UIButton!
    
//    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var sexLabel: UILabel!
    @IBOutlet weak var mailLabel: UILabel!
    @IBOutlet weak var regLabel: UILabel!
    @IBOutlet weak var ovogLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    let limitLength = 10
    var birthday:Double = 0
    var lastname = ""
    var surname = ""
    var email = ""
    var password = ""
    var registerId = ""
    var visible = 0
    var complete = false
    let userDefaults = UserDefaults.standard
    var gender = ""
    let fileurl = SettingsViewController.fileurl
    var image = ""
    var phone = ""
    var displayphone = ""
    //var password = ""
    var region = ""
    var key = ""
    
    var userid = ""
    var is_confirmed = true
    var fcm_token = ""
    var pickerDataSource = [["А","Б","В","Г","Д","Е","Ё","Ж","З","И","Й","К","Л","М","Н","О","Ө","П","Р","С","Т","У","Ү","Ф","Х","Ц","Ч","Ш","Щ","Ъ","Ь","Ы","Э","Ю","Я"],             ["А","Б","В","Г","Д","Е","Ё","Ж","З","И","Й","К","Л","М","Н","О","Ө","П","Р","С","Т","У","Ү","Ф","Х","Ц","Ч","Ш","Щ","Ъ","Ь","Ы","Э","Ю","Я"]]
    
    var subview = UIView()
    var pickerTitle = UILabel()
    var closePicker = UIButton()
    var pickerView = UIPickerView()
    var isPickerShow = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.pickerView.dataSource = self
        self.pickerView.delegate = self
        if visible == 1 {
            self.backtomaster.isHidden = false
        }else{
            self.backtomaster.isHidden = false
        }
        
        self.taniNer.placeholder = "Таны нэр"
        self.taniOvog.placeholder = "Овог"
        self.taniEmail.placeholder = "Таны и-мэйл"
        self.MainConfig()
        let userInfo = userDefaults.dictionary(forKey: "userInfo")
        self.getifnameexists()
    
        let name = userInfo!["nickname"] as! String
        let lastname = userInfo!["lastname"] as! String
        let birthday = ( userInfo!["birthday"] as! Double )/1000
        let email = userInfo!["email"] as! String
        
        gender = userInfo!["gender"] as! String
        region = userInfo!["region"] as! String
        
        displayphone = userInfo!["displayphone"] as! String
        is_confirmed = userInfo!["is_confirmed"] as! Bool
        if(userDefaults.object(forKey: "fcm_token") != nil){
            fcm_token = (userDefaults.object(forKey: "fcm_token") as? String)!
        }
        
        if  userInfo!["registerId"] as? String != nil {
            let reg = userInfo!["registerId"] as? String
            print("register:: ", reg as Any)
            var numbers = reg!.components(separatedBy: ["А","Б","В","Г","Д","Е","Ё","Ж","З","И","Й","К","Л","М","Н","О","Ө","П","Р","С","Т","У","Ү","Ф","Х","Ц","Ч","Ш","Щ","Ъ","Ь","Ы","Э","Ю","Я"])
            var chars = reg!.components(separatedBy: ["0","1","2","3","4","5","6","7","8","9"])
            let last = chars[0].last
            let first = chars[0].first
            if first != nil {
                self.pickChar.text = String(first!)
                self.pikchar2.text =  String(last!)
                self.registerNumber.text = numbers[2]
            }else{
                
            }
        }
        
        self.taniNer.text = name
        self.taniOvog.text = lastname
        self.taniEmail.text = email
        image = userInfo!["image"] as! String
        if image == "" {
            self.proImage.image = UIImage(named: "circle-avatar")
        }else{
            self.proImage.sd_setImage(with: URL(string:"\(fileurl)\(image)"), completed: nil)
        }
    
        if(userDefaults.object(forKey: "key") != nil){
             self.key = (userDefaults.object(forKey: "key") as? String)!
        }
        
        userid = (userDefaults.object(forKey: "user_id") as? String)!
        self.phoneNumber.text = "+\(region) \(displayphone)"
        self.torsonOn.setDate(NSDate(timeIntervalSince1970: TimeInterval(birthday)) as Date, animated: true)
        self.taniNer.delegate = self
        self.taniOvog.delegate = self
        self.taniEmail.delegate = self
        self.registerNumber.delegate = self
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        self.buttonConfig()
        
    }
    
    func MainConfig(){
        self.pickChar.delegate = self
        self.taniNer.textColor = GlobalVariables.todpurple
        self.taniOvog.textColor = GlobalVariables.todpurple
        self.taniEmail.textColor = GlobalVariables.todpurple
        self.registerNumber.textColor = GlobalVariables.todpurple
        self.phoneNumber.textColor = GlobalVariables.todpurple
        self.pickChar.textColor = GlobalVariables.todpurple
        self.pikchar2.textColor = GlobalVariables.todpurple
        
        self.nameLabel.textColor = GlobalVariables.todpurple.withAlphaComponent(0.5)
        self.ovogLabel.textColor = GlobalVariables.todpurple.withAlphaComponent(0.5)
        self.regLabel.textColor = GlobalVariables.todpurple.withAlphaComponent(0.5)
        self.mailLabel.textColor = GlobalVariables.todpurple.withAlphaComponent(0.5)
        self.dateLabel.textColor = GlobalVariables.todpurple.withAlphaComponent(0.5)
        self.sexLabel.textColor = GlobalVariables.todpurple.withAlphaComponent(0.5)
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x:0.0, y:taniNer.frame.height - 1, width:taniNer.frame.width, height: 1.0)
        bottomLine.backgroundColor = GlobalVariables.todpurple.cgColor
        taniNer.borderStyle = UITextField.BorderStyle.none
        taniNer.layer.addSublayer(bottomLine)
        
        let bottomLine2 = CALayer()
        bottomLine2.frame = CGRect(x:0.0, y:taniOvog.frame.height - 1, width:taniOvog.frame.width, height: 1.0)
        bottomLine2.backgroundColor = GlobalVariables.todpurple.cgColor
        taniOvog.borderStyle = UITextField.BorderStyle.none
        taniOvog.layer.addSublayer(bottomLine2)
        
        let bottomLine3 = CALayer()
        bottomLine3.frame = CGRect(x:0.0, y:taniEmail.frame.height - 1, width:taniEmail.frame.width, height: 1.0)
        bottomLine3.backgroundColor = GlobalVariables.todpurple.cgColor
        taniEmail.borderStyle = UITextField.BorderStyle.none
        taniEmail.layer.addSublayer(bottomLine3)
        
        let bottomLine4 = CALayer()
        bottomLine4.frame = CGRect(x:0.0, y:registerNumber.frame.height - 1, width:registerNumber.frame.width, height: 1.0)
        bottomLine4.backgroundColor = GlobalVariables.todpurple.cgColor
        registerNumber.borderStyle = UITextField.BorderStyle.none
        registerNumber.layer.addSublayer(bottomLine4)
        
        let bottomLine5 = CALayer()
        bottomLine5.frame = CGRect(x:0.0, y:self.pickChar.frame.height - 1, width:self.pickChar.frame.width, height: 1.0)
        bottomLine5.backgroundColor = GlobalVariables.todpurple.cgColor
        self.pickChar.borderStyle = UITextField.BorderStyle.none
        self.pickChar.layer.addSublayer(bottomLine5)
        
        let bottomLine6 = CALayer()
        bottomLine6.frame = CGRect(x:0.0, y:self.pikchar2.frame.height - 1, width:self.pikchar2.frame.width - 4 , height: 1.0)
        bottomLine6.backgroundColor = GlobalVariables.todpurple.cgColor
        self.pikchar2.borderStyle = UITextField.BorderStyle.none
        self.pikchar2.layer.addSublayer(bottomLine6)
        
        self.subview.frame = CGRect(x:0, y:self.view.bounds.height,width: self.view.bounds.width, height:250)
        self.pickerTitle.frame = CGRect(x:self.view.bounds.width/2 - 75, y:4, width:150,height:20)
        self.pickerView.frame = CGRect(x:0,y:28,width:self.view.bounds.width,height:232)
        self.closePicker.frame = CGRect(x:4,y:4,width:20,height:20)

        self.subview.backgroundColor = GlobalVariables.purple
        self.pickerView.backgroundColor = UIColor.white
        self.pickerTitle.textAlignment = NSTextAlignment.center
        self.pickerTitle.text = "Регистр"
        self.pickerTitle.textColor = UIColor.white
        self.closePicker.setImage(UIImage(named: "closewhite"), for: UIControl.State.normal)
        self.closePicker.addTarget(self, action: #selector(dismissPicker), for: .touchUpInside)

        self.subview.addSubview(self.pickerTitle)
        self.subview.addSubview(self.pickerView)
        self.subview.addSubview(self.closePicker)
        self.view.addSubview(self.subview)
        self.pickerViewShowBtn.addTarget(self, action: #selector(showPicker), for: .touchUpInside)
    }
    
    @objc func dismissPicker(){
        if self.isPickerShow == true {
            UIView.transition(with: self.subview, duration: 0.3, options: .transitionCrossDissolve, animations: {
                self.isPickerShow = false
                self.subview.frame = CGRect(x:0, y:self.view.bounds.height,width: self.view.bounds.width, height:250)
            })
        }
    }
    
    @objc func showPicker(){
        print("-------showPicker-----------------")
        taniNer.resignFirstResponder()
        taniOvog.resignFirstResponder()
        taniEmail.resignFirstResponder()
        registerNumber.resignFirstResponder()
        self.proScrollview.contentOffset.y = 100
        UIView.transition(with: self.subview, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.subview.frame = CGRect(x:0, y: self.view.frame.height-250,width: self.view.bounds.width, height:250)
            self.isPickerShow = true
        })
    }
    
    func getifnameexists(){
        
        let userInfo = userDefaults.dictionary(forKey: "userInfo")
        let name = userInfo!["nickname"] as! String
        userid = (userDefaults.object(forKey: "user_id") as? String)!
        image = userInfo!["image"] as! String
        gender = userInfo!["gender"] as! String
        region = userInfo!["region"] as! String
        password = userDefaults.object(forKey: "pass") as! String
        if(userDefaults.object(forKey: "fcm_token") != nil){
            fcm_token = (userDefaults.object(forKey: "fcm_token") as? String)!
        }
        displayphone = userInfo!["displayphone"] as! String
        //userInfo!["is_confirmed"] as! Bool
        birthday = userInfo!["birthday"] as! Double
        
        if(userDefaults.object(forKey: "registerId") != nil){
            registerId = userInfo!["registerId"] as! String
        }
    
        phone = (userDefaults.object(forKey: "phone") as? String)!
        
        if(userDefaults.object(forKey: "key") != nil){
            key = (userDefaults.object(forKey: "key") as? String)!
        }
        
        lastname = userInfo!["lastname"] as! String
        email = userInfo!["email"] as! String
        self.taniNer.text = name
        
        if gender == "male"{
            self.checkmale.isSelected = true
        }else if gender == "female"{
            self.checkFemale.isSelected = true
        }
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickerDataSource[0].count
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        if component == 0{
            self.pickChar.text = self.pickerDataSource[component][row]
        }else{
            self.pikchar2.text = self.pickerDataSource[component][row]
        }
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataSource[component][row]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(CreateProfileController.showKeyboard(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        self.view.layoutIfNeeded()
        
    }
    
    @objc func dismissKeyboard(){
        taniNer.resignFirstResponder()
        taniOvog.resignFirstResponder()
        taniEmail.resignFirstResponder()
        registerNumber.resignFirstResponder()
        self.dismissPicker()
       
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        taniNer.resignFirstResponder()
        taniOvog.resignFirstResponder()
        taniEmail.resignFirstResponder()
        registerNumber.resignFirstResponder()
        if taniEmail.resignFirstResponder(){
            print("CreateProfileController - email shoud return")
          }
        return true
    }
  
    @objc func showKeyboard(notification: Notification) {
        
        if self.isPickerShow == true {
            UIView.transition(with: self.subview, duration: 0.3, options: .transitionCrossDissolve, animations: {
                self.isPickerShow = false
                self.subview.frame = CGRect(x:0, y:self.view.bounds.height,width: self.view.bounds.width, height:250)
            })
        }
        
        if let frame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let height = frame.cgRectValue.height
            self.proScrollview.contentInset.bottom = height
            self.proScrollview.scrollIndicatorInsets.bottom = height
            if height > 100{
                let keyboardHeight = Int(height.rounded(.toNearestOrEven))
                if(userDefaults.object(forKey: "keyboardheight") == nil){
                     self.userDefaults.set(keyboardHeight, forKey: "keyboardheight")
                }
            }
        }
    }
    
    func buttonConfig(){
        self.proImage.layer.cornerRadius = 65
        self.proImage.layer.borderColor = GlobalVariables.purple.cgColor
        self.proImage.layer.borderWidth = 0.5
  
        self.checkmale.setImage(UIImage(named:"Ellipse 3"), for: UIControl.State.selected)
        self.checkmale.setImage(UIImage(named:"Ellipse 2"), for: UIControl.State.normal)
        
        self.checkFemale.setImage(UIImage(named:"Ellipse 3"), for: UIControl.State.selected)
        self.checkFemale.setImage(UIImage(named:"Ellipse 2"), for: UIControl.State.normal)
        
        if self.checkmale.isSelected == true{
            labelMale.textColor = GlobalVariables.todpurple
        }else{
            labelMale.textColor = GlobalVariables.todpurple.withAlphaComponent(0.5)
        }
        
        if self.checkFemale.isSelected == true{
            labelFemale.textColor = GlobalVariables.todpurple
        }else{
            labelFemale.textColor = GlobalVariables.todpurple.withAlphaComponent(0.5)
        }
   
    }
 
    @IBAction func chooseImage(_ sender: Any) {
//        checkCameraAccess()
//        if SettingsViewController.cameraAccess == true{
//            self.performSegue(withIdentifier: "proimage", sender: self)
//        }else{
//            let alert = UIAlertController(title: "", message: "Утасныхаа тохиргоо руу орж камер ашиглах боломжийг олгоно уу !", preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
//        }
        
        let authorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authorizationStatus {
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video,completionHandler: { (granted:Bool) -> Void in
                if granted {
                    SettingsViewController.cameraAccess = true
                    self.performSegue(withIdentifier: "proimage", sender: self)
                }
                else {
                    SettingsViewController.cameraAccess = false
                    let alert = UIAlertController(title: "", message: "Утасныхаа тохиргоо руу орж камер ашиглах боломжийг олгоно уу !", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            })
        case .authorized:
            SettingsViewController.cameraAccess = true
            self.performSegue(withIdentifier: "proimage", sender: self)
        case .denied, .restricted:
            SettingsViewController.cameraAccess = false
            let alert = UIAlertController(title: "", message: "Утасныхаа тохиргоо руу орж камер ашиглах боломжийг олгоно уу !", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }

    }
    
    @IBAction func er(_ sender: Any) {
        self.gender = "male"
      
        self.checkFemale.isSelected = false
        self.checkmale.isSelected = true
        
        self.checkmale.setImage(UIImage(named:"Ellipse 3"), for: UIControl.State.selected)
        self.checkmale.setImage(UIImage(named:"Ellipse 2"), for: UIControl.State.normal)
        
        self.checkFemale.setImage(UIImage(named:"Ellipse 3"), for: UIControl.State.selected)
        self.checkFemale.setImage(UIImage(named:"Ellipse 2"), for: UIControl.State.normal)
        
        if self.checkmale.isSelected == true  {
            labelMale.textColor = GlobalVariables.todpurple
            labelFemale.textColor = GlobalVariables.todpurple.withAlphaComponent(0.5)
        }else{
            labelMale.textColor = GlobalVariables.todpurple.withAlphaComponent(0.5)
        }
    }

    @IBAction func em(_ sender: Any) {
        self.gender = "female"
        self.checkmale.isSelected = false
        self.checkFemale.isSelected = true
        
        self.checkFemale.setImage(UIImage(named:"Ellipse 3"), for: UIControl.State.selected)
        self.checkFemale.setImage(UIImage(named:"Ellipse 2"), for: UIControl.State.normal)
        
        self.checkmale.setImage(UIImage(named:"Ellipse 3"), for: UIControl.State.selected)
        self.checkmale.setImage(UIImage(named:"Ellipse 2"), for: UIControl.State.normal)
        
        if self.checkFemale.isSelected == true {
            labelFemale.textColor = GlobalVariables.todpurple
            labelMale.textColor = GlobalVariables.todpurple.withAlphaComponent(0.5)
        }else{
            labelFemale.textColor = GlobalVariables.todpurple.withAlphaComponent(0.5)
        }
    }
    
    @IBAction func completeButton(_ sender: Any) {
      
        if taniNer.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")) == "" {
            showAlert(title: "Нэр оруулна уу!")
            return
        }
        
        if taniOvog.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")) == "" {
            showAlert(title: "Овог оруулна уу!")
            return
        }
        
        if taniEmail.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")) == "" {
            showAlert(title: "Имэйл оруулна уу!")
            return
            
        }else if !Utils.isValidEmail(testStr: taniEmail.text!){
            showAlert(title: "Зөв имэйл оруулна уу!")
            return
        }
        
         let myRegister = "\(self.pickChar.text!)\(self.pikchar2.text!)\(self.registerNumber.text!)"
        
        if myRegister.trimmingCharacters(in: CharacterSet(charactersIn: " ")) == "" {
            showAlert(title: "Регистрийн дугаараа оруулна уу!")
            return
        }else if !Utils.isValidRegister(testStr: myRegister){
            showAlert(title: "Зөв регистрийн дугаар оруулна уу!")
            return
        }
        if self.proImage!.image != nil{
            self.uploadImg()
        }else{
            self.updateProfile()
        }
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        guard let text = registerNumber.text else { return true }
////        print("reg text: ", text)
//        let newLength = text.count + string.count - range.length
//        if newLength >= limitLength{
//              self.completeButton.isEnabled = true
//            //   self.loginbtn.backgroundColor = GlobalVariables.TextBvdeg.withAlphaComponent(0.2)
//        }else{
//            self.completeButton.isEnabled = false
//        }
//        return newLength <= limitLength
//    }
    
    func updateProfile(){
        print("------------updateProfile--------------------")
        let dateofbirth = self.torsonOn.date.timeIntervalSince1970
        let doubledate = Double(dateofbirth * 1000)
        
        let myRegister = "\(self.pickChar.text!)\(self.pikchar2.text!)\(self.registerNumber.text!)"
        print("-----------> :", myRegister)
        
        let profile = ["_id":userid,"image":image,"nickname": self.taniNer.text!, "phone": self.displayphone, "key": key, "region": region, "lastname": self.taniOvog.text!, "birthday": Double(doubledate),"email":self.taniEmail.text!, "gender":gender,"fcm_token":fcm_token,"registerId":myRegister,"password":password] as [String : Any]
        
        if self.taniNer.text! != "" {
           
            SettingsViewController.userId = self.userid
            User_group.updateprofile(profile: profile, userids: userid, completion: { (status) in
                 KVNProgress.show()
                if status == true {
                    DispatchQueue.main.async{
                        let userInfo = ["image": self.image,
                                        "nickname":self.taniNer.text!,
                                        "lastname":self.taniOvog.text!,
                                        "birthday":doubledate,
                                        "email":self.taniEmail.text!,
                                        "gender":self.gender,
                                        "region":self.region,
                                        "displayphone": self.displayphone,
                                        "is_confirmed": self.is_confirmed,
                                        "registerId": myRegister,
                                        ] as [String : Any]
                        UserDefaults.standard.set(userInfo, forKey: "userInfo")
                        self.userDefaults.synchronize()
                    }
                    let when = DispatchTime.now()
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        if self.visible == 1 {
                            self.viewDidLoad()
                            self.dismiss(animated: true, completion: nil)
                        }else{
                            let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainnav") as! MainNavViewController
                            self.present(viewController, animated: true, completion: nil)
                        }
                       
                    }
                     KVNProgress.dismiss()
                }else{
                    KVNProgress.dismiss()
                    print("status:: ",status)
                    print("CreateProfileController - profile update failed from server")
                    self.dismiss(animated: true, completion: nil)
                }
            })
        }else{
            print("CreateProfileController field empty")
     
            let alert = UIAlertController(title: "Алдаа !", message: "Интернетийн холболтоо шалгана уу!", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Close", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func backtomaster(_ sender: Any) {
        self.dismissKeyboard()
        self.visible = 0
        dismiss(animated: true, completion: nil)
    }
    func photoGet(capturedImage: UIImage) {
        self.proImage.image = capturedImage
    }
    
    func videoGet(clipedVideo: URL) {
        print("video didnt support")
    }
    func uploadImg(){
        print("-------uploadImg----------uploadImg")
        let userid = userDefaults.object(forKey: "user_id") as? String
        KVNProgress.show()
        Message.uploadimage(chatId: userid! ,image: self.proImage.image!, completion: { (state, imageurl) in
            DispatchQueue.main.async {
                let url = JSON(imageurl)
                self.image = url["url"].string!
                self.updateProfile()
//                KVNProgress.dismiss()
            }
        })
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "proimage" {
            GlobalVariables.isChat = false
            let camera = segue.destination as? CameraView
            camera?.disable = true
            camera?.myProtocol = self
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func showAlert(title: String? = nil, message: String? = nil){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(ok)
        present(alertController, animated: true, completion: nil)
    }

}
