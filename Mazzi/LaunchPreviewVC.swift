//
//  LaunchPreviewVC.swift
//  Mazzi
//
//  Created by Bayara on 4/1/19.
//  Copyright © 2019 Mazz Application. All rights reserved.
//

import Foundation
import UIKit


class LaunchPreviewVC:UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
 
    @IBOutlet weak var myCollectionView: UICollectionView!
    var questionCount = 0
    var unAnsweredQuestions = [Int]()
    var answeredQuestions = [Int]()
    override func viewDidLoad() {
        super.viewDidLoad()
        print("answeredQuestions::  ",self.answeredQuestions)
        self.myCollectionView.delegate = self
        self.myCollectionView.dataSource = self
        self.myCollectionView!.register(UINib(nibName: "launchPreview", bundle: nil), forCellWithReuseIdentifier: "launchpreview")
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //.horizontal
        layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        layout.minimumLineSpacing = 10.0
        layout.minimumInteritemSpacing = 4.0
        self.myCollectionView.setCollectionViewLayout(layout, animated: true)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(baktoroot))
    }
    
    @objc func baktoroot(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.myCollectionView.bounds.size.width - 30) / 4
        return CGSize(width: width,height: width)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.questionCount
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        GlobalStaticVar.selectedQuestionIndexFromLaunch = indexPath.row
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "launchpreview", for: indexPath) as! launchPreviewCell
        cell.mylabel.text = String(indexPath.row)
        if self.answeredQuestions.contains(indexPath.row){
            cell.mylabel.textColor = UIColor.red
        }else{
            cell.mylabel.textColor = UIColor.black
        }
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "updatePost"{
//            let controller = segue.destination as! UpdatePostVC
            
        }
    }
    
}
