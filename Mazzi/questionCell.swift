//
//  questionCell.swift
//  Mazzi
//
//  Created by Bayara on 4/3/19.
//  Copyright © 2019 Mazz Application. All rights reserved.
//

import Foundation
import UIKit

class questionCell: UITableViewCell{
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var pointLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var rightBtn: UIButton!
    @IBOutlet weak var leftBtn: UIButton!
    
    @IBOutlet weak var hintLabel: UILabel!
    @IBOutlet weak var questionWithImageLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var myWebView: UIWebView!
    
    func initcell(){
        self.myWebView.backgroundColor = UIColor.white
        self.leftBtn.layer.borderWidth = 1
        self.leftBtn.layer.borderColor = GlobalVariables.black.cgColor
        self.leftBtn.layer.masksToBounds = true
        
        self.rightBtn.layer.borderWidth = 1
        self.rightBtn.layer.borderColor = GlobalVariables.black.cgColor
        self.rightBtn.layer.masksToBounds = true
    }
}
