//
//  AttlistCell.swift
//  Mazzi
//
//  Created by Bayara on 1/10/19.
//  Copyright © 2019 woovoo. All rights reserved.
//

import Foundation

class AttlistCell: UITableViewCell{
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
}
