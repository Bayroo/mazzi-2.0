//
//  FeedbackVC.swift
//  Mazzi
//
//  Created by Bayara on 3/15/19.
//  Copyright © 2019 Mazz Application. All rights reserved.
//

import Foundation
import UIKit
import Apollo
import KVNProgress

class FeedbackVC: UIViewController,UITextViewDelegate{
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var sendBtn: UIButton!
    var myId = SettingsViewController.userId
    var appVersion = ""
    let apollo: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken]
        let url = URL(string:SettingsViewController.graphQL)!
        
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textView.delegate = self
        self.appVersion = (Bundle.main.infoDictionary?["CFBundleShortVersionString"]  as? String)!
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "05"), style: .plain, target: self, action: #selector(back))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "send"), style: .plain, target: self, action: #selector(sendSurvey))
        self.title = "Санал хүсэлт илгээх"
        self.sendBtn.layer.cornerRadius = 5
        self.sendBtn.backgroundColor = GlobalVariables.purple
        self.sendBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
        self.sendBtn.layer.masksToBounds = true
        self.sendBtn.addTarget(self, action: #selector(sendSurvey), for: .touchUpInside)
        
        self.textView.backgroundColor = GlobalVariables.F5F5F5
        self.textView.textColor = GlobalVariables.lightGray
        self.textView.text = "Маззи-тай холбоотой санал хүсэлтээ энд бичнэ үү"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkInternet()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if self.textView.textColor == GlobalVariables.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    @objc func sendSurvey(){
        if SettingsViewController.online {
            if self.textView.text.count > 0 && self.textView.text != "Маззи-тай холбоотой санал хүсэлтээ энд бичнэ үү"{
                let mutation = InsertFeedbackMutation(userId: self.myId, description: self.textView.text, version: self.appVersion, platform: "IOS")
                self.apollo.perform(mutation: mutation) { result, error in
                    if result!.data?.insertFeedback?.error == false{
                        let alert = UIAlertController(title: "", message: "Таны хүсэлтийг хүлээн авлаа. Баярлалаа.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction) -> Void in
                            self.back()
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }else{
                        KVNProgress.showError(withStatus: result?.data?.insertFeedback?.message, on: self.view)
                    }
                }
            }else{
                let alert = UIAlertController(title: "", message: "Хоосон хүсэлт явуулах боломжгүй", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "За", style: UIAlertAction.Style.cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу!")
        }
      
    }
    
    @objc func back(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissKeyboard(){
        self.textView.resignFirstResponder()
        if self.textView.text.count == 0 {
            self.textView.backgroundColor = GlobalVariables.F5F5F5
            self.textView.textColor = GlobalVariables.lightGray
            self.textView.text = "Маззи-тай холбоотой санал хүсэлтээ энд бичнэ үү"
        }
    }
}
