//
//  postsCell.swift
//  Mazzi
//
//  Created by Bayara on 11/22/18.
//  Copyright © 2018 Mazzi Application. All rights reserved.
//

import Foundation
import UIKit

class postsCell: UITableViewCell{
    
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var proImg: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var createdLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var comBtn: UIButton!
    @IBOutlet weak var attachLabel: UILabel!
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var TxtField: UITextField!
    @IBOutlet weak var editClickbnt: UIButton!
    @IBOutlet weak var lastnameLabel: UILabel!
    @IBOutlet weak var topicLabel: UILabel!
    @IBOutlet weak var filterBtuserBtn: UIButton!
    
    
    func initCell(){
        self.proImg.layer.cornerRadius = self.proImg.bounds.width / 2
        self.proImg.layer.borderWidth = 1
        self.proImg.layer.borderColor = GlobalVariables.F5F5F5.cgColor
        self.proImg.layer.masksToBounds = true
        
        self.filterBtn.layer.cornerRadius = 5
        self.filterBtn.layer.masksToBounds = true
        self.filterBtn.backgroundColor = GlobalVariables.purple
        self.filterBtn.setTitleColor(UIColor.white, for: .normal)
        self.createdLabel.textColor = GlobalVariables.lightGray
        self.attachLabel.textColor = GlobalVariables.lightGray
        self.commentLabel.textColor = GlobalVariables.lightGray
    }
}

class createPostsCell:UITableViewCell {
    
    @IBOutlet weak var topicBtn: UIButton!
    @IBOutlet weak var lastnameLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var proImg: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var topicLabel: UILabel!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String!) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    func initcell(){
        self.proImg.layer.borderWidth = 1
        self.proImg.layer.borderColor = GlobalVariables.F5F5F5.cgColor
        self.proImg.layer.masksToBounds = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    /// Custom setter so we can initialise the height of the text view
    var textString: String {
        get {
            return textView.text
        }
        set {
            textView.text = newValue
            
            textViewDidChange(textView)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Disable scrolling inside the text view so we enlarge to fitted size
        textView.isScrollEnabled = false
        textView.delegate = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected {
            textView.becomeFirstResponder()
        } else {
            textView.resignFirstResponder()
        }
    }
}

extension UITableViewCell {
    /// Search up the view hierarchy of the table view cell to find the containing table view
    var tableView: UITableView? {
        get {
            var table: UIView? = superview
            while !(table is UITableView) && table != nil {
                table = table?.superview
            }
            
            return table as? UITableView
        }
    }
}

extension createPostsCell: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        
        let size = textView.bounds.size
        let newSize = textView.sizeThatFits(CGSize(width: size.width, height: CGFloat.greatestFiniteMagnitude))
        
        // Resize the cell only when cell's size is changed
        if size.height != newSize.height {
            UIView.setAnimationsEnabled(false)
            tableView?.beginUpdates()
            tableView?.endUpdates()
            UIView.setAnimationsEnabled(true)
            
            if let thisIndexPath = tableView?.indexPath(for: self) {
                tableView?.scrollToRow(at: thisIndexPath, at: .bottom, animated: false)
            }
        }
    }
}
