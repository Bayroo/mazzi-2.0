//
//  LaunchStudentsCell.swift
//  Mazzi
//
//  Created by Bayara on 12/16/18.
//  Copyright © 2018 Mazzi Application. All rights reserved.
//

import Foundation

class LaunchStudentsCell:UITableViewCell{
    @IBOutlet weak var countLabel:UILabel!
     @IBOutlet weak var nameLabel:UILabel!
     @IBOutlet weak var pointLabel:UILabel!
     @IBOutlet weak var pointView:UIView!
    @IBOutlet weak var lastnameLabel: UILabel!
}
