//
//  CollectionView.swift
//  Mazzi
//
//  Created by Bayara on 10/22/18.
//  Copyright © 2018 Mazzi Application LLC. All rights reserved.
//

import Foundation
import UIKit

class CollectionView : UICollectionViewCell {
    
    @IBOutlet var btn: UIButton!
    override func awakeFromNib() {
        
        // set label Attribute
        
        super.awakeFromNib()
        self.btn.layer.borderWidth = 1.0
        self.btn.layer.borderColor = GlobalVariables.purple.cgColor
        self.btn.layer.cornerRadius = 15
        self.btn.layer.masksToBounds = true
        // Initialization code
    }
    func initcell(){
        self.btn.layer.borderWidth = 1.0
        self.btn.layer.borderColor = GlobalVariables.purple.cgColor
        self.btn.layer.cornerRadius = 15
        self.btn.layer.masksToBounds = true
    }
}
