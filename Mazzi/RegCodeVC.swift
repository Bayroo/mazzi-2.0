//
//  RegCodeVC.swift
//  Mazzi
//
//  Created by Bayara on 1/9/19.
//  Copyright © 2019 woovoo. All rights reserved.
//

import Foundation
import Apollo
import KVNProgress
import CoreLocation
import MapKit

class RegCodeVC: UIViewController,CLLocationManagerDelegate{
    
    var apollo: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
        let url = URL(string:SettingsViewController.graphQL)!
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    @IBOutlet weak var stView: UIView!
    @IBOutlet weak var textfield: UITextField!
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var label: UILabel!
    var myId = SettingsViewController.userId
    var currentLocation: CLLocation!
    var lat : Double!
    var classId = ""
    var long : Double!
    let locManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customize()
        self.classId = GlobalStaticVar.classId
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.textfield.becomeFirstResponder()
        super.viewDidAppear(true)
        self.getCurrentLocation()
    }
    
    func customize(){
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(back))
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.label.textColor = GlobalVariables.purple
        self.textfield.layer.cornerRadius = 5
        self.textfield.layer.borderColor = GlobalVariables.purple.cgColor
        self.textfield.layer.borderWidth = 1
        self.textfield.layer.masksToBounds = true
        self.btn.layer.cornerRadius = 5
        self.btn.setTitleColor(UIColor.white, for: UIControl.State.normal)
        self.btn.backgroundColor = GlobalVariables.purple
        self.btn.layer.masksToBounds = true
        self.btn.addTarget(self, action: #selector(sendCode), for: .touchUpInside)
        
    }
    
    func getCurrentLocation(){
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            currentLocation = locManager.location
            self.lat = currentLocation.coordinate.latitude
            self.long = currentLocation.coordinate.longitude
            
        }else{
            self.locationManager(manager: locManager, didChangeAuthorizationStatus: .notDetermined)
        }
    }
    
    private func locationManager(manager: CLLocationManager!,
                                 didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        var shouldIAllow = false
        var locationStatus = ""
        switch status {
        case CLAuthorizationStatus.restricted:
            locationStatus = "Restricted Access to location"
        case CLAuthorizationStatus.denied:
            locationStatus = "User denied access to location"
        case CLAuthorizationStatus.notDetermined:
            locationStatus = "notDetermined"
        default:
            locationStatus = "Allowed to location Access"
            shouldIAllow = true
        }
        print(locationStatus)
        if shouldIAllow == false {
            let alert = UIAlertController(title: "", message: "Утасныхаа байршил тогтоогчийг идэвхжүүлнэ үү", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Тохиргоо", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction) -> Void in
                let alertt = UIAlertController(title: "Заавар", message: "Settings -> Privacy -> Location Services -> Mazzi : While Using the App", preferredStyle: UIAlertController.Style.alert)
                alertt.addAction(UIAlertAction(title: "OK", style: .cancel, handler:  { (alert:UIAlertAction) -> Void in }))
                self.present(alertt, animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func dismissKeyboard(){
        self.textfield.resignFirstResponder()
    }
    @objc func back(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func sendCode(){
//        print("myId: ", self.myId)
//        print("code: ", self.textfield.text!)
//        print("lat: ", self.lat)
//        print("long: ", self.long)
        
        let mutation = CheckIrtsMutation(userId: self.myId, code: self.textfield.text!, lat: self.lat, long: self.long,classId: self.classId)
        self.apollo.perform(mutation: mutation) { result, error in
//            print("RESULT:: ",result as Any)
            if result?.data?.checkInOrOut?.error == false {
                let alert = UIAlertController(title: "", message: "Ирц амжилттай бүртгэгдлээ", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "ОК", style: .default  , handler: {action in
                    self.back()
                }))
                self.present(alert, animated: true, completion: nil)
            }else{
                KVNProgress.showError(withStatus: result?.data?.checkInOrOut?.message)
                self.back()
            }
        }
    }
}
