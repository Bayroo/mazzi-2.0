//
//  StudentAttenceCell.swift
//  Mazzi
//
//  Created by Bayara on 1/13/19.
//  Copyright © 2019 woovoo. All rights reserved.
//

import Foundation

class StudentAttenceCell: UITableViewCell{
    @IBOutlet weak var nickNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var RadiusLabel: UILabel!
    @IBOutlet weak var RegTimeLabel: UILabel!
    @IBOutlet weak var lastnameLabel: UILabel!
    @IBOutlet weak var lateMinLabel: UILabel!
    
    
}
