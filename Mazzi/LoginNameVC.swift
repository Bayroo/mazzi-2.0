//
//  LoginNameVC.swift
//  Mazzi
//
//  Created by Janibekm on 11/29/17.
//  Copyright © 2017 Mazzi App LLC. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import KVNProgress

class LoginNameVC: UIViewController, UITextFieldDelegate, myProtocol {

    @IBOutlet weak var mySCrollView: UIScrollView!
//    @IBOutlet weak var errorlabel: UILabel!
    @IBOutlet weak var username: UITextField!
  //  @IBOutlet weak var register: UITextField!
    @IBOutlet weak var mail: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var profilePic: UIImageView!
    
    @IBOutlet weak var beginMazziBtn: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
//    @IBOutlet weak var namelabel: UILabel!
  //  @IBOutlet weak var reglabel: UILabel!
//    @IBOutlet weak var mailLabel: UILabel!
//    @IBOutlet weak var passLabel: UILabel!
    
//    @IBOutlet weak var saveMeBtn: UIButton!
    @IBOutlet weak var malecheckbtn: UIButton!
    @IBOutlet weak var maleLabel: UILabel!
    @IBOutlet weak var femaleCheckBtn: UIButton!
    @IBOutlet weak var femaleLabel: UILabel!
//    @IBOutlet weak var checkboxBtn: UIButton!
    let fileurl = SettingsViewController.fileurl
    
    let userDefaults = UserDefaults.standard
    var userid = ""
    var image = ""
    var phone = ""
    var key = ""
    var gender = ""
    var region = ""
    var displayphone = ""
    var is_confirmed = false
    var birthday:Double = 0
    var lastname = ""
    var surname = ""
    var email = ""
    var visible = 0
    let limitLength = 20
    var fcm_token = ""
    var name = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(backtoROot))
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Mazzi".uppercased()
        self.profilePic.layer.borderColor = GlobalVariables.purple.cgColor
        self.profilePic.layer.borderWidth = 0.5
        self.navigationItem.hidesBackButton = true
        self.username.delegate = self
//        self.errorlabel.isHidden = true
        self.username.placeholder = "Таны нэр"
        userid = (userDefaults.object(forKey: "user_id") as? String)!
        let userInfo = userDefaults.dictionary(forKey: "userInfo")
        displayphone = userInfo!["displayphone"] as! String
        let name = userInfo!["nickname"] as! String
        region = userInfo!["region"] as! String
        phone = (userDefaults.object(forKey: "phone") as? String)!
        self.username.text = name
        self.malecheckbtn.isSelected = true
        self.gender = "male"
        self.username.textColor = GlobalVariables.purple
        self.mail.textColor =     GlobalVariables.purple
        self.password.textColor = GlobalVariables.purple
        self.beginMazziBtn.setTitleColor(GlobalVariables.TextTod, for: UIControl.State.normal)

        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x:0.0, y:username.frame.height - 1, width:username.frame.width, height: 1.0)
        bottomLine.backgroundColor = GlobalVariables.lightGray.cgColor
        username.borderStyle = UITextField.BorderStyle.none
        username.layer.addSublayer(bottomLine)
        
        let bottomLine3 = CALayer()
        bottomLine3.frame = CGRect(x:0.0, y:mail.frame.height - 1, width:mail.frame.width, height: 1.0)
        bottomLine3.backgroundColor = GlobalVariables.lightGray.cgColor
        mail.borderStyle = UITextField.BorderStyle.none
        mail.layer.addSublayer(bottomLine3)
        
        let bottomLine4 = CALayer()
        bottomLine4.frame = CGRect(x:0.0, y:password.frame.height - 1, width:password.frame.width, height: 1.0)
        bottomLine4.backgroundColor = GlobalVariables.lightGray.cgColor
        password.borderStyle = UITextField.BorderStyle.none
        password.layer.addSublayer(bottomLine4)
        
        self.profilePic.image = UIImage(named:"circle-avatar" )
        
        self.malecheckbtn.setImage(UIImage(named:"Ellipse 3"), for: UIControl.State.selected)
        self.malecheckbtn.setImage(UIImage(named:"Ellipse 2"), for: UIControl.State.normal)
        
        self.femaleCheckBtn.setImage(UIImage(named:"Ellipse 3"), for: UIControl.State.selected)
        self.femaleCheckBtn.setImage(UIImage(named:"Ellipse 2"), for: UIControl.State.normal)
        
        if self.malecheckbtn.isSelected == true{
            maleLabel.textColor = GlobalVariables.todpurple
        }else{
            maleLabel.textColor = GlobalVariables.todpurple.withAlphaComponent(0.5)
        }
        
        if self.femaleCheckBtn.isSelected == true{
            femaleLabel.textColor = GlobalVariables.todpurple
        }else{
            femaleLabel.textColor = GlobalVariables.todpurple.withAlphaComponent(0.5)
        }
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
    }
    
    @objc func  backtoROot(){
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(CreateProfileController.showKeyboard(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        self.view.layoutIfNeeded()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }

    @objc func showKeyboard(notification: Notification) {
        if let frame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let height = frame.cgRectValue.height
            self.mySCrollView.contentInset.bottom = height
            self.mySCrollView.scrollIndicatorInsets.bottom = height
            if height > 100{
                let keyboardHeight = Int(height.rounded(.toNearestOrEven))
                if(userDefaults.object(forKey: "keyboardheight") == nil){
                    self.userDefaults.set(keyboardHeight, forKey: "keyboardheight")
                }
            }
        }
    }
    
    @IBAction func insertPic(_ sender: Any) {
            self.performSegue(withIdentifier: "goCamera", sender: self)
    }
    
    @IBAction func male(_ sender: Any) {
        
        self.gender = "male"
        self.femaleCheckBtn.isSelected = false
        self.malecheckbtn.isSelected = true
        
         self.malecheckbtn.setImage(UIImage(named:"Ellipse 3"), for: UIControl.State.selected)
         self.malecheckbtn.setImage(UIImage(named:"Ellipse 2"), for: UIControl.State.normal)
        
        self.femaleCheckBtn.setImage(UIImage(named:"Ellipse 3"), for: UIControl.State.selected)
        self.femaleCheckBtn.setImage(UIImage(named:"Ellipse 2"), for: UIControl.State.normal)
        
        if self.malecheckbtn.isSelected == true  {
            maleLabel.textColor = GlobalVariables.todpurple
            femaleLabel.textColor = GlobalVariables.todpurple.withAlphaComponent(0.5)
        }else{
            maleLabel.textColor = GlobalVariables.todpurple.withAlphaComponent(0.5)
            
        }
        
    }
    @IBAction func female(_ sender: Any) {
        
        self.gender = "female"
        self.malecheckbtn.isSelected = false
        self.femaleCheckBtn.isSelected = true
        
        self.femaleCheckBtn.setImage(UIImage(named:"Ellipse 3"), for: UIControl.State.selected)
        self.femaleCheckBtn.setImage(UIImage(named:"Ellipse 2"), for: UIControl.State.normal)
        
        self.malecheckbtn.setImage(UIImage(named:"Ellipse 3"), for: UIControl.State.selected)
        self.malecheckbtn.setImage(UIImage(named:"Ellipse 2"), for: UIControl.State.normal)
        
        if self.femaleCheckBtn.isSelected == true {
            femaleLabel.textColor = GlobalVariables.todpurple
            maleLabel.textColor = GlobalVariables.todpurple.withAlphaComponent(0.5)
        }else{
            femaleLabel.textColor = GlobalVariables.todpurple.withAlphaComponent(0.5)
        }
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        username.resignFirstResponder()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (range.location == 0 && string == " ") {
            return false
        }else{
            if self.username.text?.count != 0{
                self.beginMazziBtn.isEnabled = true
            }
            return true
        }
       
    }
    
    func validation(){
        if username.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")) == "" {
            showAlert(title: "Нэр оруулна уу!")
            return
        }
        
        if mail.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")) == "" {
            showAlert(title: "Имэйл оруулна уу!")
            return
            
        }else if !Utils.isValidEmail(testStr: mail.text!){
            showAlert(title: "Зөв имэйл оруулна уу!")
            return
        }
        
        if password.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")) == "" {
            showAlert(title: "Нууц үг оруулна уу!")
            return
        }else if !Utils.isValidPass(testStr: password.text!) {
            showAlert(title: "Нууц үг буруу!",message:"Багадаа нэг үсэг болон тоо орсон 6 болон түүнээс дээш урттай нууц үг оруулна уу!")
            return
        }
        
       self.imgUpload()
    }
    
    @IBAction func beginMazziBtn(_ sender: Any) {
        validation()
    }
    
    func BeginMazzi(){
        KVNProgress.show()
        let profile = ["_id":userid,"image":image,"nickname": self.username.text!, "phone": self.phone, "key": key, "region": region, "lastname": lastname,  "surname": surname, "birthday": birthday,"email":self.mail.text!, "gender":gender, "fcm_token":fcm_token, "password": self.password.text! , "registerId": ""] as [String : Any]
        
        if self.username.text! != "" {

              KVNProgress.show()
            User_group.updateprofile(profile: profile, userids: userid, completion: { (status) in
                self.userDefaults.set(self.password.text!, forKey: "pass")
                if status == true {
                    
                    DispatchQueue.main.async{
                       
                        let userInfo = ["image": self.image,
                                        "nickname":self.username.text!,
                                        "lastname": self.lastname,
                                        "registerId":"",
                                        "birthday":self.birthday,
                                        "email":self.mail.text!,
                                        "gender":self.gender,
                                        "region":self.region,
                                        "displayphone": self.displayphone,
                                        "is_confirmed": self.is_confirmed
                            ] as [String : Any]
                        print("LOGINNAMEVC:: ",userInfo)
                        self.userDefaults.set(userInfo, forKey: "userInfo")
                        SettingsViewController.userId = self.userid
                        self.userDefaults.synchronize()
                    }
                    KVNProgress.dismiss()
                    let when = DispatchTime.now() // change 2 to desired number of seconds
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        if self.visible == 1 {
                            self.viewDidLoad()
                            self.dismiss(animated: true, completion: nil)
                        }else{
                            Conversation.getconversations(userid: self.userid, limit: 40, skip: 0) { (conversations) in }
                            let time = DispatchTime.now()
                            DispatchQueue.main.asyncAfter(deadline: time) {
                                GlobalVariables.isloggedin = "true"
                                self.userDefaults.set(GlobalVariables.isloggedin, forKey: "isLogin")
                                self.userDefaults.synchronize()
                                let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainnav") as! MainNavViewController
                                self.present(viewController, animated: true, completion: nil)
                            }
                        }
                        KVNProgress.dismiss()
                    }
                }
            })
        }else{

        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>,
                               with event: UIEvent?){
        if let touch = touches.first {
            print(touch)
            self.username.resignFirstResponder()
            self.password.resignFirstResponder()
            self.mail.resignFirstResponder()
        }
        super.touchesBegan(touches, with: event)
    }
    
    func showAlert(title: String? = nil, message: String? = nil){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(ok)
        present(alertController, animated: true, completion: nil)
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        username.resignFirstResponder()
        mail.resignFirstResponder()
        password.resignFirstResponder()
        return true
    }
    
    @objc func dismissKeyboard(){
        username.resignFirstResponder()
        mail.resignFirstResponder()
        password.resignFirstResponder()
    }
    
    func photoGet(capturedImage: UIImage) {
        self.profilePic.image = capturedImage
    }
    
    func imgUpload(){
        let userid = userDefaults.object(forKey: "user_id") as? String
        KVNProgress.show()
        Message.uploadimage(chatId: userid! ,image: self.profilePic.image!, completion: { (state, imageurl) in
             KVNProgress.dismiss()
            DispatchQueue.main.async {
                let url = JSON(imageurl)
                self.image = url["url"].string!
//                let imageurl = URL(string:"\(self.fileurl)\(url["url"].string!)")!
//                self.profilePic.sd_setImage(with: imageurl, completed: nil)
                self.BeginMazzi()
               
            }
        })
    }
    
    func videoGet(clipedVideo: URL) {
        print("video didn't support")
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goCamera" {
            GlobalVariables.isChat = false
            let camera = segue.destination as? CameraView
            camera?.disable = true
            camera?.myProtocol = self
        }
    }
    
}
