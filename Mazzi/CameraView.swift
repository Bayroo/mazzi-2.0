//
//  CameraView.swift
//  Mazzi
//
//  Created by Janibekm on 10/19/17.
//  Copyright © 2017 Mazzi App LLC. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import Photos
import MobileCoreServices

protocol myProtocol {
    func photoGet(capturedImage: UIImage)
    func videoGet(clipedVideo: URL)
}

class CameraView: UIViewController, AVCaptureFileOutputRecordingDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate  {
    
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var captureButton: UIButton!
    @IBOutlet weak var galarei: UIButton!
    @IBOutlet weak var switchCam: UIButton!
    @IBOutlet weak var backBnt: UIButton!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    
    let captureSession = AVCaptureSession()
    var backCamera: AVCaptureDevice?
    var frontCamera: AVCaptureDevice?
    var currentDevice: AVCaptureDevice?
    var videoFileOutput:AVCaptureMovieFileOutput?
    var audioFileOutput = AVCaptureDevice.default(for: AVMediaType.audio)
    var photoOutput: AVCapturePhotoOutput?
    var cameraPreviewLayer:AVCaptureVideoPreviewLayer?
    var isRecording = false
    var image: UIImage?
    var toggleCameraGestureRecognizer = UISwipeGestureRecognizer()
    var zoomInGestureRecognizer = UISwipeGestureRecognizer()
    var zoomOutGestureRecognizer = UISwipeGestureRecognizer()
    var btnVideo = true
    var disable = false
    static var visible = 1
    var myProtocol: myProtocol? = nil
    var outputURL: URL!
    let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCaptureSession()
        setupDevice()
        setupInput()
        setupPreviewLayer()
        startRunningCaptureSession()
        self.imagePicker.delegate = self
        toggleCameraGestureRecognizer.direction = .up
        toggleCameraGestureRecognizer.addTarget(self, action: #selector(self.switchCamera))
        view.addGestureRecognizer(toggleCameraGestureRecognizer)
        zoomInGestureRecognizer.direction = .right
        zoomInGestureRecognizer.addTarget(self, action: #selector(zoomIn))
        view.addGestureRecognizer(zoomInGestureRecognizer)
        zoomOutGestureRecognizer.direction = .left
        zoomOutGestureRecognizer.addTarget(self, action: #selector(zoomOut))
        view.addGestureRecognizer(zoomOutGestureRecognizer)
        self.captureBtn()
         self.descriptionLabel.isHidden = true
        if GlobalVariables.isChat{
          
             self.descriptionLabel.isHidden = false
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(Int(3))) {
                self.descriptionLabel.isHidden = true
            }
            let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(Long))
            // longGesture.allowableMovement = CGFloat.infinity
            self.captureButton.addGestureRecognizer(longGesture)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        if CameraView.visible == 0 {
            CameraView.visible = 1
            if self.outputURL == nil {
                self.myProtocol?.photoGet(capturedImage: self.image!)
            }else{
                self.myProtocol?.videoGet(clipedVideo: self.outputURL)
            }
            self.dismiss(animated: false, completion: nil)
        }else{
            self.image = nil
            self.outputURL = nil
        }
    }
    
    @objc func Long(gesture: UILongPressGestureRecognizer){
        
        if gesture.state == .began{
            self.captureButton.setImage(UIImage(named: "RecBtn"), for: UIControl.State.normal)
            setupCaptureSession()
            self.btnVideo = true
            setupOutput()
            isRecording = true
            let connection = videoFileOutput?.connection(with: AVMediaType.video)
            if (connection?.isVideoOrientationSupported)! {
                connection?.videoOrientation = currentVideoOrientation()
            }
            let when = DispatchTime.now() + 0.5
            DispatchQueue.main.asyncAfter(deadline: when) {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: [.repeat, .autoreverse, .allowUserInteraction], animations: { () -> Void in
                    self.captureButton.transform = CGAffineTransform(scaleX: 1.6, y: 1.6)
                }, completion: nil)
                
                let outputPath = NSTemporaryDirectory() + "output.mp4"
                let outputFileURL = URL(fileURLWithPath: outputPath)
                self.videoFileOutput?.startRecording(to: outputFileURL, recordingDelegate: self)
            }
            startRunningCaptureSession()
        }
        else if gesture.state == .ended{
             self.captureButton.setImage(UIImage(named: "shot"), for: UIControl.State.normal)
            isRecording = false
            UIView.animate(withDuration: 0.5, delay: 1.0, options: [], animations: { () -> Void in
                self.captureButton.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: nil)
            recordButton.layer.removeAllAnimations()
            videoFileOutput?.stopRecording()
        }
        else if gesture.state == .changed{
            
        }
    }

    func captureBtn(){
        captureButton.layer.borderColor =  UIColor.rbg(r:243 , g:243 , b:243).cgColor
        captureButton.layer.borderWidth = 0
        captureButton.clipsToBounds = true
        captureButton.layer.cornerRadius = min(captureButton.frame.width, captureButton.frame.height) / 2
      // captureButton.layer.shadowOffset = CGSize(width: 0, height: 3)
     //  captureButton.layer.shadowOpacity = 0.5
     //  captureButton.layer.shadowRadius = 10.0
        captureButton.backgroundColor =  UIColor.rbg(r:243 , g:243 , b:243)
       
        recordButton.layer.borderColor = UIColor.white.cgColor
        recordButton.layer.borderWidth = 2
        recordButton.clipsToBounds = true
        recordButton.layer.cornerRadius = min(recordButton.frame.width, recordButton.frame.height) / 4
        recordButton.layer.shadowOffset = CGSize(width: 0, height: 3)
        recordButton.layer.shadowOpacity = 0.5
        recordButton.layer.shadowRadius = 10.0
        
        galarei.layer.borderColor = UIColor.white.cgColor
        galarei.layer.borderWidth = 0
        galarei.clipsToBounds = true
        galarei.layer.cornerRadius =  min(galarei.frame.width, galarei.frame.height) / 2
        galarei.layer.shadowOffset = CGSize(width: 0, height: 3)
        galarei.layer.shadowOpacity = 0.5
        galarei.layer.shadowRadius = 10.0
        galarei.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        
        switchCam.layer.shadowOffset = CGSize(width: 0, height: 3)
        switchCam.layer.shadowOpacity = 0.5
        switchCam.layer.shadowRadius = 10.0
        backBnt.layer.shadowOffset = CGSize(width: 0, height: 3)
        backBnt.layer.shadowOpacity = 0.1
        backBnt.layer.shadowRadius = 10.0
        
    }
    func setupCaptureSession() {
        captureSession.sessionPreset = AVCaptureSession.Preset.high
    }
    
    func setupDevice() {
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
        
        let devices = deviceDiscoverySession.devices
      
        for device in devices {
            if device.position == AVCaptureDevice.Position.back {
                backCamera = device
            } else if device.position == AVCaptureDevice.Position.front {
                frontCamera = device
            }
        }
        currentDevice = backCamera
    }
    func setupInput(){
        do{
            let captureDeviceInput = try AVCaptureDeviceInput(device: currentDevice!)
            captureSession.addInput(captureDeviceInput)
        }catch{
            print("ERROR ")
        }
    }
    func setupOutput() {
        if self.btnVideo == true{
            videoFileOutput = AVCaptureMovieFileOutput()
            audioFileOutput = AVCaptureDevice.default(for: AVMediaType.audio)
            captureSession.addOutput(videoFileOutput!)
            try! captureSession.addInput(AVCaptureDeviceInput(device: audioFileOutput!))
        }else{
            photoOutput = AVCapturePhotoOutput()
            if #available(iOS 11.0, *) {
                photoOutput!.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format: [AVVideoCodecKey : AVVideoCodecType.jpeg])], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
            captureSession.addOutput(photoOutput!)
        }
    }
   
    func setupPreviewLayer() {
        
        cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        cameraPreviewLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
        cameraPreviewLayer?.frame = self.view.frame
        self.view.layer.insertSublayer(cameraPreviewLayer!, at: 0)
    }
    
    func startRunningCaptureSession() {
        captureSession.startRunning()
    }
    // MARK: - Action methods
    
    @IBAction func capture(sender: UIButton) {
        if !isRecording {
            setupCaptureSession()
            self.btnVideo = true
            setupOutput()
             isRecording = true
            let connection = videoFileOutput?.connection(with: AVMediaType.video)
            if (connection?.isVideoOrientationSupported)! {
                connection?.videoOrientation = currentVideoOrientation()
            }
            
            let when = DispatchTime.now() + 0.5
            DispatchQueue.main.asyncAfter(deadline: when) {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: [.repeat, .autoreverse, .allowUserInteraction], animations: { () -> Void in
                    self.recordButton.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
                }, completion: nil)
                
                let outputPath = NSTemporaryDirectory() + "output.mp4"
                let outputFileURL = URL(fileURLWithPath: outputPath)
                self.videoFileOutput?.startRecording(to: outputFileURL, recordingDelegate: self)
            }
             startRunningCaptureSession()
        } else {
            isRecording = false
            UIView.animate(withDuration: 0.5, delay: 1.0, options: [], animations: { () -> Void in
                self.recordButton.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: nil)
            recordButton.layer.removeAllAnimations()
            videoFileOutput?.stopRecording()
        }
        
    }
    
    @IBAction func capturePhoto(_ sender: Any) {
        self.btnVideo = false
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
        setupOutput()
      
        let settings = AVCapturePhotoSettings()
    
        let connection = self.photoOutput?.connection(with: AVMediaType.video)
        if (connection?.isVideoOrientationSupported)! {
            connection?.videoOrientation = self.currentVideoOrientation()
        }

        let when = DispatchTime.now() + 0.5
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.photoOutput?.capturePhoto(with: settings, delegate: self)
        }
        
        self.startRunningCaptureSession()
       
    }
   
    // MARK: - AVCaptureFileOutputRecordingDelegate methods
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        if error != nil {
            print(" CameraView- file output: \(String(describing: error))")
            return
        }
        performSegue(withIdentifier: "preview", sender: outputFileURL)
    }
    func currentVideoOrientation() -> AVCaptureVideoOrientation {
        var orientation: AVCaptureVideoOrientation
        switch UIDevice.current.orientation {
        case .portrait:
            orientation = AVCaptureVideoOrientation.portrait
        case .landscapeRight:
            orientation = AVCaptureVideoOrientation.landscapeLeft
        case .portraitUpsideDown:
            orientation = AVCaptureVideoOrientation.portraitUpsideDown
        default:
            orientation = AVCaptureVideoOrientation.landscapeRight
        }
        return orientation
    }
    @IBAction func switchCam(_ sender: Any) {
        self.switchCamera()
    }
    @objc func switchCamera() {
        captureSession.beginConfiguration()
        // Change the device based on the current camera
        let newDevice = (currentDevice?.position == AVCaptureDevice.Position.back) ? frontCamera : backCamera
        
        // Remove all inputs from the session
        for input in captureSession.inputs {
            captureSession.removeInput(input as! AVCaptureDeviceInput)
        }
        // Change to the new input
        let cameraInput:AVCaptureDeviceInput
        do {
            cameraInput = try AVCaptureDeviceInput(device: newDevice!)
        } catch {
            print(" CameraView- switch camera: \(error)")
            return
        }
        
        if captureSession.canAddInput(cameraInput) {
            captureSession.addInput(cameraInput)
        }
        
        currentDevice = newDevice
        captureSession.commitConfiguration()
    }
    
    @objc func zoomIn() {
        if let zoomFactor = currentDevice?.videoZoomFactor {
            if zoomFactor < 5.0 {
                let newZoomFactor = min(zoomFactor + 1.0, 5.0)
                do {
                    try currentDevice?.lockForConfiguration()
                    currentDevice?.ramp(toVideoZoomFactor: newZoomFactor, withRate: 1.0)
                    currentDevice?.unlockForConfiguration()
                } catch {
                    print(" CameraView-zoomIn : \(error)")
                }
            }
        }
    }
    
    @objc func zoomOut() {
        if let zoomFactor = currentDevice?.videoZoomFactor {
            if zoomFactor > 1.0 {
                let newZoomFactor = max(zoomFactor - 1.0, 1.0)
                do {
                    try currentDevice?.lockForConfiguration()
                    currentDevice?.ramp(toVideoZoomFactor: newZoomFactor, withRate: 1.0)
                    currentDevice?.unlockForConfiguration()
                } catch {
                    print(" CameraView - zoomOut: \(error)")
                }
            }
        }
    }
    @IBAction func backtoChat(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func galarei(_ sender: Any) {
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == .authorized || status == .notDetermined) {
            self.imagePicker.sourceType = .photoLibrary
            if disable == true {
                self.imagePicker.allowsEditing = true
            }else{
                self.imagePicker.allowsEditing = false
                self.imagePicker.mediaTypes = [(kUTTypeMovie as String), kUTTypeImage as String]
            }
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        if let videoURL = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.mediaURL)] as? URL {
              CameraView.visible = 0
            self.outputURL = videoURL
        }else if let pickedImage1 = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            CameraView.visible = 0
            self.image = pickedImage1
        }else if let pickedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as? UIImage {
            CameraView.visible = 0
            self.image = pickedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "preview" {
            let vc = segue.destination as! CameraPreview
            if self.btnVideo == true{
                self.outputURL = sender as? URL
                vc.videoURL = sender as? URL
            }else{
                vc.takenPhoto = sender as? UIImage
            }
            DispatchQueue.main.async {
                let outputs = self.captureSession.outputs
                for output in outputs {
                    self.captureSession.removeOutput(output)
                }
            }
        }
    }
}
extension CameraView: AVCapturePhotoCaptureDelegate {
    @available(iOS 11.0, *)
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        if let imageData = photo.fileDataRepresentation() {
            self.image = UIImage(data: imageData)
            performSegue(withIdentifier: "preview", sender: self.image)
        }
    }
}



// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
