//
//  GlobalFunctions.swift
//  Mazzi
//
//  Created by janibekm on 9/3/18.
//  Copyright © 2018 Mazzi App LLC. All rights reserved.
//

import Foundation
import UIKit
import SocketIO

struct GlobalStaticVar {
    static var baseUrl =    "https://mazzi.mn/rest/"
    static var fileUrl =    "https://mazzi.mn/file/"
    static var socketUrl =  "https://mazzi.mn/"
    static var graphql =    "https://mclass.mazzi.mn/backend/graphql"// "http://mazzi.mn:10000/graphql"  "http://192.168.0.105:10000/graphql"
    static var cdiofileURL =  "https://mclass.mazzi.mn/file/"
    static var cdiosocketUrl =   "https://mclass.mazzi.mn"  // "http://192.168.0.111:10001"
    static var userID = ""
    var manager:SocketManager?
    static var badgecount = [String]()
    static var AppBadgeCount = 0
    static var clickedNotification = ""
    static var selectedConversationId = ""
    static var activeConversationId = ""
    static var selectedClassId = ""
    static var classId = ""
    static var teacherId = ""
    static var className = ""
    static var classCode = ""
    static var typingConversations = [TypingConversations]()
    static var isPostCreate = false
    static var isLaunchEnd = false
    static var isEnd = false
    static var roomName = ""
    static var isPendingPost = false
    static var isUpdateAttendance = false
    static var classRole = ""
    static var topics = [String]()
    static var selectedQuestionIndexFromLaunch = 0
    
    static var notifiedClassId = ""
    static var notifiedPostId = ""
    static var notifiedRoodPostId = ""
    static var notifiedType = ""
    static var notifiedComment = false
    static var newPost = false
    static var FromNotify = false
    static var isApprovePost = false
}

func getFireUrl(url: String, completion: @escaping (Bool) -> Swift.Void)  {
    ref.observe(.value) { (snapshot) in
        let value = snapshot.value as? NSDictionary
        let baseUrls =   value?["baseUrls"] as? String ?? ""
        let fileUrls =   value?["fileUrls"] as? String ?? ""
        let socketUrls = value?["socketUrls"] as? String ?? ""
        let graphqls = value?["graphQL"] as? String ?? ""
        let cdiofileURL = value?["cdioFileUrl"] as? String ?? ""
        let cdiosocketurl = value?["cdioSocketUrl"] as? String ?? ""
        
//        print("maingraphqlurl::", GlobalStaticVar.graphql)
//         print("Dynamic graphqlurl::", graphqls)
//        print("-----------------------------------------")
//        print("base: \(SettingsViewController.baseurl)")
        if GlobalStaticVar.baseUrl != baseUrls || GlobalStaticVar.fileUrl != fileUrls || GlobalStaticVar.socketUrl != socketUrls || GlobalStaticVar.graphql != graphqls  || GlobalStaticVar.cdiofileURL != cdiofileURL || GlobalStaticVar.cdiosocketUrl != cdiosocketurl {
//            print("SOCKET URL :: ", socketUrls)
            GlobalStaticVar.baseUrl = baseUrls
            SettingsViewController.baseurl = baseUrls
            SettingsViewController.fileurl = fileUrls
            SettingsViewController.socketUrl = socketUrls
            SettingsViewController.graphQL = graphqls
            SettingsViewController.cdioFileUrl = cdiofileURL
            SettingsViewController.cdiosocketUrl = cdiosocketurl

        }
        
        if GlobalStaticVar.fileUrl != fileUrls {
            GlobalStaticVar.fileUrl = fileUrls
        }
        
        if GlobalStaticVar.socketUrl != socketUrls {
            GlobalStaticVar.socketUrl = socketUrls
        }
        
    }
}

func badgeUpdate(conid: String){
    if !GlobalStaticVar.badgecount.contains(conid){
        GlobalStaticVar.badgecount.append(conid)
    }
//    UIApplication.shared.applicationIconBadgeNumber = GlobalStaticVar.AppBadgeCount
}

func getBadgeCount(){
    UIApplication.shared.applicationIconBadgeNumber = GlobalStaticVar.AppBadgeCount
}

func bagdeRemoveItem(conid:String){
    if let index = GlobalStaticVar.badgecount.index(of: conid) {
        GlobalStaticVar.badgecount.remove(at: index)
//        UIApplication.shared.applicationIconBadgeNumber = GlobalStaticVar.AppBadgeCount
    }
}
