//
//  joinedUserCell.swift
//  Mazzi
//
//  Created by Bayara on 1/11/19.
//  Copyright © 2019 woovoo. All rights reserved.
//

import Foundation

class joinedUserCell: UITableViewCell{
    
    @IBOutlet weak var imgVIew: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timelabel: UILabel!
    @IBOutlet weak var lastnameLabel: UILabel!
}
