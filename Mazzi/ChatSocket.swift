//
//  ChatSocket.swift
//  Mazzi
//
//  Created by Mazzi iOS on 12/10/18.
//  Copyright © 2018 Mazzi Application. All rights reserved.
//

import Foundation
import UIKit
import SocketIO
import Alamofire
import RealmSwift
import SwiftyJSON
import AudioToolbox

//var manager  = SocketManager(socketURL: URL(string: SettingsViewController.socketUrl)!, config: [ .compress,.connectParams(["userId" : GlobalVariables.user_id ]),.extraHeaders(["authorization" :  SettingsViewController.secureToken]) ,.secure(false) ,.forceNew(true) ,.forceWebsockets(true)])
//let socket = manager.defaultSocket

var manager  = SocketManager(socketURL: URL(string: SettingsViewController.socketUrl)!, config: [ .compress,.connectParams(["userId" : GlobalVariables.user_id , "token": GlobalVariables.headerToken ]),.extraHeaders(["authorization" :  SettingsViewController.secureToken]) ,.secure(false) ,.forceNew(true) ,.forceWebsockets(true)])
 var socket = manager.defaultSocket
 var socketInitTime = DispatchTime.now()

func socketInitialize(){
    
    manager  = SocketManager(socketURL: URL(string: SettingsViewController.socketUrl)!, config: [ .compress,.connectParams(["userId" : GlobalVariables.user_id , "token": GlobalVariables.headerToken ]),.extraHeaders(["authorization" :  SettingsViewController.secureToken]) ,.secure(false) ,.forceNew(true) ,.forceWebsockets(true)])
    socket = manager.defaultSocket
    
    print("---SOCKET INITIALIZE---")

    socketInitTime = DispatchTime.now()
    socket.on(clientEvent: .connect) {data, ack in
        
//        print("CHAT SOCKET CONNECTED")
        let data = ["socketIsConnected": "true"]
        userDefaults.set("true", forKey: "socketIsConnected")
        socketInitTime = DispatchTime.now()
        NotificationCenter.default.post(name: .socketIsConnected, object: nil, userInfo: data)
        //todojani eniig login success bolson ued shalgana uu?
        //ygd gevel socket urgelj salj niileh uchir ter bolgond userdefaulr deer hereggui uildel ih hiih yum bn!!!!
        
//        checkInternet()
    }
    
    socket.on(clientEvent: .error){data, ack in
        print("SOCKET ERROR ::: ", data)
    }
    socket.on(clientEvent: .disconnect) { (data, SocketAckEmitter) in
       print("SOCKET DISCONECTED")
//          AlertBar.show(error: "fsefsefsefsefsef" as! Error)
    }
    
    socket.on("user_status"){data, ack in
        let status = JSON(data)
        let userid = status[0]["user_id"].string!
        let online_at = status[0]["online_at"].double!
        let userconnected = status[0]["is_connected"].bool!
        var state = 0
        if userconnected == true {
            state = 1
        }else{
            state = 0
        }
        if userid != "" {
            //let contact = self.contacts.filter("id = '\(userid)'")
            let find = realm.object(ofType: Contacts_realm.self, forPrimaryKey: userid)
            if find != nil {
                //self.contacts = realm.objects(Contacts_realm.self)
                //let contact = self.contacts.filter("id = '\(userid)'")
                let contact = try! Realm().objects(Contacts_realm.self).filter("id = '\(userid)'")
                if contact[0].id != ""{
                    var phones = [String]()
                    for p in contact[0].phones{
                        phones.append(p.phones)
                    }
                    let cont = Contacts.init(id: contact[0].id, name: contact[0].name, lastname: contact[0].lastname, surname: contact[0].surname, image: contact[0].image, phones: phones, phone: contact[0].phone, gender: contact[0].gender, birthday: contact[0].birthday, state: state, online_at: online_at, created_at: Double(contact[0].created_at), fcm_token: contact[0].fcm_token)
                    RealmService.updateContactRealm(updateContacts: cont, completion: { (success) in
                        if success == true {
                            //updated successfully
                            NotificationCenter.default.post(name: .user_status, object: cont, userInfo: nil)
                        }
                    })
                }
            }
            let findusergroup = realm.object(ofType: User_group_realm.self, forPrimaryKey: userid)
            if findusergroup != nil {
                //user_group = realm.objects(User_group_realm.self)
                let user_grp = try! Realm().objects(User_group_realm.self).filter("id = '\(userid)'")
                if user_grp[0].id != ""{
                    let users = User_group.init(id: user_grp[0].id, image: user_grp[0].image, nickname: user_grp[0].nickname, lastname: user_grp[0].lastname, surname: user_grp[0].surname, phone: user_grp[0].phone, state: state, online_at: online_at, fcm_token: user_grp[0].fcm_token, registerId: user_grp[0].register)
                    RealmService.updateUserGroupRealm(updateUserGroupUser: users, completion: { (success) in
                        //print("success")
                        NotificationCenter.default.post(name: .user_status, object: users, userInfo: nil)
                    })
                }
            }
        }
    }
    
    socket.on("receive_message"){data, ack in
        let chat = JSON(data)
        let message = chat[0]
        
        let id = message["_id"].string!
        let owner_id = message["owner_id"].string!
        let name = message["name"].string ?? ""
        let content = message["content"].string ?? ""
        //zasvar
        let filename = message["filename"].string!
        let file_size = message["file_size"].int!
        let type = message["type"].string!
        let seen = message["seen"].arrayValue.map({$0.stringValue})
        let created_at = message["created_at"].double!
        let conversation_id = message["conversation_id"].string!
        let duration = message["duration"].double!
        
        if id != ""{
            let find = realm.object(ofType: Conversation_realm.self, forPrimaryKey: conversation_id)
            if find != nil{
                let con = realm.objects(Conversation_realm.self).filter("id = '\(conversation_id)'")
                let lastmessage = Message.init(id: id, owner_id: owner_id, content: content, filename: filename, file_size: file_size, type: type, seen: seen,  created_at: created_at, duration: duration,conversation_id:conversation_id,name:name)
                
                var showing = [String]()
                for sh in con[0].show{
                    showing.append(sh.show)
                }
                
                var user_grouping = [User_group]()
                for users in con[0].user{
                    let user = User_group.init(id: users.id, image: users.image, nickname: users.nickname, lastname: users.lastname, surname: users.surname, phone: users.phone, state: users.state, online_at: Double(users.online_at), fcm_token: users.fcm_token,registerId: users.register)
                    user_grouping.append(user)
                }
                
                let conver = Conversation.init(user: user_grouping, lastmessage: lastmessage, id: con[0].id, title: con[0].title, created_at: created_at, is_thread: con[0].is_thread, show: showing, secret: con[0].secret, image: con[0].image,owner_id_con: owner_id)
               
                RealmService.updateConversationToRealm(udpateConversation: conver, completion: { (success) in
                    DispatchQueue.main.async {
                        badgeUpdate(conid: conversation_id)
                        let datas = ["conversationId": conversation_id]
                        NotificationCenter.default.post(name: .receive_message, object: lastmessage, userInfo: datas)
//                        playSound()
                    }
                })
            }else{
                Conversation.get_singleCon(con_id: conversation_id, completion: { (conver) in
                    DispatchQueue.main.async {
                        print("GGE")
                        let datas = ["conversationId": conversation_id]
                        NotificationCenter.default.post(name: .receive_message, object: nil, userInfo: datas)
                    }
                })
            }
        }
    }
    
    socket.on("receive_group"){data, ack in
        let group = JSON(data)
        let conversation_id = group[0]["conversation_id"].string ?? ""
        let when = DispatchTime.now()+3
        DispatchQueue.main.asyncAfter(deadline: when, execute: {
            let find = realm.object(ofType: Conversation_realm.self, forPrimaryKey: conversation_id)
            if find != nil{
                Conversation.get_singleCon(con_id: conversation_id, completion: { (conver) in
                    DispatchQueue.main.async {
                        print("GGEeeee")
                        let datas =  ["conversationId":conversation_id]
                        NotificationCenter.default.post(name: .receive_group, object: nil, userInfo: datas)
                    }
                })
            }
        })
    }
    
    socket.on("user_status"){data, ack in
        let time = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: time, execute: {
            if realm.objects(User_group_realm.self).count != 0 {
//                if self.isallChat {
//                    self.items = realm.objects(Conversation_realm.self).sorted(byKeyPath: "updated_at", ascending: false)
//                }else{
//                    self.items = realm.objects(Conversation_realm.self).filter("is_thread != 0").sorted(byKeyPath: "updated_at", ascending: false)
//                }

            }
//            self.listTableView.reloadData()
        })
    }
    
    socket.on("typing_receive"){data, ack in
        let type = JSON(data)
//        self.typingTime = DispatchTime.now() + 2
        print(type)
        let conversation_id = type[0]["conversation_id"].string ?? ""
        let typing = type[0]["typing"].bool ?? false
        let owner_id = type[0]["owner_id"].string ?? ""
        
        let typer = TypingConversations.init(ownerid: owner_id, conversation_id: conversation_id, typing: typing, time:    DispatchTime.now())
        NotificationCenter.default.post(name: .typing_receive, object: typer, userInfo: nil)
        
    }
    
    socket.on("statusChange"){(data,ack) in
        print("CHAT SOCKET STATUSCHANGE:: ", data)
    }
    
    
    socket.on("participant_left"){data, ack in
        let leftUser = JSON(data)
//        print("huur chin garchilaa")
        print(leftUser)
        let user_id = leftUser[0]["user_id"].string ?? ""
        let con_id = leftUser[0]["conversation_id"].string ?? ""
        let success = leftUser[0]["success"].bool ?? false
        let content = leftUser[0]["content"].string ?? ""
        let created_at = leftUser[0]["created_at"].double ?? 0
        let type = leftUser[0]["type"].string ?? "text"
        if success == true {
            if user_id == SettingsViewController.userId {
                Conversation.leaveConversation(conversation_id: con_id, completion: { (con) in
                    if con == true {
                        RealmService.deleteConversationFromRealm(deleteConversation: con_id, completion: { (succ) in
                            if succ == true {
//                                _ = self.navigationController?.popToRootViewController(animated: true)
                                let datas = ["conversationId": con_id]
                                NotificationCenter.default.post(name: .participant_left, object: nil, userInfo: datas)
                            }
                        })
                    }
                })
            } else {
                let userInfo = userDefaults.dictionary(forKey: "userInfo")
                let myname = userInfo!["nickname"] as! String
                let msg = Message.init(id: "ids\(Date().timeIntervalSinceNow)", owner_id: user_id, content: content, filename: "", file_size:0, type: type, seen: [user_id], created_at: created_at, duration: 0 , conversation_id: con_id,name: myname)
                let datas = ["conversationId": con_id]
                NotificationCenter.default.post(name: .receive_message, object: msg, userInfo: datas)
            }
        }
    }
    
    socket.on("marked_message"){data, ack in
        let json = JSON(data)
        print(json)
        let message_id = json[0]["message_id"].string ?? ""
        let user_id = json[0]["user_id"].string ?? ""
        let conversation_id = json[0]["conversation_id"].string ?? ""
        let datas = ["message_id":message_id, "user_id":user_id, "conversation_id" : conversation_id]
        if conversation_id != "" && message_id != "" && user_id != "" {
            NotificationCenter.default.post(name: .marked_message, object: nil, userInfo: datas)
        }
    }
    
    //here is get new socket event when group chat added new participants
    socket.on("participant_joined"){data,ack in
        let json = JSON(data[0])
        if json["success"].bool ?? false == true {
            if json["conversation_id"].string ?? "" != "" {
                Conversation.get_singleCon(con_id: json["conversation_id"].string ?? "", completion: { (conver) in
                    DispatchQueue.main.async {
                        print("GGE")
                        let datas = ["conversationId": json["conversation_id"].string ?? ""]
                        NotificationCenter.default.post(name: .receive_message, object: nil, userInfo: datas)
                    }
                })
            }
        }
    }
    socket.connect()
}

func playSound() {
    var soundURL: NSURL?
    var soundID:SystemSoundID = 0
    //audio name
    let filePath = Bundle.main.path(forResource: "newMessage", ofType: "wav")
    soundURL = NSURL(fileURLWithPath: filePath!)
    AudioServicesCreateSystemSoundID(soundURL!, &soundID)
    AudioServicesPlaySystemSound(soundID)
}

extension Notification.Name {
    //socket
    static let socketIsConnected = Notification.Name("socketIsConnected")
    static let user_status = Notification.Name("user_status")
    static let receive_group = Notification.Name("receive_group")
    static let typing_receive = Notification.Name("typing_receive")
    static let receive_message = Notification.Name("receive_message")
    static let marked_message = Notification.Name("marked_message")
    static let participant_left = Notification.Name("participant_left")
    static let cdioSocketCon = Notification.Name("cdiosocketIsConnected")
}
