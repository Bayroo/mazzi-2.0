//
//  FileCell.swift
//  Mazzi
//
//  Created by Bayara on 12/11/18.
//  Copyright © 2018 Mazzi Application. All rights reserved.
//

import Foundation

class FileCell:UITableViewCell{
    @IBOutlet weak var imgview: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
//    @IBOutlet weak var sizeLable: UILabel!
    
}
