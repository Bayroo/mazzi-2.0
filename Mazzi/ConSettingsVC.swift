//
//  ConSettingsVC.swift
//  Mazzi
//
//  Created by Janibekm on 12/7/17.
//  Copyright © 2017 Mazzi App LLC. All rights reserved.
//

import UIKit
import SocketIO
import KVNProgress
import SwiftyJSON
import Alamofire

class ConSettingsVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var items:[NSDictionary] = []
    var switchBtn = UISwitch()
    let userDefaults = UserDefaults.standard
    var userid = ""
    var username = ""
    var selectedgroupUsers: [User_group]?
    var groupOwner: [User_group]?
    var groupTitle = ""
    var senderchatid = ""
    var myid = SettingsViewController.userId
    let fileUrl = SettingsViewController.fileurl
    var selectedIndex = -1
    var imgName = ""
    var settingsArray = [String]()
    var isChangeOwner = false
    var isOnlyChangeAdmin = false
    //var blocked = false
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var groupImg: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(backtoRoot))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:"nemehBTN"), style: .plain, target: self, action: #selector(inviteFriend))
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        self.navigationItem.title = groupTitle
        self.settingsArray = ["Админ солих","Чатнаас гарах"]
        items = [
            ["id":1,"title":"Block User","description": "", "img":"contact","block":GlobalVariables.blocked],
        ]
        if self.imgName == ""{
             self.groupImg.image = UIImage(named: "avatar")
        }else{
             self.groupImg?.sd_setImage(with: URL(string: "\(self.fileUrl)\(self.imgName)"), completed: nil)
        }
        self.navigationController?.navigationBar.barTintColor = GlobalVariables.purple
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        for i in 0..<(selectedgroupUsers!).count{
            if ((self.selectedgroupUsers?[i].id.contains(GlobalVariables.ConversationOwnerId))!){
                self.groupOwner = [self.selectedgroupUsers![i]]
                
                if self.groupOwner![0].id == self.selectedgroupUsers![i].id{
                    self.selectedgroupUsers?.remove(at: i)
                    break
                }
                    
             }else{
            }
        }

    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func changImg(_ sender: Any) {
        performSegue(withIdentifier: "tocamera", sender: self)
    }
    
    func photoGet(capturedImage: UIImage) {
        let userid = userDefaults.object(forKey: "user_id") as? String
        KVNProgress.show()
        Message.uploadimage(chatId: userid! ,image: capturedImage, completion: { (state, imageurl) in
            DispatchQueue.main.async {
                let imageurl = URL(string:"\(self.fileUrl)\(self.imgName)")!
                self.groupImg.sd_setImage(with: imageurl, completed: { (image, error, cacheType, url) in
                self.groupImg.sd_setImage(with: imageurl, completed: nil)
                                })
                KVNProgress.dismiss()
            }
        })
    }

    func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
        var gradientImage:UIImage?
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            gradientLayer.render(in: context)
            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        UIGraphicsEndImageContext()
        return gradientImage
    }
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
        if(userDefaults.object(forKey: userid) != nil){
             GlobalVariables.blocked = userDefaults.object(forKey: userid) as! Bool
        }
        self.tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         self.tableView.reloadData()
    }
    
    @IBAction func leaveChatBtn(_ sender: Any) {
      
    }
    
    @objc func inviteFriend(){
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "searchUserView") as? SearchUserVC
        viewController?.currentUser = self.selectedgroupUsers!
        self.present(viewController!, animated: false, completion: nil)
    }
    
    @objc func backtoRoot(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func switchChanged(mySwitch: UISwitch) {
        let value = mySwitch.isOn
        self.userDefaults.set(value, forKey: userid)
        userDefaults.synchronize()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0{
           
        }else if indexPath.section == 1{
            if self.isChangeOwner {
                let name = self.selectedgroupUsers![indexPath.row].nickname
                self.selectedIndex = indexPath.row
                self.tableView.reloadData()
                let alert = UIAlertController(title: "Админ солих", message: "Тa \(name) - г админ болгохдоо итгэлтэй байна уу", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Тийм", style: .default  , handler: {action in
                    self.isChangeOwner = false
//                    print("selectedUserID:: ", self.selectedgroupUsers![indexPath.row].id as Any)
                    self.changeOwnerAndLeaveChat()
                }))
                alert.addAction(UIAlertAction(title: "Үгүй", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }else if self.isOnlyChangeAdmin {
                let name = self.selectedgroupUsers![indexPath.row].nickname
                self.selectedIndex = indexPath.row
                self.tableView.reloadData()
                let alert = UIAlertController(title: "Админ солих", message: "Тa \(name) - г админ болгохдоо итгэлтэй байна уу", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Тийм", style: .default  , handler: {action in
                    self.isChangeOwner = false
//                    print("selectedUserID:: ", self.selectedgroupUsers![indexPath.row].id as Any)
                    self.changeOwner()
                }))
                alert.addAction(UIAlertAction(title: "Үгүй", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }else{
//            print("jgsejkhfjghsef:: ", self.settingsArray[indexPath.row])
            if self.settingsArray[indexPath.row] == "Админ солих"{
                self.isOnlyChangeAdmin = true
                if self.myid == GlobalVariables.ConversationOwnerId {
                    self.isChangeOwner = false
                    self.tableView.reloadData()
                    KVNProgress.showError(withStatus: "Та админ болгох хүнээ сонгоно уу", on: self.view)
                }else{
                     self.leaveChat()
                }
//                    print("change owner")
            }else {
               self.leaveChat()
            }
        }
    }
    
    func leaveChat(){
        let alert = UIAlertController(title: "Чатаас гарах", message: "Та групп чатаас гарахдаа итгэлтэй байна уу ?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Тийм", style: .default  , handler: {action in
            if self.myid == GlobalVariables.ConversationOwnerId {
                self.isChangeOwner = true
                self.tableView.reloadData()
                KVNProgress.showError(withStatus: "Та админ болгох хүнээ сонгоно уу", on: self.view)
            }else{
                Conversation.leaveConversation(conversation_id: self.senderchatid, completion: { (con) in
                    if con == true {
                        RealmService.deleteConversationFromRealm(deleteConversation: self.senderchatid, completion: { (succ) in
                            if succ == true {
                                self.delete_conversation(con_id: self.senderchatid)
                            }
                        })
                        RealmService.deleteMessages(deleteMessageParentId: self.senderchatid, completion: { (suc) in
                            print("message uudiig ustaglaaa successfully")
                        })
                        
                        socket.emit("leave_conversation",["conversation_id": self.senderchatid, "user_id":self.myid, "content": self.myid, "ownerId":""])
                        for i in 0..<self.selectedgroupUsers!.count{
                            if self.selectedgroupUsers![i].id == self.myid{
                                self.selectedgroupUsers?.remove(at: i)
                                break
                            }
                        }
                        ChatListViewController.refreshview = true
                        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainnav") as! MainNavViewController
                        self.present(viewController, animated: true, completion: nil)
                        
                    }
                })
           
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Болих", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func delete_conversation(con_id: String){
        let token = UserDefaults.standard.object(forKey:"token") as! String
        let  header = ["x-access-token": token, "authorization": "BPXjEvWfXxx5qpQHHFMEJEB8fA3xAczC"]
        let baseurl = SettingsViewController.baseurl
        //        KVNProgress.show(withStatus: "", on: self.view)
        Alamofire.request("\(baseurl)delete_conversation", method: .post, parameters: ["conversation_id":con_id, "user_id": self.myid], encoding:
            JSONEncoding.default,headers:header).responseJSON
            {
                response in
                switch response.result {
                case .success:
                    let res = JSON(response.result.value!)
                    print("deleteConv: ",res)
                case .failure(let e):
                    print(e)
                }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.lightGray //GlobalVariables.todpurple.withAlphaComponent(0.8)
        
        let headerLabel = UILabel(frame: CGRect(x: 20, y: 2, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height-5))
        headerLabel.font = UIFont(name: "segoeui", size: 16)
        headerLabel.textColor = UIColor.white
        headerLabel.text = self.tableView(self.tableView, titleForHeaderInSection: section)
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let title : UILabel = UILabel()
        if section == 0 {
            title.textColor = UIColor.white
            title.text =  "Админ"
        }else if section == 1{
            title.textColor = UIColor.white
            title.text =  "Групп чатын гишүүд \("(")\(selectedgroupUsers!.count)\(")")"
        }else{
            title.textColor = UIColor.white
            title.text =  "Тохиргоо"
        }
        return title.text
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            if groupOwner == nil{
                return 0
            }else{
                return  groupOwner!.count
            }
        }else if section == 1{
                return selectedgroupUsers!.count
        }else{
             if GlobalVariables.ConversationOwnerId == self.myid{
                 return self.settingsArray.count
             }else{
                return 1
            }
           
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! LetterTableViewCell
//        cell.clearCellData()
        cell.userImageView.layer.cornerRadius = 20
        cell.userImageView.clipsToBounds = true
        cell.userImageView.layer.borderWidth = 0.5
        cell.userImageView.layer.borderColor = GlobalVariables.F5F5F5.cgColor
        if indexPath.section  == 0{
                cell.icon.isHidden = true
                cell.textLabel?.isHidden = true
                cell.phoneLabel.isHidden = false
                cell.userImageView.isHidden = false
                let fileurl = SettingsViewController.fileurl
                cell.userLabel?.text = self.groupOwner![indexPath.row].nickname
                cell.phoneLabel?.text = self.groupOwner![indexPath.row].phone
                let image = self.groupOwner![indexPath.row].image
                if image == "" {
                    cell.userImageView?.image = UIImage(named:"p")
                }else{
                    cell.userImageView?.sd_setImage(with: URL(string: "\(fileurl)\(image)"), completed: nil)
                }
            }else if indexPath.section == 1{
                if self.isChangeOwner || self.isOnlyChangeAdmin {
                    cell.icon.isHidden = false
                }else{
                     cell.icon.isHidden = true
                }
                cell.textLabel?.isHidden = true
                cell.phoneLabel.isHidden = false
                cell.userImageView.isHidden = false
                if self.selectedIndex == indexPath.row{
                    cell.icon.image = UIImage(named: "success")
//                    cell.accessoryType = .checkmark
                }else{
                    cell.icon.image = UIImage(named: "oval")
//                    cell.accessoryType = .none
                }
                let fileurl = SettingsViewController.fileurl
                cell.userLabel?.text = self.selectedgroupUsers![indexPath.row].nickname
                cell.phoneLabel?.text = self.selectedgroupUsers![indexPath.row].phone
                let image = self.selectedgroupUsers![indexPath.row].image
                if image == "" {
                    cell.userImageView?.image = UIImage(named:"p")
                }else{
                    cell.userImageView?.sd_setImage(with: URL(string: "\(fileurl)\(image)"), completed: nil)
                }
            }else{
                cell.icon.isHidden = true
                cell.textLabel?.isHidden = false
                cell.phoneLabel.isHidden = true
                cell.userImageView.isHidden = true
                if GlobalVariables.ConversationOwnerId == self.myid{
                    cell.textLabel?.text = self.settingsArray[indexPath.row]
                }else{
                     cell.textLabel?.text = self.settingsArray[1]
                }
               
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60.0;//Choose your custom row height
    }
    
    func changeOwner(){
        let baseurl = SettingsViewController.baseurl
        let token = UserDefaults.standard.object(forKey:"token") as! String
        let  header = ["x-access-token": token, "authorization": "BPXjEvWfXxx5qpQHHFMEJEB8fA3xAczC"]
        KVNProgress.show(withStatus: "", on: self.view)
        Alamofire.request("\(baseurl)changeOwner_conversation", method: .post, parameters: ["conversation_id":self.senderchatid,"owner_id":self.selectedgroupUsers![self.selectedIndex].id], encoding: JSONEncoding.default, headers: header).responseJSON{ response in
            KVNProgress.dismiss()
            if response.response?.statusCode == 200 {
                self.isOnlyChangeAdmin = false
                if self.groupOwner != nil {
                    self.groupOwner?.removeAll()
                    self.groupOwner = [self.selectedgroupUsers![self.selectedIndex]]
                }else{
                    self.groupOwner = [self.selectedgroupUsers![self.selectedIndex]]
                }
                self.tableView.reloadData()
                KVNProgress.showSuccess(withStatus: "Амжилттай солилоо", on: self.view)
            }else{
                
            }
        }
    }
    
    func changeOwnerAndLeaveChat(){
        let baseurl = SettingsViewController.baseurl
        let token = UserDefaults.standard.object(forKey:"token") as! String
        let  header = ["x-access-token": token, "authorization": "BPXjEvWfXxx5qpQHHFMEJEB8fA3xAczC"]
        Alamofire.request("\(baseurl)changeOwner_conversation", method: .post, parameters: ["conversation_id":self.senderchatid,"owner_id":self.selectedgroupUsers![self.selectedIndex].id], encoding: JSONEncoding.default, headers: header).responseJSON{ response in
            if response.response?.statusCode == 200 {
                ChatListViewController.refreshview = true
                socket.emit("leave_conversation",["conversation_id": self.senderchatid, "user_id":self.myid, "content": self.myid, "ownerId":""])
                
                let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainnav") as! MainNavViewController
                self.present(viewController, animated: true, completion: nil)
                
            }else{
                
            }
        }
    }
}
