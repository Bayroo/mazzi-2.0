//
//  BankAccountsVC.swift
//  Mazzi
//
//  Created by Bayara on 6/3/19.
//  Copyright © 2019 Mazz Application. All rights reserved.
//

import Foundation
import UIKit

class BankAccountsVC: UIViewController ,UIPickerViewDelegate,UIPickerViewDataSource{
    
    @IBOutlet weak var BankNameField: UITextField!
    
    @IBOutlet weak var pickBtn: UIButton!
    @IBOutlet weak var bankAccount: UILabel!
    @IBOutlet weak var receiver: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    var subview = UIView()
    var pickerTitle = UILabel()
    var closePicker = UIButton()
    var pickerView = UIPickerView()
    var isPickerShow = false
    var pickerDataSource = ["Голомт банк"]
    var filterBtn = UIButton()
    var selectedBankName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Дансны мэдээлэл"
        self.pickerView.dataSource = self
        self.pickerView.delegate = self
        self.initPickerView()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(backtoRoot))
        self.BankNameField.inputView = self.subview
        self.pickBtn.addTarget(self, action: #selector(self.showPickerTeacher), for: .touchUpInside)
        self.BankNameField.text = self.pickerDataSource[0].uppercased()
        self.bankAccount.text = "3005116509"
        self.receiver.text = "Mazzi Application LLC".uppercased()
        self.descriptionLabel.text = "3005116509"
    }
    
    func initPickerView(){
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.subview.frame = CGRect(x:0, y:self.view.bounds.height,width: self.view.bounds.width, height:250)
        self.pickerTitle.frame = CGRect(x:self.view.bounds.width/2 - 75, y:4, width:150,height:20)
        self.pickerView.frame = CGRect(x:0,y:28,width:self.view.bounds.width,height:220)
        self.closePicker.frame = CGRect(x:4,y:4,width:20,height:20)
        self.filterBtn.frame = CGRect(x: self.view.bounds.width - 100, y: 0, width: 120, height: 30)
        self.filterBtn.layer.cornerRadius = 5
        self.filterBtn.layer.borderWidth = 0
        self.filterBtn.layer.borderColor = UIColor.white.cgColor
        self.filterBtn.setTitle("Сонгох", for: .normal)
        self.filterBtn.setTitleColor(UIColor.white, for: .normal)
        self.filterBtn.layer.masksToBounds = true
        self.filterBtn.titleLabel?.font = GlobalVariables.customFont12
        self.filterBtn.addTarget(self, action: #selector(self.chooseBtnClicked), for: .touchUpInside)
        self.subview.backgroundColor = GlobalVariables.todpurple
        self.pickerView.backgroundColor = UIColor.white
        self.pickerTitle.textAlignment = NSTextAlignment.center
        self.pickerTitle.text = ""
        self.pickerTitle.font = GlobalVariables.customFont12
        self.pickerTitle.textColor = UIColor.white
        self.closePicker.setImage(UIImage(named: "closewhite"), for: UIControl.State.normal)
        self.closePicker.addTarget(self, action: #selector(dismissPicker), for: .touchUpInside)
        
        self.subview.addSubview(self.pickerTitle)
        self.subview.addSubview(self.pickerView)
        self.subview.addSubview(self.closePicker)
        self.subview.addSubview(self.filterBtn)
        self.view.addSubview(self.subview)
    }
    
    @objc func chooseBtnClicked(){
        if self.selectedBankName != "" {
            self.BankNameField.text = self.selectedBankName
            self.dismissPicker()
        }
      
    }
    
    @objc func showPickerTeacher(){
        self.pickerTitle.text = "Банк сонгох"
            if self.isPickerShow == false {
                self.isPickerShow = true
                self.pickerView.reloadAllComponents()
                UIView.transition(with: self.subview, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    self.subview.frame = CGRect(x:0, y: self.view.frame.height-250,width: self.view.bounds.width, height:250)
                })
            }else{
                self.dismissPicker()
            }
    }
    
    @objc func dismissPicker(){
        if self.isPickerShow == true {
            UIView.transition(with: self.subview, duration: 0.3, options: .transitionCrossDissolve, animations: {
                self.isPickerShow = false
                self.subview.frame = CGRect(x:0, y:self.view.bounds.height,width: self.view.bounds.width, height:250)
            })
        }
    }
    
    @objc func backtoRoot(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func copyDans(_ sender: Any) {
        UIPasteboard.general.string = self.bankAccount.text!
        let alert = UIAlertController(title: "Хуулагдлаа", message: "'\(self.bankAccount.text!)' текст хуулагдлаа", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    @IBAction func copyReceiver(_ sender: Any) {
        UIPasteboard.general.string = self.receiver.text!
        let alert = UIAlertController(title: "Хуулагдлаа", message: "'\(self.receiver.text!)' текст хуулагдлаа", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    @IBAction func copyDesc(_ sender: Any) {
        UIPasteboard.general.string = self.descriptionLabel.text!
        let alert = UIAlertController(title: "Хуулагдлаа", message: "'\(self.descriptionLabel.text!)' текст хуулагдлаа", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickerDataSource.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedBankName = self.pickerDataSource[row]
        self.BankNameField.text = self.pickerDataSource[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataSource[row]
    }
    
    
}
