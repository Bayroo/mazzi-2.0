//
//  searchContactVC.swift
//  Mazzi
//
//  Created by Bayara on 1/29/19.
//  Copyright © 2019 woovoo. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import RealmSwift
import MessageUI
import KVNProgress

class searchContactVC: UIViewController,MFMessageComposeViewControllerDelegate, UITextFieldDelegate{
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
         self.dismiss(animated: true, completion: nil)
    }
    
    let limitLength = 8
    var state = 0
    var nickname = ""
    var lastname = ""
    var number = ""
    var image = ""
    var userId = ""
    var onlineAt = Double()
    var birthday = Double()
    var Installed = false
    var selectedContactIndex = 0
    var fcm = ""
    var contacts : Results<Contacts_realm>!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var txtfield : UITextField!
    @IBOutlet weak var searachBtn : UIButton!
    let baseurl = SettingsViewController.baseurl
    var numbers = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customize()
    }
    
     override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.txtfield.text = ""
        self.txtfield.becomeFirstResponder()
        self.Installed = false
        if GlobalVariables.isFromClassMembers == true{
            GlobalVariables.isFromClassMembers = false
//            self.dismiss(animated: false, completion: nil)
            self.navigationController?.popViewController(animated: true)
        }
         self.title = "Найзаа нэмэх".uppercased()
    }
    
    @objc func back(){
        self.txtfield.resignFirstResponder()
        self.navigationController?.popViewController(animated: true)
//        self.dismiss(animated: true, completion: nil)
    }
    
    func customize(){
        self.txtfield.delegate = self
        self.searachBtn.isEnabled = false
        self.contacts = try! Realm().objects(Contacts_realm.self).sorted(byKeyPath: "name", ascending: true).filter("created_at != 0")
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "05"), style: .plain, target: self, action: #selector(back))
        self.txtfield.layer.cornerRadius = 5
        self.txtfield.layer.borderWidth = 1
        self.txtfield.layer.borderColor = GlobalVariables.purple.cgColor
        
        self.searachBtn.layer.cornerRadius = 5
        self.searachBtn.backgroundColor = GlobalVariables.purple.withAlphaComponent(0.5)
        self.searachBtn.setTitleColor(UIColor.white, for: .normal)
        self.searachBtn.addTarget(self, action: #selector(search), for: .touchUpInside)
        self.backBtn.addTarget(self, action: #selector(back), for: .touchUpInside)
    }
    
    @objc func search(){
        
        let mynumber = (userDefaults.object(forKey: "myNumber") as? String)!
        print("MNYNUMBER :: ", mynumber)
        if mynumber == "\("976")\(String(self.txtfield.text!))"{
            KVNProgress.showError(withStatus: "")
        }else{
            self.numbers = ["\("976")\(String(self.txtfield.text!))"]
            let token = UserDefaults.standard.object(forKey:"token") as! String
            let  header = ["x-access-token": token, "authorization": "BPXjEvWfXxx5qpQHHFMEJEB8fA3xAczC"]
            Alamofire.request("\(baseurl)user_info", method: .post, parameters: ["phone_numbers":self.numbers], encoding: JSONEncoding.default,headers:header).responseJSON{ response in
                if response.response?.statusCode == 200 {
                    let jsonData = JSON(response.value!)
                    
                    if  !jsonData["result"].isEmpty {
                        print("RESULT:: ", jsonData)
                        let json = jsonData["result"].arrayValue
                        self.nickname = json[0]["nickname"].stringValue
                        self.lastname = jsonData["result"][0]["lastname"].stringValue
                        self.number =  self.txtfield.text! //jsonData["result"][0]["phone"].stringValue
                        self.image = jsonData["result"][0]["image"].stringValue
                        self.fcm = jsonData["result"][0]["fcm_token"].stringValue
                        self.birthday = jsonData["result"][0]["birthday"].doubleValue
                        self.onlineAt = jsonData["result"][0]["online_at"].doubleValue
                        self.state = jsonData["result"][0]["state"].intValue
                        self.userId = jsonData["result"][0]["_id"].stringValue
                        for i in 0..<self.contacts.count {
                            if self.contacts[i].phone ==  "\("976")\(self.number)" {
                                self.Installed = true
                                self.checkInstaller(indeks: i)
                            }
                        }
                        
                        self.checkInstaller(indeks: 0)
                    }else{
                        let alert = UIAlertController(title: "", message: "Бүртгэлгүй дугаар байна. Маззи-д урих уу ?", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Тийм", style: .default  , handler: {action in
                            let controller = MFMessageComposeViewController()
                            controller.body = "Маззи апп-г татаарай. https://mazzi.mn/"
                            controller.recipients = ([self.txtfield.text] as! [String])
                            controller.messageComposeDelegate = self
                            self.present(controller, animated: true, completion: nil)
                        }))
                        alert.addAction(UIAlertAction(title: "Болих", style: .cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }else{
                    KVNProgress.showError(withStatus: "Алдаа гарлаа. Дараа дахин оролдоно уу")
                }
            }
        }
 
    }
    
    func checkInstaller(indeks : Int){
        if self.Installed == true {
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileContact") as! ProfileContact
            viewController.contactid = self.contacts[indeks].id
            viewController.username = self.contacts[indeks].lastname
            viewController.membernickname = self.contacts[indeks].name
            viewController.memberdate = self.contacts[indeks].birthday
            viewController.member_onlineAt = self.contacts[indeks].online_at
            viewController.memberphone = self.contacts[indeks].phone
            viewController.memberImg = self.contacts[indeks].image
            viewController.memberFCM = self.fcm
            let navController = UINavigationController(rootViewController: viewController)
            GlobalVariables.isFromClassMembers = true
            self.present(navController, animated:true, completion: nil)
        }else{
            self.performSegue(withIdentifier: "newcontact", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "newcontact"{
            let controller = segue.destination as! NewContactVC
            controller.nickname = self.nickname
            controller.lastname = self.lastname
            controller.image = self.image
            controller.number = self.number
            controller.state = self.state
            controller.birthday = self.birthday
            controller.onlineAt = self.onlineAt
            controller.fcm = self.fcm
            controller.userid = self.userId
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = txtfield.text else { return true }
        let newLength = text.count + string.count - range.length
        if newLength >= limitLength{
            self.searachBtn.isEnabled = true
            self.searachBtn.backgroundColor = GlobalVariables.purple
        }else{
            self.searachBtn.isEnabled = false
            self.searachBtn.backgroundColor = GlobalVariables.purple.withAlphaComponent(0.5)
        }
        return newLength <= limitLength
    }
    
}
