//
//  TimeRegisterVC.swift
//  Mazzi
//
//  Created by Bayara on 1/7/19.
//  Copyright © 2019 woovoo. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation
import Apollo
import KVNProgress
import EFQRCode

class TimeRegisterVC:UIViewController, CLLocationManagerDelegate,UIPickerViewDelegate,UIPickerViewDataSource{
  
    
    // update attendance
    var beginTimee : Double!
    var endTimee  : Double!
    var radius : Double!
    var date : Double!
    var code = ""
    var type = ""
    //---------------------------------------------
    
    @IBOutlet weak var lessonMinField: UITextField!
    @IBOutlet weak var dateBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var longField: UITextField!
    @IBOutlet weak var latField: UITextField!
    @IBOutlet weak var endTime: UITextField!
    @IBOutlet weak var startTime: UITextField!
    @IBOutlet weak var radiusField: UITextField!
    @IBOutlet weak var dateField: UITextField!
    @IBOutlet weak var teacherView: UIView!
    @IBOutlet weak var toAttsList: UIButton!
    var selectedAttendanceId = ""
    var topics = [ObjectTopicsQuery.Data.ObjectTopic.Result]()
    //---------------------------------------------
    @IBOutlet weak var studentView: UIView!
    var currentLocation: CLLocation!
    var myId = SettingsViewController.userId
    var lat : Double!
    var long : Double!
    var startTimeMilll = 0
    var endTimeMilll = 0
    var attendanceCode = ""
    let locManager = CLLocationManager()
    let topicPicker = UIPickerView()
    var pickerData = [String]()
    var classTime : Double!
    @IBOutlet weak var qrBtn: UIButton!
    @IBOutlet weak var codeBtn: UIButton!
    @IBOutlet weak var topicField: UITextField!
    
    var apollo: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
        let url = URL(string:SettingsViewController.graphQL)!
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getTopics()
        self.customize()
        self.topicPicker.delegate = self
        self.topicPicker.dataSource = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        GlobalStaticVar.isUpdateAttendance = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        locManager.startUpdatingLocation()
        locManager.requestWhenInUseAuthorization()
        self.getCurrentLocation()
        
        if GlobalStaticVar.isUpdateAttendance == true{
             self.startTimeMilll = Int(self.beginTimee)
             self.endTimeMilll = Int(self.endTimee)
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone(abbreviation: "GMT/UTC + 08:00")
            dateFormatter.dateStyle = DateFormatter.Style.none
            dateFormatter.timeStyle = DateFormatter.Style.short
             let gg =  NSDate(timeIntervalSince1970: ((self.beginTimee)!/1000))
            self.startTime.text = dateFormatter.string(from: gg as Date)
    
            let oo =  NSDate(timeIntervalSince1970: ((self.endTimee)!/1000))
            self.endTime.text = dateFormatter.string(from: oo as Date)
            
            self.radiusField.text = String(Int(self.radius))
            
            let Formatter = DateFormatter()
            Formatter.timeZone = TimeZone(abbreviation: "GMT/UTC + 08:00")
            Formatter.dateStyle = DateFormatter.Style.short
            Formatter.timeStyle = DateFormatter.Style.none
            let hh =  NSDate(timeIntervalSince1970: ((self.date)!/1000))
            self.dateField.text = Formatter.string(from: hh as Date)
            self.lessonMinField.text = String(Int(self.classTime / 60000))
            self.saveBtn.setTitle("Засах", for: UIControl.State.normal)
//            self.toAttsList.isHidden = true
        }else{
            self.saveBtn.setTitle("Ирц үүсгэх", for: UIControl.State.normal)
//            self.toAttsList.isHidden = false
        }
    }
    
    func customize(){
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(backtoRoot))
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.saveBtn.layer.cornerRadius = 5
        self.saveBtn.backgroundColor = GlobalVariables.purple
        self.saveBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
        self.saveBtn.layer.masksToBounds = true
        self.saveBtn.addTarget(self, action: #selector(qr), for: .touchUpInside)
        
//        self.toAttsList.layer.cornerRadius = 5
//        self.toAttsList.backgroundColor = GlobalVariables.purple
//        self.toAttsList.setTitleColor(UIColor.white, for: UIControl.State.normal)
//        self.toAttsList.layer.masksToBounds = true
//        self.toAttsList.addTarget(self, action: #selector(tolist), for: .touchUpInside)
        
        
//        self.qrBtn.layer.cornerRadius = 5
//        self.qrBtn.backgroundColor = GlobalVariables.purple
//        self.qrBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
//        self.qrBtn.addTarget(self, action: #selector(qrScan), for: .touchUpInside)
        
//        self.codeBtn.layer.cornerRadius = 5
//        self.codeBtn.backgroundColor = GlobalVariables.purple
//        self.codeBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
//        self.codeBtn.addTarget(self, action: #selector(toCode), for: .touchUpInside)
        self.dateField.addTarget(self, action: #selector(textFieldEditing(sender:)), for: .editingDidBegin)
        self.startTime.addTarget(self, action: #selector(startTimeFieldEditing(sender:)), for: .editingDidBegin)
        self.endTime.addTarget(self, action: #selector(endTimeFieldEditing(sender:)), for: .editingDidBegin)
        self.topicField.addTarget(self, action: #selector(topicPicker(sender:)), for: .editingDidBegin)
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        let date = Date()
        self.dateField.text = dateFormatter.string(from: date)
    }
    
    @objc func topicPicker(sender: UITextField){
        topicPicker.backgroundColor = GlobalVariables.F5F5F5
        sender.inputView = topicPicker
//        self.topicPicker.addTarget(self, action: #selector(startTimePickerValueChanged), for: UIControl.Event.valueChanged)
    }
    
    @objc func startTimeFieldEditing(sender: UITextField) {
        let datePicker:UIDatePicker = UIDatePicker()
        datePicker.backgroundColor = GlobalVariables.F5F5F5
        datePicker.datePickerMode = .time
        sender.inputView = datePicker
        datePicker.addTarget(self, action: #selector(startTimePickerValueChanged), for: UIControl.Event.valueChanged)
    }
    
    @objc func endTimeFieldEditing(sender: UITextField) {
        let datePicker:UIDatePicker = UIDatePicker()
        datePicker.backgroundColor = GlobalVariables.F5F5F5
        datePicker.datePickerMode = .time
        sender.inputView = datePicker
        datePicker.addTarget(self, action: #selector(endTimePickerValueChanged), for: UIControl.Event.valueChanged)
    }
    
    @objc func textFieldEditing(sender: UITextField) {
        let datePicker:UIDatePicker = UIDatePicker()
        datePicker.backgroundColor = GlobalVariables.F5F5F5
        datePicker.datePickerMode = .date
        sender.inputView = datePicker
        datePicker.addTarget(self, action: #selector(datePickerValueChanged), for: UIControl.Event.valueChanged)
    }
    
    @objc func startTimePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT/UTC + 08:00")
        dateFormatter.dateStyle = DateFormatter.Style.none
        dateFormatter.timeStyle = DateFormatter.Style.short
        self.startTime.text = dateFormatter.string(from: sender.date)
        let dateofbirth = sender.date.timeIntervalSince1970
        let doubledate = Double(dateofbirth * 1000)
        self.startTimeMilll = Int(doubledate)

    }
    
    @objc func endTimePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT/UTC + 08:00")
        dateFormatter.timeStyle = DateFormatter.Style.short
        self.endTime.text = dateFormatter.string(from: sender.date)
        let dateofbirth = sender.date.timeIntervalSince1970
        let doubledate = Double(dateofbirth * 1000)
        self.endTimeMilll = Int(doubledate)
    }
    
   @objc func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
//        let components = Calendar.current.dateComponents([.year ,.month, .day], from: sender.date)
//        if let day = components.day, let month = components.month , let year = components.year {
//            print("\(day) \(month) \(year)")
//        }
    
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        self.dateField.text = dateFormatter.string(from: sender.date)
    }
    
    @objc func dismissKeyboard(){
        self.dateField.resignFirstResponder()
        self.radiusField.resignFirstResponder()
        self.startTime.resignFirstResponder()
        self.endTime.resignFirstResponder()
        self.latField.resignFirstResponder()
        self.longField.resignFirstResponder()
        self.topicField.resignFirstResponder()
        self.lessonMinField.resignFirstResponder()
    }
    
    func getCurrentLocation(){
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            currentLocation = locManager.location
            self.lat =  currentLocation.coordinate.latitude //47.91552734375 //
            self.long = currentLocation.coordinate.longitude //106.92069789151647//
//            print(self.lat)
//            print(self.long)
            self.latField.text = String(format:"%0.6f",self.lat)
            self.longField.text = String(format:"%0.6f",self.long)
//            print("LOCATION  latitude:: ",currentLocation.coordinate.latitude)
//            print("LOCATION longitude :: ",currentLocation.coordinate.longitude)
        }else{
            self.locationManager(manager: locManager, didChangeAuthorizationStatus: .notDetermined)
        }
    }
   
    private func locationManager(manager: CLLocationManager!,
                         didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        var shouldIAllow = false
        var locationStatus : String!
        switch status {
        case CLAuthorizationStatus.restricted:
            locationStatus = "Restricted Access to location"
        case CLAuthorizationStatus.denied:
            locationStatus = "User denied access to location"
        case CLAuthorizationStatus.notDetermined:
            locationStatus = "notDetermined"
        default:
            locationStatus = "Allowed to location Access"
            shouldIAllow = true
        }
        print(locationStatus)
        if shouldIAllow == false {
            let alert = UIAlertController(title: "", message: "Утасныхаа байршил тогтоогчийг идэвхжүүлнэ үү", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Тохиргоо", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction) -> Void in
                let alertt = UIAlertController(title: "Заавар", message: "Settings -> Privacy -> Location Services -> Mazzi : While Using the App", preferredStyle: UIAlertController.Style.alert)
                alertt.addAction(UIAlertAction(title: "OK", style: .cancel, handler:  { (alert:UIAlertAction) -> Void in }))
                     self.present(alertt, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "Болих", style: .cancel, handler:  { (alert:UIAlertAction) -> Void in
                self.backtoRoot()
            }))
            self.present(alert, animated: true, completion: nil)
                
        }
    }
    
    @objc func backtoRoot(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc func qr(){
        self.dismissKeyboard()
        if self.radiusField.text != "" && self.radiusField.text != " " && self.radiusField.text != nil{
            if self.startTime.text != "" && self.startTime.text != " " && self.startTime.text != nil{
            if self.endTime.text != "" && self.endTime.text != " " && self.endTime.text != nil {
                if self.topicField.text != "" && self.topicField.text != " " && self.topicField.text != nil{
                     if self.lessonMinField.text != "" && self.lessonMinField.text != " " && self.lessonMinField.text != nil{
                        if GlobalStaticVar.isUpdateAttendance == true {
                            self.updateAttendance()
                        }else{
                            self.insertAttendance()
                        }
                     }else{
                        KVNProgress.showError(withStatus: "Хичээл үргэлжлэх хугацаа оруулна уу")
                    }
                }else{
                     KVNProgress.showError(withStatus: "Хичээлээ сонгоно уу")
                }
            }else{
                  KVNProgress.showError(withStatus: "Дуусах цаг оруулна уу")
                }
           }else{
                 KVNProgress.showError(withStatus: "Эхлэх цаг оруулна уу")
            }
        }else{
            KVNProgress.showError(withStatus: "Радиус оруулна уу")
        }
    }
    
    func insertAttendance(){
        var selectedTopicId = ""
        for i in 0..<self.topics.count{
//            print("topics:: ", self.topics)
//            print("selectedTopic:: ",self.topicField.text!)
            if self.topics[i].name == self.topicField.text! {
                selectedTopicId = self.topics[i].id ?? ""
            }
        }
        let radius = Double(self.radiusField.text!)
        let lessonTime = Double(Int(self.lessonMinField.text!)! * 60000)
        let setting = InputAttendanceSettings(enforceGeoLocation: true)
        let location = InputAttendanceLocation(latitude: self.lat, longitude: self.long)
        let time = InputAttendanceTime(timeEnds: Double(self.endTimeMilll), timeBegan: Double(self.startTimeMilll))
//        let inputAtt = InputAttendance(settings: setting, location: location, radius: radius, time: time)
//        let gg = InputAttendance(settings: setting, location: location, radius: radius, time: time, topicId: selectedTopicId)
        
        let gg = InputAttendance(settings: setting, location: location, radius: radius, time: time, topicId: selectedTopicId, classTime: lessonTime)
        let mutation = InsertAttendanceMutation(userId: self.myId, classId: GlobalStaticVar.classId, input: gg)
        
        self.apollo.perform(mutation: mutation) { result, error in
//            print("CREATEQR :: ", result)?
            if result?.data?.insertAttendance?.error == false{
                let code = result?.data?.insertAttendance?.result?.code ?? ""
                self.attendanceCode = code
                let type = result?.data?.insertAttendance?.result?.__typename ?? ""
                let dic = ["code":code, "type":type]
                let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
                let jsonString = String(data: jsonData!, encoding: .utf8)
                let utf8str = jsonString?.data(using: String.Encoding.utf8)
                let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))

                self.generateQR(content: base64Encoded!)
            }else{
                KVNProgress.showError(withStatus: result?.data?.insertAttendance?.message)
            }
        }
    }
    
    func generateQR(content: String){
        if let tryImage = EFQRCode.generate(
            content: content
//            watermark: UIImage(named: "avatar")?.toCGImage()
            ){
            let myCIQR = self.convertCGImageToCIImage(inputImage: tryImage)
            let myQR = self.convert(cmage: myCIQR!)
            self.backtoRoot()
            let alert = CustomAlertView.init(title: self.attendanceCode , image: myQR)
            alert.show(animated: true)
//            alert.rescan.addTarget(self, action: #selector(goscaneer), for: UIControlEvents.touchUpInside)
        } else {
            print("Create QRCode image failed!")
        }
    }
    
    func convertCGImageToCIImage(inputImage: CGImage) -> CIImage! {
        let ciImage = CIImage(cgImage: inputImage, options: nil)
        return ciImage
    }
    
    func convert(cmage:CIImage) -> UIImage
    {
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent)!
        let image:UIImage = UIImage.init(cgImage: cgImage)
        return image
    }
    
    func updateAttendance(){
        var selectedTopicId = ""
        for i in 0..<self.topics.count{
            if self.topics[i].name == self.topicField.text! {
                selectedTopicId = self.topics[i].id ?? ""
            }else{
                selectedTopicId = "empty"
            }
        }
        let radius = Double(self.radiusField.text!)
        let setting = InputAttendanceSettings(enforceGeoLocation: true)
        let location = InputAttendanceLocation(latitude: self.lat, longitude: self.long)
        let time = InputAttendanceTime(timeEnds: Double(self.endTimeMilll), timeBegan: Double(self.startTimeMilll))
//        print("CLASS TIME :: ",Double( self.lessonMinField.text!)!)
        let lessonTime = Double(Int(self.lessonMinField.text!)! * 60000)
//        let inputAtt = InputAttendance(settings: setting, location: location, radius: radius, time: time, topicId:selectedTopicId)
        let inputAtt = InputAttendance(settings: setting, location: location, radius: radius, time: time, topicId: selectedTopicId, classTime: lessonTime )
        let mutation = UpdateAttendanceMutation(userId: self.myId, attId: self.selectedAttendanceId, input: inputAtt, classId: GlobalStaticVar.classId)
        self.apollo.perform(mutation: mutation) { result, error in
//            print("inputAtt:: " , inputAtt)
//            print("-----------------")
//            print("userId;", self.myId)
//            print("ATTT ID:: ",self.selectedAttendanceId)
//            print("RESULT::: ", result?.data?.updateAttendance?.status)
            if result?.data?.updateAttendance?.error == false{
                GlobalStaticVar.isUpdateAttendance = false
                let code = self.code
                self.attendanceCode = code
                let type = self.type
                let dic = ["code":code, "type":type]
                let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
                let jsonString = String(data: jsonData!, encoding: .utf8)
                let utf8str = jsonString?.data(using: String.Encoding.utf8)
                let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
                
                self.generateQR(content: base64Encoded!)
            }else{
                KVNProgress.showError(withStatus: result?.data?.updateAttendance?.message)
            }
        }
    }
    
    func getTopics(){
        let query = ObjectTopicsQuery(userId: self.myId, classId: GlobalStaticVar.classId)
        apollo.fetch(query: query) { result, error in
            if result!.data?.objectTopics != nil {
                self.topics = (result!.data?.objectTopics?.result)! as! [ObjectTopicsQuery.Data.ObjectTopic.Result]
                if self.topics.count == 1 {
                   self.topicField.text = self.topics[0].name!
                }
                
                if self.topics.count > 0{
                    for i in 0..<self.topics.count{
                        if self.pickerData.count > 0 {
                            if self.topics.contains(where: {$0.name! != self.topics[0].name!}){
                                self.pickerData.append(self.topics[i].name!)
                            }
                        }else{
                            self.pickerData.append(self.topics[i].name!)
                        }
                        self.topicPicker.reloadAllComponents()
                    }
                }else{
                   KVNProgress.showError(withStatus: "Топик оруулна уу")
                }
               
            }else{
                KVNProgress.showError(withStatus: result?.data?.objectTopics?.message)
            }
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return  self.pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.topicField.text = self.pickerData[row]
    }
}
