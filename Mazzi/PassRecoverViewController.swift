//
//  PassRecoverViewController.swift
//  Mazzi
//
//  Created by Bayara on 8/15/18.
//  Copyright © 2018 Mazzi App LLC. All rights reserved.
//

import Foundation
import KVNProgress
import Alamofire
import SwiftyJSON
import FirebaseAuth

class PassRecoverViewController : UIViewController {
    
     let userDefaults = UserDefaults.standard
    var verificationID = ""
    @IBOutlet weak var confirmCodeField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var rePassField: UITextField!
    @IBOutlet weak var confirmBtn: UIButton!
    
    
    override func viewDidLoad() {
        super .viewDidLoad()
        self.navigationItem.title = "Шинэ нууц үг оруулах"
//        self.navigationItem.hidesBackButton = true
        configure()
    }
    @IBAction func back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func configure(){
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(backtoROot))
         self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        verificationID = userDefaults.string(forKey: "authPass")!
        self.confirmBtn.backgroundColor = GlobalVariables.purple
        
        self.confirmCodeField.layer.cornerRadius = 5
        self.confirmCodeField.layer.borderWidth = 1
        self.confirmCodeField.layer.borderColor = GlobalVariables.purple.cgColor
        self.confirmCodeField.textColor = GlobalVariables.purple
        
        self.passwordField.layer.cornerRadius = 5
        self.passwordField.layer.borderWidth = 1
        self.passwordField.layer.borderColor = GlobalVariables.purple.cgColor
        self.passwordField.textColor = GlobalVariables.purple
        
        self.rePassField.layer.cornerRadius = 5
        self.rePassField.layer.borderWidth = 1
        self.rePassField.layer.borderColor = GlobalVariables.purple.cgColor
        self.rePassField.textColor = GlobalVariables.purple
        
         self.confirmCodeField.attributedPlaceholder = NSAttributedString(string: "Баталгаажуулах код",attributes: [NSAttributedString.Key.foregroundColor: GlobalVariables.purple.withAlphaComponent(0.2)])
        
        self.passwordField.attributedPlaceholder = NSAttributedString(string: "Шинэ нууц үг",attributes: [NSAttributedString.Key.foregroundColor: GlobalVariables.purple.withAlphaComponent(0.2)])
        
        self.rePassField.attributedPlaceholder = NSAttributedString(string: "Нууц үг давтах",attributes: [NSAttributedString.Key.foregroundColor: GlobalVariables.purple.withAlphaComponent(0.2)])
    }
    
    func showAlert(title: String? = nil, message: String? = nil){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(ok)
        present(alertController, animated: true, completion: nil)
    }

    @IBAction func Confirm(_ sender: Any) {
        if self.passwordField.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")) == "" {
            showAlert(title: "Нууц үг оруулна уу!")
            return
        }else if !Utils.isValidPass(testStr: self.passwordField.text!) {
            showAlert(title: "Нууц үг буруу!",message:"Багадаа нэг үсэг болон тоо орсон 6 болон түүнээс дээш урттай нууц үг оруулна уу!")
            return
        }
        self.recoverPass()
    
    }
    
    func recoverPass(){
        let basurl = SettingsViewController.baseurl
        let headers = SettingsViewController.headers
        
        let code = confirmCodeField.text!
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID,verificationCode: code)
        //         KVNProgress.show()
        Auth.auth().signInAndRetrieveData(with: credential) { (user, error) in
            if let error = error {
                print("LoginConfirmViewController - login error: \(error.localizedDescription)")
                let alert = UIAlertController(title: "", message: "Баталгаажуулах код буруу байна !", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                checkInternet()
                return
            }else{
                
                if self.passwordField.text! == self.rePassField.text! {
                    
                    Alamofire.request("\(basurl)recover_password", method: .post, parameters: ["phone":GlobalVariables.phone,"password":self.passwordField.text!], encoding: JSONEncoding.default,headers:headers).responseJSON{ response in
                        KVNProgress.dismiss()
                        
                        let res = JSON(response.value!)
                        print("response:: ",res)
                        if res["error"] == false {
                            GlobalVariables.isloggedin = "true"
                            self.userDefaults.set(SettingsViewController.userId, forKey: "user_id")
                            self.userDefaults.set(GlobalVariables.isloggedin, forKey: "isLogin")
                            self.userDefaults.synchronize()
                            GlobalVariables.isFromPassRecover = true
                            self.navigationController?.popViewController(animated: true)
                            
                            //                            self.performSegue(withIdentifier: "toPassconfirm", sender: self)
                        }else{
                            print("error")
                        }
                    }
                }else{
                    let alert = UIAlertController(title: "Давтсан нууц үг буруу!", message: "Давтсан нууц үг таарахгүй байна!", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    self.passwordField.text = ""
                    self.rePassField.text = ""
                }
            }
        }
    }
    
    @objc func backtoROot(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissKeyboard(){
        passwordField.resignFirstResponder()
        rePassField.resignFirstResponder()
        confirmCodeField.resignFirstResponder()
    }
}

