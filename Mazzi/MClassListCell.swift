//
//  AnswersCell.swift
//  Mazzi
//
//  Created by Bayara on 10/12/18.
//  Copyright © 2018 Mazzi Application LLC. All rights reserved.
//

import Foundation
import UIKit

class MClassListCell:UITableViewCell {

    @IBOutlet weak var backview: UIView!
    @IBOutlet weak var mylabel: UILabel!
    @IBOutlet weak var classCodeLabel: UILabel!
    @IBOutlet weak var badge: UIButton!
    
    func initcellData(){
//        backview.layer.borderColor = GlobalVariables.purple.cgColor
//        backview.layer.borderWidth = 1.0
//        backview.layer.cornerRadius = 5
//        backview.layer.masksToBounds = true
        
        mylabel.textColor = UIColor.black //?GlobalVariables.black
    }
}
