//
//  MyTopicsVC.swift
//  Mazzi
//
//  Created by Bayara on 1/23/19.
//  Copyright © 2019 woovoo. All rights reserved.
//

import Foundation
import Apollo
import KVNProgress

class MyTopicsVC: UIViewController, UITableViewDelegate , UITableViewDataSource{
  
    
    var myid = SettingsViewController.userId
    var classId = GlobalStaticVar.classId
    var topics = [ObjectTopicsQuery.Data.ObjectTopic.Result]()
    var selectedTopic = ""
    var selectedIndex = 0
    var refresher = UIRefreshControl()
    var apollo: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken]
        let url = URL(string:SettingsViewController.graphQL)!
        
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    @IBOutlet weak var topicTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Миний сэдвүүд"
        self.navigationItem.leftBarButtonItem  = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(backtoRoot))
       
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.topicTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        self.topicTableView.dataSource = self
        self.topicTableView.delegate = self
        self.topicTableView.refreshControl = self.refresher
        self.refresher.addTarget(self, action: #selector(reloadData), for: .valueChanged)
    }
    
    override func viewDidAppear(_ animated: Bool) {
         self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:"nemehBTN"), style: .plain, target: self, action: #selector(addTopic))
    }
    
    @objc func reloadData(){
        _ = self.apollo.clearCache()
        let query = ObjectTopicsQuery(userId: self.myid, classId: self.classId)
        apollo.fetch(query: query) { result, error in
           if result!.data?.objectTopics?.error == false {
                self.topics = (result!.data?.objectTopics?.result)! as! [ObjectTopicsQuery.Data.ObjectTopic.Result]
                self.topicTableView.reloadData()
                self.refresher.endRefreshing()
            }else{
                KVNProgress.showError(withStatus: result?.data?.objectTopics?.message)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        _ = self.apollo.clearCache()
        self.getTopics()
    }
    @objc  func addTopic(){
        self.performSegue(withIdentifier: "edittopic", sender: self)
    }
    
    @objc func backtoRoot(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Засах", style: .default, handler: { action in
            self.selectedIndex = indexPath.row
            GlobalVariables.isUpdateTopic = true
             self.performSegue(withIdentifier: "edittopic", sender: self)
        }))
        alert.addAction(UIAlertAction(title: "Устгах", style: .default, handler:{action in
            self.deleteTopic(index: indexPath.row)
        }))
        alert.addAction(UIAlertAction(title: "Болих", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.topics.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "MyCell")
        
        if indexPath.row % 2 == 0 {
            cell.backgroundColor =  GlobalVariables.F5F5F5
        }else{
            cell.backgroundColor =  UIColor.white
        }
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.text = self.topics[indexPath.row].name!.uppercased()
        return cell
    }
    
    func getTopics(){
        let query = ObjectTopicsQuery(userId: self.myid, classId: self.classId)
        apollo.fetch(query: query) { result, error in
//            print("TTTT RESULT:: ", result)
            if result!.data?.objectTopics?.error == false {
                self.topics = (result!.data?.objectTopics?.result)! as! [ObjectTopicsQuery.Data.ObjectTopic.Result]
                self.topicTableView.reloadData()
            }else{
                KVNProgress.showError(withStatus: result?.data?.objectTopics?.message)
            }
        }
    }
    
    func deleteTopic(index: Int){
        let topicId = self.topics[index].id!
        
        let mutation = DeleteObjectTopicMutation(userId: self.myid, topicId: topicId,classId: self.classId)
        self.apollo.perform(mutation: mutation) { result, error in
            if result?.data?.deleteObjectTopic!.error == false{
                KVNProgress.showSuccess(withStatus: "Амжилттай")
                self.topics.remove(at: index)
                self.topicTableView.reloadData()
            }else{
                KVNProgress.showError(withStatus: result?.data?.deleteObjectTopic?.message)
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "edittopic"{
            let controller = segue.destination as! EditTopicVC
            if self.topics.count > 0 {
                controller.editTopicId = self.topics[self.selectedIndex].id ?? ""
                controller.editedTopicName = self.topics[self.selectedIndex].name ?? ""
            }
        }
    }
}
