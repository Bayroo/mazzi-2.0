//
//  chat.swift
//  Mazzi
//
//  Created by Janibekm on 9/1/17.
//  Copyright © 2017 Mazzi App LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire
import Photos
import AVKit
import AVFoundation
import RealmSwift

class Message:NSObject {
    
    
    var id:String = "";
    var conversation_id: String = ""
    var owner_id: String = ""
    var owner_name:String = ""
    var type: String = ""
    var content:String = ""
    //zasvar
    var filename:String = ""
    var file_size:Int = 0
    
    var seen:[String] = [""]
    var created_at:Double = 0
    var image: UIImage?
    var duration:Double = 0
    var register:String = ""
    
    class func downloadAllMessages( chatId: String,limit:Int, skip: Int,save:Bool, completion: @escaping (Message,Bool) -> Swift.Void) {
        let currentUserID = SettingsViewController.userId
        let baseurl = SettingsViewController.baseurl
        let token = UserDefaults.standard.object(forKey:"token") as! String
        let header = ["x-access-token": token, "authorization": "BPXjEvWfXxx5qpQHHFMEJEB8fA3xAczC"]
        
//        print("chatId::",chatId)
//        print("limit::",limit)
//        print("skip::",skip)
//        print("GlobalVariables.user_id::", GlobalVariables.user_id)
//        print("header:: ", header)
        
        Alamofire.request("\(baseurl)get_messages", method: .post , parameters: ["conversation_id":chatId, "limit":limit, "skip":skip,"user_id":GlobalVariables.user_id], encoding: JSONEncoding.default,headers:header).responseJSON{ response in
//              print("RESPOONSE::", response)
            if response.response?.statusCode == 200 {
                let receivedMessage = JSON(response.value!)
                let count = receivedMessage["messages"].arrayValue.count
                let messages = receivedMessage["messages"].arrayValue
                if count != 0 {
                    let fileurl = receivedMessage["conversation"]["flink"]
                    let show = receivedMessage["conversation"]["show"].arrayValue
                    for i in 0..<show.count{
                        if show[i].string! != currentUserID { //&& !SingleChatController.show.contains(show[i].string!){
                            if !SingleChatVC.shows.contains(show[i].string!){
                                SingleChatVC.shows.append(show[i].string!)
                            }
                        }
                    }
                    
                    for i in 0..<fileurl.count{
                        if !SingleChatVC.fileurl.contains(fileurl[i].string!){
                            SingleChatVC.fileurl.append(fileurl[i].string!)
                        }
                    }
                    //  let deleteSuccess = true
                    if save == true {
                        //                        RealmService.deleteMessages(deleteMessageParentId: chatId, completion: { (deleteComplete) in
                        //                            if deleteComplete == true {
                        //                            }
                        //                        })
                        //                        RealmService.deleteMessageSeenObjs(deleteMessgeSeenObjByConid: chatId, completion: { (deleteSeenObjs) in
                        //                            if deleteSeenObjs == true {
                        //                                deleteSuccess = true
                        //                            }
                        //                        })
                    }
                    
                    for i in 0..<count{
                        let messageType = messages[i]["type"].string!
                        let messageID = messages[i]["_id"].string!
                        let content = messages[i]["content"].string ?? ""
//                        print("id:content -\(messages[i]["_id"].string!):\(messages[i]["content"].string!)")
                        let conversation_id = messages[i]["conversation_id"].string!
                        //zasvar
                        let filename = messages[i]["filename"] == JSON.null ? "" : messages[i]["filename"].string!
                        let file_size = messages[i]["file_size"] == JSON.null ? 0 : messages[i]["file_size"].int!
                        
                        let fromID = messages[i]["owner_id"].string!
                        let timestamp = messages[i]["created_at"].double!
                        let seen = messages[i]["seen"].arrayValue.map({$0.stringValue})
                        let duration = messages[i]["duration"].double!
                        let name = messages[i]["name"].string ?? ""
                        //zasvar
                        let message = Message.init(id: messageID, owner_id: fromID, content: content, filename: filename, file_size: file_size, type: messageType, seen: seen, created_at: timestamp, duration: duration, conversation_id: conversation_id,name:name)
                        
                        if save == true {
                            //if deleteSuccess == true {
                                RealmService.createMessageToRealm(createMessage: message,conversation_id:chatId, completion: { (success) in
                                    if success == true {
                                        if count-1 == i{
                                            completion(message,true)
                                        }else{
                                            completion(message,false)
                                        }
                                    }
                                })
                                if i == 0{
                                    let updateConversation = realm.objects(Conversation_realm.self).filter("id = '\(chatId)'")
                                    if updateConversation.count != 0 {
                                        var user_grouping = [User_group]()
                                        for users in updateConversation[0].user{
                                            let user = User_group.init(id: users.id, image: users.image, nickname: users.nickname, lastname: users.lastname, surname: users.surname, phone: users.phone, state: users.state, online_at: Double(users.online_at), fcm_token: users.fcm_token,registerId:users.register)
                                            user_grouping.append(user)
                                        }
                                        var showing = [String]()
                                        for sh in updateConversation[0].show{
                                            showing.append(sh.show)
                                        }
                                        let con = Conversation.init(user: user_grouping, lastmessage: message, id: updateConversation[0].id, title: updateConversation[0].title, created_at: Double(updateConversation[0].created_at), is_thread: updateConversation[0].is_thread, show: showing, secret: updateConversation[0].secret, image: updateConversation[0].image,owner_id_con:updateConversation[0].owner_id_con )
                                        RealmService.updateConversationToRealm(udpateConversation: con, completion: { (conversationHasBeenUpdated) in
                                           // print("jani:",conversationHasBeenUpdated)
                                        })
                                    }
                                }
                         //   }
                        }else{
                            completion(message,true)
                        }
                    }
                }
            }
        }
    }
    class func deleteMessage( message_id: String, conversation_id: String, completion: @escaping (Bool) -> Swift.Void) {
        let baseurl = SettingsViewController.baseurl
        let myId = SettingsViewController.userId
        let token = UserDefaults.standard.object(forKey:"token") as! String
        let header = ["x-access-token": token, "authorization": "BPXjEvWfXxx5qpQHHFMEJEB8fA3xAczC"]
//        print("realm inside")
//        print(message_id)
        Alamofire.request("\(baseurl)delete_message", method: .post, parameters: ["_id":message_id ,"user_id":myId, "conversation_id":conversation_id], encoding: JSONEncoding.default, headers: header).responseJSON{ response in
            if response.response?.statusCode == 200 {
//                print("delete messsage response")
                let getConList = JSON(response.value!)
//                print(getConList)
                if getConList["success"].bool! == true {
                    RealmService.deleteMsg(deleteMsgId: message_id, completion: { (succ) in
                        if succ == true {
                            completion(true)
                        }
                    })
                }
            }
        }
    }
    class func uploadimage( chatId: String, image: UIImage, completion: @escaping (Bool,Any) -> Swift.Void) {
     
        let baseurl = SettingsViewController.fileurl
        let parameters = ["conversation_id":chatId]
        let URL = "\(baseurl)imageUpload"
       // print("params::", parameters)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append(image.jpegData(compressionQuality: 0.1)!, withName: "file", fileName: "image\(Int(Date().timeIntervalSince1970)).jpg", mimeType: "jpg")
            
            for (key, value) in parameters {
               
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                
            }
        }, to:URL)
        { (result) in
          
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                   
                })
                upload.responseJSON { response in
              
                    if let JSON = response.result.value {
                        let res = JSON
//                        print("res:", res)
                        completion(true, res)
                        
                    }
                    else{
                         print("res:", response)
                        print("ELSE ERORO")
                    }
                }
            case .failure(let encodingError):
                //self.delegate?.showFailAlert()
                print("Message - encode: \(encodingError)")
            }
        }
    }
    //zasvar
    class func uploadfile( chatId: String, file: URL, fileName: String,fileExtension: String, completion: @escaping (Bool,Any) -> Swift.Void) {
       // print("UPLOAD FILE")
        let baseurl = SettingsViewController.fileurl
        let parameters = ["conversation_id":chatId]
        let URL = "\(baseurl)documentupload"
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(file, withName: "file")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to:URL)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON { response in
                    if let JSON = response.result.value {
                        let res = JSON
                        completion(true, res)
                    }
                }
            case .failure(let encodingError):
                //self.delegate?.showFailAlert()
                print(encodingError)
            }
        }
    }
    class func uploadvideo( chatId: String, video: URL, completion: @escaping (Bool,Any) -> Swift.Void) {
        let baseurl = SettingsViewController.fileurl
        let parameters = ["conversation_id":chatId]
        let URL = "\(baseurl)videoupload"
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(video, withName: "file", fileName: "video\(Int(Date().timeIntervalSince1970))", mimeType: "mp4")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to:URL)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                  //  print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON { response in
                    if let JSON = response.result.value {
                        let res = JSON
//                        print("res:",res)
                        completion(true, res)
                    }
                }
            case .failure(let encodingError):
                //self.delegate?.showFailAlert()
                print(encodingError)
            }
        }
    }
    class func audioupload( chatId: String, audio: URL, completion: @escaping (Bool,Any) -> Swift.Void) {
        let baseurl = SettingsViewController.fileurl
        //let secureCode = SettingsViewController.secureToken
        let parameters = ["conversation_id":chatId]
        let URL = "\(baseurl)audioupload"
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(audio, withName: "file", fileName: "audio\(Int(Date().timeIntervalSince1970)).mp4", mimeType: "mp4")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to:URL /*, method: .post, HTTPHeaders: ["Authorization": secureCode]*/)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Audio Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON { response in
                    if let JSON = response.result.value {
                        let res = JSON
                        completion(true, res)
                    }
                }
            case .failure(let encodingError):
                //self.delegate?.showFailAlert()
                print(encodingError)
            }
        }
    }
    class func send(message: Message, chatid: String,fcm_token: [String],completion: @escaping (Bool,String,String) -> Swift.Void)  {
        
     //   print("SEND MSG")
        let currentUserID = SettingsViewController.userId
        //zasvar
        let values = ["id":message.id,"conversation_id":chatid,"owner_id": currentUserID, "content": message.content,"filename": message.filename, "file_size": message.file_size, "seen": ["\(currentUserID)"], "type": message.type,  "created_at": message.created_at, "duration": message.duration,"fcm_tokens":fcm_token , "owner_name": message.owner_name] as [String : Any]
        socket.emit("send_message", values)
        Message.uploadMessage(withValues: message, completion: { (status,msgid,sentid) in
            completion(status,msgid,sentid)
        })
    }

    class func uploadMessage(withValues: Message, completion: @escaping (Bool,String,String) -> Swift.Void) {
      //  print("UPLOAD MSG")
        let currentUserID = SettingsViewController.userId
        socket.on("sent_message"){data,ack in
            let sentmsg = JSON(data)
              //  print(sentmsg)
            let conversation_id = sentmsg[0]["conversation_id"].string!
            let sendermessageid = sentmsg[0]["sent_id"].string ?? ""
            let insertedid = sentmsg[0]["_id"].string!

            if sendermessageid == withValues.id {
    
                let updateConversation =  realm.objects(Conversation_realm.self).filter("id = '\(conversation_id)'")
                if updateConversation[0].id != "" {

                    var chatuser = [User_group]()

                    for user in updateConversation[0].user{

                        let users = User_group.init(id: user.id, image: user.image, nickname: user.nickname, lastname: user.lastname, surname: user.surname, phone: user.phone, state: user.state, online_at: Double(user.online_at), fcm_token: user.fcm_token,registerId: user.register)
                        chatuser.append(users)

                    }
                    //zasvar

                    let msg = Message.init(id: insertedid, owner_id: currentUserID, content: withValues.content,filename: withValues.filename, file_size: withValues.file_size, type: withValues.type, seen: ["\(currentUserID)"], created_at: withValues.created_at, duration: withValues.duration,conversation_id: withValues.conversation_id,name:withValues.owner_name )

                    var showing = [String]()
                    for shows in updateConversation[0].show{
                        showing.append(shows.show)
                    }

                    let conversationUpdate = Conversation.init(user: chatuser, lastmessage: msg, id: conversation_id, title: updateConversation[0].title, created_at: Double(updateConversation[0].created_at), is_thread: updateConversation[0].is_thread, show: showing, secret: updateConversation[0].secret, image: updateConversation[0].image,owner_id_con: updateConversation[0].owner_id_con)
                    RealmService.updateConversationToRealm(udpateConversation: conversationUpdate, completion: { (success) in
                        completion(true,insertedid, sendermessageid)
                    })

                }else{
                    print("empty")
                }
            }
        }
    }
    
    class func markMessagesRead(lastmessageid : String, conversation_id : String, save: Bool)  {
        let userid = SettingsViewController.userId
//        if save == true {
//            RealmService.updateMessageToRealm(updateMessageID: lastmessageid) { (success) in
//                if success ==  true {
//                    if SettingsViewController.online == true {
//                        socket.emit("mark_read",["user_id":userid,"message_id": lastmessageid,"conversation_id": conversation_id])
//                    }else{
//                        print("no internet connection")
//                    }
//                }
//            }
//        }else{
//            socket.emit("mark_read",["user_id":userid,"message_id": lastmessageid,"conversation_id": conversation_id])
//        }
        
         if SettingsViewController.online == true {
            RealmService.updateMessageToRealm(updateMessageID: lastmessageid) { (success) in
                if success ==  true {
                    socket.emit("mark_read",["user_id":userid,"message_id": lastmessageid,"conversation_id": conversation_id])
                }
            }
        }
//        else {
//            socket.emit("mark_read",["user_id":userid,"message_id": lastmessageid,"conversation_id": conversation_id])
//        }
    }
    func downloadImage(indexpathRow: Int, completion: @escaping (Bool, Int) -> Swift.Void)  {
        if self.type == "photo" {
            let imageLink = self.content
            let base = SettingsViewController.baseurl
            let imageURL = URL(string: "\(base)\(imageLink)")
            let url = imageURL!
            URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                if error == nil {
                    self.image = UIImage.init(data: data!)!
                    completion(true, indexpathRow)
                }
            }).resume()
        }
    }
    override init() {}
    //zasvar
    init(id:String,owner_id:String, content: String, filename: String, file_size: Int, type:String,seen:[String],created_at:Double,duration:Double,conversation_id:String , name:String) {
        self.id = id
        self.owner_id = owner_id
        self.content = content
        //zasvar
        self.filename = filename
        self.file_size = file_size
        
        self.type = type
        self.seen = seen
        self.created_at = created_at
        self.duration = duration
        self.conversation_id = conversation_id
        self.owner_name = name
    }
}
class Typing:NSObject{
    var owner: String = ""
    var touserid = [String]()
    var tochatid:String = ""
    var typing : Bool
    init(owner: String, touserid: [String], tochatid:String, typing:Bool){
        self.owner = owner
        self.touserid = touserid
        self.tochatid = tochatid
        self.typing = typing
    }
}
class TypingConversations : NSObject {
    var ownerid: String = ""
    var conversation_id:String = ""
    var typing : Bool
    var time = DispatchTime.now()
    init(ownerid: String, conversation_id:String, typing:Bool, time:DispatchTime){
        self.ownerid = ownerid
        self.conversation_id = conversation_id
        self.typing = typing
        self.time = time
    }
}
