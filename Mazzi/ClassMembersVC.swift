//
//  ClassMembersVC.swift
//  Mazzi
//
//  Created by Bayara on 12/7/18.
//  Copyright © 2018 Mazzi Application. All rights reserved.
//

import Foundation
import KVNProgress
import SwiftyJSON
import Apollo


class ClassMembersVC:UIViewController,UITableViewDelegate,UITableViewDataSource ,UITextFieldDelegate{

    var blackView = UIView()
    var linkView = UIView()
    var textfield = UITextField()
    var cancelBtn = UIButton()
    var addLinkBtn = UIButton()
    var label = UILabel()
    var descriptionLabel = UILabel()
    var userTopics = [String]()
    var Topics = ""
    var studentId = ""
    var selectedUserIndex : Int!
    var allTopics = [ClassTopicsQuery.Data.ClassTopic.Result]()
    @IBOutlet weak var mytableview: UITableView!
    var topics = [ObjectTopicsQuery.Data.ObjectTopic.Result]()
    var classId = GlobalStaticVar.classId
    var teacherId = GlobalStaticVar.teacherId
    var className = GlobalStaticVar.className
    var AllusersList = [ClassUsersListQuery.Data.ClassesUserListClassId]()
    var acceptedUsers =  [ClassUsersListQuery.Data.ClassesUserListClassId]()
    var pending = [ClassUsersListQuery.Data.ClassesUserListClassId]()
    var teachers = [ClassUsersListQuery.Data.ClassesUserListClassId]()
    var user = ClassUsersListQuery.Data.ClassesUserListClassId.UserId()
//    var apollo = SettingsViewController.apollo
    var myId = SettingsViewController.userId
    var selectedUserId = ""
    var selectedStudentId = ""
    var classUserListId = ""
    let refreshControl = UIRefreshControl()
    var apollo: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        // Add additional headers as needed
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
//        print("TOKEN APOLLOO:: ", GlobalVariables.headerToken)
        let url = URL(string:SettingsViewController.graphQL)!
        
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Ангийн гишүүд"
        self.linkviewInit()
        self.getUserList()
        self.mytableview.delegate = self
        self.mytableview.dataSource = self
        self.mytableview.register((UINib(nibName: "MemberCell", bundle: nil)), forCellReuseIdentifier: "member")
        self.mytableview.refreshControl = self.refreshControl
        self.refreshControl.addTarget(self, action: #selector(reloadData), for: .valueChanged)
    }
    
    @objc func reloadData(){
        checkInternet()
        if SettingsViewController.online == true {
            _ = self.apollo.clearCache()
            self.getUserList()
            self.getsubUser()
            self.getTopicList()
        }else{
            self.refreshControl.endRefreshing()
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        self.navigationController?.navigationBar.topItem?.title = ""
//        if self.teacherId != self.myId {
//            let rightBtn = UIBarButtonItem(image: UIImage(named:"logout"), style: .plain, target: self, action: #selector(self.leaveclass))
//            self.navigationController?.navigationBar.topItem?.rightBarButtonItem = rightBtn
//        }else{
//            let rightBtn = UIBarButtonItem(image: UIImage(named:"editmenu"), style: .plain, target: self, action: #selector(self.goSettings))
//            self.navigationController?.navigationBar.topItem?.rightBarButtonItem = rightBtn
//        }
      
    }
    
    @objc func goSettings(){
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "setting") as? ClassroomSettingVC
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        _ = self.apollo.clearCache()
        super.viewWillAppear(animated)
        self.studentId = ""
        self.getsubUser()
        self.getTopicList()
        self.classId = GlobalStaticVar.classId
    }
    
    override func viewDidDisappear(_ animated: Bool) {
//        self.navigationController?.navigationBar.topItem?.rightBarButtonItem = nil
    }
    
   @objc func leaveclass(){
    
        let alert = UIAlertController(title: "Ангиас гарах", message: "Та ангиас гарахдаа итгэлтэй байна уу ?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Тийм", style: .default  , handler: {action in
            checkInternet()
            if SettingsViewController.online {
                let mutation = LeaveClassMutation(userId: self.myId, classId: self.classId)
                self.apollo.perform(mutation: mutation) { result, error in
                    if result?.data?.leaveClassRoomList?.error == false{
                        KVNProgress.showSuccess()
                        self.navigationController?.popToRootViewController(animated: true)
                    }else{
                        KVNProgress.showError(withStatus: result?.data?.leaveClassRoomList?.message, on: self.view)
                    }
                }
            }else{
                KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу")
            }
        }))
        alert.addAction(UIAlertAction(title: "Үгүй", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
 
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        print("seciton:: ", indexPath.section)
        if indexPath.section == 1{
                 if self.pending.count > 0{
                     if self.myId == self.teacherId{
                        self.classUserListId = self.pending[indexPath.row].id ?? ""
                        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        alert.addAction(UIAlertAction(title: "Зөвшөөрөх", style: .default, handler: { action in
                            //                print("classUserListId::" , self.classUserListId)
                            self.selectedUserIndex = indexPath.row
                            self.acceptStudent()
                        }))
                        alert.addAction(UIAlertAction(title: "Татгалзах", style: .default, handler:{action in
                            self.selectedUserIndex = indexPath.row
                            self.rejectStudent()
                        }))
                        alert.addAction(UIAlertAction(title: "Болих", style: .cancel, handler: nil))
                        self.present(alert, animated: true)
                     }else{
                        self.toProfile(index: indexPath.row ,section: indexPath.section)
                    }
                }else{
                    if self.myId == self.teacherId{
                        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        alert.addAction(UIAlertAction(title: "Багш болгох", style: .default, handler: { action in
                            
                            self.blackView.isHidden = false
                            self.textfield.text = ""
                            self.textfield.becomeFirstResponder()
                             self.selectedUserIndex = indexPath.row
                            self.selectedStudentId = (self.acceptedUsers[indexPath.row].userId?.id!)!
                            
                        }))
                        alert.addAction(UIAlertAction(title: "Профайл үзэх", style: .default, handler:{action in
                            self.toProfile(index: indexPath.row,section: indexPath.section)
                        }))
                        alert.addAction(UIAlertAction(title: "Болих", style: .cancel, handler: nil))
                        self.present(alert, animated: true)
                    }else{
                        self.toProfile(index: indexPath.row ,section: indexPath.section)
                    }
                }
            
        }else if indexPath.section == 2{
            
            if self.myId == self.teacherId{
                let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                alert.addAction(UIAlertAction(title: "Багш болгох", style: .default, handler: { action in
                     self.selectedUserIndex = indexPath.row
                    self.blackView.isHidden = false
                    self.textfield.text = ""
                    self.textfield.becomeFirstResponder()
                    self.selectedStudentId = (self.acceptedUsers[indexPath.row].userId?.id!)!
                    
                }))
                alert.addAction(UIAlertAction(title: "Профайл үзэх", style: .default, handler:{action in
                    self.toProfile(index: indexPath.row,section: indexPath.section)
                }))
                alert.addAction(UIAlertAction(title: "Болих", style: .cancel, handler: nil))
                self.present(alert, animated: true)
            }else{
                 self.toProfile(index: indexPath.row ,section: indexPath.section)
            }
        }else{
            
            if self.myId != self.teachers[indexPath.row].userId?.id {
                if self.myId == self.teacherId{
                    let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                    alert.addAction(UIAlertAction(title: "Оюутан болгох", style: .default, handler: { action in
                        let sel_userId = self.teachers[indexPath.row].userId?.id!
                         self.selectedUserIndex = indexPath.row
                        self.demoteFromTeacher(selectedUser: sel_userId!)
                    }))
                    alert.addAction(UIAlertAction(title: "Профайл үзэх", style: .default, handler:{action in
                        self.toProfile(index: indexPath.row,section: indexPath.section)
                    }))
                    alert.addAction(UIAlertAction(title: "Болих", style: .cancel, handler: nil))
                    self.present(alert, animated: true)
                }else{
//                    print("section:: ",indexPath.section)
                    self.toProfile(index: indexPath.row,section: indexPath.section)
                }
            }else{
                self.performSegue(withIdentifier: "mytopics", sender: self)
            }
        }
    }
    
    func demoteFromTeacher(selectedUser: String){
        let mutation = DemoteFromTeacherMutation(userId: self.myId, subjectUserId: selectedUser, classId: self.classId)
        self.apollo.perform(mutation: mutation) { result, error in
            if result?.data?.demoteFromTeacher?.error == false{
                KVNProgress.showSuccess(withStatus: "Амжилттай")
                self.teachers.remove(at: self.selectedUserIndex)
                self.reloadData()
            }else{
                KVNProgress.showError(withStatus: result?.data?.demoteFromTeacher?.message)
            }
        }
    }
    
    func promoteToTeacher(selectedUserId: String){
        let mutation = PromoteToTeacherMutation(userId: self.myId, subjectUserId: selectedUserId, classId: self.classId)
         self.apollo.perform(mutation: mutation) { result, error in
            if result?.data?.promoteToTeacher?.error == false{
                KVNProgress.showSuccess(withStatus: "Амжилттай")
                self.Topics = ""
                self.reloadData()
            }else{
                KVNProgress.showError(withStatus: result?.data?.promoteToTeacher?.message)
            }
        }
    }
    
    func toProfile(index: Int, section: Int){
//        print("?????????????????????????????????????????????????/")
//        print("selected section:: ",section)
//        print("selected INDEX::: ", index)
        if section == 0{
            if self.myId != (self.teachers[index].userId?.id)!{
                KVNProgress.show(withStatus: "", on: self.view)
                let viewController = storyboard?.instantiateViewController(withIdentifier: "ProfileContact") as! ProfileContact
                viewController.contactid = (self.teachers[index].userId?.id)!
                viewController.username = (self.teachers[index].userId?.nickname)!
                viewController.membernickname = (self.teachers[index].userId?.nickname)!
                viewController.memberdate = (self.teachers[index].userId?.birthday)!
                viewController.member_onlineAt = (self.teachers[index].userId?.onlineAt)!
                viewController.memberphone = (self.teachers[index].userId?.phone)!
                viewController.memberImg = (self.teachers[index].userId?.image)!
                let navController = UINavigationController(rootViewController: viewController)
                KVNProgress.dismiss()
                GlobalVariables.isFromClassMembers = true
                self.present(navController, animated:true, completion: nil)
            }else{
                KVNProgress.showError(withStatus: "Өөрийнхөө профайл руу орох боломжгүй !", on: self.view)
            }
        }else{
            if self.myId != (self.acceptedUsers[index].userId?.id)!{
                KVNProgress.show(withStatus: "", on: self.view)
                let viewController = storyboard?.instantiateViewController(withIdentifier: "ProfileContact") as! ProfileContact
                viewController.contactid = (self.acceptedUsers[index].userId?.id)!
                viewController.username = (self.acceptedUsers[index].userId?.nickname)!
                viewController.membernickname = (self.acceptedUsers[index].userId?.nickname)!
                viewController.memberdate = (self.acceptedUsers[index].userId?.birthday)!
                viewController.member_onlineAt = (self.acceptedUsers[index].userId?.onlineAt)!
                viewController.memberphone = (self.acceptedUsers[index].userId?.phone)!
                viewController.memberImg = (self.acceptedUsers[index].userId?.image)!
                viewController.memberState = (self.acceptedUsers[index].userId?.state)!
                let navController = UINavigationController(rootViewController: viewController)
                KVNProgress.dismiss()
                 GlobalVariables.isFromClassMembers = true
                self.present(navController, animated:true, completion: nil)
            }else{
                GlobalVariables.isUpdateStudentId = true
                self.performSegue(withIdentifier: "updateStudentId", sender: self)
//                KVNProgress.showError(withStatus: "Өөрийнхөө профайл руу орох боломжгүй !", on: self.view)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let title : UILabel = UILabel()
        if section == 0 {
            title.textColor = GlobalVariables.purple
            title.text =  "Багш".uppercased()
        }else if section == 1 {
            
             if GlobalStaticVar.teacherId == self.myId{
                title.textColor = GlobalVariables.purple
                if self.pending.count > 0 {
                    title.text =  "Хүлээгдэж буй оюутнууд \("(")\(self.pending.count)\(")")".uppercased()
                }else{
                    if self.acceptedUsers.count > 0{
                        title.text =  "Оюутан \("(")\(self.acceptedUsers.count)\(")")".uppercased()
                    }else{
                         title.text =  ""
                    }
                }
             }else{
                title.textColor = GlobalVariables.purple
                title.text =  "Оюутан \("(")\(self.acceptedUsers.count)\(")")".uppercased()
            }

        }else {
            title.textColor = GlobalVariables.purple
            title.text =  "Оюутан \("(")\(self.acceptedUsers.count)\(")")".uppercased()
        }
        return title.text
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.white
        let headerLabel = UILabel(frame: CGRect(x: 4, y: 5, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height-5))
        headerLabel.font = UIFont.boldSystemFont(ofSize: 16)
        headerLabel.textColor = GlobalVariables.purple
        headerLabel.text = self.tableView(self.mytableview, titleForHeaderInSection: section)
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.teachers.count
        }else if section == 1 {
            if GlobalStaticVar.teacherId == self.myId{
                if self.pending.count > 0{
                     return self.pending.count
                }else{
                    return self.acceptedUsers.count
                }
            }else{
                return self.acceptedUsers.count
            }
            
        } else{
            return self.acceptedUsers.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            if self.pending.count > 0{
                return 30
            }else{
                return 30
            }
        }else{
             return 30
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if pending.count > 0{
            if GlobalStaticVar.teacherId == self.myId{
                 return 3
            }
             return 2
        }else{
             return 2
        }
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
            return "Ангиас хасах"
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle
    {
        if self.pending.count > 0{
            if indexPath.section == 2 {
                if self.teacherId == self.myId {
                    return UITableViewCell.EditingStyle.delete
                } else {
                    return UITableViewCell.EditingStyle.none
                }
            } else {
                return UITableViewCell.EditingStyle.none
            }
        }else{
            if indexPath.section == 1 {
                if self.teacherId == self.myId {
                    return UITableViewCell.EditingStyle.delete
                } else {
                    return UITableViewCell.EditingStyle.none
                }
            }else{
                 return UITableViewCell.EditingStyle.none
            }
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if indexPath.section == 2  || indexPath.section == 1 {
            if editingStyle == .delete {
                let name = self.acceptedUsers[indexPath.row].userId!.nickname!
                let selecteduseId = self.acceptedUsers[indexPath.row].userId?.id!
                self.selectedUserIndex = indexPath.row
                let alert = UIAlertController(title: "Ангиас хасах", message: "Та \("(")\(name)\(")") - г ангиас хасах гэж байна !", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Тийм", style: .default  , handler: {action in
                    self.kickUserQuery(selecteduseId!)
                }))
                alert.addAction(UIAlertAction(title: "Үгүй", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }else{
//               print("bi bhguieee")
            }
        }
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "member", for: indexPath) as! MemberTableViewCell
        if indexPath.row % 2 == 0 {
            cell.contentView.backgroundColor = GlobalVariables.F5F5F5
        }else{
            cell.contentView.backgroundColor = UIColor.white
        }
        cell.imgView.layer.borderColor = GlobalVariables.F5F5F5.cgColor
        cell.imgView.layer.borderWidth = 1
        if indexPath.section == 0 {
            cell.selectionStyle = .none
            let pic = self.teachers[indexPath.row].userId!.image ?? ""
            if pic == ""{
                cell.imgView?.image = UIImage(named: "circle-avatar")
            }else{
                cell.imgView.sd_setImage(with:URL(string: "\(SettingsViewController.fileurl)\(pic)"),placeholderImage: UIImage(named: "avatar"))
            }
            
            let nickname = self.teachers[indexPath.row].userId!.nickname ?? ""
            let lastname = self.teachers[indexPath.row].userId!.lastname ?? ""
             var top = [String]()
            for i in 0..<self.allTopics.count{
                if self.teachers[indexPath.row].userId?.id! == self.allTopics[i].userId{
//                    if !self.userTopics.contains(where:{$0 == self.allTopics[i].name!}){
                    
                        top.append(self.allTopics[i].name ?? "")
                        let joined = top.joined(separator: ", ")
                        cell.roleLabel.text = joined
//                    }
                }else{
//                    self.userTopics.removeAll()
                }
            }

            cell.label.text = lastname
            cell.lastnameLabel.text = nickname
        }else if indexPath.section == 1{
             if GlobalStaticVar.teacherId == self.myId{
                cell.roleLabel.isHidden = true
                cell.selectionStyle = .default
                if self.pending.count > 0 {
                    let pic = self.pending[indexPath.row].userId!.image ?? ""
                    if pic == ""{
                        cell.imgView?.image = UIImage(named: "circle-avatar")
                    }else{
                        cell.imgView?.sd_setImage(with: URL(string: "\(SettingsViewController.fileurl)\(pic)"), completed: nil)
                    }
                    
                    let nickname = self.pending[indexPath.row].userId!.nickname ?? ""
                    let lastname = self.pending[indexPath.row].userId!.lastname ?? ""
                    cell.label?.text = lastname
                    cell.lastnameLabel.text = nickname
                }else{
                    if self.acceptedUsers.count > 0 {
                        if self.myId == self.acceptedUsers[indexPath.row].userId?.id{
                            cell.roleLabel.isHidden = false
                            cell.roleLabel.text = "Оюутны код: \(self.studentId)"
                        }else{
                            cell.roleLabel.isHidden = true
                        }
                        
                        cell.selectionStyle = .none
                        if self.acceptedUsers.count > 0{
                            let pic = self.acceptedUsers[indexPath.row].userId!.image ?? ""
                            if pic == ""{
                                cell.imgView?.image = UIImage(named: "circle-avatar")
                            }else{
                                cell.imgView?.sd_setImage(with: URL(string: "\(SettingsViewController.fileurl)\(pic)"), completed: nil)
                            }
                            let nickname = self.acceptedUsers[indexPath.row].userId!.nickname ?? ""
                            let lastname = self.acceptedUsers[indexPath.row].userId!.lastname ?? ""
                            cell.label?.text = lastname
                            cell.lastnameLabel.text = nickname
                        }
                    }
                }
             }else{
                if self.acceptedUsers.count > 0 {
                    if self.myId == self.acceptedUsers[indexPath.row].userId?.id{
                        cell.roleLabel.isHidden = false
                        cell.roleLabel.text = "Оюутны код: \(self.studentId)"
                    }else{
                        cell.roleLabel.isHidden = true
                    }
                    
                    cell.selectionStyle = .none
                    if self.acceptedUsers.count > 0{
                        let pic = self.acceptedUsers[indexPath.row].userId!.image ?? ""
                        if pic == ""{
                            cell.imgView?.image = UIImage(named: "circle-avatar")
                        }else{
                            cell.imgView?.sd_setImage(with: URL(string: "\(SettingsViewController.fileurl)\(pic)"), completed: nil)
                        }
                        let nickname = self.acceptedUsers[indexPath.row].userId!.nickname ?? ""
                        let lastname = self.acceptedUsers[indexPath.row].userId!.lastname ?? ""
                        cell.label?.text = lastname
                        cell.lastnameLabel.text = nickname
                    }
                }
            }
           
        }else{
        
            if self.acceptedUsers.count > 0 {
                if self.myId == self.acceptedUsers[indexPath.row].userId?.id{
                    cell.roleLabel.isHidden = false
                    cell.roleLabel.text = "Оюутны код: \(self.studentId)"
                }else{
                    cell.roleLabel.isHidden = true
                }
                
                cell.selectionStyle = .none
                if self.acceptedUsers.count > 0{
                    let pic = self.acceptedUsers[indexPath.row].userId!.image ?? ""
                    if pic == ""{
                        cell.imgView?.image = UIImage(named: "circle-avatar")
                    }else{
                        cell.imgView?.sd_setImage(with: URL(string: "\(SettingsViewController.fileurl)\(pic)"), completed: nil)
                    }
                    let nickname = self.acceptedUsers[indexPath.row].userId!.nickname ?? ""
                    let lastname = self.acceptedUsers[indexPath.row].userId!.lastname ?? ""
                    cell.label?.text = lastname
                    cell.lastnameLabel.text = nickname
                }
            }
            
        }
    
        return cell
    }
    
    func getUserList(){
        checkInternet()
        if SettingsViewController.online == true {
            let query = ClassUsersListQuery(classId: self.classId)
            KVNProgress.show(withStatus: "", on: self.view)
            self.apollo.fetch(query: query) { result, error in
                KVNProgress.dismiss()
                
                if result?.data?.classesUserListClassId != nil {
                    self.AllusersList = (result?.data?.classesUserListClassId)! as! [ClassUsersListQuery.Data.ClassesUserListClassId]
                    
                    for i in 0..<self.AllusersList.count{
                        if self.AllusersList[i].classRole == "Admin" ||  self.AllusersList[i].classRole == "Teacher"{
                            if self.teachers.count > 0 {
                                if !self.teachers.contains(where:{$0.userId?.id == self.AllusersList[i].userId?.id}){
                                       self.teachers.append(self.AllusersList[i])
                                }
                            }else{
                                   self.teachers.append(self.AllusersList[i])
                            }
                        } else if self.AllusersList[i].state! == "joined" && self.AllusersList[i].classRole == "Student" {
                            if self.acceptedUsers.count > 0 {
                                if !self.acceptedUsers.contains(where:{$0.userId?.id == self.AllusersList[i].userId?.id}){
                                    self.acceptedUsers.append(self.AllusersList[i])
                                }
                            }else{
                                self.acceptedUsers.append(self.AllusersList[i])
                            }

                        
                        }else if self.AllusersList[i].state! == "pending" {
                            if self.pending.count > 0 {
                                if !self.pending.contains(where:{$0.userId?.id == self.AllusersList[i].userId?.id}){
                                    self.pending.append(self.AllusersList[i])
                                }
                            }else{
                                self.pending.append(self.AllusersList[i])
                            }
                        }
                    
                    }
                    self.mytableview.reloadData()
                    self.refreshControl.endRefreshing()
                }else{
                    
                }
            }
        }else{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу")
        }
    
    }
    
    func kickUserQuery(_ selectedUserId: String){
        checkInternet()
        if SettingsViewController.online {
            let mutation = KickClassRoomMutation(userId: self.myId, classId: self.classId, targetUserId: selectedUserId)
            KVNProgress.show(withStatus: "", on: self.view)
            self.apollo.perform(mutation: mutation) { result, error in
                KVNProgress.dismiss()
                if result?.data?.kickClassRoomList?.error == false{
                    KVNProgress.showSuccess()
                    _ = self.apollo.clearCache()
                    self.acceptedUsers.remove(at: self.selectedUserIndex)
                    self.mytableview.reloadData()
                }else{
                    KVNProgress.showError(withStatus: result?.data?.kickClassRoomList?.message)
                }
            }
        }else{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу")
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func acceptStudent(){
        checkInternet()
        if SettingsViewController.online {
            let mutation =  AcceptClassRoomListMutation(classUserListId: self.classUserListId, userId: self.myId)
            self.apollo.perform(mutation: mutation) { result, error in
                //             print("acceptResult:: ", result as Any)
                if result?.data?.acceptClassRoomList?.error == false{
                   self.pending.remove(at: self.selectedUserIndex)
                    self.mytableview.reloadData()
                    self.reloadData()
                }else{
                    KVNProgress.showError(withStatus: result?.data?.acceptClassRoomList?.message)
                }
            }
        }else{
             KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу")
        }
    }
    
    func rejectStudent(){
       checkInternet()
        if SettingsViewController.online {
            let mutation =  RejectClassRoomListMutation(classUserListId: self.classUserListId, userId: self.myId)
            self.apollo.perform(mutation: mutation) { result, error in
                //            print("rejectResult:: ", result as Any)
                if result?.data?.rejectClassRoomList?.error == false{
                    self.pending.remove(at: self.selectedUserIndex)
                    self.mytableview.reloadData()
                }else{
                    KVNProgress.showError(withStatus: result?.data?.rejectClassRoomList?.message)
                }
            }
        }else{
             KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу")
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (range.location == 0 && string == " ") {
            return false
        }else{
            return true
        }
    }
    
    
    func linkviewInit(){
        self.blackView.frame = CGRect(x:0 , y:0, width:self.view.bounds.width, height: self.view.bounds.height)
        self.blackView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        
        self.linkView.frame = CGRect(x:24 , y:130, width:self.view.bounds.width - 48, height: 240)
        self.linkView.backgroundColor = UIColor.white
        self.label.frame = CGRect(x: 16, y: 16,width:140,height: 25)
        self.textfield.frame = CGRect(x: 16, y: 49,width: self.linkView.bounds.width - 32 , height: 40)
        self.cancelBtn.frame = CGRect(x: 50, y:self.linkView.bounds.height - 50, width:100,height:40)
        self.addLinkBtn.frame = CGRect(x: 180, y:self.linkView.bounds.height - 50, width:100,height:40)
        self.descriptionLabel.frame = CGRect(x: 16, y: 100, width: self.linkView.bounds.width - 32, height: 40)
        self.descriptionLabel.font = GlobalVariables.customFont12
        self.descriptionLabel.text = "* Олон сэдэв оруулах бол сэдэв хооронд таслал тавихыг анхаарна уу!"
        self.descriptionLabel.textColor = GlobalVariables.purple.withAlphaComponent(0.8)
        self.descriptionLabel.textAlignment = .center
        self.descriptionLabel.numberOfLines = 3
        self.descriptionLabel.lineBreakMode = .byWordWrapping
        self.label.text = "Сэдэв оруулах:"
        self.cancelBtn.setTitle("Болих", for: UIControl.State.normal)
        self.addLinkBtn.setTitle("Оруулах", for: UIControl.State.normal)
        
        self.cancelBtn.addTarget(self, action: #selector(cancelTopic), for: .touchUpInside)
        self.addLinkBtn.addTarget(self, action: #selector(addTopic), for: .touchUpInside)
        self.cancelBtn.layer.cornerRadius = 5
        self.cancelBtn.backgroundColor = GlobalVariables.purple
        self.cancelBtn.setTitleColor(UIColor.white, for: .normal)
        self.addLinkBtn.layer.cornerRadius = 5
        self.addLinkBtn.backgroundColor = GlobalVariables.purple.withAlphaComponent(0.5)
        self.addLinkBtn.setTitleColor(UIColor.white, for: .normal)
        self.textfield.layer.borderColor = GlobalVariables.purple.cgColor
        self.textfield.layer.borderWidth = 1
        self.textfield.layer.cornerRadius = 5
        self.textfield.textContentType = UITextContentType.URL
        self.textfield.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        self.textfield.delegate = self
        self.textfield.autocapitalizationType = .none
        self.textfield.borderStyle = .roundedRect
        let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        self.textfield.bounds.inset(by: padding)
    
        self.textfield.layer.masksToBounds = true
        self.cancelBtn.layer.masksToBounds = true
        self.addLinkBtn.layer.masksToBounds = true
        self.addLinkBtn.isEnabled = false
        
        self.blackView.addSubview(self.linkView)
        self.linkView.addSubview(self.label)
        self.linkView.addSubview(self.textfield)
        self.linkView.addSubview(self.cancelBtn)
        self.linkView.addSubview(self.addLinkBtn)
        self.linkView.addSubview(self.descriptionLabel)
        self.view.addSubview(self.blackView)
        self.blackView.isHidden = true
    }
    
    @objc func cancelTopic(){
        UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.blackView.isHidden = true
        })
        self.textfield.resignFirstResponder()
    }
    
    
    
    @objc func addTopic(){
        UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.blackView.isHidden = true
            if self.textfield.text != ""{
                self.assignTopic()
            }else{
                KVNProgress.showError(withStatus: "Топик заавал оруулна уу")
            }
        })
        self.textfield.resignFirstResponder()
    }
    
    @objc func textFieldDidChange(textField:UITextField){
        
        if (textfield.text?.count)! > 0{
            self.addLinkBtn.backgroundColor = GlobalVariables.purple
            self.addLinkBtn.setTitleColor(UIColor.white, for: .normal)
            self.addLinkBtn.isEnabled = true
        } else{
            self.addLinkBtn.backgroundColor = GlobalVariables.purple.withAlphaComponent(0.5)
            self.addLinkBtn.setTitleColor(UIColor.white, for: .normal)
            self.addLinkBtn.isEnabled = false
        }
    }
    
    func getTopicList(){
        checkInternet()
        if SettingsViewController.online {
            let query = ClassTopicsQuery(classId: self.classId)
            KVNProgress.show(withStatus: "", on: self.view)
            self.apollo.fetch(query: query) { result, error in
                KVNProgress.dismiss()
                if result?.data?.classTopics?.result != nil{
                    self.allTopics = result?.data?.classTopics?.result as! [ClassTopicsQuery.Data.ClassTopic.Result]
             
                    self.mytableview.reloadData()
                }else{
                    KVNProgress.showError(withStatus: result?.data?.classTopics?.message)
                }
            }
        }else{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу")
        }
    }
    
    
    func assignTopic(){
        checkInternet()
        if SettingsViewController.online {
            let mutation = InsertTopicMutation(userId: self.selectedStudentId, classId: self.classId, name: self.textfield.text!)
            self.apollo.perform(mutation: mutation) { result, error in
                if result?.data?.insertTopic?.error == false{
                    KVNProgress.showSuccess(withStatus: "Амжилттай")
                    self.promoteToTeacher(selectedUserId: self.selectedStudentId)
                    self.acceptedUsers.remove(at: self.selectedUserIndex)
                }else{
                    KVNProgress.showError(withStatus: result?.data?.insertTopic?.message)
                }
            }
        }else{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу")
        }
        
    
    }
    
    
    func getsubUser(){
        checkInternet()
        if SettingsViewController.online {
            let query = GetsubUserQuery.init(userId: self.myId)
            self.apollo.fetch(query: query) { result, error in
                if let path =  result?.data?.subUser{
                    self.studentId = path.studentIdCode ?? ""
                    self.mytableview.reloadData()
                }
            }
        }else{
             KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу")
        }
       
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "updateStudentId"{
            let controller = segue.destination as! StudentIdRegVC
            controller.myStudentId = self.studentId
        }
    }
}
