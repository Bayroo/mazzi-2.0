//
//  JoinedUsersListVC.swift
//  Mazzi
//
//  Created by Bayara on 1/10/19.
//  Copyright © 2019 woovoo. All rights reserved.
//

import Foundation
import Apollo
import KVNProgress
import EFQRCode

class JoinedUsersListVC: UIViewController,UITableViewDataSource,UITableViewDelegate{
    var classId = GlobalStaticVar.classId
    var attendanceId = ""
    var selectedUserId = ""
    var selectedUserIndex = 0
    var myId = ""
    var state = ""
    var code = ""
    var type = ""
    var refrechControl = UIRefreshControl()
    var dateformatter = DateFormatter()
    var endTime : Double!
    var users = [AttendancesEntryRecordQuery.Data.AttendancesEntryRecord]()
    var apollo: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        // Add additional headers as needed
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
        //        print("TOKEN APOLLOO:: ", GlobalVariables.headerToken)
        let url = URL(string:SettingsViewController.graphQL)!
        
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customize()
    }
    
    func customize(){
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(back))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:"showQR"), style: .plain, target: self, action: #selector(showQR))
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "Ирц өгсөн оюутнууд"
        self.tableview.delegate = self
        self.tableview.dataSource = self
        self.tableview.register((UINib(nibName: "joinedUserCell", bundle: nil)), forCellReuseIdentifier: "joinedUser")
        self.tableview.refreshControl = self.refrechControl
        self.refrechControl.addTarget(self, action: #selector(reloadData), for: .valueChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         _ = self.apollo.clearCache()
        self.myId = SettingsViewController.userId
        self.getAttendanceUserslist()
    }
    
    @objc func back(){
        _ = self.apollo.clearCache()
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func showQR(){
        let dic = ["code":self.code, "type":self.type]
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let utf8str = jsonString?.data(using: String.Encoding.utf8)
        let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        
        self.generateQR(content: base64Encoded!)
    }
    
    func generateQR(content: String){
        if let tryImage = EFQRCode.generate(
            content: content
            //            watermark: UIImage(named: "avatar")?.toCGImage()
            ){
            let myCIQR = self.convertCGImageToCIImage(inputImage: tryImage)
            let myQR = self.convert(cmage: myCIQR!)
            self.back()
            let alert = CustomAlertView.init(title: self.code , image: myQR)
            alert.show(animated: true)
            //    alert.rescan.addTarget(self, action: #selector(goscaneer), for: UIControlEvents.touchUpInside)
        } else {
            print("Create QRCode image failed!")
        }
    }
    
    func convertCGImageToCIImage(inputImage: CGImage) -> CIImage! {
        let ciImage = CIImage(cgImage: inputImage, options: nil)
        return ciImage
    }
    
    func convert(cmage:CIImage) -> UIImage
    {
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent)!
        let image:UIImage = UIImage.init(cgImage: cgImage)
        return image
    }
    
    
    func getAttendanceUserslist(){
        let query = AttendancesEntryRecordQuery(classId: self.classId, attendanceId: self.attendanceId)
          self.apollo.fetch(query: query) { result, error in
//            print("RESULT :: ", result?.data?.attendancesEntryRecord![0]?.attendanceEntry?.userId)
            if result?.data?.attendancesEntryRecord != nil {
                let list =  (result?.data?.attendancesEntryRecord)! as! [AttendancesEntryRecordQuery.Data.AttendancesEntryRecord]
                
                for i in 0..<list.count{
                    if list[i].attendanceEntry != nil {
                        for i in 0..<list.count{
                            if list[i].attendanceEntry != nil {
                                if self.users.count > 0{
                                    if !self.users.contains(where:{$0.userId?.id == list[i].userId?.id}){
                                        if list[i].userId?.id != self.myId{
                                            self.users.append(list[i])
                                            self.tableview.reloadData()
                                        }
                                    }
                                }else{
                                    if list[i].userId?.id != self.myId{
                                        self.users.append(list[i])
                                        self.tableview.reloadData()
                                    }
                                }
                            }
                        }
                    }
                }
                 self.tableview.reloadData()
            }else{
                
            }
        }
    }
    
    @objc func reloadData(){
         _ = self.apollo.clearCache()
        let query = AttendancesEntryRecordQuery(classId: self.classId, attendanceId: self.attendanceId)
        self.apollo.fetch(query: query) { result, error in
            if result?.data?.attendancesEntryRecord != nil {
                let list =  (result?.data?.attendancesEntryRecord)! as! [AttendancesEntryRecordQuery.Data.AttendancesEntryRecord]
                
                for i in 0..<list.count{
                    if list[i].attendanceEntry != nil {
                        if self.users.count > 0{
                            if !self.users.contains(where:{$0.userId?.id == list[i].userId?.id}){
                                if list[i].userId?.id != self.myId{
                                    self.users.append(list[i])
                                    self.tableview.reloadData()
                                    self.refrechControl.endRefreshing()
                                }
                            }
                        }else{
                             if list[i].userId?.id != self.myId{
                                self.users.append(list[i])
                                self.tableview.reloadData()
                                self.refrechControl.endRefreshing()
                            }
                        }
                    }
                }
//                 self.tableview.reloadData()
//                self.refrechControl.endRefreshing()
            }else{
                
            }
        }
        self.refrechControl.endRefreshing()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "joinedUser", for: indexPath) as! joinedUserCell
        cell.imgVIew?.layer.cornerRadius = 20
        cell.imgVIew?.layer.borderColor = GlobalVariables.F5F5F5.cgColor
        cell.imgVIew?.layer.masksToBounds = true
   
        let nickname = self.users[indexPath.row].userId!.nickname ?? ""
        let lastname = self.users[indexPath.row].userId!.lastname ?? ""
        cell.nameLabel.text = lastname
        cell.lastnameLabel.text = nickname
        let pic = self.users[indexPath.row].userId!.image ?? ""
        if pic == ""{
            cell.imgVIew?.image = UIImage(named: "circle-avatar")
        }else{
            cell.imgVIew?.sd_setImage(with: URL(string: "\(SettingsViewController.fileurl)\(pic)"), completed: nil)
        }

        if indexPath.row % 2 == 0{
            cell.backgroundColor = GlobalVariables.F5F5F5
        }else{
            cell.backgroundColor = UIColor.white
        }
        if self.users[indexPath.row].attendanceEntry?.checkedIn != 0 {
            if self.endTime < (self.users[indexPath.row].attendanceEntry?.checkedIn)!{
                let late =  (self.users[indexPath.row].attendanceEntry?.checkedIn)! - self.endTime
                let gg = NSDate(timeIntervalSince1970: (late/1000))
                self.dateformatter.dateStyle = .none
                self.dateformatter.timeStyle = .short
                let latedMin = self.dateformatter.string(from: gg as Date)
                let lateddate = self.dateformatter.date(from: latedMin)
                let comps = Calendar.current.dateComponents([.hour , .minute], from: lateddate!)
                let minute = comps.minute
                
                let startTime = NSDate(timeIntervalSince1970: ((self.users[indexPath.row].attendanceEntry?.checkedIn)!/1000))
                self.dateformatter.dateStyle = .none
                self.dateformatter.timeStyle = .short
                let started = self.dateformatter.string(from: startTime as Date)
                cell.timelabel.textColor = UIColor.red
                cell.timelabel.text = "\(started) (-\(minute!)) мин"
              
           
            }else{
                let startTime = NSDate(timeIntervalSince1970: ((self.users[indexPath.row].attendanceEntry?.checkedIn)!/1000))
                self.dateformatter.dateStyle = .none
                self.dateformatter.timeStyle = .short
                let started = self.dateformatter.string(from: startTime as Date)
                cell.timelabel.textColor = GlobalVariables.green
                cell.timelabel.text = started
            }
            
        }else{
            cell.timelabel.textColor = UIColor.black
            if self.users[indexPath.row].attendanceEntry?.state == "" || self.users[indexPath.row].attendanceEntry?.state == " "{
                cell.timelabel.text =  "-"
            }else{
                cell.timelabel.text = self.users[indexPath.row].attendanceEntry?.state ?? "-"
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
            self.selectedUserId = self.users[indexPath.row].userId?.id ?? ""
            self.selectedUserIndex = indexPath.row
            let actionSheet = UIAlertController(title: "", message: "Ирцийн төлөв сонгох", preferredStyle: .actionSheet)
            
            actionSheet.addAction(UIAlertAction(title: "Тасалсан", style: .default, handler: { (action) -> Void in
               self.changeState(state: "Тасалсан")
            }))
            
            actionSheet.addAction(UIAlertAction(title: "Чөлөөтэй", style: .default, handler: { (action) -> Void in
                self.changeState(state: "Чөлөөтэй")
            }))
            
            actionSheet.addAction(UIAlertAction(title: "Өвчтэй", style: .default, handler: { (action) -> Void in
                 self.changeState(state: "Өвчтэй")
            }))
            
            actionSheet.addAction(UIAlertAction(title: "Болих", style: .cancel, handler: nil))
            self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func changeState(state: String!){
        let mutation = UpdateAttendanceEntryStateMutation(userId: self.selectedUserId, attId: self.attendanceId, state: state, classId: self.classId)
         self.apollo.perform(mutation: mutation) { result, error in
          
            if result?.data?.updateAttendanceEntryState?.error == false{
//                KVNProgress.showSuccess()
                self.users.remove(at: self.selectedUserIndex)
                self.reloadData()
            }else{
                KVNProgress.showError(withStatus: result?.data?.updateAttendanceEntryState?.message)
            }
        }
    }
}
