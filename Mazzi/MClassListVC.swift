//
//  MClassListVC.swift
//  Mazzi
//
//  Created by Bayara on 11/20/18.
//  Copyright © 2018 Mazzi Application LLC. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import KVNProgress
import SwiftyJSON
import Apollo

// regSudentId
class  MClassListVC:UIViewController,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet weak var mytableView: UITableView!
    
    @IBOutlet weak var qrBtn: UIButton!
    
    var roleId = ""
    var state = ""
//    var createdClass = [MclassListQuery.Data.UsersClassList]()
//    var pendingClass = [MclassListQuery.Data.UsersClassList]()
//    var AcceptedClass = [MclassListQuery.Data.UsersClassList]()
//    var allClassList = [MclassListQuery.Data.UsersClassList]()
     var allClassList = [UsersCorporateMemberListClassPopulatedQuery.Data.UsersCorporateMemberListClassPopulated.Result.Class]()
     var AcceptedClass = [UsersCorporateMemberListClassPopulatedQuery.Data.UsersCorporateMemberListClassPopulated.Result.Class]()
     var pendingClass = [UsersCorporateMemberListClassPopulatedQuery.Data.UsersCorporateMemberListClassPopulated.Result.Class]()
     var createdClass = [UsersCorporateMemberListClassPopulatedQuery.Data.UsersCorporateMemberListClassPopulated.Result.Class]()
    
    var selectedClassID = ""
    var myId = SettingsViewController.userId
    var corpId = ""
    var teacherId = ""
    var className = ""
    var ownerId = ""
    var studentId = ""
    var tierId = ""
    var badgeCount = 0
    var selectedSection = 0
    var selectedIndex = 0
    var myNotify = [SumNotificationsQuery.Data.SumNotification.Result.Mclass]()
    let apollos: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        // Add additional headers as needed
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
        //        print("TOKEN APOLLOO:: ", GlobalVariables.headerToken)
        let url = URL(string:SettingsViewController.graphQL)!
        
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    private let refreshControl = UIRefreshControl()
    //    weak var delegate: PostsVC!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customize()
        self.navigationController?.navigationBar.barTintColor = GlobalVariables.black
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("URL :: ",SettingsViewController.graphQL)
        print("ROLE:: ", self.roleId)
        super.viewWillAppear(animated)
        if self.roleId == "5cdcdda4f29e0d3284d8acbe"{
            self.mytableView.isHidden = true
            self.qrBtn.isHidden = false
        }else{
            self.mytableView.isHidden = false
            self.qrBtn.isHidden = true
            self.getDatas()
        }
       
        checkInternet()
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateBadge(notification:)), name: Notification.Name("received_notify"), object: nil)
        self.myId = SettingsViewController.userId
        self.corpId = ""
        self.studentId = ""
        self.tierId = ""
        _ = self.apollos.clearCache()
        self.getSubUser()

        if SettingsViewController.online == true {
//                self.getDatas()
        }else{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу!")
        }
        
          self.getNotifyBadge()
        if GlobalStaticVar.FromNotify == true {
            self.pushFromNotify()
        }
        
    }
    
    @objc func updateBadge(notification:Notification){
        self.allClassList.removeAll()
        self.createdClass.removeAll()
        self.AcceptedClass.removeAll()
        self.pendingClass.removeAll()
//        self.getDatas()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        GlobalStaticVar.selectedClassId = ""
        GlobalStaticVar.FromNotify = false
    }
    
    func customize(){
        self.title = "M CLASS"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(backtoRoot))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:"nemehBTN"), style: .plain, target: self, action: #selector(classRequest))
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.mytableView.delegate = self
        self.mytableView.dataSource = self
        self.mytableView.register((UINib(nibName: "MClassListCell", bundle: nil)), forCellReuseIdentifier: "mclass")
        self.mytableView.refreshControl = self.refreshControl
        self.refreshControl.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        self.refreshControl.attributedTitle =  NSAttributedString(string: "", attributes: nil)
        
        self.qrBtn.layer.cornerRadius = 5
        self.qrBtn.backgroundColor = GlobalVariables.purple
        self.qrBtn.setTitle("QR Уншуулах", for: .normal)
        self.qrBtn.setTitleColor(UIColor.white, for: .normal)
        self.qrBtn.addTarget(self, action: #selector(toQR), for: .touchUpInside)
        self.qrBtn.layer.masksToBounds = true
    }
    
    @objc func toQR(){
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "QRscanVC") as? QRscanVC
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
    @objc func refreshList(){
         checkInternet()
        if SettingsViewController.online {
            self.getDatas()
        }else{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу!")
        }
    }
    
    @objc func backtoRoot(){
        GlobalVariables.isFromMClass = true
        self.navigationController?.popViewController(animated: true)
    }
    
    func pushFromNotify(){
        if GlobalStaticVar.notifiedType == "announcement"{
             GlobalStaticVar.newPost = true
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "maintab") as? MainTabbarVC
            self.navigationController?.pushViewController(viewController!, animated: false)
        }else if GlobalStaticVar.notifiedType == "comment" {
            GlobalStaticVar.notifiedComment = true
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "maintab") as? MainTabbarVC
            self.navigationController?.pushViewController(viewController!, animated: false)
            
        }else if GlobalStaticVar.notifiedType == "acceptedIntoClass"{
            
        }else if GlobalStaticVar.notifiedType == "requestClass" {
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "maintab") as? MainTabbarVC
//            GlobalStaticVar.classId = GlobalStaticVar.notifiedClassId
            viewController?.selectedIndex = 1
            self.navigationController?.pushViewController(viewController!, animated: false)
        }else if GlobalStaticVar.notifiedType == "approvedPost" {
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "maintab") as? MainTabbarVC
            self.navigationController?.pushViewController(viewController!, animated: false)
        }else if GlobalStaticVar.notifiedType == "approveAnnouncement" {
            GlobalStaticVar.isApprovePost = true
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "maintab") as? MainTabbarVC
            self.navigationController?.pushViewController(viewController!, animated: false)
        }
    }
    
    @objc func classRequest(){
         if SettingsViewController.online {
            self.getSubUser()
            if self.studentId != ""{
                self.performSegue(withIdentifier: "torequest", sender: self)
            }else{
                if self.tierId == ""{
                    self.alertForStudentId()
                }else{
                    self.performSegue(withIdentifier: "torequest", sender: self)
                }
            }
         }else{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу!")
        }
       
    }
    
    func getDatas(){

        print("USERID:: ", self.myId)
        print("corpId:: ", self.corpId)
        let query = MclassListQuery(userId: self.myId, corpId: self.corpId)
        KVNProgress.show(withStatus: "", on: self.view)
        self.apollos.fetch(query: query) { result, error in
            KVNProgress.dismiss()
//            print("LIST:: ", result)
            if self.allClassList.count > 0 {
//                self.allClassList = result!.data!.usersClassList! as! [MclassListQuery.Data.UsersClassList]
                for i in 0..<self.allClassList.count {
                    if self.allClassList[i].state == "joined" && self.allClassList[i].classRole == "Student" {
                        if self.allClassList[i].classOwnerId != self.myId {
                            if self.AcceptedClass.count > 0 {
                                  if !self.AcceptedClass.contains(where:{$0.classId == self.allClassList[i].classId ?? ""}){
                                    self.AcceptedClass.append(self.allClassList[i])
                                    self.mytableView.reloadData()
                                    self.refreshControl.endRefreshing()
                                }
                            }else{
                                self.AcceptedClass.append(self.allClassList[i])
                                self.mytableView.reloadData()
                                self.refreshControl.endRefreshing()
                            }


                        }
                    }else if  self.allClassList[i].state == "pending" {
                        if self.pendingClass.count > 0 {
                            if !self.pendingClass.contains(where:{$0.classId == self.allClassList[i].classId ?? ""}){
                                self.pendingClass.append(self.allClassList[i])
                                self.mytableView.reloadData()
                                self.refreshControl.endRefreshing()
                            }
                        }else{
                            self.pendingClass.append(self.allClassList[i])
                            self.mytableView.reloadData()
                            self.refreshControl.endRefreshing()
                        }

                    }
                    if (self.allClassList[i].classOwnerId?.contains(self.myId))! || self.allClassList[i].classRole != "Student"  {
                        if self.createdClass.count > 0 {
                             if !self.createdClass.contains(where:{$0.classId == self.allClassList[i].classId ?? ""}){
                                self.createdClass.append(self.allClassList[i])
                                self.mytableView.reloadData()
                                self.refreshControl.endRefreshing()
                            }

                        }else{
                            self.createdClass.append(self.allClassList[i])
                            self.mytableView.reloadData()
                            self.refreshControl.endRefreshing()
                        }

                    }

                }
             self.getNotifyBadge()
                self.refreshControl.endRefreshing()
            }else{
                 self.refreshControl.endRefreshing()
                print("ERORR:: ", result?.data)
            }

            // print("BADGE COUNT MCLASS::", self.createdClass[0].notificationCount)
        }
//        print("BLALBA:: ", self.AcceptedClass)
//        print("BLALBA:: ", self.AcceptedClass.count)
    }
    
//    func getRoles(){
//        let baseurl = SettingsViewController.baseurl
//        let headers = SettingsViewController.headers
//        Alamofire.request("\(baseurl)roles", method:.post, parameters: nil, encoding: JSONEncoding.default,headers:headers).responseJSON{ response in
//            let res = JSON(response.value!)
//            //            print("res:",res)
//        }
//    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let title : UILabel = UILabel()
        if section == 0{
            title.text =  "анги".uppercased()
        }else if section == 1{
            title.text =  "Миний элссэн анги".uppercased()
        }else{
            title.text =  "Хүлээгдэж буй анги".uppercased()
        }
        return title.text
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.white
        let headerLabel = UILabel(frame: CGRect(x: 0, y: 5, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height-5))
        headerLabel.font = UIFont.boldSystemFont(ofSize: 16)
        headerLabel.textColor =   GlobalVariables.purple
        headerLabel.text = self.tableView(self.mytableView, titleForHeaderInSection: section)
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            if createdClass.count == 0{
                if AcceptedClass.count == 0{
                    if pendingClass.count == 0{
                        return 30
                    }
                    else{
                        return 0
                    }
                }else{
                    return 0
                }
            }else{
                return 30
            }
        }else if section == 1{
            if AcceptedClass.count == 0{
                return 0
            }else{
                return 30
            }
        }else{
            if pendingClass.count == 0 {
                return 0
            }else{
                return 30
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if SettingsViewController.online {
            self.selectedSection = indexPath.section
            self.selectedIndex = indexPath.row
            if self.studentId != "" {
                if indexPath.section == 0{
                    if self.createdClass.count == 0{
                        self.performSegue(withIdentifier: "torequest", sender: self)
                    }else{
                        self.selectedClassID = self.createdClass[indexPath.row].classId ?? ""
                        GlobalStaticVar.teacherId = self.createdClass[indexPath.row].classOwnerId ?? ""
                        GlobalStaticVar.className = self.createdClass[indexPath.row].className ?? ""
                        GlobalStaticVar.classId = self.createdClass[indexPath.row].classId ?? ""
                        GlobalStaticVar.classCode = self.createdClass[indexPath.row].classCode ?? ""
                        //                    self.decreaseNotifyCount()
                        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "maintab") as? MainTabbarVC
                        self.navigationController?.pushViewController(viewController!, animated: true)
                    }
                }else if indexPath.section == 1{
                    if self.AcceptedClass.count > indexPath.row{
                        self.selectedClassID = self.AcceptedClass[indexPath.row].classId ?? ""
                        GlobalStaticVar.teacherId = self.AcceptedClass[indexPath.row].classOwnerId ?? ""
                        GlobalStaticVar.className = self.AcceptedClass[indexPath.row].className ?? ""
                        GlobalStaticVar.classId = self.AcceptedClass[indexPath.row].classId ?? ""
                        GlobalStaticVar.classCode = self.AcceptedClass[indexPath.row].classCode ?? ""
                        //                     self.decreaseNotifyCount()
//                        print("SELECTEDCLASS ID ::: ", GlobalStaticVar.className)
                        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "maintab") as? MainTabbarVC
                        self.navigationController?.pushViewController(viewController!, animated: true)
                    }
                }else{
                    let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                    alert.addAction(UIAlertAction(title: "Цуцлах", style: .default, handler: { action in
                        self.cancelRequest(index: indexPath.row)
                    }))
                    alert.addAction(UIAlertAction(title: "Болих", style: .cancel, handler: nil))
                    self.present(alert, animated: true)
                }
            }else{
                if self.tierId == ""{
                    self.alertForStudentId()
                }else{
                    if indexPath.section == 0{
                        if self.createdClass.count == 0{
                            self.performSegue(withIdentifier: "torequest", sender: self)
                        }else{
                            self.selectedClassID = self.createdClass[indexPath.row].classId ?? ""
                            GlobalStaticVar.teacherId = self.createdClass[indexPath.row].classOwnerId ?? ""
                            GlobalStaticVar.className = self.createdClass[indexPath.row].className ?? ""
                            GlobalStaticVar.classId = self.createdClass[indexPath.row].classId ?? ""
                            GlobalStaticVar.classCode = self.createdClass[indexPath.row].classCode ?? ""
                            //                         self.decreaseNotifyCount()
                            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "maintab") as? MainTabbarVC
                            self.navigationController?.pushViewController(viewController!, animated: true)
                        }
                    }else if indexPath.section == 1{
                        if self.AcceptedClass.count > indexPath.row{
                            self.selectedClassID = self.AcceptedClass[indexPath.row].classId ?? ""
                            GlobalStaticVar.teacherId = self.AcceptedClass[indexPath.row].classOwnerId ?? ""
                            GlobalStaticVar.className = self.AcceptedClass[indexPath.row].className ?? ""
                            GlobalStaticVar.classId = self.AcceptedClass[indexPath.row].classId ?? ""
                            GlobalStaticVar.classCode = self.AcceptedClass[indexPath.row].classCode ?? ""
                            //                         self.decreaseNotifyCount()
                            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "maintab") as? MainTabbarVC
                            self.navigationController?.pushViewController(viewController!, animated: true)
                        }
                    }else{
                        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        alert.addAction(UIAlertAction(title: "Цуцлах", style: .default, handler: { action in
                            self.cancelRequest(index: indexPath.row)
                        }))
                        alert.addAction(UIAlertAction(title: "Болих", style: .cancel, handler: nil))
                        self.present(alert, animated: true)
                    }
                }
            }
        }else{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу")
        }
    }
    
    func alertForStudentId(){
       
        let alert = UIAlertController(title: "Оюутны дугаараа оруулна уу", message: "Оюутны дугаараа бүртгүүлээгүй бол үйлчилгээ авах боломжгүйг анхаарна уу", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Оруулах", style: .default  , handler: {action in
            self.performSegue(withIdentifier: "regSudentId", sender: self)
        }))
        alert.addAction(UIAlertAction(title: "Болих", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            if createdClass.count == 0 && AcceptedClass.count == 0 && pendingClass.count == 0{
                return 1
            }else{
                return createdClass.count
            }
        }else if section == 1{
            return AcceptedClass.count
        }else{
            return pendingClass.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        _ = self.apollos.clearCache()
        let cell = tableView.dequeueReusableCell(withIdentifier: "mclass", for: indexPath) as! MClassListCell
        cell.initcellData()
        cell.badge.isHidden = true
        if indexPath.section == 0{
            if self.createdClass.count == 0{
                if self.AcceptedClass.count == 0{
                    if self.pendingClass.count == 0{
                        cell.mylabel.text = "Анги руу орох хүсэлт илгээх"
                    }
                }
            }else{
                cell.mylabel.text = self.createdClass[indexPath.row].className ?? ""
                cell.classCodeLabel.text = "\("Ангийн код: ")\(self.createdClass[indexPath.row].classCode!)"
                for i in 0..<self.myNotify.count{
                    if self.createdClass[indexPath.row].classId == self.myNotify[i].classId{
                        let badgeCounts = self.myNotify[i].badge ?? 0
                        let badge = String(badgeCounts)
                        if badgeCounts > 0 {
                            cell.badge.isHidden = false
                            cell.badge.setTitle(badge, for: UIControl.State.normal)
                        }else{
//                            cell.badge.isHidden = true
                        }
                    }
                }
                
            }
        }else if indexPath.section == 1{
            if self.AcceptedClass.count > 0 {
                cell.mylabel.text = self.AcceptedClass[indexPath.row].className ?? ""
                cell.classCodeLabel.text = "\("Ангийн код: ")\(self.AcceptedClass[indexPath.row].classCode!)"
                for i in 0..<self.myNotify.count{
                    if self.myNotify[i].classId == self.AcceptedClass[indexPath.row].classId{
                        let badgeCounts = self.myNotify[i].badge ?? 0
                        if badgeCounts > 0 {
                             let badge = String(badgeCounts)
                            cell.badge.isHidden = false
                            cell.badge.setTitle(badge, for: UIControl.State.normal)
                        }else{
//                            cell.badge.isHidden = true
                        }
                    }
                }
            }
        }else{
            cell.mylabel.text = self.pendingClass[indexPath.row].className ?? ""
            cell.classCodeLabel.text = self.pendingClass[indexPath.row].classCode ?? ""
        }
        return cell
    }
    
    func cancelRequest(index: Int){
        let selectedClassId = self.pendingClass[index].classId ?? ""
        let mutation = LeaveClassMutation(userId: self.myId, classId: selectedClassId)
        self.apollos.perform(mutation: mutation) { result, error in
            if result?.data?.leaveClassRoomList?.error == false{
                KVNProgress.showSuccess(withStatus: "Хүсэлт цуцлагдлаа", on: self.view)
                self.pendingClass.remove(at: index)
                self.mytableView.reloadData()
            }else{
                KVNProgress.showError(withStatus: result?.data?.leaveClassRoomList?.message, on: self.view)
            }
        }
    }
    
    func getSubUser(){
        let query = GetsubUserQuery.init(userId: self.myId)
        self.apollos.fetch(query: query) { result, error in
            if let path =  result?.data?.subUser{
                self.studentId = path.studentIdCode ?? ""
                self.tierId = path.tierId ?? ""
                GlobalVariables.studentID = self.studentId
            }
        }
    }
    
    func getNotifyBadge(){
        _ = self.apollos.clearCache()
        self.myNotify = []
        let query = SumNotificationsQuery(userId: self.myId)
        self.apollos.fetch(query: query) { result, error in
            if result?.data?.sumNotifications != nil {
                self.myNotify = (result!.data?.sumNotifications?.result?.mclass)! as! [SumNotificationsQuery.Data.SumNotification.Result.Mclass]
//                print("NOTIF::: ", self.myNotify)
                self.mytableView.reloadData()
            }
        }
    }
}
