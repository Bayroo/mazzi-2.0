//
//  PageViewController.swift
//  Mazzi
//
//  Created by Janibekm on 10/27/17.
//  Copyright © 2017 Mazzi App LLC. All rights reserved.
//
import UIKit
import KVNProgress
import MobileCoreServices
import Photos

class PageViewController: UIViewController, UIPageViewControllerDataSource, UIScrollViewDelegate,UIGestureRecognizerDelegate{
    
    var pageViewController : UIPageViewController?
    var urls = [String]()
    var sMsgType = ""
    var sFileUrl = ""
    var sFileName = ""
    var currentIndex : Int = 0
    var scrollview = UIScrollView()
    var closebtn = UIButton()
    var isSaved = false
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true //disable back swipe
        pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageViewController!.dataSource = self
       
        if sFileUrl != ""{
            print(urls)
            if urls.contains(sFileUrl){
                let index = urls.index(of: sFileUrl)
                let startingViewController: FileViewController = viewControllerAtIndex(index: index!)!
                currentIndex = index!
                let viewControllers = [startingViewController]
                pageViewController!.setViewControllers(viewControllers , direction: .forward, animated: false, completion: nil)
                pageViewController!.view.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height);
                addChild(pageViewController!)
                self.view.addSubview(pageViewController!.view)
            }else{
                if urls.count > 0 {
                    var index = 0
                    if urls.count < 1{
                        index = urls.count
                    }else{
                        index = urls.count - 1
                    }
                    let startingViewController: FileViewController = viewControllerAtIndex(index: index)!
                    currentIndex = index
                    let viewControllers = [startingViewController]
                    pageViewController!.setViewControllers(viewControllers , direction: .forward, animated: false, completion: nil)
                    pageViewController!.view.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height);
                    addChild(pageViewController!)
                    self.view.addSubview(pageViewController!.view)
                }                
            }
        }

        view.isUserInteractionEnabled =  true
        let theHeight = view.frame.size.height
        let theWidth = view.frame.size.width
        
        scrollview = UIScrollView(frame:CGRect(x:0, y: Int(theHeight-140), width: Int(theWidth), height:140))
        scrollview.delegate = self
        scrollview.backgroundColor = UIColor.white
        view.addSubview(scrollview)
        
        let frames = CGRect(x: Int(theWidth - 50), y: Int(theHeight-155), width: 30, height: 30)
        self.closebtn = UIButton(frame: frames)
        self.closebtn.backgroundColor = UIColor.white
        self.closebtn.clipsToBounds = true
        let img = UIImage(named: "close")
        self.closebtn.layer.cornerRadius = 15
        self.closebtn.setImage(img, for: .normal)
        self.closebtn.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        view.addSubview(closebtn)
        self.closebtn.isHidden = true
        do {
            var allwidth = 0
            for i in 0 ..< urls.count{
                let frame = CGRect(x: allwidth, y: 5, width: 100, height: 130)
                allwidth = allwidth + 100 + 5
                let sortButton = UIButton(frame: frame)
                //let images = UIImage(contentsOfFile: documentsPath+"/magazine/"+inwid+"/"+dirnames[i]+"/thumb.jpg")
                let images = UIImage(named: "01")
                sortButton.setImage(images, for: .normal)
                sortButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
                sortButton.tag = i
                scrollview.contentSize.width = CGFloat(105 * urls.count)
                scrollview.addSubview(sortButton)
            }
        }
        self.scrollview.isHidden = true
    }
    
    @objc func closeAction(sender: UIButton!) {
        self.scrollview.isHidden = true
        self.closebtn.isHidden = true
        //self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func webviewshort(sender: UITapGestureRecognizer){
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    @objc func buttonAction(sender: UIButton!) {
        //let keys = self.dirnames[sender.tag]
        pageViewController?.setViewControllers([viewControllerAtIndex(index: sender.tag)] as! [FileViewController], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        var index = (viewController as! FileViewController).pageIndex
        if (index == 0) || (index == NSNotFound) {
            return nil
        }
        index -= 1
        return viewControllerAtIndex(index: index)
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        var index = (viewController as! FileViewController).pageIndex
        if index == NSNotFound {
            return nil
        }
        index += 1
        if (index == self.urls.count) {
            return nil
        }
        return viewControllerAtIndex(index: index)
    }
    
    func viewControllerAtIndex(index: Int) -> FileViewController?
    {
        if self.urls.count == 0 || index >= self.urls.count
        {
            return nil
        }
        let page = FileViewController()

        print("TYPE:: ", sMsgType)
      
         print("sFileName:: ", sFileName)
        page.sFileUrl = urls[index]
        page.sFileName = sFileName
        page.sMsgType = sMsgType
           print("sFileUrl:: ",  urls[index])
        page.pageIndex = index
        currentIndex = index
        return page
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int
    {
        return self.urls.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int
    {
        return 0
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(backtoroot))
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:"save"), style: .plain, target: self, action: #selector(saveImage))
        if self.sFileUrl.contains("jpg") ||  self.sFileUrl.contains("png") {
             self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Хадгалах", style: .plain, target: self, action: #selector(saveImage))
        }else if self.sFileUrl.contains("MOV") ||  self.sFileUrl.contains("MP4") || self.sFileUrl.contains("mov") ||  self.sFileUrl.contains("mp4") {
             self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Хадгалах", style: .plain, target: self, action: #selector(saveVideo))
        }
    
    }
    @objc func backtoroot(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func saveImage(){
        let fileurl = SettingsViewController.cdioFileUrl
        let imageview = UIImageView()
        imageview.sd_setImage(with: URL(string: "\(fileurl)\(sFileUrl)"), placeholderImage: UIImage(named: "loading"))
        
        guard let imageToSave = imageview.image else {
            return
        }
        UIImageWriteToSavedPhotosAlbum(imageToSave, self, nil, nil)
        //    dismiss(animated: true, completion: nil)
        KVNProgress.showSuccess()
    }
    
  @objc  func saveVideo(){
     KVNProgress.show(0.0, status: "Хадгалж байна...", on: self.view)
     self.updateProgress()
    

         let fileurl = SettingsViewController.cdioFileUrl
        let videoUrl = "\(fileurl)\(sFileUrl)"
        DispatchQueue.global(qos: .background).async {
            if let url = URL(string: videoUrl),
                let urlData = NSData(contentsOf: url)
            {
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
                let filePath="\(documentsPath)/tempFile.mp4"
                DispatchQueue.main.async {
                    urlData.write(toFile: filePath, atomically: true)
                    PHPhotoLibrary.shared().performChanges({
                        PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: filePath))
                    }) { completed, error in
                        if completed {
//                            KVNProgress.dismiss()
                           
                        }
                    }
                }
            }
        }
//          KVNProgress.dismiss()
    }

     func updateProgress()
        {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(Int(1))) {
                KVNProgress.update(0.2, animated: true)
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(Int(2))) {
                KVNProgress.update(0.5, animated: true)
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(Int(3))) {
                KVNProgress.update(0.7, animated: true)
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(Int(4))) {
                KVNProgress.update(0.9, animated: true)
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(Int(5))) {
                KVNProgress.update(1.0, animated: true)
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(Int(5.5))) {
                KVNProgress.showSuccess()
            }
            
    }

    
    
}

