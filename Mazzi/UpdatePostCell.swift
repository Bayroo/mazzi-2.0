//
//  UpdatePostCell.swift
//  Mazzi
//
//  Created by Bayara on 12/13/18.
//  Copyright © 2018 Mazzi Application. All rights reserved.
//

import Foundation

class UpdatePostCell:UITableViewCell, UITextViewDelegate{
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var proImg: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var textview: UITextView!
    @IBOutlet weak var topicBtn: UIButton!
    override func awakeFromNib() {
        textview.delegate = self
    }
}
