//
//  ProfileContact.swift
//  Mazzi
//
//  Created by Janibekm on 10/5/17.
//  Copyright © 2017 Mazzi App LLC. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import SwiftyJSON
import RealmSwift
import Foundation
import KVNProgress
import Contacts

class ProfileContact: UIViewController {

    var numbersToDB = [String]()
    var accessAuthorized = false
    var mynumber = ""
    
    @IBOutlet weak var cover: UIImageView!
    @IBOutlet weak var createmessage: UIButton!
    @IBOutlet weak var procallBtn: UIButton!
    @IBOutlet weak var provideocallBtn: UIButton!
    @IBOutlet weak var provoiceocallBtn: UIButton!
    
    @IBOutlet weak var myswtich: UISwitch!
    @IBOutlet weak var prousername: UILabel!
    @IBOutlet weak var prolastlogin: UILabel!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    
    
    var memberdate = Double()
    var memberlastname = ""
    var membernickname = ""
    var memberphone = ""
    var member_onlineAt = Double()
    var memberImg = ""
    var memberState = 0
    var memberFCM = ""
    
    let userDefaults = UserDefaults.standard
    var contactid = ""
    var selectedgroupuser: User_group?
    var senderchatid = ""
    var username = ""
    var userid = ""
    var contact : Results<Contacts_realm>!
    var conversations: Results<Conversation_realm>!
    override func viewDidLoad() {
        super.viewDidLoad()
        mynumber = (userDefaults.object(forKey: "myNumber") as? String)!
        self.navigationController?.navigationBar.isHidden = false
        self.prolastlogin.frame = CGRect(x:self.view.bounds.size.width/2-125 , y:25 , width:250 , height:20)
        prolastlogin.textAlignment = NSTextAlignment.center
        self.navigationController?.navigationBar.addSubview(self.prolastlogin)
        self.prousername.frame = CGRect(x:self.view.bounds.size.width/2-125 , y:5 , width:250 , height:20)
        prousername.textAlignment = NSTextAlignment.center
        self.navigationController?.navigationBar.addSubview(self.prousername)
       // provoiceocallBtn.isHidden = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(baktoroot))
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        navigationController?.navigationBar.barTintColor = UIColor.white
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        let dateformatter = DateFormatter()
        let fileurl = SettingsViewController.fileurl
        
        if GlobalVariables.isFromContacts == true {
            print("SELECTED USERID: " , self.contactid)
            if realm.object(ofType: Contacts_realm.self, forPrimaryKey: contactid) != nil {
                contact = realm.objects(Contacts_realm.self).filter("id = '\(contactid)'")
               self.showInfo()
            }else{
                self.get_contacts()
                self.get_installed()
            }
           
        }else if  GlobalVariables.isFromClassMembers == true {
            print("username:: ", self.membernickname)
            self.prousername.text = self.membernickname
            self.label3.text = self.memberphone
            
            let date = NSDate(timeIntervalSince1970: (self.memberdate)/1000)
            dateformatter.dateFormat = "MM/dd/yyyy"
            let now = dateformatter.string(from: date as Date)
            self.label2.text = now
            
            let lastlogin = NSDate(timeIntervalSince1970: (self.member_onlineAt)/1000)
            dateformatter.dateStyle = .short
            dateformatter.timeStyle = .short
            let last = dateformatter.string(from: lastlogin as Date)
            self.prolastlogin.text = "Сүүлд нэвтэрсэн огноо \(last)"
            
            if let img = URL(string: "\(fileurl)\((self.memberImg))"){
                self.cover.sd_setImage(with: img,placeholderImage: UIImage(named: "avatarr"))
            }else{
                self.cover.image = UIImage(named: "avatarr")
            }
            
            
        } else{
            if realm.object(ofType: Contacts_realm.self, forPrimaryKey: contactid) != nil {
                contact = realm.objects(Contacts_realm.self).filter("id = '\(contactid)'")
            }
            let name = selectedgroupuser!.nickname
            if let img = URL(string: "\(fileurl)\((selectedgroupuser!.image))"){
                self.cover.sd_setImage(with: img,placeholderImage: UIImage(named: "avatarr"))
            }else{
                self.cover.image = UIImage(named: "avatarr")
            }
            _ = selectedgroupuser!.lastname
            self.prousername.text = name //"\(name) \(lastname)"
            let lastlogin = NSDate(timeIntervalSince1970: (selectedgroupuser!.online_at)/1000)
            dateformatter.dateStyle = .short
            dateformatter.timeStyle = .short
            let last = dateformatter.string(from: lastlogin as Date)
            self.prolastlogin.text = "Сүүлд нэвтэрсэн огноо \(last)"
            let date = NSDate(timeIntervalSince1970: (selectedgroupuser!.birthday)/1000)
            dateformatter.dateFormat = "MM/dd/yyyy"
            let now = dateformatter.string(from: date as Date)
            self.label2.text = now
            
            let phone = selectedgroupuser!.phone
            self.label3.text = phone
        }
       
        self.createmessage.layer.cornerRadius = 15
        self.procallBtn.layer.cornerRadius = 15
        username = SettingsViewController.username
//        if let navigationBar = self.navigationController?.navigationBar {
//            let gradient = CAGradientLayer()
//            var bounds = navigationBar.bounds
//            bounds.size.height += UIApplication.shared.statusBarFrame.size.height
//            gradient.frame = bounds
//            gradient.colors = [GlobalVariables.black.cgColor, GlobalVariables.purple.cgColor]
//            gradient.startPoint = CGPoint(x: 0, y: 0)
//            gradient.endPoint = CGPoint(x: 1, y: 0)
//            if let image = getImageFrom(gradientLayer: gradient) {
//                navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
//            }
//        }
        self.myswtich.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
        self.myswtich.isOn = (self.userDefaults.value(forKey: contactid) != nil)
    }
    
    func showInfo(){
        KVNProgress.show(withStatus: "", on: self.view)
         let dateformatter = DateFormatter()
         let fileurl = SettingsViewController.fileurl
        if  contact[0].name != ""{
            self.prousername.text = contact[0].name
        }else{
            self.prousername.text = self.username
        }
        if let img = URL(string: "\(fileurl)\((contact[0].image))"){
            self.cover.sd_setImage(with: img,placeholderImage: UIImage(named: "avatar"))
        }else{
            self.cover.image = UIImage(named: "avatar")
        }
        _ = contact[0].lastname
        //"\(name) \(lastname)"
        let lastlogin = NSDate(timeIntervalSince1970: (contact[0].online_at)/1000)
        dateformatter.dateStyle = .short
        dateformatter.timeStyle = .short
        let last = dateformatter.string(from: lastlogin as Date)
        self.prolastlogin.text = "Сүүлд нэвтэрсэн огноо \(last)"
        let date = NSDate(timeIntervalSince1970: (contact[0].birthday)/1000)
        dateformatter.dateFormat = "MM/dd/yyyy"
        let now = dateformatter.string(from: date as Date)
        self.label2.text = now
        
        let phone = contact[0].phone
        self.label3.text = phone
        KVNProgress.dismiss()
    }
    @objc func switchChanged(mySwitch: UISwitch) {
//        let value = mySwitch.isOn
//        self.userDefaults.set(value, forKey: contactid)
//        userDefaults.synchronize()
    }
    
    func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
        var gradientImage:UIImage?
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            gradientLayer.render(in: context)
            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        UIGraphicsEndImageContext()
        return gradientImage
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        print("PROFILE",  GlobalVariables.isFromChat)
        self.prousername.isHidden = false
        self.prolastlogin.isHidden = false
        self.navigationController?.navigationBar.barTintColor = GlobalVariables.purple
        
        if GlobalVariables.isFromChat == true {
            self.dismiss(animated: false, completion: nil)
             GlobalVariables.isFromChat = false
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
//     GlobalVariables.isFromClassMembers = false
//        GlobalVariables.isFromContacts = false
     self.prousername.isHidden = true
     self.prolastlogin.isHidden = true
    }
    
    @objc func prodropmenu() {
        let viewController = storyboard?.instantiateViewController(withIdentifier: "consettings") as! ConSettingsVC
        if GlobalVariables.isFromContacts {
            viewController.userid = contact[0].id
            viewController.username = contact[0].name
        }else if GlobalVariables.isFromClassMembers {
            viewController.userid = self.contactid
            viewController.username = self.membernickname
        } else{
            viewController.userid = selectedgroupuser!.id
            viewController.username = selectedgroupuser!.nickname
        }
       
        let navController = UINavigationController(rootViewController: viewController)
        self.present(navController, animated:true, completion: nil)
    }
    @objc func baktoroot(){
        ChatListViewController.refreshview = true
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func createmessage(_ sender: Any) {
        let userid = SettingsViewController.userId
        let baseurl = SettingsViewController.baseurl
        
        var selecteduserid = ""
        if GlobalVariables.isFromContacts == true {
            selecteduserid = contact[0].id
            print("111111111")
        }else if GlobalVariables.isFromClassMembers == true{
            selecteduserid = self.contactid
             print("2222222222222", selecteduserid)
        }else{
             print("3333333333333333333")
            selecteduserid = selectedgroupuser!.id
        }
       // let headers = SettingsViewController.headers
        let token = UserDefaults.standard.object(forKey:"token") as! String
        let  header = ["x-access-token": token, "authorization": "BPXjEvWfXxx5qpQHHFMEJEB8fA3xAczC"]
        print("token:",token)
        checkInternet()
        if SettingsViewController.online == true {
          KVNProgress.show()
            Alamofire.request("\(baseurl)create_private", method: .post, parameters: ["user_id":userid, "participant_id":selecteduserid], encoding: JSONEncoding.default,headers:header).responseJSON{ response in
              //  print("myid:",userid)
             //   print("selecteduserid",selecteduserid)
                KVNProgress.dismiss()
                if response.response?.statusCode == 200 {
                    let getCon = JSON(response.value!)
               //     print("json:",getCon)
                    let error = getCon["error"].bool!
                    if(error == false){
                        let chatid = getCon["_id"].string!
                        self.senderchatid = chatid
                        Conversation.get_singleCon(con_id : chatid, completion: { (con) in

                        })
                        ChatListViewController.nowMessagingConversationID = chatid
                         self.performSegue(withIdentifier: "SingleChatVC", sender: self)
                    }
                }
            }
        }else{
            let alert = UIAlertController(title: "Алдаа !", message: "Интернетийн холболтоо шалгана уу ?", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Close", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    @IBAction func procallBtn(_ sender: Any) {
         var phoneNumber = ""
        if GlobalVariables.isFromContacts {
            phoneNumber = contact[0].phone
        }else if (GlobalVariables.isFromClassMembers == true){
            phoneNumber = self.memberphone
        }else{
            phoneNumber = selectedgroupuser!.phone
        }
       
        if let phoneCallURL:NSURL = NSURL(string: "tel://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL as URL)) {
               application.open(phoneCallURL as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
        }
    }
    
//        let userid = SettingsViewController.userId
//        let baseurl = SettingsViewController.baseurl
//        let selecteduserid = contact[0].id
//        let headers = SettingsViewController.headers
//        checkInternet()
//        if SettingsViewController.online == true {
//            Alamofire.request("\(baseurl)create_private", method: .post, parameters: ["user_id":userid, "participant_id":selecteduserid], encoding: JSONEncoding.default,headers:headers).responseJSON{ response in
//                if response.response?.statusCode == 200 {
//                    let getCon = JSON(response.value!)
//                    let error = getCon["error"].boolValue
//                    if(error == false){
//                        let chatid = getCon["_id"].string!
//                        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "videoCall") as! ViewController
//                        viewController.chatid = chatid
//                        viewController.touserid = self.contact[0].id
//                        viewController.imCalling = true
//                        viewController.myname = self.username
//                        viewController.isVideoCall = false
//                        Conversation.get_singleCon(con_id: chatid, completion: { (con) in
//                        })
//                        ChatListViewController.nowMessagingConversationID = chatid
//                        self.present(viewController, animated:true, completion: nil)
//
//                    }
//                }
//            }
//        }else{
//            let alert = UIAlertController(title: "Алдаа !", message: "Интернетийн холболтоо шалгана уу ?", preferredStyle: UIAlertControllerStyle.alert)
//            alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
//        }
    
//    @IBAction func provideocallBtn(_ sender: Any){
//        let userid = SettingsViewController.userId
//        let baseurl = SettingsViewController.baseurl
//        let selecteduserid = contact[0].id
//        let headers = SettingsViewController.headers
//        checkInternet()
//        if SettingsViewController.online == true {
//            Alamofire.request("\(baseurl)create_private", method: .post, parameters: ["user_id":userid, "participant_id":selecteduserid], encoding: JSONEncoding.default,headers:headers).responseJSON{ response in
//                if response.response?.statusCode == 200 {
//                    let getCon = JSON(response.value!)
//                    let error = getCon["error"].boolValue
//                    if(error == false){
//                        let chatid = getCon["_id"].string!
//                        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "videoCall") as! ViewController
//                        viewController.chatid = chatid
//                        viewController.touserid = self.contact[0].id
//                        viewController.imCalling = true
//                        viewController.myname = self.username
//                        viewController.isVideoCall = true
//                        Conversation.get_singleCon(con_id: chatid, completion: { (con) in
//                        })
//                        ChatListViewController.nowMessagingConversationID = chatid
//                        self.present(viewController, animated:true, completion: nil)
//                    }
//                }
//            }
//        }else{
//            let alert = UIAlertController(title: "Алдаа !", message: "Интернетийн холболтоо шалгана уу ?", preferredStyle: UIAlertControllerStyle.alert)
//            alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
//        }
//
//    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SingleChatVC" {
            ChatListViewController.refreshview = true
            let singlechat = segue.destination as? SingleChatVC
            
            if GlobalVariables.isFromContacts{
                let user = User_group.init(id: contact[0].id, image: contact[0].image, nickname: contact[0].name, lastname: contact[0].lastname, surname: contact[0].surname, phone: contact[0].phone, state: contact[0].state, online_at: contact[0].online_at, fcm_token: contact[0].fcm_token,registerId: contact[0].register)
                singlechat?.selectedgroupUser = [user]
                singlechat?.senderchatid = self.senderchatid
                singlechat?.navtitle = [contact[0].name]
            }else if GlobalVariables.isFromClassMembers == true{
                let user = User_group.init(id: self.contactid, image: self.memberImg, nickname: self.membernickname, lastname: self.memberlastname, surname: "", phone: self.memberphone, state: self.memberState, online_at: self.member_onlineAt, fcm_token: self.memberFCM ,registerId: "")
                print("USER:::", user)
                singlechat?.selectedgroupUser = [user]
//                print("SELECTED USER:: ",  user)
                singlechat?.senderchatid = self.senderchatid
                singlechat?.navtitle = [self.membernickname]
            }else{
                
                let user = User_group.init(id: selectedgroupuser!.id, image: selectedgroupuser!.image, nickname: selectedgroupuser!.nickname, lastname: selectedgroupuser!.lastname, surname: selectedgroupuser!.surname, phone: selectedgroupuser!.phone, state: selectedgroupuser!.state, online_at: selectedgroupuser!.online_at, fcm_token: selectedgroupuser!.fcm_token,registerId: selectedgroupuser!.registerId)
                singlechat?.selectedgroupUser = [user]
                singlechat?.senderchatid = self.senderchatid
                singlechat?.navtitle = [selectedgroupuser!.nickname]
            }
        }
    }
    
    func get_contacts(){
        KVNProgress.show(withStatus: "", on: self.view)
        let store = CNContactStore()
        let fetchRequest = CNContactFetchRequest(keysToFetch: [CNContactFamilyNameKey as CNKeyDescriptor, CNContactGivenNameKey as CNKeyDescriptor, CNContactPhoneNumbersKey as CNKeyDescriptor, CNContactIdentifierKey as CNKeyDescriptor])
        do{
            try store.enumerateContacts(with: fetchRequest) { contact, stop in
                //   print("contact:" , contact)
                if contact.phoneNumbers.count > 0 {
                    self.accessAuthorized = true
                    let key = contact.identifier
                    var name = ""
                    if contact.givenName.isEmpty {
                        name = contact.familyName
                    }
                    name = contact.givenName
                    let lastname = contact.familyName
                    var phones = [String]()
                    for number: CNLabeledValue in contact.phoneNumbers {
                        
                        let numbervalue = number.value
                        var digits = (numbervalue.value(forKey: "digits") as? String)!
                        if digits.hasPrefix("+"){
                            digits.remove(at: digits.startIndex)
                            if digits.count == 8 {
                                phones.append("976\(digits)")
                                self.numbersToDB.append("976\(digits)")
                            }else{
                                phones.append(digits)
                                self.numbersToDB.append(digits)
                            }
                        }else{
                            if digits.count == 8 {
                                phones.append("976\(digits)")
                                self.numbersToDB.append("976\(digits)")
                            }else{
                                phones.append(digits)
                                self.numbersToDB.append(digits)
                            }
                        }
                    }
                    DispatchQueue.main.async {
                        
                        if phones.contains(self.mynumber){
                            //  print(phones)
                        }else{
                            let contactToDB = Contacts.init(id: key, name: name, lastname: lastname, surname: "", image: "", phones: phones, phone: "", gender: "", birthday: 0, state: 0, online_at: 0, created_at: 0, fcm_token: "")
                            
                            RealmService.writeContactToRealm(installedContacts: contactToDB, completion: { (complete) in
                                if complete == true{
                                 
//                                    self.tableView.reloadData()
                                }
                            })
                        }
                    }
                }
            }
        }catch{
            self.getAccessToContacts()
        }
        
        KVNProgress.dismiss()
    }
    func get_installed(){
        let baseurl = SettingsViewController.baseurl
        //  let headers = SettingsViewController.headers
        let phone = (userDefaults.object(forKey: "myNumber") as? String)!
        if self.numbersToDB.contains(phone){
            let index = self.numbersToDB.index(of: phone)
            self.numbersToDB.remove(at: index!)
        }
        
        let token = UserDefaults.standard.object(forKey:"token") as! String
        let  header = ["x-access-token": token, "authorization": "BPXjEvWfXxx5qpQHHFMEJEB8fA3xAczC"]
        KVNProgress.show(withStatus: "", on: self.view)
        Alamofire.request("\(baseurl)user_info", method: .post, parameters: ["phone_numbers":self.numbersToDB], encoding: JSONEncoding.default,headers:header).responseJSON{ response in
            if response.response?.statusCode == 200 {
                let jsonData = JSON(response.value!)
                let error = jsonData["error"].boolValue
                DispatchQueue.main.async {
                    let loops = jsonData["result"]
                    if error == false {
                        for i in 0..<loops.count{
                            let dispid = jsonData["result"][i]["_id"].string ?? ""
                            let dispphone = jsonData["result"][i]["phone"].string ?? ""
                            let dispgender = jsonData["result"][i]["gender"].string ?? ""
                            let disppic = jsonData["result"][i]["image"].string ?? ""
                            let dispnickname = jsonData["result"][i]["nickname"].string ?? ""
                            let displastname = jsonData["result"][i]["lastname"].string ?? ""
                            let dispsurname = jsonData["result"][i]["surname"].string ?? ""
                            let dispbirthday = jsonData["result"][i]["birthday"].double ?? 0
                            let dispstate = jsonData["result"][i]["state"].int ?? 0
                            let online_at = jsonData["result"][i]["online_at"].double ?? 0
                            let fcm_token = jsonData["result"][i]["fcm_token"].string ?? ""
//                            if self.newMember == true {
//                                self.newMemberName = dispsurname
//                                self.notification()
//                                self.newMember = false
//                            }
                            DispatchQueue.main.async {
                                let installedApp = Contacts.init(id: dispid, name: dispnickname, lastname: displastname, surname: dispsurname, image: disppic, phones: [""], phone: dispphone, gender: dispgender,birthday:dispbirthday, state: dispstate, online_at: online_at, created_at: online_at, fcm_token: fcm_token)
                                RealmService.writeContactToRealm(installedContacts: installedApp, completion: { (writeComplete) in
                                    if writeComplete == true {
                                        if realm.object(ofType: Contacts_realm.self, forPrimaryKey: self.contactid) != nil {
                                            self.contact = realm.objects(Contacts_realm.self).filter("id = '\(self.contactid)'")
                                            self.showInfo()
                                        }
                                    }
                                })
                            }
                        }
                    }else{
                        print("ContactViewController - print error")
                    }
                }
            }
        }
        KVNProgress.dismiss()
    }
    
    func getAccessToContacts(){
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        switch authorizationStatus {
        case .authorized:
            accessAuthorized = true
        case .denied, .notDetermined:
            accessAuthorized = false
            let alert = UIAlertController(title: "", message: "Тохиргоо руу орж хадгалсан дугаарын жагсаалт авах тохиргоог идэвхжүүлнэ үү", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
            
            alert.addAction(UIAlertAction(title: "Тохиргоо", style: UIAlertAction.Style.default, handler:  { (alert:UIAlertAction) -> Void in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }))
        default:
            let alert = UIAlertController(title: "", message: "Тохиргоо руу орж хадгалсан дугаарын жагсаалт авах тохиргоог идэвхжүүлнэ үү", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
            
            alert.addAction(UIAlertAction(title: "Тохиргоо", style: UIAlertAction.Style.default, handler:  { (alert:UIAlertAction) -> Void in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }))
            accessAuthorized = false
        }
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
