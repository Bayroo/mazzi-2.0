//
//  SearchVC.swift
//  Mazzi
//
//  Created by Janibekm on 11/20/17.
//  Copyright © 2017 Mazzi App LLC. All rights reserved.
//

import UIKit
import Contacts
import Alamofire
import SwiftyJSON
import SocketIO
import RealmSwift

class SearchUserVC: UIViewController,UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var groupimage: UIImageView!
    @IBOutlet weak var grouptitle: UITextField!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchhead: UIView!
    @IBOutlet weak var groupimageSelect: UIButton!
    @IBOutlet weak var createGroupChat: UIButton!
    var numbersToDB = [String]()
    var isCreateChat = true
    var groupChatusers = [String]()
    var currentChatusers = [String]()
    var pic = ""
    let searchBar = UISearchBar()
    var senderchatid = ""
    static var selectedconvid = ""
    var navtitle = ""
    var chatuser = [User_group]()
    var currentUser : [User_group]?
    var installedContacts : Results<Contacts_realm>!
    var filteredContacts : Results<Contacts_realm>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = GlobalVariables.todpurple
//        self.navigationItem.setHidesBackButton(true, animated: false)
//        self.navigationController?.navigationBar.barTintColor = GlobalVariables.todpurple
//        self.navigationController?.navigationBar.tintColor = GlobalVariables.todpurple
        let icon = UIImage.init(named: "05")?.withRenderingMode(.alwaysOriginal)
        let backButton = UIBarButtonItem.init(image: icon!, style: .plain, target: self, action: #selector(baktoroot))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.titleView = searchBar
        
        self.tableview.delegate = self
        self.tableview.dataSource = self
        self.searchBar.delegate = self
        if realm.objects(Contacts_realm.self).count != 0{
            installedContacts = realm.objects(Contacts_realm.self).filter("created_at != 0")
            filteredContacts = installedContacts
        }

       for i in 0..<currentUser!.count{
            self.currentChatusers.append(self.currentUser![i].id)
        }
   
        self.searchTextField.layer.masksToBounds = true
        self.searchTextField.delegate = self
        self.searchTextField.layer.borderWidth = 1.0
        self.searchTextField.layer.borderColor = GlobalVariables.whiteGray.cgColor
        self.searchTextField.layer.cornerRadius = 8.0
        self.searchTextField.tintColor = GlobalVariables.todpurple
        self.searchTextField.attributedPlaceholder = NSAttributedString(string: "Хайх",attributes: [NSAttributedString.Key.foregroundColor: GlobalVariables.whiteGray.withAlphaComponent(0.4)])
        self.searchTextField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        
        if self.searchTextField.text != "" {
            let searchtxt = self.searchTextField.text?.lowercased()
            if (searchtxt?.isAlphanumeric1)!{
                self.filteredContacts = self.installedContacts.filter("name CONTAINS[c]'\(searchtxt!)'")
                self.tableview.reloadData()
            }else{
                self.filteredContacts = self.installedContacts.filter("phone CONTAINS '\(searchtxt!)'")
                self.tableview.reloadData()
            }
        }else{
            self.filteredContacts = self.installedContacts
        }
        self.tableview.reloadData()
        
    }
    
    @objc func baktoroot(){
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.searchTextField.resignFirstResponder()
//         let userid = SettingsViewController.userId
        if isCreateChat == true {
            if groupChatusers.contains(self.filteredContacts[indexPath.row].id){
                let index = self.groupChatusers.index(where: {$0 == self.filteredContacts[indexPath.row].id})
                self.groupChatusers.remove(at: index!)
        
                let indexs = self.chatuser.index(where: {$0.id == self.filteredContacts[indexPath.row].id})
                self.chatuser.remove(at: indexs!)
                self.tableview.reloadData()
            }else{
                self.groupChatusers.append(self.filteredContacts[indexPath.row].id)
                let users = User_group.init(id: self.filteredContacts[indexPath.row].id, image: self.filteredContacts[indexPath.row].image, nickname: self.filteredContacts[indexPath.row].name, lastname: self.filteredContacts[indexPath.row].lastname, surname: self.filteredContacts[indexPath.row].surname, phone: self.filteredContacts[indexPath.row].phone, state: self.filteredContacts[indexPath.row].state, online_at: self.filteredContacts[indexPath.row].online_at, fcm_token: self.filteredContacts[indexPath.row].fcm_token, registerId: self.filteredContacts[indexPath.row].register)
               
                self.chatuser.append(users)
                self.tableview.reloadData()
            }
        }else{
            let viewController = storyboard?.instantiateViewController(withIdentifier: "ProfileContact") as! ProfileContact
            viewController.contactid = self.filteredContacts[indexPath.row].id
            let navController = UINavigationController(rootViewController: viewController)
            self.present(navController, animated:true, completion: nil)
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if realm.objects(Contacts_realm.self).count != 0{
            return self.filteredContacts.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath) as! SearchCell
        cell.clearCellData()
        
        if indexPath.row % 2 == 0 {
            cell.contentView.backgroundColor = GlobalVariables.F5F5F5
        }else{
            cell.contentView.backgroundColor = UIColor.white
        }
       
        if isCreateChat == true {
//            print(self.groupChatusers.count)
            if self.currentChatusers.contains(filteredContacts[indexPath.row].id) {
                cell.accessoryType = .checkmark
                cell.isUserInteractionEnabled = false
                cell.tintColor = GlobalVariables.purple
                cell.subview.layer.borderColor = GlobalVariables.purple.cgColor
            }else{
                if groupChatusers.contains(self.filteredContacts[indexPath.row].id){
                    cell.accessoryType = .checkmark
                    cell.tintColor = GlobalVariables.purple
                    cell.subview.layer.borderColor = GlobalVariables.purple.cgColor
                    if groupChatusers.count > 0 || groupChatusers.count == 1{
                        self.createGroupChat.setImage(UIImage(named: "001-bear-pawprint-saaral"), for: .normal)
                        self.createGroupChat.isEnabled = true
                    }else {
                        self.createGroupChat.setImage(UIImage(named: "001-bear-pawprintt"), for: .normal)
                        self.createGroupChat.isEnabled = false
                    }
                }else{
                    if(groupChatusers.count < 1){
                        self.createGroupChat.setImage(UIImage(named: "001-bear-pawprintt"), for: .normal)
                        self.createGroupChat.isEnabled = false
                    }
                    cell.accessoryType = .none
                  }
            }
        }else{
            cell.backgroundColor = UIColor.clear
        }
        let fileurl = SettingsViewController.fileurl
        let mypic = self.filteredContacts[indexPath.row].image
        cell.searchProname.text = self.filteredContacts[indexPath.row].name
        if mypic != ""{
            cell.searchPropic.sd_setImage(with: URL(string:"\(fileurl)\(mypic)"), completed: nil)
        }else{
            cell.searchPropic.image = UIImage(named: "circle-avatar")
        }
        cell.searchProdetail.text = self.filteredContacts[indexPath.row].phone
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
 
    func photoGet(capturedImage: UIImage) {
        let userid = userDefaults.object(forKey: "user_id") as? String
        let fileurl = SettingsViewController.fileurl
        Message.uploadimage(chatId: userid! ,image: capturedImage, completion: { (state, imageurl) in
            DispatchQueue.main.async {
                let url = JSON(imageurl)
                self.pic = url["url"].string!
                print(self.pic)
                if self.pic == ""{
                    let imageurl = URL(string:"\(fileurl)\(self.pic)")!
                    self.groupimage.sd_setImage(with: imageurl, completed: { (image, error, cacheType, url) in
                        self.groupimage.sd_setImage(with: imageurl, completed: nil)
                    })
                }else{
                    
                }
            }
        })
    }
    //end aldaa bje bayaraa ???
    //todojani
    @IBAction func createGroupChat(_ sender: Any) {
        print("JOIN")
        checkInternet()
        socket.emit("join_conversation",  ["conversation_id":SearchUserVC.selectedconvid, "user_ids": self.groupChatusers, "content": "", "owner_id":SettingsViewController.userId])
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func groupimageSelect(_ sender: Any) {
        self.performSegue(withIdentifier: "groupimage", sender: self)
    }
    @IBOutlet weak var createGroupConversation: UIButton!
    
    @IBAction func createGroupConversation(_ sender: Any) {
        print("CREATE")
        let userid = SettingsViewController.userId
        let baseurl = SettingsViewController.baseurl
        if !self.groupChatusers.contains(userid){
            self.groupChatusers.append(userid)
        }
        let spiner = UIActivityIndicatorView()
        let frame = CGRect(x: 0,y:0,width:self.view.frame.width,height:self.view.frame.height)
        spiner.frame = frame
        spiner.startAnimating()
        self.view.layer.opacity = 0.5
        self.view.addSubview(spiner)
        let headers = SettingsViewController.headers
        checkInternet()
        if SettingsViewController.online == true {
            Alamofire.request("\(baseurl)create_group", method: .post, parameters: ["participants":self.groupChatusers,"image":self.pic, "title": grouptitle.text!,"owner_id":GlobalVariables.user_id], encoding: JSONEncoding.default,headers:headers).responseJSON{ response in
                
                print("response::",response)
                if response.response?.statusCode == 200 {
                    let res = JSON(response.value!)
                    print("res:",res)
                    if res["error"].bool == false {
                        let id = res["_id"].string!
                        let title = res["title"].string!
                        let created_at = res["created_at"].double!
                        let is_thread = res["is_thread"].bool!
                        let isSecret = res["secret"].bool!
                        let image = res["image"].string!
                        let owner_id_con = res["owner_id"].string!
                        let show = res["show"].arrayValue.map({$0.stringValue})
                        let conversation = Conversation.init(user: self.chatuser, lastmessage: Message.init(), id: id, title: title, created_at: created_at, is_thread: is_thread, show: show, secret: isSecret, image: image,owner_id_con: owner_id_con)
                        RealmService.createConversationToRealm(createConversation: conversation, completion: { (success) in
                            if success == true{
                                spiner.stopAnimating()
                                spiner.isHidden = true
                                self.view.layer.opacity = 1
                                self.senderchatid = id
                                self.navtitle = title
                                socket.emit("create_group", ["participants":self.groupChatusers, "conversation_id":id])
                                self.performSegue(withIdentifier: "groupchatcreated", sender: self)
                            }else{
                                spiner.stopAnimating()
                                spiner.isHidden = true
                                self.view.layer.opacity = 1
                            }
                        })
                    }else{
                        spiner.stopAnimating()
                        spiner.isHidden = true
                        self.view.layer.opacity = 1
                    }
                }
            }
        }else{
            spiner.stopAnimating()
            spiner.isHidden = true
            self.view.layer.opacity = 1
            let alert = UIAlertController(title: "Алдаа !", message: "Интернетийн холболтоо шалгана уу !", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    @IBAction func cancelInvite(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "groupimage" {
            let camera = segue.destination as? CameraView
            camera?.disable = true
           // camera?.myProtocol = self
        }else if segue.identifier == "groupchatcreated" {
            let chatview = segue.destination as? SingleChatVC
            chatview?.selectedgroupUser = self.chatuser
            let myid = SettingsViewController.userId
            var showids = [String]()
            for show in self.chatuser{
                if show.id != myid{
                    showids.append(show.id)
                }
            }
            chatview?.selectedgroupUser = self.chatuser
            chatview?.senderchatid = self.senderchatid
            chatview?.navtitle = [self.navtitle]
            SingleChatVC.shows = showids
            baktoroot()
            self.tabBarController?.selectedIndex = 0
        }
    }
}
extension String {
    var isAlphanumeric1: Bool {
        return !isEmpty && range(of: "[^a-zA-Z]", options: .regularExpression) == nil
    }
}
