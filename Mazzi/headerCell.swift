//
//  headerCell.swift
//  Mazzi
//
//  Created by Bayara on 4/3/19.
//  Copyright © 2019 Mazz Application. All rights reserved.
//

import Foundation
import UIKit

class headerCell:UITableViewCell{
    
    @IBOutlet weak var filterByNameBtn: UIButton!
    @IBOutlet weak var filterByTopicBtn: UIButton!
    @IBOutlet weak var LateMinLabel: UILabel!
    
    func initcell(){
        self.filterByNameBtn.layer.cornerRadius = 5
        self.filterByNameBtn.layer.borderColor = GlobalVariables.black.cgColor
        self.filterByNameBtn.layer.borderWidth = 1
        self.filterByNameBtn.setTitleColor(GlobalVariables.black, for: .normal)
        self.filterByNameBtn.layer.masksToBounds = true
        
        self.filterByTopicBtn.layer.cornerRadius = 5
        self.filterByTopicBtn.layer.borderColor = GlobalVariables.black.cgColor
        self.filterByTopicBtn.layer.borderWidth = 1
        self.filterByTopicBtn.setTitleColor(GlobalVariables.black, for: .normal)
        self.filterByTopicBtn.layer.masksToBounds = true
    }
    
 
}
