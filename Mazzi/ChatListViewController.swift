//
//  ChatListViewController.swift
//  Mazzi App LLCChatTestV
//
//  Created by Janibekm on 8/23/17.
//  Copyright © 2017 Janibekm. All rights reserved.
//

import UIKit
import SocketIO
import SwiftyJSON
import AudioToolbox
import SDWebImage
import Alamofire
import UserNotifications
import UserNotificationsUI
import RealmSwift
import KVNProgress
import Contacts
import Realm

class ChatListViewController: UITableViewController, UISearchBarDelegate  {
    @IBOutlet var listTableView: UITableView!
    @IBOutlet var mySearchBar: UISearchBar!
    @IBOutlet weak var backBtn: UIBarButtonItem!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    var accessAuthorized = false
    var numbersToDB = [String]()
    let searchController = UISearchController(searchResultsController: nil)
    let userDefaults = UserDefaults.standard
    var senderchat = ""
    var is_thread = false
    var backtitle = [""]
    var lastmessageid = ""
    var limit = 40
    var skip = 0
    var notificationTitle = ""
    var notificationMessage = ""
    var notificationImage = ""
    var prevcall = 0
    var myid = SettingsViewController.userId
    var lastmessageSeened = false
    var groupchatowner = ""
    var selectedgroupUser:[User_group]?
    var items : Results<Conversation_realm>!
    var origitems: Results<Conversation_realm>!
    var filtereditems : Results<Conversation_realm>!
    var isallChat = true
    static var refreshview = false
    var verificationID = ""
    var customSC = UISegmentedControl()
    var searchActive = false
    var realmitems : Results<Message_realm>!
    let createChatBtn = UIButton()
    let tapGesture = UITapGestureRecognizer()
    var users = User_group?.self
    var shooow = [String]()
    var groupChatImgName = ""
    public static var nowMessagingConversationID = ""
    public static var requestIdentifier = "SampleRequest"
    var chatlistviewtrue = false
    let refresher = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if SettingsViewController.online {
            if socket.status == .notConnected || socket.status == .disconnected{
                socketInitialize()
            }
        }else{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу")
        }

        let attributesNormal = [
            NSAttributedString.Key.foregroundColor : GlobalVariables.lightGray,
            NSAttributedString.Key.font : GlobalVariables.customFont12
        ]
        
        let attributesSelected  = [
            NSAttributedString.Key.foregroundColor : GlobalVariables.todpurple,
            NSAttributedString.Key.font : GlobalVariables.customFont12
        ]
        UITabBarItem.appearance().setTitleTextAttributes(attributesNormal as [NSAttributedString.Key : Any], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes(attributesSelected as [NSAttributedString.Key : Any], for: .selected)
        
        self.mySearchBar.delegate = self
        self.mySearchBar.tintColor = GlobalVariables.todpurple
        self.mySearchBar.layer.borderWidth = 0
        self.mySearchBar.layer.masksToBounds = true
        self.mySearchBar.backgroundColor = GlobalVariables.todpurple
        self.mySearchBar.barTintColor = GlobalVariables.F5F5F5
        self.listTableView.delegate = self
        self.listTableView.dataSource = self
        self.listTableView.refreshControl = self.refresher
        self.refresher.addTarget(self, action: #selector(reloadChatList), for: .valueChanged)
        
        myid = SettingsViewController.userId
        self.conversationSyncToRealDB()

        if(userDefaults.object(forKey: "phone") != nil){
            SettingsViewController.userId = userDefaults.object(forKey: "user_id") as! String
            listTableView.delegate = self
            listTableView.dataSource = self
            verificationID = userDefaults.string(forKey: "authVerificationID")!
        }
        let items = ["Бүх чат", "Групп чат "]
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        UISegmentedControl.appearance().setTitleTextAttributes(titleTextAttributes, for: .normal)
        customSC = UISegmentedControl(items: items)
        self.customSC.selectedSegmentIndex = 0
        customSC.frame = CGRect(x: (self.view.bounds.size.width/2) - 70, y:7.5, width: 140, height: 30)
        customSC.layer.borderColor = GlobalVariables.purple.cgColor
        customSC.layer.borderWidth = 1
        customSC.backgroundColor = UIColor.white
        customSC.tintColor = GlobalVariables.purple
        customSC.layer.cornerRadius = 12
        customSC.layer.masksToBounds = true
        let font = UIFont (name: "segoeui", size: 12)
        customSC.setTitleTextAttributes([NSAttributedString.Key.font: font as Any],
                                        for: .normal)
        
        // Add target action method
        customSC.addTarget(self, action: #selector(changeSegment), for: .valueChanged)
        // Add this custom Segmented Control to our view
        self.navigationController?.navigationBar.addSubview(customSC)
        self.navigationController?.navigationBar.barTintColor = GlobalVariables.purple
        createChatBtn.setTitle("   Групп чат үүсгэх", for: UIControl.State.normal)
        createChatBtn.frame = CGRect(x:15, y:10, width:200, height:50)
        createChatBtn.setImage(UIImage(named: "nemehBTN"), for: UIControl.State.normal)
        createChatBtn.backgroundColor = GlobalVariables.todpurple
        createChatBtn.addTarget(self, action: #selector(buttonTapped(sender:)), for: .touchUpInside)
        
        if self.items != nil {
            self.createChatBtn.isHidden = true
            self.mySearchBar.isHidden = false
        }else{
            self.createChatBtn.isHidden = false
            self.mySearchBar.isHidden = true
            createChatBtn.setTitle("   Чат үүсгэх", for: UIControl.State.normal)
            self.listTableView.tableHeaderView = createChatBtn
        }

        self.get_contacts()
        self.get_installed()
    }
    
    @objc func reloadChatList(){
        self.conversationSyncToRealDB()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.mySearchBar.resignFirstResponder()
        self.navigationController?.navigationBar.topItem?.rightBarButtonItem = nil
        ChatListViewController.refreshview = false
        self.customSC.isHidden = true
        chatlistviewtrue = false
        //remove observer
        NotificationCenter.default.removeObserver(self, name: .receive_message, object: nil)
        NotificationCenter.default.removeObserver(self, name: .participant_left, object: nil)
        NotificationCenter.default.removeObserver(self, name: .user_status, object: nil)
        NotificationCenter.default.removeObserver(self, name: .receive_group, object: nil)
        NotificationCenter.default.removeObserver(self, name: .socketIsConnected, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if GlobalVariables.isFromChat == true {
            GlobalVariables.isFromChat = false
        }
        let turshiltbtn = UIBarButtonItem(image: UIImage(named:"compose"), style: .plain, target: self, action: #selector(buttonTapped(sender:)))
                self.navigationController?.navigationBar.topItem?.rightBarButtonItem = turshiltbtn
    }
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
        isallChat = true
        self.lastmessageid = ""
        chatlistviewtrue = true
        self.receiveSocket()
        self.listTableView.isUserInteractionEnabled = true
        self.customSC.isHidden = false
        self.listTableView.tableHeaderView = mySearchBar
        self.customSC.selectedSegmentIndex = 0;
        self.shooow = [""]
        if realm.objects(Conversation_realm.self).count != 0 {
            self.items = realm.objects(Conversation_realm.self).sorted(byKeyPath: "updated_at", ascending: false)
            self.origitems = self.items
            self.filtereditems = self.items
            self.listTableView.reloadData()
        }
        
        if ChatListViewController.refreshview == true{
            self.conversationSyncToRealDB()
        }
        if self.items != nil {
            self.mySearchBar.isHidden = false
            self.createChatBtn.isHidden = true
        }else{
            self.createChatBtn.isHidden = false
            self.mySearchBar.isHidden = true
            createChatBtn.setTitle("   Чат үүсгэх", for: UIControl.State.normal)
            self.listTableView.tableHeaderView = createChatBtn
        }
  
        self.listTableView.reloadData()
        getBadgeCount()
    
        if GlobalStaticVar.clickedNotification != ""{
            GlobalStaticVar.clickedNotification = ""
        }
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    func conversationSyncToRealDB(){
        
        DispatchQueue.main.async {
//            print("CHECKER ::: conversationSyncToRealDB ")
            Conversation.getconversations(userid: self.myid, limit: self.limit, skip: 0) { (conversations) in
                if self.customSC.selectedSegmentIndex == 0 {
                    self.items = realm.objects(Conversation_realm.self).sorted(byKeyPath: "updated_at", ascending: false)
                }else{
                    self.items = realm.objects(Conversation_realm.self).filter("is_thread != 0").sorted(byKeyPath: "updated_at", ascending: false)
                }
                self.refresher.endRefreshing()
                self.listTableView.reloadData()
            }
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.menuButton.isEnabled = false
        self.customSC.isEnabled = false
        self.mySearchBar.showsCancelButton = true
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text != "" {
            let searchtxt = searchBar.text?.lowercased()
            self.items = self.filtereditems.filter("title CONTAINS[c]'\(searchtxt!)' OR ANY user.nickname CONTAINS[c]'\(searchtxt!)'")
            self.listTableView.reloadData()
        }else{
            self.items = self.origitems
        }
        self.listTableView.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.mySearchBar.showsCancelButton = false
        self.menuButton.isEnabled = true
        self.customSC.isEnabled = true
        self.backBtn.isEnabled = true
        self.mySearchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.mySearchBar.resignFirstResponder()
        self.menuButton.isEnabled = true
        self.customSC.isEnabled = true
        self.backBtn.isEnabled = true
        self.mySearchBar.text = ""
        self.items = self.origitems
        self.listTableView.reloadData()
    }
    
    func isFiltering() -> Bool {
        if self.mySearchBar.text != ""{
            return true
        }
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
  
    func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
        var gradientImage:UIImage?
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            gradientLayer.render(in: context)
            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        UIGraphicsEndImageContext()
        return gradientImage
    }
    
    @objc func changeSegment(sender: UISegmentedControl!)  {
        switch sender.selectedSegmentIndex {
        case 0:
            self.items = realm.objects(Conversation_realm.self).sorted(byKeyPath: "updated_at", ascending: false)
            self.isallChat = true
            if self.items.count == 0 {
                 self.createChatBtn.isHidden = false
                self.mySearchBar.isHidden = true
                createChatBtn.setTitle("   Чат үүсгэх", for: UIControl.State.normal)
                self.listTableView.tableHeaderView = createChatBtn
            }else{
                self.createChatBtn.isHidden = true
                self.mySearchBar.isHidden = false
                self.listTableView.tableHeaderView = self.mySearchBar
            }
           
            listTableView.reloadData()
        case 1:
            self.isallChat = false
            self.mySearchBar.isHidden = true
            listTableView.tableHeaderView?.isHidden = false
            self.listTableView.tableHeaderView = createChatBtn
            self.items = realm.objects(Conversation_realm.self).filter("is_thread != 0").sorted(byKeyPath: "updated_at", ascending: false)
            self.createChatBtn.isHidden = false
            createChatBtn.setTitle("  Групп чат үүсгэх", for: UIControl.State.normal)

            listTableView.reloadData()
        default:
            self.mySearchBar.isHidden = false
            self.items = self.origitems
            listTableView.reloadData()
        }
    }
    
    @objc func buttonTapped(sender: UIButton){
            self.customSC.isHidden = true
            self.performSegue(withIdentifier: "tocreate", sender: self)
    }

    func dateFormat(date: Double) -> String {
        let dateformatter = DateFormatter()
        let lastlogin = NSDate(timeIntervalSince1970: date / 1000)
        dateformatter.dateStyle = .short
        dateformatter.timeStyle = .short
        let last = dateformatter.string(from: lastlogin as Date)
        return last
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.items != nil {
            return items.count
        }
        return 0
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    override func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Гарах"
    }
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let selected_con_id = self.items[indexPath.row].id
            let alert = UIAlertController(title: "Чатаас гарах", message: "Та чатаас гарахдаа итгэлтэй байна уу ?", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Тийм", style: .default  , handler: {action in
                Conversation.leaveConversation(conversation_id: selected_con_id, completion: { (con) in
                    if con == true {
                        RealmService.deleteConversationFromRealm(deleteConversation: selected_con_id, completion: { (succ) in
                            if succ == true {
                                self.listTableView.deleteRows(at: [indexPath], with: .fade)
                                self.items = realm.objects(Conversation_realm.self).sorted(byKeyPath: "updated_at", ascending: false)
                                self.delete_conversation(con_id: selected_con_id)
                                self.listTableView.reloadData()
                            }
                        })
                        RealmService.deleteMessages(deleteMessageParentId: selected_con_id, completion: { (suc) in
//                            print("message uudiig ustaglaaa successfully")
                        })
                    }
                })
            }))
            alert.addAction(UIAlertAction(title: "Болих", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func delete_conversation(con_id: String){
        let token = UserDefaults.standard.object(forKey:"token") as! String
        let  header = ["x-access-token": token, "authorization": "BPXjEvWfXxx5qpQHHFMEJEB8fA3xAczC"]
        let baseurl = SettingsViewController.baseurl
//        KVNProgress.show(withStatus: "", on: self.view)
        Alamofire.request("\(baseurl)delete_conversation", method: .post, parameters: ["conversation_id":con_id, "user_id":GlobalVariables.user_id], encoding:
            JSONEncoding.default,headers:header).responseJSON
           {
                response in
                switch response.result {
                case .success:
                    let res = JSON(response.result.value!)
                    print("deleteConv: ",res)
                case .failure(let e):
                    print(e)
                }
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        print("CELLFORROW-------------------------------")
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ConversationsTBCell
        let fileurl = SettingsViewController.fileurl
        let users = self.items[indexPath.row].user
        let conversation = self.items[indexPath.row]
        cell.clearCellData()
        cell.groupview.isHidden = true
        cell.profilePic.isHidden = false
        for i in 0..<users.count{
            if !users[i].id.contains(myid){
                cell.nameLabel.text = users[i].nickname
                let url = URL(string: "\(fileurl)\(users[i].image)")
                cell.profilePic?.sd_setImage(with: url, placeholderImage: UIImage(named: "circle-avatar"))
                if self.items[indexPath.row].user[i].state == 1 {
                    cell.onlineGreen.isHidden = false
                }
            }
        }
        
        if conversation.is_thread == true {
            var states = 0
            for s in 0..<users.count{
                states += self.items[indexPath.row].user[s].state
            }
            if states > 0{
                cell.onlineGreen.isHidden = false
            }
            if self.items[indexPath.row].image == ""{
                cell.profilePic.isHidden = true
                cell.groupview.isHidden = false
                cell.nameLabel.text = self.items[indexPath.row].title
                if users.count == 2 && users.count > 1 {
                    let url1 = URL(string: "\(fileurl)\(users[0].image)")
                    let url4 = URL(string: "\(fileurl)\(users[1].image)")
                    cell.gimg1.sd_setImage(with: url1,placeholderImage: UIImage(named: "circle-avatar"))
                    cell.gimg4.sd_setImage(with: url4,placeholderImage: UIImage(named: "circle-avatar"))
                }else if users.count == 3 {
                    let url1 = URL(string: "\(fileurl)\(users[0].image)")
                    let url2 = URL(string: "\(fileurl)\(users[1].image)")
                    let url3 = URL(string: "\(fileurl)\(users[2].image)")
                    cell.gimg1.sd_setImage(with: url1, placeholderImage: UIImage(named: "circle-avatar"))
                    cell.gimg2.sd_setImage(with: url2, placeholderImage: UIImage(named: "circle-avatar"))
                    cell.gimg3.sd_setImage(with: url3, placeholderImage: UIImage(named: "circle-avatar"))
                }else if users.count > 3{
                    let url1 = URL(string: "\(fileurl)\(users[0].image)")
                    let url2 = URL(string: "\(fileurl)\(users[1].image)")
                    let url3 = URL(string: "\(fileurl)\(users[2].image)")
                    cell.gimg1.sd_setImage(with: url1,placeholderImage: UIImage(named: "circle-avatar"))
                    cell.gimg2.sd_setImage(with: url2,placeholderImage: UIImage(named: "circle-avatar"))
                    cell.gimg3.sd_setImage(with: url3,placeholderImage: UIImage(named: "circle-avatar"))
                }
            }else{
                cell.profilePic.isHidden = false
                cell.groupview.isHidden = true
                let url = URL(string: "\(fileurl)\(conversation.image)")
                cell.profilePic.sd_setImage(with: url)
                cell.nameLabel.text = conversation.title
            }
        }

        var lastmessage = ""
      
        if self.items[indexPath.row].lastmessage != nil{
            let msg = self.items[indexPath.row].lastmessage!
            let ownerId = msg.owner_id
            if GlobalVariables.deletedMsgsID.contains(msg.id){
                lastmessage = "deleted"
            }else{
                
            
            switch msg.type {
            case "text":
                lastmessage = msg.content
            case "photo":
                lastmessage = "Зураг илгээлээ"
            case "audio":
                lastmessage = "Аудио илгээлээ"
            case "video":
                lastmessage = "Бичлэг илгээлээ"
            case "voice":
                lastmessage = "Бичлэг илгээлээ"
            case "other":
                lastmessage = "Файл илгээлээ"
            case "sticker":
                lastmessage = "Sticker илгээлээ"
            //zasvar
            case "document":
                lastmessage = "Файл илгээлээ"
            case "conversation_leave":
                lastmessage = "чатаас гарлаа"
            case "conversation_join":
                if ownerId != myid {
                    if msg.content != myid{
                        if let ind = users.index(where: {id in id.id == msg.content}){
                            let nickname = users[ind].nickname
                            lastmessage = "\(nickname) чатад нэмэгдлээ"
                        }
                    }else{
                        lastmessage = "Би чатад нэгдлээ"
                    }
                }else{
                    if let ind = users.index(where: {id in id.id == msg.content}){
                        let nickname = self.items[indexPath.row].user[ind].nickname
                        lastmessage = "\(nickname) чатад нэмэгдлээ"
                    }
                }
            default:
                lastmessage = "Хоосон"
            }
            if ownerId == myid {
                if msg.type != "conversation_join" {
                    cell.messageLabel.text = ("Би: \(lastmessage)")
                }else{
                    cell.messageLabel.text = lastmessage
                }
            } else {
                if let ind = users.index(where: {id in id.id == ownerId}){
                    let nickname = self.items[indexPath.row].user[ind].nickname
                    if self.items[indexPath.row].is_thread == false {
                        cell.messageLabel.text = "\(lastmessage)"
                    } else {
                        if msg.type != "conversation_join" {
                            cell.messageLabel.text = ("\(nickname): \(lastmessage)")
                        }else{
                            cell.messageLabel.text = lastmessage
                        }
                    }
                }
            }
            let date = self.dateFormat(date: Double(msg.created_at))
            cell.timeLabel.text = date
            if msg.seen.contains(where: {id in id.seen == myid}){
                bagdeRemoveItem(conid: self.items[indexPath.row].id)
            } else {
                badgeUpdate(conid: conversation.id)
                cell.nameLabel.font = UIFont(name:"HelveticaNeue-Bold", size: 16.0)
                cell.nameLabel.textColor = UIColor.black
                cell.messageLabel.font = UIFont(name:"HelveticaNeue-Bold", size: 11.0)
                cell.timeLabel.font = UIFont(name:"HelveticaNeue-Bold", size: 11.0)
                cell.profilePic.layer.borderColor = GlobalVariables.purple.cgColor
                cell.groupview.layer.borderColor = GlobalVariables.purple.cgColor
                cell.messageLabel.textColor = UIColor.black
            }
            }
        } else {
            cell.messageLabel.text = "Одоогоор хоосон байна"
            let date = self.dateFormat(date: Double(conversation.created_at))
            cell.timeLabel.text = date
        }
        return cell
    }
    //added observers
    func receiveSocket(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.socketDatasMessageIsReceived(notification:)), name: .receive_message, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.socketParticipantLeft(notification:)), name: .participant_left, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.socketUserStatus(notification:)), name: .user_status, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.socketReceiveGroup(notification:)), name: .receive_group, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.socketIsConnected(notification:)), name: .socketIsConnected, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityNotification(notification:)), name: Notification.Name("ReachabilityStatusChangedNotification"), object: nil)
    }
    
    @objc func reachabilityNotification(notification: Notification) {
//        print("---------reachabilityNotification-------------------")
        if SettingsViewController.online {
            if socket.status == .connected {
                socketInitialize()
            }else{
                socketInitialize()
            }
        }else{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу")
        }
    }
    
    @objc func socketIsConnected(notification:Notification){
//        print("---------socketIsConnected-------------------")
//        print(socket.status)
        if SettingsViewController.online {
            if socket.status == .notConnected || socket.status == .disconnected{
                socketInitialize()
            }
        }else{
            KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу")
        }
    }
    
    @objc func socketDatasMessageIsReceived(notification:Notification){
        //here is coming socket datas
//        print("T##items: Any...##Any")
        self.items = realm.objects(Conversation_realm.self).sorted(byKeyPath: "updated_at", ascending: false)
        self.listTableView.reloadData()
    }
    
    @objc func socketParticipantLeft(notification:Notification){
        self.items = realm.objects(Conversation_realm.self).sorted(byKeyPath: "updated_at", ascending: false)
        self.listTableView.reloadData()
    }
    @objc func socketReceiveGroup(notification:Notification){
        self.items = realm.objects(Conversation_realm.self).sorted(byKeyPath: "updated_at", ascending: false)
        self.listTableView.reloadData()
    }
    @objc func socketUserStatus(notification:Notification){
        if realm.objects(User_group_realm.self).count != 0 {
            if self.isallChat {
                self.items = realm.objects(Conversation_realm.self).sorted(byKeyPath: "updated_at", ascending: false)
            }else{
                self.items = realm.objects(Conversation_realm.self).filter("is_thread != 0").sorted(byKeyPath: "updated_at", ascending: false)
            }
        }
        self.listTableView.reloadData()
    }
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if items != nil {
            let lastElement = items.count - 1
            if indexPath.row == lastElement {
                self.skip = self.skip + 40
                if self.items.count > 30 {
                    Conversation.getconversations(userid: myid, limit: limit, skip: skip) { (conversations) in
                        DispatchQueue.main.async {
                            self.listTableView.reloadData()
                        }
                    }
                }
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        gotoSingleChat(conid: self.items[indexPath.row].id)
    }
    
    func gotoSingleChat(conid:String){
        self.listTableView.isUserInteractionEnabled = false
            self.customSC.isHidden = true
            self.mySearchBar.text = ""
            for index in 0..<self.items!.count{
                if self.items[index].id == conid {
                    GlobalVariables.ConversationOwnerId = self.items[index].owner_id_con
                    self.groupChatImgName = self.items[index].image
                    let selectedUser = self.items[index].user
                    var title = [String]()
                    is_thread = self.items[index].is_thread
                    if is_thread == true{
                        title.append(self.items[index].title)
                        backtitle = title
                    }else{
                        for i in 0..<self.items[index].user.count{
                            if  !self.items[index].user[i].id.contains(myid){
                                backtitle = [self.items[index].user[i].nickname]
                            }
                        }
                    }
                    // print("SELECTED CHAT: ",self.items[indexPath.row])
                    senderchat = self.items[index].id
                    var users = [User_group]()
                    for u in 0..<selectedUser.count{
                        let selected = User_group.init(id: selectedUser[u].id, image: selectedUser[u].image, nickname: selectedUser[u].nickname, lastname: selectedUser[u].lastname, surname: selectedUser[u].surname, phone: selectedUser[u].phone, state: selectedUser[u].state, online_at: Double(selectedUser[u].online_at), fcm_token: selectedUser[u].fcm_token,registerId: selectedUser[u].register)
                        users.append(selected)
                    }
                    selectedgroupUser = users
                    let shows = self.items[index].show
                    //                self.shooow =  self.items[indexPath.row].show
                    SingleChatVC.shows = []
                    let me = SettingsViewController.userId
                    for i in 0..<shows.count{
                        self.shooow.append(shows[i].show)
                    }
                    // print("SELECTED CHATUSERS ::: ",self.shooow as Any)
                    for s in shows{
                        if me != s.show{
                            SingleChatVC.shows.append(s.show)
                        }
                    }
                    
                    if self.items[index].lastmessage != nil{
                        if self.items[index].lastmessage.seen.contains(where: {$0.seen == me}){
                            lastmessageSeened = false
                        }else{
                            lastmessageSeened = true
                            bagdeRemoveItem(conid: self.items[index].id)
                        }
                            lastmessageid = self.items[index].lastmessage.id
                    }
                    GlobalStaticVar.activeConversationId = self.items[index].id
                    self.performSegue(withIdentifier: "SingleChatVC", sender: self)
                }else{
                    GlobalStaticVar.selectedConversationId = ""
                }
            }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SingleChatVC" {
//            print("lastmessageid:: ", lastmessageid)
            let singlechat = segue.destination as! SingleChatVC
            singlechat.selectedgroupUser = selectedgroupUser
            singlechat.senderchatid = self.senderchat
            SearchUserVC.selectedconvid = self.senderchat
            singlechat.navtitle = backtitle
            singlechat.is_thread = is_thread
            ChatListViewController.nowMessagingConversationID = self.senderchat
            singlechat.lastmessageid = lastmessageid
            singlechat.showusersid = self.shooow
            singlechat.groupChatImg = self.groupChatImgName
        }
    }
    
    func getAccessToContacts(){
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        switch authorizationStatus {
        case .authorized:
            accessAuthorized = true
        case .denied, .notDetermined:
            accessAuthorized = false
        let alert = UIAlertController(title: "", message: "Тохиргоо руу орж хадгалсан дугаарын жагсаалт авах тохиргоог идэвхжүүлнэ үү", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
            
            alert.addAction(UIAlertAction(title: "Тохиргоо", style: UIAlertAction.Style.default, handler:  { (alert:UIAlertAction) -> Void in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        default:

            let alert = UIAlertController(title: "", message: "Тохиргоо руу орж хадгалсан дугаарын жагсаалт авах тохиргоог идэвхжүүлнэ үү", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
            
            alert.addAction(UIAlertAction(title: "Тохиргоо", style: UIAlertAction.Style.default, handler:  { (alert:UIAlertAction) -> Void in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }))
            
            self.present(alert, animated: true, completion: nil)
            accessAuthorized = false
        }
    }
    
    func get_contacts(){
        let store = CNContactStore()
        let fetchRequest = CNContactFetchRequest(keysToFetch: [CNContactFamilyNameKey as CNKeyDescriptor, CNContactGivenNameKey as CNKeyDescriptor, CNContactPhoneNumbersKey as CNKeyDescriptor, CNContactIdentifierKey as CNKeyDescriptor])
        do{
            try store.enumerateContacts(with: fetchRequest) { contact, stop in
                if contact.phoneNumbers.count > 0 {
                    self.accessAuthorized = true
                    let key = contact.identifier
                    var name = ""
                    if contact.givenName.isEmpty {
                        name = contact.familyName
                    }
                    name = contact.givenName
                    let lastname = contact.familyName
                    var phones = [String]()
                    for number: CNLabeledValue in contact.phoneNumbers {
                        
                        let numbervalue = number.value
                        var digits = (numbervalue.value(forKey: "digits") as? String)!
                        if digits.hasPrefix("+"){
                            digits.remove(at: digits.startIndex)
                            if digits.count == 8 {
                                phones.append("976\(digits)")
                                self.numbersToDB.append("976\(digits)")
                            }else{
                                phones.append(digits)
                                self.numbersToDB.append(digits)
                            }
                        }else{
                            if digits.count == 8 {
                                phones.append("976\(digits)")
                                self.numbersToDB.append("976\(digits)")
                            }else{
                                phones.append(digits)
                                self.numbersToDB.append(digits)
                            }
                        }
                    }
    
                    DispatchQueue.main.async {
                        let contactToDB = Contacts.init(id: key, name: name, lastname: lastname, surname: "", image: "", phones: phones, phone: "", gender: "", birthday: 0, state: 0, online_at: 0, created_at: 0, fcm_token: "")
                        
//                        RealmService.writeContactToRealm(installedContacts: contactToDB, completion: { (complete) in
//                            if complete == true{
//                              //  self.tableview.reloadData()
//                            }
//                        })
                    }
                }
            }
        }catch{
            self.getAccessToContacts()
        }
    }
    
    func get_installed(){
        let baseurl = SettingsViewController.baseurl
        //  let headers = SettingsViewController.headers
        let phone = userDefaults.string(forKey: "phone")!
        if self.numbersToDB.contains(phone){
            let index = self.numbersToDB.index(of: phone)
            self.numbersToDB.remove(at: index!)
        }
        
        let token = UserDefaults.standard.object(forKey:"token") as! String
        let  header = ["x-access-token": token, "authorization": "BPXjEvWfXxx5qpQHHFMEJEB8fA3xAczC"]
//        KVNProgress.show()
        Alamofire.request("\(baseurl)user_info", method: .post, parameters: ["phone_numbers":self.numbersToDB], encoding: JSONEncoding.default,headers:header).responseJSON{ response in
            KVNProgress.dismiss()
            if response.response?.statusCode == 200 {
                let jsonData = JSON(response.value!)
                let error = jsonData["error"].boolValue
                DispatchQueue.main.async {
                    let loops = jsonData["result"]
                    if error == false {
                        for i in 0..<loops.count{
                            let dispid = jsonData["result"][i]["_id"].string ?? ""
                            let dispphone = jsonData["result"][i]["phone"].string ?? ""
                            let dispgender = jsonData["result"][i]["gender"].string ?? ""
                            let disppic = jsonData["result"][i]["image"].string ?? ""
                            let dispnickname = jsonData["result"][i]["nickname"].string ?? ""
                            let displastname = jsonData["result"][i]["lastname"].string ?? ""
                            let dispsurname = jsonData["result"][i]["surname"].string ?? ""
                            let dispbirthday = jsonData["result"][i]["birthday"].double ?? 0
                            let dispstate = jsonData["result"][i]["state"].int ?? 0
                            let online_at = jsonData["result"][i]["online_at"].double ?? 0
                            let fcm_token = jsonData["result"][i]["fcm_token"].string ?? ""
                            DispatchQueue.main.async {
                                let installedApp = Contacts.init(id: dispid, name: dispnickname, lastname: displastname, surname: dispsurname, image: disppic, phones: [""], phone: dispphone, gender: dispgender,birthday:dispbirthday, state: dispstate, online_at: online_at, created_at: online_at, fcm_token: fcm_token)
//                                RealmService.writeContactToRealm(installedContacts: installedApp, completion: { (writeComplete) in
//                                    if writeComplete == true {
//                                      //  self.tableview.reloadData()
//                                    }
//                                })
                            }
                        }
                    }else{
                        print("ContactViewController - print error")
                    }
                }
            }
        }
    }
}
extension ChatListViewController:UNUserNotificationCenterDelegate{
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void){
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        if notification.request.identifier == ChatListViewController.requestIdentifier{
            completionHandler( [.alert,.sound,.badge])
        }
    }
}

