import Foundation
import UIKit
import MessageUI

class SenderCell: UITableViewCell {
  
    
    @IBOutlet weak var profilePic: RoundedImageView!
    @IBOutlet weak var message: UITextView!
    @IBOutlet weak var messageBackground: UIImageView!
    @IBOutlet weak var audio: UIView!
    @IBOutlet weak var playpause: UIButton!
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var senderNameLabel: UILabel!
    
    func clearCellData()  {
        self.message.text = nil
        self.message.textColor = GlobalVariables.black
        self.message.isHidden = false
        self.audio.isHidden = true
        self.messageBackground.image = nil
        self.messageBackground.isHidden = true
       // self.message.font = UIFont(name:"sego", size: 17.0)
        self.senderNameLabel.textColor = GlobalVariables.purple
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.message.textContainerInset = UIEdgeInsets.init(top: 10, left: 10, bottom: 10, right: 10)
        
        self.audio.layer.cornerRadius = 15
//        self.audio.layer.borderWidth = 1
//        self.audio.layer.borderColor = GlobalVariables.lightGray.cgColor
        self.playpause.layer.borderWidth = 1
        self.playpause.layer.borderColor = UIColor.white.cgColor
        self.playpause.contentEdgeInsets = UIEdgeInsets(top: 5,left: 5,bottom: 5,right: 5)
        self.playpause.layer.cornerRadius = 15
        
        self.messageBackground.layer.cornerRadius = 10
        self.message.layer.cornerRadius = 15
        
//        let maskPath = UIBezierPath.init(roundedRect: self.message.bounds, byRoundingCorners:[.topLeft, .bottomLeft], cornerRadii: CGSize.init(width: 8.0, height: 8.0))
//        let maskLayer = CAShapeLayer()
//        maskLayer.frame = self.message.bounds
//        maskLayer.path = maskPath.cgPath
//        self.message.layer.mask = maskLayer
        
        self.messageBackground.clipsToBounds = true
         self.messageBackground.layer.masksToBounds = true
        self.profilePic.layer.borderWidth = 1.0
        self.profilePic.layer.borderColor = GlobalVariables.purple.cgColor
    }
}
class ReceiverCell: UITableViewCell {
    
    @IBOutlet weak var message: UITextView!
    @IBOutlet weak var messageBackground: UIImageView!
    @IBOutlet weak var audio: UIView!
    @IBOutlet weak var playpause: UIButton!
    @IBOutlet weak var seen: UILabel!
    @IBOutlet weak var receiverProPic: UIImageView!
    
    
    func clearCellData()  {
        self.message.text = nil
     //   self.messageBackground.backgroundColor = GlobalVariables.purple
        self.receiverProPic.layer.borderColor = GlobalVariables.purple.cgColor
        self.receiverProPic.layer.borderWidth = 1.0
    
        self.message.textColor = UIColor.white
        self.message.isHidden = false
        self.audio.isHidden = true
        self.messageBackground.image = nil
        self.messageBackground.isHidden = true
        self.messageBackground.layer.masksToBounds = true
       // self.message.font = UIFont(name:"segoeui", size: 17.0)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.message.textContainerInset = UIEdgeInsets.init(top: 10, left: 10, bottom: 10, right: 10)
        self.audio.layer.cornerRadius = 15
       // self.audio.layer.borderWidth = 1
//        self.audio.layer.borderColor = GlobalVariables.lightGray.cgColor
        self.playpause.layer.borderWidth = 1
        self.playpause.layer.borderColor = UIColor.white.cgColor
        self.playpause.contentEdgeInsets = UIEdgeInsets(top: 5,left: 5,bottom: 5,right: 5)
        self.playpause.layer.cornerRadius = 15
        self.message.layer.cornerRadius = 15
        self.messageBackground.layer.cornerRadius = 15
        self.messageBackground.clipsToBounds = true
      //  self.messageBackground.backgroundColor = GlobalVariables.purple
         self.messageBackground.layer.masksToBounds = true
        self.message.textColor = UIColor.red
    }
}
class LogCell:UITableViewCell {
    @IBOutlet weak var leaveUserImg: UIImageView!
    @IBOutlet weak var leaveUserMsg: UILabel!
    @IBOutlet weak var backView: UIView!
    
    func clearCellData() {
        leaveUserImg.image = nil
        leaveUserMsg.text = nil
    }
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            var frame = newFrame
            frame.origin.y += 4
            super.frame = frame
        }
    }
    
    override func awakeFromNib() {
        self.backView.layer.cornerRadius = 20
        self.backView.layer.masksToBounds = true
        self.selectionStyle = .none
        self.leaveUserImg.layer.cornerRadius = 25
        self.leaveUserImg.clipsToBounds = true
        self.leaveUserMsg.font = UIFont(name:"Helvetica", size: 12.0)
    }
}
class ConversationsTBCell: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var profilePic: RoundedImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var groupview: RoundView!
    @IBOutlet weak var gimg1: UIImageView!
    @IBOutlet weak var gimg2: UIImageView!
    @IBOutlet weak var gimg3: UIImageView!
    @IBOutlet weak var gimg4: UIImageView!
    @IBOutlet weak var onlineGreen: UIView!
    
    func clearCellData()  {
        let scale = true
        self.backView.layer.shadowColor = UIColor.black.cgColor
        self.backView.layer.shadowOpacity = 0.5
        self.backView.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.backView.layer.shadowRadius = 1
//        self.backView.layer.masksToBounds = false
        self.backView.layer.shadowPath = UIBezierPath(rect: self.backView.bounds).cgPath
        self.backView.layer.shouldRasterize = true
        self.backView.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
        
        self.nameLabel.font = UIFont(name:"segoeui", size: 16.0)
        self.messageLabel.font = UIFont(name:"HelveticaNeue-Light", size: 11.0)
        self.timeLabel.font = UIFont(name:"HelveticaNeue-Light", size: 11.0)
        self.profilePic.layer.borderColor = UIColor.rbg(r: 240, g: 240, b: 240).cgColor
        self.groupview.layer.borderColor = UIColor.rbg(r: 240, g: 240, b: 240).cgColor
        self.groupview.layer.cornerRadius = 25
        self.profilePic.layer.cornerRadius = 25
        self.nameLabel.textColor = UIColor.rbg(r: 94, g: 96, b: 103)
        self.messageLabel.textColor = UIColor.rbg(r: 101, g: 102, b: 111)
        self.groupview.layer.borderColor = UIColor.clear.cgColor
        self.profilePic.layer.borderColor = UIColor.clear.cgColor
        self.onlineGreen.layer.cornerRadius = 6
        self.onlineGreen.layer.borderWidth = 1
        self.onlineGreen.layer.borderColor = UIColor.clear.cgColor
        self.onlineGreen.backgroundColor = GlobalVariables.green
        self.onlineGreen.isHidden = true
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.profilePic.layer.borderWidth = 2
        self.profilePic.layer.borderColor = GlobalVariables.purple.cgColor
        self.groupview.layer.borderWidth = 2
        self.groupview.layer.borderColor = GlobalVariables.purple.cgColor
    }
}
class LetterTableViewCell: UITableViewCell {
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userLabel: UILabel?
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var inviteBtn: UIButton!
    
    @IBOutlet weak var icon: UIImageView!
    @IBAction func inviteBtn(_ sender: Any) {
//        if (MFMessageComposeViewController.canSendText()) {
//            let controller = MFMessageComposeViewController()
//            controller.body = "Message Body"
//            controller.recipients = ["97699135543"]
//            controller.messageComposeDelegate = self as! MFMessageComposeViewControllerDelegate
//            self.presentViewController(controller, animated: true, completion: nil)
//        }
    }
    
    func clearCellData()  {
        self.userLabel?.text = nil
        self.userLabel?.isHidden = false
        self.userLabel?.font = GlobalVariables.customFont16
        //self.phoneLabel.isHidden = true
        self.userImageView.layer.cornerRadius = 25
        self.userImageView.clipsToBounds = true
        self.userImageView.layer.borderWidth = 0.5
        self.userImageView.layer.borderColor = GlobalVariables.F5F5F5.cgColor
        //self.userImageView.contentMode = .scaleAspectFit
        
        self.inviteBtn.layer.borderColor = UIColor.white.cgColor
        self.inviteBtn.layer.borderWidth = 1.0
        self.inviteBtn.layer.cornerRadius = 10
        self.inviteBtn.backgroundColor = GlobalVariables.todpurple
    }
   
//    func configure(name: String, phone: String) {
//        userLabel?.text = name
//        userImageView?.setImage(string: name, color: UIColor.gray, circular: true)
//        phoneLabel.text = phone
//    }

}
class sideMenuCell: UITableViewCell{
    
    @IBOutlet weak var sideMenuIcons: UIImageView!
    @IBOutlet weak var sideMenuname: UILabel!
    @IBOutlet weak var sideMenuNameDetail: UILabel!
    
}
class SearchCell:UITableViewCell {
    
    @IBOutlet weak var searchPropic: UIImageView!
    @IBOutlet weak var searchProname: UILabel!
    @IBOutlet weak var searchProdetail: UILabel!
    @IBOutlet weak var subview: UIView!
    func clearCellData()  {
        self.searchProname.textColor  = GlobalVariables.purple
        self.searchPropic.layer.cornerRadius = 20
        self.searchProname.font = UIFont(name:"segoeui", size: 16.0)
        self.searchProdetail.font = UIFont(name:"segoeui", size: 12.0)
        self.searchProdetail.textColor = GlobalVariables.purple
        self.subview.layer.borderColor = GlobalVariables.purple.withAlphaComponent(0.4).cgColor
        self.subview.layer.borderWidth = 0
        self.subview.layer.masksToBounds = true
        
    }
}

class stickerViewCell:UICollectionViewCell {
    @IBOutlet weak var stickerImage: UIImageView!
    func clearCellData(){
        
    }
}

class stickerViewDownloadCell:UICollectionViewCell{
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var stickerThumImage: UIImageView!
    @IBOutlet weak var stickerDownload: UIButton!
    
    func clearCellData(){
        self.stickerDownload.layer.cornerRadius = 25
    }
}

class stickerHeaderView:UICollectionReusableView{
    
    @IBOutlet weak var stickerTabScrollview: UIScrollView!
    
}
class SettingsTableCell:UITableViewCell{
    
}

class singleReceiver:UITableViewCell{
    
    @IBOutlet weak var receiverImage: UIImageView!
    @IBOutlet weak var textMessage: UILabel!
    @IBOutlet weak var imageMessage: UIImageView!
    @IBOutlet weak var audio: UIView!
    @IBOutlet weak var playpause: UIButton!
    @IBOutlet weak var playImg : UIImageView!
    @IBOutlet weak var time: UILabel!
    func clearCellData() {
        self.time.text = ""
        self.time.isHidden = true
        self.time.setNeedsDisplay(CGRect(x: 0, y: 0, width: 0, height: 0))
        self.time.setNeedsLayout()
        self.receiverImage.isHidden = true
        self.receiverImage.layer.borderColor = GlobalVariables.F5F5F5.cgColor
        self.receiverImage.layer.borderWidth = 1.0
        self.textMessage.text = nil
        self.textMessage.textColor = UIColor.white
        self.textMessage.isHidden = false
        self.audio.isHidden = true
        self.imageMessage.image = nil
        self.imageMessage.isHidden = true
        self.playImg.isHidden = true
      
    }

    func setFrame(){
        var frame: CGRect {
            get {
                return super.frame
            }
            set (newFrame) {
                var frame = newFrame
                frame.origin.y += 10
                super.frame = frame
            }
        }
    }
    
    override func awakeFromNib() {
        time.textColor = GlobalVariables.lightGray
        time.font = UIFont(name:"Helvetica", size: 12.0)
        textMessage.backgroundColor = GlobalVariables.purple
        textMessage.textColor = UIColor.white
        textMessage.layer.cornerRadius = 15
        textMessage.layer.masksToBounds = true
        receiverImage.layer.cornerRadius = 7.5
        receiverImage.layer.masksToBounds = true
        imageMessage.layer.cornerRadius = 15
        imageMessage.layer.masksToBounds = true
        audio.layer.cornerRadius = 15
        playpause.layer.borderWidth = 1
        playpause.layer.borderColor = UIColor.white.cgColor
        playpause.contentEdgeInsets = UIEdgeInsets(top: 5,left: 5,bottom: 5,right: 5)
        playpause.layer.cornerRadius = 15
        self.layoutIfNeeded()
    }
}

class singleSender:UITableViewCell{
    
    @IBOutlet weak var senderImage: UIImageView!
    @IBOutlet weak var textMessage: UILabel!
    @IBOutlet weak var imageMessage: UIImageView!
    @IBOutlet weak var audio: UIView!
    @IBOutlet weak var playpause: UIButton!
    @IBOutlet weak var play: UIImageView!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var singleChatSenderSeen: UIImageView!
    
    @IBOutlet weak var ner: UILabel!
    func clearCellData() {
        self.time.text = ""
        self.time.isHidden = true
        self.time.setNeedsDisplay(CGRect(x: 0, y: 0, width: 0, height: 0))
        self.time.setNeedsLayout()
        self.ner.text = ""
        self.ner.isHidden = true
        self.ner.setNeedsDisplay(CGRect(x: 0, y: 0, width: 0, height: 0))
        self.ner.setNeedsLayout()
        self.senderImage.isHidden = true
        self.ner.isHidden = true
        self.textMessage.text = nil
//        self.senderImage.layer.borderColor = GlobalVariables.purple.cgColor
//        self.senderImage.layer.borderWidth = 1.0
        self.textMessage.textColor = UIColor.black
        self.textMessage.isHidden = false
        self.audio.isHidden = true
//        self.imageMessage.backgroundColor = UIColor.darkGray
        self.imageMessage.image = nil
        self.imageMessage.isHidden = true
        self.play.isHidden = true
        singleChatSenderSeen.isHidden = true
        self.layoutIfNeeded()
    }
    
    func setFrame(){
        var frame: CGRect {
            get {
                return super.frame
            }
            set (newFrame) {
                var frame = newFrame
                frame.origin.y += 10
                super.frame = frame
            }
        }
    }
    
    override func awakeFromNib() {
        time.textColor = GlobalVariables.lightGray
        time.font = UIFont(name:"Helvetica", size: 12.0)
        ner.textColor = GlobalVariables.lightGray
        ner.font = UIFont(name:"Helvetica", size: 12.0)
        textMessage.textColor = UIColor.black
        textMessage.layer.cornerRadius = 15
        textMessage.layer.masksToBounds = true
        senderImage.layer.cornerRadius = 20
        senderImage.layer.masksToBounds = true
        imageMessage.layer.cornerRadius = 15
        imageMessage.layer.masksToBounds = true
        audio.layer.cornerRadius = 15
        // self.audio.layer.borderWidth = 1
        //        self.audio.layer.borderColor = GlobalVariables.lightGray.cgColor
        playpause.layer.borderWidth = 1
        playpause.layer.borderColor = UIColor.white.cgColor
        playpause.contentEdgeInsets = UIEdgeInsets(top: 5,left: 5,bottom: 5,right: 5)
        playpause.layer.cornerRadius = 15
        
        singleChatSenderSeen.layer.cornerRadius = 7.5
        singleChatSenderSeen.layer.masksToBounds = true
        
        self.layoutIfNeeded()
    }
    
}

@IBDesignable
class EdgeInsetLabel: UILabel {
    var textInsets = UIEdgeInsets.zero {
        didSet { invalidateIntrinsicContentSize() }
    }
    
    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        let insetRect = bounds.inset(by: textInsets)
        let textRect = super.textRect(forBounds: insetRect, limitedToNumberOfLines: numberOfLines)
        let invertedInsets = UIEdgeInsets(top: -textInsets.top,
                                          left: -textInsets.left,
                                          bottom: -textInsets.bottom,
                                          right: -textInsets.right)
        return textRect.inset(by: invertedInsets)
    }
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: textInsets))
    }
}

extension EdgeInsetLabel {
    @IBInspectable
    var leftTextInset: CGFloat {
        set { textInsets.left = newValue }
        get { return textInsets.left }
    }
    
    @IBInspectable
    var rightTextInset: CGFloat {
        set { textInsets.right = newValue }
        get { return textInsets.right }
    }
    
    @IBInspectable
    var topTextInset: CGFloat {
        set { textInsets.top = newValue }
        get { return textInsets.top }
    }
    
    @IBInspectable
    var bottomTextInset: CGFloat {
        set { textInsets.bottom = newValue }
        get { return textInsets.bottom }
    }
}
















