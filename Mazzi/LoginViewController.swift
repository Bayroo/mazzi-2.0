//
//  LoginViewController.swift
//  Mazzi
//
//  Created by Janibekm on 8/23/17.
//  Copyright © 2017 Janibekm. All rights reserved.
//

import UIKit
import SocketIO
import SwiftyJSON
import Alamofire
import FirebaseAuth
//zasvar
import FirebaseInstanceID
import FirebaseMessaging

class LoginViewController: UIViewController, UITextFieldDelegate, AuthUIDelegate {

    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var loginbtn: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var countrycode: UITextField!
    @IBOutlet weak var spiner: UIActivityIndicatorView!
    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    let userDefaults = UserDefaults.standard
    let limitLength = 8
    var phone = ""
    var inputnumber = ""
    var nickname = ""
    var image = ""
    var countcode = ""
    var verificationID = ""
    var fcm_token = ""
    //97699417882
    //97695652645
    var developmentNumbers = ["9769913554","97699991313","97699100000","+97690909090","97612345678","97691919293","97611112222"]
    override func viewDidLoad() {
        super.viewDidLoad()
        if  let displayphone = self.userDefaults.string(forKey: "displayphone") {
            self.phoneNumber.text = displayphone
            self.loginbtn.isEnabled = true
        }else{
               self.loginbtn.isEnabled = false
        }
        self.errorLabel.isHidden = true
        self.spiner.isHidden = true
        self.loginbtn.layer.cornerRadius = 25
        self.phoneNumber.attributedPlaceholder = NSAttributedString(string: "Утасны дугаар",attributes: [NSAttributedString.Key.foregroundColor: GlobalVariables.purple.withAlphaComponent(0.2)])
        self.phoneNumber.textColor = GlobalVariables.purple
        self.phoneNumber.delegate = self
        self.countrycode.delegate = self
        self.countrycode.borderStyle = .none
        self.countrycode.textColor = GlobalVariables.black
        self.loginbtn.setTitleColor(UIColor.gray, for: UIControl.State.normal)
        self.loginbtn.backgroundColor = GlobalVariables.purple
        
        let bottomLine4 = CALayer()
        bottomLine4.frame = CGRect(x:0.0, y:phoneNumber.frame.height - 1, width:phoneNumber.frame.width, height: 1.0)
        bottomLine4.backgroundColor = GlobalVariables.todpurple.cgColor
        phoneNumber.borderStyle = UITextField.BorderStyle.none
        phoneNumber.layer.addSublayer(bottomLine4)
        checkInternet()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.showKeyboard(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.phoneNumber.becomeFirstResponder()
        self.spiner.stopAnimating()
        self.spiner.isHidden = true
        self.navigationItem.hidesBackButton = true
        self.navigationItem.title = ""
    }
    
    @objc func dismissKeyboard(){
         phoneNumber.resignFirstResponder()
    }
    @objc func showKeyboard(notification: Notification) {
        if let frame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let height = frame.cgRectValue.height
            if height > 0{
                if userDefaults.string(forKey: "keyboardheight") == nil{
                    self.userDefaults.set(height, forKey: "keyboardheight")
                    GlobalVariables.keyboardheight = Int(height)
                    self.userDefaults.synchronize()
                }
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        phoneNumber.resignFirstResponder()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        if newLength >= limitLength{
            self.loginbtn.isEnabled = true
            self.loginbtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
         //   self.loginbtn.backgroundColor = GlobalVariables.TextBvdeg.withAlphaComponent(0.2)
        }else{
            self.loginbtn.isEnabled = false
            self.loginbtn.setTitleColor(UIColor.gray, for: UIControl.State.normal)
        }
        return newLength <= limitLength
    }
    
    @IBAction func loginbtn(_ sender: UIButton) {
        self.sendkey()
    }
    func sendkey(){

        let baseurl = SettingsViewController.baseurl
        self.spiner.isHidden = false
        self.spiner.startAnimating()
        self.spiner.color = UIColor.white
        self.spiner.backgroundColor = UIColor.black
        self.spiner.alpha = 0.5
        if SettingsViewController.online == true {
            let headers = SettingsViewController.headers
            //zasvar
            let mynumber = "\(countrycode.text!)\(phoneNumber.text!)"
            self.userDefaults.set(mynumber, forKey: "myNumber")
            self.userDefaults.synchronize()
            
            Alamofire.request("\(baseurl)getkey", method: .post, parameters: ["phone":phoneNumber.text!, "region":"976", "fcm_token":GlobalVariables.fcm_token], encoding: JSONEncoding.default,headers:headers).responseJSON{ response in
                print("PHONE::",  self.phoneNumber.text! )
                 print("countrycode::",  self.countrycode.text! )
                if response.response?.statusCode == 200 {
                    let res = JSON(response.value!)
                    print("RES::: ", res)
                    if res["created"].bool! == false{
                       if res["error"].bool! == false {
                            self.phone = res["phone"].string!
                            GlobalVariables.phone = self.phone
                            self.inputnumber = self.phoneNumber.text!
                            self.countcode = self.countrycode.text!
                            let when = DispatchTime.now() + 2
                            if self.developmentNumbers.contains(self.phone){
                                print("devMode")
                                DispatchQueue.main.asyncAfter(deadline: when) {
                                    self.userDefaults.set(self.inputnumber, forKey: "phone")
                                    self.userDefaults.set("development", forKey: "authVerificationID")
                                    self.userDefaults.synchronize()
                                    self.performSegue(withIdentifier: "confirmPass", sender: self)
                                }
                            } else {
                                DispatchQueue.main.asyncAfter(deadline: when) {
                                   // self.verificationID = verificationID!
                                  //  self.userDefaults.set(self.verificationID, forKey: "authVerificationID")
                                    
                                     self.userDefaults.set(self.image, forKey: "image")
                                     self.userDefaults.set(self.nickname, forKey: "nickname")
                                    
                                    self.userDefaults.set(self.inputnumber, forKey: "phone")
                                    self.userDefaults.set("development", forKey: "authVerificationID")
                                    self.userDefaults.synchronize()
                                    self.performSegue(withIdentifier: "confirmPass", sender: self)
                                }
                            }
                        }else{
                            self.errorLabel.isHidden = false
                            self.errorLabel.text = "Сервертэй холбогдож чадсангүй дахин илгээнэ үү!"
                            self.spiner.stopAnimating()
                            self.spiner.isHidden = true
                            checkInternet()
                        }
                    }else{
                        if res["error"].bool! == false {
                            self.phone = res["phone"].string!
                            self.inputnumber = self.phoneNumber.text!
                            self.countcode = self.countrycode.text!
                            let when = DispatchTime.now() + 2
                            print("PHONE: ::: ", self.phone)
                            if self.developmentNumbers.contains(self.phone){
                                print("devMode")
                                DispatchQueue.main.asyncAfter(deadline: when) {
                                    self.userDefaults.set("development", forKey: "authVerificationID")
                                    self.userDefaults.synchronize()
//                                    self.performSegue(withIdentifier: "toTerms", sender: self)
                                    let viewController = testvc(nibName: "test", bundle: nil)
                                    viewController.modalTransitionStyle = .crossDissolve
                                    viewController.modalPresentationStyle = .overFullScreen
                                    //                                            self.present(viewController, animated: true, completion: nil)
                                    self.navigationController?.pushViewController(viewController, animated: false)
                                    self.spiner.isHidden = true
                                    self.spiner.stopAnimating()
                                }
                            }else{
                                PhoneAuthProvider.provider().verifyPhoneNumber("+\(self.phone)", uiDelegate: self, completion: { (verificationID, error) in
                                    if let error = error {
                                        print("LoginViewController - sendkey: \(error)")
                                        let viewController = testvc(nibName: "test", bundle: nil)
                                        viewController.modalTransitionStyle = .crossDissolve
                                        viewController.modalPresentationStyle = .overFullScreen
                                        //                                            self.present(viewController, animated: true, completion: nil)
                                        self.navigationController?.pushViewController(viewController, animated: false)
                                        self.spiner.isHidden = true
                                        self.spiner.stopAnimating()
                                        return
                                        
                                    } else{
                                        self.verificationID = verificationID!
                                        self.userDefaults.set(self.verificationID, forKey: "authVerificationID")
                                        DispatchQueue.main.asyncAfter(deadline: when) {
                                            
                                            let viewController = testvc(nibName: "test", bundle: nil)
                                            viewController.modalTransitionStyle = .crossDissolve
                                            viewController.modalPresentationStyle = .overFullScreen
//                                            self.present(viewController, animated: true, completion: nil)
                                            self.navigationController?.pushViewController(viewController, animated: false)
                                            self.spiner.isHidden = true
                                            self.spiner.stopAnimating()
                                            
//                                            self.performSegue(withIdentifier: "toTerms", sender: self)
                                        }
                                    }
                                })
                            }
                        }else{
                            self.errorLabel.isHidden = false
                            self.errorLabel.text = "Сервертэй холбогдож чадсангүй дахин илгээнэ үү!"
                            self.spiner.stopAnimating()
                            self.spiner.isHidden = true
                            checkInternet()
                        }
                    }
                } else{
                    self.errorLabel.isHidden = false
                    self.errorLabel.text = "Сервертэй холбогдож чадсангүй дахин илгээнэ үү !"
                    self.spiner.stopAnimating()
                    self.spiner.isHidden = true
                    checkInternet()
                }
            }
        }else{
            self.spiner.isHidden = true
            self.spiner.stopAnimating()
            self.errorLabel.isHidden = false
            self.errorLabel.text = "Интернетийн холболтоо шалгана уу!"
            checkInternet()
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toTerms" {
            let tokey = segue.destination as? TermsofUseVC
            tokey?.phone = self.phone
            tokey?.inputnumber = self.inputnumber
            tokey?.countCode = self.countcode
            self.spiner.isHidden = true
            self.spiner.stopAnimating()
        }
    }
    
    
}
