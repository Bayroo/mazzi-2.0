//
//  Utils.swift
//  PWS
//
//  Created by MiD on 3/6/18.
//  Copyright © 2018 Bayara. All rights reserved.
//
import UIKit
import Foundation
import SwiftyJSON
import Alamofire


class Utils{
     static let sharedInstance = Utils()
    
    var allMsg = JSON([])
    var filteredMsgs = JSON([])
    var token = String()
    var phone = String()
    static func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    static func isValidPhone(testStr:String) -> Bool {
        let phoneRegEx = "^[0-9]{8}$"
        
        let phoneTest = NSPredicate(format:"SELF MATCHES %@", phoneRegEx)
        return phoneTest.evaluate(with: testStr)
    }
    
    
    static func isValidPass(testStr:String) -> Bool {
        let regex = "^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+${6,}"
        
        let test = NSPredicate(format:"SELF MATCHES %@", regex)
        return test.evaluate(with: testStr)
    }
    
    static func isValidRegister(testStr:String) -> Bool{
        
        let regex = "[а-яА-Я]{2,}+[0-9]{8}"
        let test = NSPredicate(format:"SELF MATCHES %@", regex)
        return test.evaluate(with:testStr)
    
    }
    
    static func isValidLink(testStr:String) -> Bool {
        let regex = "(http://|https://?=.*[.a-zA-Z0-9а-яА-Я])"
        let test = NSPredicate(format:"SELF MATCHES %@", regex)
        return test.evaluate(with:testStr)
    }
    
}

