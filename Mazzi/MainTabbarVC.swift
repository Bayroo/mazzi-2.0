//
//  MainTabbarVC.swift
//  Mazzi
//
//  Created by Bayara on 12/7/18.
//  Copyright © 2018 Mazzi Application. All rights reserved.
//

import Foundation
import Apollo
import KVNProgress

class MainTabbarVC:UITabBarController{
    var myNotify = [SumNotificationsQuery.Data.SumNotification.Result.Mclass]()
    var myId = SettingsViewController.userId
    var exam = UIViewController()
    var classId = GlobalStaticVar.classId
    
    let apollos: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        // Add additional headers as needed
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
        //        print("TOKEN APOLLOO:: ", GlobalVariables.headerToken)
        let url = URL(string:SettingsViewController.graphQL)!
        
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(backtoRoot))
        if GlobalStaticVar.teacherId == self.myId{
             self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:"editmenu"), style: .plain, target: self, action: #selector(goSettings))
        }else{
//           self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:"logout"), style: .plain, target: self, action: #selector(leaveClass))
        }
       
        self.navigationController?.navigationBar.tintColor = UIColor.black
        let leftbtn = UIBarButtonItem(image: UIImage(named:"Rounded"), style: .plain, target: self, action: #selector(self.goSettings))
        self.navigationController?.navigationBar.topItem?.leftBarButtonItem = leftbtn
        
        let rightBtn = UIBarButtonItem(image: UIImage(named:""), style: .plain, target: self, action: #selector(self.goSettings))
       
        rightBtn.tintColor = UIColor.black
        rightBtn.title = "₮10000"
        self.navigationController?.navigationBar.topItem?.rightBarButtonItem = rightBtn
        self.navigationController?.navigationBar.topItem?.title = "Mazzi"
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
       self.getSelectedClassBadge()
          NotificationCenter.default.addObserver(self, selector: #selector(self.upBadge(notification:)), name: Notification.Name("received_notify"), object: nil)
//        tabBarController?.tabBar.items?.last?.badgeValue = badgeNumber != 0 ? String(badgeNumber) : nil
    }
    
    @objc func backtoRoot(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func goSettings(){
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "setting") as? ClassroomSettingVC
        self.navigationController?.pushViewController(viewController!, animated: true)
    }

    
    func getSelectedClassBadge(){
        _ = self.apollos.clearCache()
        self.myNotify = []
        let query = SumNotificationsQuery(userId: self.myId)
        self.apollos.fetch(query: query) { result, error in
            if result?.data?.sumNotifications != nil {
                self.myNotify = (result!.data?.sumNotifications?.result?.mclass)! as! [SumNotificationsQuery.Data.SumNotification.Result.Mclass]
                for i in 0..<self.myNotify.count {
                    if self.myNotify[i].classId == GlobalStaticVar.classId {
                         let gg = self.myNotify[i].badge
                         self.tabBar.items?.last?.badgeValue =  gg != 0 ? String(gg!) : nil
                    }
                }
            }
        }
    }
    
    @objc func upBadge(notification:Notification){
        self.getSelectedClassBadge()
    }
}
