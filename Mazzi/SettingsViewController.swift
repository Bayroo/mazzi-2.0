//
//  SettingsViewController.swift
//  Mazzi App LLCChatTestV
//
//  Created by Janibekm on 8/24/17.
//  Copyright © 2017 Janibekm. All rights reserved.

import UIKit
import SocketIO
//import SideMenu
import Alamofire
import Foundation
import Apollo

//var cdiosocket = SettingsViewController.cdiosocket
var apollo = SettingsViewController.apollo
class SettingsViewController: UIViewController {
    
    let userDefaults = UserDefaults.standard
    static var locale:String = "en"
    static var phone:String = ""
    static var verify:Int = 0
    static var userId:String = GlobalVariables.user_id
    public static var username = ""
    static var loggedin = false
    static var limit = 30
    
//test server----------------------------------------------------
    
//    static var socketUrl =      "http://192.168.0.103:8000"
//    static var baseurl =          "http://192.168.0.109:8001/"
//    static var fileurl =        "http://192.168.0.118:8005/"
//    static var cdiosocketUrl =   "http://mazzi.mn:10001" //"http://192.168.0.111:10001"//
//    static var cdioFileUrl =    "http://192.168.0.118:10001"
//    static var graphQL =    "http://mazzi.mn:10000/graphql" // "http://192.168.0.108:10000/graphql"  //
   
//production server ---------------------------------------------
    
    static var baseurl   =     "https://mazzi.mn/rest/"
    static var socketUrl =     "https://mazzi.mn/"
    static var fileurl   =     "https://mazzi.mn/file/"
    static var graphQL =       "https://mclass.mazzi.mn/backend/graphql" //"http://mazzi.mn:10000/graphql" //  "http://192.168.0.105:10000/graphql"
    static var cdioFileUrl =   "https://mclass.mazzi.mn/file/"
    static var cdiosocketUrl = "https://mclass.mazzi.mn"
    static var token = GlobalVariables.headerToken
    static var secureToken = "BPXjEvWfXxx5qpQHHFMEJEB8fA3xAczC" //authorization token
    public static var online = true
    public static var is_confirmed = true
    public static var headers : HTTPHeaders = [ "authorization" :  secureToken , "x-access-token" : token ]
    public static var cameraAccess = false
    public static var micAccess    = false
    public static var photoAccess  = false
    public static var keyboardHeight = 0

    static var apollo: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        // Add additional headers as needed
        configuration.httpAdditionalHeaders = ["x-access-token": token] // Replace `<token>`
        let url = URL(string:SettingsViewController.graphQL)!
        
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
