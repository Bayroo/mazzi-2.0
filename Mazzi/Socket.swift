//
//  Socket.swift
//  Mazzi
//
//  Created by Janibekm on 12/8/17.
//  Copyright © 2017 Mazzi App LLC. All rights reserved.
//

import Foundation
import SocketIO
import SwiftyJSON
import RealmSwift
import AVKit
import UIKit
import KVNProgress
import UserNotifications
import UserNotificationsUI

public func checkInternet(){
    let status = Reach().connectionStatus()
    switch status {
    case .unknown, .offline:
        SettingsViewController.online = false
    case .online(.wwan):
        SettingsViewController.online = true
    case .online(.wiFi):
        SettingsViewController.online = true
    }
}
public func checkCameraAccess(){
    let authorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
    switch authorizationStatus {
    case .notDetermined:
        AVCaptureDevice.requestAccess(for: AVMediaType.video,completionHandler: { (granted:Bool) -> Void in
            if granted {
                SettingsViewController.cameraAccess = true
                
            }
            else {
                SettingsViewController.cameraAccess = false
            }
        })
    case .authorized:
        SettingsViewController.cameraAccess = true
    case .denied, .restricted:
        SettingsViewController.cameraAccess = false
    }
}
public func checkMicAccess(){
    switch AVAudioSession.sharedInstance().recordPermission {
    case AVAudioSession.RecordPermission.granted:
        SettingsViewController.micAccess = true
    case AVAudioSession.RecordPermission.denied:
        SettingsViewController.micAccess = false
    case AVAudioSession.RecordPermission.undetermined:
        SettingsViewController.micAccess = false
    }
}

//public func connectSocket(userid: String, completion: @escaping (Bool) -> Swift.Void)  {
//    let baseurl = SettingsViewController.baseurl
    
    
//    Alamofire.request("\(baseurl)update_pro", method: .post, parameters: ["userid":userids,"pic":profile.pic, "nickname": profile.nickname, "lastname":profile.lastname, "surname":profile.surname, "birthday":profile.birthday, "email":profile.email, "gender":profile.gender], encoding: JSONEncoding.default).responseJSON{ response in
//        if response.response?.statusCode == 200 {
//            let res = JSON(response.value!)
//            if res["error"].bool == false {
//                completion(true)
//            }else{
//                completion(false)
//            }
//        }else{
//            completion(false)
//        }
//    }
//}






