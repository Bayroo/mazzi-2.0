//
//  TesterViewController.swift
//  Mazzi
//
//  Created by Janibekm on 11/2/17.
//  Copyright © 2017 Mazzi App LLC. All rights reserved.
//

import UIKit
import SDWebImage

class TesterViewController: UIViewController {

    var sFileUrl = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        let fileurl = SettingsViewController.fileurl
        self.filesimg.sd_setImage(with: URL(string:"\(fileurl)\(sFileUrl)"), completed: nil)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var filesimg: UIImageView!
    @IBOutlet weak var bakbtn: UIButton!
    @IBAction func bakbtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
