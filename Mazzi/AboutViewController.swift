//
//  AboutViewController.swift
//  Mazzi
//
//  Created by Bayara on 8/5/18.
//  Copyright © 2018 Mazzi App LLC. All rights reserved.
//

import Foundation

class AboutViewController : UIViewController {
    
    override func viewDidLoad() {
        super .viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "Тухай"
        if let navigationBar = self.navigationController?.navigationBar {
             self.navigationController?.title = ""
            let gradient = CAGradientLayer()
            var bounds = navigationBar.bounds
            bounds.size.height += UIApplication.shared.statusBarFrame.size.height
            gradient.frame = bounds
            gradient.colors = [GlobalVariables.black.cgColor, GlobalVariables.purple.cgColor]
            gradient.startPoint = CGPoint(x: 0, y: 0)
            gradient.endPoint = CGPoint(x: 1, y: 0)
            if let image = getImageFrom(gradientLayer: gradient) {
                navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
            }
         
        }
    }
    
    func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
        var gradientImage:UIImage?
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            gradientLayer.render(in: context)
            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        UIGraphicsEndImageContext()
        return gradientImage
    }
    
    @IBAction func back(_ sender: Any) {
       
        dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
}
