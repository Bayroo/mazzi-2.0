//
//  singleChat2.swift
//  Mazzi
//
//  Created by janibekm on 8/26/18.
//  Copyright © 2018 Mazzi App LLC. All rights reserved.
//

import UIKit
import RealmSwift
import SocketIO
import SwiftyJSON
import Photos
import MobileCoreServices
import AVKit
import AVFoundation
import RealmSwift
//import FileBrowser
import Alamofire
import QuickLook
import KVNProgress
import AVFoundation
import MediaPlayer

class SingleChatVC: UIViewController, UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource,myProtocol,UIImagePickerControllerDelegate,UINavigationControllerDelegate, AVAudioRecorderDelegate,QLPreviewControllerDataSource,QLPreviewControllerDelegate {
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return fileURLs.last!
    }
    
    @IBOutlet weak var bottomTool: UIView!
    @IBOutlet weak var closeImg: UIButton!
    @IBOutlet weak var messageTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var sendMessageBtn: UIButton!
    @IBOutlet weak var showStickersBtn: UIButton!
    @IBOutlet weak var showCameraBtn: UIButton!
    @IBOutlet weak var showGalleryBtn: UIButton!
    @IBOutlet weak var showAudioRec: UIButton!
    @IBOutlet weak var recordTimeLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableVIew: UITableView!
    @IBOutlet weak var recBtn: UIButton!
    
    fileprivate let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 20.0, right: 10.0)
    static var fileurl = [String]()
    let quickLookController = QLPreviewController()
    var fileURLs = [NSURL]()
    var backview = UIView()
    var mySticker = UIImageView()
    var selectedStickerIndexPath = 0
    var bottomConstraint:NSLayoutConstraint?
    var textFieldConstraint:NSLayoutConstraint?
    var BtnConstraint:NSLayoutConstraint?
    var extraButtonsIsHidden = true
    var staticheight:CGFloat = 40
    var isStickersIsHidden = true
    let imagePicker = UIImagePickerController()
    var limit = 20
    var skip = 0
    var save = false
    let userDefaults = UserDefaults.standard
    var typingTime = DispatchTime.now()
    //dataSource Variablbes
    var stickerCat : Results<StickersCatRealm>!
    var myname = ""
    var owner_id = ""
    var is_thread = false
    var selectedgroupUser: [User_group]?
    var activeUsers = [User_group]()
    var showusersid = [String]()
    var senderchatid = ""
    var myId = SettingsViewController.userId
    var navtitle = [""]
    var lastmessageid = ""
    var sFileUrl = ""
    var sFileName = ""
    var sMsgType = ""
    var items = [Message]()
    var realmitems : Results<Message_realm>!
    var nowplaying = ""
    var image = ""
    var SelectedStickerIndex = 0
    static var shows = [String]()
    var audioURL:URL!
    var audioPlayer : AVAudioPlayer!
    var avplayer = AVPlayer()
    var avplayeritem:AVPlayerItem!
    var audiosettings = [String : Int]()
    var audioRecorder:AVAudioRecorder!
    var recordingSession : AVAudioSession!
    var meterTimer:Timer!
    var isMove = false
    var panGesture: UIPanGestureRecognizer! = nil
    var typingme = true
    var expandCell = false
    var selectedIndexPath:IndexPath? = nil
    var groupChatImg = ""
    @IBOutlet var saveBtn : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewInit()
        checkMicAccess()
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(Long))
        longGesture.allowableMovement = CGFloat.infinity
        self.showAudioRec.addGestureRecognizer(longGesture)
        self.audiorecInit()
        let StickerslongGesture = UILongPressGestureRecognizer(target: self, action: #selector(previewSticker(gesture:)))
        self.collectionView.addGestureRecognizer(StickerslongGesture)
        ChatListViewController.refreshview = true
        customize()
        for i in 0..<self.selectedgroupUser!.count{
            if self.showusersid.contains(String(self.selectedgroupUser![i].id)){
                self.activeUsers.append(self.selectedgroupUser![i])
            }else{
                
            }
        }
        self.messageTextField.becomeFirstResponder()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initDataSources()
        GlobalStaticVar.selectedConversationId = self.senderchatid
        GlobalStaticVar.clickedNotification = ""
        bagdeRemoveItem(conid: self.senderchatid)
        
        if socket.status == .notConnected || socket.status == .disconnected{
            if SettingsViewController.online == true{
                
                socketInitialize()
            }else{
                KVNProgress.showError(withStatus: "Интернэт холболтоо шалгана уу!")
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        ChatListViewController.refreshview = true
        GlobalStaticVar.activeConversationId = ""
        ChatListViewController.nowMessagingConversationID = ""
        GlobalStaticVar.selectedConversationId = ""
        //remove observer
        NotificationCenter.default.removeObserver(self, name: .receive_message, object: nil)
        NotificationCenter.default.removeObserver(self, name: .participant_left, object: nil)
        NotificationCenter.default.removeObserver(self, name: .marked_message, object: nil)
        NotificationCenter.default.removeObserver(self, name: .socketIsConnected, object: nil)
        NotificationCenter.default.removeObserver(self, name: .typing_receive, object: nil)
    }
    
    @objc func updateAudioMeter(timer: Timer)
    {
        if audioRecorder != nil {
            let min = Int(audioRecorder.currentTime / 60)
            let sec = Int(audioRecorder.currentTime.truncatingRemainder(dividingBy: 60))
            let totalTimeString = String(format: "%02d:%02d", min, sec)
            self.recordTimeLabel.text = "Цуцлах --> \(totalTimeString)"
            audioRecorder.updateMeters()
        }
    }
    
    @objc func Long(gesture: UILongPressGestureRecognizer){
        if SettingsViewController.micAccess == false {
            SettingsViewController.micAccess = false
            let alert = UIAlertController(title: "", message: "Тохиргоо руу орж микрофон ашиглах тохиргоог идэвхжүүлнэ үү", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
            
            alert.addAction(UIAlertAction(title: "Тохиргоо", style: UIAlertAction.Style.default, handler:  { (alert:UIAlertAction) -> Void in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }else{
            
        
        if gesture.state == .began{
            checkMicAccess()
            self.recBtn.isHidden = false
            UIView.animate(withDuration: 0.5, delay: 0.0, options: [.repeat, .autoreverse, .allowUserInteraction], animations: { () -> Void in
                self.recBtn.transform = CGAffineTransform(scaleX: 1.7, y: 1.7)
            }, completion: nil)
            self.audiorecInit()
            self.backview.isHidden = false
            if SettingsViewController.micAccess == true{
                self.messageTextField.isHidden = true
                self.recordTimeLabel.isHidden = false
                self.startRecording()
                isMove = true
            }else{
                
                SettingsViewController.micAccess = false
                let alert = UIAlertController(title: "", message: "Тохиргоо руу орж микрофон ашиглах тохиргоог идэвхжүүлнэ үү", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
                
                 alert.addAction(UIAlertAction(title: "Тохиргоо", style: UIAlertAction.Style.default, handler:  { (alert:UIAlertAction) -> Void in
                     UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                 }))

                self.present(alert, animated: true, completion: nil)
            }
        }
            
        else if gesture.state == .ended{
            self.backview.isHidden = true
            self.recBtn.isHidden = true
            let translation = gesture.location(in: self.bottomTool)
            if(translation.x < 260){
                self.recordTimeLabel.text = ""
                self.messageTextField.isHidden = false
                self.recordTimeLabel.isHidden = true
                self.finishRecording(success: true)
                isMove = false
                UIView.animate(withDuration: 0.5, delay: 1.0, options: [], animations: { () -> Void in
                    self.showAudioRec.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                }, completion: nil)
                self.showAudioRec.layer.removeAllAnimations()
                DispatchQueue.main.async {
                    Message.audioupload(chatId: self.senderchatid,audio: self.audioURL, completion: { (state, audiourl) in
                        //todojani
                        print(self.audioURL)
                        
                        DispatchQueue.main.async {
                            let str = JSON(audiourl)
                            let duration = str["duration"].double!
                            let content = str["url"].string!
                            self.composeMessage(type: "audio", content: content, filename: "", file_size: 0, duration:duration,name: self.myname)
                            self.audioRecorder = nil
                        }
                    })
                }
            }
        }
        else if gesture.state == .changed{
            
            let translation = gesture.location(in: self.bottomTool)
            gesture.view!.center = CGPoint(x: self.showAudioRec.center.x, y: self.showAudioRec.center.y)
            if translation.x > 260 {
                self.recordTimeLabel.text = ""
                self.messageTextField.isHidden = false
                self.recordTimeLabel.isHidden = true
                self.finishRecording(success: true)
                isMove = false
                UIView.animate(withDuration: 0.5, delay: 1.0, options: [], animations: { () -> Void in
                    self.showAudioRec.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                }, completion: nil)
                self.showAudioRec.layer.removeAllAnimations()
            }
        }
            
        }
    }
    
    func startRecording() {
        
        if SettingsViewController.micAccess == false{
            let alert = UIAlertController(title: "", message: "Тохиргоо руу орж микрофон ашиглах тохиргоог идэвхжүүлнэ үү", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
            
            alert.addAction(UIAlertAction(title: "Тохиргоо", style: UIAlertAction.Style.default, handler:  { (alert:UIAlertAction) -> Void in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }else{
            meterTimer = Timer.scheduledTimer(timeInterval: 0.1, target:self, selector:#selector(self.updateAudioMeter(timer:)), userInfo:nil, repeats:true)
            let audioSession = AVAudioSession.sharedInstance()
            do {
                audioRecorder = try AVAudioRecorder(url: self.directoryURL()! as URL, settings: audiosettings)
                audioRecorder.delegate = self
                audioRecorder.prepareToRecord()
            } catch {
                finishRecording(success: false)
            }
            do {
                try audioSession.setActive(true)
                audioRecorder.record()
            }catch {
                
            }
        }
      
    }
    
    func audiorecInit(){
        
        recordingSession = AVAudioSession.sharedInstance()
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.default, options: AVAudioSession.CategoryOptions.defaultToSpeaker)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { /*[unowned self]*/ allowed in
            }
        } catch {
            print("SingleChatController - failed to record!")
        }
        // Audio Settings
        audiosettings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
    }
    
    func finishRecording(success: Bool) {
        audioRecorder.stop()
        if !success {
            audioRecorder = nil
        }
    }
    
    func directoryURL() -> NSURL? {
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = urls[0] as NSURL
        let soundURL = documentDirectory.appendingPathComponent("sound.mp4")
        self.audioURL = soundURL!
        return soundURL as NSURL?
    }
    func customize(){
        NotificationCenter.default.addObserver(self, selector: #selector(SingleChatVC.showKeyboard(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SingleChatVC.hideKeyboard(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.messageTextField.delegate = self
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.tableVIew.delegate = self
        self.tableVIew.dataSource = self
        self.imagePicker.delegate = self
        imagePicker.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.backview.frame = CGRect(x:0 , y:0, width:self.view.bounds.width, height: self.view.bounds.height)
        self.backview.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        mySticker = UIImageView()
        mySticker.frame = CGRect(x:backview.center.x-110 , y:backview.center.y-150, width:250 , height:300)
        mySticker.contentMode = .scaleAspectFit
        
        let vWidth = self.view.frame.width
        let vHeight = self.view.frame.height
        
        let scrollImg: UIScrollView = UIScrollView()
        scrollImg.backgroundColor = UIColor.clear
        scrollImg.delegate = self
        scrollImg.frame = CGRect(x:0, y:0, width: vWidth,height: vHeight)
        scrollImg.alwaysBounceVertical = false
        scrollImg.alwaysBounceHorizontal = false
        scrollImg.showsVerticalScrollIndicator = true
        scrollImg.flashScrollIndicators()
        
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 10.0
        self.mySticker.clipsToBounds = false
        self.backview.addSubview(scrollImg)
        self.view.addSubview(backview)
        scrollImg.addSubview(mySticker)
        self.backview.addSubview(recBtn)
        self.backview.addSubview(recordTimeLabel)
        self.backview.addSubview(saveBtn)
        self.backview.addSubview(closeImg)
        self.backview.isHidden = true
        self.mySticker.isHidden = true
        let userInfo = userDefaults.dictionary(forKey: "userInfo")
        myname = userInfo!["nickname"] as! String
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.mySticker
    }
    
    func viewInit(){
        
        bottomTool.layer.borderColor = GlobalVariables.F5F5F5.cgColor
        bottomTool.layer.borderWidth = 1
        bottomTool.layer.masksToBounds = true
        bottomConstraint = NSLayoutConstraint(item: self.bottomTool, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        view.addConstraint(bottomConstraint!)
        hideStickerView()
        
        textFieldConstraint = NSLayoutConstraint(item: messageTextField, attribute: .left, relatedBy: .equal, toItem: bottomTool, attribute: .left, multiplier: 1, constant: 35)
        self.bottomTool.addConstraint(textFieldConstraint!)
        
        self.messageTextField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        self.moreButton.transform = CGAffineTransform(rotationAngle: -CGFloat.pi/2 )
        self.moreButton.setImage(UIImage(named: "plus"), for: .normal)
        

        self.sendMessageBtn.isEnabled = false
        
        //        self.showCameraBtn.isEnabled = false
        //        self.showAudioRec.isEnabled = false
        //        self.showStickersBtn.isEnabled = false
        //        self.showGalleryBtn.isEnabled = false
        
        let keyboardheight = userDefaults.string(forKey: "keyboardheight")
        GlobalVariables.keyboardheight = Int(keyboardheight!) ?? 375
        //navigation setup
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(baktoroot))
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        let stringRepresentation = navtitle.joined(separator: ", ")
        navigationItem.title = stringRepresentation
        let userInfo = userDefaults.dictionary(forKey: "userInfo")
        self.image = userInfo!["image"] as! String
        if is_thread == false{
            let rightAddBarButtonItem:UIBarButtonItem = UIBarButtonItem(image: UIImage(named:"editmenu"), style: .plain, target: self, action: #selector(consettings))
            self.navigationItem.setRightBarButtonItems([rightAddBarButtonItem], animated: true)
            if(userDefaults.object(forKey: selectedgroupUser![0].id) != nil){
                GlobalVariables.blocked = userDefaults.object(forKey: selectedgroupUser![0].id) as! Bool
            }
        } else {
            let rightAddBarButtonItem:UIBarButtonItem = UIBarButtonItem(image: UIImage(named:"editmenu"), style: .plain, target: self, action: #selector(consettings))
            self.navigationController?.navigationBar.tintColor = UIColor.white
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
            self.navigationItem.setRightBarButtonItems([rightAddBarButtonItem], animated: true)
        }
        //tableivew
        self.tableView.rowHeight = UITableView.automaticDimension
    }
    
    
    @objc func baktoroot(){
        if avplayer.isPlaying {
            self.nowplaying = ""
            avplayer.pause()
        }
        ChatListViewController.refreshview = true
        GlobalVariables.isFromChat = true
        self.navigationController?.popViewController(animated: false)
    }
    func hideStickerView(){
        
        for constraint in self.bottomTool.constraints {
            if constraint.identifier == "toolHeight" {
                constraint.constant = staticheight
            }
        }
        
        self.tableView.contentInset.bottom = 20
        self.tableView.scrollIndicatorInsets.bottom = 20
        self.tableView.reloadData()
        UIView.animate(withDuration: 0, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.bottomTool.layoutIfNeeded()
        }) { (completed) in }
    }
    
    func showStickerView(){
        for constraint in self.bottomTool.constraints {
            if constraint.identifier == "toolHeight" {
                constraint.constant = CGFloat(GlobalVariables.keyboardheight)+staticheight
            }
        }
        self.bottomTool.layoutIfNeeded()
        self.tableView.contentInset.bottom = 20
        self.tableView.scrollIndicatorInsets.bottom = 20
        UIView.animate(withDuration: 0, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.bottomTool.layoutIfNeeded()
            
        }) { (completed) in
            if self.items.count > 0 {
                self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
            }
        }
    }
    
    @IBAction func closeImg(_ sender: Any) {
        self.backview.isHidden = true
        self.mySticker.isHidden = true
        self.saveBtn.isHidden = true
        self.closeImg.isHidden = true
    }
    
    @IBAction func saveImage(_ sender: Any) {
        guard let imageToSave = self.mySticker.image else {
            return
        }
        UIImageWriteToSavedPhotosAlbum(imageToSave, self, nil, nil)
        //    dismiss(animated: true, completion: nil)
        KVNProgress.showSuccess()
    }
    
    func hideExtraButtons(toHide: Bool)  {
//        print(self.messageTextField.frame)
        self.bottomTool.translatesAutoresizingMaskIntoConstraints = false
        self.messageTextField.translatesAutoresizingMaskIntoConstraints = false
    
        switch toHide {
        case true:
            self.textFieldConstraint?.constant = 35
            UIView.animate(withDuration: 0.3) {
                self.bottomTool.layoutIfNeeded()
                self.view.setNeedsLayout()
            }
            
        default:
            self.textFieldConstraint?.constant = 190
            UIView.animate(withDuration: 0.3) {
                self.bottomTool.layoutIfNeeded()
                self.view.layoutIfNeeded()
            }
        }
      
    }
    
    @objc func showKeyboard(notification: Notification) {
        print("SHOWKEYBORAD")
        if let frame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            hideStickerView()
            let height = frame.cgRectValue.height
            bottomConstraint?.constant = -height
            self.tableView.contentInset.bottom = 20
            self.tableView.scrollIndicatorInsets.bottom = 20
            UIView.animate(withDuration: 0, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.view.layoutIfNeeded()
                if self.items.count > 0{
                    self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                }
            }) { (completed) in
                
            }
        }
    }
    
    @objc func hideKeyboard(notification: Notification) {
        bottomConstraint?.constant = 0
        self.tableView.contentInset.bottom = 20
        self.tableView.scrollIndicatorInsets.bottom = 20
        UIView.animate(withDuration: 0, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.view.layoutIfNeeded()
            if self.items.count > 0{
                self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
            }
        }) { (completed) in
            
        }
        if isStickersIsHidden == false{
            isStickersIsHidden = true
            hideStickerView()
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if extraButtonsIsHidden == false{
            changeMoreButtonImage()
            self.extraButtonsIsHidden = true
            self.hideExtraButtons(toHide: true)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var participants = ""
        if (range.location == 0 && string == " ") {
            return false
        }else{
            if typingme == true {
                typingme = false
                var typing = DispatchTime.now()+1
                let owner = myId
                for i in 0..<(selectedgroupUser!).count{
                    participants.append(selectedgroupUser![i].id)
                }
                let participants = SingleChatVC.shows
                let mytypes = ["owner_id":owner, "conversation_id": self.senderchatid, "participants" : participants, "typing": true] as [String : Any]
                socket.emit("typing", with: [mytypes])
                typing = DispatchTime.now() + 1
                DispatchQueue.main.asyncAfter(deadline: typing) {
                    self.typingme = true
                }
            }
            return true
        }
    }
    
    @objc func textFieldDidChange(textField:UITextField){
        
        if extraButtonsIsHidden == false{
            changeMoreButtonImage()
            self.extraButtonsIsHidden = true
            self.hideExtraButtons(toHide: true)
        }
        
        if (textField.text?.count)! > 0{
            // string contains non-whitespace characters
            self.sendMessageBtn.setImage(UIImage(named: "001-bear-pawprint"), for: .normal)
            self.sendMessageBtn.isEnabled = true
            
        }else{
            self.sendMessageBtn.setImage(UIImage(named: "001-bear-pawprint-saaral"), for: .normal)
            self.sendMessageBtn.isEnabled = false
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func changeMoreButtonImage(){
        if extraButtonsIsHidden == true{
            UIView.animate(withDuration: 0.2, animations: {
                self.moreButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi * 2)
                self.moreButton.setImage(UIImage(named:"minus"), for: .normal)
            })
        }else{
            UIView.animate(withDuration: 0.2, animations: {
                self.moreButton.transform = CGAffineTransform(rotationAngle: -CGFloat.pi/2 )
                self.moreButton.setImage(UIImage(named: "plus"), for: .normal)
            })
        }
    }
    
    @IBAction func moreButton(_ sender: Any) {
        messageTextField.becomeFirstResponder()
        changeMoreButtonImage()
        if extraButtonsIsHidden == true{
            self.extraButtonsIsHidden = false
            self.hideExtraButtons(toHide: false)
        }else{
            self.extraButtonsIsHidden = true
            self.hideExtraButtons(toHide: true)
        }
    }
    
    @IBAction func sendMessageBtn(_ sender: Any) {
        self.sendMessageBtn.isEnabled = false
        self.sendMessageBtn.setImage(UIImage(named: "001-bear-pawprint-saaral"), for: .normal)
        self.composeMessage(type: "text", content: self.messageTextField.text!, filename: "", file_size: 0, duration: 0,name:self.myname)
        self.messageTextField.text = ""
    }
    
    @IBAction func showStickers(_ sender: Any) {
        self.messageTextField.resignFirstResponder()
        if isStickersIsHidden == true{
            isStickersIsHidden = false
            showStickerView()
        }else{
            isStickersIsHidden = true
            hideStickerView()
        }
    }
    
    @IBAction func showCameraBtn(_ sender: Any) {
//        checkCameraAccess()
//        let when = DispatchTime.now() + 2
//        DispatchQueue.main.asyncAfter(deadline: when) {
//            if SettingsViewController.cameraAccess == true {
//                GlobalVariables.isChat = true
//                self.performSegue(withIdentifier: "cameraview", sender: self)
//
//            }else{
//                self.recordingSession = AVAudioSession.sharedInstance()
//                do {
//                    //                try recordingSession.setCategory(AVAudioSession.Category(rawValue: convertFromAVAudioSessionCategory(AVAudioSession.Category.playAndRecord)), mode: AVAudioSession.Mode)
//                    //                try recordingSession.setCategory(AVAudioSession.Category.playAndRecord,mode:AVAudioSession.Mode)
//                    try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.default, options: AVAudioSession.CategoryOptions.defaultToSpeaker)
//                    try self.recordingSession.setActive(true)
//                    self.recordingSession.requestRecordPermission() { /*[unowned self]*/ allowed in
//                        //                    DispatchQueue.main.async {
//                        //                    }
//                    }
//                } catch {
//                    print("SingleChatController - failed to record!")
//                }
//                // Audio Settings
//                self.audiosettings = [
//                    AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
//                    AVSampleRateKey: 12000,
//                    AVNumberOfChannelsKey: 1,
//                    AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
//                ]
//                let alert = UIAlertController(title: "", message: "Утасныхаа тохиргоо руу орж камер ашиглах боломжийг олгоно уу !", preferredStyle: UIAlertController.Style.alert)
//                alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
//                self.present(alert, animated: true, completion: nil)
//            }
//        }
        
        let authorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authorizationStatus {
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video,completionHandler: { (granted:Bool) -> Void in
                if granted {
                    SettingsViewController.cameraAccess = true
                    self.performSegue(withIdentifier: "cameraview", sender: self)
                }
                else {
                    SettingsViewController.cameraAccess = false
                    let alert = UIAlertController(title: "", message: "Утасныхаа тохиргоо руу орж камер ашиглах боломжийг олгоно уу !", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            })
        case .authorized:
            SettingsViewController.cameraAccess = true
            self.performSegue(withIdentifier: "cameraview", sender: self)
        case .denied, .restricted:
            SettingsViewController.cameraAccess = false
            let alert = UIAlertController(title: "", message: "Утасныхаа тохиргоо руу орж камер ашиглах боломжийг олгоно уу !", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func showGalleryBtn(_ sender: Any) {
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == .authorized || status == .notDetermined) {
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.mediaTypes = [(kUTTypeMovie as String), kUTTypeImage as String]
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func showAudioRec(_ sender: Any) {
        
    }
    
    @objc func consettings(){
        if is_thread == false{
            let viewController = storyboard?.instantiateViewController(withIdentifier: "ProfileContact") as! ProfileContact
            self.navigationController?.navigationBar.tintColor = UIColor.white
            //            viewController.contactid = selectedgroupUser![0].id
            //            viewController.selectedgroupuser = selectedgroupUser![0]
            
            for i in 0..<selectedgroupUser!.count{
                if !selectedgroupUser![i].id.contains(myId){
                    viewController.contactid = selectedgroupUser![i].id
                    viewController.userid = selectedgroupUser![i].id
                    viewController.selectedgroupuser = selectedgroupUser![i]
                }
                //                  print("users: ",selectedgroupUser![i].nickname)
            }
            
            GlobalVariables.isFromClassMembers = false
            GlobalVariables.isFromContacts = false
            let navController = UINavigationController(rootViewController: viewController)
            self.present(navController, animated:true, completion: nil)
        }else{
            //            print("participants: ", self.activeUsers as Any)
            let viewController = storyboard?.instantiateViewController(withIdentifier: "consettings") as! ConSettingsVC
            viewController.selectedgroupUsers = self.activeUsers
            viewController.senderchatid = self.senderchatid
            viewController.imgName = self.groupChatImg
            let stringRepresentation = navtitle.joined(separator: ", ")
            viewController.groupTitle = stringRepresentation
            let navController = UINavigationController(rootViewController: viewController)
            self.present(navController, animated:true, completion: nil)
        }
    }
    //collectionview init
    
    func initStickerView(){
        for constraint in collectionView.constraints {
            if constraint.identifier == "coll"{
                constraint.constant = CGFloat(GlobalVariables.keyboardheight)
            }
        }
        let flow = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        flow.sectionHeadersPinToVisibleBounds = true
        self.collectionView.layoutIfNeeded()
    }
    //tableview init
    //DataSorces begin here
    func initDataSources(){
        getStickersFromDB()
        writeStickersToRealm()
        self.receiveSocket()
        
        //        if userDefaults.string(forKey: "socketIsConnected") != nil {
        //            if userDefaults.string(forKey: "socketIsConnected") == "false"{
        //                self.moreButton.isEnabled = false
        //                self.messageTextField.isEnabled = false
        //            }
        //        }
        
        if(userDefaults.object(forKey: selectedgroupUser![0].id) != nil){
            GlobalVariables.blocked = userDefaults.object(forKey: selectedgroupUser![0].id) as! Bool
            if( GlobalVariables.blocked == false){
                self.fetchData()
            }else{

            }
            print("stat", GlobalVariables.blocked)
        }else{
            self.fetchData()
        }
    }
    
    func getStickersFromDB(){
        stickerCat = realm.objects(StickersCatRealm.self).sorted(byKeyPath: "time", ascending: false)
        initStickerView()
    }
    
    func writeStickersToRealm(){
        StickersCat.getStickerFromServer(userid: "") { (err,stickers) in
            if err == false{
                RealmService.createStickersCatRealm(createStickers: stickers, completion: { (comp) in
                    if comp == true {
                        self.getStickersFromDB()
                        self.collectionView.reloadData()
                    }
                })
            }
        }
    }
    func fetchData(){
        checkInternet()
        save = true

        if SettingsViewController.online == true {
            Message.downloadAllMessages(chatId: self.senderchatid,limit:self.limit,skip:self.skip,save:self.save, completion: {[weak weakSelf = self] (message,success) in
                DispatchQueue.main.async {
                    weakSelf?.items.sort{ $0.created_at < $1.created_at }
                    if success == true {
                        self.realmitems = try! Realm().objects(Message_realm.self).filter("conversation_id = '\(self.senderchatid)'").sorted(byKeyPath: "created_at", ascending: true)
                        self.getRealmMessages()
                    }
                    //weakSelf?.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                }
            })
        }else{
            self.realmitems = try! Realm().objects(Message_realm.self).filter("conversation_id = '\(self.senderchatid)'").sorted(byKeyPath: "created_at", ascending: true)
            self.getRealmMessages()
            
        }
        Message.markMessagesRead(lastmessageid: self.lastmessageid, conversation_id: senderchatid, save: save)
    }
    //todojani
    func getRealmMessages(){
        checkInternet()
        if self.realmitems.count != 0{
            //            self.items = []
            for i in 0..<self.realmitems.count{
                var seens = [String]()
                if self.realmitems[i].seen.count != 0{
                    for ids in self.realmitems[i].seen{
                        seens.append(ids.seen)
                    }
                }
                let msg = Message.init(id: self.realmitems[i].id, owner_id: self.realmitems[i].owner_id, content: self.realmitems[i].content, filename: self.realmitems[i].filename, file_size: self.realmitems[i].file_size, type: self.realmitems[i].type, seen: seens, created_at: Double(self.realmitems[i].created_at), duration: Double(self.realmitems[i].duration), conversation_id:self.realmitems[i].conversation_id , name:self.myname)
                if self.items.count > 0{
                    if !self.items.contains(where:{$0.id == self.realmitems[i].id}){
                        self.items.insert(msg, at:0)
                        for item in self.realmitems[i].seen {
                            if !item.seen.contains(myId) {
                                if item.id != "typing"{
                                    Message.markMessagesRead(lastmessageid: self.realmitems[i].id, conversation_id: self.realmitems[i].conversation_id, save: true)
                                }
                            }
                        }
                        
                        if self.realmitems[i].type == "photo" || self.realmitems[i].type == "video" || self.realmitems[i].type == "document" {
                            SingleChatVC.fileurl.append(self.realmitems[i].content)
                        }
                        self.items.sort{ $0.created_at < $1.created_at }
                        self.tableView.reloadData()
                        if self.items.count > 0 {
                            self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                        }
                    }
                }else{
                    self.items.insert(msg, at:0)
                    if self.realmitems[i].type == "photo" || self.realmitems[i].type == "video" || self.realmitems[i].type == "document" {
                        SingleChatVC.fileurl.append(self.realmitems[i].content)
                    }
                    self.items.sort{ $0.created_at < $1.created_at }
                    for item in self.realmitems[i].seen {
                        if !item.seen.contains(myId) {
                            if item.id != "typing"{
                                Message.markMessagesRead(lastmessageid: self.realmitems[i].id, conversation_id: self.realmitems[i].conversation_id, save: true)
                            }
                        }
                    }
                    self.tableView.reloadData()
                    if self.items.count > 0 {
                        self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                    }
                }
            }
        }
    }
    
    func receiveSocket(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.socketMessageReceived(notification:)), name: Notification.Name("receive_message"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.socketParticipantLeft(notification:)), name: Notification.Name("participant_left"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.socketMarkedMessage(notification:)), name: Notification.Name("marked_message"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.socketIsConnected(notification:)), name: Notification.Name("socketIsConnected"), object: nil)
        if is_thread == false {
            NotificationCenter.default.addObserver(self, selector: #selector(self.socketTypingReceived(notification:)), name: Notification.Name("typing_receive"), object: nil)
        }
    }
    
    @objc func socketIsConnected(notification:Notification){
        print("SINGLECHAT VC SOCKET")
        //here is coming socket datas
        let userInfo = notification.userInfo
        let isConnected = userInfo!["socketIsConnected"] as? String
        if isConnected == "true" {
            //connected
            userDefaults.set("true", forKey: "socketIsConnected")
            userDefaults.synchronize()
        } else {
            checkInternet()
            if SettingsViewController.online {
                DispatchQueue.main.asyncAfter(deadline: socketInitTime+5) {
                    socketInitialize()
                }
            }
        }
    }
    
    @objc func socketMessageReceived(notification:Notification){
        //here is coming socket datas
        //        print("observer worked")
        if notification.object != nil {
            let obj = notification.object as! Message
            if obj.conversation_id == self.senderchatid {
                getRealmMessages()
            }
        }
    }
    
    @objc func socketParticipantLeft(notification:Notification){
        let userinfo = notification.userInfo
        let conid = userinfo!["conversationId"] as! String
        if conid == self.senderchatid {
            baktoroot()
        }
    }
    
    @objc func socketMarkedMessage(notification:Notification){
        let userInfo = notification.userInfo
        let message_id = userInfo!["message_id"] as! String
        let user_id = userInfo!["user_id"] as! String
        let conversation_id = userInfo!["conversation_id"] as! String
        if conversation_id == self.senderchatid {
            if let row = self.items.index(where: {$0.id == message_id}) {
                if self.items[row].seen.contains(where:{$0 != user_id}) {
                    print("marking message =======")
                    //call here conversation update --> update realm conversation last message - > then
                    //todojani
                    //                    DispatchQueue.main.async {
                    self.items[row].seen.append(user_id)
                    //                        let indexPath = IndexPath(item: row, section: 0)
                    self.tableView.reloadData()
                    //                        self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                    //                    }
                }
            }
        }
    }
    
    @objc func socketTypingReceived(notification:Notification){
        if notification.object != nil {
            let object = notification.object as! TypingConversations
            if object.conversation_id == self.senderchatid {
                print("yag mun bn ")
                let created = Date().timeIntervalSince1970
                let doubledate = Double(created * 1000) + 100000
                if object.typing == true {
                    let lastmessage = Message.init(id: "typing", owner_id: owner_id, content: " . . . ", filename: "", file_size:0, type: "text", seen: [""],  created_at: doubledate, duration: 0, conversation_id: object.conversation_id,name:self.myname)
                    if self.items.contains(where: { name in name.id == "typing"}) {
                        self.typingTime = DispatchTime.now() + 3
                        self.items.sort{ $0.created_at < $1.created_at }
                        self.tableView.reloadData()
                        self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                    } else {
                        //jani
                        DispatchQueue.main.async {
                            if self.items.count > 0 {
                                if self.items[self.items.count-1].id != "typing"{
                                    self.items.append(lastmessage)
                                    self.typingTime = DispatchTime.now() + 3
                                    self.items.sort{ $0.created_at < $1.created_at }
                                    self.tableView.reloadData()
                                    self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    //        socket.on("receive_message"){data, ack in
    //            let chat = JSON(data)
    //            let message = chat[0]
    //            let id = message["_id"].string!
    //            let owner_id = message["owner_id"].string!
    //            let content = message["content"].string!
    //            let filename = message["filename"].string!
    //            let file_size = message["file_size"].int!
    //            let type = message["type"].string!
    //            var seen = message["seen"].arrayValue.map({$0.stringValue})
    //            let created_at = message["created_at"].double!
    //            let conversation_id = message["conversation_id"].string!
    //            let duration = message["duration"].double!
    //            if conversation_id == self.senderchatid {
    //                if id != ""{
    //                    let find = realm.object(ofType: Conversation_realm.self, forPrimaryKey: conversation_id)
    //                    if find != nil{
    //                        let con = realm.objects(Conversation_realm.self).filter("id = '\(conversation_id)'")
    //                        seen.append(self.myId)
    //                        let lastmessage = Message.init(id: id, owner_id: owner_id, content: content, filename: filename, file_size:file_size, type: type, seen: seen,  created_at: created_at, duration: duration,conversation_id:conversation_id,name:self.myname)
    //                        var showing = [String]()
    //                        for sh in con[0].show{
    //                            showing.append(sh.show)
    //                        }
    //                        if type == "photo" || type == "video" || type == "document" {
    //                            SingleChatVC.fileurl.append(content)
    //                        }
    //                        self.items.sort{ $0.created_at < $1.created_at }
    //                        Message.markMessagesRead(lastmessageid: id, conversation_id: conversation_id, save: false)
    //                        DispatchQueue.main.async {
    //                            if GlobalVariables.blocked == true{
    //                                let alert = UIAlertController(title: "\(self.selectedgroupUser![0].nickname) is blocked", message: "Go to settings to unblock this person", preferredStyle: UIAlertController.Style.alert)
    //                                alert.addAction(UIAlertAction(title: "Close", style: UIAlertAction.Style.default, handler: nil))
    //                                self.present(alert, animated: true, completion: nil)
    //                            }else{
    //                                self.items.append(lastmessage)
    //                                self.items.sort{ $0.created_at < $1.created_at }
    //                                self.tableView.reloadData()
    //                                self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //        socket.on("typing_receive"){data, ack in
    //            let type = JSON(data)
    //            self.typingTime = DispatchTime.now() + 2
    //            let tochatid = type[0]["conversation_id"].string!
    //            let typing = type[0]["typing"].boolValue
    //            let owner_id = type[0]["owner_id"].string!
    //            let created = Date().timeIntervalSince1970
    //            let doubledate = Double(created * 1000) + 100000
    //            if tochatid == self.senderchatid{
    //
    //                if GlobalVariables.blocked == true {
    //
    //                }else{
    //                    if typing == true {
    //                        // change 5 to desired number of seconds
    //                        let lastmessage = Message.init(id: "typing", owner_id: owner_id, content: " . . . ", filename: "", file_size:0, type: "text", seen: [""],  created_at: doubledate, duration: 0, conversation_id: tochatid,name:self.myname)
    //                        if self.items.contains(where: { name in name.id == "typing"}) {
    //                            self.typingTime = DispatchTime.now() + 2
    //                            self.items.sort{ $0.created_at < $1.created_at }
    //                            self.tableView.reloadData()
    //                        } else {
    //                            DispatchQueue.main.async {
    //                                self.items.append(lastmessage)
    //                                self.items.sort{ $0.created_at < $1.created_at }
    //                                self.tableView.reloadData()
    //                                self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
    //                            }
    //                        }
    //                    }
    //                }
    //
    //            }
    //        }
    //
    //        socket.on("participant_left"){data, ack in
    //            let leftUser = JSON(data)
    //            let user_id = leftUser[0]["user_id"].string!
    //            let con_id = leftUser[0]["conversation_id"].string!
    //            let success = leftUser[0]["success"].bool!
    //            let content = leftUser[0]["content"].string!
    //            let created_at = leftUser[0]["created_at"].double!
    //            let type = leftUser[0]["type"].string!
    //            if success == true {
    //                if user_id == self.myId {
    //                    Conversation.leaveConversation(conversation_id: con_id, completion: { (con) in
    //                        if con == true {
    //                            RealmService.deleteConversationFromRealm(deleteConversation: con_id, completion: { (succ) in
    //                                if succ == true {
    //                                    _ = self.navigationController?.popToRootViewController(animated: true)
    //                                }
    //                            })
    //                        }
    //                    })
    //                }else{
    //                    let message = Message.init(id: "ids\(Date().timeIntervalSinceNow)", owner_id: user_id, content: content, filename: "", file_size:0, type: type, seen: [user_id], created_at: created_at, duration: 0 , conversation_id: con_id,name:self.myname)
    //                    DispatchQueue.main.async {
    //                        self.items.append(message)
    //                        self.tableView.reloadData()
    //                    }
    //                }
    //            }
    //        }
    //get if socket sent me seened mesage id
    //then update table view
    //        socket.on(se)
    
    //tableView begin
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView.isDragging{
            if indexPath.row == 0 {
                skip = skip + limit
                save = false
                Message.downloadAllMessages(chatId: self.senderchatid,limit:limit,skip:skip,save:save, completion: { (message,success) in
                    DispatchQueue.main.async() {
                        if success == true {
                            self.items.append(message)
                            self.items.sort{ $0.created_at < $1.created_at }
                            self.tableView.reloadData()
                            let numberOfSections = self.tableView.numberOfSections
                            let numberOfRows = self.tableView.numberOfRows(inSection: numberOfSections-1)
                            self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - self.skip, section: 0), at: .top, animated: false)
                            if numberOfRows > self.skip {
                                let indexPath = IndexPath(row: numberOfRows-self.skip, section: (numberOfSections-1))
                                self.tableView.scrollToRow(at: indexPath, at: .top, animated: false)
                            }
                        }
                    }
                })
            }
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        DispatchQueue.main.asyncAfter(deadline: self.typingTime){
            if self.items.contains(where: { name in name.id == "typing"}) {
                let index = self.items.index(where: {$0.id == "typing"})
                self.items.remove(at: index!)
                self.tableView.reloadData()
            }
        }
        if self.items[indexPath.row].type != "conversation_leave" && self.items[indexPath.row].type != "conversation_join"{
            if self.items[indexPath.row].owner_id == myId {
                let cell = tableView.dequeueReusableCell(withIdentifier: "singleReceiver", for: indexPath) as! singleReceiver
                cell.clearCellData()
                
                if self.items.count > indexPath.row{
                    if self.items.count > indexPath.row+1 {
                        
                    }else{
                        if is_thread == false{
                            cell.receiverImage.isHidden = false
                            for i in 0..<selectedgroupUser!.count{
                                if selectedgroupUser![i].id != myId {
                                    if self.items[indexPath.row].seen.contains(where:{name in name == selectedgroupUser![i].id }){
                                        cell.receiverImage.sd_setImage(with: URL(string: "\(SettingsViewController.fileurl)\(selectedgroupUser![i].image)"), placeholderImage: UIImage(named: "avatar"))
                                    }else if self.items[indexPath.row].id.hasPrefix("ids") {
                                        cell.receiverImage.image = nil
                                        cell.receiverImage.layer.borderWidth = 1
                                        cell.receiverImage.layer.borderColor = GlobalVariables.lightGray.cgColor
                                        cell.receiverImage.layer.masksToBounds = true
                                    }else{
                                        cell.receiverImage.image = UIImage(named: "delivered")
                                    }
                                }
                            }
                        }else{
                            //group chat seen here
                            //                             let cellwidth = cell.frame.width
                            //                             let myIntValue = Int(cellwidth)
                            //                             for i in 0..<self.items[indexPath.row].seen.count {
                            //                                let img = UIImageView()
                            //                                let singleseen = self.items[indexPath.row].seen[i]
                            //                                img.frame = CGRect(x: (myIntValue - 16) - i * 20, y:0 , width:15,height:15)
                            //                                img.layer.cornerRadius = 7.5
                            //                                img.layer.masksToBounds = true
                            //                                img.layer.borderColor = UIColor.red.cgColor
                            //                                img.layer.borderWidth = 1.0
                            //                                cell.addSubview(img)
                            //                                for j in 0..<selectedgroupUser!.count{
                            //                                    if selectedgroupUser![i].id == singleseen {
                            //                                        img.sd_setImage(with: URL(string: "\(SettingsViewController.fileurl)\(selectedgroupUser![j].image)"), placeholderImage: UIImage(named: "avatar"))
                            //                                    }
                            //                                }
                            //                            }
                            
                        }
                    }
                }
                switch self.items[indexPath.row].type {
                case "text":
                    cell.textMessage.isHidden = false
                    cell.textMessage.text = self.items[indexPath.row].content
                    if selectedIndexPath == indexPath {
                        if expandCell == true {
                            cell.time.isHidden = false
                            cell.time.text = timstampToDate(time: self.items[indexPath.row].created_at)
                        }else{
                            cell.time.text = ""
                            cell.time.isHidden = true
                        }
                    }
                case "photo":
                    cell.imageMessage.image = UIImage.init(named: "loading2")
                    cell.textMessage.isHidden = true
                    cell.imageMessage.isHidden = false
                    let imgstring = self.items[indexPath.row].content
                    cell.imageMessage.sd_setImage(with: URL(string: "\(SettingsViewController.fileurl)\(imgstring)"), placeholderImage: UIImage(named: "loading2"))
                    cell.imageMessage.contentMode = .scaleAspectFill
                case "sticker":
                    cell.receiverImage.isHidden = false
                    cell.textMessage.isHidden = true
                    cell.imageMessage.isHidden = false
                    cell.imageMessage.image = UIImage(named: "loading2")
                    cell.imageMessage.contentMode = .scaleAspectFit
                    let imgstring = self.items[indexPath.row].content
                    cell.imageMessage.sd_setImage(with: URL(string: "\(SettingsViewController.fileurl)\(imgstring)"), placeholderImage: UIImage(named: "loading2"))
                    if selectedIndexPath == indexPath {
                        if expandCell == true {
                            cell.time.isHidden = false
                            cell.time.text = timstampToDate(time: self.items[indexPath.row].created_at)
                        }else{
                            cell.time.text = ""
                            cell.time.isHidden = true
                        }
                    }
                case "video":
                    cell.receiverImage.isHidden = false
                    cell.textMessage.isHidden = true
                    cell.imageMessage.isHidden = false
                    cell.imageMessage.image = UIImage(named: "loading2")
                    cell.imageMessage.contentMode = .scaleAspectFill
                    let imgstring = self.items[indexPath.row].content
                    let imgurl = URL(string: "\(SettingsViewController.fileurl)\(imgstring).jpg")
                    cell.imageMessage.sd_setImage(with: imgurl, placeholderImage: UIImage(named: "loading2"))
                    cell.playImg.isHidden = false
                case "audio":
                    cell.textMessage.isHidden = true
                    cell.imageMessage.isHidden = true
                    cell.audio.isHidden = false
                    if self.nowplaying == self.items[indexPath.row].id {
                        cell.playpause.setBackgroundImage(UIImage(named: "pause"), for: .normal)
                    }else{
                        cell.playpause.setBackgroundImage(UIImage(named: "play"), for: .normal)
                    }
                    cell.playpause.accessibilityHint = self.items[indexPath.row].content
                    cell.playpause.accessibilityValue = self.items[indexPath.row].id
                    cell.playpause.addTarget(self, action: #selector(playAudio), for: .touchDown)
                default:
                    print("other type")
                }
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "singleSender", for: indexPath) as! singleSender
                cell.clearCellData()
                
                if is_thread == true{
                    if indexPath.row == 0 {
                        cell.ner.isHidden = false
                        for i in 0..<selectedgroupUser!.count{
                            if self.items[indexPath.row].owner_id == selectedgroupUser![i].id{
                                cell.ner.text = selectedgroupUser![i].nickname
                            }
                        }
                    }else{
                        if self.items[indexPath.row].owner_id != self.items[indexPath.row-1].owner_id{
                            cell.ner.isHidden = false
                            for i in 0..<selectedgroupUser!.count{
                                if self.items[indexPath.row].owner_id == selectedgroupUser![i].id{
                                    cell.ner.text = selectedgroupUser![i].nickname
                                }
                            }
                        }
                    }
                    
                }else{
                    if self.items.count-1 == indexPath.row {
                        cell.singleChatSenderSeen.isHidden = false
                        for i in 0..<selectedgroupUser!.count{
                            if self.items[indexPath.row].owner_id == selectedgroupUser![i].id{
                                cell.singleChatSenderSeen.sd_setImage(with: URL(string: "\(SettingsViewController.fileurl)\(self.selectedgroupUser![i].image)"), placeholderImage: UIImage(named: "avatar"))
                            }
                        }
                    }
                }
                
                let chatusers = self.selectedgroupUser!
                if self.items.count > indexPath.row{
                    if self.items.count > indexPath.row+1 {
                        if self.items[indexPath.row+1].owner_id != self.items[indexPath.row].owner_id{
                            cell.senderImage.isHidden = false
                            for i in 0..<chatusers.count{
                                if chatusers[i].id == self.items[indexPath.row].owner_id{
                                    cell.senderImage.sd_setImage(with: URL(string: "\(SettingsViewController.fileurl)\(self.selectedgroupUser![i].image)"), placeholderImage: UIImage(named: "avatar"))
                                }
                            }
                        }
                    } else {
                        cell.senderImage.isHidden = false
                        for i in 0..<chatusers.count{
                            if chatusers[i].id == self.items[indexPath.row].owner_id{
                                cell.senderImage.sd_setImage(with: URL(string: "\(SettingsViewController.fileurl)\(self.selectedgroupUser![i].image)"), placeholderImage: UIImage(named: "avatar"))
                                
                            }
                        }
                    }
                }
                
                
                //                if !self.items[indexPath.row].seen.contains(myId){
                //                    if self.items[indexPath.row].id != "typing"{
                //                        Message.markMessagesRead(lastmessageid: self.items[indexPath.row].id, conversation_id: senderchatid,save:save)
                //                    }
                //                }
                
                switch self.items[indexPath.row].type {
                case "text":
                    cell.textMessage.isHidden = false
                    cell.textMessage.text = self.items[indexPath.row].content
                    
                    if selectedIndexPath == indexPath {
                        if expandCell == true {
                            cell.time.isHidden = false
                            cell.time.text = timstampToDate(time: self.items[indexPath.row].created_at)
                        }else{
                            cell.time.text = ""
                            cell.time.isHidden = true
                        }
                    }
                    
                case "photo":
                    cell.imageMessage.image = UIImage.init(named: "loading2")
                    cell.textMessage.isHidden = true
                    cell.imageMessage.isHidden = false
                    cell.imageMessage.contentMode = .scaleAspectFill
                    //                    print("IMGPATH::", self.items[indexPath.row].content)
                    let imgstring = self.items[indexPath.row].content
                    cell.imageMessage.sd_setImage(with: URL(string: "\(SettingsViewController.fileurl)\(imgstring)"), placeholderImage: UIImage(named: "loading2"))
                case "sticker":
                    cell.senderImage.isHidden = false
                    cell.textMessage.isHidden = true
                    cell.imageMessage.isHidden = false
                    cell.imageMessage.contentMode = .scaleAspectFit
                    cell.imageMessage.image = UIImage.init(named: "loading2")
                    let imgstring = self.items[indexPath.row].content
                    cell.imageMessage.sd_setImage(with: URL(string: "\(SettingsViewController.fileurl)\(imgstring)"), placeholderImage: UIImage(named: "loading2"))
                case "video":
                    cell.senderImage.isHidden = false
                    cell.textMessage.isHidden = true
                    cell.imageMessage.isHidden = false
                    cell.imageMessage.contentMode = .scaleAspectFill
                    cell.imageMessage.image = UIImage.init(named: "loading2")
                    let imgstring = self.items[indexPath.row].content
                    let imgurl = URL(string: "\(SettingsViewController.fileurl)\(imgstring).jpg")
                    print("-------------------------")
//                    print("IMG URL ::: ", imgurl)
                    cell.imageMessage.sd_setImage(with: imgurl, placeholderImage: UIImage(named: "loading2"))
                    cell.play.isHidden = false
                case "audio":
                    cell.textMessage.isHidden = true
                    cell.imageMessage.isHidden = true
                    cell.audio.isHidden = false
                    if self.nowplaying == self.items[indexPath.row].id {
                        cell.playpause.setBackgroundImage(UIImage(named: "pause"), for: .normal)
                    }else{
                        cell.playpause.setBackgroundImage(UIImage(named: "play"), for: .normal)
                    }
                    cell.playpause.accessibilityHint = self.items[indexPath.row].content
                    cell.playpause.accessibilityValue = self.items[indexPath.row].id
                    cell.playpause.addTarget(self, action: #selector(playAudio), for: .touchDown)
                default:
                    print("other type")
                }
                return cell
            }
        }else{
            let logcell = tableView.dequeueReusableCell(withIdentifier: "LogCell", for: indexPath) as! LogCell
            logcell.clearCellData()
            if self.items[indexPath.row].type == "conversation_leave"{
                if self.items[indexPath.row].owner_id == myId{
                    let userInfo = userDefaults.dictionary(forKey: "userInfo")
                    let image = userInfo!["image"] as! String
                    let img = URL(string: "\(SettingsViewController.fileurl)\(image)")
                    logcell.leaveUserImg.sd_setImage(with: img)
                    logcell.leaveUserMsg.text = "Би групп чатаас гарлаа"
                }else{
                    let chatusers = self.selectedgroupUser!
                    for i in 0..<chatusers.count {
                        if chatusers[i].id == self.items[indexPath.row].content {
                            let propic = chatusers[i].image
                            let img = URL(string: "\(SettingsViewController.fileurl)\(propic)")
                            logcell.leaveUserImg.sd_setImage(with: img, placeholderImage: UIImage(named: "avatar"))
                            logcell.leaveUserMsg.text = "\(chatusers[i].nickname) групп чатаас гарлаа"
                        }
                    }
                }
            }else if self.items[indexPath.row].type == "conversation_join"{
                if self.items[indexPath.row].content == myId {
                    let userInfo = userDefaults.dictionary(forKey: "userInfo")
                    let image = userInfo!["image"] as! String
                    let img = URL(string: "\(SettingsViewController.fileurl)\(image)")
                    logcell.leaveUserImg.sd_setImage(with: img)
                    logcell.leaveUserMsg.text = "Би групп чатад нэмэгдлээ"
                }else{
                    let chatusers = self.selectedgroupUser!
                    for i in 0..<chatusers.count {
                        if chatusers[i].id == self.items[indexPath.row].content {
                            let propic = chatusers[i].image
                            let img = URL(string: "\(SettingsViewController.fileurl)\(propic)")
                            logcell.leaveUserImg.sd_setImage(with: img, placeholderImage: UIImage(named: "avatar"))
                            logcell.leaveUserMsg.text = "\(chatusers[i].nickname) групп чатад нэмэгдлээ"
                        }
                    }
                }
            }
            return logcell
        }
    }
    
    func timstampToDate(time:TimeInterval)->String?{
        let date = Date(timeIntervalSince1970: time/1000)
        let dateFormatter = DateFormatter()
        //  dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    func downloadFile(url : String, name : String){
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let documentsURL = URL(fileURLWithPath: documentsPath, isDirectory: true)
            let fileURL = documentsURL.appendingPathComponent(name)
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        Alamofire.download(url, to: destination).response { response in
            print("file download")
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.messageTextField.resignFirstResponder()
        switch self.items[indexPath.row].type {
        //zasvar
        case "text":
            print("clicked")
            if selectedIndexPath == nil {
                expandCell = true
                self.selectedIndexPath = indexPath
                tableView.reloadRows(at: [self.selectedIndexPath!], with: .none )
            } else {
                expandCell = false
                tableView.reloadRows(at: [self.selectedIndexPath!], with: .none)
                self.selectedIndexPath = indexPath
                expandCell = true
                tableView.reloadRows(at: [self.selectedIndexPath!], with: .none)
                UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
                    self.tableView.beginUpdates()
                    self.tableView.endUpdates()},completion:({ _ in
                        
                    }))
            }
            
        case "document":
            self.sFileUrl = self.items[indexPath.row].content
            self.sMsgType = "document"
            let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
            let url = NSURL(fileURLWithPath: path)
            if let pathComponent = url.appendingPathComponent(self.items[indexPath.row].filename) {
                quickLookController.dataSource = self
                let filePath = pathComponent.path
                let fileManager = FileManager.default
                if fileManager.fileExists(atPath: filePath) {
                    let fileUrlToAdd = NSURL.fileURL(withPath: filePath)
                    fileURLs.append(fileUrlToAdd as NSURL as NSURL)
                    if QLPreviewController.canPreview(fileURLs.last!) {
                        quickLookController.reloadData()
                        quickLookController.currentPreviewItemIndex = fileURLs.count
                        navigationController?.pushViewController(quickLookController, animated: true)
                    }
                } else {
                    downloadFile(url: SettingsViewController.fileurl + self.items[indexPath.row].content, name : self.items[indexPath.row].filename)
                }
            } else {
                print("FILE PATH NOT AVAILABLE")
            }
        case "photo":
            self.sFileUrl = self.items[indexPath.row].content
            print(sFileUrl)
            self.sMsgType = "photo"
            let url = URL(string: "\(SettingsViewController.fileurl)\(self.sFileUrl)")
            //            self.mySticker.sd_setImage(with: url, completed: nil)
            mySticker.sd_setImage(with: url, placeholderImage: UIImage(named: "avatar"))
            self.mySticker.contentMode = .scaleAspectFit
            self.backview.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            let width = self.mySticker.image?.size.width
            let height = self.mySticker.image?.size.height
            let haritsaa = Float(width!)/Float(height!)
            let frameWidth = self.view.bounds.width - 60
            self.mySticker.sd_setImage(with: url, completed: {
                (image, error, cacheType, url) in
                let frame = CGRect(x: (self.view.bounds.width/2)-(self.view.bounds.width - 60)/2, y: (self.view.bounds.height/2) - (CGFloat(Float(frameWidth)*haritsaa + Float(frameWidth))-50)/2, width: frameWidth, height: CGFloat(Float(frameWidth)*haritsaa + Float(frameWidth)))
                self.mySticker.frame = frame
                self.mySticker.image = image
                self.mySticker.contentMode = .scaleAspectFit
                
            })
            self.backview.isHidden = false
            self.mySticker.isHidden = false
            self.saveBtn.isHidden = false
            self.closeImg.isHidden = false
            //            self.backview.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTappedOnBackgroundView)))
            
        case "sticker":
            if selectedIndexPath == nil {
                expandCell = true
                self.selectedIndexPath = indexPath
                tableView.reloadRows(at: [self.selectedIndexPath!], with: .none )
            } else {
                expandCell = false
                tableView.reloadRows(at: [self.selectedIndexPath!], with: .none)
                self.selectedIndexPath = indexPath
                expandCell = true
                tableView.reloadRows(at: [self.selectedIndexPath!], with: .none)
                UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
                    self.tableView.beginUpdates()
                    self.tableView.endUpdates()},completion:({ _ in
                        
                    }))
            }
            
        case "video":
            print("VIDEOOOO ")
            self.sFileUrl = self.items[indexPath.row].content
            self.sFileName = self.items[indexPath.row].filename
            self.sMsgType = "video"
            let url = URL(string: "\(SettingsViewController.fileurl)\(self.sFileUrl)\(".mp4")")
            let videoURL = url!

//            self.performSegue(withIdentifier: "filepreview", sender: self)
            
            let player = AVPlayer(url: videoURL)

            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            
            if extraButtonsIsHidden == false{
                changeMoreButtonImage()
                self.extraButtonsIsHidden = true
                self.hideExtraButtons(toHide: true)
            }
//            self.navigationController?.seg(playerViewController, animated: true)
            self.navigationController?.present(playerViewController, animated: true, completion: nil)
//            self.present(playerViewController, animated: true) {
//                playerViewController.player!.play()
//            }
            
        default:break
        }
    }
    
    
    @objc  func didTappedOnBackgroundView(){
        self.backview.isHidden = true
        self.mySticker.isHidden = true
        //   self.saveBtn.isHidden = false
    }
    
    //colletionview
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if(self.stickerCat.count > 0){
            return self.stickerCat[selectedStickerIndexPath].stickers.count
        }else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "stickerHeader", for: indexPath) as! stickerHeaderView
        var allwidth = 10
        for i in 0..<stickerCat.count{
            let imageFrame = CGRect(x: allwidth, y: 5, width: 40, height: 40)
            let stickerBtn = UIButton(frame: imageFrame)
            let url = URL(string: "\(SettingsViewController.fileurl)\(self.stickerCat[i].thumb)")
            stickerBtn.sd_setImage(with: url, for: .normal, completed: nil)
            stickerBtn.imageView?.contentMode = .scaleAspectFit
            allwidth = allwidth + 40 + 10;
            stickerBtn.tag = i
            stickerBtn.addTarget(self, action: #selector(stickerChoosen), for: .touchUpInside)
            header.stickerTabScrollview.addSubview(stickerBtn)
        }
        header.stickerTabScrollview.backgroundColor = GlobalVariables.F5F5F5
        header.stickerTabScrollview.contentSize.width = CGFloat(allwidth)
        return header
        
    }
    
    @objc  func previewSticker(gesture: UILongPressGestureRecognizer){
        if gesture.state == .began {
            
            let p = gesture.location(in: self.collectionView)
            if let indexPath = self.collectionView.indexPathForItem(at: p) {
                // get the cell at indexPath (the one you long pressed)
                _ = self.collectionView.cellForItem(at: indexPath)
                let fileurl = SettingsViewController.fileurl
                let url = URL(string: "\(fileurl)\(stickerCat[selectedStickerIndexPath].stickers[indexPath.row].path)")
                mySticker.sd_setImage(with: url, completed: nil)
                
                UIView.animate(withDuration: 0, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                    self.backview.isHidden = false
                    self.mySticker.isHidden = false
                }) { (completed) in }
                
                // do stuff with the cell
            } else {
                print("couldn't find index path")
            }
        }else if gesture.state == .ended{
            UIView.animate(withDuration: 0.5, delay: 0.4, options: .curveEaseIn, animations: {
                self.mySticker.image = nil
                self.backview.isHidden = true
                self.mySticker.isHidden = true
                
            }, completion: nil)
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        SelectedStickerIndex = indexPath.row
        let stickerpath = self.stickerCat[selectedStickerIndexPath].stickers[indexPath.row].path
        self.composeMessage(type: "sticker", content: stickerpath, filename: "", file_size: 0, duration: 0,name: self.myname)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let fileurl = SettingsViewController.fileurl
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "stickerCell", for: indexPath) as! stickerViewCell
        
        let StickerslongGesture = UILongPressGestureRecognizer(target: self, action: #selector(previewSticker(gesture:)))
        self.collectionView.addGestureRecognizer(StickerslongGesture)
        cell.addGestureRecognizer(StickerslongGesture)
        
        let url = URL(string: "\(fileurl)\(stickerCat[selectedStickerIndexPath].stickers[indexPath.row].path)")
        cell.stickerImage.sd_setImage(with: url, completed: nil)
        return cell
    }
    @objc func stickerChoosen(sender: UIButton){
        selectedStickerIndexPath = sender.tag
        self.collectionView.reloadData()
        self.collectionView.setContentOffset(.zero, animated: true)
    }
    
    //main functions
    @objc func playerDidFinishPlaying(note: NSNotification) {
        self.nowplaying = ""
        self.tableView.reloadData()
    }
    @objc func playAudio(sender: UIButton!) {
        if avplayer.isPlaying {
            self.nowplaying = ""
            avplayer.pause()
            self.tableView.reloadData()
        }else{
            let fileurl = SettingsViewController.fileurl
            let files = sender.accessibilityHint!
            let id = sender.accessibilityValue!
            self.nowplaying = id
            self.tableView.reloadData()
            print("\(fileurl)\(files)")
            if let url = URL(string: "\(fileurl)\(files)") {
                avplayeritem = AVPlayerItem.init(url: url as URL)
                avplayer = AVPlayer.init(playerItem: avplayeritem)
                NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: avplayer.currentItem)
                avplayer.volume = AVAudioSession.sharedInstance().outputVolume
                let session = AVAudioSession.sharedInstance()
                do {
                    try session.overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
                } catch let error as NSError {
                    print("SingleChatController - audioSession error: \(error.localizedDescription)")
                }
                avplayer.play()
            } else {
                print("url nil")
            }
        }
        
        //let url = NSURL(string: "\(fileurl)\(files)")!
    }
    
    func composeMessage(type: String, content: String, filename: String, file_size: Int,duration:Double , name:String)  {
        let timenow = (Date().timeIntervalSince1970) * 1000
        let message = Message.init(id: "ids\(Int(Date().timeIntervalSince1970))", owner_id: myId, content: content, filename:filename, file_size: file_size, type: type, seen: ["\(myId)"], created_at: timenow, duration: duration,conversation_id: senderchatid, name: name)
        let mysms =  Message.init(id: "ids\(Int(Date().timeIntervalSince1970))",owner_id: myId, content: content, filename:filename, file_size: file_size, type: type, seen: ["\(myId)"], created_at: timenow,duration: duration,conversation_id: senderchatid,name: name)
        if type == "video" {
            SingleChatVC.fileurl.append(content)
        }else if type == "photo"{
            SingleChatVC.fileurl.append(content)
        }else if type == "document"{
            SingleChatVC.fileurl.append(content)
        }
        items.sort{ $0.created_at < $1.created_at }
        DispatchQueue.main.async {
            self.items.append(mysms)
            self.tableView.reloadData()
            self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
        }
        var toid = [String]()
        if !toid.contains(SettingsViewController.userId){
            toid.append(SettingsViewController.userId)
        }
        for fcm in selectedgroupUser!{
            toid.append(fcm.fcm_token)
        }
        
        Message.send(message: message, chatid: senderchatid, fcm_token: toid) { (suc, msgid, sentid) in
            if suc == true {
                if let row = self.items.index(where: {$0.id == sentid}) {
                    self.items[row].id = msgid //message successfully sent
                    
                    //                    let idex = IndexPath(row: row,section: 1)
                    //                    tableView.reloadRows(at: [idex!], with: .none )
                    //                    self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                    //                    self.tableView.reloadData()
                }
            }
        }
    }
    
    
    func photoGet(capturedImage: UIImage) {
        Message.uploadimage(chatId: self.senderchatid,image: capturedImage, completion: { (state, imageurl) in
            DispatchQueue.main.async {
                let str = JSON(imageurl)
                let duration = str["duration"].double!
                let content = str["url"].string!
                self.composeMessage(type: "photo", content: content, filename: "", file_size: 0, duration:duration,name:self.myname)
            }
        })
    }
    
    func videoGet(clipedVideo: URL) {
        Message.uploadvideo(chatId: self.senderchatid,video: clipedVideo, completion: { (state, imageurl) in
            DispatchQueue.main.async {
                let str = JSON(imageurl)
                let duration = str["duration"].double!
                let content = str["url"].string!
                //                print("URL",content)
                let filename = str["filename"].string!
                self.composeMessage(type: "video", content: content, filename: filename, file_size: 0, duration: duration,name:self.myname)
            }
        })
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        if let pickedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as? UIImage {
            Message.uploadimage(chatId: self.senderchatid,image: pickedImage, completion: { (state, imageurl) in
                DispatchQueue.main.async {
                    let str = JSON(imageurl)
                    let duration = str["duration"].double!
                    let content = str["url"].string!
                    self.composeMessage(type: "photo", content: content,  filename: "", file_size: 0, duration: duration,name:self.myname)
                }
            })
        }else if let videoURL = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.mediaURL)] as? URL {
            Message.uploadvideo(chatId: self.senderchatid,video: videoURL, completion: { (state, imageurl) in
                DispatchQueue.main.async {
                    let str = JSON(imageurl)
                    let duration = str["duration"].double!
                    let content = str["url"].string!
                    self.composeMessage(type: "video", content: content, filename: "", file_size: 0, duration: duration,name:self.myname)
                }
            })
        }else if let pickedImage1 = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            Message.uploadimage(chatId: self.senderchatid,image: pickedImage1, completion: { (state, imageurl) in
                DispatchQueue.main.async {
                    let str = JSON(imageurl)
                    let duration = str["duration"].double!
                    let content = str["url"].string!
                    self.composeMessage(type: "photo", content: content, filename: "", file_size: 0, duration: duration,name:self.myname)
                }
            })
        }
        picker.dismiss(animated: true, completion: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "cameraview" {
            GlobalVariables.isChat = true
            let camera = segue.destination as? CameraView
            camera?.myProtocol = self
        }else if segue.identifier == "filepreview"{
            let pv = segue.destination as! PageViewController
            pv.sMsgType = sMsgType
            pv.sFileUrl = sFileUrl
            pv.sFileName = sFileName
            pv.urls = SingleChatVC.fileurl
        }else if segue.identifier == "videoCall"{
            
        }
    }
    override func didMove(toParent parent: UIViewController?) {
        if parent == nil {
            self.senderchatid = ""
            self.lastmessageid = ""
            SingleChatVC.fileurl = []
        }
    }
}
extension SingleChatVC : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.left * (4 + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / 5
        return CGSize(width: widthPerItem, height: widthPerItem )
    }
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.bottom
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
    return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
