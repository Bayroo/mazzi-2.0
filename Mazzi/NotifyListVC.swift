//
//  NotifyListVC.swift
//  Mazzi
//
//  Created by Bayara on 3/13/19.
//  Copyright © 2019 Mazz Application. All rights reserved.
//

import Foundation
import UIKit
import Apollo
import KVNProgress


class NotifyListVC:UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    var skip = 0
    var limit = 10
    var myId = SettingsViewController.userId
    var notifyList = [NotificationQuery.Data.Notification]()
    var refresher = UIRefreshControl()
    var dateformatter = DateFormatter()
    var selectedNotifyId = ""
    var selectedIndex = 0
    @IBOutlet weak var notifytableView: UITableView!
    
    let apollo: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken]
        let url = URL(string:SettingsViewController.graphQL)!
        
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.notifytableView.delegate = self
        self.notifytableView.dataSource = self
        self.notifytableView.register(UINib(nibName: "notifyCell", bundle: nil), forCellReuseIdentifier: "notifyCell")
        self.notifytableView.refreshControl = self.refresher
        self.refresher.addTarget(self, action: #selector(reloadData), for: .valueChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        _ = self.apollo.clearCache()
        self.notifyList.removeAll()
        self.getNotifyList()
        self.equilizeBadge()
        self.tabBarItem.badgeValue = nil
    }
    
    @objc func reloadData(){
        self.notifyList.removeAll()
        self.getNotifyList()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = ""
        let turshiltbtn = UIBarButtonItem(image: UIImage(named:"closewhite"), style: .plain, target: self, action: #selector(deleteAllNotify))
        self.navigationController?.navigationBar.topItem?.rightBarButtonItem = turshiltbtn

    }
    
    override func viewWillDisappear(_ animated: Bool) {
       self.navigationController?.navigationBar.topItem?.rightBarButtonItem = nil
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.notifyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notifyCell", for: indexPath) as! notifeCell
        cell.mainLabel.text = "\(self.notifyList[indexPath.row].ownerId!.nickname!.uppercased()) : \(self.notifyList[indexPath.row].content!)"
    
//        print("SENN :: ", self.notifyList[indexPath.row].seen)
        if (self.notifyList[indexPath.row].seen?.contains(self.myId))!{
            cell.mainLabel.font = .systemFont(ofSize: 14)
            cell.dateLabel.font = .systemFont(ofSize: 14)
        }else{
            cell.mainLabel.font = .boldSystemFont(ofSize: 14)
            cell.dateLabel.font = .boldSystemFont(ofSize: 14)
        }
        
        let lastlogin = NSDate(timeIntervalSince1970: (self.notifyList[indexPath.row].createdDate!/1000))
        self.dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let last = self.dateformatter.string(from: lastlogin as Date)
        cell.dateLabel.text = last
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Устгах"
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let selected_con_id = self.notifyList[indexPath.row].id
            let alert = UIAlertController(title: "Мэдэгдэл устгах", message: "Та мэдэгдэл утсгахдаа итгэлтэй байна уу ?", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Тийм", style: .default  , handler: {action in
                self.deleteNotification(notifyId: selected_con_id!)
            }))
            alert.addAction(UIAlertAction(title: "Болих", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.notifyList.count > 0 {
            let lastElement = self.notifyList.count - 1
            if indexPath.row == lastElement {
                self.skip = self.skip + self.limit
                if self.notifyList.count == self.skip {
                    let query = NotificationQuery(userId: self.myId, skip: skip, limit: limit)
                    self.apollo.fetch(query: query) { result, error in
                        if result?.data?.notification != nil {
                            let p = (result?.data?.notification)! as! [NotificationQuery.Data.Notification]
                            //                            self.notifyList.append(contentsOf: p)
                            for i in 0..<p.count{
                                if p[i].classId == GlobalStaticVar.classId{
                                    self.notifyList.append(p[i])
                                }
                            }
                            DispatchQueue.main.async {
                                self.notifytableView.reloadData()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedNotifyId = self.notifyList[indexPath.row].id ?? ""
        self.selectedIndex = indexPath.row
        
          if (self.notifyList[indexPath.row].seen?.contains(self.myId))!{
            GlobalStaticVar.FromNotify = true
            GlobalStaticVar.notifiedType = self.notifyList[self.selectedIndex].type ?? ""
            GlobalStaticVar.notifiedPostId = self.notifyList[self.selectedIndex].postId ?? ""
            GlobalStaticVar.notifiedClassId = self.notifyList[self.selectedIndex].classId ?? ""
            GlobalStaticVar.notifiedRoodPostId = self.notifyList[self.selectedIndex].rootPostId ?? ""
            self.navigationController?.popViewController(animated: false)
          }else{
             self.markNotify()
         }
    }
    
    func getNotifyList(){
        self.skip = 0
        let query = NotificationQuery(userId: self.myId, skip: skip, limit: limit)
        self.apollo.fetch(query: query) { result, error in
            if result?.data?.notification != nil {
                var allList = (result?.data?.notification)! as! [NotificationQuery.Data.Notification]
                for i in 0..<allList.count{
                    if allList[i].classId == GlobalStaticVar.classId{
                        self.notifyList.append(allList[i])
                      
                    }
                }
                self.notifytableView.reloadData()
                self.refresher.endRefreshing()
            }
        }
    }
    
    func deleteNotification(notifyId : String){
        let mutation = DeleteNotificationMutation(userId: self.myId, notificationId: notifyId)
        self.apollo.perform(mutation: mutation) { result, error in
            if result?.data?.deleteNotification?.error == false{
                KVNProgress.showSuccess(withStatus: "Амжилттай устгалаа", on: self.view)
                for i in 0..<self.notifyList.count{
                    if self.notifyList[i].id == notifyId{
                        self.notifyList.remove(at: i)
                        break
                    }
                }
                self.notifytableView.reloadData()
            }else{
                KVNProgress.showError(withStatus: result?.data?.deleteNotification?.message, on: self.view)
            }
        }
    }
    
    func equilizeBadge(){
        let mutation = EquilizeMclassSumNotificationMutation(userId: self.myId , classId: GlobalStaticVar.classId)
        self.apollo.perform(mutation: mutation) { result, error in
            if result?.data?.equilizeMclassSumNotification?.error == false{
                
            }else{
                
            }
        }
    }
    
   @objc func deleteAllNotify(){
    
        if self.notifyList.count > 0 {
            let alert = UIAlertController(title: "Бүх мэдэгдэл устгах", message: "Та бүх мэдэгдлээ устгахдаа итгэлтэй байна уу ?", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Тийм", style: .default  , handler: {action in
                let mutation = DeleteAllNotificationMutation(userId: self.myId, classId: GlobalStaticVar.classId)
                self.apollo.perform(mutation: mutation) { result, error in
                    if result?.data?.deleteAllNotification?.error == false{
                        self.notifyList = []
                        self.notifytableView.reloadData()
                    }else{
                        KVNProgress.showError(withStatus: result?.data?.deleteAllNotification?.message)
                    }
                }
            }))
            alert.addAction(UIAlertAction(title: "Болих", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            KVNProgress.showError(withStatus: "Устгах мэдэгдэл байхгүй байна.")
        }
    }
    
    func markNotify(){
        let mutation = MarkNotificationMutation(userId: self.myId, notifyId: self.selectedNotifyId)
         self.apollo.perform(mutation: mutation) { result, error in
            if result?.data?.markNotification?.error == false{
                GlobalStaticVar.FromNotify = true
                GlobalStaticVar.notifiedType = self.notifyList[self.selectedIndex].type ?? ""
                GlobalStaticVar.notifiedPostId = self.notifyList[self.selectedIndex].postId ?? ""
                GlobalStaticVar.notifiedClassId = self.notifyList[self.selectedIndex].classId ?? ""
                GlobalStaticVar.notifiedRoodPostId = self.notifyList[self.selectedIndex].rootPostId ?? ""
                self.navigationController?.popViewController(animated: false)
            }else{
                KVNProgress.showError(withStatus: result?.data?.markNotification?.message)
            }
        }
    }
}

