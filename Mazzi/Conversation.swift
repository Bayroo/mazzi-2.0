//
//  model.swift
//  Mazzi App LLCChatTestV
//
//  Created by Janibekm on 8/17/17.
//  Copyright © 2017 Janibekm. All rights reserved.
//

import Foundation
import SocketIO
import SwiftyJSON
import Alamofire
import RealmSwift
import SSZipArchive
import KVNProgress

let userDefaults = UserDefaults.standard
var fromID = ""

class Conversation:NSObject {
    
    let user: [User_group]
    let lastmessage: Message
    var id:String = "";
    var created_at:Double = 0
    var is_thread:Bool
    var show = [String]()
    var secret:Bool
    var title:String = ""
    var image: String = ""
    var conversation_id: String = ""
    var owner_id_con:String = ""
    
    class func getconversations( userid: String, limit:Int,skip: Int, completion: @escaping (Conversation) -> Swift.Void) {
        let baseurl = SettingsViewController.baseurl
        let myid = SettingsViewController.userId
        // let headers = SettingsViewController.headers
        let token = UserDefaults.standard.object(forKey:"token") as! String
        let  header = ["x-access-token": token, "authorization": "BPXjEvWfXxx5qpQHHFMEJEB8fA3xAczC"]
        Alamofire.request("\(baseurl)conversations", method: .post, parameters: ["user_id":myid,"limit":limit, "skip":skip], encoding: JSONEncoding.default, headers: header).responseJSON{ response in
            if response.response?.statusCode == 200 {
                let getConList = JSON(response.value!)
//                print("conList:" , getConList)
                if(getConList.count != 0){
                    for i in 0..<getConList.count{
                        let show = getConList[i]["show"].arrayValue.map({$0.stringValue})
                        if show.contains(myid){
                            let id = getConList[i]["_id"].string ?? ""
                            let created_at = getConList[i]["created_at"].double!
                            let is_thread = getConList[i]["is_thread"].bool!
                            let secret = getConList[i]["secret"].bool!
                            let image = getConList[i]["image"].string ?? ""
                            let lastMsg = getConList[i]["msg"]["_id"]
                            let title = getConList[i]["title"].string!
                            let name = getConList[i]["name"].string ?? ""
                            let owner_id_con = getConList[i]["owner_id"].string ?? ""
                            var emptyMessage = Message.init(id: "", owner_id: "", content: "", filename: "", file_size: 0, type: "", seen: [""], created_at: 0, duration: 0,conversation_id: "",name: "")
                            if lastMsg.exists(){
                                let lastMsgid = getConList[i]["msg"]["_id"].string!
                                let lastMsgcontent = getConList[i]["msg"]["content"].string ?? "" 
                                let lastMsgfilename = getConList[i]["msg"]["filename"] == JSON.null  ? "" : getConList[i]["msg"]["filename"].string!
                                let lastMsgfile_size = getConList[i]["msg"]["file_size"] == JSON.null  ? 0 : getConList[i]["msg"]["file_size"].int!
                                let lastMsgseen = getConList[i]["msg"]["seen"].arrayValue.map({$0.stringValue})
                                let lastMsgtype = getConList[i]["msg"]["type"].string!
                                let owner_id = getConList[i]["msg"]["owner_id"].string!
                                let lastMsgcreated_at = getConList[i]["msg"]["created_at"].double!
                                let duration = getConList[i]["msg"]["duration"].double!
                                let conversation_id = getConList[i]["msg"]["conversation_id"].string!
                                emptyMessage = Message.init(id: lastMsgid, owner_id: owner_id, content: lastMsgcontent, filename: lastMsgfilename, file_size: lastMsgfile_size,  type: lastMsgtype, seen: lastMsgseen,  created_at: lastMsgcreated_at, duration: duration,conversation_id:conversation_id , name:name)
                            }
                            var usersArray = [User_group]()
                            let info = getConList[i]["participantsinfo"]
                            
                            for i in 0..<info.count {
                                
                                let usergroup = User_group.init(id: info[i]["_id"].string ?? "", image: info[i]["image"].string ?? "", nickname: info[i]["nickname"].string ?? "", lastname: info[i]["lastname"].string ?? "", surname: "", phone: info[i]["phone"].string ?? "", state: info[i]["state"].int ?? 0, online_at: Double(info[i]["online_at"].int ?? 0), fcm_token: info[i]["fcm_token"].string ?? "",registerId: info[i]["registerId"].string ?? "" )
                                usersArray.append(usergroup)
                            }
                            if info.count != 0 {
                                let conversation = Conversation.init(user: usersArray, lastmessage: emptyMessage, id: id, title: title, created_at: created_at, is_thread: is_thread, show: show, secret: secret, image: image,owner_id_con: owner_id_con )
                               // print("conversation:  ",conversation)
                                RealmService.createConversationToRealm(createConversation: conversation, completion: { (success) in
                                    if success == true {
//                                        print("CONS WRITE TO REALMS")
                                        completion(conversation)
                                    }
                                })
                            }
                        }else{
                            let id = getConList[i]["_id"].string ?? ""
                            if id != ""{
                                RealmService.deleteConversationFromRealm(deleteConversation: id, completion: { (succ) in
                                })
                            }
                        }
                    }
                }
            }
        }
    }
    class func get_singleCon( con_id: String, completion: @escaping (Conversation) -> Swift.Void) {
        let baseurl = SettingsViewController.baseurl
        let myid = SettingsViewController.userId
        // let headers = SettingsViewController.headers
        
        let token = UserDefaults.standard.object(forKey:"token") as! String
        let  header = ["x-access-token": token, "authorization": "BPXjEvWfXxx5qpQHHFMEJEB8fA3xAczC"]
        
        Alamofire.request("\(baseurl)get_conversation", method: .post, parameters: ["_id":con_id, "user_id":myid], encoding: JSONEncoding.default, headers: header).responseJSON{ response in
            print("header:",header)
            print("con_id:",con_id)
            print("myid:",myid)
            
            if response.response?.statusCode == 200 {
                print(response)
                let getConList = JSON(response.value!)
                
                let show = getConList["show"].arrayValue.map({$0.stringValue})
//                print("show:",show)
                if show.contains(myid){
                    let id = getConList["_id"].string!
                    let created_at = getConList["created_at"].double!
                    let is_thread = getConList["is_thread"].bool!
                    let secret = getConList["secret"].bool!
                    let image = getConList["image"].string!
                    let lastMsg = getConList["msg"]["_id"]
                    let title = getConList["title"].string!
//                    let owner_id_con = getConList["owner_id"].string!
                    var emptyMessage = Message.init(id: "", owner_id:"", content: "", filename: "", file_size: 0, type: "", seen: [""], created_at: 0, duration: 0, conversation_id: "", name:"")
                    if lastMsg.exists(){
                        let lastMsgid = getConList["msg"]["_id"].string!
                        let lastMsgcontent = getConList["msg"]["content"].string!
                        let lastMsgfilename = getConList["msg"]["filename"].string!
                        let lastMsgfile_size = getConList["msg"]["file_size"].int!
                        let lastMsgseen = getConList["msg"]["seen"].arrayValue.map({$0.stringValue})
                        let lastMsgtype = getConList["msg"]["type"].string!
                        let owner_id = getConList["msg"]["owner_id"].string!
                        let lastMsgcreated_at = getConList["msg"]["created_at"].double!
                        let duration = getConList["msg"]["duration"].double!
                        let conversation_id = getConList["msg"]["conversation_id"].string!
                        let name = getConList["msg"]["name"].string ?? ""
                        emptyMessage = Message.init(id: lastMsgid, owner_id: owner_id, content: lastMsgcontent, filename: lastMsgfilename, file_size: lastMsgfile_size, type: lastMsgtype, seen: lastMsgseen,  created_at: lastMsgcreated_at, duration: duration, conversation_id: conversation_id,name:name)
                    }
                    var usersArray = [User_group]()
                    let info = getConList["participantsinfo"]
                    
//                    print("info:",info)
                    for i in 0..<info.count {
                        let usergroup = User_group.init(id: info[i]["_id"].string!, image: info[i]["image"].string!, nickname: info[i]["nickname"].string!, lastname: info[i]["lastname"].string!, surname: "", phone: info[i]["phone"].string!, state: info[i]["state"].int!, online_at: Double(info[i]["online_at"].int!), fcm_token: info[i]["fcm_token"].string!, registerId: info[i]["registerId"].string! )
                        usersArray.append(usergroup)
                    }
                    if info.count != 0 {
                        print("here is may be bug")
                        let conversation = Conversation.init(user: usersArray, lastmessage: emptyMessage, id: id, title: title, created_at: created_at, is_thread: is_thread, show: show, secret: secret, image: image,owner_id_con:"")
                        RealmService.createConversationToRealm(createConversation: conversation, completion: { (success) in
                            if success == true {
                                completion(conversation)
                            }
                        })
                    }
                }
            }
        }
    }
    class func leaveConversation(conversation_id: String, completion: @escaping (Bool) -> Swift.Void) {
        let baseurl = SettingsViewController.baseurl
        let myid = SettingsViewController.userId
        //  let headers = SettingsViewController.headers
        
        let token = UserDefaults.standard.object(forKey:"token") as! String
        let  header = ["x-access-token": token, "authorization": "BPXjEvWfXxx5qpQHHFMEJEB8fA3xAczC"]
        
        Alamofire.request("\(baseurl)leave_conversation", method: .post, parameters: ["conversation_id":conversation_id, "user_id":myid], encoding: JSONEncoding.default, headers: header).responseJSON{ response in
            if response.response?.statusCode == 200 {
                let getConList = JSON(response.value!)
                let data = JSON(getConList)
                if data["success"].bool! == true {
                    completion(true)
                }else{
                    completion(false)
                }
            }
        }
    }
    
    init(user: [User_group], lastmessage: Message,id:String, title:String, created_at:Double, is_thread: Bool, show: [String], secret: Bool, image:String,owner_id_con:String) {
        self.user = user
        self.lastmessage = lastmessage
        self.id = id
        self.title = title
        self.created_at = created_at
        self.is_thread = is_thread
        self.show = show
        self.secret = secret
        self.image = image
        self.owner_id_con = owner_id_con
    }
}

class StickersCat:NSObject{
    var _id:String
    var name:String = ""
    var price:Int = 0
    var thumb:String = ""
    var time:Double = 0
    var stickers = [newStickers]()
    let baseurl = SettingsViewController.baseurl
    let myid = SettingsViewController.userId
    
    class func getStickerFromServer( userid: String, completion: @escaping (Bool,StickersCat) -> Swift.Void) {
//        print("getStickerFromServer")
        let baseurl = SettingsViewController.baseurl
        let myid = SettingsViewController.userId
        let token = UserDefaults.standard.object(forKey:"token") as! String
        let header = ["x-access-token": token, "authorization": "BPXjEvWfXxx5qpQHHFMEJEB8fA3xAczC"]
        
        Alamofire.request("\(baseurl)stickers", method: .post, parameters: ["userid":myid], encoding: JSONEncoding.default, headers: header).responseJSON{ response in
            if response.response?.statusCode == 200 {
                let stickerList = JSON(response.value!)
                let list = stickerList["result"]
                for i in 0..<list.count{
                    let id = list[i]["_id"].string!
                    let name = list[i]["name"].string!
                    let price = list[i]["price"].int!
                    let thumb = list[i]["thumb"].string!
                    let stick = list[i]["stickers"].arrayValue
                    var sticks = [newStickers]()
                    for s in 0..<stick.count{
                        let id = stick[s]["_id"].string!
                        let name = stick[s]["name"].string!
                        let path = stick[s]["path"].string!
                        let categoryId = stick[s]["categoryId"].string!
                        let is_active = stick[s]["is_active"].bool
                        
                        let stickerss = newStickers.init(_id: id, name: name, path: path, categoryId: categoryId, is_active: is_active!)
                        sticks.append(stickerss)
                    }
                  let stickCats = StickersCat.init(_id: id, name: name, price: price, thumb: thumb, stickers:sticks)
                     completion(false,stickCats)
                }
            }
        }
    }
    
    init(_id:String, name:String,price:Int, thumb:String, stickers:[newStickers]){
        self._id = _id
        self.name = name
        self.price = price
        self.thumb = thumb
        self.stickers = stickers
    }
    
}

class newStickers:NSObject{
    var _id:String = ""
    var name:String = ""
    var path:String = ""
    var categoryId:String = ""
    var is_active:Bool = true
    init(_id:String, name:String, path:String, categoryId:String, is_active:Bool){
        self._id = _id
        self.name = name
        self.path = path
        self.categoryId = categoryId
        self.is_active = is_active
    }
}

