//
//  ActiveLaunchVC.swift
//  Mazzi
//
//  Created by Bayara on 12/14/18.
//  Copyright © 2018 Mazzi Application. All rights reserved.
//

import Foundation
import KVNProgress
import Apollo
import SwiftyJSON
import SocketIO

class ActiveLaunchVC:UIViewController,UITableViewDelegate,UITableViewDataSource{
   
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var roomCode: UILabel!
    var isFinish = false
    var launchResult = [DisableLaunchMutation.Data.DisableLaunch]()
    var allAnswers = [StudentAnswersQuery.Data.StudentAnswer]()
    var roomName = ""
    var launchId = ""
    var teacherId = ""
    var cdiotoken = GlobalVariables.headerToken
    var joinedUsers = [joinesUsers]()
    var isstart = false
    var setId = ""
    var classId = ""
    var average = ""
    var AnsweredStudents = [joinesUsers]()
    var startedTime = 0.0
    var Alltime = 0.0
    var seconds = 60
    var timer = Timer()
 
    let apollo: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        // Add additional headers as needed
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
//        print("TOKEN APOLLOO:: ", GlobalVariables.headerToken)
        let url = URL(string:SettingsViewController.graphQL)!
        
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    static var manager  = SocketManager(socketURL: URL(string: SettingsViewController.cdiosocketUrl)!, config: [ .compress,.connectParams(["userId" : GlobalVariables.user_id, "token":GlobalVariables.headerToken]) ,.secure(false) ,.forceNew(true),.log(false) ,.forceWebsockets(true),.path("/socket/socket.io")])
    var launchsocket = manager.defaultSocket
    
    @IBOutlet weak var onAndOffSwitch: UISwitch!
    @IBOutlet weak var maTableView: UITableView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customize()
        self.classId = GlobalStaticVar.classId
        self.headerView.backgroundColor = GlobalVariables.F5F5F5
    }
    
    func customize(){
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "05"), style: .plain, target: self, action: #selector(back))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "logout"), style: .plain, target: self, action: #selector(closeLaunch))
        self.title = ""
        print("roomCODE:: ", self.roomName)
        self.roomCode.text = "\("Шалгалтын код:") \(self.roomName)"
        self.maTableView.delegate = self
        self.maTableView.dataSource = self
        self.maTableView.register(UINib(nibName: "LaunchStudentsCell", bundle: nil), forCellReuseIdentifier: "launch")
        onAndOffSwitch.isOn = self.isstart
    }
    
    func runTimer(){
       self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer(){
         if GlobalStaticVar.isEnd == false {
            seconds -= 1
            var ti = NSInteger(self.Alltime)
            ti -= 1000
            self.Alltime = Double(ti)
            if self.Alltime == 0.0{
                self.Alltime = 0.0
                ti = 0
                KVNProgress.showSuccess(withStatus: "Шалгалтын цаг дууслаа!")
//                KVNProgress.show(withStatus: "Шалгалтын цаг дууслаа!", on: self.view)
                self.stopTimerTest()
            }
            self.timeLabel.text = self.timeString(time: TimeInterval(ti))
        }
    }
    
    func stopTimerTest() {
        self.timer.invalidate()
        if GlobalStaticVar.isEnd == false {
             self.endLaunch()
        }
    }
    
    func timeString(time: TimeInterval)->String {
//        let hours = Int(time) / 3600000
        let minutes = Int(time) % 3600000 / 60000
        let seconds = Int(time) % 60000 / 1000
        return String(format:"%02i : %02i",minutes, seconds)
    }
    
    @objc func closeLaunch(){
        let alert = UIAlertController(title: "", message: "Та шалгалтыг дуусгахдаа итгэлтэй байна уу", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Тийм", style: .default  , handler: {action in
             print("CLOSE ENDLAUNCH")
            self.endLaunch()
        }))
        alert.addAction(UIAlertAction(title: "Үгүй", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func back(){
        print("OBOLL;;", self.isFinish)
        if  self.isFinish == true {
            let when = DispatchTime.now()
            DispatchQueue.main.asyncAfter(deadline: when + 2) {
                _ = self.navigationController?.popViewController(animated: true)
            }
        }else{
            let alert = UIAlertController(title: "", message: "Та шалгалтаас түр гарахдаа итгэлтэй байна уу", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Тийм", style: .default  , handler: {action in
                GlobalStaticVar.isLaunchEnd = true
                self.navigationController?.popViewController(animated: false)
            }))
            alert.addAction(UIAlertAction(title: "Үгүй", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
      
    }
    @IBAction func onOff(_ sender: Any) {
        if self.onAndOffSwitch.isOn == false {
            print("SWITCH ENDLAUNCH")
            self.endLaunch()
        }else{
            self.launchsocket.emit("teacher_launch",["key":self.roomName, "classId":self.classId])
            KVNProgress.showSuccess(withStatus: "Шалгалт нээгдлээ")
            if self.Alltime != 0{
                let currentDate = Date()
                let nowDouble = currentDate.timeIntervalSince1970
                let currentdate = nowDouble * 1000
                self.Alltime = (currentdate + Alltime) - currentdate
                print("runtimer for switch")
                self.runTimer()
            }else{
            }
        }
    }
    
    func initSocket(){
      
        self.launchsocket.on("connect") { ( dataArray, ack) -> Void in
             KVNProgress.dismiss()
            self.launchsocket.emit("teacher_join_room",["key":self.roomName , "classId":self.classId])
            print("ACTIVE LAUNCH SOCKET CONNECTED")
        }

        self.launchsocket.on("teacher_join_room_response") {(data,ack) -> Void in
            print("teacher_join_room_response:: ", data)
            let json = JSON(data)
            let st = json[0]["students"].arrayValue
       
            if st.count > 0 {
                for i in 0..<st.count{
                    if self.joinedUsers.count > 0 {
                        if !self.joinedUsers.contains(where:{$0.userId == st[i]["_id"].string ?? ""}){
                            let join = joinesUsers.init(userId: st[i]["_id"].string ?? "", nickname:  st[i]["nickname"].string ?? "", lastname:  st[i]["lastname"].string ?? "", average: "")
                            self.joinedUsers.append(join)
                            self.maTableView.reloadData()
                        }
                    }else{
                        let join = joinesUsers.init(userId: st[i]["_id"].string ?? "", nickname:  st[i]["nickname"].string ?? "", lastname:  st[i]["lastname"].string ?? "", average: "")
                        self.joinedUsers.append(join)
                        self.maTableView.reloadData()
                    }
                }
            }else{
                
            }
        }
        
        self.launchsocket.on("statusChange"){(data,ack) in
            print("LAUNCH SOCKET STATUSCHANGE: ", data)
        }
        
        self.launchsocket.on("teacher_recieve_answer"){(data,ack) -> Void in
            let json = JSON(data)
            print("teacher_recieve_answer :: ", json)
            let id = json[0]["studentId"].stringValue
            self.getAnswer(studentId: id)
        }
        
        self.launchsocket.on("teacher_student_joined"){(data,ack) -> Void in
            print("teacher_student_joined ::: ", data)
            let json = JSON(data)
//            print("JoinedUser: ", json)
            
            if self.joinedUsers.count > 0 {
                if !self.joinedUsers.contains(where:{$0.userId == json[0]["_id"].string ?? ""}){
                    let join = joinesUsers.init(userId: json[0]["_id"].string ?? "", nickname:  json[0]["nickname"].string ?? "", lastname:  json[0]["lastname"].string ?? "", average: "")
                    self.joinedUsers.append(join)
                    self.maTableView.reloadData()
                }
            }else{
                    let join = joinesUsers.init(userId: json[0]["_id"].string ?? "", nickname:  json[0]["nickname"].string ?? "", lastname:  json[0]["lastname"].string ?? "", average: "")
                    self.joinedUsers.append(join)
                    self.maTableView.reloadData()
            }
        }
        
        self.launchsocket.on("teacher_student_leave"){(data,ack) ->Void in
//            print("LEAVE: ", data)
            let json = JSON(data)
            if self.joinedUsers.contains(where:{$0.userId == json[0]["id"].string ?? ""}){
                let ind = self.joinedUsers.index(where:{$0.userId == json[0]["id"].string ?? ""})

                self.joinedUsers.remove(at: ind!)
                self.maTableView.reloadData()
            }
        }
        
        self.launchsocket.on(clientEvent: .disconnect) { (data, SocketAckEmitter) in
            print("SOCKET DISCONECTED")
        }
        self.launchsocket.connect()  
       
    }

    func getAnswer(studentId: String){
        let query = SingleStudentAnswerQuery(teacherId: self.teacherId, launchId: self.launchId, studentId: studentId, questionSetId:self.setId )
         apollo.fetch(query: query) { result, error in
//            print("res:: ",result as Any)
            if result?.data?.singleStudentAnswers != nil {
//                print("AVERAGE:: ",result?.data?.singleStudentAnswers as Any)
                self.average = String(format:"%0.2f", (result?.data?.singleStudentAnswers?.average!)!)
                
                for i in 0..<self.joinedUsers.count {
                    if self.joinedUsers[i].userId == studentId{
                        let nick = self.joinedUsers[i].nickname
                        let last = self.joinedUsers[i].lastname
                        let gg = joinesUsers.init(userId: studentId, nickname: nick ,lastname: last, average: self.average)
                       
                        self.AnsweredStudents.append(gg)
                        self.maTableView.reloadData()
                    }
                }
            }
        }
    }
    
    func getAnswers(){
        let query = StudentAnswersQuery(teacherId: self.teacherId, launchId: self.launchId, setId: self.setId)
         apollo.fetch(query: query) { result, error in
            if result?.data?.studentAnswers != nil {
                self.allAnswers = (result?.data?.studentAnswers)! as! [StudentAnswersQuery.Data.StudentAnswer]
                self.maTableView.reloadData()
            }else{
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
     override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         GlobalStaticVar.isEnd = false
        self.initSocket()
        if self.Alltime != 0 && self.startedTime > 0{
            let currentDate = Date()
            let nowDouble = currentDate.timeIntervalSince1970
            let currentdate = nowDouble * 1000
            self.Alltime = (self.startedTime + Alltime) - currentdate
            if self.Alltime < 100.0{
                let alert = UIAlertController(title: "", message: "Шалгалтын цаг дууссан байна!", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "За", style: .default  , handler: {action in
                    self.navigationController?.popViewController(animated: false)
                }))
                
                self.present(alert, animated: true, completion: nil)
            }else{
                print("runtimer for didload")
                self.runTimer()
            }
        }
        self.getAnswers()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let title : UILabel = UILabel()
            title.textColor = GlobalVariables.purple
         if section == 1{
            title.textColor = GlobalVariables.purple
            if self.AnsweredStudents.count == 0{
                 title.text =  ""
            }else{
                title.text =  "Хариултаа илгээсэн оюутнууд".uppercased()
            }
            
        }
        return title.text
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }else{
            return 25.0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.white
        //        headerView.backgroundColor = GlobalVariables.todpurple.withAlphaComponent(0.8)
        let headerLabel = UILabel()
        if section == 0{
            headerLabel.frame = CGRect(x: 16, y: 5, width:
                tableView.bounds.size.width, height: 0)
        }else{
            headerLabel.frame = CGRect(x: 16, y: 5, width:
                tableView.bounds.size.width, height: tableView.bounds.size.height-5)
        }
        headerLabel.font = UIFont.boldSystemFont(ofSize: 13)
        headerLabel.textColor = GlobalVariables.purple
        headerLabel.text = self.tableView(self.maTableView, titleForHeaderInSection: section)
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        _ = self.apollo.clearCache()
        self.launchsocket.disconnect()
        self.launchsocket.removeAllHandlers()
    }
    
   func endLaunch() {

        let mutation = DisableLaunchMutation.init(launchId: self.launchId, teacherId: self.teacherId, classId: self.classId)
        apollo.perform(mutation: mutation) { result, error in
            if result?.data?.disableLaunch != nil {
                if  result?.data?.disableLaunch?.launchId != nil {
                      KVNProgress.showSuccess(withStatus: "Шалгалтын цаг дууслаа.")
//                      self.onAndOffSwitch.isEnabled = false
//                      self.navigationItem.rightBarButtonItem?.isEnabled = false
                      self.isFinish = true
                      GlobalStaticVar.isEnd = true
                      GlobalStaticVar.isLaunchEnd = true
                    
//                    self.navigationController?.popToRootViewController(animated: false)
                        self.back()
 
                }else{
                    if self.isFinish == false{
                        KVNProgress.showError(withStatus: "Шалгалт хаахад алдаа гарлаа. Дахин оролдоно уу")
                    }
                }
            }else{
                if self.isFinish == false{
                    KVNProgress.showError(withStatus: "Шалгалт хаахад алдаа гарлаа. Дахин оролдоно уу")
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
              return self.joinedUsers.count
        }else{
            if self.AnsweredStudents.count > 0{
                 return self.AnsweredStudents.count
            }else{
                 return self.allAnswers.count
            }
            
        }
    
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            let cell = self.maTableView.dequeueReusableCell(withIdentifier: "launch") as! LaunchStudentsCell
            if indexPath.row % 2 == 0 {
                cell.contentView.backgroundColor = GlobalVariables.F5F5F5
            }else{
                cell.contentView.backgroundColor = UIColor.white
            }
            cell.countLabel.textColor = UIColor.darkGray
            cell.pointView.layer.cornerRadius = 5
            cell.pointView.layer.masksToBounds = true
            cell.nameLabel.text = self.joinedUsers[indexPath.row].lastname
            cell.lastnameLabel.text = self.joinedUsers[indexPath.row].nickname
            cell.countLabel.text = "\(String(indexPath.row + 1))\(".")"
             cell.pointView.isHidden = true
            return cell
        }else{
            
            if self.AnsweredStudents.count > 0 {
                let cell = self.maTableView.dequeueReusableCell(withIdentifier: "launch") as! LaunchStudentsCell
                if indexPath.row % 2 == 0 {
                    cell.contentView.backgroundColor = GlobalVariables.F5F5F5
                }else{
                    cell.contentView.backgroundColor = UIColor.white
                }
                cell.countLabel.textColor = UIColor.darkGray
                cell.pointView.layer.cornerRadius = 5
                cell.pointView.layer.masksToBounds = true
                cell.nameLabel.text = self.AnsweredStudents[indexPath.row].lastname
                cell.lastnameLabel.text = self.AnsweredStudents[indexPath.row].nickname
                cell.countLabel.text = "\(String(indexPath.row + 1))\(".")"
                cell.pointView.isHidden = false
                
                //            let b:String = String(format:"%0.2f", self.AnsweredStudents[indexPath.row].average)
                let myInt2 = (self.AnsweredStudents[indexPath.row].average as NSString).integerValue
                
                if myInt2 > 80 {
                    cell.pointLabel.textColor = GlobalVariables.green
                }else if myInt2 < 80 &&  myInt2 > 60 {
                    cell.pointLabel.textColor = UIColor.orange
                }else if myInt2 < 60{
                    cell.pointLabel.textColor = UIColor.red
                }
                
                cell.pointLabel.text = "\(self.AnsweredStudents[indexPath.row].average)\("%")"
                return cell
            }else{
                let cell = self.maTableView.dequeueReusableCell(withIdentifier: "launch") as! LaunchStudentsCell
                if indexPath.row % 2 == 0 {
                    cell.contentView.backgroundColor = GlobalVariables.F5F5F5
                }else{
                    cell.contentView.backgroundColor = UIColor.white
                }
                cell.countLabel.textColor = UIColor.darkGray
                cell.pointView.layer.cornerRadius = 5
                cell.pointView.layer.masksToBounds = true
                cell.nameLabel.text = self.allAnswers[indexPath.row].lastname
                cell.lastnameLabel.text = self.allAnswers[indexPath.row].nickname
                cell.countLabel.text = "\(String(indexPath.row + 1))\(".")"
                cell.pointView.isHidden = false
                
                //            let b:String = String(format:"%0.2f", self.AnsweredStudents[indexPath.row].average)
                let myInt2 = Int(self.allAnswers[indexPath.row].average!)
                
                if myInt2 > 80 {
                    cell.pointLabel.textColor = GlobalVariables.green
                }else if myInt2 < 80 &&  myInt2 > 60 {
                    cell.pointLabel.textColor = UIColor.orange
                }else if myInt2 < 60{
                    cell.pointLabel.textColor = UIColor.red
                }
                let f = self.allAnswers[indexPath.row].average!
                let s = NSString(format: "%.2f", f)
                cell.pointLabel.text =  "\(s)%"
                return cell
            }
        }
    }
}
