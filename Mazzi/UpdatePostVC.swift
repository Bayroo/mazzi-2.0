//
//  UpdatePost.swift
//  Mazzi
//
//  Created by Bayara on 11/27/18.
//  Copyright © 2018 Mazzi Application. All rights reserved.
//

import Foundation
import KVNProgress
import Photos
import MobileCoreServices
import WebKit
import AVFoundation
import Alamofire
import SwiftyJSON
import Apollo
//import PopoverSwift

class UpdatePostVC:UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIDocumentPickerDelegate,UINavigationControllerDelegate,UIDocumentMenuDelegate,UIImagePickerControllerDelegate, UITextViewDelegate,UIPopoverControllerDelegate{
    
    enum AttachmentType: String{
        case camera, video, photoLibrary
    }
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var progressLabel: UILabel!
    var isPostedClicked = false
    var selectedTopicId = ""
    var selectedTopicName = ""
    var selectedImages = [UIImage]()
    var files = [SinglepostQuery.Data.Post.File]()
    var content  = ""
    var tapGesture : UITapGestureRecognizer!
    var classId = GlobalStaticVar.classId
    var postId = ""
    var SelectedFileUrl = [URL]()
    var selectedAllFilesURL = [URL]()
    var imagePickedBlock: ((UIImage) -> Void)?
    var videoPickedBlock: ((NSURL) -> Void)?
    var filePickedBlock: ((URL) -> Void)?
    var posterNickname = ""
    var links = [String]()
    var path = ""
    var fileInput = [InputPostFile]()
    var selectedimageName = [String]()
    var posterLastname = ""
    var postedDate : Double!
    var posterImg = ""
    var fileType = ""
    var btn = UIButton()
    var dateformatter = DateFormatter()
    var teacherId = ""
    var allfileCount = 0
    var isImage = false
    //jani
    var textHeightConstraint: NSLayoutConstraint?
    
    var blackView = UIView()
    var linkView = UIView()
    var textfield = UITextField()
    var cancelBtn = UIButton()
    var addLinkBtn = UIButton()
    var label = UILabel()
    var exampleLabel = UILabel()
    
    var topicBlackView = UIView()
    var topicView = UIView()
    var topicTableView = UITableView()
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerLable: UILabel!
    @IBOutlet weak var attachBt: UIButton!
    @IBOutlet weak var tablview: UITableView!
    var topicss = [ObjectTopicsQuery.Data.ObjectTopic.Result]()
    var myId = SettingsViewController.userId
    let apollo: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
        let url = URL(string:SettingsViewController.graphQL)!
        
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customize()
        self.getSubUser()
        self.linkviewInit()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.isPostedClicked = false
        if GlobalStaticVar.classRole != "Student"{
            self.topicViewInit()
            self.getTopics()
        }else{
            self.selectedTopicId = "5ca2b669a861d443508b75e9"
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.selectedTopicId = ""
         self.isPostedClicked = false
    }
    
    func customize(){
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(backtoroot))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:"whiteAttachment"), style: .plain, target: self, action: #selector(chooseFile))
        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.view.addGestureRecognizer(self.tapGesture)
        //        self.attachBt.addTarget(self, action: #selector(chooseFile), for: .touchUpInside)
        if GlobalStaticVar.isPostCreate == true{
            self.title = "Нийтлэл оруулах"
        }else{
            self.title = "Нийтлэл засах"
        }
        
        self.tablview.delegate = self
        self.tablview.dataSource = self
        
        self.tablview.register((UINib(nibName: "FilesCell", bundle: nil)), forCellReuseIdentifier: "filecell")
        self.tablview.register((UINib(nibName: "UpdatePostCell", bundle: nil)), forCellReuseIdentifier: "updatePost")
        self.tablview.register(UINib(nibName: "btnCell", bundle: nil), forCellReuseIdentifier: "btncell")
        tablview.estimatedRowHeight = 44.0
        tablview.rowHeight = UITableView.automaticDimension
        
        //        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        self.headerView.frame = CGRect(x:0, y:0 , width: self.view.bounds.width, height: 65)
        self.headerView.backgroundColor = GlobalVariables.todpurple
    }
    
    
    func linkviewInit(){
        self.blackView.frame = CGRect(x:0 , y:0, width:self.view.bounds.width, height: self.view.bounds.height)
        self.blackView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        
        self.linkView.frame = CGRect(x:self.blackView.bounds.width/2 - 140 , y:150, width:280, height: 200)
        self.linkView.backgroundColor = UIColor.white
        self.label.frame = CGRect(x: 16, y: 16,width:140,height: 25)
        self.textfield.frame = CGRect(x: 16, y: 49,width: 248 , height: 40)
        self.cancelBtn.frame = CGRect(x: 30, y:120, width:100,height:40)
        self.addLinkBtn.frame = CGRect(x: 150, y:120, width:100,height:40)
        self.exampleLabel.frame = CGRect(x:20, y:165, width:240, height: 35)
        
        label.font = GlobalVariables.customFont14
        self.label.text = "Холбоос оруулах"
        self.cancelBtn.setTitle("Болих", for: UIControl.State.normal)
        self.addLinkBtn.setTitle("Оруулах", for: UIControl.State.normal)
        
        self.cancelBtn.addTarget(self, action: #selector(cancelLink), for: .touchUpInside)
        self.addLinkBtn.addTarget(self, action: #selector(addlink), for: .touchUpInside)
        self.cancelBtn.layer.cornerRadius = 5
        self.cancelBtn.backgroundColor = GlobalVariables.purple
        self.cancelBtn.setTitleColor(UIColor.white, for: .normal)
        self.addLinkBtn.layer.cornerRadius = 5
        self.addLinkBtn.backgroundColor = GlobalVariables.todpurple.withAlphaComponent(0.5)
        self.addLinkBtn.setTitleColor(UIColor.white, for: .normal)
        self.textfield.layer.borderColor = GlobalVariables.purple.cgColor
        self.textfield.layer.borderWidth = 1
        self.textfield.borderStyle = .roundedRect
        self.textfield.layer.cornerRadius = 5
        self.textfield.textContentType = UITextContentType.URL
        self.textfield.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        self.textfield.delegate = self
        self.exampleLabel.numberOfLines = 2
        self.exampleLabel.font = GlobalVariables.customFont10
        self.exampleLabel.textColor = UIColor.red
        self.exampleLabel.textAlignment = .center
        self.exampleLabel.text = "Зөв холбоос оруулах жишээ: https://www.mazzi.mn , http://mazzi.mn"
        
        let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        self.textfield.bounds.inset(by: padding)
        
        self.textfield.autocapitalizationType = .none
        self.textfield.layer.masksToBounds = true
        self.cancelBtn.layer.masksToBounds = true
        self.addLinkBtn.layer.masksToBounds = true
        self.addLinkBtn.isEnabled = false
        
        self.blackView.addSubview(self.linkView)
        self.linkView.addSubview(self.label)
        self.linkView.addSubview(self.textfield)
        self.linkView.addSubview(self.cancelBtn)
        self.linkView.addSubview(self.addLinkBtn)
        self.linkView.addSubview(self.exampleLabel)
        self.view.addSubview(self.blackView)
        self.blackView.isHidden = true
    }
    
    @objc func cancelLink(){
        UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.blackView.isHidden = true
        })
        self.textfield.resignFirstResponder()
    }
    
    @objc func addlink(){
        UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.blackView.isHidden = true
            self.links.append(self.textfield.text!.lowercased())
            self.tablview.reloadData()
            print(self.SelectedFileUrl)
        })
        self.textfield.resignFirstResponder()
    }
    
    @objc func textFieldDidChange(textField:UITextField){
        
        if (textfield.text?.count)! > 0 && (textfield.text?.contains("."))!{
            if (textfield.text?.contains("http://"))! || (textfield.text?.contains("https://"))!{
                self.addLinkBtn.backgroundColor = GlobalVariables.purple
                self.addLinkBtn.setTitleColor(UIColor.white, for: .normal)
                self.addLinkBtn.isEnabled = true
            }
        }
        else{
            self.addLinkBtn.backgroundColor = GlobalVariables.todpurple.withAlphaComponent(0.5)
            self.addLinkBtn.setTitleColor(UIColor.white, for: .normal)
            self.addLinkBtn.isEnabled = false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (range.location == 0 && string == " ") {
            return false
        }else{
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
        if textField == self.textfield{
            self.textfield.resignFirstResponder()
            if self.checkEmpty() == true {
                if GlobalStaticVar.isPostCreate == true{
                    switch self.fileType {
                    case "image":
                        self.postwithFile()
                    case "video":
                        self.postwithFile()
                    case "file":
                        self.postwithFile()
                    default:
                        self.postOnlyContent()
                    }
                }else{
                    self.updatewithFile()
                }
            }else {
                KVNProgress.showError(withStatus: "Хоосон нийтлэл оруулах боломжгүй", on: self.view)
            }
        }
        
        return true
    }
    
    private func textField(_ textField: UITextField, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            self.textfield.resignFirstResponder()
            return false
        }
        return true
    }
    
    @objc func updatePost() {
        if self.isPostedClicked == false{
            if self.checkEmpty() == true {
                self.isPostedClicked = true
                if GlobalStaticVar.isPostCreate == true{
                    switch self.fileType {
                    case "image":
                        self.postwithFile()
                    case "video":
                        self.postwithFile()
                    case "file":
                        self.postwithFile()
                    default:
                        self.postOnlyContent()
                    }
                }else{
                    self.updatewithFile()
                }
            }else {
                self.isPostedClicked = false
                KVNProgress.showError(withStatus: "Хоосон нийтлэл оруулах боломжгүй", on: self.view)
            }
        }else{
            
        }
       
    }
    
    func postOnlyContent(){
        let post = InputPost.init(ownerId: self.myId, content: self.content, link: self.links, file: self.fileInput , topicId: self.selectedTopicId)
        let mutation = InsertPostMutation(InputPost: post, classId: self.classId)
        self.apollo.perform(mutation: mutation) { result, error in
            if result?.data?.insertPost?.error == false{
                self.navigationController?.popViewController(animated: true)
            }else{
                KVNProgress.showError(withStatus: result?.data?.insertPost?.message)
            }
            print(result?.data as Any)
        }
    }
    
    @objc func dismissKeyboard(){
        self.view.endEditing(true)
    }
    
    @IBAction func dismissView(_ sender: Any) {
        
    }
    
    @objc func backtoroot(){
        
        if self.checkEmpty() == true{
            
            let alert = UIAlertController(title: "", message: "Та гарахдаа итгэлтэй байна уу?", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Тийм", style: .default  , handler: {action in
                self.navigationController?.popViewController(animated: true)
                
            }))
            alert.addAction(UIAlertAction(title: "Үгүй", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == self.topicTableView{
            return 1
        }else{
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.white
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section  == 4 {
            return 10
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
        if tableView == self.topicTableView{
            self.selectedTopicId = self.topicss[indexPath.row].id!
            self.selectedTopicName = self.topicss[indexPath.row].name!
            self.topicTableView.reloadData()
            self.tablview.reloadData()
            self.topicBlackView.isHidden = true
            self.view.addGestureRecognizer(self.tapGesture)
        }else{
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.topicTableView {
            return self.topicss.count
        }else{
            if section == 0{
                return 1
            }else if section == 1{
                return self.files.count
            }else if  section == 2{
                return self.links.count
            }
            else if section == 3 {
                return self.selectedAllFilesURL.count
            }else{
                return 1
            }
        }
    }
    
    @objc func chooseTopic(_ sender: UIButton){
        UIView.transition(with: self.blackView, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.view.removeGestureRecognizer(self.tapGesture)
            self.topicBlackView.isHidden = false
        })
    }
    
    func getTopics(){
//        print( self.myId)
//         print( self.classId)
        let query = ObjectTopicsQuery(userId: self.myId, classId: self.classId)
        apollo.fetch(query: query) { resultt, error in
//            print("RESULT:: ", resultt)
                if resultt!.data?.objectTopics?.error == false{
                    self.topicss = (resultt!.data?.objectTopics?.result)! as! [ObjectTopicsQuery.Data.ObjectTopic.Result]
//                    print("TOPICS :: ",self.topicss)
                    self.selectedTopicId = self.topicss[0].id ?? ""
                    self.selectedTopicName = self.topicss[0].name ?? ""
                    self.topicTableView.reloadData()
                    self.tablview.reloadData()
              
                self.topicTableView.reloadData()
            }else{
                KVNProgress.showError(withStatus: resultt?.data?.objectTopics?.message)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.topicTableView {
            let cell: UITableViewCell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "MyCell")
            // cell.textLabel?.backgroundColor = GlobalVariables.black
            cell.textLabel?.text = self.topicss[indexPath.row].name!
            return cell
        }else{
            
            if indexPath.section == 0{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "createPostsCell", for: indexPath) as! createPostsCell
                
                cell.initcell()
                if GlobalStaticVar.classRole == "Student"{
                    cell.topicLabel.isHidden = true
                }else{
                    cell.topicLabel.isHidden = false
                }
                cell.topicBtn.addTarget(self, action: #selector(chooseTopic(_:)), for: .touchUpInside)
                if self.selectedTopicName == ""{
                    cell.topicLabel.text = "Сэдэв сонгох"
                }else{
                    cell.topicLabel.text = "\("сэдэв: ")\(self.selectedTopicName.uppercased())"
                }
                
                cell.textView.text = self.content
                cell.textView.delegate = self
                cell.nameLabel.text = self.posterLastname
                cell.lastnameLabel.text = self.posterNickname
                if self.posterImg == ""{
                    cell.proImg.image = UIImage(named: "circle-avatar")
                }else{
                    cell.proImg.sd_setImage(with:URL(string: "\(SettingsViewController.fileurl)\(self.posterImg)"),placeholderImage: UIImage(named: "circle-avatar"))
                }
                
                return cell
            }else if indexPath.section == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "filecell", for: indexPath) as! FileCell
                cell.deleteBtn.isHidden = false
                cell.deleteBtn.addTarget(self, action: #selector(deleteFile(_:)), for: .touchUpInside)
                cell.deleteBtn.tag = indexPath.row
                cell.label.text = self.files[indexPath.row].name!
                let ext = self.files[indexPath.row].extension!
                switch(ext){
                case ".jpg":
                    cell.imgview.image = UIImage(named: "jpg")
                case ".png":
                    cell.imgview.image = UIImage(named: "jpg")
                case ".pdf":
                    cell.imgview.image = UIImage(named: "pdf")
                case ".doc":
                    cell.imgview.image = UIImage(named: "doc")
                case ".docx":
                    cell.imgview.image = UIImage(named: "doc")
                case ".mov":
                    cell.imgview.image = UIImage(named: "mov")
                case ".MOV":
                    cell.imgview.image = UIImage(named: "mov")
   
                default:
                    cell.imgview.image = UIImage(named: "clip")
                }
                return cell
            }else if indexPath.section == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "filecell", for: indexPath) as! FileCell
                cell.deleteBtn.isHidden = false
                cell.deleteBtn.addTarget(self, action: #selector(deleteLinks(_:)), for: .touchUpInside)
                cell.deleteBtn.tag = indexPath.row
                cell.label.text = self.links[indexPath.row]
                cell.imgview.image = UIImage(named: "hyperlink")
                return cell
            }else if  indexPath.section == 3{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "filecell", for: indexPath) as! FileCell
                cell.deleteBtn.isHidden = false
                cell.deleteBtn.addTarget(self, action: #selector(deleteFileSection2(_:)), for: .touchUpInside)
                cell.deleteBtn.tag = indexPath.row
                var ext = ""
                
                ext = self.selectedAllFilesURL[indexPath.row].pathExtension.lowercased()
                if ext == "jpg" || ext == "png"{
                    cell.label.text = "\("img")\(indexPath.row)\("_")\(self.selectedAllFilesURL[indexPath.row].lastPathComponent.lowercased())"
                }else{
                    cell.label.text = self.selectedAllFilesURL[indexPath.row].lastPathComponent
                }
                
                switch(ext){
                case "jpg":
                    cell.imgview.image = UIImage(named: "jpg")
                case "png":
                    cell.imgview.image = UIImage(named: "jpg")
                case "pdf":
                    cell.imgview.image = UIImage(named: "pdf")
                case "doc":
                    cell.imgview.image = UIImage(named: "doc")
                case "docx":
                    cell.imgview.image = UIImage(named: "doc")
                case ".mov":
                    cell.imgview.image = UIImage(named: "mov")
                case ".MOV":
                    cell.imgview.image = UIImage(named: "mov")
          
                default:
                    cell.imgview.image = UIImage(named: "clip")
                }
                return cell
                
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "btncell", for: indexPath) as! btnCell
                
                if GlobalStaticVar.isPostCreate == true{
                    cell.sendBtn.setTitle("Нийтлэх", for: .normal)
                }else{
                    cell.sendBtn.setTitle("Засах", for: .normal)
                }
                
                cell.sendBtn.layer.cornerRadius = 5
                cell.sendBtn.backgroundColor = GlobalVariables.todpurple
                cell.sendBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
                cell.sendBtn.addTarget(self, action: #selector(self.updatePost), for: .touchUpInside)
                return cell
                
            }
        }
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
        
        let size = textView.bounds.size
        let newSize = textView.sizeThatFits(CGSize(width: size.width, height: CGFloat.greatestFiniteMagnitude))
        // Resize the cell only when cell's size is changed
        if size.height != newSize.height {
            UIView.setAnimationsEnabled(false)
            self.tablview.beginUpdates()
            self.tablview.endUpdates()
            UIView.setAnimationsEnabled(true)
        }
        if textView.text != "" {
            self.content = textView.text!
        }
    }
    
    @objc func deleteFile(_ sender:Any){
        
        let basurl = SettingsViewController.cdioFileUrl
        let headers = SettingsViewController.headers
        let path = self.files[(sender as AnyObject).tag].url ?? ""
        Alamofire.request("\(basurl)deleteImage", method: .post, parameters: ["deletePath":path], encoding: JSONEncoding.default,headers:headers).responseJSON{ response in
            switch response.result {
            case .success:
                let res = JSON(response.result.value!)
                if res["error"] == false {
                    print("deleteFile SUCCESS")
                    self.files.remove(at: (sender as AnyObject).tag)
                    self.isImage = false
                    self.tablview.reloadData()
                }
            case .failure(let e):
                print(e)
            }
        }
    }
    
    @objc func deleteFileSection2(_ sender:Any){
        self.selectedAllFilesURL.remove(at: (sender as AnyObject).tag)
        self.isImage = false
        self.tablview.reloadData()
    }
    
    @objc func deleteLinks(_ sender:Any){
        self.links.remove(at: (sender as AnyObject).tag)
        self.tablview.reloadData()
    }
    
    func postwithFile(){
      
        KVNProgress.show(withStatus: "", on: self.view)
        if self.selectedImages.count > 0{
            for p in 0..<self.selectedImages.count{
                uploadImage(teacherId: self.teacherId, image: self.selectedImages[p], completion: {(state,imageurl) in
                    DispatchQueue.main.async {
                        let str = JSON(imageurl)
                        if !str.isEmpty{
                            let msg = str["error"].boolValue
                            if msg == false{
                                let url = str["url"].string ?? ""
                                let ext = str["extension"].string ?? ""
                                let name = "\("image")\(p)\(ext.lowercased())"
                                let size = str["file_size"].double ?? 0
                                let thumb = str["thumbnail"].string ?? ""
                                let imageurl = InputPostFile.init(extension: ext, url: url, name: name, size: size, thumbnail: thumb)
                                self.fileInput.append(imageurl)
                                if self.fileInput.count == self.selectedAllFilesURL.count{
                                    let post = InputPost.init(ownerId: self.myId, content: self.content, link: self.links, file: self.fileInput,topicId: self.selectedTopicId)
                                    let mutation = InsertPostMutation(InputPost: post, classId: self.classId)
                                    self.apollo.perform(mutation: mutation) { result, error in
                                        if result?.data?.insertPost?.error == false{
                                            self.navigationController?.popViewController(animated: true)
                                        }else{
                                            KVNProgress.showError(withStatus: result?.data?.insertPost?.message)
                                        }
                                        self.isPostedClicked = false
                                    }
                                }
                            }
                        }
                    }
                })
            }
            KVNProgress.dismiss()
        }
        if self.SelectedFileUrl.count > 0 {
           
            
            KVNProgress.show(withStatus: "", on: self.view)
            for i in 0..<self.SelectedFileUrl.count{
                uploadfile(teacherId: self.teacherId, file: self.SelectedFileUrl[i],completion: { (state, fileUrl) in
                    KVNProgress.dismiss()
                    DispatchQueue.main.async {
                        let str = JSON(fileUrl)
                        if str.isEmpty{
                            KVNProgress.showError(withStatus: "Файл хуулах явцад алдаа гарлаа. Дахин оролдоно уу", on: self.view)
                            self.SelectedFileUrl = []
                            self.selectedAllFilesURL = []
                            self.fileInput = []
                            self.isImage = false
                            self.tablview.reloadData()
                        }else{
                            let msg = str["error"].boolValue
                            if msg == false{
                                let imgUrl  = str["url"].string ?? ""
                                let fileName = str["filename"].string ?? ""
                                let fileExt = str["extension"].string ?? ""
                                let size = str["file_size"].double ?? 0
                                let file = InputPostFile.init(extension: fileExt, url: imgUrl, name: fileName, size: size, thumbnail: imgUrl)
                                self.fileInput.append(file)
//                                print("FILECOUNT door:: ", self.fileInput.count)
                                if self.fileInput.count == self.selectedAllFilesURL.count{
                                    let post = InputPost.init(ownerId: self.myId, content: self.content, link: self.links, file: self.fileInput,topicId: self.selectedTopicId)
                                    let mutation = InsertPostMutation(InputPost: post, classId: self.classId)
                                    KVNProgress.show(withStatus: "", on: self.view)
                                    self.apollo.perform(mutation: mutation) { result, error in
                                        KVNProgress.dismiss()
                                        if result?.data?.insertPost?.error == false{
//                                            KVNProgress.showSuccess(withStatus: "пост амжилттай нэмэгдлээ")
                                            self.navigationController?.popViewController(animated: true)
                                        }else{
                                            KVNProgress.showError(withStatus: result?.data?.insertPost?.message)
                                        }
                                        self.isPostedClicked = false
//                                        print(result?.data as Any)
                                    }
                                }
                            }
                        }
                    }
                })
            }
        }
    }
    
    func updatewithFile(){
      
        //        print("selected Topic:: ",self.selectedTopic)
        self.allfileCount = self.files.count + self.selectedAllFilesURL.count
//        print(self.allfileCount)
        if self.allfileCount == 0 {
            let post = UpdatePost.init(content: self.content, link: self.links, file: self.fileInput,topicId: self.selectedTopicId)
            let mutation = UpdatePostMutation(postId: self.postId, classId: self.classId, userId: self.myId, updatePost: post, topic:self.selectedTopicId)
            KVNProgress.show(withStatus: "", on: self.view)
            self.apollo.perform(mutation: mutation) { result, error in
                KVNProgress.dismiss()
                if result?.data?.updatePost?.error == false{
                    self.isPostedClicked = false
                    KVNProgress.showSuccess(withStatus: "Success")
                    self.navigationController?.popViewController(animated: true)
                }else{
                    self.isPostedClicked = false
                    KVNProgress.showError(withStatus: result?.data?.updatePost?.message)
                }
            }
        }else{
            if self.files.count == 0{
                self.selectedfileUpload()
            }else{
              
                for p in 0..<self.files.count{
                    let imgUrl  = self.files[p].url!
                    let fileName = self.files[p].name!
                    let fileExt = self.files[p].extension!
                    let size = self.files[p].size
                    let file = InputPostFile.init(extension: fileExt, url: imgUrl, name: fileName, size: size, thumbnail: imgUrl)
                    self.fileInput.append(file)
                    
                    if self.fileInput.count == self.allfileCount{
                        let post = UpdatePost.init(content: self.content, link: self.links, file: self.fileInput,topicId: self.selectedTopicId)
                        KVNProgress.show(withStatus: "", on: self.view)
                        let mutation = UpdatePostMutation(postId: self.postId, classId: self.classId, userId: self.myId, updatePost: post,topic: self.selectedTopicId)
                        
                        self.apollo.perform(mutation: mutation) { result, error in
                            KVNProgress.dismiss()
                            if result?.data?.updatePost?.error == false{
                                self.isPostedClicked = false
                                KVNProgress.showSuccess(withStatus: "Success")
                                self.navigationController?.popViewController(animated: true)
                            }else{
//                                  print("ERRORR:: ", result)
                                 self.isPostedClicked = false
                                KVNProgress.showError(withStatus: result?.data?.updatePost?.message)
                            }
                        }
                    }else{
                        self.selectedfileUpload()
                    }
                }
            }
        }
    }
    
    func selectedfileUpload(){
      
        if self.selectedImages.count != 0{
            for p in 0..<self.selectedImages.count{
                uploadImage(teacherId: self.teacherId, image: self.selectedImages[p], completion: {(state,imageurl) in
                    DispatchQueue.main.async {
                        let str = JSON(imageurl)
                        //                        print("UPLOAD RESULT::" , str as Any)
                        if !str.isEmpty{
                            let msg = str["error"].boolValue
                            if msg == false{
                                let url = str["url"].string ?? ""
                                let ext = str["extension"].string ?? ""
                                let name = str["filename"].string ?? ""
                                let size = str["file_size"].double ?? 0
                                let thumb = str["thumbnail"].string ?? ""
                                
                                let imageurl = InputPostFile.init(extension: ext, url: url, name: name, size: size, thumbnail: thumb)
                                self.fileInput.append(imageurl)
                                if self.fileInput.count == self.allfileCount{
                                    let post = UpdatePost.init(content: self.content, link: self.links, file: self.fileInput,topicId: self.selectedTopicId,topic: "")
                                    let mutation = UpdatePostMutation(postId: self.postId, classId: self.classId, userId: self.myId, updatePost: post,topic: self.selectedTopicId)
                                    KVNProgress.show(withStatus: "", on: self.view)
                                    self.apollo.perform(mutation: mutation) { result, error in
                                        KVNProgress.dismiss()
                                        if result?.data?.updatePost?.error == false{
                                             self.isPostedClicked = false
                                            KVNProgress.showSuccess(withStatus: "Success")
                                            self.navigationController?.popViewController(animated: true)
                                        }else{
//                                            print("ERROR:: ", result)
                                             self.isPostedClicked = false
                                            KVNProgress.showError(withStatus: result?.data?.updatePost?.message)
                                        }
                                    }
                                }
                            }
                        }
                    }
                })
            }
        }
        
        for i in 0..<self.SelectedFileUrl.count{
          
            KVNProgress.show(withStatus: "", on: self.view)
            uploadfile(teacherId: self.teacherId, file: self.SelectedFileUrl[i],completion: { (state, fileUrl) in
                KVNProgress.dismiss()
                DispatchQueue.main.async {
                    let str = JSON(fileUrl)
                    if str.isEmpty{
                        KVNProgress.showError(withStatus: "Файл хуулах явцад алдаа гарлаа. Дахин оролдоно уу", on: self.view)
                        self.SelectedFileUrl = []
                        self.fileInput = []
                        self.isImage = false
                        self.tablview.reloadData()
                    }else{
                        let msg = str["error"].boolValue
                        if msg == false{
                            let imgUrl  = str["url"].string!
                            let fileName = str["filename"].string!
                            let fileExt = str["extension"].string!
                            let size = str["file_size"].double
                            let file = InputPostFile.init(extension: fileExt, url: imgUrl, name: fileName, size: size, thumbnail: imgUrl)
                            self.fileInput.append(file)
                            
                            if self.fileInput.count == self.allfileCount{
                                let post = UpdatePost.init(content: self.content, link: self.links, file: self.fileInput,topicId: self.selectedTopicId)
                                let mutation = UpdatePostMutation(postId: self.postId, classId: self.classId, userId: self.myId, updatePost: post,topic: self.selectedTopicId)
                                self.apollo.perform(mutation: mutation) { result, error in
                                    if result?.data?.updatePost?.error == false{
                                         self.isPostedClicked = false
                                        KVNProgress.showSuccess(withStatus: "Success")
                                        self.navigationController?.popViewController(animated: true)
                                    }else{
                                         self.isPostedClicked = false
                                        KVNProgress.showError(withStatus: result?.data?.updatePost?.message)
                                    }
                                }
                            }
                        }
                    }
                }
            })
        }
    }

    
    func uploadImage( teacherId: String, image:UIImage, completion: @escaping (Bool,Any) -> Swift.Void) {
        let baseurl = SettingsViewController.cdioFileUrl
        let parameters = ["userId":teacherId,"path":self.path]
        let URL = "\(baseurl)fileUpload"
    
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(image.jpegData(compressionQuality: 0.1)!, withName: "file", fileName: "image\(Int(Date().timeIntervalSince1970)).jpg", mimeType: "jpeg")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to:URL)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    //                        let progressPercent = Int(Progress.fractionCompleted*100)
                    //                        KVNProgress.show(withStatus: "Зураг хуулж байна \(progressPercent)%", on: self.view)
                    KVNProgress.show(withStatus: "Зураг хуулж байна. Түр хүлээнэ үү", on: self.view)
                })
                upload.responseJSON { response in
                    if let JSON = response.result.value {
                        let res = JSON
                        completion(true, res)
                    }
                    else{
                    }
                }
            case .failure(let encodingError):
                //self.delegate?.showFailAlert()
                print("Message - encode: \(encodingError)")
            }
        }
    }
    
    func getSubUser(){
        let query = GetsubUserQuery.init(userId: self.myId)
        apollo.fetch(query: query) { result, error in
            if let path =  result?.data?.subUser?.path{
                self.path = path
            }
        }
    }
    
    func uploadfile( teacherId: String, file: URL, completion: @escaping (Bool,Any) -> Swift.Void) {
        let baseurl = SettingsViewController.cdioFileUrl
        let parameters = ["userId":teacherId, "path":self.path]
        let URL = "\(baseurl)fileUpload"
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(file, withName: "file")
            
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to:URL)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    //                    let progressPercent = Int(Progress.fractionCompleted*100)
                    //                    KVNProgress.show(withStatus: "Файл хуулж байна \(progressPercent)%", on: self.view)
                    KVNProgress.show(withStatus: "Файл хуулж байна. Түр хүлээнэ үү", on: self.view)
                })
                upload.responseJSON { response in
                    if let JSON = response.result.value {
                        let res = JSON
                        completion(true, res)
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    
    
    @objc func chooseFile(){
        let actionSheet = UIAlertController(title: "", message: "Хавсаргах файл сонгох...", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Линк", style: .default, handler: { (action) -> Void in
            UIView.transition(with: self.blackView, duration: 0.3, options: .transitionCrossDissolve, animations: {
                self.blackView.isHidden = false
            })
            self.textfield.becomeFirstResponder()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Зураг", style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .photoLibrary, vc: self)
            self.fileType = "image"
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Бичлэг", style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .video, vc: self)
            self.fileType = "video"
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Файл", style: .default, handler: { (action) -> Void in
            self.documentPicker()
            self.fileType = "file"
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Болих", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func authorisationStatus(attachmentTypeEnum: AttachmentType, vc: UIViewController){
        //        currentVC = vc
        
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            if attachmentTypeEnum == AttachmentType.camera{
                openCamera()
            }
            if attachmentTypeEnum == AttachmentType.photoLibrary{
                photoLibrary()
            }
            if attachmentTypeEnum == AttachmentType.video{
                videoLibrary()
            }
        case .denied:
            print("permission denied")
            self.addAlertForSettings(attachmentTypeEnum)
        case .notDetermined:
            print("Permission Not Determined")
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == PHAuthorizationStatus.authorized{
                    // photo library access given
                    print("access given")
                    if attachmentTypeEnum == AttachmentType.camera{
                        self.openCamera()
                    }
                    if attachmentTypeEnum == AttachmentType.photoLibrary{
                        self.photoLibrary()
                    }
                    if attachmentTypeEnum == AttachmentType.video{
                        self.videoLibrary()
                    }
                }else{
                    print("restriced manually")
                    self.addAlertForSettings(attachmentTypeEnum)
                }
            })
        case .restricted:
            print("permission restricted")
            self.addAlertForSettings(attachmentTypeEnum)
        default:
            break
        }
    }
    
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .camera
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    
    //MARK: - PHOTO PICKER
    func photoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .photoLibrary
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    //MARK: - VIDEO PICKER
    func videoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .photoLibrary
            myPickerController.mediaTypes = [kUTTypeMovie as String, kUTTypeVideo as String]
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    //MARK: - FILE PICKER
    func documentPicker(){
        //        let importMenu = UIDocumentMenuViewController(documentTypes: [], in: .import)
        
        let importMenu = UIDocumentMenuViewController(documentTypes: ["public.content"], in: .import)
        
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .fullScreen
        self.present(importMenu, animated: true, completion: nil)
    }
    
    //MARK: - SETTINGS ALERT
    func addAlertForSettings(_ attachmentTypeEnum: AttachmentType){
        var alertTitle: String = ""
        if attachmentTypeEnum == AttachmentType.camera{
            alertTitle = "App does not have access to your camera. To enable access, tap settings and turn on Camera."
        }
        if attachmentTypeEnum == AttachmentType.photoLibrary{
            alertTitle = "App does not have access to your photos. To enable access, tap settings and turn on Photo Library Access."
        }
        if attachmentTypeEnum == AttachmentType.video{
            alertTitle = "App does not have access to your video. To enable access, tap settings and turn on Video Library Access."
        }
        
        let cameraUnavailableAlertController = UIAlertController (title: alertTitle , message: nil, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title:"Settings", style: .destructive) { (_) -> Void in
            let settingsUrl = NSURL(string:UIApplication.openSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            }
        }
        let cancelAction = UIAlertAction(title:"Settings", style: .default, handler: nil)
        cameraUnavailableAlertController .addAction(cancelAction)
        cameraUnavailableAlertController .addAction(settingsAction)
        self.present(cameraUnavailableAlertController , animated: true, completion: nil)
    }
}

extension UpdatePostVC {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        self.fileType = ""
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        if let videoURL = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.mediaURL)] as? URL {
            CameraView.visible = 0
            self.SelectedFileUrl.append(videoURL)
            self.selectedAllFilesURL.append(videoURL)
            self.isImage = false
            self.tablview.reloadData()
        }
        else if let pickedImage1 = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            CameraView.visible = 0
            self.selectedImages.append(pickedImage1)
            if let imageUrl = info["UIImagePickerControllerReferenceURL"] as? URL{
                self.selectedAllFilesURL.append(imageUrl)
                self.selectedimageName.append("\("img")\(self.selectedAllFilesURL.count) \(imageUrl.lastPathComponent)")
                self.isImage = true
                self.tablview.reloadData()
            }
        }else if let pickedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as? UIImage {
            CameraView.visible = 0
            self.selectedImages.append(pickedImage)
            if let imageUrl = info["UIImagePickerControllerReferenceURL"] as? URL{
                self.selectedAllFilesURL.append(imageUrl)
                self.isImage = true
                self.selectedimageName.append("\("image") \(imageUrl.lastPathComponent)")
                self.tablview.reloadData()
            }
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Video Compressing technique
    fileprivate func compressWithSessionStatusFunc(_ videoUrl: NSURL) {
        let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".MOV")
        compressVideo(inputURL: videoUrl as URL, outputURL: compressedURL) { (exportSession) in
            guard let session = exportSession else {
                return
            }
            
            switch session.status {
            case .unknown:
                break
            case .waiting:
                break
            case .exporting:
                break
            case .completed:
                guard let compressedData = NSData(contentsOf: compressedURL) else {
                    return
                }
                print("File size after compression: \(Double(compressedData.length / 1048576)) mb")
                
                DispatchQueue.main.async {
                    self.videoPickedBlock?(compressedURL as NSURL)
                }
                
            case .failed:
                break
            case .cancelled:
                break
            }
        }
    }
    
    // Now compression is happening with medium quality, we can change when ever it is needed
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPreset1280x720) else {
            handler(nil)
            
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mov
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
    
    func checkEmpty() -> Bool{
        if self.content != "" || self.files.count > 0 || self.links.count > 0 {
            return true
        }else if self.content == "" || self.content == " "{
            if self.files.count == 0 && self.selectedImages.count == 0 && self.selectedAllFilesURL.count == 0{
                if self.links.count == 0{
                    return false
                }else{
                    return true
                }
            }else{
                return true
            }
        }else{
            return true
        }
    }
    
    func topicViewInit(){
        self.topicBlackView.frame = CGRect(x:0 , y:0, width:self.view.bounds.width, height: self.view.bounds.height)
        self.topicBlackView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        
        self.topicView.frame = CGRect(x:self.topicBlackView.bounds.width/2 - 140 , y:150, width:280, height: 250)
        self.topicView.backgroundColor = UIColor.white
        self.topicTableView.frame = CGRect(x: 0, y: 0, width: self.topicView.bounds.width, height: self.topicView.bounds.height)
        self.topicTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        self.topicTableView.dataSource = self
        self.topicTableView.delegate = self
        self.view.addSubview(self.topicBlackView)
        self.topicBlackView.addSubview(self.topicView)
        self.topicView.addSubview(self.topicTableView)
        self.topicBlackView.isHidden = true
    }
    
}

//MARK: - FILE IMPORT DELEGATE
extension UpdatePostVC{
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        self.present(documentPicker, animated: true, completion: nil)
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        self.filePickedBlock?(url)
        KVNProgress.show(0.75, status: "loading", on: self.view)
        self.SelectedFileUrl.append(url)
        self.selectedAllFilesURL.append(url)
        self.isImage = false
        self.tablview.reloadData()
        KVNProgress.dismiss()
        
    }
    //    Method to handle cancel action.
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true, completion: nil)
        self.fileType = ""
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}


