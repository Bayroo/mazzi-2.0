//
//  MoreVC.swift
//  Mazzi
//
//  Created by Bayara on 2/15/19.
//  Copyright © 2019 Mazz Application. All rights reserved.
//

import Foundation
import RealmSwift
import EFQRCode
import Alamofire
import SwiftyJSON

class MoreVC: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
   var myId = SettingsViewController.userId
   var nickname = ""
    var lastname = ""
    var birthday : Double!
    var region = ""
    var email = ""
    var displayphone = ""
    var profileImg = ""
    var fileurl = SettingsViewController.fileurl
    var appVersion = ""
    @IBOutlet weak var proImgView: UIImageView!
    @IBOutlet weak var myTableView: UITableView!
    var items = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getInfo()
        self.items = ["Найзаа урих","Найз нэмэх","QR уншигч"]
        self.myTableView.delegate = self
        self.myTableView.dataSource = self
        self.myTableView.register((UINib(nibName: "profileCell", bundle: nil)), forCellReuseIdentifier: "more")
//        self.myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "DefaultCell")
        self.appVersion = (Bundle.main.infoDictionary?["CFBundleShortVersionString"]  as? String)!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.myTableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        let turshiltbtn = UIBarButtonItem(image: UIImage(named:"showQR"), style: .plain, target: self, action: #selector(showQR))
          let turshiltbtn = UIBarButtonItem(image: UIImage(named:""), style: .plain, target: self, action: #selector(showQR))
          self.navigationController?.navigationBar.topItem?.rightBarButtonItem = turshiltbtn
        
    }

    func getInfo(){
        let userInfo = userDefaults.dictionary(forKey: "userInfo")
        self.nickname = userInfo!["nickname"] as! String
        self.lastname = userInfo!["lastname"] as! String
        self.birthday = ( userInfo!["birthday"] as! Double )/1000
        self.email = userInfo!["email"] as! String
        self.region = userInfo!["region"] as! String
        self.displayphone = userInfo!["displayphone"] as! String
        self.profileImg = userInfo!["image"] as! String
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 90
        }else{
            return 50
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else if section == 1{
            return self.items.count
        }else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
             self.performSegue(withIdentifier: "updatePro", sender: self)
        }else if indexPath.section == 1{
            if indexPath.row == 1{
                self.performSegue(withIdentifier: "addFriend", sender: self)
            }else if indexPath.row == 2 {
                self.performSegue(withIdentifier: "qrScan", sender: self)
            }else if indexPath.row == 0{
                let viewController = storyboard?.instantiateViewController(withIdentifier: "inviteContacts") as! inviteContacts
                let navController = UINavigationController(rootViewController: viewController)
                self.present(navController, animated:true, completion: nil)
                
//                 self.performSegue(withIdentifier: "toInvite", sender: self)
            }
        }else{
            self.logout()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "more", for: indexPath) as! MoreCell
            cell.accessoryType = .disclosureIndicator
            cell.nameLabel.text = self.nickname.uppercased()
            cell.phoneLabel.text = self.displayphone
            if self.profileImg == "" {
               cell.imgView.image = UIImage(named: "circle-avatar")
            }else{
                cell.imgView.sd_setImage(with: URL(string:"\(self.fileurl)\(self.profileImg)"), completed: nil)
            }
            return cell
        }else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultCell")!
            cell.accessoryType = .disclosureIndicator
            cell.textLabel?.font = GlobalVariables.customFont14
            cell.textLabel?.text = self.items[indexPath.row]
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultCell")!
            cell.accessoryType = .disclosureIndicator
            cell.textLabel?.font = GlobalVariables.customFont14
            cell.textLabel?.text = "Гарах"
//            cell.imageView?.image = UIImage(named: "logout")
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }else if section == 1{
             return 20
        }else{
            return 20
        }
    }
    
    
    // -------------  LOGOUT -------------------------
    func logout(){
        let alert = UIAlertController(title: "Системээс гарах", message: "Та маззи системээс гарахдаа итгэлтэй байна уу?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction) -> Void in
            let userDefaultsData = UserDefaults.standard
            
            //userDefaultsData.removeObject(forKey: "phone")
            userDefaultsData.removeObject(forKey: "isLogin")
            userDefaultsData.removeObject(forKey: "key")
            userDefaultsData.removeObject(forKey: "user_id")
            userDefaultsData.removeObject(forKey: "token")
            userDefaultsData.removeObject(forKey: "fcm_token")
            userDefaultsData.removeObject(forKey: "userInfo")
            userDefaultsData.removeObject(forKey: "keyboardheight")
            userDefaultsData.removeObject(forKey: "authVerificationID")
            GlobalVariables.headerToken = ""
            GlobalVariables.user_id = ""
            SettingsViewController.userId = ""
          
            RealmService.deleteAllRealm(completion: { (suc) in
                
                self.navigationController?.popToRootViewController(animated: false)
                    self.logoutRest()
                    
                
            })
//            self.apollo.clearCache()
         
        }))
        alert.addAction(UIAlertAction(title: "Цуцлах", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func logoutRest(){
        let baseurl = SettingsViewController.baseurl
        Alamofire.request("\(baseurl)logout", method: .post, parameters: ["user_id": self.myId], encoding: JSONEncoding.default).responseJSON
            { response in
                switch response.result {
                case .success:
                    let res = JSON(response.result.value!)
                    print("logout sidemenu: ",res)
                    self.dismiss(animated: true, completion: nil)
                    if res["error"].boolValue == false{
                        print("LOGOUT SUCCESS")
                    }
                case .failure(let e):
                    print(e)
                }
        }
    }
    
    //------------ GENERATE QR CODE ---------------------------
    
    @objc func showQR(){
//        let dic = ["code":self.myId, "type":self.myId]
//        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
//        let jsonString = String(data: jsonData!, encoding: .utf8)
//        let utf8str = jsonString?.data(using: String.Encoding.utf8)
//        let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
//        
//        self.generateQR(content: base64Encoded!)
    }
    
    func generateQR(content: String){
        if let tryImage = EFQRCode.generate(
            content: content
            //            watermark: UIImage(named: "avatar")?.toCGImage()
            ){
            let myCIQR = self.convertCGImageToCIImage(inputImage: tryImage)
            let myQR = self.convert(cmage: myCIQR!)
            let alert = CustomAlertView.init(title: self.myId , image: myQR)
            alert.show(animated: true)
            //    alert.rescan.addTarget(self, action: #selector(goscaneer), for: UIControlEvents.touchUpInside)
        } else {
            print("Create QRCode image failed!")
        }
    }
    
    func convertCGImageToCIImage(inputImage: CGImage) -> CIImage! {
        let ciImage = CIImage(cgImage: inputImage, options: nil)
        return ciImage
    }
    
    func convert(cmage:CIImage) -> UIImage
    {
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent)!
        let image:UIImage = UIImage.init(cgImage: cgImage)
        return image
    }
    
}
