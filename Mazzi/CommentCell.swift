//
//  CommentCell.swift
//  Mazzi
//
//  Created by Bayara on 12/11/18.
//  Copyright © 2018 Mazzi Application. All rights reserved.
//

import Foundation

class CommentCell:UITableViewCell {
    
    @IBOutlet weak var proImg: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var lastnameLabel: UILabel!
    @IBOutlet weak var topicLabel: UILabel!
    
    func initcell(){
        self.proImg.layer.borderWidth = 1
        self.proImg.layer.borderColor = GlobalVariables.F5F5F5.cgColor
        self.proImg.layer.masksToBounds = true
    }
}
