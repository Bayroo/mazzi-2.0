//
//  NewContactVC.swift
//  Mazzi
//
//  Created by Bayara on 1/29/19.
//  Copyright © 2019 woovoo. All rights reserved.
//

import Foundation
import Contacts
import KVNProgress

class NewContactVC:UIViewController{
    var nickname = ""
    var lastname = ""
    var number = ""
    var image = ""
    var userid = ""
    var onlineAt = Double()
    var birthday = Double()
    var state = 0
    var fcm = ""
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var saveBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if GlobalVariables.isFromClassMembers == true{
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    func customize(){
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "05"), style: .plain, target: self, action: #selector(back))
        
        self.imgView.layer.cornerRadius = 25
        self.imgView.layer.borderColor = GlobalVariables.F5F5F5.cgColor
        self.imgView.layer.borderWidth = 1
        if self.image == ""{
            self.imgView?.image = UIImage(named: "circle-avatar")
        }else{
            self.imgView?.sd_setImage(with:URL(string: "\(SettingsViewController.fileurl)\(self.image)"),placeholderImage: UIImage(named: "avatar"))
        }
        
        self.nameLabel.text = "\(self.nickname) \(self.lastname)"
        self.numberLabel.text = self.number
        
        self.saveBtn.layer.cornerRadius = 5
        self.saveBtn.backgroundColor = GlobalVariables.purple
        self.saveBtn.setTitle("Хадгалах", for: .normal)
        self.saveBtn.setTitleColor(UIColor.white, for: .normal)
        self.saveBtn.addTarget(self, action: #selector(saveContact), for: .touchUpInside)
    }
    @IBAction func baaack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func back(){
//        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func saveContact(){
        
        let newContact = CNMutableContact()
        let store = CNContactStore()
        newContact.givenName = self.nickname
        newContact.familyName = self.lastname
        let test = CNPhoneNumber.init(stringValue: self.number)
        newContact.phoneNumbers = [CNLabeledValue(label: CNLabelPhoneNumberiPhone , value: test)]
        let saveReq = CNSaveRequest()
        saveReq.add(newContact, toContainerWithIdentifier: nil)
        try! store.execute(saveReq)
        self.showProfile()
//        self.dismiss(animated: true, completion: nil)
       
    }
    
    func showProfile(){
        KVNProgress.show(withStatus: "", on: self.view)
        let viewController = storyboard?.instantiateViewController(withIdentifier: "ProfileContact") as! ProfileContact
        viewController.contactid = self.userid
        viewController.username = self.lastname
        viewController.membernickname = self.nickname
        viewController.memberdate = self.birthday
        viewController.member_onlineAt = self.onlineAt
        viewController.memberphone = self.number
        viewController.memberImg = self.image
        let navController = UINavigationController(rootViewController: viewController)
        KVNProgress.dismiss()
        GlobalVariables.isFromClassMembers = true
        self.present(navController, animated:true, completion: nil)
    }
}
