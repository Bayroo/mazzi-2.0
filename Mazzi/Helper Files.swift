import Foundation
import UIKit

//Global variables
struct GlobalVariables {
//    static let todpurple = UIColor.rbg(r:26, g:33, b:49)
     static let todpurple = UIColor.rbg(r:47, g:56, b:108)
//     static let purple = UIColor.rbg(r: 12, g: 23, b: 84)
    static let purple = UIColor.rbg(r: 0, g: 0, b: 0)
     static let black = UIColor.rbg(r: 12, g: 23, b: 84)
//     static let black = UIColor.rbg(r: 50, g: 80, b: 170)
    
    static let blue = UIColor.rbg(r: 129, g: 144, b: 255)
    static let lightGray = UIColor.rbg(r: 170, g: 170, b: 170)
    static let appColor = UIColor.rbg(r: 88, g: 47, b: 226)
    static let whiteGray = UIColor.rbg(r: 200, g: 200, b: 200)
    static let F5F5F5 = UIColor.rbg(r: 245, g: 245, b: 245)
    static let saaral = UIColor.rbg(r:247 , g:247 , b:247)
//    static let todpurple = UIColor.rbg(r:60 , g:8 , b:78)
//    static let purple = UIColor.rbg(r:167, g:56 , b: 144)
//    static let black = UIColor.rbg(r: 167, g:56 , b: 144)
    static let newPurple = UIColor.rbg(r:195, g:35 , b: 144)
    static let gradiendBlack = UIColor.rbg(r:41, g:44 , b: 96)
    static let green = UIColor.rbg(r:23, g:154 , b:25)
   
    static let TextTod = UIColor.rbg(r: 223, g: 222, b: 222)
    static let TextBvdeg = UIColor.rbg(r: 223, g: 222, b: 222)
    static let bvdegYagaan = UIColor.rbg(r: 251, g: 240, b: 252)
    static let customFont8 = UIFont(name: "segoeui", size: 8.0)
    static let customFont14 = UIFont(name: "segoeui", size: 14.0)
    static let customFont16 = UIFont(name: "segoeui", size: 16.0)
    static let customFont10 = UIFont(name: "segoeui", size: 10.0)
    static let customFont12 = UIFont(name: "segoeui", size: 12.0)
    static let customFont18 = UIFont(name: "segoeui", size: 22.0)
    static let customBold14 = UIFont(name: "segoe-ui-semibold",size:12)
//    static let customBold14 = UIFont(name: "seguisb",size:14)
    static let ButtonBackgroundColor = UIColor.rbg(r:60 , g:8 , b:78)
    
    static var headerToken = ""
    static var phone = ""
    
    static var isloggedin = ""
    static var fcm_token = ""
    static var badgeNumber = Int()
    
    static var blocked = false
    static var keyboardheight = 0
    static var isChat = false
    
    static var user_id = ""
    static var isFromContacts = false
    static var isFromClassMembers = false
    static var isUpdateTopic = false
    static var ConversationOwnerId = ""
    static var isFromChat = false
    static var studentID = ""
    static var isUpdateStudentId = false
    static var isFromPassRecover = false
    static var isFromMClass = false
    static var ConversationID = ""
    static var lastMsgId = ""
    static var deletedMsgsID = [String]()
  //  static var savedMe = false
}

//Extensions
extension UIColor{
    class func rbg(r: CGFloat, g: CGFloat, b: CGFloat) -> UIColor {
        let color = UIColor.init(red: r/255, green: g/255, blue: b/255, alpha: 1)
        return color
    }
}

class RoundedImageView: UIImageView {
    override func layoutSubviews() {
        super.layoutSubviews()
        let radius: CGFloat = self.bounds.size.width / 2.0
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
}
class RoundView: UIView {
    override func layoutSubviews() {
        super.layoutSubviews()
        let radius: CGFloat = self.bounds.size.width / 2.0
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
}
class RoundedButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        let radius: CGFloat = self.bounds.size.height / 2.0
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
}

//Enums
enum ViewControllerType {
    case welcome
    case conversations
}

enum PhotoSource {
    case library
    case camera
}

enum ShowExtraView {
    case contacts
    case profile
    case preview
    case map
}
enum CameraType {
    case front
    case back
}
struct PlatformUtils {
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
            isSim = true
        #endif
        return isSim
    }()
}

struct TokenUtils {
    static func fetchToken(url : String) throws -> String {
        print("URL: ", url)
        var token: String = "TWILIO_ACCESS_TOKEN"
        let requestURL: URL = URL(string: url)!
        print("REQUESTURL:: ",requestURL)
        do {
            let data = try Data(contentsOf: requestURL)
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String:AnyObject]
                print("JSON :: ",json)
                token = json["token"] as! String
                print("TOKEN: ", token)
            } catch let error as NSError{
                print ("Helper Files - Error with json, error = \(error)")
                throw error
            }
        } catch let error as NSError {
            print ("Helper Files - Invalid token url, error = \(error)")
            throw error
        }
        return token
    }
}
