//
//  ContactUsViewController.swift
//  Mazzi
//
//  Created by Bayara on 8/18/18.
//  Copyright © 2018 Mazzi App LLC. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

class ContactUsViewController:UIViewController, MFMailComposeViewControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "Холбоо барих"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:UIImage(named: "05"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(back))
    }
    
    @objc func back(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func goWebsite(_ sender: Any) {
        if let url = URL(string: "https://mazzi.mn/") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    @IBAction func ComposeMail(_ sender: Any) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["mazzi.app@gmail.com"])
            mail.setMessageBody("<p>Санал хүсэлтээ бичнэ үү</p>", isHTML: true)
            
            present(mail, animated: true)
           
        } else {
            let alert = UIAlertController(title: "", message: "Амжилтгүй боллоо! Та дахин оролдоно уу!", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Хаах", style: UIAlertAction.Style.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    @IBAction func toMap(_ sender: Any) {
        if let url = URL(string: "https://map.what3words.com/%D0%B3%D2%AF%D0%B9%D1%81%D1%8D%D0%BD.%D1%82%D2%AF%D0%B3%D1%8D%D1%85.%D1%82%D0%B0%D0%B2%D0%BB%D0%B0%D0%B3") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    @IBAction func toCall(_ sender: Any) {
        let phoneNumber = "+97675105602"
        if let phoneCallURL:NSURL = NSURL(string: "tel://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL as URL)) {
                application.open(phoneCallURL as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
