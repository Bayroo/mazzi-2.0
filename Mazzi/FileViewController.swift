//
//  FileViewController.swift
//  Mazzi
//
//  Created by Janibekm on 10/15/17.
//  Copyright © 2017 Mazzi App LLC. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import SDWebImage
import Photos

import Alamofire

class FileViewController: UIViewController {

//    let avPlayer = AVPlayer()
//    var avPlayerLayer: AVPlayerLayer!
    var pageIndex : Int = 0
    var sMsgType = ""
    var sFileUrl = ""
    var sFileName = ""
    var takenPhoto:UIImage? = nil
    var preview = UIView()
    var player = AVPlayer()
    var av = AVPlayerViewController()
    var pause = true
    var playbtn = UIButton()
    let playimg = UIImage(named: "play")
    let pauseimg = UIImage(named: "pause")
    let saveimg = UIImage(named: "file")
    let photoExtensions = ["bmp", "gif", "img", "jpe", "jpeg", "jpg", "pbm", "pgm", "png"]
    let videoExtensions = ["mov", "mp4", "avi", "mpeg", "mpe", "mpv", "ogg", "m4v", "m4p"]
    let fileExtensions = ["pdf","doc"]
    var theHeight = 0
    var theWidth = 0
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(backtoroot))
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        theHeight = Int(view.frame.size.height)
        theWidth = Int(view.frame.size.width)
        preview = UIView(frame:CGRect(x:0, y: 0, width: screenWidth, height: screenHeight ))
        view.addSubview(preview)
        let backbtn = UIButton(frame:CGRect(x:10, y: 16, width: 35, height:28))
        let img = UIImage(named: "05")
        backbtn.layer.cornerRadius = 5
        backbtn.layer.masksToBounds = true
        backbtn.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        backbtn.setImage(img, for: .normal)
        backbtn.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        //backbtn.layer.zPosition = 2
        view.addSubview(backbtn)
        
        let savebtn = UIButton(frame:CGRect(x: theWidth - 32, y: 16, width: 28, height:28))
        savebtn.setImage(saveimg, for: .normal)
        savebtn.addTarget(self, action: #selector(saveAction), for: .touchUpInside)
        //savebtn.layer.zPosition = 3
        view.addSubview(savebtn)
        
        self.playbtn = UIButton(frame:CGRect(x: Int(theWidth / 2 - 14), y: Int(theHeight/2 - 14), width: 60, height:60))
        playbtn.setImage(playimg, for: .normal)
        playbtn.addTarget(self, action: #selector(playAction), for: .touchUpInside)
        playbtn.isHidden = true
        view.addSubview(playbtn)
        //zasvar
        //if sFileUrl.hasSuffix(".jpg"){
        if photoExtensions.contains((sFileUrl.lowercased() as NSString).pathExtension){
            playbtn.isHidden = true
            var imageview = UIImageView()
            imageview = UIImageView(frame:CGRect(x:0, y: 0, width: screenWidth, height: screenHeight))
            let fileurl = SettingsViewController.cdioFileUrl
            imageview.isOpaque = true
            imageview.contentMode = .scaleAspectFit
            imageview.clipsToBounds = true
            imageview.layer.zPosition = 0
            preview.addSubview(imageview)
            if sFileUrl.range(of:"thumb") != nil {
                let replaced = sFileUrl.replacingOccurrences(of: "thumb", with: "original")
                imageview.sd_setImage(with: URL(string: "\(fileurl)\(replaced)"), placeholderImage: UIImage(named: "loading"))
                self.takenPhoto = imageview.image
            }else{
                imageview.sd_setImage(with: URL(string: "\(fileurl)\(sFileUrl)"), placeholderImage: UIImage(named: "loading"))
                self.takenPhoto = imageview.image
            }
        }else if videoExtensions.contains((sFileUrl.lowercased() as NSString).pathExtension){
            playbtn.isHidden = false
            let fileurl = SettingsViewController.cdioFileUrl
            let stringtourl = URL(string: "\(fileurl)\(sFileUrl)")
            if stringtourl != nil {
                self.player = AVPlayer(url: stringtourl!)
            }
            self.av = AVPlayerViewController()
            self.av.player = self.player
            self.av.showsPlaybackControls = false
            self.av.view.frame = CGRect(x: 0, y: 0, width: theWidth, height: theHeight)
            self.addChild(av)
            NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
            preview.layer.zPosition = 0
            preview.addSubview(av.view)
        }
        
        else{
            var webview = UIWebView()
            webview = UIWebView(frame:CGRect(x:0,y:0,width: screenWidth, height:screenHeight))
            let fileurl = SettingsViewController.cdioFileUrl
            let stringtourl = URL(string: "\(fileurl)\(sFileUrl)")
            webview.loadRequest(URLRequest(url: stringtourl!))
            preview.addSubview(webview)
        }
    }
    
//    @objc func backtoroot(){
//        self.navigationController?.popViewController(animated: true)
//    }
   
    @objc func buttonAction(sender: UIButton!) {
        dismiss(animated: true, completion: nil)
    }
    @objc func playAction(sender: UIButton){
        if ((player.rate != 0) && (player.error == nil)) {
            self.player.pause()
            playbtn.setImage(playimg, for: .normal)
        }else{
            playbtn.setImage(pauseimg, for: .normal)
            self.player.play()
        }
    }
    @objc func playerDidFinishPlaying(note: NSNotification) {
        self.viewDidLoad()
//        self.player.pause()
//        playbtn.setImage(playimg, for: .normal)
    }
    //zasvar
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
    @objc func saveAction(sender: UIButton!) {
        let status = PHPhotoLibrary.authorizationStatus()
        //zasvar
        //if sFileUrl.hasSuffix(".jpg"){
        if photoExtensions.contains((sFileUrl.lowercased() as NSString).pathExtension){
            
            if (status == .authorized || status == .notDetermined) {
//                guard let imageToSave = takenPhoto else {
//                    return
//                }
                let downloadImageUrl = "\(SettingsViewController.cdioFileUrl)\(sFileUrl.replacingOccurrences(of: "thumb", with: "original"))"
                getDataFromUrl(url: URL(string : downloadImageUrl)!) { data, response, error in
                    guard let data = data, error == nil else { return }
                    DispatchQueue.main.async() {
                        let imageToSave = UIImage(data: data)
                        UIImageWriteToSavedPhotosAlbum(imageToSave!, nil, nil, nil)
                    }
                }
//                UIImageWriteToSavedPhotosAlbum(imageToSave, nil, nil, nil)
            }
            dismiss(animated: true, completion: nil)
        }else{
            let downloadVideoUrl = "\(SettingsViewController.cdioFileUrl)\(sFileUrl.replacingOccurrences(of: "thumb", with: "original"))"
            UISaveVideoAtPathToSavedPhotosAlbum(downloadVideoUrl+".mp4", nil, nil, nil)
            let fileurl = SettingsViewController.cdioFileUrl
            guard let videoURL = URL(string: "\(fileurl)\(sFileUrl).mp4") else { return }
            guard let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
            if FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent(videoURL.lastPathComponent).path) {
                print("File already exists at destination url")
            }
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.player.pause()
        playbtn.setImage(playimg, for: .normal)
    }
}
extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
}

