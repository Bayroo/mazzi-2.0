//
//  chatTabbarVC.swift
//  Mazzi
//
//  Created by Bayara on 1/23/19.
//  Copyright © 2019 woovoo. All rights reserved.
//

import Foundation

class chatTabbarVC : UITabBarController{
    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(backtoRoot))
         self.navigationController?.navigationBar.barTintColor = GlobalVariables.black
        
        let attributesNormal = [
            NSAttributedString.Key.foregroundColor : GlobalVariables.lightGray,
            NSAttributedString.Key.font : GlobalVariables.customFont12
        ]
        let attributesSelected  = [
            NSAttributedString.Key.foregroundColor : GlobalVariables.todpurple,
            NSAttributedString.Key.font : GlobalVariables.customFont12
        ]
        
        
        UITabBarItem.appearance().setTitleTextAttributes(attributesNormal as [NSAttributedString.Key : Any], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes(attributesSelected as [NSAttributedString.Key : Any], for: .selected)
        
//        print("CC:: ", self.navigationController?.topViewController)
    }

    @objc func backtoRoot(){
        self.navigationController?.popViewController(animated: true)
    }
}
