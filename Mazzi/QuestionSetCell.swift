//
//  QuestionSetCell.swift
//  Mazzi
//
//  Created by Bayara on 12/16/18.
//  Copyright © 2018 Mazzi Application. All rights reserved.
//

import Foundation

class QuestionSetCell:UITableViewCell{
    
    @IBOutlet weak var semNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var questionCountLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    
}
