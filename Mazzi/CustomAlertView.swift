//
//  CustomAlertView.swift
//  Mazzi
//
//  Created by Bayara on 8/22/18.
//  Copyright © 2018 Mazzi App LLC. All rights reserved.
//

import Foundation
import UIKit


class CustomAlertView: UIView ,Modal {
    var backgroundView = UIView()
    var dialogView = UIView()
    var rescan = UIButton()
  @objc   func dismiss(animated: Bool) {
        if animated {
            UIView.animate(withDuration: 0.33, animations: {
                self.backgroundView.alpha = 0
            }, completion: { (completed) in
                
            })
            UIView.animate(withDuration: 0.33, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 10, options: UIView.AnimationOptions(rawValue: 0), animations: {
                self.dialogView.center = CGPoint(x: self.center.x, y: self.frame.height + self.dialogView.frame.height/2)
            }, completion: { (completed) in
                self.removeFromSuperview()
            })
        }else{
            self.removeFromSuperview()
        }
    
    }
  

    convenience init(title:String, image:UIImage) {
        self.init(frame: UIScreen.main.bounds)
        
        backgroundView.frame = frame
        backgroundView.backgroundColor = UIColor.black
        backgroundView.alpha = 0.6
        addSubview(backgroundView)
        
        let dialogViewWidth = frame.width-64
        
        let titleLabel = UILabel(frame: CGRect(x: 8, y: 8, width: dialogViewWidth-16, height: 40))
        titleLabel.text = title
        titleLabel.numberOfLines = 2
        titleLabel.font = UIFont(name: "segoeui", size: 16)
        titleLabel.textAlignment = .center
        titleLabel.textColor = GlobalVariables.purple
        dialogView.addSubview(titleLabel)
        let separatorLineView = UIView()
        separatorLineView.frame.origin = CGPoint(x: 0, y: titleLabel.frame.height + 8)
        separatorLineView.frame.size = CGSize(width: dialogViewWidth, height: 1)
        separatorLineView.backgroundColor = GlobalVariables.purple
        dialogView.addSubview(separatorLineView)
       
        let imageView = UIImageView()
        imageView.frame.origin = CGPoint(x: (frame.width-64)/2 - 75, y: separatorLineView.frame.height + separatorLineView.frame.origin.y + 8)
        imageView.frame.size = CGSize(width: 150 , height: 150)
      
        imageView.image = image
        imageView.layer.cornerRadius = 4
        imageView.clipsToBounds = true
        
//        let separatorLineView2 = UIView()
//        separatorLineView2.frame.origin = CGPoint(x: 0, y: imageView.frame.height + 8 + titleLabel.frame.height + 8 + 8 )
//        separatorLineView2.frame.size = CGSize(width: dialogViewWidth, height: 1)
//        separatorLineView2.backgroundColor = GlobalVariables.purple
//        dialogView.addSubview(separatorLineView2)
//        
//       
//        rescan = UIButton()
//        rescan.frame.origin = CGPoint(x: 8, y: imageView.frame.origin.y + imageView.frame.size.height + 8 + separatorLineView2.frame.height + 8)
//        rescan.frame.size = CGSize(width: dialogViewWidth-16 , height: 40)
//        rescan.setTitle("Дахиж уншуулах", for: UIControl.State.normal)
//        rescan.layer.borderWidth = 0.0
//        rescan.layer.borderColor = UIColor.black.cgColor
//        rescan.layer.cornerRadius = 4
//        rescan.setTitleColor(GlobalVariables.purple, for: UIControl.State.normal)
//        rescan.layer.masksToBounds = true
//      //  rescan.addTarget(self, action: #selector(rescans), for: UIControlEvents.touchUpInside)
//        dialogView.addSubview(rescan)
        
        let separatorLineView3 = UIView()
        separatorLineView3.frame.origin = CGPoint(x: 0, y: imageView.frame.height + 8 + titleLabel.frame.height + 8 + 8 +  rescan.frame.size.height + 8 + 8 )
        separatorLineView3.frame.size = CGSize(width: dialogViewWidth, height: 1)
        separatorLineView3.backgroundColor = GlobalVariables.purple
        dialogView.addSubview(separatorLineView3)
        
        let closeBtn = UIButton()
        closeBtn.frame.origin = CGPoint(x: 8, y: imageView.frame.origin.y + imageView.frame.size.height + 8 + rescan.frame.size.height + 8 + separatorLineView3.frame.height + 8 + 8)
        closeBtn.frame.size = CGSize(width: dialogViewWidth-16 , height: 40)
        closeBtn.setTitle("Хаах", for: UIControl.State.normal)
        closeBtn.layer.borderWidth = 0.0
        closeBtn.layer.borderColor = UIColor.black.cgColor
        closeBtn.layer.cornerRadius = 4
        closeBtn.setTitleColor(GlobalVariables.purple, for: UIControl.State.normal)
        closeBtn.layer.masksToBounds = true
        closeBtn.addTarget(self, action: #selector(dismiss(animated:)), for: UIControl.Event.touchUpInside)
        dialogView.addSubview(closeBtn)
        
       
        dialogView.addSubview(imageView)
        let dialogViewHeight = titleLabel.frame.height + 8 + separatorLineView.frame.height + 8 + imageView.frame.height + 8 + closeBtn.frame.height  + 8 + rescan.frame.height + 8 + separatorLineView3.frame.height + 8
        dialogView.frame.origin = CGPoint(x: 32, y: frame.height)
        dialogView.frame.size = CGSize(width: frame.width-64, height: dialogViewHeight)
        dialogView.backgroundColor = UIColor.white
        dialogView.layer.cornerRadius = 6
        dialogView.clipsToBounds = true
        addSubview(dialogView)
        
      
        
        backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTappedOnBackgroundView)))
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
       
    }
    
  @objc  func didTappedOnBackgroundView(){
        dismiss(animated: true)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    
}
