//
//  HomeCollectionViewCell.swift
//  Mazzi
//
//  Created by Bayara on 7/28/18.
//  Copyright © 2018 Mazzi App LLC. All rights reserved.
//

import Foundation
import UIKit

class HomeCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet weak var imageviews: UIImageView!
    @IBOutlet weak var myLabel: UILabel!
    @IBOutlet weak var badgeCountLabel: UIButton!
    
    override func awakeFromNib() {
        
        // set label Attribute
        
        super.awakeFromNib()
        // Initialization code
    }
}
