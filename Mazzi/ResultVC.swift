//
//  ResultVC.swift
//  Mazzi
//
//  Created by Bayara on 11/11/18.
//  Copyright © 2018 Mazzi Application LLC. All rights reserved.
//

import Foundation
import UIKit
import Apollo
import KVNProgress

class ResultVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
//  var apollo = SettingsViewController.apollo
    @IBOutlet weak var headerView: UIView!
    var refresher = UIRefreshControl()
    @IBOutlet weak var tableview: UITableView!
    var StudentResults = [StudentResultQuery.Data.SingleStudentResult]()
    
    var apollo: ApolloClient = {
        let configuration = URLSessionConfiguration.default
        // Add additional headers as needed
        configuration.httpAdditionalHeaders = ["x-access-token": GlobalVariables.headerToken] // Replace `<token>`
        //        print("TOKEN APOLLOO:: ", GlobalVariables.headerToken)
        let url = URL(string:SettingsViewController.graphQL)!
        
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Өмнөх шалгалтын дүн"
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.tableview.delegate = self
        self.tableview.dataSource = self
        self.tableview.register(UINib(nibName: "resultTableViewCell", bundle: nil), forCellReuseIdentifier: "resultTableViewCell")
        self.tableview.refreshControl = self.refresher
        self.refresher.addTarget(self, action: #selector(reloadResults), for: .valueChanged)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"05"), style: .plain, target: self, action: #selector(baktoroot))
        self.headerView.backgroundColor = GlobalVariables.F5F5F5
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        _ = self.apollo.clearCache()
    }
    
    @objc func reloadResults(){
        self.getData()
    }
    
    @objc func baktoroot(){
            self.navigationController?.popViewController(animated: true)
    }
    
    func getData(){
//        print("------------------------")
//        print(GlobalVariables.user_id)
//        print(GlobalStaticVar.classId)
        let query = StudentResultQuery(studentId: GlobalVariables.user_id , classId:GlobalStaticVar.classId)
            KVNProgress.show(withStatus: "", on: self.view)
             self.apollo.fetch(query: query) { result, error in
                KVNProgress.dismiss()
//                print(result)
                if result?.data?.singleStudentResults != nil {
                     self.StudentResults = (result?.data?.singleStudentResults!)! as! [StudentResultQuery.Data.SingleStudentResult]
                    self.StudentResults =  self.StudentResults.sorted(by: { $0.testStartedAt! > $1.testStartedAt!})
                    self.tableview.reloadData()
                    self.refresher.endRefreshing()
//                    print("SORTED RESULT:: ",ready)
                }else{
                    KVNProgress.showError(withStatus: "Та шалгалт өгөөгүй байна")
                    let when = DispatchTime.now()
                    DispatchQueue.main.asyncAfter(deadline: when + 2) {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
//        var ready =  self.StudentResults.sorted(by: { $0.testStartedAt!($1) == .orderedDescending })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.StudentResults.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "resultTableViewCell", for: indexPath) as! resultTableViewCell
        cell.SemNameLabel.text = self.StudentResults[indexPath.row].seminarName
        if indexPath.row % 2 == 0{
            cell.contentView.backgroundColor = UIColor.white
        }else{
            cell.contentView.backgroundColor = GlobalVariables.F5F5F5
        }
       
        let b:String = String(format:"%0.2f", self.StudentResults[indexPath.row].average!)
        let myInt2 = (b as NSString).integerValue

        if myInt2 > 80 {
            cell.realPointLabel.textColor = GlobalVariables.green
        }else if myInt2 < 80 &&  myInt2 > 60 {
            cell.realPointLabel.textColor = UIColor.orange
        }else if myInt2 < 60{
            cell.realPointLabel.textColor = UIColor.red
        }

        cell.realPointLabel.text = "\(b)\("%")"
         let dateformatter = DateFormatter()
        if self.StudentResults[indexPath.row].testStartedAt != nil {
            let lastlogin = NSDate(timeIntervalSince1970: (self.StudentResults[indexPath.row].testStartedAt!/1000))
//            dateformatter.dateStyle = .short
//            dateformatter.timeStyle = .short
             dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let last = dateformatter.string(from: lastlogin as Date)
            cell.realTrueLabel.text = last
        }else{
            cell.realTrueLabel.text = "---"
        }
        return cell
    }

}

